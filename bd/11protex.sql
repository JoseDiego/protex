CREATE DATABASE  IF NOT EXISTS `11protex` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `11protex`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: 11protex
-- ------------------------------------------------------
-- Server version	5.7.15-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activo`
--

DROP TABLE IF EXISTS `activo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activo` (
  `Activo_Id` int(11) NOT NULL AUTO_INCREMENT,
  `sActDescripcion` varchar(50) NOT NULL,
  `nActEstado` int(11) NOT NULL,
  `nActEliminado` int(11) NOT NULL,
  `dActFecha_Act` datetime NOT NULL,
  `Usuario_Id` int(11) NOT NULL,
  PRIMARY KEY (`Activo_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activo`
--

LOCK TABLES `activo` WRITE;
/*!40000 ALTER TABLE `activo` DISABLE KEYS */;
INSERT INTO `activo` VALUES (1,'N/A',0,0,'2015-10-12 00:00:00',1);
/*!40000 ALTER TABLE `activo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `almacen`
--

DROP TABLE IF EXISTS `almacen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `almacen` (
  `Almacen_Id` int(11) NOT NULL AUTO_INCREMENT,
  `sAlmNombre` varchar(45) NOT NULL,
  `sAlmUbicacion` varchar(45) NOT NULL,
  `nAlmEstado` int(11) NOT NULL,
  `nAlmEliminado` int(11) NOT NULL,
  `dAlmFecha_Act` datetime NOT NULL,
  `Usuario_Id` int(11) NOT NULL,
  PRIMARY KEY (`Almacen_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `almacen`
--

LOCK TABLES `almacen` WRITE;
/*!40000 ALTER TABLE `almacen` DISABLE KEYS */;
INSERT INTO `almacen` VALUES (1,'PROTEX','Av. America - Trujillo',1,0,'2016-12-20 11:51:45',1);
/*!40000 ALTER TABLE `almacen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bitacorausuario`
--

DROP TABLE IF EXISTS `bitacorausuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bitacorausuario` (
  `nBiusuTipoModulo` int(11) NOT NULL,
  `nBiusuUsuario_Id` int(11) NOT NULL,
  `dBiusuFechaHora` datetime NOT NULL,
  `sBiusuDescripcion` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bitacorausuario`
--

LOCK TABLES `bitacorausuario` WRITE;
/*!40000 ALTER TABLE `bitacorausuario` DISABLE KEYS */;
INSERT INTO `bitacorausuario` VALUES (1,17,'2016-02-22 22:27:43','Inició Sessión'),(1,17,'2016-02-22 23:29:57','Cerro Sessión'),(1,17,'2016-02-23 07:48:25','Inició Sessión'),(1,17,'2016-03-03 23:02:39','Inició Sessión'),(1,17,'2016-03-03 23:28:29','Cerro Sessión'),(1,17,'2016-03-06 21:34:37','Inició Sessión'),(1,17,'2016-03-06 22:26:16','Cerro Sessión'),(1,17,'2016-03-31 17:42:51','Inició Sessión'),(1,17,'2016-03-31 17:43:35','Cerro Sessión'),(1,17,'2016-03-31 17:45:55','Inició Sessión'),(1,17,'2016-03-31 17:49:16','Cerro Sessión'),(1,17,'2016-03-31 17:49:37','Inició Sessión'),(1,17,'2016-03-31 17:55:26','Inició Sessión'),(1,17,'2016-03-31 18:00:09','Cerro Sessión'),(1,17,'2016-03-31 18:35:34','Inició Sessión'),(1,17,'2016-03-31 19:15:25','Inició Sessión'),(1,17,'2016-03-31 19:56:56','Cerro Sessión'),(1,17,'2016-03-31 20:07:38','Inició Sessión'),(1,17,'2016-03-31 20:09:13','Cerro Sessión'),(1,19,'2016-03-31 20:10:06','Inició Sessión'),(1,19,'2016-03-31 20:13:15','Cerro Sessión'),(1,19,'2016-04-04 09:53:46','Inició Sessión'),(1,19,'2016-04-04 11:43:14','Cerro Sessión'),(1,17,'2016-04-14 10:34:28','Inició Sessión'),(1,17,'2016-04-14 10:47:56','Inició Sessión'),(1,17,'2016-04-14 11:18:04','Inició Sessión'),(1,17,'2016-04-14 11:19:12','Inició Sessión'),(1,17,'2016-04-14 11:20:42','Cerro Sessión'),(1,17,'2016-04-14 11:21:02','Inició Sessión'),(1,17,'2016-04-14 11:21:58','Cerro Sessión'),(1,19,'2016-04-14 11:33:16','Inició Sessión'),(1,19,'2016-04-15 13:11:34','Inició Sessión'),(1,17,'2016-04-19 20:45:57','Inició Sessión'),(1,17,'2016-04-19 20:59:45','Cerro Sessión'),(1,19,'2016-04-27 16:30:51','Inició Sessión'),(1,19,'2016-04-27 16:53:28','Cerro Sessión'),(1,19,'2016-04-28 20:14:10','Inició Sessión'),(1,19,'2016-04-30 10:15:53','Inició Sessión'),(1,19,'2016-04-30 13:08:31','Cerro Sessión'),(1,19,'2016-05-05 11:35:34','Inició Sessión'),(1,19,'2016-05-06 10:52:59','Inició Sessión'),(1,19,'2016-05-06 11:29:26','Cerro Sessión'),(1,19,'2016-05-07 10:01:20','Inició Sessión'),(1,19,'2016-05-07 12:37:58','Inició Sessión'),(1,19,'2016-05-07 12:48:00','Cerro Sessión'),(1,19,'2016-05-07 12:49:09','Inició Sessión'),(1,19,'2016-05-07 13:11:42','Inició Sessión'),(1,19,'2016-05-07 13:12:53','Inició Sessión'),(1,19,'2016-05-07 13:26:38','Cerro Sessión'),(1,19,'2016-05-09 10:06:40','Inició Sessión'),(1,19,'2016-05-09 11:15:03','Inició Sessión'),(1,19,'2016-05-09 12:02:39','Cerro Sessión'),(1,19,'2016-05-10 10:53:50','Inició Sessión'),(1,19,'2016-05-10 11:02:04','Cerro Sessión'),(1,19,'2016-05-11 09:22:19','Inició Sessión'),(1,19,'2016-05-11 09:23:18','Cerro Sessión'),(1,19,'2016-05-11 09:24:10','Inició Sessión'),(1,19,'2016-05-11 09:33:23','Cerro Sessión'),(1,19,'2016-05-13 09:45:43','Inició Sessión'),(1,19,'2016-05-13 09:57:43','Cerro Sessión'),(1,19,'2016-05-16 18:01:05','Inició Sessión'),(1,17,'2016-05-16 18:08:36','Inició Sessión'),(1,17,'2016-05-16 18:58:00','Cerro Sessión'),(1,19,'2016-05-17 10:14:22','Inició Sessión'),(1,19,'2016-05-17 10:29:48','Cerro Sessión'),(1,19,'2016-05-18 11:41:28','Inició Sessión'),(1,19,'2016-05-18 12:25:16','Cerro Sessión'),(1,19,'2016-05-24 11:19:49','Inició Sessión'),(1,19,'2016-05-24 12:37:22','Cerro Sessión'),(1,19,'2016-05-25 10:45:43','Inició Sessión'),(1,19,'2016-05-25 11:47:02','Cerro Sessión'),(1,19,'2016-05-25 11:53:20','Inició Sessión'),(1,19,'2016-05-25 12:23:55','Cerro Sessión'),(1,19,'2016-05-26 09:47:24','Inició Sessión'),(1,19,'2016-05-26 10:31:19','Cerro Sessión'),(1,19,'2016-05-26 10:33:30','Inició Sessión'),(1,19,'2016-05-26 10:40:23','Cerro Sessión'),(1,19,'2016-05-31 11:39:47','Inició Sessión'),(1,19,'2016-05-31 12:08:28','Cerro Sessión'),(1,19,'2016-06-01 11:58:34','Inició Sessión'),(1,19,'2016-06-01 12:28:18','Cerro Sessión'),(1,19,'2016-06-03 10:37:53','Inició Sessión'),(1,19,'2016-06-03 10:55:56','Cerro Sessión'),(1,19,'2016-06-06 09:23:37','Inició Sessión'),(1,19,'2016-06-06 11:12:48','Cerro Sessión'),(1,19,'2016-06-07 10:11:40','Inició Sessión'),(1,19,'2016-06-07 10:32:33','Cerro Sessión'),(1,19,'2016-06-08 11:00:17','Inició Sessión'),(1,19,'2016-06-08 11:11:18','Cerro Sessión'),(1,19,'2016-06-09 09:49:09','Inició Sessión'),(1,19,'2016-06-09 10:21:28','Cerro Sessión'),(1,19,'2016-06-10 12:34:14','Inició Sessión'),(1,19,'2016-06-10 12:46:01','Cerro Sessión'),(1,19,'2016-06-10 20:21:53','Inició Sessión'),(1,17,'2016-06-10 20:55:26','Inició Sessión'),(1,19,'2016-06-10 20:56:32','Cerro Sessión'),(1,17,'2016-06-10 20:56:40','Cerro Sessión'),(1,19,'2016-06-13 10:19:27','Inició Sessión'),(1,19,'2016-06-13 12:13:17','Cerro Sessión'),(1,19,'2016-06-14 10:12:52','Inició Sessión'),(1,19,'2016-06-14 10:23:41','Cerro Sessión'),(1,19,'2016-06-14 11:15:38','Inició Sessión'),(1,19,'2016-06-14 11:28:47','Cerro Sessión'),(1,19,'2016-06-15 16:25:35','Inició Sessión'),(1,19,'2016-06-15 16:53:45','Cerro Sessión'),(1,19,'2016-06-15 16:57:05','Inició Sessión'),(1,19,'2016-06-15 17:10:53','Cerro Sessión'),(1,19,'2016-06-15 17:32:59','Inició Sessión'),(1,19,'2016-06-15 17:58:30','Cerro Sessión'),(1,19,'2016-06-15 18:00:46','Inició Sessión'),(1,19,'2016-06-15 19:05:55','Cerro Sessión'),(1,19,'2016-06-16 09:40:03','Inició Sessión'),(1,19,'2016-06-16 12:27:06','Cerro Sessión'),(1,19,'2016-06-16 12:36:28','Inició Sessión'),(1,19,'2016-06-16 12:43:20','Cerro Sessión'),(1,19,'2016-06-17 11:54:52','Inició Sessión'),(1,19,'2016-06-17 12:28:46','Cerro Sessión'),(1,19,'2016-06-20 11:44:29','Inició Sessión'),(1,19,'2016-06-20 12:05:50','Cerro Sessión'),(1,19,'2016-06-20 12:07:26','Inició Sessión'),(1,19,'2016-06-20 12:48:27','Cerro Sessión'),(1,19,'2016-06-20 12:53:50','Inició Sessión'),(1,19,'2016-06-20 13:15:54','Cerro Sessión'),(1,19,'2016-06-21 11:10:31','Inició Sessión'),(1,19,'2016-06-21 11:55:56','Cerro Sessión'),(1,19,'2016-06-22 09:51:46','Inició Sessión'),(1,19,'2016-06-22 11:17:28','Cerro Sessión'),(1,19,'2016-06-22 11:24:47','Inició Sessión'),(1,19,'2016-06-22 12:06:08','Cerro Sessión'),(1,19,'2016-06-23 10:19:44','Inició Sessión'),(1,19,'2016-06-23 10:48:41','Cerro Sessión'),(1,19,'2016-06-23 10:57:52','Inició Sessión'),(1,19,'2016-06-23 11:09:05','Cerro Sessión'),(1,19,'2016-06-23 11:15:40','Inició Sessión'),(1,19,'2016-06-23 11:57:06','Cerro Sessión'),(1,19,'2016-06-24 10:39:27','Inició Sessión'),(1,19,'2016-06-24 11:18:59','Cerro Sessión'),(1,19,'2016-06-24 11:56:49','Inició Sessión'),(1,19,'2016-06-24 12:43:16','Cerro Sessión'),(1,17,'2016-06-25 08:53:40','Inició Sessión'),(1,17,'2016-06-25 09:39:52','Inició Sessión'),(1,17,'2016-06-25 10:08:55','Cerro Sessión'),(1,17,'2016-06-25 10:09:40','Inició Sessión'),(1,17,'2016-06-25 10:24:01','Cerro Sessión'),(1,19,'2016-06-27 10:18:14','Inició Sessión'),(1,19,'2016-06-27 10:32:37','Cerro Sessión'),(1,19,'2016-06-28 09:24:40','Inició Sessión'),(1,19,'2016-06-28 09:37:51','Cerro Sessión'),(1,19,'2016-07-04 09:52:57','Inició Sessión'),(1,19,'2016-07-04 10:14:31','Cerro Sessión'),(1,19,'2016-07-07 09:26:20','Inició Sessión'),(1,19,'2016-07-07 09:40:50','Cerro Sessión'),(1,19,'2016-07-07 10:28:50','Inició Sessión'),(1,19,'2016-07-07 10:53:15','Cerro Sessión'),(1,19,'2016-07-07 10:58:05','Inició Sessión'),(1,19,'2016-07-07 12:04:03','Cerro Sessión'),(1,19,'2016-07-08 09:57:04','Inició Sessión'),(1,19,'2016-07-08 10:24:01','Cerro Sessión'),(1,19,'2016-07-11 09:51:58','Inició Sessión'),(1,19,'2016-07-11 10:08:45','Cerro Sessión'),(1,19,'2016-07-11 12:03:27','Inició Sessión'),(1,19,'2016-07-11 12:26:54','Cerro Sessión'),(1,19,'2016-07-12 11:01:12','Inició Sessión'),(1,19,'2016-07-12 11:20:39','Cerro Sessión'),(1,19,'2016-07-12 11:26:49','Inició Sessión'),(1,19,'2016-07-12 11:41:14','Cerro Sessión'),(1,19,'2016-07-12 11:55:41','Inició Sessión'),(1,19,'2016-07-13 09:54:59','Inició Sessión'),(1,19,'2016-07-13 10:02:59','Cerro Sessión'),(1,19,'2016-07-13 11:55:11','Inició Sessión'),(1,19,'2016-07-13 12:00:04','Cerro Sessión'),(1,19,'2016-07-14 09:53:55','Inició Sessión'),(1,19,'2016-07-14 10:18:07','Cerro Sessión'),(1,19,'2016-07-18 10:46:29','Inició Sessión'),(1,19,'2016-07-18 11:53:03','Cerro Sessión'),(1,19,'2016-07-18 12:09:21','Inició Sessión'),(1,19,'2016-07-18 12:13:06','Cerro Sessión'),(1,19,'2016-07-19 09:31:51','Inició Sessión'),(1,19,'2016-07-19 10:35:59','Cerro Sessión'),(1,19,'2016-07-19 10:36:20','Inició Sessión'),(1,19,'2016-07-19 10:51:21','Cerro Sessión'),(1,19,'2016-07-22 09:57:08','Inició Sessión'),(1,19,'2016-07-22 09:59:06','Cerro Sessión'),(1,19,'2016-07-22 10:35:33','Inició Sessión'),(1,19,'2016-07-22 11:13:33','Cerro Sessión'),(1,17,'2016-08-08 09:55:34','Inició Sessión'),(1,17,'2016-08-08 09:56:13','Cerro Sessión'),(1,18,'2016-08-08 09:56:42','Inició Sessión'),(1,18,'2016-08-08 11:08:34','Cerro Sessión'),(1,18,'2016-08-08 11:08:57','Inició Sessión'),(1,19,'2016-08-08 11:14:01','Inició Sessión'),(1,19,'2016-08-08 11:28:38','Cerro Sessión'),(1,18,'2016-08-08 11:33:21','Inició Sessión'),(1,19,'2016-08-08 11:47:59','Inició Sessión'),(1,19,'2016-08-08 11:59:10','Cerro Sessión'),(1,18,'2016-08-09 09:11:52','Inició Sessión'),(1,18,'2016-08-09 11:32:32','Inició Sessión'),(1,18,'2016-08-09 11:36:41','Inició Sessión'),(1,19,'2016-08-09 11:43:10','Inició Sessión'),(1,18,'2016-08-09 11:50:31','Cerro Sessión'),(1,19,'2016-08-09 11:54:36','Cerro Sessión'),(1,19,'2016-08-10 08:37:23','Inició Sessión'),(1,18,'2016-08-10 09:06:22','Inició Sessión'),(1,19,'2016-08-10 11:09:42','Inició Sessión'),(1,19,'2016-08-10 11:48:50','Cerro Sessión'),(1,19,'2016-08-10 11:52:46','Inició Sessión'),(1,19,'2016-08-10 11:55:33','Cerro Sessión'),(1,18,'2016-08-11 09:04:56','Inició Sessión'),(1,19,'2016-08-11 09:43:01','Inició Sessión'),(1,19,'2016-08-11 10:16:17','Cerro Sessión'),(1,19,'2016-08-11 10:31:13','Inició Sessión'),(1,18,'2016-08-12 09:43:33','Inició Sessión'),(1,18,'2016-08-12 10:21:58','Cerro Sessión'),(1,18,'2016-08-12 10:27:18','Inició Sessión'),(1,18,'2016-08-12 10:45:44','Cerro Sessión'),(1,18,'2016-08-12 11:00:12','Inició Sessión'),(1,18,'2016-08-12 11:10:04','Cerro Sessión'),(1,18,'2016-08-12 11:10:18','Inició Sessión'),(1,18,'2016-08-12 11:20:55','Cerro Sessión'),(1,18,'2016-08-12 11:22:50','Inició Sessión'),(1,19,'2016-08-12 11:31:49','Inició Sessión'),(1,18,'2016-08-12 11:35:00','Cerro Sessión'),(1,18,'2016-08-12 11:38:26','Inició Sessión'),(1,19,'2016-08-12 12:01:01','Cerro Sessión'),(1,18,'2016-08-12 12:05:27','Cerro Sessión'),(1,18,'2016-08-12 12:09:25','Inició Sessión'),(1,19,'2016-08-12 12:11:19','Inició Sessión'),(1,19,'2016-08-12 13:05:40','Cerro Sessión'),(1,19,'2016-08-12 15:36:18','Inició Sessión'),(1,19,'2016-08-12 17:55:57','Cerro Sessión'),(1,19,'2016-08-12 17:58:47','Inició Sessión'),(1,19,'2016-08-12 18:03:08','Cerro Sessión'),(1,19,'2016-08-12 19:55:41','Inició Sessión'),(1,19,'2016-08-12 21:26:52','Cerro Sessión'),(1,19,'2016-08-13 07:02:26','Inició Sessión'),(1,19,'2016-08-13 08:43:20','Cerro Sessión'),(1,18,'2016-08-13 09:37:43','Inició Sessión'),(1,18,'2016-08-13 10:22:01','Cerro Sessión'),(1,18,'2016-08-13 10:27:00','Inició Sessión'),(1,18,'2016-08-13 10:40:55','Cerro Sessión'),(1,18,'2016-08-13 10:49:46','Inició Sessión'),(1,18,'2016-08-13 10:59:06','Inició Sessión'),(1,18,'2016-08-13 11:31:12','Cerro Sessión'),(1,18,'2016-08-13 11:49:45','Inició Sessión'),(1,18,'2016-08-13 11:59:08','Cerro Sessión'),(1,17,'2016-08-13 11:59:19','Inició Sessión'),(1,17,'2016-08-13 12:12:18','Cerro Sessión'),(1,17,'2016-08-13 12:26:54','Inició Sessión'),(1,17,'2016-08-13 12:39:55','Inició Sessión'),(1,17,'2016-08-13 12:46:17','Cerro Sessión'),(1,18,'2016-08-13 13:33:57','Inició Sessión'),(1,18,'2016-08-13 13:46:13','Cerro Sessión'),(1,17,'2016-08-13 13:49:43','Cerro Sessión'),(1,17,'2016-08-13 14:05:32','Inició Sessión'),(1,17,'2016-08-13 14:07:17','Cerro Sessión'),(1,20,'2016-08-13 14:08:13','Inició Sessión'),(1,20,'2016-08-13 14:12:03','Cerro Sessión'),(1,20,'2016-08-13 14:12:43','Inició Sessión'),(1,18,'2016-08-16 09:36:07','Inició Sessión'),(1,18,'2016-08-16 09:36:54','Cerro Sessión'),(1,21,'2016-08-16 09:37:25','Inició Sessión'),(1,21,'2016-08-16 09:37:30','Cerro Sessión'),(1,21,'2016-08-16 09:38:03','Inició Sessión'),(1,21,'2016-08-16 09:38:09','Cerro Sessión'),(1,21,'2016-08-16 09:40:25','Inició Sessión'),(1,21,'2016-08-16 10:13:34','Cerro Sessión'),(1,18,'2016-08-16 10:14:02','Inició Sessión'),(1,18,'2016-08-16 10:29:49','Inició Sessión'),(1,18,'2016-08-16 10:53:31','Cerro Sessión'),(1,20,'2016-08-16 10:55:38','Inició Sessión'),(1,20,'2016-08-16 11:01:30','Cerro Sessión'),(1,18,'2016-08-16 11:05:37','Inició Sessión'),(1,18,'2016-08-16 11:05:50','Cerro Sessión'),(1,20,'2016-08-16 11:06:08','Inició Sessión'),(1,20,'2016-08-16 11:16:33','Cerro Sessión'),(1,21,'2016-08-16 11:18:25','Inició Sessión'),(1,21,'2016-08-16 11:35:02','Cerro Sessión'),(1,20,'2016-08-16 11:37:04','Inició Sessión'),(1,20,'2016-08-16 11:50:30','Cerro Sessión'),(1,21,'2016-08-16 11:52:32','Inició Sessión'),(1,21,'2016-08-16 11:58:49','Cerro Sessión'),(1,20,'2016-08-16 12:00:36','Inició Sessión'),(1,20,'2016-08-16 12:01:20','Cerro Sessión'),(1,20,'2016-08-16 12:06:30','Inició Sessión'),(1,21,'2016-08-17 10:10:54','Inició Sessión'),(1,20,'2016-08-17 10:19:20','Inició Sessión'),(1,21,'2016-08-17 10:34:15','Inició Sessión'),(1,18,'2016-08-17 10:49:02','Inició Sessión'),(1,18,'2016-08-17 11:16:11','Cerro Sessión'),(1,18,'2016-08-17 11:16:41','Inició Sessión'),(1,18,'2016-08-17 11:16:41','Inició Sessión'),(1,21,'2016-08-17 11:58:26','Inició Sessión'),(1,21,'2016-08-17 12:02:37','Inició Sessión'),(1,21,'2016-08-17 15:07:38','Inició Sessión'),(1,21,'2016-08-17 16:09:08','Inició Sessión'),(1,20,'2016-08-17 16:14:13','Inició Sessión'),(1,20,'2016-08-17 16:53:43','Inició Sessión'),(1,20,'2016-08-17 16:59:57','Inició Sessión'),(1,20,'2016-08-17 21:28:16','Inició Sessión'),(1,20,'2016-08-18 09:43:29','Inició Sessión'),(1,20,'2016-08-18 10:03:11','Inició Sessión'),(1,20,'2016-08-18 15:54:52','Inició Sessión'),(1,21,'2016-08-18 19:08:48','Inició Sessión'),(1,21,'2016-08-19 11:18:29','Inició Sessión'),(1,21,'2016-08-19 11:27:14','Cerro Sessión'),(1,21,'2016-08-19 11:29:55','Inició Sessión'),(1,21,'2016-08-19 12:00:10','Inició Sessión'),(1,21,'2016-08-19 12:04:55','Inició Sessión'),(1,21,'2016-08-19 12:36:43','Cerro Sessión'),(1,21,'2016-08-19 12:41:12','Inició Sessión'),(1,20,'2016-08-19 16:05:38','Inició Sessión'),(1,20,'2016-08-19 21:14:13','Inició Sessión'),(1,20,'2016-08-19 22:00:34','Inició Sessión'),(1,17,'2016-08-20 14:10:58','Inició Sessión'),(1,17,'2016-08-20 14:34:38','Cerro Sessión'),(1,20,'2016-08-20 18:59:53','Inició Sessión'),(1,20,'2016-08-20 20:40:56','Inició Sessión'),(1,20,'2016-08-21 18:55:52','Inició Sessión'),(1,20,'2016-08-21 20:09:56','Inició Sessión'),(1,21,'2016-08-22 16:32:03','Inició Sessión'),(1,21,'2016-08-22 17:00:16','Inició Sessión'),(1,21,'2016-08-22 17:28:55','Inició Sessión'),(1,21,'2016-08-22 17:42:21','Inició Sessión'),(1,21,'2016-08-22 18:26:09','Inició Sessión'),(1,21,'2016-08-22 21:42:33','Inició Sessión'),(1,19,'2016-08-23 10:17:35','Inició Sessión'),(1,19,'2016-08-23 10:17:58','Cerro Sessión'),(1,20,'2016-08-23 10:20:01','Inició Sessión'),(1,20,'2016-08-23 10:20:37','Cerro Sessión'),(1,20,'2016-08-23 10:25:17','Inició Sessión'),(1,20,'2016-08-23 12:20:23','Cerro Sessión'),(1,20,'2016-08-23 12:32:10','Inició Sessión'),(1,20,'2016-08-23 12:32:14','Cerro Sessión'),(1,20,'2016-08-23 12:33:05','Inició Sessión'),(1,20,'2016-08-23 12:56:54','Inició Sessión'),(1,20,'2016-08-23 13:12:36','Inició Sessión'),(1,21,'2016-08-23 17:37:29','Inició Sessión'),(1,21,'2016-08-23 17:51:04','Inició Sessión'),(1,19,'2016-08-24 11:14:25','Inició Sessión'),(1,19,'2016-08-24 11:25:03','Cerro Sessión'),(1,20,'2016-08-24 11:25:20','Inició Sessión'),(1,20,'2016-08-24 11:33:48','Cerro Sessión'),(1,19,'2016-08-24 11:34:38','Inició Sessión'),(1,19,'2016-08-24 11:43:44','Cerro Sessión'),(1,20,'2016-08-24 14:44:20','Inició Sessión'),(1,21,'2016-08-24 16:22:24','Inició Sessión'),(1,21,'2016-08-24 20:10:50','Inició Sessión'),(1,18,'2016-08-25 09:48:07','Inició Sessión'),(1,19,'2016-08-25 10:23:29','Inició Sessión'),(1,19,'2016-08-25 10:36:42','Cerro Sessión'),(1,20,'2016-08-25 14:56:46','Inició Sessión'),(1,20,'2016-08-26 13:33:21','Inició Sessión'),(1,21,'2016-08-26 20:01:40','Inició Sessión'),(1,20,'2016-08-27 14:07:12','Inició Sessión'),(1,21,'2016-08-27 18:49:28','Inició Sessión'),(1,20,'2016-08-29 14:50:49','Inició Sessión'),(1,20,'2016-08-29 18:16:15','Inició Sessión'),(1,20,'2016-08-29 19:26:26','Inició Sessión'),(1,21,'2016-08-30 15:37:21','Inició Sessión'),(1,20,'2016-08-30 19:24:50','Inició Sessión'),(1,20,'2016-08-30 20:08:52','Inició Sessión'),(1,19,'2016-09-01 09:33:11','Inició Sessión'),(1,19,'2016-09-01 10:06:07','Cerro Sessión'),(1,19,'2016-09-01 10:07:07','Inició Sessión'),(1,19,'2016-09-01 10:40:13','Cerro Sessión'),(1,20,'2016-09-01 10:58:03','Inició Sessión'),(1,20,'2016-09-01 11:25:42','Cerro Sessión'),(1,20,'2016-09-01 11:56:33','Inició Sessión'),(1,20,'2016-09-01 12:04:15','Cerro Sessión'),(1,18,'2016-09-01 12:04:45','Inició Sessión'),(1,17,'2016-09-01 20:16:18','Inició Sessión'),(1,20,'2016-09-01 21:26:18','Inició Sessión'),(1,20,'2016-09-02 20:07:30','Inició Sessión'),(1,20,'2016-09-04 19:53:13','Inició Sessión'),(1,21,'2016-09-05 14:34:26','Inició Sessión'),(1,21,'2016-09-05 20:41:00','Inició Sessión'),(1,21,'2016-09-05 21:11:26','Inició Sessión'),(1,21,'2016-09-05 21:38:55','Inició Sessión'),(1,21,'2016-09-05 21:45:33','Inició Sessión'),(1,21,'2016-09-05 22:06:07','Inició Sessión'),(1,21,'2016-09-06 00:18:21','Inició Sessión'),(1,21,'2016-09-06 00:25:29','Inició Sessión'),(1,21,'2016-09-06 00:38:04','Inició Sessión'),(1,21,'2016-09-06 00:49:25','Inició Sessión'),(1,21,'2016-09-06 00:54:56','Inició Sessión'),(1,19,'2016-09-06 11:39:40','Inició Sessión'),(1,19,'2016-09-06 11:43:43','Cerro Sessión'),(1,21,'2016-09-06 17:19:39','Inició Sessión'),(1,21,'2016-09-06 18:19:33','Inició Sessión'),(1,21,'2016-09-06 19:10:05','Inició Sessión'),(1,20,'2016-09-07 08:48:00','Inició Sessión'),(1,21,'2016-09-07 16:57:42','Inició Sessión'),(1,21,'2016-09-08 19:04:22','Inició Sessión'),(1,17,'2016-09-08 19:53:51','Inició Sessión'),(1,17,'2016-09-08 20:15:52','Cerro Sessión'),(1,17,'2016-09-08 20:31:13','Inició Sessión'),(1,17,'2016-09-08 20:33:38','Cerro Sessión'),(1,17,'2016-09-08 20:37:11','Inició Sessión'),(1,17,'2016-09-08 20:42:19','Inició Sessión'),(1,17,'2016-09-08 21:25:29','Inició Sessión'),(1,17,'2016-09-08 21:26:00','Cerro Sessión'),(1,19,'2016-09-09 12:46:19','Inició Sessión'),(1,19,'2016-09-09 12:56:21','Cerro Sessión'),(1,21,'2016-09-09 18:14:57','Inició Sessión'),(1,19,'2016-09-13 09:31:27','Inició Sessión'),(1,19,'2016-09-13 12:20:30','Cerro Sessión'),(1,20,'2016-09-13 12:32:56','Inició Sessión'),(1,20,'2016-09-13 19:14:08','Inició Sessión'),(1,19,'2016-09-14 11:43:11','Inició Sessión'),(1,19,'2016-09-14 12:14:10','Cerro Sessión'),(1,20,'2016-09-17 12:19:50','Inició Sessión'),(1,20,'2016-09-17 12:25:41','Inició Sessión'),(1,20,'2016-09-17 12:30:14','Inició Sessión'),(1,20,'2016-09-17 15:22:35','Inició Sessión'),(1,20,'2016-09-17 15:43:56','Inició Sessión'),(1,20,'2016-09-17 16:32:54','Inició Sessión'),(1,19,'2016-09-19 11:21:44','Inició Sessión'),(1,19,'2016-09-19 12:01:47','Cerro Sessión'),(1,21,'2016-09-19 15:41:13','Inició Sessión'),(1,19,'2016-09-20 10:11:32','Inició Sessión'),(1,19,'2016-09-20 10:46:21','Cerro Sessión'),(1,19,'2016-09-20 12:17:33','Inició Sessión'),(1,19,'2016-09-20 13:02:37','Cerro Sessión'),(1,20,'2016-09-21 09:11:37','Inició Sessión'),(1,20,'2016-09-21 09:18:21','Inició Sessión'),(1,20,'2016-09-21 10:01:08','Cerro Sessión'),(1,19,'2016-09-21 10:56:29','Inició Sessión'),(1,19,'2016-09-21 11:16:31','Cerro Sessión'),(1,19,'2016-09-21 11:30:00','Inició Sessión'),(1,19,'2016-09-21 11:53:49','Cerro Sessión'),(1,20,'2016-09-21 11:54:07','Inició Sessión'),(1,20,'2016-09-21 12:36:39','Cerro Sessión'),(1,20,'2016-09-21 16:36:15','Inició Sessión'),(1,19,'2016-09-22 11:34:19','Inició Sessión'),(1,19,'2016-09-22 11:47:06','Cerro Sessión'),(1,21,'2016-09-22 19:30:28','Inició Sessión'),(1,20,'2016-09-23 08:43:31','Inició Sessión'),(1,21,'2016-09-23 16:55:51','Inició Sessión'),(1,20,'2016-09-24 13:06:13','Inició Sessión'),(1,21,'2016-09-24 17:37:15','Inició Sessión'),(1,21,'2016-09-25 19:48:50','Inició Sessión'),(1,20,'2016-09-26 15:27:44','Inició Sessión'),(1,20,'2016-09-27 16:19:55','Inició Sessión'),(1,19,'2016-09-28 10:48:55','Inició Sessión'),(1,19,'2016-09-28 10:52:23','Cerro Sessión'),(1,19,'2016-09-28 11:16:13','Inició Sessión'),(1,19,'2016-09-28 11:22:20','Cerro Sessión'),(1,21,'2016-09-28 14:04:51','Inició Sessión'),(1,19,'2016-09-29 12:52:14','Inició Sessión'),(1,19,'2016-09-29 13:35:34','Cerro Sessión'),(1,20,'2016-09-29 16:33:32','Inició Sessión'),(1,19,'2016-09-30 12:54:05','Inició Sessión'),(1,19,'2016-09-30 12:58:53','Cerro Sessión'),(1,21,'2016-10-03 17:39:40','Inició Sessión'),(1,21,'2016-10-03 19:20:09','Inició Sessión'),(1,21,'2016-10-03 19:39:42','Inició Sessión'),(1,21,'2016-10-03 19:44:38','Inició Sessión'),(1,21,'2016-10-03 19:54:22','Inició Sessión'),(1,21,'2016-10-05 16:32:29','Inició Sessión'),(1,21,'2016-10-05 16:51:59','Inició Sessión'),(1,21,'2016-10-05 21:14:22','Inició Sessión'),(1,19,'2016-10-06 12:13:15','Inició Sessión'),(1,19,'2016-10-06 12:22:30','Cerro Sessión'),(1,21,'2016-10-06 18:47:16','Inició Sessión'),(1,21,'2016-10-06 18:47:17','Inició Sessión'),(1,21,'2016-10-06 18:47:17','Inició Sessión'),(1,21,'2016-10-06 18:47:17','Inició Sessión'),(1,21,'2016-10-07 18:10:19','Inició Sessión'),(1,21,'2016-10-09 18:44:44','Inició Sessión'),(1,21,'2016-10-09 18:46:48','Inició Sessión'),(1,19,'2016-10-10 11:06:44','Inició Sessión'),(1,19,'2016-10-10 11:39:15','Cerro Sessión'),(1,19,'2016-10-10 11:41:39','Inició Sessión'),(1,19,'2016-10-10 11:56:30','Cerro Sessión'),(1,20,'2016-10-10 17:55:12','Inició Sessión'),(1,19,'2016-10-11 11:54:40','Inició Sessión'),(1,19,'2016-10-11 12:06:46','Cerro Sessión'),(1,19,'2016-10-12 10:52:38','Inició Sessión'),(1,19,'2016-10-12 11:06:11','Cerro Sessión'),(1,20,'2016-10-12 17:22:43','Inició Sessión'),(1,20,'2016-10-12 18:37:06','Inició Sessión'),(1,19,'2016-10-13 10:07:52','Inició Sessión'),(1,19,'2016-10-13 10:27:55','Cerro Sessión'),(1,21,'2016-10-13 10:28:16','Inició Sessión'),(1,21,'2016-10-13 10:32:32','Cerro Sessión'),(1,19,'2016-10-13 10:32:50','Inició Sessión'),(1,19,'2016-10-13 11:43:39','Cerro Sessión'),(1,21,'2016-10-13 15:27:20','Inició Sessión'),(1,21,'2016-10-13 15:32:11','Inició Sessión'),(1,21,'2016-10-13 15:36:29','Inició Sessión'),(1,19,'2016-10-14 12:14:07','Inició Sessión'),(1,19,'2016-10-14 12:22:49','Cerro Sessión'),(1,21,'2016-10-15 08:23:06','Inició Sessión'),(1,20,'2016-10-16 03:08:24','Inició Sessión'),(1,21,'2016-10-17 17:30:36','Inició Sessión'),(1,21,'2016-10-17 17:56:53','Inició Sessión'),(1,21,'2016-10-17 18:11:46','Inició Sessión'),(1,21,'2016-10-17 18:39:07','Inició Sessión'),(1,21,'2016-10-17 18:55:07','Inició Sessión'),(1,21,'2016-10-18 18:42:39','Inició Sessión'),(1,21,'2016-10-18 18:51:23','Inició Sessión'),(1,19,'2016-10-19 11:21:47','Inició Sessión'),(1,19,'2016-10-19 12:12:16','Cerro Sessión'),(1,21,'2016-10-19 19:12:06','Inició Sessión'),(1,19,'2016-10-20 11:18:14','Inició Sessión'),(1,19,'2016-10-20 11:20:26','Cerro Sessión'),(1,20,'2016-10-21 11:27:26','Inició Sessión'),(1,20,'2016-10-21 11:53:11','Inició Sessión'),(1,20,'2016-10-21 12:08:52','Cerro Sessión'),(1,19,'2016-10-21 12:09:22','Inició Sessión'),(1,19,'2016-10-21 12:18:57','Cerro Sessión'),(1,19,'2016-10-21 12:32:43','Inició Sessión'),(1,19,'2016-10-21 12:34:09','Cerro Sessión'),(1,20,'2016-10-21 12:58:59','Inició Sessión'),(1,19,'2016-10-24 10:59:33','Inició Sessión'),(1,19,'2016-10-24 11:45:46','Cerro Sessión'),(1,20,'2016-10-24 14:11:31','Inició Sessión'),(1,19,'2016-10-25 11:27:10','Inició Sessión'),(1,19,'2016-10-25 11:53:59','Cerro Sessión'),(1,21,'2016-10-25 12:03:29','Inició Sessión'),(1,20,'2016-10-25 17:34:09','Inició Sessión'),(1,20,'2016-10-25 20:30:40','Inició Sessión'),(1,19,'2016-10-26 10:53:38','Inició Sessión'),(1,19,'2016-10-26 10:56:53','Cerro Sessión'),(1,20,'2016-10-27 18:48:56','Inició Sessión'),(1,21,'2016-10-28 14:53:12','Inició Sessión'),(1,21,'2016-10-30 11:32:03','Inició Sessión'),(1,21,'2016-10-31 14:34:31','Inició Sessión'),(1,21,'2016-10-31 21:18:10','Inició Sessión'),(1,21,'2016-10-31 21:40:27','Inició Sessión'),(1,20,'2016-11-01 12:17:45','Inició Sessión'),(1,21,'2016-11-02 16:30:09','Inició Sessión'),(1,21,'2016-11-03 18:04:41','Inició Sessión'),(1,21,'2016-11-04 17:44:17','Inició Sessión'),(1,21,'2016-11-04 17:52:53','Inició Sessión'),(1,17,'2016-11-04 18:56:33','Inició Sessión'),(1,17,'2016-11-04 20:20:41','Inició Sessión'),(1,17,'2016-11-04 20:32:38','Cerro Sessión'),(1,17,'2016-11-04 21:06:01','Inició Sessión'),(1,21,'2016-11-04 21:09:05','Inició Sessión'),(1,17,'2016-11-04 21:26:55','Inició Sessión'),(1,17,'2016-11-20 11:11:55','Inició Sessión'),(1,17,'2016-11-20 11:39:16','Cerro Sessión'),(1,1,'2016-11-20 11:40:24','Inició Sessión'),(1,1,'2016-11-20 12:50:22','Cerro Sessión'),(1,1,'2016-11-20 12:58:00','Inició Sessión'),(1,1,'2016-11-20 13:03:10','Inició Sessión'),(1,1,'2016-11-20 13:05:32','Cerro Sessión'),(1,1,'2016-11-20 13:17:03','Inició Sessión'),(1,1,'2016-11-20 16:06:03','Cerro Sessión'),(1,2,'2016-11-21 11:23:58','Inició Sessión'),(1,1,'2016-12-09 15:13:52','Inició Sessión'),(1,1,'2016-12-09 20:44:54','Inició Sessión'),(1,1,'2016-12-09 21:56:14','Cerro Sessión'),(1,1,'2016-12-09 21:56:17','Cerro Sessión'),(1,1,'2016-12-09 22:12:40','Inició Sessión'),(1,1,'2016-12-10 21:54:25','Inició Sessión'),(1,1,'2016-12-10 22:43:10','Cerro Sessión'),(1,1,'2016-12-10 22:43:13','Cerro Sessión'),(1,1,'2016-12-10 23:02:03','Inició Sessión'),(1,1,'2016-12-10 23:27:23','Cerro Sessión'),(1,1,'2016-12-10 23:27:26','Cerro Sessión'),(1,1,'2016-12-10 23:36:10','Inició Sessión'),(1,1,'2016-12-11 14:35:26','Inició Sessión'),(1,1,'2016-12-12 21:51:12','Inició Sessión'),(1,1,'2016-12-12 22:38:33','Cerro Sessión'),(1,1,'2016-12-12 22:38:36','Cerro Sessión'),(1,1,'2016-12-12 22:51:58','Inició Sessión'),(1,1,'2016-12-12 23:14:16','Cerro Sessión'),(1,1,'2016-12-19 16:36:01','Inició Sessión'),(1,1,'2016-12-19 17:09:09','Cerro Sessión'),(1,1,'2016-12-19 17:15:20','Inició Sessión'),(1,1,'2016-12-19 19:13:12','Inició Sessión'),(1,1,'2016-12-19 20:20:02','Inició Sessión'),(1,1,'2016-12-20 01:42:38','Cerro Sessión'),(1,1,'2016-12-20 01:42:57','Inició Sessión'),(1,1,'2016-12-20 11:05:03','Inició Sessión'),(1,1,'2016-12-20 15:53:25','Inició Sessión'),(1,1,'2016-12-20 20:37:41','Inició Sessión'),(1,1,'2016-12-20 21:39:08','Cerro Sessión'),(1,1,'2016-12-20 22:14:51','Inició Sessión'),(1,1,'2016-12-22 10:51:24','Inició Sessión'),(1,1,'2016-12-23 18:31:21','Inició Sessión'),(1,1,'2016-12-23 20:27:08','Inició Sessión'),(1,1,'2016-12-28 17:31:50','Inició Sessión'),(1,1,'2016-12-28 19:04:28','Inició Sessión'),(1,1,'2016-12-28 20:02:17','Inició Sessión'),(1,1,'2016-12-28 22:20:36','Inició Sessión'),(1,1,'2016-12-29 00:12:26','Cerro Sessión'),(1,1,'2016-12-29 10:36:06','Inició Sessión'),(1,1,'2016-12-29 14:09:20','Cerro Sessión'),(1,1,'2016-12-29 14:09:55','Inició Sessión'),(1,1,'2016-12-29 15:25:07','Cerro Sessión'),(1,1,'2016-12-29 15:25:10','Cerro Sessión'),(1,1,'2016-12-29 16:43:43','Inició Sessión'),(1,1,'2016-12-30 09:13:03','Inició Sessión'),(1,1,'2016-12-30 14:09:26','Cerro Sessión'),(1,1,'2016-12-30 14:09:29','Cerro Sessión'),(1,1,'2016-12-30 14:10:29','Inició Sessión'),(1,1,'2016-12-30 17:59:01','Cerro Sessión'),(1,1,'2016-12-30 17:59:04','Cerro Sessión'),(1,1,'2016-12-30 18:57:44','Inició Sessión'),(1,1,'2016-12-30 20:18:32','Inició Sessión'),(1,1,'2017-01-16 23:53:22','Inició Sessión'),(1,1,'2017-01-17 11:42:17','Inició Sessión'),(1,1,'2017-01-17 11:57:57','Cerro Sessión'),(1,1,'2017-01-17 11:58:19','Inició Sessión'),(1,1,'2017-01-17 12:19:27','Cerro Sessión'),(1,1,'2017-01-17 12:19:39','Inició Sessión'),(1,1,'2017-01-17 15:13:59','Cerro Sessión'),(1,1,'2017-01-17 15:14:10','Inició Sessión'),(1,1,'2017-01-17 15:16:35','Cerro Sessión'),(1,1,'2017-01-17 15:16:44','Inició Sessión'),(1,1,'2017-01-17 15:51:53','Cerro Sessión'),(1,1,'2017-01-17 15:52:06','Inició Sessión'),(1,1,'2017-01-17 20:25:41','Cerro Sessión'),(1,1,'2017-01-17 20:30:07','Inició Sessión'),(1,1,'2017-01-17 21:02:14','Cerro Sessión'),(1,1,'2017-01-17 21:02:27','Inició Sessión'),(1,1,'2017-01-17 21:49:14','Cerro Sessión'),(1,1,'2017-01-17 21:49:47','Inició Sessión'),(1,1,'2017-01-17 21:57:46','Cerro Sessión'),(1,1,'2017-01-17 21:57:57','Inició Sessión'),(1,1,'2017-01-17 22:32:37','Cerro Sessión'),(1,1,'2017-01-17 22:32:47','Inició Sessión'),(1,1,'2017-01-17 22:57:25','Cerro Sessión'),(1,1,'2017-01-17 22:57:37','Inició Sessión'),(1,1,'2017-01-17 23:05:39','Cerro Sessión'),(1,1,'2017-01-17 23:05:49','Inició Sessión'),(1,1,'2017-01-18 10:28:14','Cerro Sessión'),(1,1,'2017-01-18 10:35:08','Inició Sessión'),(1,1,'2017-01-18 11:20:14','Cerro Sessión'),(1,1,'2017-01-18 11:20:27','Inició Sessión'),(1,1,'2017-01-19 21:25:10','Inició Sessión'),(1,1,'2017-01-19 21:27:54','Cerro Sessión'),(1,1,'2017-01-19 21:28:10','Inició Sessión'),(1,1,'2017-01-20 03:06:30','Cerro Sessión'),(1,1,'2017-01-20 03:06:50','Inició Sessión'),(1,1,'2017-01-20 03:08:55','Inició Sessión'),(1,1,'2017-01-20 21:21:40','Cerro Sessión'),(1,1,'2017-01-20 21:21:53','Inició Sessión'),(1,1,'2017-01-20 22:06:41','Cerro Sessión'),(1,1,'2017-01-20 22:06:54','Inició Sessión'),(1,1,'2017-01-20 23:46:22','Inició Sessión'),(1,1,'2017-01-21 11:14:42','Inició Sessión'),(1,1,'2017-01-21 11:37:49','Inició Sessión'),(1,1,'2017-01-21 11:50:30','Inició Sessión'),(1,1,'2017-01-21 11:55:42','Inició Sessión'),(1,1,'2017-01-21 11:57:19','Inició Sessión'),(1,1,'2017-01-21 21:31:49','Cerro Sessión'),(1,1,'2017-01-21 23:32:15','Inició Sessión'),(1,1,'2017-01-22 09:44:58','Inició Sessión'),(1,1,'2017-01-23 09:42:44','Inició Sessión'),(1,1,'2017-01-23 11:45:46','Cerro Sessión'),(1,1,'2017-01-23 11:45:55','Inició Sessión'),(1,1,'2017-01-23 13:31:48','Cerro Sessión'),(1,1,'2017-01-23 13:32:00','Inició Sessión'),(1,1,'2017-01-23 19:47:03','Inició Sessión'),(1,1,'2017-01-23 23:09:43','Cerro Sessión'),(1,1,'2017-01-24 00:32:04','Inició Sessión'),(1,1,'2017-01-24 08:47:54','Inició Sessión'),(1,1,'2017-01-25 16:47:44','Cerro Sessión'),(1,1,'2017-01-25 16:47:44','Cerro Sessión'),(1,1,'2017-01-25 16:47:44','Cerro Sessión'),(1,1,'2017-01-25 16:47:44','Cerro Sessión'),(1,1,'2017-01-25 16:47:44','Cerro Sessión'),(1,1,'2017-01-25 16:47:44','Cerro Sessión'),(1,1,'2017-01-25 16:47:44','Cerro Sessión'),(1,1,'2017-01-25 16:47:44','Cerro Sessión'),(1,1,'2017-01-25 16:47:44','Cerro Sessión'),(1,1,'2017-01-25 16:47:44','Cerro Sessión'),(1,1,'2017-01-25 16:47:44','Cerro Sessión'),(1,1,'2017-01-25 16:47:44','Cerro Sessión'),(1,1,'2017-01-25 16:47:44','Cerro Sessión'),(1,1,'2017-01-25 16:47:44','Cerro Sessión'),(1,1,'2017-01-25 16:47:45','Cerro Sessión'),(1,1,'2017-01-25 16:47:45','Cerro Sessión'),(1,1,'2017-01-25 16:47:45','Cerro Sessión'),(1,1,'2017-01-25 17:03:02','Inició Sessión'),(1,1,'2017-01-26 10:44:44','Inició Sessión'),(1,1,'2017-01-26 10:46:13','Cerro Sessión'),(1,4,'2017-01-26 10:46:38','Inició Sessión'),(1,4,'2017-01-27 09:54:30','Cerro Sessión'),(1,4,'2017-01-27 09:54:30','Cerro Sessión'),(1,4,'2017-01-27 09:54:31','Cerro Sessión'),(1,4,'2017-01-27 09:54:31','Cerro Sessión'),(1,4,'2017-01-27 09:54:31','Cerro Sessión'),(1,4,'2017-01-27 09:54:31','Cerro Sessión'),(1,4,'2017-01-27 09:54:31','Cerro Sessión'),(1,4,'2017-01-27 09:54:31','Cerro Sessión'),(1,4,'2017-01-27 09:54:31','Cerro Sessión'),(1,4,'2017-01-27 09:54:31','Cerro Sessión'),(1,4,'2017-01-27 09:54:31','Cerro Sessión'),(1,4,'2017-01-27 09:54:31','Cerro Sessión'),(1,4,'2017-01-27 09:54:31','Cerro Sessión'),(1,4,'2017-01-27 09:54:31','Cerro Sessión'),(1,4,'2017-01-27 09:54:31','Cerro Sessión'),(1,4,'2017-01-27 09:54:31','Cerro Sessión'),(1,4,'2017-01-27 09:54:31','Cerro Sessión'),(1,4,'2017-01-27 09:54:32','Cerro Sessión'),(1,4,'2017-01-27 09:54:32','Cerro Sessión'),(1,4,'2017-01-27 09:54:32','Cerro Sessión'),(1,4,'2017-01-27 09:54:32','Cerro Sessión'),(1,4,'2017-01-27 09:54:32','Cerro Sessión'),(1,1,'2017-01-27 09:55:22','Inició Sessión'),(1,1,'2017-01-27 13:29:26','Cerro Sessión'),(1,1,'2017-01-28 09:24:27','Inició Sessión'),(1,1,'2017-01-28 09:56:53','Inició Sessión'),(1,1,'2017-01-28 10:17:59','Inició Sessión'),(1,1,'2017-01-28 13:18:01','Inició Sessión'),(1,1,'2017-01-28 20:39:41','Inició Sessión'),(1,1,'2017-01-28 20:54:01','Inició Sessión'),(1,1,'2017-01-28 21:09:08','Inició Sessión'),(1,1,'2017-01-29 01:21:47','Inició Sessión'),(1,1,'2017-01-29 09:17:03','Inició Sessión'),(1,1,'2017-01-30 10:53:03','Inició Sessión'),(1,1,'2017-01-30 14:05:12','Cerro Sessión'),(1,1,'2017-01-30 14:24:19','Inició Sessión'),(1,1,'2017-02-16 23:37:33','Inició Sessión'),(1,1,'2017-02-18 00:15:40','Cerro Sessión'),(1,1,'2017-02-18 00:15:42','Cerro Sessión'),(1,1,'2017-02-18 00:15:56','Inició Sessión'),(1,1,'2017-02-18 13:03:35','Cerro Sessión'),(1,1,'2017-02-18 13:20:41','Inició Sessión'),(1,1,'2017-02-18 14:21:52','Cerro Sessión'),(1,1,'2017-02-18 14:44:51','Inició Sessión'),(1,1,'2017-02-18 19:13:50','Cerro Sessión'),(1,1,'2017-02-20 00:52:47','Inició Sessión');
/*!40000 ALTER TABLE `bitacorausuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clasificador`
--

DROP TABLE IF EXISTS `clasificador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clasificador` (
  `Clasificador_Id` int(11) NOT NULL AUTO_INCREMENT,
  `sClaDescripcion` text NOT NULL,
  `nClaEliminado` int(11) NOT NULL,
  `dClaFecha_Act` datetime NOT NULL,
  `Usuario_Id` int(11) NOT NULL,
  PRIMARY KEY (`Clasificador_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clasificador`
--

LOCK TABLES `clasificador` WRITE;
/*!40000 ALTER TABLE `clasificador` DISABLE KEYS */;
INSERT INTO `clasificador` VALUES (1,'N/A',0,'2015-11-02 00:00:00',1);
/*!40000 ALTER TABLE `clasificador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conceptofijo`
--

DROP TABLE IF EXISTS `conceptofijo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conceptofijo` (
  `Concepto_Id` int(11) NOT NULL AUTO_INCREMENT,
  `sConNombre` varchar(45) DEFAULT NULL,
  `nConValor` double DEFAULT NULL,
  PRIMARY KEY (`Concepto_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conceptofijo`
--

LOCK TABLES `conceptofijo` WRITE;
/*!40000 ALTER TABLE `conceptofijo` DISABLE KEYS */;
INSERT INTO `conceptofijo` VALUES (1,'IGV',18);
/*!40000 ALTER TABLE `conceptofijo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documento`
--

DROP TABLE IF EXISTS `documento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documento` (
  `Documento_Id` int(11) NOT NULL AUTO_INCREMENT,
  `Almacen_Id` int(11) NOT NULL,
  `nDocTipoMovimiento_Id` int(11) NOT NULL,
  `sDocNombre` varchar(50) NOT NULL,
  `sDocNombreCorto` varchar(50) NOT NULL,
  `nDocNomAutomatico` tinyint(4) NOT NULL,
  `sDocSiguiente` varchar(15) NOT NULL DEFAULT '0',
  `nDocAfectoIGV` tinyint(4) NOT NULL,
  `nDocEstado` tinyint(4) NOT NULL,
  `nDocEliminado` int(11) NOT NULL,
  `dDocFecha_Act` datetime NOT NULL,
  `Usuario_Id` int(11) NOT NULL,
  PRIMARY KEY (`Documento_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documento`
--

LOCK TABLES `documento` WRITE;
/*!40000 ALTER TABLE `documento` DISABLE KEYS */;
INSERT INTO `documento` VALUES (1,1,2,'FACTURA PROTEX','FT 001',1,'101',1,1,0,'2016-11-20 11:47:44',1),(2,1,2,'BOLETA VENTA PROTEX','BV 001',0,'201',0,1,0,'2016-11-20 11:47:57',1),(3,1,1,'FACTURA','FT',0,'0',1,1,0,'2016-03-31 17:58:56',17),(4,1,1,'BOLETA VENTA','BV',0,'1',0,1,0,'2015-10-12 13:04:14',1),(5,0,0,'ANULACION','ANUL',1,'1',0,1,0,'2015-10-12 13:05:03',1),(6,0,0,'ACTUALIZACION','ACT',1,'1',0,1,0,'2015-10-12 13:08:27',1),(7,1,2,'NOTA DE VENTA','NV',1,'300',0,1,0,'2016-11-20 11:48:14',1),(8,1,3,'NOTA DE INGRESO','NI',1,'10',0,1,0,'2016-11-20 11:48:27',1),(9,1,1,'NOTA DE COMPRA','NC',1,'6',0,1,0,'2016-05-07 12:40:59',19),(10,1,4,'NOTA DE SALIDA','NS',1,'22',0,1,0,'2016-11-20 11:48:39',1),(11,1,1,'asdf','asdf',0,'1',0,1,0,'2017-02-20 02:42:57',1);
/*!40000 ALTER TABLE `documento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `familia`
--

DROP TABLE IF EXISTS `familia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `familia` (
  `Familia_Id` int(11) NOT NULL AUTO_INCREMENT,
  `sFamCodigo` varchar(30) NOT NULL,
  `sFamDescripcion` varchar(60) NOT NULL,
  `nFamPadre` int(11) NOT NULL,
  `nFamTipo` tinyint(4) NOT NULL,
  `nFamEstado` tinyint(4) NOT NULL,
  `nFamEliminado` int(11) NOT NULL,
  `dFamFecha_Act` datetime NOT NULL,
  `Usuario_Id` int(11) NOT NULL,
  PRIMARY KEY (`Familia_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `familia`
--

LOCK TABLES `familia` WRITE;
/*!40000 ALTER TABLE `familia` DISABLE KEYS */;
INSERT INTO `familia` VALUES (1,'','FAMILIAS',0,0,1,0,'2015-11-23 00:00:00',1),(2,'1','PRODUCTOS',1,1,1,0,'2015-11-23 00:00:00',1),(3,'2','SERVICIOS',1,2,1,0,'2015-11-23 00:00:00',1),(14,'1.1','PRENDAS',2,1,1,0,'2016-11-20 12:01:04',1),(15,'1.2','INSUMOS',2,1,1,0,'2016-11-20 12:01:28',1);
/*!40000 ALTER TABLE `familia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moneda`
--

DROP TABLE IF EXISTS `moneda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moneda` (
  `Moneda_Id` int(11) NOT NULL AUTO_INCREMENT,
  `sMonDescripcion` varchar(50) DEFAULT NULL,
  `sMonAlias` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`Moneda_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moneda`
--

LOCK TABLES `moneda` WRITE;
/*!40000 ALTER TABLE `moneda` DISABLE KEYS */;
INSERT INTO `moneda` VALUES (1,'Soles','S/.'),(2,'Dolares','US$');
/*!40000 ALTER TABLE `moneda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movimiento`
--

DROP TABLE IF EXISTS `movimiento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movimiento` (
  `Movimiento_Id` int(11) NOT NULL AUTO_INCREMENT,
  `dMovFecha` date NOT NULL,
  `nMovTipoMovimiento_Id` int(11) NOT NULL,
  `nMovTipopago` tinyint(4) NOT NULL,
  `Almacen_Id` int(11) NOT NULL,
  `nMovTipoOrigenDestino` int(11) NOT NULL,
  `nMovOrigenDestino_Id` int(11) NOT NULL,
  `nMovTipodestino` tinyint(4) NOT NULL,
  `Documento_Id` int(11) NOT NULL,
  `sMovDocumento` varchar(20) NOT NULL,
  `sMovDocReferencia` varchar(20) NOT NULL,
  `Moneda_Id` int(11) NOT NULL,
  `nMovTipoCambio` decimal(10,2) NOT NULL,
  `nMovImporte` decimal(10,2) NOT NULL,
  `nMovIGV` decimal(10,2) NOT NULL,
  `nMovTotal` decimal(10,2) NOT NULL,
  `nMovSaldo` decimal(10,2) DEFAULT NULL,
  `nMovTotalCancelado` decimal(10,2) NOT NULL,
  `dMovDetraccion` decimal(10,2) NOT NULL,
  `dMovPercepcion` decimal(10,2) NOT NULL,
  `dMovRetencion` decimal(10,2) NOT NULL,
  `sMovObservacion` varchar(200) NOT NULL,
  `nMovEstado` tinyint(4) NOT NULL,
  `nMovEliminado` tinyint(4) NOT NULL,
  `dMovFecha_Act` datetime NOT NULL,
  `Usuario_Id` int(11) NOT NULL,
  `nMovClasificador_Id` int(11) NOT NULL,
  `nMovMovimiento_Id` int(11) NOT NULL,
  `nMovOrdenCampo_Id` int(11) NOT NULL,
  PRIMARY KEY (`Movimiento_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movimiento`
--

LOCK TABLES `movimiento` WRITE;
/*!40000 ALTER TABLE `movimiento` DISABLE KEYS */;
INSERT INTO `movimiento` VALUES (1,'2016-12-28',1,1,1,1,1,1,3,'0','erf',1,1.00,254.24,45.76,300.00,NULL,300.00,1.00,0.00,0.00,'',2,0,'2016-12-28 22:56:18',1,0,0,0),(2,'2016-12-28',1,1,1,1,1,1,3,'3456','edsa',1,1.00,677.97,122.03,800.00,NULL,800.00,1.00,0.00,0.00,'dergvc',2,0,'2016-12-28 23:03:38',1,0,0,0),(3,'2016-12-29',1,1,1,1,1,1,3,'5643','eeee',1,1.00,84.75,15.25,100.00,NULL,100.00,1.00,0.00,0.00,'',2,0,'2016-12-29 10:40:16',1,0,0,0),(4,'2016-12-29',4,1,1,1,1,1,10,'20','dfer',1,1.00,33.90,0.00,33.90,NULL,33.90,0.00,0.00,0.00,'',2,0,'2016-12-29 10:44:29',1,0,0,0),(5,'2016-12-29',4,1,1,1,1,1,10,'21','r',1,1.00,60.00,0.00,60.00,NULL,60.00,1.00,0.00,0.00,'rre',2,0,'2016-12-29 18:35:13',1,0,0,0);
/*!40000 ALTER TABLE `movimiento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movimientodetalle`
--

DROP TABLE IF EXISTS `movimientodetalle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movimientodetalle` (
  `Movimiento_Id` int(11) NOT NULL,
  `ProductoLote_Id` int(11) NOT NULL,
  `nMovDetUnidad_Id` int(11) NOT NULL,
  `nMovDetCantidad` int(11) NOT NULL,
  `nMovDetPrecioLista` int(11) NOT NULL,
  `nMovDetPorcDescuento1` int(11) NOT NULL,
  `nMovDetPorcDecuento2` int(11) NOT NULL,
  `nMovDetPrecioUnitario` double NOT NULL,
  `nMovDetImporte` double NOT NULL,
  `nMovDetPrecioFlete` double NOT NULL,
  `nMovDetSaldoAnteriorLote` double NOT NULL,
  `nMovDetSaldoPosteriorLote` double NOT NULL,
  `nMovDetSaldoAnterior` double NOT NULL,
  `nMovDetSaldoPosterior` double NOT NULL,
  `nMovDetIncluyeIGV` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movimientodetalle`
--

LOCK TABLES `movimientodetalle` WRITE;
/*!40000 ALTER TABLE `movimientodetalle` DISABLE KEYS */;
INSERT INTO `movimientodetalle` VALUES (1,1,29,10,25,0,0,25.423728813559,254.23728813559,0,0,120,0,120,0),(2,2,2,20,34,0,0,33.898305084746,677.96610169492,0,0,20,0,20,0),(3,3,2,5,17,0,0,16.949152542373,84.745762711864,0,0,5,20,25,0),(4,3,2,2,17,0,0,16.949152542373,33.898305084746,0,5,3,25,23,0),(5,2,2,4,15,0,0,15,60,0,20,16,23,19,0);
/*!40000 ALTER TABLE `movimientodetalle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movimientodetalleconsumo`
--

DROP TABLE IF EXISTS `movimientodetalleconsumo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movimientodetalleconsumo` (
  `Movimiento_Id` int(11) NOT NULL,
  `Producto_Id` int(11) DEFAULT NULL,
  `nMovDetConUnidad_Id` int(11) DEFAULT NULL,
  `sMovDetConCantidad` double DEFAULT NULL,
  `nMovDetConPrecioLista` double DEFAULT NULL,
  `nMovDetConPorcDescuento1` double DEFAULT NULL,
  `nMovDetConPorcDescuento2` double DEFAULT NULL,
  `nMovDetConPrecioUnitario` double DEFAULT NULL,
  `nMovDetConImporte` double DEFAULT NULL,
  `nMovDetConPrecioFlete` double DEFAULT NULL,
  PRIMARY KEY (`Movimiento_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movimientodetalleconsumo`
--

LOCK TABLES `movimientodetalleconsumo` WRITE;
/*!40000 ALTER TABLE `movimientodetalleconsumo` DISABLE KEYS */;
/*!40000 ALTER TABLE `movimientodetalleconsumo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `operario`
--

DROP TABLE IF EXISTS `operario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `operario` (
  `Operario_Id` int(11) NOT NULL AUTO_INCREMENT,
  `sOpeDni` varchar(8) NOT NULL,
  `sOpeApePat` varchar(45) NOT NULL,
  `sOpeApeMat` varchar(45) NOT NULL,
  `sOpeNombres` varchar(45) NOT NULL,
  `sOpeCargo` varchar(45) NOT NULL,
  `sOpeDireccion` varchar(45) NOT NULL,
  `sOpeTelefono` varchar(45) NOT NULL,
  `sOpeCorreo` varchar(45) DEFAULT NULL,
  `nOpeEstado` tinyint(4) NOT NULL,
  `nOpeEliminado` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Operario_Id`,`sOpeTelefono`),
  UNIQUE KEY `Operario_Id_UNIQUE` (`Operario_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `operario`
--

LOCK TABLES `operario` WRITE;
/*!40000 ALTER TABLE `operario` DISABLE KEYS */;
INSERT INTO `operario` VALUES (1,'48311683','Diaz','Cardenas','Jose Diego','Developer','Av 123 123','98654781','diego@gmail.com',1,0),(5,'12345678','nOpeEstadoa','Holalo','Holalo','Holalo','Holalo','Holalo','Holalo',0,1),(7,'12345678','WHERE nOpeEliminado = 0','WHERE nOpeEliminado = 0','WHERE nOpeEliminado = 0','WHERE nOpeEliminado = 0','WHERE nOpeEliminado = 0','WHERE nOpeEliminado = 0','WHERE nOpeEliminado = 0',0,1);
/*!40000 ALTER TABLE `operario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordentrabajo`
--

DROP TABLE IF EXISTS `ordentrabajo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordentrabajo` (
  `OrdenTrabajo_Id` int(11) NOT NULL AUTO_INCREMENT,
  `otFecha` date NOT NULL,
  `otNroOrdenCompra` varchar(10) NOT NULL,
  `otCliente_Id` int(11) NOT NULL,
  `otNroOrdenTrabajo` varchar(10) NOT NULL,
  `otFechaE` date NOT NULL,
  `otEstado` tinyint(4) DEFAULT NULL,
  `otEliminado` tinyint(4) NOT NULL,
  `otFecha_Act` datetime NOT NULL,
  `Usuario_Id` int(11) NOT NULL,
  PRIMARY KEY (`OrdenTrabajo_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ordentrabajo`
--

LOCK TABLES `ordentrabajo` WRITE;
/*!40000 ALTER TABLE `ordentrabajo` DISABLE KEYS */;
INSERT INTO `ordentrabajo` VALUES (3,'2017-01-27','1',2,'10','2017-01-27',1,0,'2017-01-27 10:36:46',1),(7,'2017-01-27','09',2,'3','2017-02-10',1,0,'2017-01-27 10:41:03',1),(9,'2017-01-27','2000',2,'2000','2017-02-11',1,0,'2017-01-27 10:57:33',1),(10,'2017-01-28','9090',2,'10','2017-02-11',1,0,'2017-01-28 10:03:25',1),(11,'2017-01-28','90',2,'90900','2017-01-28',1,0,'2017-01-29 10:02:52',1),(12,'2017-01-29','90',2,'9090','2017-02-10',1,0,'2017-01-29 11:32:57',1),(13,'2017-01-29','21',2,'90900','2017-02-11',1,0,'2017-01-29 12:21:33',1),(14,'2017-01-29','9898',2,'90900','2017-02-11',1,0,'2017-01-29 14:44:48',1),(15,'2017-01-29','665',2,'9090','2017-12-22',1,0,'2017-01-29 14:48:55',1),(16,'2017-01-30','200',2,'9000','2017-12-22',1,1,'2017-01-30 14:25:16',1),(17,'2017-01-30','201',2,'9000','2017-07-12',1,1,'2017-01-30 14:29:16',1),(18,'2017-01-30','200',2,'9000','2017-01-30',1,1,'2017-01-30 14:35:08',1),(19,'2017-01-30','0012',2,'001-1','2017-01-30',1,1,'2017-01-30 14:55:05',1),(20,'2017-01-30','767',2,'001-1','2017-12-07',1,1,'2017-01-30 14:56:49',1),(21,'2017-01-30','0009',2,'1234','2017-07-13',1,0,'2017-01-30 15:01:39',1),(22,'2017-02-17','666',2,'2000','2017-02-16',1,0,'2017-02-17 07:18:33',1),(23,'2017-02-17','987654321',2,'1234','2017-02-17',1,0,'2017-02-17 07:19:11',1),(24,'2017-02-17',' ',0,'1234','2017-02-17',1,0,'2017-02-17 07:19:21',1),(25,'2017-02-09','213123',2,'9090','2017-02-17',1,0,'2017-02-17 07:27:27',1),(26,'2017-02-18','12312',2,'1000','2017-02-22',1,0,'2017-02-18 10:47:02',1),(27,'2017-02-18','54654',2,'1000','2017-02-18',1,0,'2017-02-18 10:47:22',1);
/*!40000 ALTER TABLE `ordentrabajo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordentrabajo_detalle`
--

DROP TABLE IF EXISTS `ordentrabajo_detalle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordentrabajo_detalle` (
  `OrdenTrabajo_detalle` int(11) NOT NULL AUTO_INCREMENT,
  `OrdenTrabajo_Id` int(11) NOT NULL,
  `ProdServ_Id` int(11) NOT NULL,
  `UnidadMedida_Id` int(11) NOT NULL,
  `CantidadAsig` decimal(10,2) NOT NULL,
  `CantidadProg` decimal(10,2) NOT NULL,
  `CantidadRest` decimal(10,2) NOT NULL,
  PRIMARY KEY (`OrdenTrabajo_detalle`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ordentrabajo_detalle`
--

LOCK TABLES `ordentrabajo_detalle` WRITE;
/*!40000 ALTER TABLE `ordentrabajo_detalle` DISABLE KEYS */;
INSERT INTO `ordentrabajo_detalle` VALUES (3,3,63,2,10.00,10.00,0.00),(4,4,59,30,20.00,15.00,5.00),(11,7,59,30,20.00,8.00,12.00),(12,8,59,0,122.00,121.00,1.00),(13,8,12,0,2000.00,112.00,1888.00),(14,8,53,0,1000.00,800.00,200.00),(15,9,76,2,26.00,20.00,6.00),(16,9,74,2,200.00,200.00,0.00),(17,9,63,2,1000.00,1000.00,0.00),(18,10,63,2,10.00,10.00,0.00),(19,11,77,2,30.00,20.00,10.00),(20,11,77,2,30.00,20.00,10.00),(21,12,77,2,20.00,9.00,11.00),(22,12,76,2,10.00,9.00,1.00),(23,13,77,2,10.00,5.00,5.00),(24,14,77,2,5.00,5.00,0.00),(25,15,77,2,11.00,6.00,5.00),(26,15,76,2,1.00,1.00,0.00),(27,16,77,2,12.00,10.00,2.00),(28,17,77,2,2.00,2.00,0.00),(29,18,77,2,12.00,10.00,2.00),(30,19,77,2,1.00,1.00,0.00),(31,20,77,2,1.00,1.00,0.00),(32,20,77,2,12.00,10.00,2.00),(33,20,77,2,100.00,50.00,50.00),(34,20,77,2,6.00,6.00,0.00),(35,20,77,2,3.00,3.00,0.00),(36,21,76,2,10.00,5.00,5.00),(37,21,77,2,12.00,6.00,6.00),(38,22,76,2,6.00,6.00,0.00),(39,23,76,2,5.00,5.00,0.00),(40,23,77,2,6.00,6.00,0.00),(41,25,77,2,5.00,5.00,0.00),(42,25,76,2,0.00,0.00,0.00),(43,26,59,0,20.00,10.00,10.00),(44,27,59,0,10.00,10.00,0.00);
/*!40000 ALTER TABLE `ordentrabajo_detalle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `origendestino`
--

DROP TABLE IF EXISTS `origendestino`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `origendestino` (
  `OrigenDestino_Id` int(11) NOT NULL AUTO_INCREMENT,
  `nODTipo` int(11) NOT NULL,
  `sODRucDni` varchar(11) NOT NULL,
  `sODNombre` varchar(50) NOT NULL,
  `sODNombreComercial` varchar(50) NOT NULL,
  `sODDireccion` varchar(100) NOT NULL,
  `sODTelefono` varchar(50) NOT NULL,
  `sODCorreo` varchar(50) NOT NULL,
  `sODContacto` varchar(30) NOT NULL,
  `nODDiasCredito` varchar(11) NOT NULL,
  `nODEstado` tinyint(4) NOT NULL,
  `dODFechaActualizacion` datetime NOT NULL,
  `nODUsuario_Id` int(11) NOT NULL,
  `nODEliminado` tinyint(4) NOT NULL,
  PRIMARY KEY (`OrigenDestino_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `origendestino`
--

LOCK TABLES `origendestino` WRITE;
/*!40000 ALTER TABLE `origendestino` DISABLE KEYS */;
INSERT INTO `origendestino` VALUES (1,1,'11223344556','PROTEX CORPORATION S.A.C.','PROTEX CORPORATION S.A.C.','','','','','0',1,'2016-11-20 12:16:57',1,0),(2,2,'5938457',' CLIENTES VARIOS',' CLIENTES VARIOS','av. 23 de marzo','506949','clie@clientes.com','49586767','0',1,'2017-01-19 21:27:44',1,0),(3,2,'20445545556','MV SERVICIOS S.R.L.','MV SERVICIOS S.R.L.','PACASMAYO','','pacasmayo@grupomv.pe','Felipe Marroquín','15',1,'2016-11-21 11:27:48',2,0),(4,2,'2038947','eee','eee','ddd','11111','eg@f.com','prueba','30',1,'2016-12-20 13:28:44',1,1),(5,1,'12345678901','Home','Home','Mi Casa','234345','contacto@home.com','Juan','0',1,'2017-01-23 20:09:40',1,0);
/*!40000 ALTER TABLE `origendestino` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pago`
--

DROP TABLE IF EXISTS `pago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pago` (
  `Pago_Id` int(11) NOT NULL AUTO_INCREMENT,
  `Movimiento_Id` int(11) DEFAULT NULL,
  `dPagFecha` date DEFAULT NULL,
  `nPagTipoPago_Id` int(11) DEFAULT NULL,
  `sPagDocumento` varchar(45) DEFAULT NULL,
  `nPagImporte` double DEFAULT NULL,
  `nPagLetra_Id` int(11) DEFAULT NULL,
  `dPagFecha_Act` datetime DEFAULT NULL,
  `nPagUsuario_Id` int(11) DEFAULT NULL,
  `nPagEliminado` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`Pago_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=227 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pago`
--

LOCK TABLES `pago` WRITE;
/*!40000 ALTER TABLE `pago` DISABLE KEYS */;
INSERT INTO `pago` VALUES (1,1,'2016-03-31',1,'',100,1,'2016-03-31 19:13:41',17,0),(2,1,'2016-03-31',3,'',88.5,1,'2016-03-31 19:16:36',17,0),(3,84,'2016-05-11',1,'',9,1,'2016-05-16 18:50:49',17,0),(4,25,'2016-05-13',1,'',55,1,'2016-05-24 11:45:56',19,0),(5,26,'2016-05-13',1,'',55,1,'2016-05-24 11:47:26',19,0),(6,114,'2016-05-16',1,'',15,1,'2016-06-10 20:30:54',19,0),(7,91,'2016-05-22',1,'',4.1,1,'2016-06-13 11:17:14',19,0),(8,111,'2016-05-24',1,'',4,1,'2016-06-13 11:19:12',19,0),(9,111,'1969-12-31',1,'',0,1,'2016-06-13 11:19:15',19,0),(10,29,'2016-05-24',1,'',3.5,1,'2016-06-13 11:20:08',19,0),(11,29,'2016-05-24',1,'',3.5,1,'2016-06-13 11:20:57',19,0),(12,42,'2016-05-24',1,'',3.5,1,'2016-06-13 11:21:45',19,0),(13,42,'2016-05-24',1,'',3.5,1,'2016-06-13 11:24:31',19,0),(14,42,'2016-05-24',1,'',0,1,'2016-06-13 11:24:47',19,0),(15,42,'2016-05-24',1,'',2,1,'2016-06-13 11:25:17',19,0),(16,42,'1969-12-31',1,'',0,1,'2016-06-13 11:25:20',19,0),(17,42,'1969-12-31',1,'',0,1,'2016-06-13 11:25:53',19,0),(18,96,'2016-05-26',1,'',3.5,1,'2016-06-13 11:31:42',19,0),(19,96,'1969-12-31',1,'',0,1,'2016-06-13 11:35:40',19,0),(20,96,'2016-05-26',1,'',5,1,'2016-06-13 11:36:11',19,0),(21,96,'2016-05-26',1,'',5,1,'2016-06-13 11:36:32',19,0),(22,96,'2016-05-26',1,'',2,1,'2016-06-13 11:36:57',19,0),(23,110,'2016-05-26',1,'',2,1,'2016-06-13 11:41:58',19,0),(24,110,'2016-05-26',1,'',2,1,'2016-06-13 11:42:10',19,0),(25,110,'2016-05-26',1,'',5,1,'2016-06-13 11:42:31',19,0),(26,110,'2016-05-26',1,'',5,1,'2016-06-13 11:42:41',19,0),(27,151,'2016-05-26',1,'',2,1,'2016-06-13 11:43:05',19,0),(28,151,'2016-05-26',1,'',5,1,'2016-06-13 11:43:16',19,0),(29,151,'2016-05-26',1,'',5,1,'2016-06-13 11:43:26',19,0),(30,226,'2016-05-26',1,'',5,1,'2016-06-13 11:44:33',19,0),(31,226,'2016-05-26',1,'',5,1,'2016-06-13 11:44:43',19,0),(32,124,'2016-05-30',1,'',16,1,'2016-06-13 11:45:38',19,0),(33,124,'2016-05-30',1,'',111.3,1,'2016-06-13 11:46:03',19,0),(34,126,'2016-05-30',1,'',12,1,'2016-06-13 11:46:26',19,0),(35,125,'2016-05-30',1,'',16,1,'2016-06-13 11:47:36',19,0),(36,125,'1969-12-31',1,'',0,1,'2016-06-13 11:47:41',19,0),(37,198,'2016-06-09',1,'',21.5,1,'2016-06-13 11:49:00',19,0),(38,199,'2016-06-09',1,'',27,1,'2016-06-13 12:05:50',19,0),(39,199,'2016-06-09',1,'',11.5,1,'2016-06-13 12:06:26',19,0),(40,200,'2016-06-09',1,'',11.5,1,'2016-06-13 12:06:56',19,0),(41,119,'2016-05-19',2,'',481.35,1,'2016-06-13 12:08:25',19,0),(42,182,'2016-05-31',2,'',221.7,1,'2016-06-13 12:09:19',19,0),(43,183,'2016-05-31',2,'',173.6,1,'2016-06-13 12:10:13',19,0),(44,183,'1969-12-31',1,'',0,1,'2016-06-13 12:10:17',19,0),(45,188,'2016-06-12',1,'',12.1,1,'2016-06-14 10:13:26',19,0),(46,227,'2016-05-26',1,'',5,1,'2016-06-15 16:36:16',19,0),(47,227,'1969-12-31',1,'',0,1,'2016-06-15 16:36:19',19,0),(48,47,'2016-05-13',1,'',55,1,'2016-06-15 16:40:41',19,0),(49,47,'1969-12-31',1,'',0,1,'2016-06-15 16:40:44',19,0),(50,196,'2016-06-17',2,'',220.1,1,'2016-06-17 11:55:46',19,0),(51,196,'1969-12-31',1,'',0,1,'2016-06-17 11:55:51',19,0),(52,133,'2016-06-10',1,'',10.5,1,'2016-06-22 10:45:24',19,0),(53,150,'2016-06-10',1,'',10.5,1,'2016-06-22 10:46:06',19,0),(54,150,'1969-12-31',1,'',0,1,'2016-06-22 10:46:09',19,0),(55,370,'1969-12-31',1,'',70,1,'2016-06-22 12:03:14',19,0),(56,330,'2016-06-23',1,'',2,1,'2016-06-23 10:58:23',19,0),(57,330,'1969-12-31',1,'',0,1,'2016-06-23 10:58:26',19,0),(58,224,'2015-07-25',1,'',60,1,'2016-06-25 09:16:03',17,0),(59,355,'2016-06-30',2,'',304.28,1,'2016-07-04 10:04:11',19,0),(60,329,'2016-07-03',1,'',18,1,'2016-07-11 12:07:02',19,0),(61,329,'1969-12-31',1,'',0,1,'2016-07-11 12:07:10',19,0),(62,331,'2016-07-03',1,'',17,1,'2016-07-11 12:07:50',19,0),(63,399,'2016-07-03',1,'',10,1,'2016-07-11 12:08:26',19,0),(64,399,'2016-07-03',1,'',4.5,1,'2016-07-11 12:08:58',19,0),(65,401,'2016-07-03',1,'',4.5,1,'2016-07-11 12:10:19',19,0),(66,37,'2016-07-11',1,'',30,1,'2016-07-13 09:55:33',19,0),(67,275,'2016-07-11',1,'',3,1,'2016-07-13 09:56:25',19,0),(68,275,'1969-12-31',1,'',0,1,'2016-07-13 09:56:28',19,0),(69,353,'2016-07-19',1,'',27,1,'2016-07-22 09:57:32',19,0),(70,591,'2016-07-30',2,'vaucher op 5961',269.22,1,'2016-08-10 09:47:09',19,0),(71,595,'2016-07-30',2,'op 596168',160.7,1,'2016-08-10 09:48:14',19,0),(72,79,'2016-07-24',1,'4113',6,1,'2016-08-10 11:27:24',19,0),(73,314,'2016-07-26',1,'5752',19.6,1,'2016-08-10 11:29:40',19,0),(74,496,'2016-08-10',1,'4112',14.5,1,'2016-08-10 11:30:39',19,0),(75,496,'2016-08-10',1,'5731',12,1,'2016-08-10 11:31:04',19,0),(76,496,'1969-12-31',1,'',0,1,'2016-08-10 11:31:06',19,0),(77,496,'2016-08-10',1,'5749',15,1,'2016-08-10 11:31:27',19,0),(78,514,'2016-08-10',1,'',15,1,'2016-08-10 11:32:37',19,0),(79,514,'2016-08-10',1,'',14.5,1,'2016-08-10 11:32:47',19,0),(80,568,'2016-08-10',1,'',14.5,1,'2016-08-10 11:33:19',19,0),(81,568,'1969-12-31',1,'',0,1,'2016-08-10 11:33:22',19,0),(82,568,'1969-12-31',1,'',0,1,'2016-08-10 11:33:25',19,0),(83,343,'2016-07-20',1,'5752',19.6,1,'2016-08-10 11:34:23',19,0),(84,208,'2016-07-24',1,'',6,1,'2016-08-10 11:37:10',19,0),(85,208,'1969-12-31',1,'',0,1,'2016-08-10 11:37:12',19,0),(86,385,'2016-07-26',1,'',19.6,1,'2016-08-10 11:37:46',19,0),(87,232,'2016-08-11',1,'',4,1,'2016-08-11 09:38:31',18,0),(88,517,'2016-07-26',1,'',19.6,1,'2016-08-11 09:50:55',19,0),(89,300,'2016-07-06',1,'',4.6,1,'2016-08-11 09:52:32',19,0),(90,300,'1969-12-31',1,'',0,1,'2016-08-11 09:52:34',19,0),(91,300,'1969-12-31',1,'',4.6,1,'2016-08-11 09:53:48',19,0),(92,454,'2016-07-23',1,'',49.4,1,'2016-08-11 09:59:39',19,0),(93,463,'2016-07-25',1,'',6.2,1,'2016-08-11 10:00:10',19,0),(94,463,'2016-07-26',1,'',3.5,1,'2016-08-11 10:00:41',19,0),(95,463,'2016-07-26',1,'',5.5,1,'2016-08-11 10:01:07',19,0),(96,463,'2016-07-26',1,'',11,1,'2016-08-11 10:02:21',19,0),(97,208,'2016-07-24',1,'',6,1,'2016-08-11 10:43:35',19,0),(98,29,'2016-08-11',1,'',9,1,'2016-08-12 16:04:23',19,0),(99,606,'2016-08-12',1,'',126,1,'2016-08-12 20:03:47',19,0),(100,726,'2016-08-12',1,'',126,1,'2016-08-12 20:06:50',19,0),(101,726,'1969-12-31',1,'',0,1,'2016-08-12 20:06:52',19,0),(102,726,'2016-08-12',1,'',126,1,'2016-08-12 20:11:21',19,0),(103,857,'2016-08-20',1,'',25,1,'2016-08-20 14:34:09',17,0),(104,720,'2016-08-12',2,'',535.78,1,'2016-08-24 11:37:37',19,0),(105,912,'2016-08-25',2,'',80,1,'2016-08-25 10:34:59',19,0),(106,333,'2016-09-01',1,'',3.5,1,'2016-09-01 09:35:04',19,0),(107,333,'1969-12-31',1,'',0,1,'2016-09-01 09:35:17',19,0),(108,333,'2016-09-01',1,'',4.8,1,'2016-09-01 09:35:33',19,0),(109,333,'1969-12-31',1,'',0,1,'2016-09-01 09:35:37',19,0),(110,333,'2016-09-01',1,'5736',9.5,1,'2016-09-01 09:36:13',19,0),(111,333,'1969-12-31',1,'',0,1,'2016-09-01 09:36:15',19,0),(112,333,'2016-09-01',1,'5865',11.5,1,'2016-09-01 09:36:38',19,0),(113,194,'2016-08-21',1,'5836',50,1,'2016-09-01 09:42:10',19,0),(114,640,'2016-08-21',1,'5537',5,1,'2016-09-01 09:42:55',19,0),(115,640,'1969-12-31',1,'',0,1,'2016-09-01 09:42:57',19,0),(116,845,'2016-08-30',1,'5968',26.4,1,'2016-09-01 09:44:41',19,0),(117,845,'2016-08-30',1,'5968',26.4,1,'2016-09-01 09:45:33',19,0),(118,275,'2016-08-30',1,'5964',19.5,1,'2016-09-01 09:46:10',19,0),(119,408,'2016-09-01',1,'5716',4.8,1,'2016-09-01 09:53:22',19,0),(120,408,'2016-09-01',1,'5736',9.5,1,'2016-09-01 09:53:43',19,0),(121,408,'2016-09-01',1,'5865',11.5,1,'2016-09-01 09:54:11',19,0),(122,269,'2016-09-01',1,'5836',50,1,'2016-09-01 09:55:27',19,0),(123,269,'1969-12-31',1,'',0,1,'2016-09-01 09:55:29',19,0),(124,292,'2016-09-01',1,'5968',26.4,1,'2016-09-01 09:56:19',19,0),(125,614,'2016-09-01',1,'5836',50,1,'2016-09-01 09:56:41',19,0),(126,628,'2016-09-01',1,'5968',26.4,1,'2016-09-01 09:57:19',19,0),(127,477,'2016-09-01',1,'5716',4.8,1,'2016-09-01 10:00:18',19,0),(128,477,'2016-09-01',1,'5736',9.5,1,'2016-09-01 10:00:36',19,0),(129,477,'2016-09-01',1,'5865',11.5,1,'2016-09-01 10:01:09',19,0),(130,477,'1969-12-31',1,'',0,1,'2016-09-01 10:01:14',19,0),(131,622,'2016-08-21',1,'5836',50,1,'2016-09-01 10:04:50',19,0),(132,628,'2016-08-30',1,'5968',26.4,1,'2016-09-01 10:05:24',19,0),(133,728,'2016-08-21',1,'5836',50,1,'2016-09-01 10:09:43',19,0),(134,925,'2016-08-30',1,'5968',26.4,1,'2016-09-01 10:10:44',19,0),(135,501,'2016-09-01',1,'5736',9.5,1,'2016-09-01 10:12:41',19,0),(136,757,'2016-09-01',1,'5865',11.5,1,'2016-09-01 10:13:16',19,0),(137,675,'2016-09-01',1,'5780',4.5,1,'2016-09-01 10:14:44',19,0),(138,277,'2016-09-08',1,'',5,1,'2016-09-08 19:54:33',17,0),(139,316,'2016-09-08',1,'',5,1,'2016-09-08 19:55:03',17,0),(140,351,'2016-09-08',1,'',10,1,'2016-09-08 19:55:26',17,0),(141,995,'2016-09-08',1,'',5,1,'2016-09-08 20:32:19',17,0),(142,969,'2016-09-08',1,'',10,1,'2016-09-08 20:33:07',17,0),(143,924,'2016-09-11',1,'',2.4,1,'2016-09-13 09:39:59',19,0),(144,924,'1969-12-31',1,'',0,1,'2016-09-13 09:40:02',19,0),(145,943,'2016-08-30',1,'',1.5,1,'2016-09-13 09:41:08',19,0),(146,277,'2016-09-10',1,'',31.5,1,'2016-09-13 09:44:24',19,0),(147,316,'2016-09-10',1,'',31.5,1,'2016-09-13 09:46:44',19,0),(148,316,'1969-12-31',1,'',0,1,'2016-09-13 09:46:46',19,0),(149,351,'2016-09-10',1,'6030',31.5,1,'2016-09-13 09:48:15',19,0),(150,632,'2016-09-10',1,'',31.5,1,'2016-09-13 09:49:21',19,0),(151,465,'2016-08-13',1,'',14,1,'2016-09-13 10:04:43',19,0),(152,465,'1969-12-31',1,'',0,1,'2016-09-13 10:04:45',19,0),(153,1002,'2016-09-10',1,'',31.5,1,'2016-09-13 10:05:14',19,0),(154,1002,'1969-12-31',1,'',0,1,'2016-09-13 10:05:18',19,0),(155,717,'2016-09-06',1,'',12,1,'2016-09-13 11:58:08',19,0),(156,841,'2016-09-06',1,'',12,1,'2016-09-13 11:59:53',19,0),(157,841,'1969-12-31',1,'',0,1,'2016-09-13 12:00:02',19,0),(158,973,'2016-09-06',1,'',12,1,'2016-09-13 12:02:11',19,0),(159,973,'1969-12-31',1,'',0,1,'2016-09-13 12:02:14',19,0),(160,947,'2016-09-13',1,'',203.6,1,'2016-09-14 11:51:36',19,0),(161,1028,'2016-09-16',2,'781803',117,1,'2016-09-20 10:13:00',19,0),(162,1028,'2016-09-20',2,'124462',56.6,1,'2016-09-20 10:13:56',19,0),(163,966,'2016-09-18',1,'',9,1,'2016-09-20 12:19:21',19,0),(164,966,'2016-09-18',1,'',23,1,'2016-09-20 12:19:42',19,0),(165,981,'2016-09-18',1,'',23,1,'2016-09-20 12:21:12',19,0),(166,81,'2016-09-18',1,'',2.4,1,'2016-09-20 12:23:11',19,0),(167,340,'2016-09-18',1,'',24,1,'2016-09-20 12:23:55',19,0),(168,649,'2016-09-18',1,'',24,1,'2016-09-20 12:24:47',19,0),(169,702,'2016-09-18',1,'',103,1,'2016-09-20 12:25:51',19,0),(170,758,'2016-09-18',1,'',80.4,1,'2016-09-20 12:27:10',19,0),(171,847,'2016-09-18',1,'',14,1,'2016-09-20 12:28:04',19,0),(172,858,'2016-09-18',1,'',7,1,'2016-09-20 12:28:55',19,0),(173,772,'2016-09-11',1,'',5.5,1,'2016-09-20 12:36:49',19,0),(174,787,'2016-09-11',1,'',6.1,1,'2016-09-20 12:37:31',19,0),(175,790,'2016-09-11',1,'',3.5,1,'2016-09-20 12:38:02',19,0),(176,793,'2016-09-11',1,'',4.7,1,'2016-09-20 12:38:56',19,0),(177,793,'2016-09-20',1,'',4.7,1,'2016-09-20 12:39:44',19,0),(178,800,'2016-09-11',1,'',4.7,1,'2016-09-20 12:40:30',19,0),(179,1009,'2016-09-16',1,'',15,1,'2016-09-20 13:00:57',19,0),(180,247,'2016-09-21',3,'TARJETA VISA',40.2,1,'2016-09-22 11:36:00',19,0),(181,299,'2016-09-21',3,'TARJETA VISA',40.2,1,'2016-09-22 11:37:07',19,0),(182,299,'2016-09-21',1,'',40.2,1,'2016-09-22 11:39:49',19,0),(183,299,'1969-12-31',1,'',0,1,'2016-09-22 11:39:52',19,0),(184,337,'2016-09-21',1,'',40.2,1,'2016-09-22 11:42:04',19,0),(185,346,'2016-09-21',3,'',40.2,1,'2016-09-22 11:43:20',19,0),(186,611,'2016-09-21',3,'TARJETA VISA',40.2,1,'2016-09-22 11:45:00',19,0),(187,611,'1969-12-31',1,'',0,1,'2016-09-22 11:45:09',19,0),(188,623,'2016-09-28',1,'',40.2,1,'2016-09-28 11:17:07',19,0),(189,416,'2016-09-24',1,'',39.5,1,'2016-09-28 11:17:43',19,0),(190,993,'2016-09-24',1,'',39.5,1,'2016-09-28 11:18:41',19,0),(191,624,'2016-09-21',1,'',40.2,1,'2016-09-28 11:19:00',19,0),(192,646,'2016-09-21',1,'',40.2,1,'2016-09-28 11:19:57',19,0),(193,1034,'2016-09-21',1,'',40.2,1,'2016-09-28 11:21:19',19,0),(194,1034,'1969-12-31',1,'',0,1,'2016-09-28 11:21:21',19,0),(195,1010,'2016-09-28',1,'',31.5,1,'2016-09-29 12:52:40',19,0),(196,678,'2016-09-25',1,'',7.1,1,'2016-09-29 12:54:13',19,0),(197,1087,'2016-09-30',2,'125107',500,1,'2016-09-30 12:56:38',19,0),(198,1087,'2016-09-30',2,'125110',207.3,1,'2016-09-30 12:57:28',19,0),(199,1087,'1969-12-31',1,'',0,1,'2016-09-30 12:57:30',19,0),(200,1092,'2016-10-08',1,'',6,1,'2016-10-13 10:38:13',19,0),(201,1092,'2016-10-08',1,'',6,1,'2016-10-13 10:38:23',19,0),(202,1101,'2016-10-08',1,'',6,1,'2016-10-13 10:38:55',19,0),(203,1101,'1969-12-31',1,'',0,1,'2016-10-13 10:38:58',19,0),(204,1117,'2016-10-03',1,'',9,1,'2016-10-13 11:07:24',19,0),(205,1117,'1969-12-31',1,'',0,1,'2016-10-13 11:07:26',19,0),(206,1143,'2016-10-18',1,'',215.15,1,'2016-10-19 11:24:15',19,0),(207,770,'2016-10-16',1,'',19,1,'2016-10-19 12:00:40',19,0),(208,774,'2016-10-16',1,'',19,1,'2016-10-19 12:03:31',19,0),(209,795,'2016-10-16',1,'',19,1,'2016-10-19 12:07:32',19,0),(210,795,'1969-12-31',1,'',0,1,'2016-10-19 12:07:38',19,0),(211,795,'1969-12-31',1,'',0,1,'2016-10-19 12:07:40',19,0),(212,967,'2016-10-16',1,'',19,1,'2016-10-19 12:08:26',19,0),(213,967,'1969-12-31',1,'',19,1,'2016-10-19 12:08:40',19,0),(214,967,'2016-10-16',1,'',19,1,'2016-10-19 12:08:54',19,0),(215,1116,'2016-10-17',1,'',10,1,'2016-10-19 12:10:12',19,0),(216,1116,'1969-12-31',1,'',0,1,'2016-10-19 12:10:19',19,0),(217,1116,'1969-12-31',1,'',0,1,'2016-10-19 12:10:21',19,0),(218,1126,'2016-10-17',1,'',21.2,1,'2016-10-19 12:11:39',19,0),(219,1185,'2016-10-24',1,'',267,1,'2016-10-24 11:00:13',19,0),(220,1156,'2016-10-16',1,'',19,1,'2016-10-24 11:19:27',19,0),(221,1084,'2016-09-25',1,'',9,1,'2016-10-25 11:46:41',19,0),(222,291,'2016-10-14',1,'',38,1,'2016-10-25 11:49:42',19,0),(223,1155,'2016-10-14',1,'',38,1,'2016-10-25 11:50:32',19,0),(224,1193,'2016-10-21',1,'',42,1,'2016-10-26 10:54:14',19,0),(225,1243,'2016-11-04',1,'',26.5,1,'2016-11-04 18:58:00',17,0),(226,1241,'2016-11-04',1,'',23,1,'2016-11-04 18:58:58',17,0);
/*!40000 ALTER TABLE `pago` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parametros`
--

DROP TABLE IF EXISTS `parametros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parametros` (
  `Parametro_Id` int(11) NOT NULL AUTO_INCREMENT,
  `sParDescripcion` varchar(100) NOT NULL,
  `sParValor` text NOT NULL,
  PRIMARY KEY (`Parametro_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parametros`
--

LOCK TABLES `parametros` WRITE;
/*!40000 ALTER TABLE `parametros` DISABLE KEYS */;
INSERT INTO `parametros` VALUES (1,'Tiempo de conexión Activa|(Minutos)','60'),(2,'Valor del IGV|Impuesto General a las Ventas)','18');
/*!40000 ALTER TABLE `parametros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido`
--

DROP TABLE IF EXISTS `pedido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido` (
  `Pedido_Id` int(11) NOT NULL AUTO_INCREMENT,
  `OrigenDestino_Id` int(11) NOT NULL,
  `dPedFecha` date NOT NULL,
  `nPedTipopago` int(11) NOT NULL,
  `Documento_Id` int(11) DEFAULT NULL,
  `dPedImporte` decimal(10,2) NOT NULL,
  `dPedIGV` decimal(10,2) NOT NULL,
  `dPedTotal` decimal(10,2) NOT NULL,
  `dPedSaldo` decimal(10,2) NOT NULL,
  `sPedObservaciones` varchar(200) NOT NULL,
  `nPedEstado` tinyint(4) DEFAULT NULL,
  `dPedEliminado` tinyint(4) NOT NULL,
  `dPedFecha_Act` datetime NOT NULL,
  `Usuario_Id` int(11) NOT NULL,
  `dPedImporteAd` decimal(10,2) DEFAULT NULL,
  `nroOrdenTrabajo` varchar(10) DEFAULT NULL,
  `Moneda_Id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`Pedido_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=773 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido`
--

LOCK TABLES `pedido` WRITE;
/*!40000 ALTER TABLE `pedido` DISABLE KEYS */;
INSERT INTO `pedido` VALUES (1,2,'2016-03-06',1,7,10.00,0.00,10.00,0.00,'',2,0,'2016-03-06 22:11:13',17,NULL,NULL,0),(2,3,'2016-03-31',1,7,10.00,0.00,10.00,0.00,'',2,0,'2016-03-31 19:09:51',17,NULL,NULL,0),(3,2,'2016-03-31',1,7,14.00,0.00,14.00,0.00,'Juan Carlos Chavez',2,0,'2016-03-31 19:08:58',17,NULL,NULL,0),(4,2,'2016-04-25',1,7,37.20,0.00,37.20,0.00,'',2,0,'2016-04-30 12:40:58',19,NULL,NULL,0),(5,5,'2016-04-29',1,7,4.00,0.00,4.00,0.00,'',2,0,'2016-04-30 12:56:34',19,NULL,NULL,0),(6,6,'2016-04-29',1,7,4.00,0.00,4.00,0.00,'',2,0,'2016-04-30 12:57:32',19,NULL,NULL,0),(7,2,'2016-04-29',1,7,50.00,0.00,50.00,0.00,'',2,0,'2016-04-30 12:59:36',19,NULL,NULL,0),(8,2,'2016-04-26',1,7,2.00,0.00,2.00,0.00,'',2,0,'2016-05-07 10:13:25',19,NULL,NULL,0),(9,7,'2016-04-29',1,7,2.00,0.00,2.00,0.00,'',2,0,'2016-05-07 10:15:50',19,NULL,NULL,0),(10,9,'2016-04-30',1,7,10.00,0.00,10.00,0.00,'',2,0,'2016-05-07 10:22:59',19,NULL,NULL,0),(11,2,'2016-04-29',1,7,6.70,0.00,6.70,0.00,'',2,0,'2016-05-07 10:28:23',19,NULL,NULL,0),(12,10,'2016-04-30',1,7,9.00,0.00,9.00,0.00,'',2,0,'2016-05-07 10:29:27',19,NULL,NULL,0),(13,17,'2016-04-30',1,7,2.40,0.00,2.40,0.00,'',2,0,'2016-05-07 10:31:51',19,NULL,NULL,0),(14,17,'2016-04-30',2,7,5.00,0.00,5.00,0.00,'',2,0,'2016-05-07 10:34:14',19,NULL,NULL,0),(15,12,'2016-04-30',1,7,45.00,0.00,45.00,0.00,'',2,0,'2016-05-07 10:39:45',19,NULL,NULL,0),(16,13,'2016-04-30',1,7,10.50,0.00,10.50,0.00,'',1,1,'2016-05-07 10:41:04',19,NULL,NULL,0),(17,14,'2016-05-01',2,7,8.70,0.00,8.70,0.00,'',1,1,'2016-05-07 10:46:56',19,NULL,NULL,0),(18,15,'2016-05-01',1,7,41.40,0.00,41.40,0.00,'',1,1,'2016-05-07 11:01:54',19,NULL,NULL,0),(19,7,'2016-05-01',1,7,8.00,0.00,8.00,0.00,'',1,1,'2016-05-07 11:03:14',19,NULL,NULL,0),(20,16,'2016-05-01',1,7,3.00,0.00,3.00,0.00,'',2,0,'2016-05-07 11:04:21',19,NULL,NULL,0),(21,10,'2016-05-01',1,7,2.00,0.00,2.00,0.00,'',1,1,'2016-05-07 11:05:43',19,NULL,NULL,0),(22,6,'2016-05-01',1,7,10.00,0.00,10.00,0.00,'',2,0,'2016-05-07 11:12:12',19,NULL,NULL,0),(23,5,'2016-05-01',1,7,8.00,0.00,8.00,0.00,'',2,0,'2016-05-07 11:18:47',19,NULL,NULL,0),(24,2,'2016-05-03',1,7,8.00,0.00,8.00,0.00,'',2,0,'2016-05-07 11:20:32',19,NULL,NULL,0),(25,12,'2016-05-03',2,7,30.00,0.00,30.00,0.00,'',2,0,'2016-05-07 11:38:58',19,NULL,NULL,0),(26,2,'2016-05-03',1,7,22.00,0.00,22.00,0.00,'',2,0,'2016-05-07 11:42:36',19,NULL,NULL,0),(27,13,'2016-05-04',1,7,16.00,0.00,16.00,0.00,'',2,0,'2016-05-07 11:49:36',19,NULL,NULL,0),(28,14,'2016-05-04',2,7,1.50,0.00,1.50,0.00,'',2,0,'2016-05-07 11:51:30',19,NULL,NULL,0),(29,7,'2016-05-04',2,7,4.00,0.00,4.00,0.00,'',2,0,'2016-05-07 11:54:31',19,NULL,NULL,0),(30,2,'2016-05-05',1,7,9.10,0.00,9.10,0.00,'',2,0,'2016-05-07 12:08:59',19,NULL,NULL,0),(31,19,'2016-05-05',1,7,4.00,0.00,4.00,0.00,'',2,0,'2016-05-07 12:10:13',19,NULL,NULL,0),(32,23,'2016-05-05',2,7,2.00,0.00,2.00,0.00,'',2,0,'2016-05-07 12:11:15',19,NULL,NULL,0),(33,2,'2016-05-05',1,7,35.00,0.00,35.00,0.00,'',2,0,'2016-05-07 12:13:17',19,NULL,NULL,0),(34,24,'2016-05-06',1,7,7.80,0.00,7.80,0.00,'',2,0,'2016-05-07 12:17:07',19,NULL,NULL,0),(35,21,'2016-05-06',1,7,277.50,0.00,277.50,0.00,'',2,0,'2016-05-07 12:27:33',19,NULL,NULL,0),(36,22,'2016-05-06',1,7,235.70,0.00,235.70,0.00,'',2,0,'2016-05-07 12:34:15',19,NULL,NULL,0),(37,27,'2016-05-04',2,7,55.00,0.00,55.00,0.00,'',2,0,'2016-05-07 13:26:13',19,NULL,NULL,0),(38,9,'2016-05-01',1,7,17.50,0.00,17.50,0.00,'',2,0,'2016-05-09 10:16:01',19,NULL,NULL,0),(39,27,'2016-05-04',2,7,55.00,0.00,55.00,0.00,'',2,0,'2016-05-09 10:19:21',19,NULL,NULL,0),(40,7,'2016-05-07',1,7,4.00,0.00,4.00,0.00,'',2,0,'2016-05-09 10:25:08',19,NULL,NULL,0),(41,28,'2016-05-07',1,7,50.00,0.00,50.00,0.00,'',2,0,'2016-05-09 10:31:21',19,NULL,NULL,0),(42,9,'2016-05-07',1,7,27.00,0.00,27.00,0.00,'',2,0,'2016-05-09 10:36:53',19,NULL,NULL,0),(43,7,'2016-05-07',1,7,6.00,0.00,6.00,0.00,'',2,0,'2016-05-09 10:38:28',19,NULL,NULL,0),(44,20,'2016-05-07',1,7,2.40,0.00,2.40,0.00,'',2,0,'2016-05-09 10:40:05',19,NULL,NULL,0),(45,2,'2016-05-07',1,7,5.00,0.00,5.00,0.00,'',2,0,'2016-05-09 10:40:46',19,NULL,NULL,0),(46,15,'2016-05-07',1,7,5.00,0.00,5.00,0.00,'',2,0,'2016-05-09 10:41:39',19,NULL,NULL,0),(47,15,'2016-05-07',1,7,10.00,0.00,10.00,0.00,'',2,0,'2016-05-09 10:42:53',19,NULL,NULL,0),(48,7,'2016-05-07',1,7,9.00,0.00,9.00,0.00,'',2,0,'2016-05-09 10:43:46',19,NULL,NULL,0),(49,5,'2016-05-07',1,7,8.00,0.00,8.00,0.00,'',2,0,'2016-05-09 10:44:32',19,NULL,NULL,0),(50,9,'2016-05-07',1,7,4.00,0.00,4.00,0.00,'',2,0,'2016-05-09 10:45:16',19,NULL,NULL,0),(51,29,'2016-05-07',1,7,55.40,0.00,55.40,0.00,'',2,0,'2016-05-09 10:50:15',19,NULL,NULL,0),(52,9,'2016-05-08',1,7,37.40,0.00,37.40,0.00,'',2,0,'2016-05-09 10:53:46',19,NULL,NULL,0),(53,7,'2016-05-08',1,7,10.00,0.00,10.00,0.00,'',2,0,'2016-05-09 10:55:27',19,NULL,NULL,0),(54,20,'2016-05-08',1,7,2.00,0.00,2.00,0.00,'',2,0,'2016-05-09 10:56:23',19,NULL,NULL,0),(55,29,'2016-05-08',1,7,65.90,0.00,65.90,0.00,'',2,0,'2016-05-09 11:01:46',19,NULL,NULL,0),(56,30,'2016-05-08',1,7,7.10,0.00,7.10,0.00,'',2,0,'2016-05-09 11:03:21',19,NULL,NULL,0),(57,31,'2016-05-08',1,7,18.50,0.00,18.50,0.00,'',2,0,'2016-05-09 11:04:23',19,NULL,NULL,0),(58,9,'2016-05-08',1,7,12.00,0.00,12.00,0.00,'',2,0,'2016-05-09 11:06:06',19,NULL,NULL,0),(59,2,'2016-05-08',1,7,6.30,0.00,6.30,0.00,'',2,0,'2016-05-09 11:07:05',19,NULL,NULL,0),(60,2,'2016-05-08',1,7,9.00,0.00,9.00,0.00,'',2,0,'2016-05-09 11:08:03',19,NULL,NULL,0),(61,32,'2016-05-08',2,7,3.50,0.00,3.50,0.00,'',2,0,'2016-05-09 11:45:44',19,NULL,NULL,0),(62,15,'2016-05-08',1,7,28.40,0.00,28.40,0.00,'',2,0,'2016-05-09 11:52:57',19,NULL,NULL,0),(63,15,'2016-05-08',1,7,3.50,0.00,3.50,0.00,'',2,0,'2016-05-09 12:02:12',19,NULL,NULL,0),(64,20,'2016-05-09',1,7,2.00,0.00,2.00,0.00,'',2,0,'2016-05-10 10:55:19',19,NULL,NULL,0),(65,33,'2016-05-09',2,7,38.00,0.00,38.00,0.00,'',2,0,'2016-05-10 10:56:43',19,NULL,NULL,0),(66,2,'2016-05-09',1,7,4.00,0.00,4.00,0.00,'',2,0,'2016-05-10 10:57:58',19,NULL,NULL,0),(67,34,'2016-05-09',1,7,5.50,0.00,5.50,0.00,'',2,0,'2016-05-10 10:59:10',19,NULL,NULL,0),(68,2,'2016-05-10',1,7,2.50,0.00,2.50,0.00,'',2,0,'2016-05-11 09:23:00',19,NULL,NULL,0),(69,12,'2016-05-10',1,7,177.00,0.00,177.00,0.00,'',2,0,'2016-05-11 09:33:09',19,NULL,NULL,0),(70,2,'2016-05-11',1,7,14.60,0.00,14.60,0.00,'',2,0,'2016-05-13 09:49:26',19,NULL,NULL,0),(71,7,'2016-05-11',2,7,2.00,0.00,2.00,0.00,'',2,0,'2016-05-13 09:50:53',19,NULL,NULL,0),(72,35,'2016-05-12',1,7,24.00,0.00,24.00,0.00,'',2,0,'2016-05-13 09:53:33',19,NULL,NULL,0),(73,5,'2016-05-12',2,7,2.40,0.00,2.40,0.00,'',2,0,'2016-05-13 09:55:09',19,NULL,NULL,0),(74,2,'2016-05-12',1,7,21.60,0.00,21.60,0.00,'',2,0,'2016-05-13 09:57:23',19,NULL,NULL,0),(75,36,'2016-03-21',2,7,9.00,0.00,9.00,0.00,'',2,0,'2016-05-16 18:48:32',17,NULL,NULL,0),(76,9,'2016-05-14',1,7,45.00,0.00,45.00,0.00,'',2,0,'2016-05-18 11:45:03',19,NULL,NULL,0),(77,9,'2016-05-15',1,7,8.50,0.00,8.50,0.00,'',2,0,'2016-05-18 11:46:11',19,NULL,NULL,0),(78,15,'2016-05-15',1,7,11.00,0.00,11.00,0.00,'',2,0,'2016-05-18 11:47:32',19,NULL,NULL,0),(79,38,'2016-05-15',1,7,6.50,0.00,6.50,0.00,'',2,0,'2016-05-18 11:56:25',19,NULL,NULL,0),(80,39,'2016-05-15',1,7,7.00,0.00,7.00,0.00,'',2,0,'2016-05-18 11:59:09',19,NULL,NULL,0),(81,6,'2016-05-15',1,7,10.00,0.00,10.00,0.00,'',2,0,'2016-05-18 12:00:27',19,NULL,NULL,0),(82,20,'2016-05-15',2,7,4.10,0.00,4.10,0.00,'',2,0,'2016-05-18 12:01:47',19,NULL,NULL,0),(83,31,'2016-05-15',1,7,29.00,0.00,29.00,0.00,'',2,0,'2016-05-18 12:05:40',19,NULL,NULL,0),(84,9,'2016-05-15',1,7,5.50,0.00,5.50,0.00,'',2,0,'2016-05-18 12:08:13',19,NULL,NULL,0),(85,2,'2016-05-15',1,7,16.00,0.00,16.00,0.00,'',2,0,'2016-05-18 12:11:18',19,NULL,NULL,0),(86,2,'2016-05-15',1,7,6.00,0.00,6.00,0.00,'',2,0,'2016-05-18 12:12:38',19,NULL,NULL,0),(87,17,'2016-05-16',2,7,3.50,0.00,3.50,0.00,'',2,0,'2016-05-18 12:13:55',19,NULL,NULL,0),(88,40,'2016-05-16',1,7,57.00,0.00,57.00,0.00,'',2,0,'2016-05-18 12:16:16',19,NULL,NULL,0),(89,2,'2016-05-16',1,7,42.00,0.00,42.00,0.00,'',2,0,'2016-05-18 12:18:08',19,NULL,NULL,0),(90,2,'2016-05-17',1,7,10.00,0.00,10.00,0.00,'',2,0,'2016-05-18 12:19:20',19,NULL,NULL,0),(91,35,'2016-05-14',1,7,19.00,0.00,19.00,0.00,'',2,0,'2016-05-24 11:20:43',19,NULL,NULL,0),(92,13,'2016-05-18',1,7,82.00,0.00,82.00,0.00,'',2,0,'2016-05-24 11:22:25',19,NULL,NULL,0),(93,2,'2016-05-18',1,7,5.20,0.00,5.20,0.00,'',2,0,'2016-05-24 11:24:15',19,NULL,NULL,0),(94,5,'2016-05-19',1,7,28.00,0.00,28.00,0.00,'',2,0,'2016-05-24 11:25:22',19,NULL,NULL,0),(95,2,'2016-05-21',1,7,5.70,0.00,5.70,0.00,'',2,0,'2016-05-24 11:26:39',19,NULL,NULL,0),(96,2,'2016-05-21',1,7,21.00,0.00,21.00,0.00,'',2,0,'2016-05-24 11:28:57',19,NULL,NULL,0),(97,9,'2016-05-22',1,7,20.50,0.00,20.50,0.00,'',2,0,'2016-05-24 11:31:05',19,NULL,NULL,0),(98,15,'2016-05-22',1,7,17.40,0.00,17.40,0.00,'',2,0,'2016-05-24 11:32:47',19,NULL,NULL,0),(99,7,'2016-05-22',1,7,8.00,0.00,8.00,0.00,'',2,0,'2016-05-24 11:34:05',19,NULL,NULL,0),(100,42,'2016-05-22',1,7,3.50,0.00,3.50,0.00,'',2,0,'2016-05-24 11:35:35',19,NULL,NULL,0),(101,17,'2016-05-22',2,7,2.00,0.00,2.00,0.00,'',2,0,'2016-05-24 11:36:34',19,NULL,NULL,0),(102,43,'2016-05-22',2,7,4.00,0.00,4.00,0.00,'',2,0,'2016-05-24 11:38:34',19,NULL,NULL,0),(103,10,'2016-05-22',1,7,1.50,0.00,1.50,0.00,'',2,0,'2016-05-24 11:39:09',19,NULL,NULL,0),(104,20,'2016-05-22',1,7,2.00,0.00,2.00,0.00,'',2,0,'2016-05-24 11:40:04',19,NULL,NULL,0),(105,37,'2016-02-05',2,7,15.00,0.00,15.00,0.00,'',2,0,'2016-05-24 11:52:06',19,NULL,NULL,0),(106,2,'2016-05-24',1,7,31.50,0.00,31.50,0.00,'',2,0,'2016-05-25 11:17:24',19,NULL,NULL,0),(107,41,'2016-05-17',2,7,16.00,0.00,16.00,0.00,'vale 5514 canjear con factura',2,0,'2016-05-31 11:41:31',19,NULL,NULL,0),(108,40,'2016-05-17',2,7,12.00,0.00,12.00,0.00,'vale 5515 por factura',2,0,'2016-05-31 11:42:46',19,NULL,NULL,0),(109,39,'2016-05-17',2,7,111.30,0.00,111.30,0.00,'',2,0,'2016-05-31 11:51:29',19,NULL,NULL,0),(110,2,'2016-05-27',1,7,7.00,0.00,7.00,0.00,'',2,0,'2016-05-31 11:56:39',19,NULL,NULL,0),(111,2,'2016-05-27',1,7,1.50,0.00,1.50,0.00,'',2,0,'2016-05-31 11:57:40',19,NULL,NULL,0),(112,57,'2016-06-28',1,7,3.00,0.00,3.00,0.00,'',2,0,'2016-06-01 12:00:41',19,NULL,NULL,0),(113,15,'2016-05-28',1,7,10.00,0.00,10.00,0.00,'',2,0,'2016-06-01 12:01:49',19,NULL,NULL,0),(114,9,'2016-05-28',1,7,11.00,0.00,11.00,0.00,'',2,0,'2016-06-01 12:03:04',19,NULL,NULL,0),(115,58,'2016-05-28',1,7,36.60,0.00,36.60,0.00,'',2,0,'2016-06-01 12:06:22',19,NULL,NULL,0),(116,16,'2016-05-28',2,7,5.00,0.00,5.00,0.00,'',2,0,'2016-06-01 12:07:38',19,NULL,NULL,0),(117,20,'2016-05-28',1,7,11.20,0.00,11.20,0.00,'',2,0,'2016-06-01 12:09:08',19,NULL,NULL,0),(118,24,'2016-05-28',1,7,10.50,0.00,10.50,0.00,'',2,0,'2016-06-01 12:10:18',19,NULL,NULL,0),(119,7,'2016-05-28',1,7,5.50,0.00,5.50,0.00,'',2,0,'2016-06-01 12:11:30',19,NULL,NULL,0),(120,2,'2016-05-28',1,7,7.40,0.00,7.40,0.00,'',2,0,'2016-06-01 12:12:58',19,NULL,NULL,0),(121,37,'2016-05-28',1,7,22.00,0.00,22.00,0.00,'',2,0,'2016-06-01 12:14:42',19,NULL,NULL,0),(122,59,'2016-05-28',2,7,2.00,0.00,2.00,0.00,'',2,0,'2016-06-01 12:17:00',19,NULL,NULL,0),(123,17,'2016-05-28',1,7,0.80,0.00,0.80,0.00,'',2,0,'2016-06-01 12:17:40',19,NULL,NULL,0),(124,7,'2016-05-28',1,7,2.00,0.00,2.00,0.00,'',2,0,'2016-06-01 12:18:17',19,NULL,NULL,0),(125,15,'2016-05-28',1,7,19.50,0.00,19.50,0.00,'',2,0,'2016-06-01 12:19:30',19,NULL,NULL,0),(126,12,'2016-05-28',1,7,33.00,0.00,33.00,0.00,'',2,0,'2016-06-01 12:20:45',19,NULL,NULL,0),(127,2,'2016-05-28',1,7,8.50,0.00,8.50,0.00,'',2,0,'2016-06-01 12:22:36',19,NULL,NULL,0),(128,15,'2016-05-29',1,7,20.20,0.00,20.20,0.00,'',2,0,'2016-06-01 12:24:58',19,NULL,NULL,0),(129,57,'2016-05-29',1,7,19.90,0.00,19.90,0.00,'',2,0,'2016-06-01 12:26:43',19,NULL,NULL,0),(130,43,'2016-05-27',1,7,10.20,0.00,10.20,0.00,'',2,0,'2016-06-06 09:26:41',19,NULL,NULL,0),(131,2,'2016-05-27',1,7,12.40,0.00,12.40,0.00,'',2,0,'2016-06-06 09:28:42',19,NULL,NULL,0),(132,7,'2016-05-29',1,7,6.00,0.00,6.00,0.00,'',2,0,'2016-06-06 09:29:57',19,NULL,NULL,0),(133,16,'2016-05-29',2,7,10.50,0.00,10.50,0.00,'',2,0,'2016-06-06 09:31:52',19,NULL,NULL,0),(134,17,'2016-05-27',2,7,2.00,0.00,2.00,0.00,'',2,0,'2016-06-06 09:32:40',19,NULL,NULL,0),(135,32,'2016-05-29',1,7,2.00,0.00,2.00,0.00,'',2,0,'2016-06-06 09:33:21',19,NULL,NULL,0),(136,39,'2016-05-29',1,7,6.50,0.00,6.50,0.00,'',2,0,'2016-06-06 09:34:21',19,NULL,NULL,0),(137,9,'2016-05-29',1,7,14.50,0.00,14.50,0.00,'',2,0,'2016-06-06 09:36:09',19,NULL,NULL,0),(138,2,'2016-05-30',1,7,3.00,0.00,3.00,0.00,'',2,0,'2016-06-06 09:42:45',19,NULL,NULL,0),(139,2,'2016-05-31',1,7,20.00,0.00,20.00,0.00,'',2,0,'2016-06-06 09:50:12',19,NULL,NULL,0),(140,2,'2016-06-02',1,7,15.00,0.00,15.00,0.00,'',2,0,'2016-06-06 09:50:48',19,NULL,NULL,0),(141,2,'2016-06-03',1,7,4.50,0.00,4.50,0.00,'',2,0,'2016-06-06 09:52:04',19,NULL,NULL,0),(142,60,'2016-06-04',1,7,18.00,0.00,18.00,0.00,'',2,0,'2016-06-06 09:53:34',19,NULL,NULL,0),(143,9,'2016-06-04',1,7,17.00,0.00,17.00,0.00,'',2,0,'2016-06-06 09:54:26',19,NULL,NULL,0),(144,7,'2016-06-04',1,7,2.00,0.00,2.00,0.00,'',2,0,'2016-06-06 09:55:08',19,NULL,NULL,0),(145,10,'2016-06-04',1,7,1.50,0.00,1.50,0.00,'',2,0,'2016-06-06 09:55:51',19,NULL,NULL,0),(146,61,'2016-06-04',1,7,3.90,0.00,3.90,0.00,'',2,0,'2016-06-06 10:00:12',19,NULL,NULL,0),(147,19,'2016-06-04',1,7,5.90,0.00,5.90,0.00,'',2,0,'2016-06-06 10:01:46',19,NULL,NULL,0),(148,19,'2016-06-04',1,7,4.50,0.00,4.50,0.00,'',2,0,'2016-06-06 10:02:39',19,NULL,NULL,0),(149,62,'2016-06-05',1,7,8.10,0.00,8.10,0.00,'',2,0,'2016-06-06 10:04:14',19,NULL,NULL,0),(150,15,'2016-06-05',1,7,19.30,0.00,19.30,0.00,'',2,0,'2016-06-06 10:07:11',19,NULL,NULL,0),(151,61,'2016-06-05',1,7,8.20,0.00,8.20,0.00,'',2,0,'2016-06-06 10:08:14',19,NULL,NULL,0),(152,7,'2016-06-05',1,7,6.00,0.00,6.00,0.00,'',2,0,'2016-06-06 10:09:13',19,NULL,NULL,0),(153,6,'2016-06-05',1,7,12.80,0.00,12.80,0.00,'',2,0,'2016-06-06 10:10:59',19,NULL,NULL,0),(154,17,'2016-06-05',1,7,2.00,0.00,2.00,0.00,'',2,0,'2016-06-06 10:11:45',19,NULL,NULL,0),(155,2,'2016-06-05',1,7,3.00,0.00,3.00,0.00,'',2,0,'2016-06-06 10:12:23',19,NULL,NULL,0),(156,2,'2016-06-05',1,7,4.00,0.00,4.00,0.00,'',2,0,'2016-06-06 10:13:42',19,NULL,NULL,0),(157,10,'2016-06-05',1,7,4.00,0.00,4.00,0.00,'',2,0,'2016-06-06 10:14:27',19,NULL,NULL,0),(158,57,'2016-06-05',1,7,3.00,0.00,3.00,0.00,'',2,0,'2016-06-06 10:14:57',19,NULL,NULL,0),(159,9,'2016-06-05',1,7,19.50,0.00,19.50,0.00,'',2,0,'2016-06-06 10:16:55',19,NULL,NULL,0),(160,20,'2016-06-05',1,7,4.00,0.00,4.00,0.00,'',2,0,'2016-06-06 10:17:41',19,NULL,NULL,0),(161,63,'2016-06-05',1,7,3.50,0.00,3.50,0.00,'',2,0,'2016-06-06 10:18:37',19,NULL,NULL,0),(162,9,'2016-06-05',1,7,3.00,0.00,3.00,0.00,'',2,0,'2016-06-06 10:19:30',19,NULL,NULL,0),(163,15,'2016-06-05',1,7,4.00,0.00,4.00,0.00,'',2,0,'2016-06-06 10:20:11',19,NULL,NULL,0),(164,43,'2016-06-05',2,7,12.10,0.00,12.10,0.00,'',2,0,'2016-06-09 09:57:26',19,NULL,NULL,0),(165,64,'2016-06-05',2,7,5.50,0.00,5.50,0.00,'',2,0,'2016-06-09 10:01:27',19,NULL,NULL,0),(166,2,'2016-06-06',1,7,9.00,0.00,9.00,0.00,'',2,0,'2016-06-09 10:05:23',19,NULL,NULL,0),(167,2,'2016-06-07',1,7,6.40,0.00,6.40,0.00,'',2,0,'2016-06-09 10:06:51',19,NULL,NULL,0),(168,30,'2016-06-08',1,7,15.00,0.00,15.00,0.00,'',2,0,'2016-06-09 10:08:36',19,NULL,NULL,0),(169,2,'2016-06-08',1,7,4.00,0.00,4.00,0.00,'',2,0,'2016-06-09 10:09:18',19,NULL,NULL,0),(170,42,'2016-06-08',2,7,0.60,0.00,0.60,0.00,'',2,0,'2016-06-09 10:10:10',19,NULL,NULL,0),(171,2,'2016-06-08',1,7,15.00,0.00,15.00,0.00,'',2,0,'2016-06-09 10:11:05',19,NULL,NULL,0),(172,65,'2016-06-10',2,7,21.50,0.00,21.50,0.00,'4-12-15',2,0,'2016-06-10 12:37:25',19,NULL,NULL,0),(173,65,'2016-06-10',2,7,27.00,0.00,27.00,0.00,'4-1-16',2,0,'2016-06-10 12:38:57',19,NULL,NULL,0),(174,65,'2016-06-10',2,7,11.50,0.00,11.50,0.00,'',2,0,'2016-06-10 12:40:26',19,NULL,NULL,0),(175,2,'2016-06-09',1,7,4.00,0.00,4.00,0.00,'',2,0,'2016-06-13 10:20:16',19,NULL,NULL,0),(176,15,'2016-06-09',1,7,2.70,0.00,2.70,0.00,'',2,0,'2016-06-13 10:21:27',19,NULL,NULL,0),(177,16,'2016-06-10',1,7,14.30,0.00,14.30,0.00,'',2,0,'2016-06-13 10:23:54',19,NULL,NULL,0),(178,2,'2016-06-10',1,7,4.00,0.00,4.00,0.00,'',2,0,'2016-06-13 10:25:13',19,NULL,NULL,0),(179,2,'2016-06-10',1,7,14.50,0.00,14.50,0.00,'',2,0,'2016-06-13 10:27:27',19,NULL,NULL,0),(180,7,'2016-06-11',1,7,11.50,0.00,11.50,0.00,'',2,0,'2016-06-13 10:29:21',19,NULL,NULL,0),(181,15,'2016-06-11',1,7,18.00,0.00,18.00,0.00,'',2,0,'2016-06-13 10:30:42',19,NULL,NULL,0),(182,66,'2016-06-11',2,7,11.00,0.00,11.00,0.00,'',2,0,'2016-06-13 10:38:41',19,NULL,NULL,0),(183,29,'2016-06-11',1,7,12.00,0.00,12.00,0.00,'',2,0,'2016-06-13 10:40:43',19,NULL,NULL,0),(184,62,'2016-06-11',1,7,25.50,0.00,25.50,0.00,'',2,0,'2016-06-13 10:43:08',19,NULL,NULL,0),(185,6,'2016-06-11',2,7,2.00,0.00,2.00,0.00,'',2,0,'2016-06-13 10:44:01',19,NULL,NULL,0),(186,2,'2016-06-11',1,7,18.00,0.00,18.00,0.00,'',2,0,'2016-06-13 10:45:44',19,NULL,NULL,0),(187,9,'2016-06-11',1,7,26.50,0.00,26.50,0.00,'',2,0,'2016-06-13 10:47:18',19,NULL,NULL,0),(188,2,'2016-06-11',1,7,49.00,0.00,49.00,0.00,'',2,0,'2016-06-13 10:50:30',19,NULL,NULL,0),(189,9,'2016-06-11',1,7,8.00,0.00,8.00,0.00,'',2,0,'2016-06-13 10:51:45',19,NULL,NULL,0),(190,67,'2016-06-11',1,7,4.00,0.00,4.00,0.00,'',2,0,'2016-06-13 10:53:06',19,NULL,NULL,0),(191,2,'2016-06-12',1,7,47.70,0.00,47.70,0.00,'',2,0,'2016-06-13 10:56:42',19,NULL,NULL,0),(192,17,'2016-06-12',1,7,2.00,0.00,2.00,0.00,'',2,0,'2016-06-13 10:57:40',19,NULL,NULL,0),(193,43,'2016-06-12',1,7,9.00,0.00,9.00,0.00,'',2,0,'2016-06-13 10:58:22',19,NULL,NULL,0),(194,24,'2016-06-12',1,7,5.00,0.00,5.00,0.00,'',2,0,'2016-06-13 10:59:01',19,NULL,NULL,0),(195,9,'2016-06-12',1,7,12.00,0.00,12.00,0.00,'',2,0,'2016-06-13 11:00:13',19,NULL,NULL,0),(196,15,'2016-06-12',1,7,23.50,0.00,23.50,0.00,'',2,0,'2016-06-13 11:02:11',19,NULL,NULL,0),(197,6,'2016-06-12',1,7,17.20,0.00,17.20,0.00,'',2,0,'2016-06-13 11:04:28',19,NULL,NULL,0),(198,68,'2016-06-12',2,7,8.00,0.00,8.00,0.00,'',2,0,'2016-06-13 11:05:54',19,NULL,NULL,0),(199,7,'2016-06-12',1,7,4.00,0.00,4.00,0.00,'',2,0,'2016-06-13 11:06:45',19,NULL,NULL,0),(200,17,'2016-01-30',2,7,5.00,0.00,5.00,0.00,'',2,0,'2016-06-13 11:27:00',19,NULL,NULL,0),(201,17,'2016-04-21',2,7,5.00,0.00,5.00,0.00,'',2,0,'2016-06-13 11:27:37',19,NULL,NULL,0),(202,24,'2014-02-01',2,7,1.50,0.00,1.50,0.00,'Hijo vale 1395',2,0,'2016-06-15 17:00:44',19,NULL,NULL,0),(203,52,'1969-12-31',2,7,5.00,0.00,5.00,0.00,'2346',2,0,'2016-06-15 17:35:17',19,NULL,NULL,0),(204,71,'1969-12-31',2,7,2.00,0.00,2.00,0.00,'2653',2,0,'2016-06-15 17:44:01',19,NULL,NULL,0),(205,70,'1969-12-31',2,7,5.00,0.00,5.00,0.00,'',2,0,'2016-06-15 18:04:17',19,NULL,NULL,0),(206,47,'1969-12-31',2,7,9.50,0.00,9.50,0.00,'2491',2,0,'2016-06-15 18:06:48',19,NULL,NULL,0),(207,51,'1969-12-31',2,7,4.00,0.00,4.00,0.00,'3272',2,0,'2016-06-15 18:09:00',19,NULL,NULL,0),(208,71,'1969-12-31',2,7,4.00,0.00,4.00,0.00,'',2,0,'2016-06-15 18:11:13',19,NULL,NULL,0),(209,51,'1969-12-31',2,7,5.50,0.00,5.50,0.00,'',2,0,'2016-06-15 18:14:40',19,NULL,NULL,0),(210,47,'1969-12-31',2,7,4.00,0.00,4.00,0.00,'',2,0,'2016-06-15 18:19:24',19,NULL,NULL,0),(211,51,'1969-12-31',2,7,9.00,0.00,9.00,0.00,'',2,0,'2016-06-15 18:23:03',19,NULL,NULL,0),(212,7,'1969-12-31',2,7,3.50,0.00,3.50,0.00,'3650',2,0,'2016-06-15 18:26:32',19,NULL,NULL,0),(213,71,'2015-04-05',2,7,4.00,0.00,4.00,0.00,'3657',2,0,'2016-06-15 18:30:35',19,NULL,NULL,0),(214,72,'2015-09-10',2,7,3.00,0.00,3.00,0.00,'',2,0,'2016-06-15 18:33:45',19,NULL,NULL,0),(215,47,'2015-04-12',1,7,12.20,0.00,12.20,0.00,'3683',2,0,'2016-06-15 18:39:46',19,NULL,NULL,0),(216,7,'2015-04-30',2,7,2.00,0.00,2.00,0.00,'',2,0,'2016-06-15 18:41:56',19,NULL,NULL,0),(217,47,'2015-04-30',2,7,3.00,0.00,3.00,0.00,'',2,0,'2016-06-15 18:43:09',19,NULL,NULL,0),(218,14,'2015-05-04',2,7,1.50,0.00,1.50,0.00,'',2,0,'2016-06-15 18:45:40',19,NULL,NULL,0),(219,7,'2015-05-09',2,7,4.70,0.00,4.70,0.00,'',2,0,'2016-06-15 18:47:49',19,NULL,NULL,0),(220,51,'2015-05-10',2,7,2.10,0.00,2.10,0.00,'',2,0,'2016-06-15 18:49:25',19,NULL,NULL,0),(221,47,'2015-05-10',2,7,8.50,0.00,8.50,0.00,'',2,0,'2016-06-15 18:50:58',19,NULL,NULL,0),(222,7,'2015-05-10',2,7,3.00,0.00,3.00,0.00,'',2,0,'2016-06-15 18:51:49',19,NULL,NULL,0),(223,73,'2015-05-15',2,7,4.40,0.00,4.40,0.00,'',2,0,'2016-06-15 18:54:26',19,NULL,NULL,0),(224,14,'1969-12-31',2,7,4.00,0.00,4.00,0.00,'',2,0,'2016-06-15 18:55:51',19,NULL,NULL,0),(225,47,'2015-05-28',2,7,2.00,0.00,2.00,0.00,'',2,0,'2016-06-15 18:58:04',19,NULL,NULL,0),(226,47,'2015-05-31',2,7,6.20,0.00,6.20,0.00,'',2,0,'2016-06-15 18:59:46',19,NULL,NULL,0),(227,51,'2015-07-11',2,7,36.00,0.00,36.00,0.00,'',2,0,'2016-06-15 19:04:21',19,NULL,NULL,0),(228,70,'2015-07-12',2,7,5.50,0.00,5.50,0.00,'',2,0,'2016-06-16 09:41:46',19,NULL,NULL,0),(229,61,'2015-07-19',2,7,3.00,0.00,3.00,0.00,'',2,0,'2016-06-16 09:44:27',19,NULL,NULL,0),(230,51,'2015-03-20',2,7,3.00,0.00,3.00,0.00,'',2,0,'2016-06-16 09:46:43',19,NULL,NULL,0),(231,57,'2015-07-19',2,7,4.50,0.00,4.50,0.00,'',2,0,'2016-06-16 09:48:17',19,NULL,NULL,0),(232,24,'2015-07-23',2,7,11.00,0.00,11.00,0.00,'',2,0,'2016-06-16 09:50:20',19,NULL,NULL,0),(233,10,'1969-12-31',2,7,5.00,0.00,5.00,0.00,'',2,0,'2016-06-16 09:54:12',19,NULL,NULL,0),(234,51,'2015-07-27',2,7,14.50,0.00,14.50,0.00,'',2,0,'2016-06-16 09:56:16',19,NULL,NULL,0),(235,10,'2015-07-28',2,7,51.70,0.00,51.70,0.00,'',2,0,'2016-06-16 10:01:35',19,NULL,NULL,0),(236,74,'2015-07-28',2,7,4.00,0.00,4.00,0.00,'',2,0,'2016-06-16 10:03:54',19,NULL,NULL,0),(237,16,'2015-07-28',2,7,14.50,0.00,14.50,0.00,'',2,0,'2016-06-16 10:09:07',19,NULL,NULL,0),(238,66,'2015-07-28',2,7,6.00,0.00,6.00,0.00,'',2,0,'2016-06-16 10:10:38',19,NULL,NULL,0),(239,10,'2015-07-31',2,7,1.50,0.00,1.50,0.00,'',2,0,'2016-06-16 10:11:58',19,NULL,NULL,0),(240,42,'2015-08-01',2,7,4.00,0.00,4.00,0.00,'',2,0,'2016-06-16 10:14:33',19,NULL,NULL,0),(241,51,'2015-08-04',2,7,1.50,0.00,1.50,0.00,'',2,0,'2016-06-16 10:17:40',19,NULL,NULL,0),(242,73,'2015-08-09',2,7,3.50,0.00,3.50,0.00,'',2,0,'2016-06-16 10:22:47',19,NULL,NULL,0),(243,47,'2015-08-09',2,7,4.60,0.00,4.60,0.00,'',2,0,'2016-06-16 10:27:36',19,NULL,NULL,0),(244,7,'1969-12-31',2,7,5.00,0.00,5.00,0.00,'',2,0,'2016-06-16 10:31:06',19,NULL,NULL,0),(245,47,'2015-08-16',2,7,4.00,0.00,4.00,0.00,'',2,0,'2016-06-16 10:35:44',19,NULL,NULL,0),(246,37,'2015-08-28',2,7,2.00,0.00,2.00,0.00,'',2,0,'2016-06-16 10:38:06',19,NULL,NULL,0),(247,10,'2015-09-20',2,7,5.00,0.00,5.00,0.00,'',2,0,'2016-06-16 10:43:29',19,NULL,NULL,0),(248,37,'2015-09-30',2,7,4.50,0.00,4.50,0.00,'',2,0,'2016-06-16 10:55:35',19,NULL,NULL,0),(249,32,'2015-10-03',2,7,4.00,0.00,4.00,0.00,'',2,0,'2016-06-16 11:04:56',19,NULL,NULL,0),(250,10,'2016-06-16',2,7,42.00,0.00,42.00,0.00,'',2,0,'2016-06-16 11:07:41',19,NULL,NULL,0),(251,7,'2015-10-04',2,7,12.60,0.00,12.60,0.00,'',2,0,'2016-06-16 11:14:55',19,NULL,NULL,0),(252,59,'2015-10-04',2,7,6.50,0.00,6.50,0.00,'',2,0,'2016-06-16 11:18:34',19,NULL,NULL,0),(253,7,'2015-10-08',2,7,2.80,0.00,2.80,0.00,'',2,0,'2016-06-16 11:22:02',19,NULL,NULL,0),(254,39,'2015-10-09',2,7,5.00,0.00,5.00,0.00,'',2,0,'2016-06-16 11:24:30',19,NULL,NULL,0),(255,24,'2015-10-11',2,7,20.50,0.00,20.50,0.00,'',2,0,'2016-06-16 11:30:12',19,NULL,NULL,0),(256,71,'2015-10-23',2,7,2.00,0.00,2.00,0.00,'',2,0,'2016-06-16 11:34:24',19,NULL,NULL,0),(257,71,'2015-10-31',2,7,5.00,0.00,5.00,0.00,'',2,0,'2016-06-16 11:36:02',19,NULL,NULL,0),(258,7,'2015-11-12',2,7,2.00,0.00,2.00,0.00,'',2,0,'2016-06-16 11:37:31',19,NULL,NULL,0),(259,7,'2015-11-13',2,7,13.00,0.00,13.00,0.00,'',2,0,'2016-06-16 11:40:40',19,NULL,NULL,0),(260,10,'2015-12-08',2,7,8.50,0.00,8.50,0.00,'',2,0,'2016-06-16 11:43:36',19,NULL,NULL,0),(261,62,'2015-12-12',2,7,6.00,0.00,6.00,0.00,'',2,0,'2016-06-16 11:45:18',19,NULL,NULL,0),(262,63,'2015-12-13',2,7,2.00,0.00,2.00,0.00,'',2,0,'2016-06-16 11:47:22',19,NULL,NULL,0),(263,75,'2015-12-14',2,7,1.50,0.00,1.50,0.00,'',2,0,'2016-06-16 11:49:54',19,NULL,NULL,0),(264,71,'2015-12-16',2,7,2.00,0.00,2.00,0.00,'',2,0,'2016-06-16 11:52:13',19,NULL,NULL,0),(265,51,'2015-12-18',2,7,9.00,0.00,9.00,0.00,'',2,0,'2016-06-16 11:54:32',19,NULL,NULL,0),(266,10,'2015-12-21',2,7,3.90,0.00,3.90,0.00,'',2,0,'2016-06-16 11:56:31',19,NULL,NULL,0),(267,10,'2015-12-26',2,7,3.20,0.00,3.20,0.00,'',2,0,'2016-06-16 12:00:23',19,NULL,NULL,0),(268,6,'2015-12-27',2,7,8.00,0.00,8.00,0.00,'',2,0,'2016-06-16 12:01:31',19,NULL,NULL,0),(269,51,'2015-12-28',2,7,1.80,0.00,1.80,0.00,'',2,0,'2016-06-16 12:05:41',19,NULL,NULL,0),(270,50,'2016-01-01',2,7,12.00,0.00,12.00,0.00,'',2,0,'2016-06-16 12:08:08',19,NULL,NULL,0),(271,76,'2016-01-01',2,7,4.60,0.00,4.60,0.00,'',2,0,'2016-06-16 12:11:56',19,NULL,NULL,0),(272,74,'2016-01-10',2,7,4.00,0.00,4.00,0.00,'',2,0,'2016-06-16 12:13:01',19,NULL,NULL,0),(273,32,'1969-12-31',2,7,4.00,0.00,4.00,0.00,'',2,0,'2016-06-16 12:22:23',19,NULL,NULL,0),(274,24,'1969-12-31',2,7,11.50,0.00,11.50,0.00,'',2,0,'2016-06-16 12:38:41',19,NULL,NULL,0),(275,6,'2015-01-10',2,7,7.00,0.00,7.00,0.00,'',2,0,'2016-06-17 12:08:23',19,NULL,NULL,0),(276,10,'2016-01-15',2,7,3.50,0.00,3.50,0.00,'',2,0,'2016-06-17 12:11:51',19,NULL,NULL,0),(277,10,'2016-01-16',2,7,1.50,0.00,1.50,0.00,'',2,0,'2016-06-17 12:13:06',19,NULL,NULL,0),(278,77,'2016-01-16',2,7,4.50,0.00,4.50,0.00,'',2,0,'2016-06-17 12:15:12',19,NULL,NULL,0),(279,10,'2016-01-17',2,7,4.50,0.00,4.50,0.00,'',2,0,'2016-06-17 12:16:26',19,NULL,NULL,0),(280,24,'2016-01-20',2,7,1.50,0.00,1.50,0.00,'',2,0,'2016-06-17 12:17:42',19,NULL,NULL,0),(281,71,'2016-01-26',2,7,18.60,0.00,18.60,0.00,'',2,0,'2016-06-17 12:20:24',19,NULL,NULL,0),(282,71,'2013-02-02',2,7,8.00,0.00,8.00,0.00,'',2,0,'2016-06-17 12:22:48',19,NULL,NULL,0),(283,10,'2016-02-05',2,7,7.80,0.00,7.80,0.00,'',2,0,'2016-06-17 12:28:23',19,NULL,NULL,0),(284,37,'2016-06-17',2,7,3.00,0.00,3.00,0.00,'',2,0,'2016-06-20 12:08:02',19,NULL,NULL,0),(285,2,'2016-06-17',1,7,11.00,0.00,11.00,0.00,'',2,0,'2016-06-20 12:09:06',19,NULL,NULL,0),(286,19,'2016-06-17',1,7,10.50,0.00,10.50,0.00,'',2,0,'2016-06-20 12:10:05',19,NULL,NULL,0),(287,12,'2016-06-17',1,7,10.00,0.00,10.00,0.00,'',2,0,'2016-06-20 12:11:04',19,NULL,NULL,0),(288,9,'2016-06-18',1,7,46.30,0.00,46.30,0.00,'',2,0,'2016-06-20 12:13:04',19,NULL,NULL,0),(289,7,'2016-06-18',1,7,4.00,0.00,4.00,0.00,'',2,0,'2016-06-20 12:14:13',19,NULL,NULL,0),(290,20,'2016-06-18',1,7,24.00,0.00,24.00,0.00,'',2,0,'2016-06-20 12:16:50',19,NULL,NULL,0),(291,78,'2016-06-18',1,7,3.00,0.00,3.00,0.00,'',2,0,'2016-06-20 12:21:10',19,NULL,NULL,0),(292,2,'2016-06-18',1,7,4.50,0.00,4.50,0.00,'',2,0,'2016-06-20 12:22:23',19,NULL,NULL,0),(293,5,'2016-06-18',1,7,27.00,0.00,27.00,0.00,'',2,0,'2016-06-20 12:24:18',19,NULL,NULL,0),(294,2,'2016-06-18',1,7,91.50,0.00,91.50,0.00,'',2,0,'2016-06-20 12:28:39',19,NULL,NULL,0),(295,24,'2016-06-18',1,7,4.00,0.00,4.00,0.00,'',2,0,'2016-06-20 12:29:59',19,NULL,NULL,0),(296,7,'2016-06-18',2,7,6.00,0.00,6.00,0.00,'',2,0,'2016-06-20 12:31:17',19,NULL,NULL,0),(297,21,'2016-06-18',2,7,18.00,0.00,18.00,0.00,'',2,0,'2016-06-20 12:35:28',19,NULL,NULL,0),(298,17,'2016-06-18',2,7,2.00,0.00,2.00,0.00,'',2,0,'2016-06-20 12:36:28',19,NULL,NULL,0),(299,20,'2016-06-18',2,7,17.00,0.00,17.00,0.00,'',2,0,'2016-06-20 12:38:20',19,NULL,NULL,0),(300,42,'2016-06-18',1,7,36.00,0.00,36.00,0.00,'',2,0,'2016-06-20 12:58:53',19,NULL,NULL,0),(301,15,'2016-06-18',2,7,3.50,0.00,3.50,0.00,'',2,0,'2016-06-20 13:00:39',19,NULL,NULL,0),(302,62,'2016-06-18',1,7,5.00,0.00,5.00,0.00,'',2,0,'2016-06-20 13:10:52',19,NULL,NULL,0),(303,2,'2016-06-19',1,7,21.50,0.00,21.50,0.00,'',2,0,'2016-06-20 13:15:33',19,NULL,NULL,0),(304,51,'2016-06-21',2,7,4.00,0.00,4.00,0.00,'',2,0,'2016-06-21 11:14:38',19,NULL,NULL,0),(305,14,'2016-02-06',2,7,2.00,0.00,2.00,0.00,'',2,0,'2016-06-21 11:19:15',19,NULL,NULL,0),(306,49,'2016-02-06',2,7,4.00,0.00,4.00,0.00,'',2,0,'2016-06-21 11:21:25',19,NULL,NULL,0),(307,50,'2016-02-06',2,7,30.50,0.00,30.50,0.00,'',2,0,'2016-06-21 11:23:47',19,NULL,NULL,0),(308,5,'2016-02-06',1,7,2.00,0.00,2.00,0.00,'',2,0,'2016-06-21 11:25:14',19,NULL,NULL,0),(309,42,'2016-02-06',2,7,21.50,0.00,21.50,0.00,'',2,0,'2016-06-21 11:27:21',19,NULL,NULL,0),(310,49,'2016-02-07',2,7,2.80,0.00,2.80,0.00,'',2,0,'2016-06-21 11:28:55',19,NULL,NULL,0),(311,10,'2016-02-08',2,7,1.50,0.00,1.50,0.00,'',2,0,'2016-06-21 11:30:19',19,NULL,NULL,0),(312,49,'2016-02-22',2,7,5.60,0.00,5.60,0.00,'',2,0,'2016-06-21 11:32:25',19,NULL,NULL,0),(313,48,'2016-02-13',2,7,9.00,0.00,9.00,0.00,'',2,0,'2016-06-21 11:34:07',19,NULL,NULL,0),(314,14,'2016-02-26',2,7,2.00,0.00,2.00,0.00,'',2,0,'2016-06-21 11:37:12',19,NULL,NULL,0),(315,7,'2016-03-04',2,7,6.00,0.00,6.00,0.00,'',2,0,'2016-06-21 11:38:52',19,NULL,NULL,0),(316,7,'2016-03-14',2,7,2.00,0.00,2.00,0.00,'',2,0,'2016-06-21 11:40:23',19,NULL,NULL,0),(317,24,'2016-03-23',2,7,5.00,0.00,5.00,0.00,'',2,0,'2016-06-21 11:41:33',19,NULL,NULL,0),(318,57,'2016-03-29',2,7,1.50,0.00,1.50,0.00,'',2,0,'2016-06-21 11:42:56',19,NULL,NULL,0),(319,37,'2016-04-13',2,7,2.10,0.00,2.10,0.00,'',2,0,'2016-06-21 11:46:49',19,NULL,NULL,0),(320,42,'2016-04-24',2,7,19.50,0.00,19.50,0.00,'',2,0,'2016-06-21 11:54:06',19,NULL,NULL,0),(321,79,'2016-03-30',2,7,27.00,0.00,27.00,0.00,'',2,0,'2016-06-22 09:54:40',19,NULL,NULL,0),(322,7,'2016-04-03',2,7,6.00,0.00,6.00,0.00,'',2,0,'2016-06-22 09:56:05',19,NULL,NULL,0),(323,7,'2015-05-04',2,7,2.00,0.00,2.00,0.00,'',2,0,'2016-06-22 10:36:10',19,NULL,NULL,0),(324,2,'2016-06-20',1,7,3.00,0.00,3.00,0.00,'',2,0,'2016-06-22 10:41:11',19,NULL,NULL,0),(325,5,'2016-06-21',1,7,2.00,0.00,2.00,0.00,'',2,0,'2016-06-22 10:41:47',19,NULL,NULL,0),(326,2,'2016-06-21',1,7,35.00,0.00,35.00,0.00,'',2,0,'2016-06-22 10:43:00',19,NULL,NULL,0),(327,58,'2014-02-15',2,7,7.00,0.00,7.00,0.00,'',2,0,'2016-06-22 11:01:34',19,NULL,NULL,0),(328,63,'1969-12-31',2,7,2.50,0.00,2.50,0.00,'',2,0,'2016-06-22 11:05:15',19,NULL,NULL,0),(329,52,'2014-04-20',2,7,2.00,0.00,2.00,0.00,'',2,0,'2016-06-22 11:06:43',19,NULL,NULL,0),(330,63,'2014-06-29',2,7,2.50,0.00,2.50,0.00,'',2,0,'2016-06-22 11:25:39',19,NULL,NULL,0),(331,52,'2014-07-28',2,7,5.00,0.00,5.00,0.00,'',2,0,'2016-06-22 11:37:16',19,NULL,NULL,0),(332,71,'1969-12-31',2,7,58.00,0.00,58.00,0.00,'',2,0,'2016-06-22 11:38:44',19,NULL,NULL,0),(333,10,'2014-12-28',2,7,27.00,0.00,27.00,0.00,'',2,0,'2016-06-22 11:39:53',19,NULL,NULL,0),(334,80,'2015-01-03',2,7,11.80,0.00,11.80,0.00,'',2,0,'2016-06-22 11:43:00',19,NULL,NULL,0),(335,51,'1969-12-31',2,7,12.50,0.00,12.50,0.00,'',2,0,'2016-06-22 11:50:14',19,NULL,NULL,0),(336,81,'2015-01-09',2,7,70.00,0.00,70.00,0.00,'',2,0,'2016-06-22 12:02:27',19,NULL,NULL,0),(337,51,'2016-02-08',2,7,18.00,0.00,18.00,0.00,'',2,0,'2016-06-23 11:21:02',19,NULL,NULL,0),(338,51,'2016-02-11',2,7,2.50,0.00,2.50,0.00,'',2,0,'2016-06-23 11:23:35',19,NULL,NULL,0),(339,10,'2016-02-17',2,7,76.10,0.00,76.10,0.00,'',2,0,'2016-06-23 11:31:02',19,NULL,NULL,0),(340,51,'2015-03-13',2,7,4.70,0.00,4.70,0.00,'',2,0,'2016-06-23 11:39:35',19,NULL,NULL,0),(341,7,'2016-04-04',2,7,11.00,0.00,11.00,0.00,'',2,0,'2016-06-23 11:49:07',19,NULL,NULL,0),(342,39,'2015-04-04',2,7,8.50,0.00,8.50,0.00,'',2,0,'2016-06-23 11:52:54',19,NULL,NULL,0),(343,24,'2016-04-06',2,7,7.00,0.00,7.00,0.00,'',2,0,'2016-06-23 11:56:38',19,NULL,NULL,0),(344,71,'2015-04-11',2,7,21.00,0.00,21.00,0.00,'',2,0,'2016-06-24 10:50:17',19,NULL,NULL,0),(345,70,'2015-04-19',2,7,8.00,0.00,8.00,0.00,'',2,0,'2016-06-24 10:54:16',19,NULL,NULL,0),(346,24,'2015-05-03',2,7,9.20,0.00,9.20,0.00,'',2,0,'2016-06-24 10:56:42',19,NULL,NULL,0),(347,73,'2015-05-08',2,7,7.00,0.00,7.00,0.00,'',2,0,'2016-06-24 10:58:22',19,NULL,NULL,0),(348,47,'2015-05-24',2,7,6.60,0.00,6.60,0.00,'',2,0,'2016-06-24 11:08:50',19,NULL,NULL,0),(349,32,'2015-06-14',2,7,0.50,0.00,0.50,0.00,'',2,0,'2016-06-24 11:59:43',19,NULL,NULL,0),(350,10,'2015-07-03',2,7,27.00,0.00,27.00,0.00,'',2,0,'2016-06-24 12:05:04',19,NULL,NULL,0),(351,46,'2015-01-03',2,7,40.50,0.00,40.50,0.00,'',2,0,'2016-06-24 12:10:20',19,NULL,NULL,0),(352,63,'2015-07-25',2,7,3.50,0.00,3.50,0.00,'',2,0,'2016-06-24 12:16:52',19,NULL,NULL,0),(353,7,'1969-12-31',2,7,90.00,0.00,90.00,0.00,'a cta 60.00 sra. Mariana 30.00 valeria',2,0,'2016-06-24 12:20:48',19,NULL,NULL,0),(354,51,'2015-08-01',2,7,18.80,0.00,18.80,0.00,'',2,0,'2016-06-24 12:25:36',19,NULL,NULL,0),(355,71,'1969-12-31',2,7,11.00,0.00,11.00,0.00,'',2,0,'2016-06-24 12:27:04',19,NULL,NULL,0),(356,37,'1969-12-31',2,7,4.00,0.00,4.00,0.00,'',2,0,'2016-06-24 12:31:32',19,NULL,NULL,0),(357,7,'1969-12-31',2,7,6.20,0.00,6.20,0.00,'',2,0,'2016-06-24 12:35:45',19,NULL,NULL,0),(358,51,'1969-12-31',2,7,8.60,0.00,8.60,0.00,'',2,0,'2016-06-24 12:37:54',19,NULL,NULL,0),(359,39,'1969-12-31',2,7,11.50,0.00,11.50,0.00,'',2,0,'2016-06-24 12:39:50',19,NULL,NULL,0),(360,10,'1969-12-31',2,7,9.20,0.00,9.20,0.00,'',2,0,'2016-06-24 12:42:13',19,NULL,NULL,0),(361,2,'2016-06-23',1,7,38.00,0.00,38.00,0.00,'',2,0,'2016-06-27 10:18:59',19,NULL,NULL,0),(362,2,'2016-06-23',1,7,6.00,0.00,6.00,0.00,'',2,0,'2016-06-27 10:20:07',19,NULL,NULL,0),(363,13,'2016-06-24',1,7,35.00,0.00,35.00,0.00,'',2,0,'2016-06-27 10:20:58',19,NULL,NULL,0),(364,17,'2016-06-25',2,7,10.00,0.00,10.00,0.00,'',2,0,'2016-06-27 10:21:43',19,NULL,NULL,0),(365,7,'2016-06-25',1,7,2.00,0.00,2.00,0.00,'',2,0,'2016-06-27 10:22:27',19,NULL,NULL,0),(366,17,'2016-06-25',1,7,4.50,0.00,4.50,0.00,'',2,0,'2016-06-27 10:23:46',19,NULL,NULL,0),(367,2,'2016-06-25',1,7,95.00,0.00,95.00,0.00,'',2,0,'2016-06-27 10:28:42',19,NULL,NULL,0),(368,43,'2016-06-26',1,7,8.80,0.00,8.80,0.00,'',2,0,'2016-06-27 10:30:45',19,NULL,NULL,0),(369,7,'2016-06-26',1,7,6.00,0.00,6.00,0.00,'',2,0,'2016-06-27 10:31:34',19,NULL,NULL,0),(370,61,'2016-08-13',1,7,2.00,0.00,2.00,0.00,'',1,1,'2016-08-13 13:35:53',18,NULL,NULL,0),(371,5,'2016-08-13',1,7,4.00,0.00,4.00,0.00,'',2,0,'2016-08-16 09:46:56',21,NULL,NULL,0),(372,17,'2016-08-13',1,7,7.20,0.00,7.20,0.00,'',2,0,'2016-08-16 09:54:38',21,NULL,NULL,0),(373,5,'2016-08-13',1,7,31.50,0.00,31.50,0.00,'',2,0,'2016-08-16 10:52:52',18,NULL,NULL,0),(374,59,'2016-08-13',1,7,71.00,0.00,71.00,0.00,'',2,0,'2016-08-16 11:06:56',20,NULL,NULL,0),(375,77,'2016-08-13',1,7,2.10,0.00,2.10,0.00,'',2,0,'2016-08-16 11:15:05',20,NULL,NULL,0),(376,2,'2016-08-13',1,7,100.00,0.00,100.00,0.00,'',2,0,'2016-08-16 11:24:36',21,NULL,NULL,0),(377,6,'2016-08-14',2,7,14.40,0.00,14.40,0.00,'',2,0,'2016-08-16 11:33:50',21,NULL,NULL,0),(378,9,'2016-08-14',1,7,15.50,0.00,15.50,0.00,'',2,0,'2016-08-16 11:43:40',20,NULL,NULL,0),(379,62,'2016-08-14',1,7,11.00,0.00,11.00,0.00,'',2,0,'2016-08-16 11:48:57',20,NULL,NULL,0),(380,2,'2016-08-14',1,7,20.00,0.00,20.00,0.00,'',2,0,'2016-08-16 11:57:07',21,NULL,NULL,0),(381,36,'2016-08-14',1,7,26.80,0.00,26.80,0.00,'',2,0,'2016-08-16 12:15:34',20,NULL,NULL,0),(382,35,'2016-08-14',2,7,8.50,0.00,8.50,0.00,'',2,0,'2016-08-17 10:16:47',21,NULL,NULL,0),(383,93,'2016-08-14',1,7,5.00,0.00,5.00,0.00,'',2,0,'2016-08-17 10:21:11',20,NULL,NULL,0),(384,32,'2016-08-14',1,7,5.00,0.00,5.00,0.00,'',2,0,'2016-08-17 10:24:21',20,NULL,NULL,0),(385,9,'2016-08-14',2,7,13.00,0.00,13.00,0.00,'',2,0,'2016-08-17 10:28:30',20,NULL,NULL,0),(386,5,'2016-08-14',2,7,14.00,0.00,14.00,0.00,'',2,0,'2016-08-17 10:30:52',20,NULL,NULL,0),(387,2,'2016-08-14',1,7,20.00,0.00,20.00,0.00,'',2,0,'2016-08-17 10:40:53',21,NULL,NULL,0),(388,2,'2016-08-16',1,7,23.40,0.00,23.40,0.00,'',2,0,'2016-08-17 10:47:41',21,NULL,NULL,0),(389,19,'2016-08-17',2,7,25.00,0.00,25.00,0.00,'',2,0,'2016-08-19 12:08:10',21,NULL,NULL,0),(390,5,'2016-08-18',2,7,7.00,0.00,7.00,0.00,'',2,0,'2016-08-19 12:14:53',21,NULL,NULL,0),(391,5,'2016-08-13',1,7,4.00,0.00,4.00,0.00,'',2,0,'2016-08-19 12:44:14',21,NULL,NULL,0),(392,2,'2016-08-19',1,7,11.00,0.00,11.00,0.00,'',2,0,'2016-08-22 16:39:22',21,NULL,NULL,0),(393,10,'2016-08-20',1,7,33.00,0.00,33.00,0.00,'',2,0,'2016-08-22 16:43:30',21,NULL,NULL,0),(394,7,'2016-08-20',2,7,18.00,0.00,18.00,0.00,'',2,0,'2016-08-22 16:48:24',21,NULL,NULL,0),(395,19,'2016-08-20',1,7,5.00,0.00,5.00,0.00,'',2,0,'2016-08-22 17:49:43',21,NULL,NULL,0),(396,93,'2016-08-20',1,7,10.00,0.00,10.00,0.00,'',2,0,'2016-08-22 17:52:29',21,NULL,NULL,0),(397,12,'2016-08-20',1,7,70.50,0.00,70.50,0.00,'',2,0,'2016-08-22 18:36:29',21,NULL,NULL,0),(398,20,'2016-08-20',1,7,2.00,0.00,2.00,0.00,'',2,0,'2016-08-22 18:42:49',21,NULL,NULL,0),(399,5,'2016-08-20',1,7,7.00,0.00,7.00,0.00,'',2,0,'2016-08-22 18:58:49',21,NULL,NULL,0),(400,7,'2016-08-20',1,7,2.00,0.00,2.00,0.00,'',2,0,'2016-08-22 19:04:17',21,NULL,NULL,0),(401,20,'2016-08-20',1,7,25.40,0.00,25.40,0.00,'',2,0,'2016-08-23 10:29:09',20,NULL,NULL,0),(402,5,'2016-08-20',1,7,59.80,0.00,59.80,0.00,'',2,0,'2016-08-23 10:36:27',20,NULL,NULL,0),(403,15,'2016-08-23',1,7,12.00,0.00,12.00,0.00,'',2,0,'2016-08-23 10:39:54',20,NULL,NULL,0),(404,10,'2016-08-20',1,7,6.00,0.00,6.00,0.00,'',2,0,'2016-08-23 10:42:10',20,NULL,NULL,0),(405,62,'2016-08-23',1,7,3.00,0.00,3.00,0.00,'',2,0,'2016-08-23 10:43:33',20,NULL,NULL,0),(406,24,'2016-08-20',1,7,9.00,0.00,9.00,0.00,'',2,0,'2016-08-23 10:46:58',20,NULL,NULL,0),(407,17,'2016-08-20',1,7,2.00,0.00,2.00,0.00,'',2,0,'2016-08-23 10:48:35',20,NULL,NULL,0),(408,93,'2016-08-20',1,7,14.00,0.00,14.00,0.00,'',2,0,'2016-08-23 10:50:50',20,NULL,NULL,0),(409,2,'2016-08-19',1,7,11.00,0.00,11.00,0.00,'',2,0,'2016-08-23 11:40:42',20,NULL,NULL,0),(410,2,'2016-08-19',1,7,11.00,0.00,11.00,0.00,'',2,0,'2016-08-23 11:48:32',20,NULL,NULL,0),(411,20,'2016-08-20',1,7,25.40,0.00,25.40,0.00,'',2,0,'2016-08-23 11:50:31',20,NULL,NULL,0),(412,10,'2016-08-20',1,7,33.00,0.00,33.00,0.00,'',2,0,'2016-08-23 11:52:13',20,NULL,NULL,0),(413,7,'2016-08-20',2,7,18.00,0.00,18.00,0.00,'',2,0,'2016-08-23 11:53:46',20,NULL,NULL,0),(414,5,'2016-08-20',1,7,59.80,0.00,59.80,0.00,'',2,0,'2016-08-23 11:56:50',20,NULL,NULL,0),(415,12,'2016-08-20',1,7,70.50,0.00,70.50,0.00,'',2,0,'2016-08-23 11:59:57',20,NULL,NULL,0),(416,93,'2016-08-20',1,7,14.00,0.00,14.00,0.00,'',2,0,'2016-08-23 12:00:57',20,NULL,NULL,0),(417,15,'2016-08-21',1,7,18.00,0.00,18.00,0.00,'',2,0,'2016-08-23 12:07:13',20,NULL,NULL,0),(418,7,'2016-08-21',1,7,6.50,0.00,6.50,0.00,'',2,0,'2016-08-23 12:12:03',20,NULL,NULL,0),(419,10,'2016-08-21',1,7,2.00,0.00,2.00,0.00,'',2,0,'2016-08-23 12:36:26',20,NULL,NULL,0),(420,82,'2016-08-21',1,7,13.00,0.00,13.00,0.00,'',2,0,'2016-08-23 12:41:27',20,NULL,NULL,0),(421,9,'2016-08-21',1,7,28.30,0.00,28.30,0.00,'',2,0,'2016-08-23 12:47:07',20,NULL,NULL,0),(422,16,'2016-08-21',1,7,17.10,0.00,17.10,0.00,'',2,0,'2016-08-23 13:02:10',20,NULL,NULL,0),(423,32,'2016-08-21',1,7,2.70,0.00,2.70,0.00,'',2,0,'2016-08-23 13:14:20',20,NULL,NULL,0),(424,2,'2016-08-21',1,7,10.00,0.00,10.00,0.00,'',2,0,'2016-08-23 17:44:03',21,NULL,NULL,0),(425,2,'2016-08-21',1,7,11.50,0.00,11.50,0.00,'',2,0,'2016-08-23 17:52:58',21,NULL,NULL,0),(426,7,'2016-08-22',2,7,2.00,0.00,2.00,0.00,'',2,0,'2016-08-23 17:56:44',21,NULL,NULL,0),(427,95,'2016-08-21',1,7,45.00,0.00,45.00,0.00,'',2,0,'2016-08-24 11:31:57',20,NULL,NULL,0),(428,2,'2016-08-23',1,7,3.00,0.00,3.00,0.00,'',2,0,'2016-08-24 16:23:51',21,NULL,NULL,0),(429,36,'2016-08-14',1,7,26.80,0.00,26.80,0.00,'',2,0,'2016-08-25 10:31:25',19,NULL,NULL,0),(430,2,'2016-08-24',1,7,9.20,0.00,9.20,0.00,'',2,0,'2016-08-25 14:59:45',20,NULL,NULL,0),(431,37,'2016-08-25',2,7,19.50,0.00,19.50,0.00,'',2,0,'2016-08-26 20:05:42',21,NULL,NULL,0),(432,7,'2016-08-26',2,7,8.00,0.00,8.00,0.00,'',2,0,'2016-08-27 14:09:59',20,NULL,NULL,0),(433,17,'2016-08-27',1,7,5.00,0.00,5.00,0.00,'',2,0,'2016-08-27 14:11:54',20,NULL,NULL,0),(434,15,'2016-08-29',2,7,2.40,0.00,2.40,0.00,'',2,0,'2016-08-29 14:52:04',20,NULL,NULL,0),(435,9,'2016-08-27',2,7,26.40,0.00,26.40,0.00,'',2,0,'2016-08-29 14:57:45',20,NULL,NULL,0),(436,88,'2016-08-27',1,7,20.00,0.00,20.00,0.00,'',2,0,'2016-08-29 15:01:03',20,NULL,NULL,0),(437,88,'2016-08-27',1,7,14.00,0.00,14.00,0.00,'',2,0,'2016-08-29 15:04:05',20,NULL,NULL,0),(438,2,'2016-08-27',1,7,21.80,0.00,21.80,0.00,'',2,0,'2016-08-29 15:09:50',20,NULL,NULL,0),(439,15,'2016-08-28',1,7,140.40,0.00,140.40,0.00,'',2,0,'2016-08-29 18:23:22',20,NULL,NULL,0),(440,16,'2016-08-28',1,7,7.50,0.00,7.50,0.00,'',2,0,'2016-08-29 18:25:46',20,NULL,NULL,0),(441,7,'2016-08-28',2,7,8.00,0.00,8.00,0.00,'',2,0,'2016-08-29 18:30:25',20,NULL,NULL,0),(442,6,'2016-08-28',1,7,12.20,0.00,12.20,0.00,'',2,0,'2016-08-29 18:34:58',20,NULL,NULL,0),(443,17,'2016-08-28',1,7,20.00,0.00,20.00,0.00,'',2,0,'2016-08-29 18:38:38',20,NULL,NULL,0),(444,63,'2016-08-28',1,7,3.50,0.00,3.50,0.00,'',2,0,'2016-08-29 18:41:45',20,NULL,NULL,0),(445,9,'2016-08-28',1,7,43.40,0.00,43.40,0.00,'',2,0,'2016-08-29 19:32:34',20,NULL,NULL,0),(446,2,'2016-08-28',1,7,7.50,0.00,7.50,0.00,'',2,0,'2016-08-30 19:27:12',20,NULL,NULL,0),(447,15,'2016-08-28',1,7,4.50,0.00,4.50,0.00,'',2,0,'2016-08-30 19:29:45',20,NULL,NULL,0),(448,13,'2016-08-29',1,7,20.00,0.00,20.00,0.00,'',2,0,'2016-08-30 20:10:10',20,NULL,NULL,0),(449,65,'2016-08-30',1,7,10.00,0.00,10.00,0.00,'',2,0,'2016-08-30 20:11:49',20,NULL,NULL,0),(450,28,'2016-08-30',1,7,5.00,0.00,5.00,0.00,'',2,0,'2016-08-30 20:13:52',20,NULL,NULL,0),(451,7,'2016-08-30',1,7,2.00,0.00,2.00,0.00,'',2,0,'2016-08-30 20:23:08',20,NULL,NULL,0),(452,17,'2016-08-30',2,7,14.80,0.00,14.80,0.00,'',2,0,'2016-08-30 20:25:51',20,NULL,NULL,0),(453,16,'2016-08-30',2,7,1.50,0.00,1.50,0.00,'',2,0,'2016-08-30 20:27:43',20,NULL,NULL,0),(454,9,'2016-08-30',1,7,15.50,0.00,15.50,0.00,'',2,0,'2016-08-30 20:30:49',20,NULL,NULL,0),(455,62,'2016-08-30',1,7,1.50,0.00,1.50,0.00,'',2,0,'2016-08-30 20:32:09',20,NULL,NULL,0),(456,37,'2016-09-30',1,7,4.00,0.00,4.00,0.00,'',2,0,'2016-09-01 11:00:06',20,NULL,NULL,0),(457,2,'2016-08-30',1,7,15.00,0.00,15.00,0.00,'',2,0,'2016-09-01 11:03:36',20,NULL,NULL,0),(458,2,'2016-08-30',1,7,4.50,0.00,4.50,0.00,'',2,0,'2016-09-01 11:05:31',20,NULL,NULL,0),(459,2,'2016-09-01',1,7,4.50,0.00,4.50,0.00,'',2,0,'2016-09-05 14:41:10',21,NULL,NULL,0),(460,2,'2016-09-01',1,7,1.50,0.00,1.50,0.00,'',2,0,'2016-09-05 14:44:00',21,NULL,NULL,0),(461,57,'2016-09-02',1,7,6.50,0.00,6.50,0.00,'',2,0,'2016-09-05 14:48:47',21,NULL,NULL,0),(462,95,'2016-09-02',2,7,9.00,0.00,9.00,0.00,'',2,0,'2016-09-05 20:44:02',21,NULL,NULL,0),(463,42,'2016-09-02',2,7,1.50,0.00,1.50,0.00,'',2,0,'2016-09-05 21:14:20',21,NULL,NULL,0),(464,65,'2016-09-02',1,7,12.00,0.00,12.00,0.00,'',2,0,'2016-09-05 21:19:50',21,NULL,NULL,0),(465,37,'2016-09-02',2,7,10.00,0.00,10.00,0.00,'',2,0,'2016-09-05 21:22:19',21,NULL,NULL,0),(466,2,'2016-09-02',1,7,23.00,0.00,23.00,0.00,'',2,0,'2016-09-05 21:25:27',21,NULL,NULL,0),(467,10,'2016-09-05',1,7,3.50,0.00,3.50,0.00,'',2,0,'2016-09-05 21:41:32',21,NULL,NULL,0),(468,15,'2016-09-03',1,7,22.50,0.00,22.50,0.00,'',2,0,'2016-09-05 21:49:40',21,NULL,NULL,0),(469,35,'2016-09-03',2,7,12.00,0.00,12.00,0.00,'',2,0,'2016-09-05 22:08:25',21,NULL,NULL,0),(470,62,'2016-09-03',1,7,3.00,0.00,3.00,0.00,'',2,0,'2016-09-05 22:10:09',21,NULL,NULL,0),(471,7,'2016-09-03',1,7,4.00,0.00,4.00,0.00,'',2,0,'2016-09-05 22:12:59',21,NULL,NULL,0),(472,24,'2016-09-03',1,7,10.00,0.00,10.00,0.00,'',2,0,'2016-09-05 22:15:10',21,NULL,NULL,0),(473,95,'2016-09-03',1,7,118.20,0.00,118.20,0.00,'',2,0,'2016-09-05 22:20:53',21,NULL,NULL,0),(474,9,'2016-09-03',1,7,15.00,0.00,15.00,0.00,'',2,0,'2016-09-05 22:23:36',21,NULL,NULL,0),(475,12,'2016-09-03',1,7,47.50,0.00,47.50,0.00,'',2,0,'2016-09-06 00:58:07',21,NULL,NULL,0),(476,95,'2016-09-04',2,7,23.00,0.00,23.00,0.00,'',2,0,'2016-09-06 17:22:13',21,NULL,NULL,0),(477,95,'2016-09-04',2,7,23.00,0.00,23.00,0.00,'',2,0,'2016-09-06 17:27:11',21,NULL,NULL,0),(478,15,'2016-09-04',1,7,32.60,0.00,32.60,0.00,'',2,0,'2016-09-06 17:34:28',21,NULL,NULL,0),(479,61,'2016-09-04',1,7,10.20,0.00,10.20,0.00,'',2,0,'2016-09-06 17:38:16',21,NULL,NULL,0),(480,7,'2016-09-04',1,7,7.00,0.00,7.00,0.00,'',2,0,'2016-09-06 17:42:02',21,NULL,NULL,0),(481,9,'2016-09-04',1,7,33.50,0.00,33.50,0.00,'',2,0,'2016-09-06 17:46:59',21,NULL,NULL,0),(482,10,'2016-09-04',1,7,3.50,0.00,3.50,0.00,'',2,0,'2016-09-06 17:54:19',21,NULL,NULL,0),(483,17,'2016-09-04',1,7,2.00,0.00,2.00,0.00,'',2,0,'2016-09-06 17:56:15',21,NULL,NULL,0),(484,16,'2016-09-04',1,7,3.00,0.00,3.00,0.00,'',2,0,'2016-09-06 17:58:02',21,NULL,NULL,0),(485,24,'2016-09-04',1,7,1.50,0.00,1.50,0.00,'',2,0,'2016-09-06 17:59:35',21,NULL,NULL,0),(486,2,'2016-09-04',1,7,10.20,0.00,10.20,0.00,'',2,0,'2016-09-06 18:23:19',21,NULL,NULL,0),(487,2,'2016-09-04',1,7,42.40,0.00,42.40,0.00,'',2,0,'2016-09-06 18:31:37',21,NULL,NULL,0),(488,65,'2016-09-05',2,7,39.50,0.00,39.50,0.00,'',2,0,'2016-09-06 18:41:12',21,NULL,NULL,0),(489,35,'2016-09-05',1,7,5.00,0.00,5.00,0.00,'',2,0,'2016-09-06 19:11:32',21,NULL,NULL,0),(490,37,'2016-09-05',2,7,5.00,0.00,5.00,0.00,'',2,0,'2016-09-06 19:13:49',21,NULL,NULL,0),(491,2,'2016-09-06',1,7,7.10,0.00,7.10,0.00,'',2,0,'2016-09-06 19:18:26',21,NULL,NULL,0),(492,2,'2016-09-06',1,7,5.70,0.00,5.70,0.00,'',2,0,'2016-09-06 19:22:43',21,NULL,NULL,0),(493,94,'2016-09-06',1,7,10.00,0.00,10.00,0.00,'',2,0,'2016-09-07 16:59:22',21,NULL,NULL,0),(494,62,'2016-09-07',1,7,1.50,0.00,1.50,0.00,'',2,0,'2016-09-07 17:00:58',21,NULL,NULL,0),(495,37,'2016-09-07',1,7,1.50,0.00,1.50,0.00,'',2,0,'2016-09-08 19:05:48',21,NULL,NULL,0),(496,2,'2016-09-08',1,7,14.00,0.00,14.00,0.00,'',2,0,'2016-09-08 19:08:13',21,NULL,NULL,0),(497,37,'2016-09-08',2,7,31.50,0.00,31.50,0.00,'',2,0,'2016-09-08 19:11:21',21,NULL,NULL,0),(498,3,'2016-09-08',1,7,1.50,0.00,1.50,0.00,'',2,0,'2016-09-08 20:37:36',17,NULL,NULL,0),(499,37,'2016-09-08',2,7,15.00,0.00,15.00,0.00,'',2,0,'2016-09-09 18:16:26',21,NULL,NULL,0),(500,65,'2016-09-08',2,7,31.50,0.00,31.50,0.00,'',2,0,'2016-09-09 18:19:32',21,NULL,NULL,0),(501,13,'2016-09-08',1,7,12.40,0.00,12.40,0.00,'',2,0,'2016-09-09 18:22:14',21,NULL,NULL,0),(502,21,'2016-09-08',1,7,15.00,0.00,15.00,0.00,'',2,0,'2016-09-09 18:26:37',21,NULL,NULL,0),(503,2,'2016-09-08',1,7,25.00,0.00,25.00,0.00,'',2,0,'2016-09-09 18:28:24',21,NULL,NULL,0),(504,15,'2016-09-11',1,7,14.40,0.00,14.40,0.00,'',2,0,'2016-09-13 12:39:09',20,NULL,NULL,0),(505,6,'2016-09-11',1,7,10.40,0.00,10.40,0.00,'',2,0,'2016-09-13 12:46:08',20,NULL,NULL,0),(506,7,'2016-09-11',1,7,5.50,0.00,5.50,0.00,'',2,0,'2016-09-13 12:49:22',20,NULL,NULL,0),(507,20,'2016-09-11',1,7,4.40,0.00,4.40,0.00,'',2,0,'2016-09-13 12:51:24',20,NULL,NULL,0),(508,77,'2016-09-11',1,7,5.90,0.00,5.90,0.00,'',2,0,'2016-09-13 12:53:36',20,NULL,NULL,0),(509,17,'2016-09-13',1,7,2.00,0.00,2.00,0.00,'',2,0,'2016-09-13 12:54:46',20,NULL,NULL,0),(510,14,'2016-09-11',1,7,2.00,0.00,2.00,0.00,'',2,0,'2016-09-13 12:56:37',20,NULL,NULL,0),(511,63,'2016-09-11',1,7,12.00,0.00,12.00,0.00,'',2,0,'2016-09-13 13:00:18',20,NULL,NULL,0),(512,15,'2016-09-11',1,7,5.00,0.00,5.00,0.00,'',2,0,'2016-09-13 13:06:47',20,NULL,NULL,0),(513,15,'2016-09-11',1,7,2.00,0.00,2.00,0.00,'',2,0,'2016-09-13 13:08:10',20,NULL,NULL,0),(514,2,'2016-09-12',1,7,2.00,0.00,2.00,0.00,'',2,0,'2016-09-13 13:09:29',20,NULL,NULL,0),(515,24,'2016-09-13',1,7,23.00,0.00,23.00,0.00,'',2,0,'2016-09-13 19:16:36',20,NULL,NULL,0),(516,97,'2016-09-17',1,7,2.00,0.00,2.00,0.00,'',2,0,'2016-09-17 12:20:54',20,NULL,NULL,0),(517,2,'2016-09-17',1,7,6.00,0.00,6.00,0.00,'',2,0,'2016-09-17 12:31:59',20,NULL,NULL,0),(518,37,'2016-09-17',1,7,3.50,0.00,3.50,0.00,'',2,0,'2016-09-17 12:34:09',20,NULL,NULL,0),(519,2,'2016-09-17',1,7,11.30,0.00,11.30,0.00,'',2,0,'2016-09-17 12:36:40',20,NULL,NULL,0),(520,14,'2016-09-16',2,7,40.20,0.00,40.20,0.00,'',2,0,'2016-09-17 15:27:57',20,NULL,NULL,0),(521,2,'2016-09-16',1,7,68.00,0.00,68.00,0.00,'',2,0,'2016-09-17 15:31:43',20,NULL,NULL,0),(522,2,'2016-09-17',1,7,9.00,0.00,9.00,0.00,'',2,0,'2016-09-17 15:34:34',20,NULL,NULL,0),(523,2,'2016-09-17',1,7,9.00,0.00,9.00,0.00,'',2,0,'2016-09-21 09:13:33',20,NULL,NULL,0),(524,2,'2016-09-17',1,7,470.00,0.00,470.00,0.00,'',2,0,'2016-09-21 09:33:17',20,NULL,NULL,0),(525,20,'2016-09-17',1,7,17.50,0.00,17.50,0.00,'',2,0,'2016-09-21 09:39:08',20,NULL,NULL,0),(526,20,'2016-09-17',1,7,17.50,0.00,17.50,0.00,'',2,0,'2016-09-21 09:40:56',20,NULL,NULL,0),(527,93,'2016-09-18',1,7,53.00,0.00,53.00,0.00,'',2,0,'2016-09-21 11:55:25',20,NULL,NULL,0),(528,20,'2016-09-18',2,7,13.40,0.00,13.40,0.00,'',2,0,'2016-09-21 16:41:02',20,NULL,NULL,0),(529,7,'2016-09-18',1,7,8.00,0.00,8.00,0.00,'',2,0,'2016-09-21 16:43:54',20,NULL,NULL,0),(530,43,'2016-09-18',1,7,8.90,0.00,8.90,0.00,'',2,0,'2016-09-21 16:48:28',20,NULL,NULL,0),(531,5,'2016-09-18',1,7,40.50,0.00,40.50,0.00,'',2,0,'2016-09-21 16:52:23',20,NULL,NULL,0),(532,10,'2016-09-18',1,7,2.00,0.00,2.00,0.00,'',2,0,'2016-09-21 16:54:48',20,NULL,NULL,0),(533,6,'2016-09-18',1,7,14.20,0.00,14.20,0.00,'',2,0,'2016-09-21 16:59:57',20,NULL,NULL,0),(534,17,'2016-09-18',1,7,6.00,0.00,6.00,0.00,'',2,0,'2016-09-21 17:02:02',20,NULL,NULL,0),(535,35,'2016-09-18',1,7,12.50,0.00,12.50,0.00,'',2,0,'2016-09-21 17:04:34',20,NULL,NULL,0),(536,39,'2016-09-18',1,7,16.50,0.00,16.50,0.00,'',2,0,'2016-09-21 17:08:53',20,NULL,NULL,0),(537,37,'2016-09-19',1,7,2.00,0.00,2.00,0.00,'',2,0,'2016-09-22 19:32:08',21,NULL,NULL,0),(538,19,'2016-09-20',1,7,25.00,0.00,25.00,0.00,'',2,0,'2016-09-22 19:35:36',21,NULL,NULL,0),(539,99,'2016-09-18',1,7,28.00,0.00,28.00,0.00,'',2,0,'2016-09-23 08:45:20',20,NULL,NULL,0),(540,2,'2016-09-19',1,7,11.60,0.00,11.60,0.00,'',2,0,'2016-09-23 08:50:07',20,NULL,NULL,0),(541,2,'2016-09-20',1,7,3.10,0.00,3.10,0.00,'',2,0,'2016-09-23 08:53:03',20,NULL,NULL,0),(542,2,'2016-09-22',1,7,35.00,0.00,35.00,0.00,'',2,0,'2016-09-23 08:55:11',20,NULL,NULL,0),(543,58,'2016-09-22',1,7,71.00,0.00,71.00,0.00,'',2,0,'2016-09-23 08:57:57',20,NULL,NULL,0),(544,7,'2016-09-20',2,7,2.00,0.00,2.00,0.00,'',2,0,'2016-09-23 16:57:24',21,NULL,NULL,0),(545,37,'2016-09-22',1,7,10.00,0.00,10.00,0.00,'',2,0,'2016-09-24 13:08:24',20,NULL,NULL,0),(546,2,'2016-09-23',1,7,9.90,0.00,9.90,0.00,'',2,0,'2016-09-24 13:12:57',20,NULL,NULL,0),(547,5,'2016-09-23',1,7,15.00,0.00,15.00,0.00,'',2,0,'2016-09-24 17:38:52',21,NULL,NULL,0),(548,65,'2016-09-24',1,7,10.00,0.00,10.00,0.00,'',2,0,'2016-09-24 17:40:15',21,NULL,NULL,0),(549,2,'2016-09-24',1,7,55.00,0.00,55.00,0.00,'',2,0,'2016-09-25 19:52:50',21,NULL,NULL,0),(550,7,'2016-09-25',1,7,7.00,0.00,7.00,0.00,'',2,0,'2016-09-25 19:58:00',21,NULL,NULL,0),(551,5,'2016-09-25',1,7,2.00,0.00,2.00,0.00,'',2,0,'2016-09-25 19:59:57',21,NULL,NULL,0),(552,2,'2016-09-25',1,7,10.50,0.00,10.50,0.00,'',2,0,'2016-09-25 20:01:29',21,NULL,NULL,0),(553,5,'2016-09-24',1,7,28.00,0.00,28.00,0.00,'',2,0,'2016-09-26 15:30:09',20,NULL,NULL,0),(554,17,'2016-09-24',1,7,5.00,0.00,5.00,0.00,'',2,0,'2016-09-26 15:32:01',20,NULL,NULL,0),(555,7,'2016-09-24',2,7,4.00,0.00,4.00,0.00,'',2,0,'2016-09-26 15:33:45',20,NULL,NULL,0),(556,20,'2016-09-24',1,7,3.20,0.00,3.20,0.00,'',2,0,'2016-09-26 15:36:29',20,NULL,NULL,0),(557,66,'2016-09-24',1,7,18.00,0.00,18.00,0.00,'',2,0,'2016-09-26 15:39:07',20,NULL,NULL,0),(558,9,'2016-09-25',1,7,51.50,0.00,51.50,0.00,'',2,0,'2016-09-26 15:42:48',20,NULL,NULL,0),(559,17,'2016-09-25',1,7,2.00,0.00,2.00,0.00,'',2,0,'2016-09-26 15:44:17',20,NULL,NULL,0),(560,95,'2016-09-25',1,7,47.00,0.00,47.00,0.00,'',2,0,'2016-09-26 15:46:55',20,NULL,NULL,0),(561,27,'2016-09-25',1,7,11.00,0.00,11.00,0.00,'',2,0,'2016-09-26 15:49:39',20,NULL,NULL,0),(562,20,'2016-09-25',1,7,9.00,0.00,9.00,0.00,'',2,0,'2016-09-26 15:51:30',20,NULL,NULL,0),(563,6,'2016-09-25',1,7,19.80,0.00,19.80,0.00,'',2,0,'2016-09-26 15:54:40',20,NULL,NULL,0),(564,10,'2016-09-25',1,7,2.80,0.00,2.80,0.00,'',2,0,'2016-09-26 15:56:22',20,NULL,NULL,0),(565,2,'2016-09-25',1,7,48.80,0.00,48.80,0.00,'',2,0,'2016-09-27 16:33:15',20,NULL,NULL,0),(566,100,'2016-09-22',1,7,43.40,0.00,43.40,0.00,'',2,0,'2016-09-28 14:10:34',21,NULL,NULL,0),(567,95,'2016-09-25',2,7,9.00,0.00,9.00,0.00,'',2,0,'2016-09-28 14:14:45',21,NULL,NULL,0),(568,21,'2016-09-27',1,7,6.50,0.00,6.50,0.00,'',2,0,'2016-09-28 14:22:43',21,NULL,NULL,0),(569,2,'2016-09-28',1,7,37.80,0.00,37.80,0.00,'',2,0,'2016-09-29 16:38:39',20,NULL,NULL,0),(570,37,'2016-10-01',2,7,6.00,0.00,6.00,0.00,'',2,0,'2016-10-03 17:42:39',21,NULL,NULL,0),(571,5,'2016-10-01',1,7,82.80,0.00,82.80,0.00,'',2,0,'2016-10-03 17:53:40',21,NULL,NULL,0),(572,95,'2016-10-01',1,7,60.00,0.00,60.00,0.00,'',2,0,'2016-10-03 17:58:06',21,NULL,NULL,0),(573,15,'2016-10-01',1,7,46.80,0.00,46.80,0.00,'',2,0,'2016-10-03 18:03:44',21,NULL,NULL,0),(574,7,'2016-10-01',1,7,2.00,0.00,2.00,0.00,'',2,0,'2016-10-03 18:07:04',21,NULL,NULL,0),(575,77,'2016-10-01',1,7,1.50,0.00,1.50,0.00,'',2,0,'2016-10-03 18:11:05',21,NULL,NULL,0),(576,10,'2016-10-01',1,7,10.20,0.00,10.20,0.00,'',2,0,'2016-10-03 18:14:41',21,NULL,NULL,0),(577,2,'2016-10-01',1,7,38.80,0.00,38.80,0.00,'',2,0,'2016-10-03 18:21:51',21,NULL,NULL,0),(578,5,'2016-10-01',1,7,28.00,0.00,28.00,0.00,'',2,0,'2016-10-03 18:24:24',21,NULL,NULL,0),(579,37,'2016-10-02',2,7,6.00,0.00,6.00,0.00,'',2,0,'2016-10-03 18:26:44',21,NULL,NULL,0),(580,15,'2016-10-02',1,7,49.20,0.00,49.20,0.00,'',2,0,'2016-10-03 18:37:53',21,NULL,NULL,0),(581,10,'2016-10-02',1,7,75.60,0.00,75.60,0.00,'',2,0,'2016-10-03 18:57:44',21,NULL,NULL,0),(582,62,'2016-10-02',1,7,3.50,0.00,3.50,0.00,'',2,0,'2016-10-03 18:59:47',21,NULL,NULL,0),(583,2,'2016-10-02',1,7,9.00,0.00,9.00,0.00,'',2,0,'2016-10-03 19:02:16',21,NULL,NULL,0),(584,99,'2016-09-25',1,7,3.50,0.00,3.50,0.00,'',2,0,'2016-10-03 19:07:18',21,NULL,NULL,0),(585,9,'2016-10-02',1,7,55.70,0.00,55.70,0.00,'',2,0,'2016-10-03 19:14:35',21,NULL,NULL,0),(586,2,'2016-10-02',1,7,23.40,0.00,23.40,0.00,'',2,0,'2016-10-03 19:23:53',21,NULL,NULL,0),(587,39,'2016-10-02',1,7,21.00,0.00,21.00,0.00,'',2,0,'2016-10-03 19:28:08',21,NULL,NULL,0),(588,7,'2016-10-02',1,7,13.00,0.00,13.00,0.00,'',2,0,'2016-10-03 19:31:13',21,NULL,NULL,0),(589,32,'2016-10-02',1,7,2.00,0.00,2.00,0.00,'',2,0,'2016-10-03 19:32:45',21,NULL,NULL,0),(590,97,'2016-10-02',1,7,12.00,0.00,12.00,0.00,'',2,0,'2016-10-03 19:36:04',21,NULL,NULL,0),(591,95,'2016-09-25',1,7,0.00,0.00,0.00,0.00,'',2,0,'2016-10-03 19:40:58',21,NULL,NULL,0),(592,95,'2016-10-02',1,7,15.50,0.00,15.50,0.00,'',2,0,'2016-10-03 19:57:50',21,NULL,NULL,0),(593,5,'2016-10-02',1,7,2.00,0.00,2.00,0.00,'',2,0,'2016-10-03 19:59:04',21,NULL,NULL,0),(594,65,'2016-10-02',2,7,10.00,0.00,10.00,0.00,'',2,0,'2016-10-03 20:01:57',21,NULL,NULL,0),(595,2,'2016-10-02',2,7,9.00,0.00,9.00,0.00,'',2,0,'2016-10-03 20:07:30',21,NULL,NULL,0),(596,2,'2016-10-02',1,7,22.00,0.00,22.00,0.00,'',2,0,'2016-10-03 20:12:52',21,NULL,NULL,0),(597,35,'2016-10-02',1,7,25.00,0.00,25.00,0.00,'',2,0,'2016-10-03 20:16:57',21,NULL,NULL,0),(598,6,'2016-10-02',1,7,14.20,0.00,14.20,0.00,'',2,0,'2016-10-03 20:20:30',21,NULL,NULL,0),(599,7,'2016-10-04',2,7,4.00,0.00,4.00,0.00,'',2,0,'2016-10-05 16:53:28',21,NULL,NULL,0),(600,2,'2016-10-04',1,7,18.00,0.00,18.00,0.00,'',2,0,'2016-10-05 16:56:28',21,NULL,NULL,0),(601,2,'2016-10-05',1,7,16.50,0.00,16.50,0.00,'',2,0,'2016-10-05 21:16:14',21,NULL,NULL,0),(602,2,'2016-10-06',1,7,20.00,0.00,20.00,0.00,'',2,0,'2016-10-07 18:11:41',21,NULL,NULL,0),(603,65,'2016-10-05',2,7,21.20,0.00,21.20,0.00,'',2,0,'2016-10-09 18:53:47',21,NULL,NULL,0),(604,2,'2016-10-08',1,7,46.10,0.00,46.10,0.00,'',2,0,'2016-10-09 18:59:13',21,NULL,NULL,0),(605,2,'2016-10-09',1,7,10.00,0.00,10.00,0.00,'',2,0,'2016-10-09 19:01:16',21,NULL,NULL,0),(606,2,'2016-09-30',1,7,41.50,0.00,41.50,0.00,'',2,0,'2016-10-10 18:01:03',20,NULL,NULL,0),(607,37,'2016-09-30',1,7,4.00,0.00,4.00,0.00,'',2,0,'2016-10-10 18:03:02',20,NULL,NULL,0),(608,38,'2016-10-01',1,7,20.00,0.00,20.00,0.00,'',2,0,'2016-10-10 18:05:08',20,NULL,NULL,0),(609,7,'2016-10-05',2,7,2.00,0.00,2.00,0.00,'',2,0,'2016-10-10 18:08:42',20,NULL,NULL,0),(610,2,'2016-10-07',1,7,13.00,0.00,13.00,0.00,'',2,0,'2016-10-10 18:13:51',20,NULL,NULL,0),(611,7,'2016-10-08',1,7,8.00,0.00,8.00,0.00,'',2,0,'2016-10-10 18:16:13',20,NULL,NULL,0),(612,2,'2016-10-08',1,7,3.00,0.00,3.00,0.00,'',2,0,'2016-10-10 18:18:44',20,NULL,NULL,0),(613,15,'2016-10-09',1,7,48.30,0.00,48.30,0.00,'',2,0,'2016-10-10 18:21:21',20,NULL,NULL,0),(614,95,'2016-10-09',1,7,96.00,0.00,96.00,0.00,'',2,0,'2016-10-10 18:26:13',20,NULL,NULL,0),(615,9,'2016-10-09',1,7,31.50,0.00,31.50,0.00,'',2,0,'2016-10-10 18:32:42',20,NULL,NULL,0),(616,17,'2016-10-09',1,7,2.00,0.00,2.00,0.00,'',2,0,'2016-10-10 18:33:56',20,NULL,NULL,0),(617,7,'2016-10-09',1,7,4.00,0.00,4.00,0.00,'',2,0,'2016-10-10 18:35:20',20,NULL,NULL,0),(618,6,'2016-10-09',1,7,31.50,0.00,31.50,0.00,'',2,0,'2016-10-10 18:40:41',20,NULL,NULL,0),(619,17,'2016-10-10',2,7,1.00,0.00,1.00,0.00,'',2,0,'2016-10-12 17:25:45',20,NULL,NULL,0),(620,2,'2016-10-10',1,7,18.00,0.00,18.00,0.00,'',2,0,'2016-10-12 17:28:03',20,NULL,NULL,0),(621,37,'2016-10-10',1,7,6.00,0.00,6.00,0.00,'',2,0,'2016-10-12 17:29:27',20,NULL,NULL,0),(622,35,'2016-10-11',1,7,32.50,0.00,32.50,0.00,'',2,0,'2016-10-12 17:33:19',20,NULL,NULL,0),(623,12,'2016-10-11',1,7,63.90,0.00,63.90,0.00,'',2,0,'2016-10-12 17:38:11',20,NULL,NULL,0),(624,5,'2016-10-11',1,7,59.20,0.00,59.20,0.00,'',2,0,'2016-10-12 17:43:01',20,NULL,NULL,0),(625,17,'2016-10-11',2,7,2.00,0.00,2.00,0.00,'',2,0,'2016-10-12 17:44:24',20,NULL,NULL,0),(626,18,'2016-10-11',2,7,7.50,0.00,7.50,0.00,'',2,0,'2016-10-12 17:47:53',20,NULL,NULL,0),(627,7,'2016-10-11',1,7,16.00,0.00,16.00,0.00,'',2,0,'2016-10-12 18:39:05',20,NULL,NULL,0),(628,14,'2016-10-11',1,7,44.00,0.00,44.00,0.00,'',2,0,'2016-10-12 18:44:10',20,NULL,NULL,0),(629,95,'2016-10-11',2,7,38.00,0.00,38.00,0.00,'',2,0,'2016-10-12 18:47:19',20,NULL,NULL,0),(630,42,'2016-10-11',2,7,19.00,0.00,19.00,0.00,'',2,0,'2016-10-12 18:49:30',20,NULL,NULL,0),(631,99,'2016-10-11',1,7,28.00,0.00,28.00,0.00,'',2,0,'2016-10-12 18:52:07',20,NULL,NULL,0),(632,2,'2016-10-01',1,7,33.50,0.00,33.50,0.00,'',2,0,'2016-10-13 10:29:54',21,NULL,NULL,0),(633,2,'2016-10-12',1,7,5.00,0.00,5.00,0.00,'',2,0,'2016-10-13 15:40:35',21,NULL,NULL,0),(634,5,'2016-10-13',1,7,23.00,0.00,23.00,0.00,'',2,0,'2016-10-13 15:42:22',21,NULL,NULL,0),(635,95,'2016-10-15',1,7,9.00,0.00,9.00,0.00,'',2,0,'2016-10-15 08:25:56',21,NULL,NULL,0),(636,2,'2016-10-13',1,7,52.90,0.00,52.90,0.00,'',2,0,'2016-10-16 03:13:51',20,NULL,NULL,0),(637,2,'2016-10-14',1,7,8.40,0.00,8.40,0.00,'',2,0,'2016-10-16 03:19:12',20,NULL,NULL,0),(638,2,'2016-10-15',1,7,21.80,0.00,21.80,0.00,'',2,0,'2016-10-17 17:32:19',21,NULL,NULL,0),(639,32,'2016-10-16',1,7,5.90,0.00,5.90,0.00,'',2,0,'2016-10-17 17:40:11',21,NULL,NULL,0),(640,95,'2016-10-16',1,7,26.00,0.00,26.00,0.00,'',2,0,'2016-10-17 18:00:19',21,NULL,NULL,0),(641,2,'2016-10-16',1,7,1.50,0.00,1.50,0.00,'',2,0,'2016-10-17 18:02:10',21,NULL,NULL,0),(642,30,'2016-10-17',1,7,2.00,0.00,2.00,0.00,'',2,0,'2016-10-17 18:03:52',21,NULL,NULL,0),(643,2,'2016-10-16',1,7,2.00,0.00,2.00,0.00,'',2,0,'2016-10-17 18:05:28',21,NULL,NULL,0),(644,15,'2016-10-16',1,7,107.80,0.00,107.80,0.00,'',2,0,'2016-10-17 18:18:26',21,NULL,NULL,0),(645,19,'2016-10-16',1,7,16.00,0.00,16.00,0.00,'',2,0,'2016-10-17 18:33:52',21,NULL,NULL,0),(646,9,'2016-10-16',1,7,23.00,0.00,23.00,0.00,'',2,0,'2016-10-17 19:02:45',21,NULL,NULL,0),(647,17,'2016-10-16',1,7,4.00,0.00,4.00,0.00,'',2,0,'2016-10-18 18:46:12',21,NULL,NULL,0),(648,42,'2016-10-16',1,7,13.00,0.00,13.00,0.00,'',2,0,'2016-10-18 18:54:17',21,NULL,NULL,0),(649,2,'2016-10-16',1,7,59.50,0.00,59.50,0.00,'',2,0,'2016-10-18 19:03:28',21,NULL,NULL,0),(650,6,'2016-10-16',1,7,10.40,0.00,10.40,0.00,'',2,0,'2016-10-18 19:07:09',21,NULL,NULL,0),(651,39,'2016-10-16',1,7,20.50,0.00,20.50,0.00,'',2,0,'2016-10-18 19:11:58',21,NULL,NULL,0),(652,19,'2016-10-17',1,7,5.00,0.00,5.00,0.00,'',2,0,'2016-10-18 19:13:52',21,NULL,NULL,0),(653,37,'2016-10-13',2,7,6.00,0.00,6.00,0.00,'',2,0,'2016-10-19 19:13:42',21,NULL,NULL,0),(654,65,'2016-10-17',1,7,60.50,0.00,60.50,0.00,'',2,0,'2016-10-19 19:16:37',21,NULL,NULL,0),(655,35,'2016-10-18',1,7,20.00,0.00,20.00,0.00,'',2,0,'2016-10-21 11:29:03',20,NULL,NULL,0),(656,2,'2016-10-19',1,7,20.70,0.00,20.70,0.00,'',2,0,'2016-10-21 11:32:21',20,NULL,NULL,0),(657,104,'2016-10-04',2,7,42.00,0.00,42.00,0.00,'',2,0,'2016-10-21 13:04:56',20,NULL,NULL,0),(658,2,'2016-10-21',1,7,4.20,0.00,4.20,0.00,'',2,0,'2016-10-24 14:12:43',20,NULL,NULL,0),(659,9,'2016-10-22',1,7,52.00,0.00,52.00,0.00,'',2,0,'2016-10-24 14:15:04',20,NULL,NULL,0),(660,62,'2016-10-22',1,7,5.70,0.00,5.70,0.00,'',2,0,'2016-10-24 14:17:03',20,NULL,NULL,0),(661,14,'2016-10-22',1,7,14.00,0.00,14.00,0.00,'',2,0,'2016-10-24 14:21:04',20,NULL,NULL,0),(662,20,'2016-10-22',1,7,2.00,0.00,2.00,0.00,'',2,0,'2016-10-24 14:23:58',20,NULL,NULL,0),(663,10,'2016-10-22',1,7,3.60,0.00,3.60,0.00,'',2,0,'2016-10-24 14:25:38',20,NULL,NULL,0),(664,10,'2016-10-23',1,7,4.30,0.00,4.30,0.00,'',2,0,'2016-10-24 14:30:01',20,NULL,NULL,0),(665,17,'2016-10-23',1,7,4.00,0.00,4.00,0.00,'',2,0,'2016-10-24 14:35:57',20,NULL,NULL,0),(666,95,'2016-10-23',1,7,27.00,0.00,27.00,0.00,'',2,0,'2016-10-24 14:41:24',20,NULL,NULL,0),(667,6,'2016-10-23',1,7,21.70,0.00,21.70,0.00,'',2,0,'2016-10-24 14:47:04',20,NULL,NULL,0),(668,65,'2016-10-19',2,7,23.00,0.00,23.00,0.00,'',2,0,'2016-10-25 12:07:09',21,NULL,NULL,0),(669,13,'2016-10-22',1,7,41.00,0.00,41.00,0.00,'',2,0,'2016-10-25 12:14:47',21,NULL,NULL,0),(670,17,'2016-10-22',1,7,2.00,0.00,2.00,0.00,'',2,0,'2016-10-25 12:18:17',21,NULL,NULL,0),(671,42,'2016-10-22',2,7,1.50,0.00,1.50,0.00,'',2,0,'2016-10-25 12:28:08',21,NULL,NULL,0),(672,9,'2016-10-23',1,7,31.90,0.00,31.90,0.00,'',2,0,'2016-10-25 17:41:34',20,NULL,NULL,0),(673,95,'2016-10-23',1,7,12.00,0.00,12.00,0.00,'',2,0,'2016-10-25 17:44:10',20,NULL,NULL,0),(674,65,'2016-10-24',2,7,23.60,0.00,23.60,0.00,'',2,0,'2016-10-25 17:46:22',20,NULL,NULL,0),(675,2,'2016-10-24',1,7,20.40,0.00,20.40,0.00,'',2,0,'2016-10-25 17:50:00',20,NULL,NULL,0),(676,10,'2016-10-24',2,7,30.00,0.00,30.00,0.00,'',2,0,'2016-10-25 17:52:09',20,NULL,NULL,0),(677,37,'2016-10-24',2,7,3.50,0.00,3.50,0.00,'',2,0,'2016-10-25 17:53:32',20,NULL,NULL,0),(678,105,'2016-10-23',1,7,44.30,0.00,44.30,0.00,'',2,0,'2016-10-27 18:55:08',20,NULL,NULL,0),(679,37,'2016-10-26',2,7,10.00,0.00,10.00,0.00,'',2,0,'2016-10-27 18:57:25',20,NULL,NULL,0),(680,65,'2016-10-26',2,7,20.00,0.00,20.00,0.00,'',2,0,'2016-10-27 18:58:47',20,NULL,NULL,0),(681,2,'2016-10-26',1,7,30.00,0.00,30.00,0.00,'',2,0,'2016-10-27 19:03:22',20,NULL,NULL,0),(682,37,'2016-10-27',2,7,6.00,0.00,6.00,0.00,'',2,0,'2016-10-28 14:55:33',21,NULL,NULL,0),(683,99,'2016-10-27',1,7,19.00,0.00,19.00,0.00,'',2,0,'2016-10-28 14:59:07',21,NULL,NULL,0),(684,2,'2016-10-27',1,7,13.60,0.00,13.60,0.00,'',2,0,'2016-10-28 15:06:30',21,NULL,NULL,0),(685,2,'2016-10-27',1,7,11.10,0.00,11.10,0.00,'',2,0,'2016-10-28 15:15:20',21,NULL,NULL,0),(686,62,'2016-10-29',1,7,3.00,0.00,3.00,0.00,'',2,0,'2016-10-30 11:33:53',21,NULL,NULL,0),(687,17,'2016-10-29',1,7,7.40,0.00,7.40,0.00,'',2,0,'2016-10-30 11:37:11',21,NULL,NULL,0),(688,2,'2016-10-29',1,7,15.00,0.00,15.00,0.00,'',2,0,'2016-10-30 11:39:04',21,NULL,NULL,0),(689,95,'2016-10-29',1,7,19.00,0.00,19.00,0.00,'',2,0,'2016-10-31 14:37:06',21,NULL,NULL,0),(690,15,'2016-10-31',1,7,4.00,0.00,4.00,0.00,'',2,0,'2016-10-31 14:40:51',21,NULL,NULL,0),(691,62,'2016-10-30',1,7,6.50,0.00,6.50,0.00,'',2,0,'2016-10-31 14:45:53',21,NULL,NULL,0),(692,24,'2016-10-30',1,7,6.50,0.00,6.50,0.00,'',2,0,'2016-10-31 15:00:46',21,NULL,NULL,0),(693,95,'2016-10-30',1,7,37.00,0.00,37.00,0.00,'',2,0,'2016-10-31 15:04:30',21,NULL,NULL,0),(694,17,'2016-10-30',2,7,18.00,0.00,18.00,0.00,'',2,0,'2016-10-31 21:22:43',21,NULL,NULL,0),(695,19,'2016-10-30',1,7,2.00,0.00,2.00,0.00,'',2,0,'2016-10-31 21:24:28',21,NULL,NULL,0),(696,32,'2016-10-30',1,7,3.50,0.00,3.50,0.00,'',2,0,'2016-10-31 21:26:16',21,NULL,NULL,0),(697,2,'2016-10-30',1,7,3.00,0.00,3.00,0.00,'',2,0,'2016-10-31 21:27:23',21,NULL,NULL,0),(698,2,'2016-10-30',1,7,36.00,0.00,36.00,0.00,'',2,0,'2016-10-31 21:32:18',21,NULL,NULL,0),(699,2,'2016-10-30',1,7,15.00,0.00,15.00,0.00,'',2,0,'2016-10-31 21:41:52',21,NULL,NULL,0),(700,19,'2016-11-26',2,7,10.00,0.00,10.00,0.00,'',2,0,'2016-11-01 12:18:52',20,NULL,NULL,0),(701,2,'2016-10-29',1,7,4.40,0.00,4.40,0.00,'',2,0,'2016-11-01 12:25:31',20,NULL,NULL,0),(702,2,'2016-10-29',1,7,47.60,0.00,47.60,0.00,'',2,0,'2016-11-01 12:34:01',20,NULL,NULL,0),(703,2,'2016-10-31',1,7,33.20,0.00,33.20,0.00,'',2,0,'2016-11-03 18:33:15',21,NULL,NULL,0),(704,65,'2016-10-31',2,7,23.00,0.00,23.00,0.00,'',2,0,'2016-11-03 18:36:25',21,NULL,NULL,0),(705,46,'2016-10-31',1,7,20.00,0.00,20.00,0.00,'',2,0,'2016-11-03 18:38:02',21,NULL,NULL,0),(706,65,'2016-11-01',2,7,26.50,0.00,26.50,0.00,'',2,0,'2016-11-03 18:41:59',21,NULL,NULL,0),(707,2,'2016-11-03',1,7,7.00,0.00,7.00,0.00,'',2,0,'2016-11-04 17:55:01',21,NULL,NULL,0),(708,65,'2016-11-04',1,7,13.00,0.00,13.00,0.00,'',2,0,'2016-11-04 21:11:08',21,NULL,NULL,0),(709,3,'2016-12-30',1,NULL,500.00,0.00,500.00,0.00,'Prueba',1,0,'2016-12-30 19:14:01',1,20.00,'0001',1),(710,3,'2016-12-30',1,7,360.00,0.00,360.00,0.00,'Probando',1,1,'2016-12-30 21:57:22',1,20.00,'0002',1),(711,3,'2016-12-30',1,7,530.00,0.00,530.00,0.00,'pruebitaaas',1,1,'2016-12-30 22:06:22',1,19.00,'0003',1),(712,2,'2017-01-17',1,7,5000.00,0.00,5000.00,0.00,'',1,1,'2017-01-17 00:44:00',1,350.00,'12',1),(713,2,'2017-01-17',1,7,14140.00,2545.20,16685.20,0.00,'',1,1,'2017-01-17 15:30:29',1,360.00,'46',1),(714,2,'2017-01-17',1,7,5000.00,900.00,5900.00,0.00,'',1,1,'2017-01-17 15:57:00',1,0.00,'51',1),(715,2,'2017-01-17',1,7,240.00,43.20,283.20,0.00,'',1,1,'2017-01-17 21:41:43',1,0.00,'45',1),(722,3,'2017-01-17',1,7,5824.00,1048.32,6872.32,0.00,'',1,1,'2017-01-17 22:53:38',1,0.00,'900',1),(723,3,'2017-01-17',1,7,5824.00,1048.32,6872.32,0.00,'',1,1,'2017-01-17 22:54:18',1,0.00,'900',1),(724,3,'2017-01-17',1,7,5824.00,1048.32,6872.32,0.00,'',1,1,'2017-01-17 22:54:52',1,0.00,'900',1),(725,2,'2017-01-17',1,7,400.00,72.00,472.00,0.00,'',1,1,'2017-01-17 22:58:08',1,0.00,'900',1),(726,2,'2017-01-17',1,7,640.00,115.20,755.20,0.00,'',1,1,'2017-01-17 22:58:43',1,0.00,'900',1),(727,2,'2017-01-17',1,7,684.00,123.12,807.12,0.00,'prueba',1,1,'2017-01-17 23:16:41',1,0.00,'900',1),(728,2,'2017-01-18',1,7,316.00,56.88,372.88,0.00,'',1,1,'2017-01-18 00:21:14',1,0.00,'800',1),(729,2,'2017-01-18',1,7,1200.00,216.00,1416.00,416.00,'',1,1,'2017-01-18 11:14:11',1,1000.00,'800',1),(730,2,'2017-01-18',1,7,30000.00,5400.00,35400.00,29400.00,'                                                                                           ',1,1,'2017-01-20 21:07:53',1,6000.00,'800',1),(731,2,'2017-01-18',1,7,27000.00,4860.00,31860.00,31660.00,'',1,1,'2017-01-18 11:57:20',1,200.00,'800',1),(732,2,'2017-01-18',1,7,30000.00,5400.00,35400.00,27400.00,'',1,0,'2017-01-18 12:01:41',1,8000.00,'800',1),(733,2,'2017-01-20',1,7,144.00,25.92,169.92,99.92,'                                                                                           ',1,0,'2017-01-20 01:58:13',1,70.00,'123',1),(734,2,'2017-01-20',1,7,7000.00,1260.00,8260.00,2260.00,'                                                                                           ',1,1,'2017-01-20 02:26:54',1,6000.00,'95',1),(735,2,'2017-01-20',1,7,300000.00,54000.00,354000.00,354000.00,'                                                                                                                                                                                      ',1,0,'2017-01-20 17:05:51',1,6000.00,'95',1),(736,2,'2017-01-17',1,7,640.00,115.20,755.20,705.20,'                                                                                           ',1,0,'2017-01-20 17:07:33',1,50.00,'900',1),(737,2,'2017-01-20',1,7,0.00,0.00,0.00,0.00,'                                                                                                                                                                                                        ',1,1,'2017-01-20 17:26:51',1,6000.00,'95',1),(738,2,'2017-01-17',1,7,400.00,72.00,472.00,472.00,'                                                                                                                                                                                      ',1,1,'2017-01-20 17:34:40',1,50.00,'900',1),(739,2,'2017-01-17',1,7,30000.00,5400.00,35400.00,34400.00,'                                                                                                                                                                                                        ',1,1,'2017-01-20 23:47:22',1,1000.00,'900',1),(740,2,'2017-01-17',1,7,400.00,72.00,472.00,472.00,'                                                                                                                                                                                                        ',1,1,'2017-01-20 17:36:49',1,50.00,'900',1),(741,2,'2017-01-17',1,7,400.00,72.00,472.00,472.00,'                                                                                                                                                                                                        ',1,1,'2017-01-20 17:41:07',1,50.00,'900',1),(742,2,'2017-01-17',1,7,640.00,115.20,755.20,755.20,'                                                                                                                                                                                                        ',1,1,'2017-01-20 17:45:42',1,50.00,'900',1),(743,2,'2017-01-17',1,7,400.00,72.00,472.00,472.00,'                                                                                                                                                                                                        ',1,1,'2017-01-20 17:47:41',1,50.00,'900',1),(744,2,'2017-01-17',1,7,400.00,72.00,472.00,372.00,'                                                                                                                                                                                                        ',1,1,'2017-01-20 17:55:23',1,100.00,'900',1),(745,2,'2017-01-17',1,7,400.00,72.00,472.00,372.00,'                                                                                                                                                                                                        ',1,1,'2017-01-20 17:57:07',1,100.00,'900',1),(746,2,'2017-01-17',1,7,400.00,72.00,472.00,372.00,'                                                                                                                                                                                                        ',1,1,'2017-01-20 17:58:36',1,100.00,'900',1),(747,2,'2017-01-17',1,7,400.00,72.00,472.00,372.00,'                                                                                                                                                                                                        ',1,1,'2017-01-20 17:59:39',1,100.00,'900',1),(748,2,'2017-01-17',1,7,400.00,72.00,472.00,372.00,'                                                                                                                                                                                                        ',1,1,'2017-01-20 18:14:28',1,100.00,'900',1),(749,2,'2017-01-17',1,7,400.00,72.00,472.00,372.00,'                                                                                                                                                                                                        ',1,1,'2017-01-20 18:15:03',1,100.00,'900',1),(750,2,'2017-01-20',1,7,400.00,72.00,472.00,272.00,'....',1,1,'2017-01-20 18:57:01',1,200.00,'900',1),(751,2,'2017-01-20',1,7,0.00,0.00,0.00,-110.00,'                                                                                                                                                                                                        ',1,1,'2017-01-20 20:46:22',1,110.00,'800',1),(753,2,'2017-01-21',1,7,9000.00,1620.00,10620.00,10620.00,'                                                                                           ',1,0,'2017-01-21 00:40:31',1,0.00,'103',1),(754,2,'2017-01-22',1,7,6000.00,1080.00,7080.00,7080.00,'                                                                                           ',2,0,'2017-01-22 00:12:09',1,0.00,'1000',1),(755,2,'2017-01-22',1,7,50.00,9.00,59.00,59.00,'                                                                                           ',1,0,'2017-01-22 00:14:07',1,0.00,'1001',1),(756,2,'2017-01-22',1,7,610.00,109.80,719.80,719.80,'                                                                                           ',1,0,'2017-01-22 00:21:16',1,0.00,'1003',1),(757,2,'2017-01-22',1,7,1000.00,180.00,1180.00,1180.00,'                                                                                           ',1,0,'2017-01-22 00:34:27',1,0.00,'1004',1),(758,2,'2017-01-22',1,7,0.00,0.00,0.00,0.00,'                                                                                                                                                                                      ',1,1,'2017-01-22 00:38:21',1,0.00,'10005',1),(759,2,'2017-01-22',1,7,0.00,0.00,0.00,0.00,'                                                                                           ',1,1,'2017-01-22 00:42:46',1,0.00,'999',1),(760,2,'2017-01-22',1,7,240.00,43.20,283.20,283.20,'                                                                                           ',1,0,'2017-01-22 00:49:45',1,0.00,'998',1),(761,2,'2017-01-23',1,7,6000.00,1080.00,7080.00,7080.00,'                                                                                           ',1,0,'2017-01-23 09:43:27',1,0.00,'3',2),(762,2,'2017-01-24',2,7,200.00,36.00,236.00,0.00,'                                                                                                                                                                                                        ',1,0,'2017-01-24 14:09:18',1,236.00,'10',2),(763,2,'2017-01-27',1,7,37260.00,6706.80,43966.80,43966.80,'                                                                                           ',2,0,'2017-01-27 10:56:20',1,0.00,'2000',1),(764,2,'2017-01-28',1,7,300.00,80.00,380.00,380.00,'..',2,0,'2017-01-28 10:22:30',1,0.00,'9090',1),(765,2,'2017-01-28',1,7,450.00,81.00,531.00,531.00,'                                                                                                                                                                                      ',1,0,'2017-01-28 21:44:55',1,0.00,'90900',1),(766,2,'2017-01-30',1,7,0.00,0.00,0.00,0.00,'                                                                                           ',1,0,'2017-01-30 13:20:46',1,0.00,'900000',1),(767,2,'2017-01-30',1,7,0.00,0.00,0.00,0.00,'                                                                                           ',1,1,'2017-01-30 13:23:29',1,0.00,'9000',1),(768,2,'2017-01-31',1,7,0.00,0.00,0.00,0.00,'                                                                                           ',2,1,'2017-01-30 13:26:36',1,0.00,'9000',1),(769,2,'2017-01-30',1,7,1205.00,216.90,1421.90,1421.90,'                                                                                           ',1,1,'2017-01-30 14:54:21',1,0.00,'001-1',1),(770,2,'2017-01-30',1,7,200.00,36.00,236.00,236.00,'                                                                                           ',2,1,'2017-01-30 15:00:59',1,0.00,'1234',1),(771,0,'2017-02-19',1,7,100.00,18.00,118.00,118.00,'                                                                                           ',1,0,'2017-02-18 11:00:52',1,0.00,'6666',1),(772,2,'2017-02-11',1,7,14400.00,2592.00,16992.00,16992.00,'                                                                                           ',1,0,'2017-02-18 11:01:37',1,0.00,'6666',1);
/*!40000 ALTER TABLE `pedido` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_detalle`
--

DROP TABLE IF EXISTS `pedido_detalle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_detalle` (
  `Pedidodetalle_Id` int(11) NOT NULL AUTO_INCREMENT,
  `Pedido_Id` int(11) NOT NULL,
  `ProdServ_Id` int(11) NOT NULL,
  `UnidadMedida_Id` int(11) NOT NULL,
  `nPedDetCantidad` int(11) NOT NULL,
  `dPedDetPrecioUnitario` decimal(10,2) NOT NULL,
  `dPedDetSubtotal` decimal(10,2) NOT NULL,
  PRIMARY KEY (`Pedidodetalle_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2155 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_detalle`
--

LOCK TABLES `pedido_detalle` WRITE;
/*!40000 ALTER TABLE `pedido_detalle` DISABLE KEYS */;
INSERT INTO `pedido_detalle` VALUES (1,1,12,2,2,5.00,10.00),(4,3,39,5,2,7.00,14.00),(7,2,12,2,2,5.00,10.00),(8,4,44,2,4,1.50,6.00),(9,4,62,2,1,0.60,0.60),(10,4,112,2,1,0.60,0.60),(11,4,8,2,6,5.00,30.00),(12,5,53,4,1,4.00,4.00),(13,6,52,4,1,4.00,4.00),(14,7,15,12,5,5.00,25.00),(15,7,53,4,5,4.00,20.00),(16,7,93,26,5,1.00,5.00),(18,8,54,2,1,2.00,2.00),(19,9,54,2,1,2.00,2.00),(20,10,11,2,2,5.00,10.00),(29,12,15,12,1,5.00,5.00),(30,12,53,4,1,4.00,4.00),(31,13,59,2,2,1.20,2.40),(32,14,42,5,1,5.00,5.00),(47,16,11,2,2,5.00,10.00),(48,16,68,2,1,0.50,0.50),(49,17,93,26,1,1.00,1.00),(50,17,47,2,1,1.50,1.50),(51,17,87,2,1,1.20,1.20),(52,17,44,2,1,1.50,1.50),(53,17,54,2,1,2.00,2.00),(54,17,44,2,1,1.50,1.50),(67,18,8,2,2,5.00,10.00),(68,18,87,2,4,1.20,4.80),(69,18,61,2,1,1.20,1.20),(70,18,58,2,1,1.20,1.20),(71,18,55,2,1,2.00,2.00),(72,18,8,2,1,5.00,5.00),(73,18,87,2,3,1.20,3.60),(74,18,61,2,2,1.20,2.40),(75,18,58,2,1,1.20,1.20),(76,18,75,2,2,1.50,3.00),(77,18,8,2,1,5.00,5.00),(78,18,92,2,1,2.00,2.00),(79,19,50,2,1,2.00,2.00),(80,19,55,2,1,2.00,2.00),(81,19,92,2,1,2.00,2.00),(82,19,50,2,1,2.00,2.00),(85,20,44,2,1,1.50,1.50),(86,20,44,2,1,1.50,1.50),(92,22,52,4,1,4.00,4.00),(93,22,50,2,1,2.00,2.00),(94,22,52,4,1,4.00,4.00),(97,23,92,2,3,2.00,6.00),(98,23,54,2,1,2.00,2.00),(106,25,8,2,2,5.00,10.00),(107,25,8,2,2,5.00,10.00),(108,25,8,2,2,5.00,10.00),(112,26,15,12,2,5.00,10.00),(113,26,53,4,2,4.00,8.00),(114,26,54,2,2,2.00,4.00),(119,27,15,12,1,5.00,5.00),(120,27,53,4,1,4.00,4.00),(121,27,93,26,2,1.00,2.00),(122,27,8,2,1,5.00,5.00),(124,28,47,2,1,1.50,1.50),(127,29,50,2,1,2.00,2.00),(128,29,54,2,1,2.00,2.00),(135,30,44,2,2,1.50,3.00),(136,30,66,2,3,0.60,1.80),(137,30,69,2,2,0.60,1.20),(138,30,93,26,1,1.00,1.00),(139,30,102,2,1,0.60,0.60),(140,30,75,2,1,1.50,1.50),(142,31,54,2,2,2.00,4.00),(144,32,54,2,1,2.00,2.00),(147,33,11,2,4,5.00,20.00),(148,33,11,2,3,5.00,15.00),(152,34,6,2,1,6.00,6.00),(153,34,87,2,1,1.20,1.20),(154,34,63,2,1,0.60,0.60),(171,35,11,2,8,5.00,40.00),(172,35,13,2,8,5.00,40.00),(173,35,12,2,1,5.00,5.00),(174,35,15,12,12,5.00,60.00),(175,35,53,4,6,4.00,24.00),(176,35,80,4,1,3.50,3.50),(177,35,15,12,4,5.00,20.00),(178,35,53,4,3,4.00,12.00),(179,35,29,13,1,18.00,18.00),(180,35,54,2,6,2.00,12.00),(181,35,55,2,1,2.00,2.00),(182,35,15,12,5,5.00,25.00),(183,35,53,4,4,4.00,16.00),(212,37,44,2,3,1.50,4.50),(213,37,47,2,3,1.50,4.50),(214,37,116,2,2,1.50,3.00),(215,37,45,2,1,1.50,1.50),(216,37,15,12,8,5.00,40.00),(217,37,45,2,1,1.50,1.50),(223,38,11,2,1,5.00,5.00),(224,38,54,2,1,2.00,2.00),(225,38,15,12,1,5.00,5.00),(226,38,53,4,1,4.00,4.00),(227,38,116,2,1,1.50,1.50),(234,39,44,2,3,1.50,4.50),(235,39,47,2,3,1.50,4.50),(236,39,116,2,2,1.50,3.00),(237,39,45,2,1,1.50,1.50),(238,39,15,12,8,5.00,40.00),(239,39,45,2,1,1.50,1.50),(240,40,54,2,2,2.00,4.00),(241,41,11,2,1,5.00,5.00),(242,41,11,2,1,5.00,5.00),(243,41,11,2,2,5.00,10.00),(244,41,11,2,2,5.00,10.00),(245,41,11,2,1,5.00,5.00),(246,41,12,2,2,5.00,10.00),(247,41,11,2,1,5.00,5.00),(248,42,54,2,2,2.00,4.00),(249,42,54,2,2,2.00,4.00),(250,42,11,2,1,5.00,5.00),(251,42,8,2,1,5.00,5.00),(252,42,54,2,1,2.00,2.00),(253,42,8,2,1,5.00,5.00),(254,42,54,2,1,2.00,2.00),(255,43,54,2,1,2.00,2.00),(256,43,55,2,1,2.00,2.00),(257,43,50,2,1,2.00,2.00),(258,44,61,2,1,1.20,1.20),(259,44,59,2,1,1.20,1.20),(260,45,11,2,1,5.00,5.00),(261,46,50,2,1,2.00,2.00),(262,46,75,2,2,1.50,3.00),(263,47,53,4,1,4.00,4.00),(264,47,50,2,1,2.00,2.00),(265,47,53,4,1,4.00,4.00),(266,48,15,12,1,5.00,5.00),(267,48,53,4,1,4.00,4.00),(268,49,53,4,1,4.00,4.00),(269,49,53,4,1,4.00,4.00),(270,50,53,4,1,4.00,4.00),(271,51,115,3,1,38.00,38.00),(272,51,13,2,1,5.00,5.00),(273,51,12,2,2,5.00,10.00),(274,51,59,2,2,1.20,2.40),(275,52,15,12,2,5.00,10.00),(276,52,53,4,1,4.00,4.00),(277,52,15,12,2,5.00,10.00),(278,52,87,2,2,1.20,2.40),(279,52,15,12,1,5.00,5.00),(280,52,53,4,1,4.00,4.00),(281,52,55,2,1,2.00,2.00),(282,53,54,2,1,2.00,2.00),(283,53,55,2,1,2.00,2.00),(284,53,50,2,1,2.00,2.00),(285,53,55,2,1,2.00,2.00),(286,53,92,2,1,2.00,2.00),(287,54,50,2,1,2.00,2.00),(288,55,12,2,4,5.00,20.00),(289,55,63,2,4,0.60,2.40),(290,55,12,2,3,5.00,15.00),(291,55,54,2,1,2.00,2.00),(292,55,55,2,1,2.00,2.00),(293,55,80,4,1,3.50,3.50),(294,55,40,5,1,5.00,5.00),(295,55,50,2,2,2.00,4.00),(296,55,55,2,1,2.00,2.00),(297,55,54,2,1,2.00,2.00),(298,55,116,2,1,1.50,1.50),(299,55,47,2,1,1.50,1.50),(300,55,22,5,1,5.00,5.00),(301,56,59,2,3,1.20,3.60),(302,56,50,2,1,2.00,2.00),(303,56,44,2,1,1.50,1.50),(304,57,53,4,3,4.00,12.00),(305,57,15,12,1,5.00,5.00),(306,57,44,2,1,1.50,1.50),(307,58,92,2,5,2.00,10.00),(308,58,54,2,1,2.00,2.00),(309,59,69,2,8,0.60,4.80),(310,59,44,2,1,1.50,1.50),(311,60,15,12,1,5.00,5.00),(312,60,53,4,1,4.00,4.00),(313,61,47,2,1,1.50,1.50),(314,61,54,2,1,2.00,2.00),(315,62,87,2,2,1.20,2.40),(316,62,5,5,1,7.50,7.50),(317,62,54,2,1,2.00,2.00),(318,62,5,5,1,7.50,7.50),(319,62,55,2,1,2.00,2.00),(320,62,75,2,2,1.50,3.00),(321,62,53,4,1,4.00,4.00),(322,63,92,2,1,2.00,2.00),(323,63,75,2,1,1.50,1.50),(324,64,54,2,1,2.00,2.00),(325,65,115,3,1,38.00,38.00),(326,66,44,2,1,1.50,1.50),(327,66,54,2,1,2.00,2.00),(328,66,68,2,1,0.50,0.50),(329,67,54,2,2,2.00,4.00),(330,67,44,2,1,1.50,1.50),(331,68,68,2,1,0.50,0.50),(332,68,54,2,1,2.00,2.00),(333,69,8,2,21,5.00,105.00),(334,69,11,2,9,5.00,45.00),(335,69,12,2,1,5.00,5.00),(336,69,54,2,4,2.00,8.00),(337,69,54,2,4,2.00,8.00),(338,69,54,2,3,2.00,6.00),(339,70,54,2,1,2.00,2.00),(340,70,63,2,1,0.60,0.60),(341,70,54,2,1,2.00,2.00),(342,70,62,2,1,0.60,0.60),(343,70,54,2,2,2.00,4.00),(344,70,58,2,2,1.20,2.40),(345,70,63,2,4,0.60,2.40),(346,70,63,2,1,0.60,0.60),(347,71,54,2,1,2.00,2.00),(348,72,8,2,1,5.00,5.00),(349,72,15,12,2,5.00,10.00),(350,72,53,4,1,4.00,4.00),(351,72,8,2,1,5.00,5.00),(352,73,87,2,2,1.20,2.40),(353,74,11,2,2,5.00,10.00),(354,74,15,12,1,5.00,5.00),(355,74,53,4,1,4.00,4.00),(356,74,93,26,2,1.00,2.00),(357,74,64,2,1,0.60,0.60),(358,75,116,2,2,1.50,3.00),(359,75,50,2,2,2.00,4.00),(360,75,54,2,1,2.00,2.00),(361,76,80,4,6,3.50,21.00),(362,76,53,4,6,4.00,24.00),(363,77,11,2,1,5.00,5.00),(364,77,54,2,1,2.00,2.00),(365,77,116,2,1,1.50,1.50),(366,78,8,2,1,5.00,5.00),(367,78,53,4,1,4.00,4.00),(368,78,54,2,1,2.00,2.00),(371,79,8,2,1,5.00,5.00),(372,79,116,2,1,1.50,1.50),(373,80,54,2,1,2.00,2.00),(374,80,8,2,1,5.00,5.00),(375,81,52,4,1,4.00,4.00),(376,81,50,2,1,2.00,2.00),(377,81,52,4,1,4.00,4.00),(378,82,50,2,1,2.00,2.00),(379,82,116,2,1,1.50,1.50),(380,82,70,2,1,0.60,0.60),(381,83,44,2,4,1.50,6.00),(382,83,47,2,1,1.50,1.50),(383,83,116,2,1,1.50,1.50),(384,83,15,12,2,5.00,10.00),(385,83,53,4,2,4.00,8.00),(386,83,54,2,1,2.00,2.00),(387,84,92,2,2,2.00,4.00),(388,84,116,2,1,1.50,1.50),(389,85,44,2,3,1.50,4.50),(390,85,45,2,3,1.50,4.50),(391,85,47,2,1,1.50,1.50),(392,85,54,2,1,2.00,2.00),(393,85,116,2,1,1.50,1.50),(394,85,92,2,1,2.00,2.00),(395,86,116,2,2,1.50,3.00),(396,86,44,2,1,1.50,1.50),(397,86,47,2,1,1.50,1.50),(398,87,50,2,1,2.00,2.00),(399,87,44,2,1,1.50,1.50),(400,88,8,2,8,5.00,40.00),(401,88,54,2,4,2.00,8.00),(402,88,50,2,2,2.00,4.00),(403,88,44,2,2,1.50,3.00),(404,88,116,2,1,1.50,1.50),(405,88,68,2,1,0.50,0.50),(406,89,8,2,2,5.00,10.00),(407,89,11,2,6,5.00,30.00),(408,89,55,2,1,2.00,2.00),(409,90,11,2,2,5.00,10.00),(410,91,15,12,3,5.00,15.00),(411,91,53,4,1,4.00,4.00),(412,92,53,4,8,4.00,32.00),(413,92,15,12,10,5.00,50.00),(414,93,54,2,2,2.00,4.00),(415,93,62,2,2,0.60,1.20),(416,94,15,12,4,5.00,20.00),(417,94,53,4,2,4.00,8.00),(418,95,44,2,2,1.50,3.00),(419,95,45,2,1,1.50,1.50),(420,95,64,2,2,0.60,1.20),(421,96,83,13,1,18.00,18.00),(422,96,61,2,1,1.20,1.20),(423,96,87,2,1,1.20,1.20),(424,96,63,2,1,0.60,0.60),(425,97,11,2,2,5.00,10.00),(426,97,116,2,2,1.50,3.00),(427,97,53,4,1,4.00,4.00),(428,97,44,2,1,1.50,1.50),(429,97,92,2,1,2.00,2.00),(430,98,87,2,2,1.20,2.40),(431,98,53,4,2,4.00,8.00),(432,98,50,2,1,2.00,2.00),(433,98,92,2,1,2.00,2.00),(434,98,75,2,2,1.50,3.00),(435,99,50,2,1,2.00,2.00),(436,99,54,2,2,2.00,4.00),(437,99,92,2,1,2.00,2.00),(438,100,93,26,2,1.00,2.00),(439,100,116,2,1,1.50,1.50),(440,101,50,2,1,2.00,2.00),(441,102,53,4,1,4.00,4.00),(442,103,44,2,1,1.50,1.50),(443,104,50,2,1,2.00,2.00),(444,105,40,5,1,5.00,5.00),(445,105,39,5,2,5.00,10.00),(446,106,54,2,1,2.00,2.00),(447,106,2,5,3,7.50,22.50),(448,106,50,2,1,2.00,2.00),(449,106,63,2,1,0.60,0.60),(450,106,93,26,2,1.00,2.00),(451,106,62,2,1,0.60,0.60),(452,106,65,2,1,0.60,0.60),(453,106,65,2,2,0.60,1.20),(454,107,54,2,1,2.00,2.00),(455,107,15,12,2,5.00,10.00),(456,107,53,4,1,4.00,4.00),(457,108,96,24,2,5.00,10.00),(458,108,54,2,1,2.00,2.00),(459,109,5,5,11,7.50,82.50),(460,109,8,2,3,5.00,15.00),(461,109,87,2,3,1.20,3.60),(462,109,59,2,2,1.20,2.40),(463,109,58,2,1,1.20,1.20),(464,109,117,2,1,2.20,2.20),(465,109,61,2,2,1.20,2.40),(466,109,54,2,1,2.00,2.00),(467,110,44,2,1,1.50,1.50),(468,110,53,4,1,4.00,4.00),(469,110,45,2,1,1.50,1.50),(470,111,45,2,1,1.50,1.50),(471,112,44,2,2,1.50,3.00),(472,113,55,2,3,2.00,6.00),(473,113,92,2,2,2.00,4.00),(474,114,11,2,1,5.00,5.00),(475,114,92,2,1,2.00,2.00),(476,114,54,2,2,2.00,4.00),(477,115,8,2,2,5.00,10.00),(478,115,15,12,2,5.00,10.00),(479,115,53,4,1,4.00,4.00),(480,115,116,2,3,1.50,4.50),(481,115,57,2,3,2.20,6.60),(482,115,45,2,1,1.50,1.50),(483,116,44,2,2,1.50,3.00),(484,116,92,2,1,2.00,2.00),(485,117,8,2,1,5.00,5.00),(486,117,12,2,1,5.00,5.00),(487,117,87,2,1,1.20,1.20),(488,118,8,2,1,5.00,5.00),(489,118,54,2,1,2.00,2.00),(490,118,44,2,1,1.50,1.50),(491,118,92,2,1,2.00,2.00),(492,119,55,2,1,2.00,2.00),(493,119,75,2,1,1.50,1.50),(494,119,92,2,1,2.00,2.00),(495,120,11,2,1,5.00,5.00),(496,120,54,2,1,2.00,2.00),(497,120,74,2,1,0.40,0.40),(498,121,42,5,1,5.00,5.00),(499,121,15,12,2,5.00,10.00),(500,121,92,2,1,2.00,2.00),(501,121,42,5,1,5.00,5.00),(502,122,54,2,1,2.00,2.00),(503,123,74,2,2,0.40,0.80),(504,124,55,2,1,2.00,2.00),(505,125,15,12,2,5.00,10.00),(506,125,53,4,2,4.00,8.00),(507,125,116,2,1,1.50,1.50),(508,126,11,2,3,5.00,15.00),(509,126,15,12,2,5.00,10.00),(510,126,53,4,2,4.00,8.00),(511,127,54,2,1,2.00,2.00),(512,127,116,2,1,1.50,1.50),(513,127,8,2,1,5.00,5.00),(514,128,55,2,1,2.00,2.00),(515,128,53,4,2,4.00,8.00),(516,128,59,2,4,1.20,4.80),(517,128,58,2,2,1.20,2.40),(518,128,75,2,2,1.50,3.00),(519,129,18,5,2,6.50,13.00),(520,129,44,2,3,1.50,4.50),(521,129,59,2,2,1.20,2.40),(522,130,45,2,3,1.50,4.50),(523,130,47,2,2,1.50,3.00),(524,130,63,2,1,0.60,0.60),(525,130,116,2,1,1.50,1.50),(526,130,64,2,1,0.60,0.60),(527,131,8,2,1,5.00,5.00),(528,131,59,2,2,1.20,2.40),(529,131,44,2,2,1.50,3.00),(530,131,92,2,1,2.00,2.00),(531,132,50,2,1,2.00,2.00),(532,132,92,2,1,2.00,2.00),(533,132,54,2,1,2.00,2.00),(534,133,44,2,3,1.50,4.50),(535,133,50,2,1,2.00,2.00),(536,133,55,2,1,2.00,2.00),(537,133,54,2,1,2.00,2.00),(538,134,50,2,1,2.00,2.00),(539,135,54,2,1,2.00,2.00),(540,136,22,5,1,5.00,5.00),(541,136,116,2,1,1.50,1.50),(542,137,11,2,1,5.00,5.00),(543,137,53,4,1,4.00,4.00),(544,137,116,2,1,1.50,1.50),(545,137,54,2,1,2.00,2.00),(546,137,92,2,1,2.00,2.00),(547,138,69,2,1,0.60,0.60),(548,138,70,2,1,0.60,0.60),(549,138,63,2,1,0.60,0.60),(550,138,59,2,1,1.20,1.20),(551,139,12,2,4,5.00,20.00),(552,140,12,2,3,5.00,15.00),(553,141,44,2,2,1.50,3.00),(554,141,45,2,1,1.50,1.50),(555,142,12,2,3,5.00,15.00),(556,142,47,2,1,1.50,1.50),(557,142,44,2,1,1.50,1.50),(558,143,54,2,1,2.00,2.00),(559,143,11,2,3,5.00,15.00),(560,144,54,2,1,2.00,2.00),(561,145,44,2,1,1.50,1.50),(562,146,44,2,1,1.50,1.50),(563,146,87,2,1,1.20,1.20),(564,146,59,2,1,1.20,1.20),(565,147,45,2,1,1.50,1.50),(566,147,61,2,1,1.20,1.20),(567,147,59,2,1,1.20,1.20),(568,147,54,2,1,2.00,2.00),(569,148,45,2,2,1.50,3.00),(570,148,47,2,1,1.50,1.50),(571,149,45,2,2,1.50,3.00),(572,149,63,2,1,0.60,0.60),(573,149,44,2,3,1.50,4.50),(574,150,53,4,2,4.00,8.00),(575,150,116,2,1,1.50,1.50),(576,150,87,2,4,1.20,4.80),(577,150,75,2,2,1.50,3.00),(578,150,92,2,1,2.00,2.00),(579,151,53,4,1,4.00,4.00),(580,151,44,2,1,1.50,1.50),(581,151,87,2,1,1.20,1.20),(582,151,116,2,1,1.50,1.50),(583,152,50,2,1,2.00,2.00),(584,152,92,2,1,2.00,2.00),(585,152,55,2,1,2.00,2.00),(586,153,50,2,2,2.00,4.00),(587,153,52,4,1,4.00,4.00),(588,153,59,2,2,1.20,2.40),(589,153,58,2,2,1.20,2.40),(590,154,50,2,1,2.00,2.00),(591,155,44,2,2,1.50,3.00),(592,156,9,2,1,4.00,4.00),(593,157,54,2,1,2.00,2.00),(594,157,92,2,1,2.00,2.00),(595,158,44,2,2,1.50,3.00),(596,159,44,2,1,1.50,1.50),(597,159,116,2,2,1.50,3.00),(598,159,54,2,1,2.00,2.00),(599,159,92,2,2,2.00,4.00),(600,159,53,4,1,4.00,4.00),(601,159,15,12,1,5.00,5.00),(602,160,50,2,1,2.00,2.00),(603,160,92,2,1,2.00,2.00),(604,161,44,2,1,1.50,1.50),(605,161,55,2,1,2.00,2.00),(606,162,44,2,1,1.50,1.50),(607,162,116,2,1,1.50,1.50),(608,163,92,2,2,2.00,4.00),(609,164,57,2,1,2.20,2.20),(610,164,58,2,1,1.20,1.20),(611,164,117,2,1,2.20,2.20),(612,164,50,2,1,2.00,2.00),(613,164,47,2,2,1.50,3.00),(614,164,116,2,1,1.50,1.50),(615,165,80,4,1,3.50,3.50),(616,165,92,2,1,2.00,2.00),(617,166,75,2,4,1.50,6.00),(618,166,69,2,3,0.60,1.80),(619,166,63,2,1,0.60,0.60),(620,166,71,2,1,0.60,0.60),(621,167,54,2,1,2.00,2.00),(622,167,56,2,1,2.20,2.20),(623,167,117,2,1,2.20,2.20),(624,168,44,2,6,1.50,9.00),(625,168,116,2,1,1.50,1.50),(626,168,45,2,1,1.50,1.50),(627,168,47,2,2,1.50,3.00),(628,169,9,2,1,4.00,4.00),(629,170,64,2,1,0.60,0.60),(630,171,11,2,3,5.00,15.00),(631,172,11,2,4,5.00,20.00),(632,172,44,2,1,1.50,1.50),(633,173,11,2,3,5.00,15.00),(634,173,53,4,2,4.00,8.00),(635,173,54,2,2,2.00,4.00),(636,174,11,2,2,5.00,10.00),(637,174,44,2,1,1.50,1.50),(638,175,54,2,1,2.00,2.00),(639,175,55,2,1,2.00,2.00),(640,176,64,2,2,0.60,1.20),(641,176,116,2,1,1.50,1.50),(642,177,44,2,2,1.50,3.00),(643,177,116,2,2,1.50,3.00),(644,177,47,2,1,1.50,1.50),(645,177,87,2,2,1.20,2.40),(646,177,57,2,1,2.20,2.20),(647,177,57,2,1,2.20,2.20),(648,178,92,2,1,2.00,2.00),(649,178,50,2,1,2.00,2.00),(650,179,55,2,3,2.00,6.00),(651,179,54,2,1,2.00,2.00),(652,179,44,2,1,1.50,1.50),(653,179,116,2,1,1.50,1.50),(654,179,80,4,1,3.50,3.50),(655,180,50,2,1,2.00,2.00),(656,180,45,2,1,1.50,1.50),(657,180,55,2,3,2.00,6.00),(658,180,54,2,1,2.00,2.00),(659,181,8,2,2,5.00,10.00),(660,181,54,2,1,2.00,2.00),(661,181,50,2,1,2.00,2.00),(662,181,92,2,2,2.00,4.00),(663,182,15,12,1,5.00,5.00),(664,182,53,4,1,4.00,4.00),(665,182,54,2,1,2.00,2.00),(666,183,13,2,2,5.00,10.00),(667,183,54,2,1,2.00,2.00),(668,184,54,2,3,2.00,6.00),(669,184,44,2,7,1.50,10.50),(670,184,13,2,1,5.00,5.00),(671,184,92,2,2,2.00,4.00),(672,185,50,2,1,2.00,2.00),(673,186,15,12,2,5.00,10.00),(674,186,53,4,2,4.00,8.00),(675,187,15,12,3,5.00,15.00),(676,187,53,4,2,4.00,8.00),(677,187,116,2,1,1.50,1.50),(678,187,55,2,1,2.00,2.00),(679,188,44,2,4,1.50,6.00),(680,188,15,12,1,5.00,5.00),(681,188,53,4,1,4.00,4.00),(682,188,11,2,6,5.00,30.00),(683,188,55,2,1,2.00,2.00),(684,188,54,2,1,2.00,2.00),(685,189,92,2,3,2.00,6.00),(686,189,55,2,1,2.00,2.00),(687,190,55,2,2,2.00,4.00),(688,191,54,2,4,2.00,8.00),(689,191,44,2,8,1.50,12.00),(690,191,47,2,1,1.50,1.50),(691,191,8,2,4,5.00,20.00),(692,191,11,2,1,5.00,5.00),(693,191,59,2,1,1.20,1.20),(694,192,50,2,1,2.00,2.00),(695,193,47,2,4,1.50,6.00),(696,193,44,2,2,1.50,3.00),(697,194,8,2,1,5.00,5.00),(698,195,11,2,1,5.00,5.00),(699,195,55,2,2,2.00,4.00),(700,195,116,2,2,1.50,3.00),(701,196,55,2,2,2.00,4.00),(702,196,8,2,3,5.00,15.00),(703,196,75,2,3,1.50,4.50),(704,197,63,2,1,0.60,0.60),(705,197,64,2,1,0.60,0.60),(706,197,50,2,2,2.00,4.00),(707,197,52,4,1,4.00,4.00),(708,197,53,4,1,4.00,4.00),(709,197,54,2,1,2.00,2.00),(710,197,55,2,1,2.00,2.00),(711,198,53,4,2,4.00,8.00),(712,199,55,2,1,2.00,2.00),(713,199,92,2,1,2.00,2.00),(714,200,42,5,1,5.00,5.00),(715,201,42,5,1,5.00,5.00),(716,202,44,2,1,1.50,1.50),(717,203,8,2,1,5.00,5.00),(718,204,50,2,1,2.00,2.00),(719,205,45,2,2,1.50,3.00),(720,205,54,2,1,2.00,2.00),(721,206,75,2,1,1.50,1.50),(722,206,54,2,2,2.00,4.00),(723,206,45,2,2,1.50,3.00),(724,206,68,2,2,0.50,1.00),(725,207,54,2,2,2.00,4.00),(726,208,54,2,2,2.00,4.00),(727,209,54,2,1,2.00,2.00),(728,209,45,2,1,1.50,1.50),(729,209,50,2,1,2.00,2.00),(730,210,54,2,2,2.00,4.00),(731,211,54,2,1,2.00,2.00),(732,211,67,2,2,3.50,7.00),(733,212,54,2,1,2.00,2.00),(734,212,116,2,1,1.50,1.50),(735,213,54,2,2,2.00,4.00),(736,214,45,2,2,1.50,3.00),(737,215,50,2,1,2.00,2.00),(738,215,54,2,3,2.00,6.00),(739,215,58,2,1,1.20,1.20),(740,215,108,2,2,1.50,3.00),(741,216,54,2,1,2.00,2.00),(742,217,75,2,2,1.50,3.00),(743,218,44,2,1,1.50,1.50),(744,219,50,2,1,2.00,2.00),(745,219,59,2,1,1.20,1.20),(746,219,116,2,1,1.50,1.50),(747,220,45,2,1,1.50,1.50),(748,220,63,2,1,0.60,0.60),(749,221,44,2,2,1.50,3.00),(750,221,45,2,1,1.50,1.50),(751,221,54,2,2,2.00,4.00),(752,222,45,2,2,1.50,3.00),(753,223,58,2,2,1.20,2.40),(754,223,54,2,1,2.00,2.00),(755,224,54,2,1,2.00,2.00),(756,224,92,2,1,2.00,2.00),(757,225,75,2,1,1.50,1.50),(758,225,68,2,1,0.50,0.50),(759,226,50,2,1,2.00,2.00),(760,226,68,2,2,0.50,1.00),(761,226,54,2,1,2.00,2.00),(762,226,58,2,1,1.20,1.20),(763,227,83,13,1,18.00,18.00),(764,227,29,13,1,18.00,18.00),(765,228,45,2,1,1.50,1.50),(766,228,54,2,2,2.00,4.00),(767,229,44,2,2,1.50,3.00),(768,230,108,2,2,1.50,3.00),(769,231,44,2,2,1.50,3.00),(770,231,75,2,1,1.50,1.50),(771,232,44,2,6,1.50,9.00),(772,232,54,2,1,2.00,2.00),(773,233,45,2,1,1.50,1.50),(774,233,47,2,1,1.50,1.50),(775,233,54,2,1,2.00,2.00),(776,234,50,2,1,2.00,2.00),(777,234,44,2,1,1.50,1.50),(778,234,54,2,3,2.00,6.00),(779,234,15,12,1,5.00,5.00),(780,235,53,4,9,4.00,36.00),(781,235,47,2,1,1.50,1.50),(782,235,54,2,4,2.00,8.00),(783,235,11,2,1,5.00,5.00),(784,235,58,2,1,1.20,1.20),(785,236,53,4,1,4.00,4.00),(786,237,44,2,1,1.50,1.50),(787,237,50,2,2,2.00,4.00),(788,237,15,12,1,5.00,5.00),(789,237,53,4,1,4.00,4.00),(790,238,53,4,1,4.00,4.00),(791,238,54,2,1,2.00,2.00),(792,239,45,2,1,1.50,1.50),(793,240,54,2,2,2.00,4.00),(794,241,45,2,1,1.50,1.50),(795,242,45,2,1,1.50,1.50),(796,242,54,2,1,2.00,2.00),(797,243,45,2,1,1.50,1.50),(798,243,54,2,1,2.00,2.00),(799,243,68,2,1,0.50,0.50),(800,243,63,2,1,0.60,0.60),(801,244,45,2,1,1.50,1.50),(802,244,116,2,1,1.50,1.50),(803,244,54,2,1,2.00,2.00),(804,245,54,2,2,2.00,4.00),(805,246,54,2,1,2.00,2.00),(806,247,45,2,1,1.50,1.50),(807,247,44,2,1,1.50,1.50),(808,247,54,2,1,2.00,2.00),(809,248,44,2,3,1.50,4.50),(810,249,7,2,1,4.00,4.00),(811,250,15,12,6,5.00,30.00),(812,250,53,4,3,4.00,12.00),(813,251,69,2,1,0.60,0.60),(814,251,58,2,1,1.20,1.20),(815,251,74,2,2,0.40,0.80),(816,251,75,2,1,1.50,1.50),(817,251,54,2,2,2.00,4.00),(818,251,92,2,1,2.00,2.00),(819,251,55,2,1,2.00,2.00),(820,251,68,2,1,0.50,0.50),(821,252,8,2,1,5.00,5.00),(822,252,45,2,1,1.50,1.50),(823,253,54,2,1,2.00,2.00),(824,253,74,2,2,0.40,0.80),(825,254,116,2,1,1.50,1.50),(826,254,45,2,1,1.50,1.50),(827,254,54,2,1,2.00,2.00),(828,255,15,12,2,5.00,10.00),(829,255,53,4,1,4.00,4.00),(830,255,44,2,3,1.50,4.50),(831,255,55,2,1,2.00,2.00),(832,256,54,2,1,2.00,2.00),(833,257,15,12,1,5.00,5.00),(834,258,54,2,1,2.00,2.00),(835,259,55,2,3,2.00,6.00),(836,259,54,2,2,2.00,4.00),(837,259,116,2,1,1.50,1.50),(838,259,44,2,1,1.50,1.50),(839,260,44,2,1,1.50,1.50),(840,260,54,2,1,2.00,2.00),(841,260,11,2,1,5.00,5.00),(842,261,44,2,1,1.50,1.50),(843,261,45,2,3,1.50,4.50),(844,262,55,2,1,2.00,2.00),(845,263,45,2,1,1.50,1.50),(846,264,55,2,1,2.00,2.00),(847,265,15,12,1,5.00,5.00),(848,265,53,4,1,4.00,4.00),(849,266,45,2,1,1.50,1.50),(850,266,60,2,1,1.20,1.20),(851,266,58,2,1,1.20,1.20),(852,267,60,2,1,1.20,1.20),(853,267,54,2,1,2.00,2.00),(854,268,52,4,2,4.00,8.00),(855,269,63,2,1,0.60,0.60),(856,269,63,2,2,0.60,1.20),(857,270,8,2,2,5.00,10.00),(858,270,54,2,1,2.00,2.00),(859,271,54,2,1,2.00,2.00),(860,271,71,2,1,0.60,0.60),(861,271,75,2,1,1.50,1.50),(862,271,68,2,1,0.50,0.50),(863,272,53,4,1,4.00,4.00),(864,273,53,4,1,4.00,4.00),(867,274,11,2,2,5.00,10.00),(868,274,44,2,1,1.50,1.50),(869,275,54,2,2,2.00,4.00),(870,275,108,2,2,1.50,3.00),(871,276,45,2,1,1.50,1.50),(872,276,54,2,1,2.00,2.00),(873,277,45,2,1,1.50,1.50),(874,278,44,2,1,1.50,1.50),(875,278,45,2,2,1.50,3.00),(876,279,45,2,2,1.50,3.00),(877,279,44,2,1,1.50,1.50),(878,280,108,2,1,1.50,1.50),(879,281,29,13,1,18.00,18.00),(880,281,62,2,1,0.60,0.60),(881,282,54,2,3,2.00,6.00),(882,282,62,2,2,0.60,1.20),(883,282,74,2,2,0.40,0.80),(884,283,54,2,3,2.00,6.00),(885,283,62,2,3,0.60,1.80),(886,284,44,2,2,1.50,3.00),(887,285,8,2,2,5.00,10.00),(888,285,68,2,2,0.50,1.00),(889,286,12,2,1,5.00,5.00),(890,286,54,2,2,2.00,4.00),(891,286,75,2,1,1.50,1.50),(892,287,40,5,1,5.00,5.00),(893,287,96,24,1,5.00,5.00),(894,288,11,2,8,5.00,40.00),(895,288,44,2,1,1.50,1.50),(896,288,87,2,2,1.20,2.40),(897,288,60,2,2,1.20,2.40),(898,289,50,2,1,2.00,2.00),(899,289,55,2,1,2.00,2.00),(900,290,117,2,1,2.20,2.20),(901,290,87,2,2,1.20,2.40),(902,290,11,2,3,5.00,15.00),(903,290,85,2,1,1.20,1.20),(904,290,60,2,1,1.20,1.20),(905,290,55,2,1,2.00,2.00),(906,291,44,2,2,1.50,3.00),(907,292,44,2,2,1.50,3.00),(908,292,45,2,1,1.50,1.50),(909,293,54,2,1,2.00,2.00),(910,293,55,2,1,2.00,2.00),(911,293,15,12,3,5.00,15.00),(912,293,53,4,2,4.00,8.00),(913,294,116,2,1,1.50,1.50),(914,294,8,2,1,5.00,5.00),(915,294,55,2,1,2.00,2.00),(916,294,13,2,9,5.00,45.00),(917,294,15,12,3,5.00,15.00),(918,294,53,4,2,4.00,8.00),(919,294,11,2,1,5.00,5.00),(920,294,12,2,2,5.00,10.00),(921,295,44,2,2,1.50,3.00),(922,295,93,26,1,1.00,1.00),(923,296,55,2,2,2.00,4.00),(924,296,54,2,1,2.00,2.00),(925,297,44,2,2,1.50,3.00),(926,297,11,2,3,5.00,15.00),(927,298,50,2,1,2.00,2.00),(928,299,53,4,1,4.00,4.00),(929,299,15,12,2,5.00,10.00),(930,299,45,2,2,1.50,3.00),(931,300,2,5,2,7.50,15.00),(932,300,54,2,1,2.00,2.00),(933,300,15,12,1,5.00,5.00),(934,300,8,2,2,5.00,10.00),(935,300,53,4,1,4.00,4.00),(936,301,44,2,1,1.50,1.50),(937,301,50,2,1,2.00,2.00),(938,302,44,2,2,1.50,3.00),(939,302,54,2,1,2.00,2.00),(940,303,42,5,1,5.00,5.00),(941,303,94,5,1,5.00,5.00),(942,303,44,2,1,1.50,1.50),(943,303,40,5,1,5.00,5.00),(944,303,43,5,1,5.00,5.00),(945,304,54,2,1,2.00,2.00),(946,304,50,2,1,2.00,2.00),(947,305,54,2,1,2.00,2.00),(948,306,54,2,2,2.00,4.00),(949,307,15,12,1,5.00,5.00),(950,307,6,2,3,6.00,18.00),(951,307,54,2,3,2.00,6.00),(952,307,75,2,1,1.50,1.50),(953,308,54,2,1,2.00,2.00),(954,309,116,2,2,1.50,3.00),(955,309,15,12,1,5.00,5.00),(956,309,53,4,1,4.00,4.00),(957,309,5,5,1,7.50,7.50),(958,309,54,2,1,2.00,2.00),(959,310,74,2,2,0.40,0.80),(960,310,54,2,1,2.00,2.00),(961,311,45,2,1,1.50,1.50),(962,312,54,2,2,2.00,4.00),(963,312,74,2,4,0.40,1.60),(964,313,15,12,1,5.00,5.00),(965,313,53,4,1,4.00,4.00),(966,314,54,2,1,2.00,2.00),(967,315,54,2,3,2.00,6.00),(968,316,54,2,1,2.00,2.00),(969,317,8,2,1,5.00,5.00),(970,318,44,2,1,1.50,1.50),(971,319,44,2,1,1.50,1.50),(972,319,70,2,1,0.60,0.60),(973,320,54,2,3,2.00,6.00),(974,320,15,12,1,5.00,5.00),(975,320,53,4,1,4.00,4.00),(976,320,44,2,1,1.50,1.50),(977,320,116,2,2,1.50,3.00),(978,321,8,2,3,5.00,15.00),(979,321,50,2,3,2.00,6.00),(980,321,44,2,3,1.50,4.50),(981,321,45,2,1,1.50,1.50),(982,322,50,2,1,2.00,2.00),(983,322,54,2,2,2.00,4.00),(984,323,55,2,1,2.00,2.00),(985,324,44,2,2,1.50,3.00),(986,325,54,2,1,2.00,2.00),(987,326,11,2,3,5.00,15.00),(988,326,15,12,4,5.00,20.00),(989,327,120,4,2,3.50,7.00),(990,328,122,2,1,2.50,2.50),(991,329,54,2,1,2.00,2.00),(992,330,122,2,1,2.50,2.50),(993,331,54,2,1,2.00,2.00),(994,331,68,2,2,0.50,1.00),(995,331,123,2,2,1.00,2.00),(996,332,25,3,1,58.00,58.00),(997,333,15,12,4,5.00,20.00),(998,333,120,4,2,3.50,7.00),(999,334,54,2,2,2.00,4.00),(1000,334,58,2,1,1.20,1.20),(1001,334,123,2,1,1.00,1.00),(1002,334,66,2,1,0.60,0.60),(1003,334,11,2,1,5.00,5.00),(1004,335,124,2,1,0.50,0.50),(1005,335,15,12,1,5.00,5.00),(1006,335,120,4,1,3.50,3.50),(1007,335,47,2,1,1.50,1.50),(1008,335,92,2,1,2.00,2.00),(1009,336,125,3,1,70.00,70.00),(1010,337,83,13,1,18.00,18.00),(1011,338,54,2,1,2.00,2.00),(1012,338,126,2,1,0.50,0.50),(1013,339,15,12,11,5.00,55.00),(1014,339,120,4,5,3.50,17.50),(1015,339,63,2,2,0.60,1.20),(1016,339,59,2,2,1.20,2.40),(1017,340,58,2,1,1.20,1.20),(1018,340,55,2,1,2.00,2.00),(1019,340,45,2,1,1.50,1.50),(1020,341,55,2,2,2.00,4.00),(1021,341,47,2,1,1.50,1.50),(1022,341,54,2,1,2.00,2.00),(1023,341,120,4,1,3.50,3.50),(1024,342,23,5,1,6.50,6.50),(1025,342,54,2,1,2.00,2.00),(1026,343,120,4,2,3.50,7.00),(1027,344,44,2,1,1.50,1.50),(1028,344,120,4,5,3.50,17.50),(1029,344,54,2,1,2.00,2.00),(1030,345,54,2,3,2.00,6.00),(1031,345,128,2,1,0.50,0.50),(1032,345,75,2,1,1.50,1.50),(1033,346,11,2,1,5.00,5.00),(1034,346,87,2,1,1.20,1.20),(1035,346,47,2,2,1.50,3.00),(1036,347,123,2,1,1.00,1.00),(1037,347,54,2,1,2.00,2.00),(1038,347,53,4,1,4.00,4.00),(1039,348,50,2,1,2.00,2.00),(1040,348,45,2,1,1.50,1.50),(1041,348,128,2,1,0.50,0.50),(1042,348,54,2,1,2.00,2.00),(1043,348,62,2,1,0.60,0.60),(1044,349,128,2,1,0.50,0.50),(1045,350,15,12,3,5.00,15.00),(1046,350,120,4,2,3.50,7.00),(1047,350,12,2,1,5.00,5.00),(1048,351,15,12,6,5.00,30.00),(1049,351,120,4,3,3.50,10.50),(1050,352,120,4,1,3.50,3.50),(1051,353,15,12,3,5.00,15.00),(1052,353,120,4,3,3.50,10.50),(1053,353,54,2,3,2.00,6.00),(1054,353,68,2,1,0.50,0.50),(1055,353,25,3,1,58.00,58.00),(1056,354,50,2,1,2.00,2.00),(1057,354,15,12,2,5.00,10.00),(1058,354,120,4,1,3.50,3.50),(1059,354,64,2,1,0.60,0.60),(1060,354,124,2,1,0.50,0.50),(1061,354,58,2,1,1.20,1.20),(1062,354,123,2,1,1.00,1.00),(1063,355,11,2,1,5.00,5.00),(1064,355,45,2,2,1.50,3.00),(1065,355,92,2,1,2.00,2.00),(1066,355,123,2,1,1.00,1.00),(1067,356,129,2,1,4.00,4.00),(1068,357,131,2,2,1.10,2.20),(1069,357,123,2,2,1.00,2.00),(1070,357,54,2,1,2.00,2.00),(1071,358,50,2,1,2.00,2.00),(1072,358,63,2,1,0.60,0.60),(1073,358,55,2,1,2.00,2.00),(1074,358,129,2,1,4.00,4.00),(1075,359,23,5,1,6.50,6.50),(1076,359,11,2,1,5.00,5.00),(1077,360,54,2,2,2.00,4.00),(1078,360,57,2,1,2.20,2.20),(1079,360,108,2,2,1.50,3.00),(1080,361,15,12,6,5.00,30.00),(1081,361,53,4,2,4.00,8.00),(1082,362,116,2,2,1.50,3.00),(1083,362,54,2,1,2.00,2.00),(1084,362,93,26,1,1.00,1.00),(1085,363,53,4,5,4.00,20.00),(1086,363,15,12,3,5.00,15.00),(1087,364,11,2,2,5.00,10.00),(1088,365,55,2,1,2.00,2.00),(1089,366,44,2,2,1.50,3.00),(1090,366,116,2,1,1.50,1.50),(1091,367,18,5,2,6.50,13.00),(1092,367,44,2,2,1.50,3.00),(1093,367,116,2,1,1.50,1.50),(1094,367,47,2,1,1.50,1.50),(1095,367,15,12,11,5.00,55.00),(1096,367,53,4,4,4.00,16.00),(1097,367,42,5,1,5.00,5.00),(1098,368,59,2,1,1.20,1.20),(1099,368,64,2,1,0.60,0.60),(1100,368,50,2,1,2.00,2.00),(1101,368,47,2,2,1.50,3.00),(1102,368,54,2,1,2.00,2.00),(1103,369,55,2,2,2.00,4.00),(1104,369,92,2,1,2.00,2.00),(1105,370,54,2,1,2.00,2.00),(1107,371,53,4,1,4.00,4.00),(1108,372,11,2,1,5.00,5.00),(1109,372,57,2,1,2.20,2.20),(1110,373,8,2,1,5.00,5.00),(1111,373,11,2,3,5.00,15.00),(1112,373,53,4,1,4.00,4.00),(1113,373,15,27,1,2.50,2.50),(1114,373,15,12,1,5.00,5.00),(1115,374,15,12,11,5.00,55.00),(1116,374,53,4,4,4.00,16.00),(1117,375,45,2,1,1.50,1.50),(1118,375,112,2,1,0.60,0.60),(1119,376,8,2,16,5.00,80.00),(1120,376,12,2,4,5.00,20.00),(1121,377,50,2,2,2.00,4.00),(1122,377,52,4,2,4.00,8.00),(1123,377,59,2,2,1.20,2.40),(1124,378,11,2,2,5.00,10.00),(1125,378,55,2,2,2.00,4.00),(1126,378,116,2,1,1.50,1.50),(1127,379,44,2,4,1.50,6.00),(1128,379,45,2,1,1.50,1.50),(1129,379,92,2,1,2.00,2.00),(1130,379,134,2,1,1.50,1.50),(1131,380,15,12,2,5.00,10.00),(1132,380,53,4,2,4.00,8.00),(1133,380,92,2,1,2.00,2.00),(1134,381,63,2,1,0.60,0.60),(1135,381,51,2,2,3.50,7.00),(1136,381,116,2,4,1.50,6.00),(1137,381,54,2,2,2.00,4.00),(1138,381,53,4,1,4.00,4.00),(1139,381,44,2,1,1.50,1.50),(1140,381,134,2,1,1.50,1.50),(1141,381,57,2,1,2.20,2.20),(1142,382,44,2,1,1.50,1.50),(1143,382,55,2,1,2.00,2.00),(1144,382,15,12,1,5.00,5.00),(1145,383,15,12,1,5.00,5.00),(1146,384,51,2,1,3.50,3.50),(1147,384,44,2,1,1.50,1.50),(1148,385,92,2,2,2.00,4.00),(1149,385,15,12,1,5.00,5.00),(1150,385,53,4,1,4.00,4.00),(1151,386,15,12,2,5.00,10.00),(1152,386,53,4,1,4.00,4.00),(1153,387,15,12,1,5.00,5.00),(1154,387,53,4,2,4.00,8.00),(1155,387,56,2,2,2.20,4.40),(1156,387,54,2,1,2.00,2.00),(1157,387,64,2,1,0.60,0.60),(1158,388,54,2,2,2.00,4.00),(1159,388,116,2,1,1.50,1.50),(1160,388,15,12,2,5.00,10.00),(1161,388,53,4,1,4.00,4.00),(1162,388,59,2,2,1.20,2.40),(1163,388,134,2,1,1.50,1.50),(1164,389,13,2,5,5.00,25.00),(1165,390,44,2,2,1.50,3.00),(1166,390,53,4,1,4.00,4.00),(1167,391,53,4,1,4.00,4.00),(1168,392,54,2,2,2.00,4.00),(1169,392,13,2,1,5.00,5.00),(1170,392,92,2,1,2.00,2.00),(1171,393,15,12,5,5.00,25.00),(1172,393,53,4,2,4.00,8.00),(1173,394,15,12,2,5.00,10.00),(1174,394,53,4,1,4.00,4.00),(1175,394,55,2,2,2.00,4.00),(1176,395,8,2,1,5.00,5.00),(1177,396,11,2,2,5.00,10.00),(1178,397,40,5,1,5.00,5.00),(1179,397,43,5,1,5.00,5.00),(1180,397,75,2,1,1.50,1.50),(1181,397,15,12,9,5.00,45.00),(1182,397,53,4,3,4.00,12.00),(1183,397,54,2,1,2.00,2.00),(1184,398,50,2,1,2.00,2.00),(1185,399,55,2,1,2.00,2.00),(1186,399,11,2,1,5.00,5.00),(1187,400,50,2,1,2.00,2.00),(1188,401,15,12,3,5.00,15.00),(1189,401,53,4,2,4.00,8.00),(1190,401,87,2,1,1.20,1.20),(1191,401,63,2,2,0.60,1.20),(1192,402,15,12,6,5.00,30.00),(1193,402,53,4,2,4.00,8.00),(1194,402,11,2,3,5.00,15.00),(1195,402,87,2,4,1.20,4.80),(1196,402,93,26,2,1.00,2.00),(1197,403,50,2,1,2.00,2.00),(1198,403,52,4,2,4.00,8.00),(1199,403,92,2,1,2.00,2.00),(1200,404,53,4,1,4.00,4.00),(1201,404,55,2,1,2.00,2.00),(1202,405,44,2,2,1.50,3.00),(1203,406,11,2,1,5.00,5.00),(1204,406,93,26,2,1.00,2.00),(1205,406,55,2,1,2.00,2.00),(1206,407,50,2,1,2.00,2.00),(1207,408,15,12,2,5.00,10.00),(1208,408,53,4,1,4.00,4.00),(1209,409,54,2,2,2.00,4.00),(1210,409,92,2,1,2.00,2.00),(1211,409,13,2,1,5.00,5.00),(1212,410,54,2,2,2.00,4.00),(1213,410,92,2,1,2.00,2.00),(1214,410,137,23,1,5.00,5.00),(1215,411,136,12,3,5.00,15.00),(1216,411,53,4,2,4.00,8.00),(1217,411,87,2,1,1.20,1.20),(1218,411,63,2,2,0.60,1.20),(1219,412,136,12,5,5.00,25.00),(1220,412,53,4,2,4.00,8.00),(1221,413,55,2,2,2.00,4.00),(1222,413,136,12,2,5.00,10.00),(1223,413,53,4,1,4.00,4.00),(1224,414,136,12,6,5.00,30.00),(1225,414,53,4,2,4.00,8.00),(1226,414,11,2,2,5.00,10.00),(1227,414,87,2,4,1.20,4.80),(1228,414,93,26,2,1.00,2.00),(1229,414,11,2,1,5.00,5.00),(1230,415,40,5,1,5.00,5.00),(1231,415,43,5,1,5.00,5.00),(1232,415,75,2,1,1.50,1.50),(1233,415,136,12,9,5.00,45.00),(1234,415,53,4,3,4.00,12.00),(1235,415,54,2,1,2.00,2.00),(1236,416,136,12,2,5.00,10.00),(1237,416,53,4,1,4.00,4.00),(1238,417,54,2,2,2.00,4.00),(1239,417,75,2,4,1.50,6.00),(1240,417,92,2,3,2.00,6.00),(1241,417,54,2,1,2.00,2.00),(1242,418,50,2,1,2.00,2.00),(1243,418,55,2,2,2.00,4.00),(1244,418,68,2,1,0.50,0.50),(1245,419,54,2,1,2.00,2.00),(1246,420,136,12,1,5.00,5.00),(1247,420,53,4,1,4.00,4.00),(1248,420,54,2,2,2.00,4.00),(1249,421,11,2,3,5.00,15.00),(1250,421,136,12,1,5.00,5.00),(1251,421,53,4,1,4.00,4.00),(1252,421,74,2,2,0.40,0.80),(1253,421,55,2,1,2.00,2.00),(1254,421,116,2,1,1.50,1.50),(1255,422,136,12,1,5.00,5.00),(1256,422,53,4,1,4.00,4.00),(1257,422,44,2,1,1.50,1.50),(1258,422,57,2,1,2.20,2.20),(1259,422,87,2,2,1.20,2.40),(1260,422,55,2,1,2.00,2.00),(1261,423,44,2,1,1.50,1.50),(1262,423,61,2,1,1.20,1.20),(1263,424,75,2,3,1.50,4.50),(1264,424,134,2,1,1.50,1.50),(1265,424,54,2,1,2.00,2.00),(1266,424,54,2,1,2.00,2.00),(1267,425,54,2,4,2.00,8.00),(1268,425,51,2,1,3.50,3.50),(1269,426,55,2,1,2.00,2.00),(1270,427,11,2,1,5.00,5.00),(1271,427,132,3,1,38.00,38.00),(1272,427,92,2,1,2.00,2.00),(1273,428,44,2,2,1.50,3.00),(1274,429,63,2,1,0.60,0.60),(1275,429,116,2,4,1.50,6.00),(1276,429,54,2,2,2.00,4.00),(1277,429,53,4,1,4.00,4.00),(1278,429,57,2,1,2.20,2.20),(1279,429,44,2,1,1.50,1.50),(1280,429,134,2,1,1.50,1.50),(1281,429,51,2,2,3.50,7.00),(1282,430,18,5,1,6.50,6.50),(1283,430,44,2,1,1.50,1.50),(1284,430,87,2,1,1.20,1.20),(1285,431,136,12,2,5.00,10.00),(1286,431,53,4,1,4.00,4.00),(1287,431,55,2,2,2.00,4.00),(1288,431,75,2,1,1.50,1.50),(1289,432,55,2,2,2.00,4.00),(1290,432,54,2,1,2.00,2.00),(1291,432,54,2,1,2.00,2.00),(1292,433,136,12,1,5.00,5.00),(1293,434,87,2,2,1.20,2.40),(1294,435,11,2,3,5.00,15.00),(1295,435,87,2,2,1.20,2.40),(1296,435,136,12,1,5.00,5.00),(1297,435,53,4,1,4.00,4.00),(1298,436,136,12,4,5.00,20.00),(1299,437,136,12,2,5.00,10.00),(1300,437,54,2,2,2.00,4.00),(1301,438,11,2,1,5.00,5.00),(1302,438,11,2,3,5.00,15.00),(1303,438,87,2,1,1.20,1.20),(1304,438,88,2,1,0.60,0.60),(1305,439,2,5,16,7.50,120.00),(1306,439,87,2,2,1.20,2.40),(1307,439,29,13,1,18.00,18.00),(1308,440,44,2,5,1.50,7.50),(1309,441,50,2,1,2.00,2.00),(1310,441,55,2,2,2.00,4.00),(1311,441,92,2,1,2.00,2.00),(1312,442,50,2,1,2.00,2.00),(1313,442,52,4,2,4.00,8.00),(1314,442,57,2,1,2.20,2.20),(1315,443,29,13,1,18.00,18.00),(1316,443,50,2,1,2.00,2.00),(1317,444,51,2,1,3.50,3.50),(1318,445,136,12,4,5.00,20.00),(1319,445,53,4,2,4.00,8.00),(1320,445,11,2,2,5.00,10.00),(1321,445,116,2,1,1.50,1.50),(1322,445,55,2,1,2.00,2.00),(1323,445,75,2,1,1.50,1.50),(1324,445,74,2,1,0.40,0.40),(1325,446,44,2,3,1.50,4.50),(1326,446,116,2,1,1.50,1.50),(1327,446,45,2,1,1.50,1.50),(1328,447,75,2,3,1.50,4.50),(1329,448,53,4,5,4.00,20.00),(1330,449,12,2,2,5.00,10.00),(1331,450,137,23,1,5.00,5.00),(1332,451,50,2,1,2.00,2.00),(1333,452,11,2,2,5.00,10.00),(1334,452,61,2,2,1.20,2.40),(1335,452,87,2,2,1.20,2.40),(1336,453,44,2,1,1.50,1.50),(1337,454,44,2,1,1.50,1.50),(1338,454,54,2,2,2.00,4.00),(1339,454,11,2,2,5.00,10.00),(1340,455,44,2,1,1.50,1.50),(1341,456,55,2,2,2.00,4.00),(1342,457,116,2,2,1.50,3.00),(1343,457,54,2,1,2.00,2.00),(1344,457,43,5,2,5.00,10.00),(1345,458,44,2,3,1.50,4.50),(1346,459,44,2,3,1.50,4.50),(1347,460,116,2,1,1.50,1.50),(1348,461,51,2,1,3.50,3.50),(1349,461,44,2,1,1.50,1.50),(1350,461,45,2,1,1.50,1.50),(1351,462,136,12,1,5.00,5.00),(1352,462,53,4,1,4.00,4.00),(1353,463,134,2,1,1.50,1.50),(1354,464,137,23,1,5.00,5.00),(1355,464,45,2,1,1.50,1.50),(1356,464,93,26,2,1.00,2.00),(1357,464,80,4,1,3.50,3.50),(1358,465,96,24,2,5.00,10.00),(1359,466,136,12,3,5.00,15.00),(1360,466,53,4,2,4.00,8.00),(1361,467,44,2,1,1.50,1.50),(1362,467,54,2,1,2.00,2.00),(1363,468,2,5,3,7.50,22.50),(1364,469,11,2,2,5.00,10.00),(1365,469,55,2,1,2.00,2.00),(1366,470,44,2,2,1.50,3.00),(1367,471,50,2,1,2.00,2.00),(1368,471,55,2,1,2.00,2.00),(1369,472,11,2,2,5.00,10.00),(1370,473,11,2,4,5.00,20.00),(1371,473,57,2,1,2.20,2.20),(1372,473,132,3,1,38.00,38.00),(1373,473,25,3,1,58.00,58.00),(1374,474,11,2,3,5.00,15.00),(1375,475,136,12,3,5.00,15.00),(1376,475,53,4,2,4.00,8.00),(1377,475,44,2,2,1.50,3.00),(1378,475,18,5,3,6.50,19.50),(1379,475,93,26,2,1.00,2.00),(1380,476,136,12,3,5.00,15.00),(1381,476,53,4,2,4.00,8.00),(1382,477,136,12,3,5.00,15.00),(1383,477,53,4,2,4.00,8.00),(1384,478,3,5,2,10.00,20.00),(1385,478,75,2,2,1.50,3.00),(1386,478,87,2,2,1.20,2.40),(1387,478,59,2,1,1.20,1.20),(1388,478,133,2,1,2.00,2.00),(1389,478,53,4,1,4.00,4.00),(1390,479,53,4,1,4.00,4.00),(1391,479,44,2,2,1.50,3.00),(1392,479,59,2,1,1.20,1.20),(1393,479,54,2,1,2.00,2.00),(1394,480,55,2,2,2.00,4.00),(1395,480,92,2,1,2.00,2.00),(1396,480,68,2,2,0.50,1.00),(1397,481,136,12,3,5.00,15.00),(1398,481,53,4,2,4.00,8.00),(1399,481,116,2,2,1.50,3.00),(1400,481,75,2,1,1.50,1.50),(1401,481,55,2,1,2.00,2.00),(1402,481,92,2,2,2.00,4.00),(1403,482,44,2,1,1.50,1.50),(1404,482,54,2,1,2.00,2.00),(1405,483,50,2,1,2.00,2.00),(1406,484,44,2,2,1.50,3.00),(1407,485,44,2,1,1.50,1.50),(1408,486,50,2,1,2.00,2.00),(1409,486,44,2,1,1.50,1.50),(1410,486,54,2,2,2.00,4.00),(1411,486,59,2,1,1.20,1.20),(1412,486,47,2,1,1.50,1.50),(1413,487,11,2,7,5.00,35.00),(1414,487,80,4,1,3.50,3.50),(1415,487,45,2,1,1.50,1.50),(1416,487,112,2,1,0.60,0.60),(1417,487,112,2,3,0.60,1.80),(1418,488,12,2,7,5.00,35.00),(1419,488,44,2,3,1.50,4.50),(1420,489,12,2,1,5.00,5.00),(1421,490,96,24,1,5.00,5.00),(1422,491,44,2,2,1.50,3.00),(1423,491,93,26,2,1.00,2.00),(1424,491,75,2,1,1.50,1.50),(1425,491,69,2,1,0.60,0.60),(1426,492,44,2,1,1.50,1.50),(1427,492,47,2,2,1.50,3.00),(1428,492,61,2,1,1.20,1.20),(1429,493,12,2,2,5.00,10.00),(1430,494,75,2,1,1.50,1.50),(1431,495,44,2,1,1.50,1.50),(1432,496,136,12,2,5.00,10.00),(1433,496,53,4,1,4.00,4.00),(1434,497,8,2,6,5.00,30.00),(1435,497,44,2,1,1.50,1.50),(1436,498,45,2,1,1.50,1.50),(1437,499,8,2,3,5.00,15.00),(1438,500,8,2,6,5.00,30.00),(1439,500,45,2,1,1.50,1.50),(1440,501,12,2,2,5.00,10.00),(1441,501,58,2,2,1.20,2.40),(1442,502,12,2,3,5.00,15.00),(1443,503,8,2,4,5.00,20.00),(1444,503,137,23,1,5.00,5.00),(1445,504,54,2,1,2.00,2.00),(1446,504,87,2,2,1.20,2.40),(1447,504,133,2,1,2.00,2.00),(1448,504,18,5,1,6.50,6.50),(1449,504,44,2,1,1.50,1.50),(1450,505,50,2,2,2.00,4.00),(1451,505,52,4,1,4.00,4.00),(1452,505,87,2,2,1.20,2.40),(1453,506,92,2,1,2.00,2.00),(1454,506,75,2,1,1.50,1.50),(1455,506,55,2,1,2.00,2.00),(1456,507,50,2,1,2.00,2.00),(1457,507,87,2,2,1.20,2.40),(1458,508,44,2,1,1.50,1.50),(1459,508,87,2,2,1.20,2.40),(1460,508,92,2,1,2.00,2.00),(1461,509,50,2,1,2.00,2.00),(1462,510,54,2,1,2.00,2.00),(1463,511,51,2,1,3.50,3.50),(1464,511,54,2,1,2.00,2.00),(1465,511,44,2,2,1.50,3.00),(1466,511,51,2,1,3.50,3.50),(1467,512,75,2,2,1.50,3.00),(1468,512,133,2,1,2.00,2.00),(1469,513,55,2,1,2.00,2.00),(1470,514,54,2,1,2.00,2.00),(1471,515,94,5,3,5.00,15.00),(1472,515,53,4,2,4.00,8.00),(1473,516,54,2,1,2.00,2.00),(1474,517,44,2,2,1.50,3.00),(1475,517,75,2,2,1.50,3.00),(1476,518,55,2,1,2.00,2.00),(1477,518,44,2,1,1.50,1.50),(1478,519,61,2,4,1.20,4.80),(1479,519,44,2,3,1.50,4.50),(1480,519,54,2,1,2.00,2.00),(1481,520,8,2,6,5.00,30.00),(1482,520,93,26,2,1.00,2.00),(1483,520,59,2,4,1.20,4.80),(1484,520,70,2,4,0.60,2.40),(1485,520,93,26,1,1.00,1.00),(1486,521,136,12,4,5.00,20.00),(1487,521,53,4,2,4.00,8.00),(1488,521,11,2,8,5.00,40.00),(1489,522,44,2,3,1.50,4.50),(1490,522,45,2,1,1.50,1.50),(1491,522,116,2,2,1.50,3.00),(1492,523,44,2,3,1.50,4.50),(1493,523,45,2,1,1.50,1.50),(1494,523,116,2,2,1.50,3.00),(1495,524,8,2,42,5.00,210.00),(1496,524,12,2,24,5.00,120.00),(1497,524,11,2,24,5.00,120.00),(1498,524,137,2,4,5.00,20.00),(1505,526,10,2,2,6.00,12.00),(1506,526,50,2,2,2.00,4.00),(1507,526,45,2,1,1.50,1.50),(1508,527,136,12,9,5.00,45.00),(1509,527,53,4,2,4.00,8.00),(1510,528,50,2,1,2.00,2.00),(1511,528,87,2,1,1.20,1.20),(1512,528,59,2,1,1.20,1.20),(1513,528,136,12,1,5.00,5.00),(1514,528,53,4,1,4.00,4.00),(1515,529,50,2,2,2.00,4.00),(1516,529,92,2,1,2.00,2.00),(1517,529,55,2,1,2.00,2.00),(1518,530,50,2,1,2.00,2.00),(1519,530,44,2,1,1.50,1.50),(1520,530,45,2,1,1.50,1.50),(1521,530,116,2,1,1.50,1.50),(1522,530,59,2,1,1.20,1.20),(1523,530,58,2,1,1.20,1.20),(1524,531,136,12,5,5.00,25.00),(1525,531,53,4,2,4.00,8.00),(1526,531,92,2,2,2.00,4.00),(1527,531,75,2,1,1.50,1.50),(1528,531,55,2,1,2.00,2.00),(1529,532,54,2,1,2.00,2.00),(1530,533,50,2,1,2.00,2.00),(1531,533,52,4,2,4.00,8.00),(1532,533,59,2,2,1.20,2.40),(1533,533,63,2,3,0.60,1.80),(1534,534,52,4,1,4.00,4.00),(1535,534,92,2,1,2.00,2.00),(1536,535,44,2,1,1.50,1.50),(1537,535,54,2,1,2.00,2.00),(1538,535,136,12,1,5.00,5.00),(1539,535,53,4,1,4.00,4.00),(1540,536,136,12,1,5.00,5.00),(1541,536,53,4,1,4.00,4.00),(1542,536,54,2,2,2.00,4.00),(1543,536,116,2,1,1.50,1.50),(1544,536,54,2,1,2.00,2.00),(1545,537,55,2,1,2.00,2.00),(1546,538,11,2,5,5.00,25.00),(1547,539,94,5,4,5.00,20.00),(1548,539,53,4,2,4.00,8.00),(1549,540,44,2,1,1.50,1.50),(1550,540,116,2,1,1.50,1.50),(1551,540,68,2,4,0.50,2.00),(1552,540,53,4,1,4.00,4.00),(1553,540,63,2,1,0.60,0.60),(1554,540,55,2,1,2.00,2.00),(1555,541,44,2,1,1.50,1.50),(1556,541,66,2,1,0.60,0.60),(1557,541,93,26,1,1.00,1.00),(1558,542,12,2,7,5.00,35.00),(1559,543,12,2,3,5.00,15.00),(1560,543,136,12,8,5.00,40.00),(1561,543,53,4,4,4.00,16.00),(1562,544,55,2,1,2.00,2.00),(1563,545,7,2,1,4.00,4.00),(1564,545,10,2,1,6.00,6.00),(1565,546,44,2,2,1.50,3.00),(1566,546,45,2,2,1.50,3.00),(1567,546,58,2,2,1.20,2.40),(1568,546,116,2,1,1.50,1.50),(1569,547,11,2,3,5.00,15.00),(1570,548,11,2,2,5.00,10.00),(1571,549,137,2,8,5.00,40.00),(1572,549,12,2,3,5.00,15.00),(1573,550,50,2,1,2.00,2.00),(1574,550,55,2,1,2.00,2.00),(1575,550,92,2,1,2.00,2.00),(1576,550,68,2,2,0.50,1.00),(1577,551,55,2,1,2.00,2.00),(1578,552,80,4,3,3.50,10.50),(1579,553,136,12,2,5.00,10.00),(1580,553,53,4,2,4.00,8.00),(1581,553,11,2,2,5.00,10.00),(1582,554,11,2,1,5.00,5.00),(1583,555,50,2,1,2.00,2.00),(1584,555,55,2,1,2.00,2.00),(1585,556,50,2,1,2.00,2.00),(1586,556,63,2,2,0.60,1.20),(1587,557,93,26,4,1.00,4.00),(1588,557,136,12,2,5.00,10.00),(1589,557,53,4,1,4.00,4.00),(1590,558,136,12,4,5.00,20.00),(1591,558,53,4,4,4.00,16.00),(1592,558,11,2,2,5.00,10.00),(1593,558,44,2,1,1.50,1.50),(1594,558,92,2,2,2.00,4.00),(1595,559,50,2,1,2.00,2.00),(1596,560,80,4,2,3.50,7.00),(1597,560,132,3,1,38.00,38.00),(1598,560,92,2,1,2.00,2.00),(1599,561,7,2,2,4.00,8.00),(1600,561,44,2,1,1.50,1.50),(1601,561,116,2,1,1.50,1.50),(1602,562,136,12,1,5.00,5.00),(1603,562,53,4,1,4.00,4.00),(1604,563,52,4,2,4.00,8.00),(1605,563,50,2,1,2.00,2.00),(1606,563,50,2,1,2.00,2.00),(1607,563,133,2,3,2.00,6.00),(1608,563,64,2,1,0.60,0.60),(1609,563,63,2,2,0.60,1.20),(1610,564,54,2,1,2.00,2.00),(1611,564,74,2,2,0.40,0.80),(1612,565,44,2,2,1.50,3.00),(1613,565,55,2,1,2.00,2.00),(1614,565,45,2,1,1.50,1.50),(1615,565,63,2,2,0.60,1.20),(1616,565,51,2,1,3.50,3.50),(1617,565,54,2,3,2.00,6.00),(1618,565,80,4,1,3.50,3.50),(1619,565,137,2,3,5.00,15.00),(1620,565,112,2,1,0.60,0.60),(1621,565,44,2,1,1.50,1.50),(1622,565,136,12,1,5.00,5.00),(1623,565,53,4,1,4.00,4.00),(1624,565,54,2,1,2.00,2.00),(1625,566,136,12,6,5.00,30.00),(1626,566,53,4,2,4.00,8.00),(1627,566,44,2,2,1.50,3.00),(1628,566,59,2,2,1.20,2.40),(1629,567,136,12,1,5.00,5.00),(1630,567,53,4,1,4.00,4.00),(1631,568,11,2,1,5.00,5.00),(1632,568,44,2,1,1.50,1.50),(1633,569,44,2,2,1.50,3.00),(1634,569,47,2,1,1.50,1.50),(1635,569,58,2,2,1.20,2.40),(1636,569,116,2,1,1.50,1.50),(1637,569,55,2,1,2.00,2.00),(1638,569,11,2,5,5.00,25.00),(1639,569,64,2,3,0.60,1.80),(1640,569,69,2,1,0.60,0.60),(1641,570,10,2,1,6.00,6.00),(1642,571,8,2,2,5.00,10.00),(1643,571,136,12,10,5.00,50.00),(1644,571,53,4,3,4.00,12.00),(1645,571,87,2,4,1.20,4.80),(1646,571,61,2,2,1.20,2.40),(1647,571,58,2,2,1.20,2.40),(1648,571,64,2,2,0.60,1.20),(1649,572,80,4,4,3.50,14.00),(1650,572,136,12,6,5.00,30.00),(1651,572,53,4,4,4.00,16.00),(1652,573,3,5,4,10.00,40.00),(1653,573,54,2,1,2.00,2.00),(1654,573,87,2,2,1.20,2.40),(1655,573,58,2,2,1.20,2.40),(1656,574,50,2,1,2.00,2.00),(1657,575,45,2,1,1.50,1.50),(1658,576,136,12,1,5.00,5.00),(1659,576,53,4,1,4.00,4.00),(1660,576,74,2,3,0.40,1.20),(1661,577,136,12,4,5.00,20.00),(1662,577,53,4,2,4.00,8.00),(1663,577,87,2,4,1.20,4.80),(1664,577,92,2,3,2.00,6.00),(1665,578,136,12,4,5.00,20.00),(1666,578,53,4,2,4.00,8.00),(1667,579,10,2,1,6.00,6.00),(1668,580,136,12,3,5.00,15.00),(1669,580,53,4,2,4.00,8.00),(1670,580,52,4,1,4.00,4.00),(1671,580,87,2,2,1.20,2.40),(1672,580,58,2,2,1.20,2.40),(1673,580,59,2,2,1.20,2.40),(1674,580,50,2,2,2.00,4.00),(1675,580,47,2,1,1.50,1.50),(1676,580,116,2,1,1.50,1.50),(1677,580,92,2,4,2.00,8.00),(1678,581,136,12,5,5.00,25.00),(1679,581,53,4,2,4.00,8.00),(1680,581,132,3,1,38.00,38.00),(1681,581,62,2,1,0.60,0.60),(1682,581,53,4,1,4.00,4.00),(1683,582,44,2,1,1.50,1.50),(1684,582,92,2,1,2.00,2.00),(1685,583,136,12,1,5.00,5.00),(1686,583,53,4,1,4.00,4.00),(1687,584,44,2,1,1.50,1.50),(1688,584,54,2,1,2.00,2.00),(1689,585,136,12,4,5.00,20.00),(1690,585,53,4,2,4.00,8.00),(1691,585,3,5,2,10.00,20.00),(1692,585,44,2,2,1.50,3.00),(1693,585,55,2,1,2.00,2.00),(1694,585,44,2,1,1.50,1.50),(1695,585,74,2,3,0.40,1.20),(1696,586,136,12,2,5.00,10.00),(1697,586,53,4,1,4.00,4.00),(1698,586,61,2,2,1.20,2.40),(1699,586,54,2,2,2.00,4.00),(1700,586,44,2,2,1.50,3.00),(1701,587,8,2,1,5.00,5.00),(1702,587,54,2,6,2.00,12.00),(1703,587,116,2,1,1.50,1.50),(1704,587,68,2,1,0.50,0.50),(1705,587,55,2,1,2.00,2.00),(1706,588,136,12,1,5.00,5.00),(1707,588,55,2,2,2.00,4.00),(1708,588,92,2,2,2.00,4.00),(1709,589,54,2,1,2.00,2.00),(1710,590,50,2,1,2.00,2.00),(1711,590,54,2,2,2.00,4.00),(1712,590,92,2,3,2.00,6.00),(1713,592,136,12,2,5.00,10.00),(1714,592,53,4,1,4.00,4.00),(1715,592,47,2,1,1.50,1.50),(1716,593,92,2,1,2.00,2.00),(1717,594,12,2,2,5.00,10.00),(1718,595,136,12,1,5.00,5.00),(1719,595,53,4,1,4.00,4.00),(1720,596,54,2,1,2.00,2.00),(1721,596,12,2,2,5.00,10.00),(1722,596,45,2,1,1.50,1.50),(1723,596,44,2,1,1.50,1.50),(1724,596,50,2,1,2.00,2.00),(1725,596,92,2,1,2.00,2.00),(1726,596,75,2,2,1.50,3.00),(1727,597,136,12,3,5.00,15.00),(1728,597,53,4,2,4.00,8.00),(1729,597,54,2,1,2.00,2.00),(1730,598,52,4,2,4.00,8.00),(1731,598,50,2,2,2.00,4.00),(1732,598,57,2,1,2.20,2.20),(1733,599,55,2,2,2.00,4.00),(1734,600,54,2,5,2.00,10.00),(1735,600,50,2,2,2.00,4.00),(1736,600,92,2,2,2.00,4.00),(1737,601,11,2,3,5.00,15.00),(1738,601,116,2,1,1.50,1.50),(1739,602,8,2,4,5.00,20.00),(1740,603,137,2,1,5.00,5.00),(1741,603,11,2,3,5.00,15.00),(1742,603,58,2,1,1.20,1.20),(1743,604,8,2,6,5.00,30.00),(1744,604,11,2,2,5.00,10.00),(1745,604,75,2,1,1.50,1.50),(1746,604,93,26,1,1.00,1.00),(1747,604,58,2,1,1.20,1.20),(1748,604,58,2,2,1.20,2.40),(1749,605,8,2,2,5.00,10.00),(1750,606,44,2,3,1.50,4.50),(1751,606,11,2,5,5.00,25.00),(1752,606,12,2,2,5.00,10.00),(1753,606,55,2,1,2.00,2.00),(1754,607,55,2,2,2.00,4.00),(1755,608,8,2,4,5.00,20.00),(1756,609,50,2,1,2.00,2.00),(1757,610,137,2,1,5.00,5.00),(1758,610,8,2,1,5.00,5.00),(1759,610,50,2,1,2.00,2.00),(1760,610,143,2,2,0.50,1.00),(1761,611,50,2,2,2.00,4.00),(1762,611,55,2,1,2.00,2.00),(1763,611,54,2,1,2.00,2.00),(1764,612,44,2,1,1.50,1.50),(1765,612,45,2,1,1.50,1.50),(1766,613,54,2,1,2.00,2.00),(1767,613,3,5,4,10.00,40.00),(1768,613,87,2,4,1.20,4.80),(1769,613,44,2,1,1.50,1.50),(1770,614,136,12,2,5.00,10.00),(1771,614,53,4,2,4.00,8.00),(1772,614,93,26,2,1.00,2.00),(1773,614,132,3,2,38.00,76.00),(1774,615,11,2,2,5.00,10.00),(1775,615,136,12,1,5.00,5.00),(1776,615,3,5,1,10.00,10.00),(1777,615,116,2,2,1.50,3.00),(1778,615,44,2,1,1.50,1.50),(1779,615,55,2,1,2.00,2.00),(1780,616,50,2,1,2.00,2.00),(1781,617,50,2,1,2.00,2.00),(1782,617,55,2,1,2.00,2.00),(1783,618,52,4,1,4.00,4.00),(1784,618,50,2,2,2.00,4.00),(1785,618,57,2,1,3.00,3.00),(1786,618,22,5,2,5.00,10.00),(1787,618,133,2,1,2.00,2.00),(1788,618,116,2,1,1.50,1.50),(1789,618,51,2,2,3.50,7.00),(1790,619,93,26,1,1.00,1.00),(1791,620,44,2,3,1.50,4.50),(1792,620,80,4,1,3.50,3.50),(1793,620,50,2,1,2.00,2.00),(1794,620,54,2,4,2.00,8.00),(1795,621,10,2,1,6.00,6.00),(1796,622,136,12,3,5.00,15.00),(1797,622,53,4,2,4.00,8.00),(1798,622,54,2,1,2.00,2.00),(1799,622,55,2,1,2.00,2.00),(1800,622,44,2,3,1.50,4.50),(1801,622,143,2,2,0.50,1.00),(1802,623,18,5,4,6.50,26.00),(1803,623,44,2,3,1.50,4.50),(1804,623,43,5,2,5.00,10.00),(1805,623,87,2,4,1.20,4.80),(1806,623,71,2,2,0.60,1.20),(1807,623,11,2,3,5.00,15.00),(1808,623,59,2,2,1.20,2.40),(1809,624,136,12,8,5.00,40.00),(1810,624,53,4,3,4.00,12.00),(1811,624,56,2,1,2.20,2.20),(1812,624,116,2,2,1.50,3.00),(1813,624,55,2,1,2.00,2.00),(1814,625,50,2,1,2.00,2.00),(1815,626,50,2,1,2.00,2.00),(1816,626,133,2,1,2.00,2.00),(1817,626,75,2,1,1.50,1.50),(1818,626,55,2,1,2.00,2.00),(1819,627,136,12,1,5.00,5.00),(1820,627,53,4,1,4.00,4.00),(1821,627,45,2,2,1.50,3.00),(1822,627,55,2,2,2.00,4.00),(1823,628,136,12,3,5.00,15.00),(1824,628,53,4,2,4.00,8.00),(1825,628,29,13,1,18.00,18.00),(1826,628,44,2,2,1.50,3.00),(1827,629,136,12,6,5.00,30.00),(1828,629,53,4,2,4.00,8.00),(1829,630,136,12,1,5.00,5.00),(1830,630,53,4,1,4.00,4.00),(1831,630,3,5,1,10.00,10.00),(1832,631,136,12,4,5.00,20.00),(1833,631,53,4,2,4.00,8.00),(1834,632,12,2,1,5.00,5.00),(1835,632,8,2,4,5.00,20.00),(1836,632,116,2,1,1.50,1.50),(1837,632,136,12,1,5.00,5.00),(1838,632,55,2,1,2.00,2.00),(1839,633,75,2,1,1.50,1.50),(1840,633,93,26,1,1.00,1.00),(1841,633,44,2,1,1.50,1.50),(1842,633,143,2,2,0.50,1.00),(1843,634,136,12,3,5.00,15.00),(1844,634,53,4,2,4.00,8.00),(1845,635,136,12,1,5.00,5.00),(1846,635,53,4,1,4.00,4.00),(1847,636,54,2,1,2.00,2.00),(1848,636,68,2,3,0.50,1.50),(1849,636,63,2,1,0.60,0.60),(1850,636,93,26,1,1.00,1.00),(1851,636,62,2,1,0.60,0.60),(1852,636,11,2,2,5.00,10.00),(1853,636,44,2,2,1.50,3.00),(1854,636,66,2,2,0.60,1.20),(1855,636,11,2,6,5.00,30.00),(1856,636,116,2,2,1.50,3.00),(1857,637,74,2,1,0.40,0.40),(1858,637,63,2,3,0.60,1.80),(1859,637,57,2,1,3.00,3.00),(1860,637,66,2,2,0.60,1.20),(1861,637,54,2,1,2.00,2.00),(1862,638,11,2,4,5.00,20.00),(1863,638,70,2,3,0.60,1.80),(1864,639,44,2,1,1.50,1.50),(1865,639,54,2,1,2.00,2.00),(1866,639,61,2,2,1.20,2.40),(1867,640,136,12,3,5.00,15.00),(1868,640,136,12,1,5.00,5.00),(1869,640,53,4,1,4.00,4.00),(1870,640,92,2,1,2.00,2.00),(1871,641,44,2,1,1.50,1.50),(1872,642,55,2,1,2.00,2.00),(1873,643,54,2,1,2.00,2.00),(1874,644,3,5,6,10.00,60.00),(1875,644,136,12,2,5.00,10.00),(1876,644,53,4,5,4.00,20.00),(1877,644,87,2,4,1.20,4.80),(1878,644,57,2,1,3.00,3.00),(1879,644,133,2,1,2.00,2.00),(1880,644,55,2,2,2.00,4.00),(1881,644,92,2,2,2.00,4.00),(1882,645,136,12,1,5.00,5.00),(1883,645,53,4,1,4.00,4.00),(1884,645,11,2,1,5.00,5.00),(1885,645,50,2,1,2.00,2.00),(1886,646,11,2,2,5.00,10.00),(1887,646,53,4,1,4.00,4.00),(1888,646,55,2,1,2.00,2.00),(1889,646,75,2,1,1.50,1.50),(1890,646,116,2,1,1.50,1.50),(1891,646,92,2,2,2.00,4.00),(1892,647,52,4,1,4.00,4.00),(1893,648,50,2,1,2.00,2.00),(1894,648,54,2,2,2.00,4.00),(1895,648,116,2,1,1.50,1.50),(1896,648,92,2,1,2.00,2.00),(1897,648,80,4,1,3.50,3.50),(1898,649,136,12,4,5.00,20.00),(1899,649,11,2,3,5.00,15.00),(1900,649,53,4,1,4.00,4.00),(1901,649,58,2,4,1.20,4.80),(1902,649,53,4,2,4.00,8.00),(1903,649,44,2,2,1.50,3.00),(1904,649,50,2,1,2.00,2.00),(1905,649,116,2,1,1.50,1.50),(1906,649,64,2,2,0.60,1.20),(1907,650,50,2,1,2.00,2.00),(1908,650,52,4,1,4.00,4.00),(1909,650,55,2,1,2.00,2.00),(1910,650,59,2,2,1.20,2.40),(1911,651,54,2,1,2.00,2.00),(1912,651,136,12,2,5.00,10.00),(1913,651,53,4,1,4.00,4.00),(1914,651,116,2,1,1.50,1.50),(1915,651,55,2,1,2.00,2.00),(1916,651,68,2,2,0.50,1.00),(1917,652,12,2,1,5.00,5.00),(1918,653,10,2,1,6.00,6.00),(1919,654,12,2,9,5.00,45.00),(1920,654,136,12,2,5.00,10.00),(1921,654,53,4,1,4.00,4.00),(1922,654,44,2,1,1.50,1.50),(1923,655,11,2,4,5.00,20.00),(1924,656,136,12,1,5.00,5.00),(1925,656,53,4,1,4.00,4.00),(1926,656,10,2,1,6.00,6.00),(1927,656,57,2,1,3.00,3.00),(1928,656,45,2,1,1.50,1.50),(1929,656,58,2,1,1.20,1.20),(1930,657,136,12,3,5.00,15.00),(1931,657,53,4,2,4.00,8.00),(1932,657,8,2,3,5.00,15.00),(1933,657,54,2,1,2.00,2.00),(1934,657,50,2,1,2.00,2.00),(1935,658,44,2,2,1.50,3.00),(1936,658,59,2,1,1.20,1.20),(1937,659,136,12,8,5.00,40.00),(1938,659,53,4,3,4.00,12.00),(1939,660,45,2,3,1.50,4.50),(1940,660,59,2,1,1.20,1.20),(1941,661,136,12,1,5.00,5.00),(1942,661,53,4,1,4.00,4.00),(1943,661,93,26,2,1.00,2.00),(1944,661,140,2,1,3.00,3.00),(1945,662,50,2,1,2.00,2.00),(1946,663,54,2,1,2.00,2.00),(1947,663,74,2,4,0.40,1.60),(1948,664,44,2,1,1.50,1.50),(1949,664,54,2,1,2.00,2.00),(1950,664,74,2,2,0.40,0.80),(1951,665,50,2,1,2.00,2.00),(1952,665,92,2,1,2.00,2.00),(1953,666,136,12,3,5.00,15.00),(1954,666,53,4,3,4.00,12.00),(1955,667,52,4,1,4.00,4.00),(1956,667,50,2,2,2.00,4.00),(1957,667,57,2,1,3.00,3.00),(1958,667,54,2,1,2.00,2.00),(1959,667,53,4,1,4.00,4.00),(1960,667,51,2,1,3.50,3.50),(1961,667,61,2,1,1.20,1.20),(1965,668,12,2,4,5.00,20.00),(1966,668,44,2,1,1.50,1.50),(1967,668,45,2,1,1.50,1.50),(1968,669,136,12,3,5.00,15.00),(1969,669,53,4,4,4.00,16.00),(1970,669,8,2,1,5.00,5.00),(1971,669,11,2,1,5.00,5.00),(1972,670,50,2,1,2.00,2.00),(1973,671,45,2,1,1.50,1.50),(1974,672,92,2,2,2.00,4.00),(1975,672,55,2,2,2.00,4.00),(1976,672,11,2,3,5.00,15.00),(1977,672,53,4,1,4.00,4.00),(1978,672,74,2,1,0.40,0.40),(1979,672,75,2,1,1.50,1.50),(1980,672,116,2,2,1.50,3.00),(1981,673,136,12,2,5.00,10.00),(1982,673,92,2,1,2.00,2.00),(1983,674,8,2,4,5.00,20.00),(1984,674,59,2,3,1.20,3.60),(1985,675,8,2,2,5.00,10.00),(1986,675,58,2,1,1.20,1.20),(1987,675,59,2,1,1.20,1.20),(1988,675,116,2,2,1.50,3.00),(1989,675,136,12,1,5.00,5.00),(1990,676,136,12,4,5.00,20.00),(1991,676,96,24,2,5.00,10.00),(1992,677,92,2,1,2.00,2.00),(1993,677,44,2,1,1.50,1.50),(1994,678,54,2,3,2.00,6.00),(1995,678,136,12,3,5.00,15.00),(1996,678,44,2,3,1.50,4.50),(1997,678,8,2,1,5.00,5.00),(1998,678,45,2,1,1.50,1.50),(1999,678,55,2,1,2.00,2.00),(2000,678,63,2,3,0.60,1.80),(2001,678,44,2,3,1.50,4.50),(2002,678,54,2,2,2.00,4.00),(2003,679,96,24,2,5.00,10.00),(2004,680,11,2,4,5.00,20.00),(2005,681,54,2,1,2.00,2.00),(2006,681,11,2,2,5.00,10.00),(2007,681,136,12,2,5.00,10.00),(2008,681,53,4,1,4.00,4.00),(2009,681,93,26,1,1.00,1.00),(2010,681,45,2,1,1.50,1.50),(2011,681,44,2,1,1.50,1.50),(2012,682,10,2,1,6.00,6.00),(2013,683,136,12,3,5.00,15.00),(2014,683,53,4,1,4.00,4.00),(2015,684,136,12,2,5.00,10.00),(2016,684,59,2,3,1.20,3.60),(2017,685,8,2,1,5.00,5.00),(2018,685,59,2,3,1.20,3.60),(2019,685,93,26,1,1.00,1.00),(2020,685,44,2,1,1.50,1.50),(2021,686,44,2,1,1.50,1.50),(2022,686,45,2,1,1.50,1.50),(2023,687,11,2,1,5.00,5.00),(2024,687,59,2,2,1.20,2.40),(2025,688,11,2,3,5.00,15.00),(2026,689,136,12,3,5.00,15.00),(2027,689,53,4,1,4.00,4.00),(2028,690,54,2,1,2.00,2.00),(2029,690,92,2,1,2.00,2.00),(2033,691,44,2,2,1.50,3.00),(2034,691,45,2,1,1.50,1.50),(2035,691,92,2,1,2.00,2.00),(2036,692,44,2,3,1.50,4.50),(2037,692,92,2,1,2.00,2.00),(2038,693,136,12,3,5.00,15.00),(2039,693,80,4,4,3.50,14.00),(2040,693,53,4,2,4.00,8.00),(2041,694,28,13,1,18.00,18.00),(2042,695,50,2,1,2.00,2.00),(2043,696,54,2,1,2.00,2.00),(2044,696,44,2,1,1.50,1.50),(2045,697,44,2,2,1.50,3.00),(2046,698,8,2,1,5.00,5.00),(2047,698,96,24,2,5.00,10.00),(2048,698,53,4,2,4.00,8.00),(2049,698,45,2,1,1.50,1.50),(2050,698,44,2,1,1.50,1.50),(2051,698,3,5,1,10.00,10.00),(2052,699,11,2,3,5.00,15.00),(2053,700,11,2,2,5.00,10.00),(2054,701,56,2,1,2.20,2.20),(2055,701,61,2,1,1.20,1.20),(2056,701,62,2,1,0.60,0.60),(2057,701,74,2,1,0.40,0.40),(2058,702,11,2,3,5.00,15.00),(2059,702,62,2,1,0.60,0.60),(2060,702,136,12,2,5.00,10.00),(2061,702,136,12,1,5.00,5.00),(2062,702,53,4,2,4.00,8.00),(2063,702,50,2,2,2.00,4.00),(2064,702,11,2,1,5.00,5.00),(2065,703,44,2,3,1.50,4.50),(2066,703,59,2,4,1.20,4.80),(2067,703,44,2,1,1.50,1.50),(2068,703,12,2,4,5.00,20.00),(2069,703,58,2,1,1.20,1.20),(2070,703,61,2,1,1.20,1.20),(2071,704,11,2,4,5.00,20.00),(2072,704,44,2,2,1.50,3.00),(2073,705,11,2,4,5.00,20.00),(2074,706,12,2,5,5.00,25.00),(2075,706,47,2,1,1.50,1.50),(2076,707,104,2,1,4.00,4.00),(2077,707,44,2,2,1.50,3.00),(2078,708,137,2,2,5.00,10.00),(2079,708,44,2,1,1.50,1.50),(2080,708,116,2,1,1.50,1.50),(2081,711,58,2,16,10.00,160.00),(2082,711,58,2,20,10.00,200.00),(2083,711,58,2,17,10.00,170.00),(2084,712,59,2,100,50.00,5000.00),(2085,713,59,2,300,30.00,9000.00),(2086,713,12,2,10,14.00,140.00),(2087,713,59,2,100,50.00,5000.00),(2088,714,59,2,100,50.00,5000.00),(2089,725,53,0,100,4.00,400.00),(2090,726,7,0,100,4.00,400.00),(2091,726,59,0,200,1.20,240.00),(2092,727,59,0,70,1.20,84.00),(2093,727,59,0,500,1.20,600.00),(2094,728,53,0,79,4.00,316.00),(2095,729,59,0,100,12.00,1200.00),(2096,730,59,0,100,300.00,30000.00),(2097,731,59,0,90,300.00,27000.00),(2098,732,59,0,100,300.00,30000.00),(2099,733,59,0,12,12.00,144.00),(2100,734,59,0,20,300.00,6000.00),(2101,734,12,0,200,5.00,1000.00),(2102,735,59,0,1000,300.00,300000.00),(2103,736,0,0,100,4.00,400.00),(2104,736,0,0,200,1.20,240.00),(2105,738,0,0,100,4.00,400.00),(2106,739,0,0,100,4.00,400.00),(2107,740,0,0,100,4.00,400.00),(2108,741,0,0,100,4.00,400.00),(2109,742,0,0,100,4.00,400.00),(2110,742,59,0,20,12.00,240.00),(2111,743,0,0,100,4.00,400.00),(2112,744,0,0,100,4.00,400.00),(2113,745,0,0,100,4.00,400.00),(2114,746,0,0,100,4.00,400.00),(2115,747,0,0,100,4.00,400.00),(2116,748,0,0,100,4.00,400.00),(2117,749,0,0,100,4.00,400.00),(2118,750,0,0,100,4.00,400.00),(2119,751,59,0,100,300.00,30000.00),(2123,753,59,0,30,300.00,9000.00),(2124,754,59,0,20,300.00,6000.00),(2125,755,12,0,10,5.00,50.00),(2126,756,12,0,122,5.00,610.00),(2127,757,12,1,200,5.00,1000.00),(2128,758,59,1,100,12.00,1200.00),(2129,759,59,1,0,12.00,0.00),(2130,760,59,29,20,12.00,240.00),(2131,761,59,30,20,300.00,6000.00),(2132,762,63,2,10,20.00,200.00),(2133,763,76,2,26,10.00,260.00),(2134,763,74,2,200,10.00,2000.00),(2135,763,63,2,1000,35.00,35000.00),(2136,764,77,2,20,15.00,300.00),(2137,764,76,2,10,15.00,300.00),(2138,765,77,2,30,15.00,450.00),(2139,766,77,30,1,0.00,0.00),(2140,767,77,29,1,0.00,0.00),(2141,768,77,31,1,0.00,0.00),(2142,768,77,32,1,0.00,0.00),(2143,768,77,30,1,0.00,0.00),(2144,768,77,29,1,0.00,0.00),(2145,768,77,2,1,0.00,0.00),(2146,769,77,2,1,15.00,15.00),(2147,769,77,29,1,100.00,100.00),(2148,769,77,30,1,1000.00,1000.00),(2149,769,77,31,1,60.00,60.00),(2150,769,77,32,1,30.00,30.00),(2151,770,76,2,10,10.00,100.00),(2152,770,77,29,1,100.00,100.00),(2153,771,5,2,10,10.00,100.00),(2154,772,64,2,120,120.00,14400.00);
/*!40000 ALTER TABLE `pedido_detalle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `precioventa`
--

DROP TABLE IF EXISTS `precioventa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `precioventa` (
  `PrecioVenta_Id` int(11) NOT NULL AUTO_INCREMENT,
  `ProdServ_Id` int(11) NOT NULL,
  `UnidadMedida_Id` int(11) NOT NULL,
  `Moneda_Id` tinyint(4) NOT NULL,
  `PreVenPrecio` decimal(10,2) NOT NULL,
  `dPreVenFechaAct` datetime NOT NULL,
  `nPreVenEliminado` tinyint(4) NOT NULL,
  `Usuario_Id` int(11) NOT NULL,
  PRIMARY KEY (`PrecioVenta_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=199 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `precioventa`
--

LOCK TABLES `precioventa` WRITE;
/*!40000 ALTER TABLE `precioventa` DISABLE KEYS */;
INSERT INTO `precioventa` VALUES (1,7,2,0,6.00,'2016-03-03 23:17:56',1,17),(2,6,2,0,6.00,'2016-03-03 23:19:01',1,17),(3,11,2,0,5.00,'2016-03-03 23:19:24',1,17),(4,13,2,0,5.00,'2016-03-03 23:19:51',0,17),(5,8,2,0,5.00,'2016-03-03 23:21:00',1,17),(6,12,2,0,5.00,'2016-03-03 23:21:28',1,17),(7,10,2,0,4.00,'2016-03-03 23:22:07',1,17),(8,9,2,0,4.00,'2016-03-03 23:22:28',1,17),(9,80,4,0,3.50,'2016-03-03 23:25:52',0,17),(10,39,3,0,30.00,'2016-03-31 18:53:08',1,17),(11,39,5,0,7.00,'2016-03-31 18:53:20',1,17),(12,52,2,0,5.00,'2016-03-31 20:11:29',1,19),(13,12,0,0,0.00,'2016-04-04 09:56:04',0,19),(14,12,0,0,0.00,'2016-04-04 09:56:08',0,19),(15,12,0,0,0.00,'2016-04-04 09:56:10',0,19),(16,80,0,0,0.00,'2016-04-04 09:57:18',0,19),(17,80,0,0,0.00,'2016-04-04 09:57:20',0,19),(18,55,0,0,2.00,'2016-04-04 09:58:00',0,19),(19,55,2,0,2.00,'2016-04-04 09:58:20',1,19),(20,55,0,0,0.00,'2016-04-04 09:58:37',0,19),(21,2,3,0,120.00,'2016-04-04 09:59:51',1,19),(22,2,5,0,7.50,'2016-04-04 10:00:24',0,19),(23,5,3,0,120.00,'2016-04-04 10:04:03',1,19),(24,5,5,0,7.50,'2016-04-04 10:04:12',0,19),(25,14,3,0,80.00,'2016-04-04 10:05:58',0,19),(26,14,5,0,5.00,'2016-04-04 10:06:18',0,19),(27,15,12,0,5.00,'2016-04-04 10:06:50',0,19),(28,16,5,0,4.00,'2016-04-04 10:08:00',0,19),(29,16,0,0,0.00,'2016-04-04 10:08:04',0,19),(30,17,5,0,4.00,'2016-04-04 10:09:29',0,19),(31,18,5,0,6.50,'2016-04-04 10:09:49',0,19),(32,21,5,0,4.00,'2016-04-04 10:10:24',0,19),(33,22,5,0,5.00,'2016-04-04 10:11:39',0,19),(34,23,5,0,6.50,'2016-04-04 10:17:21',0,19),(35,25,3,0,58.00,'2016-04-04 10:18:02',0,19),(36,26,3,0,38.00,'2016-04-04 10:18:34',0,19),(37,31,13,0,18.00,'2016-04-04 10:19:09',0,19),(38,32,13,0,18.00,'2016-04-04 10:19:27',0,19),(39,34,13,0,22.00,'2016-04-04 10:19:49',0,19),(40,37,2,0,10.00,'2016-04-04 10:24:02',0,19),(41,28,13,0,18.00,'2016-04-04 10:24:45',0,19),(42,38,5,0,5.00,'2016-04-04 10:25:35',0,19),(43,39,5,0,4.00,'2016-04-04 10:27:20',1,19),(44,39,0,0,0.00,'2016-04-04 10:27:24',0,19),(45,40,5,0,5.00,'2016-04-04 10:28:07',0,19),(46,41,5,0,10.00,'2016-04-04 10:28:33',0,19),(47,42,5,0,5.00,'2016-04-04 10:28:56',0,19),(48,43,5,0,5.00,'2016-04-04 10:29:23',0,19),(49,54,2,0,2.00,'2016-04-04 10:29:48',1,19),(50,44,2,0,1.50,'2016-04-04 10:30:41',0,19),(51,45,2,0,1.50,'2016-04-04 10:31:04',0,19),(52,46,2,0,1.50,'2016-04-04 10:33:09',0,19),(53,47,2,0,1.50,'2016-04-04 10:33:37',0,19),(54,50,2,0,2.00,'2016-04-04 10:34:04',0,19),(55,51,2,0,3.50,'2016-04-04 10:34:17',0,19),(56,52,2,0,4.00,'2016-04-04 10:34:38',0,19),(57,53,2,0,4.00,'2016-04-04 10:34:55',1,19),(58,68,2,0,0.50,'2016-04-04 10:35:24',0,19),(59,69,2,0,0.60,'2016-04-04 10:36:06',0,19),(60,70,2,0,0.60,'2016-04-04 10:36:22',0,19),(61,71,2,0,0.60,'2016-04-04 10:36:44',0,19),(62,72,0,0,0.80,'2016-04-04 10:37:02',0,19),(63,72,2,0,0.80,'2016-04-04 10:37:11',0,19),(64,73,2,0,0.80,'2016-04-04 10:37:30',0,19),(65,74,2,0,0.40,'2016-04-04 10:38:30',1,19),(66,75,2,0,1.50,'2016-04-04 10:39:01',1,19),(67,75,0,0,0.00,'2016-04-04 10:39:04',0,19),(68,56,2,0,2.20,'2016-04-04 10:39:32',1,19),(69,57,2,0,2.20,'2016-04-04 10:39:54',1,19),(70,58,2,0,1.20,'2016-04-04 10:40:16',1,19),(71,59,2,0,1.20,'2016-04-04 10:40:38',1,19),(72,60,2,0,1.20,'2016-04-04 10:41:13',1,19),(73,61,2,0,1.20,'2016-04-04 10:41:43',1,19),(74,62,2,0,0.60,'2016-04-04 10:42:25',0,19),(75,63,2,0,0.60,'2016-04-04 10:43:09',1,19),(76,64,2,0,0.60,'2016-04-04 10:43:57',1,19),(77,65,2,0,0.60,'2016-04-04 10:44:35',0,19),(78,66,2,0,0.60,'2016-04-04 10:45:19',0,19),(79,81,3,0,120.00,'2016-04-04 11:15:38',0,19),(80,81,5,0,7.50,'2016-04-04 11:16:01',0,19),(81,82,13,0,18.00,'2016-04-04 11:23:58',0,19),(82,83,13,0,18.00,'2016-04-04 11:32:34',0,19),(83,84,2,0,1.00,'2016-04-04 11:33:15',0,19),(84,86,2,0,1.10,'2016-04-04 11:34:25',0,19),(85,88,2,0,0.60,'2016-04-04 11:34:53',0,19),(86,89,2,0,1.00,'2016-04-04 11:37:03',0,19),(87,90,2,0,2.00,'2016-04-04 11:38:53',0,19),(88,92,2,0,2.00,'2016-04-04 11:39:53',0,19),(89,91,2,0,2.00,'2016-04-04 11:40:17',0,19),(90,85,2,0,1.20,'2016-04-04 11:42:00',1,19),(91,112,2,0,0.60,'2016-04-30 12:38:11',0,19),(92,53,4,0,4.00,'2016-04-30 12:50:36',0,19),(93,52,4,0,4.00,'2016-04-30 12:51:03',0,19),(94,93,26,0,1.00,'2016-04-30 12:53:56',0,19),(95,97,23,0,10.00,'2016-04-30 12:54:48',0,19),(96,97,0,0,0.00,'2016-04-30 12:55:01',0,19),(97,115,3,0,38.00,'2016-05-06 11:15:24',0,19),(98,96,24,0,5.00,'2016-05-07 10:36:18',0,19),(99,87,2,0,1.20,'2016-05-07 10:43:42',1,19),(100,87,0,0,0.00,'2016-05-07 10:43:47',0,19),(101,68,0,0,0.00,'2016-05-07 11:57:18',0,19),(102,102,2,0,0.60,'2016-05-07 12:00:59',0,19),(103,29,0,0,18.00,'2016-05-07 12:24:36',0,19),(104,29,0,0,0.00,'2016-05-07 12:24:41',0,19),(105,29,0,0,0.00,'2016-05-07 12:24:42',0,19),(106,29,13,0,18.00,'2016-05-07 12:25:48',0,19),(107,116,2,0,1.50,'2016-05-07 13:13:19',0,19),(108,39,5,0,5.00,'2016-05-24 11:51:10',0,19),(109,39,0,0,0.00,'2016-05-24 11:51:13',0,19),(110,39,0,0,0.00,'2016-05-24 11:51:17',0,19),(111,3,3,0,160.00,'2016-05-25 11:22:36',1,19),(112,3,5,0,10.00,'2016-05-25 11:23:08',0,19),(113,3,0,0,0.00,'2016-05-25 11:23:16',0,19),(114,117,2,0,2.20,'2016-05-25 12:23:43',0,19),(115,67,2,0,3.50,'2016-06-15 18:22:10',0,19),(116,108,2,0,1.50,'2016-06-15 18:37:54',0,19),(117,6,0,0,0.00,'2016-06-16 11:03:20',0,19),(118,7,2,0,4.00,'2016-06-16 11:03:37',1,19),(119,94,5,0,5.00,'2016-06-20 13:14:22',0,19),(120,53,0,0,0.00,'2016-06-22 10:52:52',0,19),(121,53,0,0,0.00,'2016-06-22 10:52:59',0,19),(122,120,4,0,3.50,'2016-06-22 10:55:23',0,19),(123,122,2,0,2.50,'2016-06-22 11:04:36',0,19),(124,123,2,0,1.00,'2016-06-22 11:31:56',0,19),(125,124,2,0,0.50,'2016-06-22 11:45:05',0,19),(126,124,0,0,0.00,'2016-06-22 11:45:21',0,19),(127,125,3,0,70.00,'2016-06-22 11:56:28',0,19),(128,126,2,0,0.50,'2016-06-23 11:22:39',0,19),(129,128,2,0,0.50,'2016-06-24 10:52:34',0,19),(130,129,2,0,4.00,'2016-06-24 12:31:04',0,19),(131,131,2,0,1.10,'2016-06-24 12:34:10',0,19),(132,132,3,0,38.00,'2016-08-10 10:17:13',0,19),(133,133,2,0,2.00,'2016-08-10 10:31:00',0,19),(134,134,2,0,1.50,'2016-08-10 10:35:46',0,19),(135,15,27,0,2.50,'2016-08-16 10:51:27',0,18),(136,136,12,0,5.00,'2016-08-23 11:38:20',0,20),(137,136,27,0,2.50,'2016-08-23 11:38:26',0,20),(138,137,23,0,5.00,'2016-08-23 11:47:33',1,20),(139,138,13,0,18.00,'2016-09-19 11:36:45',0,19),(140,137,2,0,5.00,'2016-09-21 09:31:57',0,20),(141,10,2,0,5.00,'2016-09-21 09:34:48',1,20),(142,10,2,0,6.00,'2016-09-21 09:37:35',1,20),(143,57,2,0,3.00,'2016-10-06 12:20:09',0,19),(144,141,2,0,1.50,'2016-10-10 11:36:03',0,19),(145,140,2,0,3.00,'2016-10-10 11:36:27',0,19),(146,145,2,0,1.50,'2016-10-10 11:36:41',0,19),(147,143,2,0,0.50,'2016-10-10 11:36:58',0,19),(148,144,2,0,10.00,'2016-10-10 11:37:18',0,19),(149,104,2,0,4.00,'2016-10-21 12:12:39',0,19),(150,5,2,0,50.00,'2017-01-17 00:00:35',1,1),(151,59,29,0,12.00,'2017-01-17 23:08:06',1,1),(152,59,30,0,300.00,'2017-01-17 23:08:16',1,1),(153,11,34,1,1.00,'2017-01-23 10:32:29',1,1),(154,11,34,1,10.00,'2017-01-23 10:37:22',1,1),(155,11,34,2,3.00,'2017-01-23 10:50:32',1,1),(156,11,34,1,15.00,'2017-01-23 11:01:52',1,1),(157,11,34,1,8.00,'2017-01-23 11:05:59',1,1),(158,11,34,1,12.00,'2017-01-23 11:15:13',1,1),(159,11,34,2,3.50,'2017-01-23 11:15:36',1,1),(160,12,2,1,1.00,'2017-01-23 11:34:51',1,1),(161,12,2,2,0.30,'2017-01-23 11:35:04',1,1),(162,11,34,1,1.00,'2017-01-23 11:38:00',1,1),(163,11,34,1,10.00,'2017-01-23 11:43:49',1,1),(164,11,34,2,11.00,'2017-01-23 11:46:12',1,1),(165,11,34,1,9.00,'2017-01-23 12:08:54',1,1),(166,12,2,1,89.00,'2017-01-23 12:11:45',1,1),(167,11,34,1,7.00,'2017-01-23 12:13:34',1,1),(168,11,34,1,78.00,'2017-01-23 12:18:25',1,1),(169,11,34,1,98.00,'2017-01-23 12:20:14',1,1),(170,11,34,1,10.00,'2017-01-23 14:18:47',1,1),(171,11,34,2,23.00,'2017-01-23 14:19:17',0,1),(172,11,34,2,43.00,'2017-01-23 14:19:38',1,1),(173,11,34,1,234.00,'2017-01-23 14:19:50',1,1),(174,61,2,1,10.00,'2017-01-23 22:06:16',0,1),(175,61,2,2,3.50,'2017-01-23 22:09:07',0,1),(176,61,29,1,100.00,'2017-01-24 01:23:35',0,1),(177,61,30,1,1000.00,'2017-01-24 01:23:58',0,1),(178,61,29,2,35.00,'2017-01-24 01:24:20',0,1),(179,61,30,2,340.00,'2017-01-24 01:24:48',0,1),(180,63,2,1,20.00,'2017-01-24 09:32:12',0,1),(181,63,2,2,8.00,'2017-01-24 09:32:33',0,1),(182,63,2,1,35.00,'2017-01-24 09:32:50',0,1),(183,63,2,2,20.00,'2017-01-24 09:41:44',0,1),(184,74,2,1,10.00,'2017-01-24 10:19:29',0,1),(185,74,2,1,5.00,'2017-01-24 10:19:38',1,1),(186,76,2,1,10.00,'2017-01-24 14:02:32',0,1),(187,76,2,2,3.50,'2017-01-24 14:02:41',0,1),(188,76,2,1,8.00,'2017-01-24 14:02:57',1,1),(189,75,33,2,2.00,'2017-01-24 14:03:57',0,1),(190,75,33,1,7.00,'2017-01-24 14:12:08',0,1),(191,77,2,1,15.00,'2017-01-28 09:51:55',0,1),(192,11,34,1,50.00,'2017-01-30 11:32:58',0,1),(193,64,2,1,2.00,'2017-01-30 11:34:23',0,1),(194,64,2,2,33.00,'2017-01-30 11:36:45',0,1),(195,77,29,1,100.00,'2017-01-30 14:52:34',0,1),(196,77,30,1,1000.00,'2017-01-30 14:52:42',0,1),(197,77,31,1,60.00,'2017-01-30 14:52:51',0,1),(198,77,32,1,30.00,'2017-01-30 14:53:02',0,1);
/*!40000 ALTER TABLE `precioventa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prodserv`
--

DROP TABLE IF EXISTS `prodserv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prodserv` (
  `ProdServ_Id` int(11) NOT NULL AUTO_INCREMENT,
  `sProSrvCodigo` varchar(50) NOT NULL,
  `sProSrvNombre` varchar(50) NOT NULL,
  `sProSrvCodigoBarras` varchar(50) NOT NULL,
  `nProSrvFamilia_Id` int(11) NOT NULL,
  `Activo_Id` int(11) NOT NULL,
  `nProSrvUnidad_Id` int(11) NOT NULL,
  `nProSrvExoneradoIGV` tinyint(4) NOT NULL,
  `nProSrvEstado` tinyint(4) NOT NULL,
  `nProSrvEliminado` int(11) NOT NULL,
  `dProSrvFecha_Act` datetime NOT NULL,
  `Usuario_Id` int(11) NOT NULL,
  `ProSrvEspecificacion` text NOT NULL,
  `ProSrvImagen` mediumblob,
  `ProSrvNomImagen` varchar(255) DEFAULT NULL,
  `OrigenDestino_Id` int(11) DEFAULT NULL,
  PRIMARY KEY (`ProdServ_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prodserv`
--

LOCK TABLES `prodserv` WRITE;
/*!40000 ALTER TABLE `prodserv` DISABLE KEYS */;
INSERT INTO `prodserv` VALUES (1,'1.1-1','POLO 2 COLORES  PLOMO/NARANJA T-S','',14,1,2,0,1,0,'2016-11-20 12:18:43',1,'',NULL,NULL,NULL),(2,'1.1-2','POLO 2 COLORES  PLOMO/NARANJA T-M','',14,1,2,0,1,0,'2016-11-20 12:19:10',1,'',NULL,NULL,NULL),(3,'1.1-3','POLO 2 COLORES  PLOMO/NARANJA T-L','',14,1,2,0,1,0,'2016-11-20 12:19:24',1,'',NULL,NULL,NULL),(4,'1.1-4','POLO 2 COLORES  PLOMO/NARANJA T-XL','',14,1,2,0,1,0,'2016-11-20 12:19:34',1,'',NULL,NULL,NULL),(5,'1.1-5','PANTALON JEAN CINTA REFLECTIVA T-28','',14,1,2,0,1,0,'2016-11-21 11:38:53',2,'',NULL,NULL,NULL),(6,'1.1-6','PANTALON JEAN CINTA REFLECTIVA T-30','',14,1,2,0,1,0,'2016-11-20 12:20:22',1,'',NULL,NULL,NULL),(7,'1.1-7','PANTALON JEAN CINTA REFLECTIVA T-32','',14,1,2,0,1,0,'2016-11-20 12:20:32',1,'',NULL,NULL,NULL),(8,'1.1-8','PANTALON JEAN CINTA REFLECTIVA T-34','',14,1,2,0,1,0,'2016-11-20 12:20:41',1,'',NULL,NULL,NULL),(9,'1.1-9','PANTALON JEAN CINTA REFLECTIVA T-36','',14,1,2,0,1,0,'2016-11-20 12:20:52',1,'',NULL,NULL,NULL),(10,'1.2-1','DRILL 384 BEIGE x 1.6','',15,1,33,0,1,0,'2016-11-21 11:35:51',2,'',NULL,NULL,NULL),(11,'1.2-2','ALGODÓN PIQUE 24/1 MELANGE','',15,1,34,0,1,0,'2017-01-29 09:39:45',1,'',NULL,NULL,NULL),(12,'1.2-3','BOTÓN 4HUECOS BEIGE','',15,1,2,0,1,0,'2016-11-21 11:36:54',2,'',NULL,NULL,NULL),(52,'1.2-4','TELAS FINAS','',15,1,33,0,1,1,'2016-12-22 14:52:26',1,'Mucha tela',NULL,'1.2-4.jpg',NULL),(53,'1.1-10','TELAS GRUESAS','',14,1,2,0,1,0,'2016-12-22 15:36:28',1,'estas son las telas',NULL,'1.1-10.jpg',NULL),(54,'1.2-5','rtghy','',15,1,2,0,1,0,'2016-12-22 15:47:31',1,'ergber',NULL,'1.2-5.jpg',NULL),(55,'1.2-6','erter','',15,1,33,0,1,0,'2017-01-19 21:29:18',1,'1232',NULL,'1.2-6.jpg',NULL),(56,'1.2-7','ggeruy','',15,1,2,0,1,0,'2016-12-22 15:56:48',1,'ertuy',NULL,'1.2-7.jpg',NULL),(57,'1.2-8','fertg','',15,1,2,0,1,0,'2016-12-22 16:11:45',1,'rcxs',NULL,'1.2-8.jpg',NULL),(58,'1.2-9','termin','',15,1,2,0,1,1,'2016-12-22 16:16:47',1,'erthg',NULL,'1.2-9.jpg',NULL),(59,'1.1-11','Polo','',14,1,2,0,1,0,'2017-01-17 00:32:58',1,'',NULL,NULL,NULL),(60,'1.2-10','pep','',15,1,2,0,1,0,'2017-01-23 15:05:12',1,'',NULL,NULL,1),(61,'1.2-11','home','',15,1,2,0,1,0,'2017-01-23 20:10:44',1,'',NULL,NULL,5),(62,'1.2-12','Pro','',15,1,33,0,1,0,'2017-01-23 20:11:09',1,'',NULL,NULL,1),(63,'1.1-12','prenda','',14,1,2,0,1,0,'2017-01-24 08:51:11',1,'',NULL,NULL,1),(64,'1.1-12','pantalon','',14,1,2,0,1,0,'2017-01-24 08:58:17',1,'',NULL,NULL,1),(65,'1.1-13','polo','',14,1,2,0,1,0,'2017-01-24 09:02:42',1,'',NULL,NULL,1),(74,'1.1-14','Short','',14,1,2,0,1,0,'2017-01-24 10:18:42',1,'',NULL,NULL,1),(75,'1.2-13','hom1','',15,1,33,0,1,0,'2017-01-24 10:39:03',1,'',NULL,NULL,5),(76,'1.1-15','Polo','',14,1,2,0,1,0,'2017-01-24 14:02:10',1,'',NULL,NULL,1),(77,'1.1-16','Prueba 1','',14,1,2,0,1,0,'2017-01-29 09:41:11',1,'',NULL,NULL,NULL);
/*!40000 ALTER TABLE `prodserv` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productolote`
--

DROP TABLE IF EXISTS `productolote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productolote` (
  `ProductoLote_Id` int(11) NOT NULL AUTO_INCREMENT,
  `ProdServ_Id` int(11) NOT NULL,
  `Almacen_Id` int(11) DEFAULT NULL,
  `sProLNombreLote` varchar(15) DEFAULT NULL,
  `nProLStockReal` double DEFAULT NULL,
  `dProLFechaVencimiento` datetime DEFAULT NULL,
  `nProLPrecioL` double DEFAULT NULL,
  `nProLPrecioC` double DEFAULT NULL,
  `nProLTipoPrecio` tinyint(4) DEFAULT NULL,
  `nProLPorc1` double DEFAULT NULL,
  `nProLPrecioV` double DEFAULT NULL,
  `nProLPorc2` double DEFAULT NULL,
  `nProLPrecioV2` double DEFAULT NULL,
  `nProLPorc3` double DEFAULT NULL,
  `nProLPrecioV3` double DEFAULT NULL,
  `dProLFecha_Act` datetime NOT NULL,
  `nProLUsuario_Id` int(11) NOT NULL,
  `nProLMonedaCompra_Id` int(11) DEFAULT '1',
  `nProLDescuentoCompra` double DEFAULT NULL,
  PRIMARY KEY (`ProductoLote_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productolote`
--

LOCK TABLES `productolote` WRITE;
/*!40000 ALTER TABLE `productolote` DISABLE KEYS */;
INSERT INTO `productolote` VALUES (1,56,1,'20161228105618',120,NULL,2.11864406779658,2.11864406779658,NULL,NULL,0,NULL,0,NULL,0,'2016-12-28 22:56:18',1,1,0),(2,58,1,'20161228110338',16,NULL,33.898305084746,33.898305084746,NULL,NULL,0,NULL,0,NULL,0,'2016-12-29 00:00:00',1,1,0),(3,58,1,'20161229104016',3,NULL,16.949152542373,16.949152542373,NULL,NULL,0,NULL,0,NULL,0,'2016-12-29 00:00:00',1,1,0),(4,12,1,'20170127115636',3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-01-27 11:57:00',1,1,0),(5,53,1,'20170127115659',3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-01-27 11:59:00',1,1,0);
/*!40000 ALTER TABLE `productolote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `receta`
--

DROP TABLE IF EXISTS `receta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `receta` (
  `Receta_Id` int(11) NOT NULL AUTO_INCREMENT,
  `nRecProdServ_Id` int(11) NOT NULL,
  `UnidadMedida_Id` int(11) NOT NULL,
  `nRecCantidad` int(11) NOT NULL,
  `nRecEliminado` tinyint(4) NOT NULL,
  `dRecFecha_Act` datetime NOT NULL,
  `Usuario_Id` int(11) NOT NULL,
  `ProdServ_Id` int(11) NOT NULL,
  PRIMARY KEY (`Receta_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `receta`
--

LOCK TABLES `receta` WRITE;
/*!40000 ALTER TABLE `receta` DISABLE KEYS */;
INSERT INTO `receta` VALUES (1,7,3,3,0,'2016-01-05 13:30:55',17,13),(2,5,6,6,0,'2016-01-05 13:30:55',17,13),(3,3,10,6,1,'2016-01-05 13:30:55',17,13),(4,7,2,2,1,'2016-01-05 13:30:55',17,13),(5,7,2,1,0,'2016-01-05 13:30:55',17,13),(6,7,3,2,0,'2016-01-05 13:30:55',17,13),(7,7,2,2,0,'2016-01-05 13:30:55',17,13),(8,13,4,1,0,'2016-01-05 16:37:14',17,9),(9,13,7,2,0,'2016-01-05 16:48:24',17,14),(10,2,6,3,0,'2016-01-05 16:48:24',17,14),(11,12,6,2,0,'2016-01-05 16:50:52',17,15),(12,14,6,2,1,'2016-01-07 11:46:41',17,77),(13,51,2,1,0,'2016-01-07 11:46:41',17,77),(14,22,6,2,0,'2016-01-07 11:48:01',17,78),(15,52,2,1,0,'2016-01-07 11:48:01',17,78),(16,16,6,2,1,'2016-01-07 11:49:01',17,79),(17,44,2,1,0,'2016-01-07 11:49:01',17,79),(18,16,5,1,0,'2016-01-11 11:38:05',17,79),(19,14,5,1,1,'2016-01-11 12:29:10',17,77),(20,10,33,1,0,'2016-11-21 11:38:53',2,5),(21,12,2,1,0,'2016-11-21 11:38:53',2,5),(22,11,34,1,0,'2016-11-21 11:38:53',2,5),(23,12,2,4,0,'2017-01-17 00:32:58',1,59),(24,12,2,4,0,'2017-01-27 11:53:50',1,77),(25,53,2,3,0,'2017-01-27 11:53:50',1,77),(26,12,2,3,0,'2017-01-27 11:53:50',1,76),(27,53,2,2,0,'2017-01-27 11:53:50',1,76),(28,53,2,3,0,'2017-01-29 09:39:45',1,11);
/*!40000 ALTER TABLE `receta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipomovimiento`
--

DROP TABLE IF EXISTS `tipomovimiento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipomovimiento` (
  `TipoMovimiento_Id` int(11) NOT NULL AUTO_INCREMENT,
  `sTMovNombre` varchar(50) NOT NULL,
  `nTMovTipoOD` int(11) NOT NULL,
  `nTMovSentido` tinyint(4) NOT NULL,
  `nTMovVisible` tinyint(4) NOT NULL,
  PRIMARY KEY (`TipoMovimiento_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipomovimiento`
--

LOCK TABLES `tipomovimiento` WRITE;
/*!40000 ALTER TABLE `tipomovimiento` DISABLE KEYS */;
INSERT INTO `tipomovimiento` VALUES (1,'COMPRA',1,1,1),(2,'VENTA',2,2,1),(3,'INGRESO',1,1,1),(4,'SALIDA',1,2,1);
/*!40000 ALTER TABLE `tipomovimiento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unidadmedida`
--

DROP TABLE IF EXISTS `unidadmedida`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unidadmedida` (
  `UnidadMedida_Id` int(11) NOT NULL AUTO_INCREMENT,
  `sUndDescripcion` varchar(60) NOT NULL,
  `sUndAlias` varchar(20) NOT NULL,
  `nUndPadre_Id` int(11) NOT NULL,
  `nUndFac_Cnv` double NOT NULL,
  `nUndEstado` tinyint(4) NOT NULL,
  `nUndEliminado` int(11) NOT NULL,
  `dUndFecha_Act` datetime NOT NULL,
  `Usuario_Id` int(11) NOT NULL,
  PRIMARY KEY (`UnidadMedida_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unidadmedida`
--

LOCK TABLES `unidadmedida` WRITE;
/*!40000 ALTER TABLE `unidadmedida` DISABLE KEYS */;
INSERT INTO `unidadmedida` VALUES (1,'Unidades de medida','Unidad(es)',0,0,0,0,'2015-10-11 00:00:00',1),(2,'Unidade(s)','Und.',1,1,1,0,'2015-11-02 11:57:21',19),(3,'Servicios','Srv',1,1,1,0,'2016-11-20 11:42:16',1),(29,'Docena','Doc',2,12,1,0,'2016-11-20 11:42:36',1),(30,'Ciento','Cto',2,100,1,0,'2016-11-20 11:44:35',1),(31,'Media Docena','1/2Doc',2,6,1,0,'2016-11-20 11:45:41',1),(32,'Cuarto Docena','1/4Doc',2,3,1,0,'2016-11-20 11:46:02',1),(33,'Metros','Mt',1,1,1,0,'2016-11-20 11:46:30',1),(34,'Kilogramo(s)','Kg',1,1,1,0,'2016-11-21 11:30:00',2);
/*!40000 ALTER TABLE `unidadmedida` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `Usuario_Id` int(11) NOT NULL AUTO_INCREMENT,
  `sUsuNombre` varchar(45) NOT NULL,
  `sUsuLogin` varchar(45) NOT NULL,
  `sUsuContrasena` varchar(45) NOT NULL,
  `nUsuTipo` int(11) NOT NULL,
  `nUsuEstado` tinyint(4) NOT NULL,
  `nUsuEliminado` tinyint(4) NOT NULL,
  `dUsuFecha_Act` datetime NOT NULL,
  `nUsuUsuarioOwn_Id` int(11) NOT NULL,
  PRIMARY KEY (`Usuario_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'administrador','administrador','123',1,1,0,'2016-11-20 11:41:11',1),(2,'Alberto Cortéz','Alberto','123456',1,1,0,'2016-08-08 09:56:07',17),(3,'Silvia Chirinos','Silvia','2016',1,1,0,'2016-03-31 20:08:37',17),(4,'Carlos','Carlos','123456',2,1,0,'2016-08-13 14:05:58',17);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database '11protex'
--

--
-- Dumping routines for database '11protex'
--
/*!50003 DROP PROCEDURE IF EXISTS `act_listar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `act_listar`(IN `pActivo_Id` INT(11))
BEGIN
   IF pActivo_Id = 0 THEN
        SELECT Activo_Id
               ,sActDescripcion
               ,nActEstado
               ,nActEliminado
               ,dActFecha_Act
               ,Usuario_Id
        FROM `activo` WHERE nActEliminado = 0;
   ELSE
        SELECT Activo_Id
               ,sActDescripcion
               ,nActEstado
               ,nActEliminado
               ,dActFecha_Act
               ,Usuario_Id
        FROM `activo` WHERE `Activo_Id` = pActivo_Id AND nActEliminado = 0;
   END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `alm_crear` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `alm_crear`(IN `psAlmNombre` VARCHAR(45), IN `psAlmUbicacion` VARCHAR(45), IN `pnAlmEstado` INT(11), IN `pnAlmEliminado` INT(11), IN `pUsuario_Id` INT(11))
BEGIN
    INSERT INTO `almacen`(
                              `sAlmNombre`,
                              `sAlmUbicacion`,
                              `nAlmEstado`,
                              `nAlmEliminado`,
                              `dAlmFecha_Act`,
                              `Usuario_Id`
                              ) 
                      VALUES (
                              psAlmNombre,
                              psAlmUbicacion,
                              pnAlmEstado,
                              pnAlmEliminado,
                              now(),
                              pUsuario_Id
                              );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `alm_editar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `alm_editar`(IN `psAlmNombre` VARCHAR(45), IN `psAlmUbicacion` VARCHAR(45), IN `pnAlmEstado` INT(11), IN `pnAlmEliminado` INT(11), IN `pUsuario_Id` INT(11), IN `pAlmacen_Id` INT(11))
BEGIN
    UPDATE `almacen` SET 
                        `sAlmNombre` = psAlmNombre,
                        `sAlmUbicacion` = psAlmUbicacion,
                        `nAlmEstado` = pnAlmEstado,
                        `nAlmEliminado` = pnAlmEliminado,
                        `dAlmFecha_Act` = now(),
                        `Usuario_Id` = pUsuario_Id
                  WHERE `Almacen_Id`=pAlmacen_Id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `alm_eliminar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `alm_eliminar`(IN `pAlmacen_Id` INT(11))
BEGIN
        UPDATE `almacen` SET nAlmEliminado = 1 WHERE `Almacen_Id` = pAlmacen_Id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `alm_listar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `alm_listar`(IN `pAlmacen_Id` INT(11))
BEGIN

   IF pAlmacen_Id = 0 THEN
        SELECT Almacen_Id
               ,sAlmNombre
               ,sAlmUbicacion
               ,nAlmEstado
               ,nAlmEliminado
               ,dAlmFecha_Act

               ,Usuario_Id
        FROM `almacen` WHERE nAlmEliminado = 0 ORDER BY sAlmNombre ASC;
   ELSE
        SELECT Almacen_Id
               ,sAlmNombre
               ,sAlmUbicacion
               ,nAlmEstado
               ,nAlmEliminado
               ,dAlmFecha_Act
               ,Usuario_Id
        FROM `almacen` WHERE `Almacen_Id` = pAlmacen_Id AND nAlmEliminado = 0 ORDER BY sAlmNombre ASC;
   END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `biusu_crear` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `biusu_crear`(IN `pnBiusuTipoModulo` INT, IN `pnBiusuUsuario_Id` INT, IN `psBiusuDescripcion` TEXT)
BEGIN
INSERT INTO `bitacorausuario`(`nBiusuTipoModulo`, `nBiusuUsuario_Id`, `dBiusuFechaHora`, `sBiusuDescripcion`) 
                          VALUES (pnBiusuTipoModulo,pnBiusuUsuario_Id,now(),psBiusuDescripcion);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `doc_crear` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `doc_crear`(IN `pAlmacen_Id` INT(11), IN `pnDocTipoMovimiento_Id` INT(11), IN `psDocNombre` VARCHAR(50), IN `psDocNombreCorto` VARCHAR(50), IN `pnDocNomAutomatico` TINYINT(4), IN `psDocSiguiente` VARCHAR(15), IN `pnDocAfectoIGV` TINYINT(4), IN `pnDocEstado` TINYINT(4), IN `pnDocEliminado` INT(11), IN `pUsuario_Id` INT(11))
BEGIN
    INSERT INTO `documento`(
                              `Almacen_Id`,
                              `nDocTipoMovimiento_Id`,
                              `sDocNombre`,
                              `sDocNombreCorto`,
                              `nDocNomAutomatico`,
                              `sDocSiguiente`,
                              `nDocAfectoIGV`,
                              `nDocEstado`,
                              `nDocEliminado`,
                              `dDocFecha_Act`,
                              `Usuario_Id`
                              ) 
                      VALUES (
                              pAlmacen_Id,
                              pnDocTipoMovimiento_Id,
                              psDocNombre,
                              psDocNombreCorto,
                              pnDocNomAutomatico,
                              psDocSiguiente,
                              pnDocAfectoIGV,
                              pnDocEstado,
                              pnDocEliminado,
                              now(),
                              pUsuario_Id
                              );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `doc_editar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `doc_editar`(IN `pAlmacen_Id` INT(11), IN `pnDocTipoMovimiento_Id` INT(11), IN `psDocNombre` VARCHAR(50), IN `psDocNombreCorto` VARCHAR(50), IN `pnDocNomAutomatico` TINYINT(4), IN `psDocSiguiente` VARCHAR(15), IN `pnDocAfectoIGV` TINYINT(4), IN `pnDocEstado` TINYINT(4), IN `pnDocEliminado` INT(11), IN `pUsuario_Id` INT(11), IN `pDocumento_Id` INT(11))
BEGIN
    UPDATE `documento` SET 
                        `Almacen_Id` = pAlmacen_Id,
                        `nDocTipoMovimiento_Id` = pnDocTipoMovimiento_Id,
                        `sDocNombre` = psDocNombre,
                        `sDocNombreCorto` = psDocNombreCorto,
                        `nDocNomAutomatico` = pnDocNomAutomatico,
                        `sDocSiguiente` = psDocSiguiente,
                        `nDocAfectoIGV` = pnDocAfectoIGV,
                        `nDocEstado` = pnDocEstado,
                        `nDocEliminado` = pnDocEliminado,
                        `dDocFecha_Act` = now(),
                        `Usuario_Id` = pUsuario_Id
                  WHERE `Documento_Id`=pDocumento_Id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `doc_eliminar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `doc_eliminar`(IN `pDocumento_Id` INT(11))
BEGIN
        UPDATE `documento` SET nDocEliminado = 1 WHERE `Documento_Id` = pDocumento_Id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Doc_Incrementa` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `Doc_Incrementa`(IN `pDocumento_Id` INT(11))
BEGIN
	DECLARE Automatico tinyint;
	DECLARE Numero varchar(11);
	
	SET Automatico=(Select nDocNomAutomatico
	FROM documento
	where Documento_Id= pDocumento_Id);
	
	SET Numero=(Select sDocSiguiente
	FROM documento
	where Documento_Id= pDocumento_Id);

	if Automatico =1 then
		BEGIN
			UPDATE documento
			SET
			sDocSiguiente = Numero +1        
			WHERE Documento_Id = pDocumento_Id;
		END;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `doc_listar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `doc_listar`(IN `pDocumento_Id` INT(11))
BEGIN
   IF pDocumento_Id = 0 THEN
        SELECT d.Documento_Id
               ,d.Almacen_Id
               ,a.sAlmNombre
               ,d.nDocTipoMovimiento_Id
               ,t.sTMovNombre
               ,d.sDocNombre
               ,d.sDocNombreCorto
               ,d.nDocNomAutomatico
               ,d.sDocSiguiente
               ,d.nDocAfectoIGV
               ,d.nDocEstado
               ,d.nDocEliminado
               ,d.dDocFecha_Act
               ,d.Usuario_Id
        FROM documento d
        INNER JOIN almacen a 
        ON d.Almacen_Id = a.Almacen_Id
        INNER JOIN tipomovimiento t
        ON t.TipoMovimiento_Id = d.nDocTipoMovimiento_Id
        WHERE d.nDocEliminado = 0;
   ELSE
        SELECT d.Documento_Id
               ,d.Almacen_Id
               ,a.sAlmNombre
               ,d.nDocTipoMovimiento_Id
               ,t.sTMovNombre
               ,d.sDocNombre
               ,d.sDocNombreCorto
               ,d.nDocNomAutomatico
               ,d.sDocSiguiente
               ,d.nDocAfectoIGV
               ,d.nDocEstado
               ,d.nDocEliminado
               ,d.dDocFecha_Act
               ,d.Usuario_Id
        FROM documento d
        INNER JOIN almacen a 
        ON d.Almacen_Id = a.Almacen_Id
        INNER JOIN tipomovimiento t
        ON t.TipoMovimiento_Id = d.nDocTipoMovimiento_Id
        WHERE d.Documento_Id = pDocumento_Id 
        AND d.nDocEliminado = 0;
   END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fam_crear` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fam_crear`(IN `psFamCodigo` VARCHAR(30), IN `psFamDescripcion` VARCHAR(60), IN `pnFamPadre` INT(11), IN `pnFamTipo` TINYINT(4), IN `pnFamEstado` TINYINT(4), IN `pnFamEliminado` INT(11), IN `pUsuario_Id` INT(11))
BEGIN
    INSERT INTO `familia`(
                              `sFamCodigo`,
                              `sFamDescripcion`,
                              `nFamPadre`,
                              `nFamTipo`,
                              `nFamEstado`,
                              `nFamEliminado`,
                              `dFamFecha_Act`,
                              `Usuario_Id`
                              ) 
                      VALUES (
                              psFamCodigo,
                              psFamDescripcion,
                              pnFamPadre,
                              pnFamTipo,
                              pnFamEstado,
                              pnFamEliminado,
                              now(),
                              pUsuario_Id
                              );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fam_editar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fam_editar`(IN `psFamCodigo` VARCHAR(30), IN `psFamDescripcion` VARCHAR(60), IN `pnFamPadre` INT(11), IN `pnFamTipo` TINYINT(4), IN `pnFamEstado` TINYINT(4), IN `pnFamEliminado` INT(11), IN `pUsuario_Id` INT(11), IN `pFamilia_Id` INT(11))
BEGIN
    UPDATE `familia` SET 
                        `sFamCodigo` = psFamCodigo,
                        `sFamDescripcion` = psFamDescripcion,
                        `nFamPadre` = pnFamPadre,
                        `nFamTipo` = pnFamTipo,
                        `nFamEstado` = pnFamEstado,
                        `nFamEliminado` = pnFamEliminado,
                        `dFamFecha_Act` = now(),
                        `Usuario_Id` = pUsuario_Id
                  WHERE `Familia_Id`=pFamilia_Id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fam_eliminar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fam_eliminar`(IN `pFamilia_Id` INT(11))
BEGIN
        UPDATE `familia` SET `nFamEliminado` = 1 WHERE `Familia_Id` = pFamilia_Id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fam_hijos` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fam_hijos`(IN `pFamilia_Id` INT)
    NO SQL
SELECT Familia_Id
               ,sFamCodigo
               ,sFamDescripcion
               ,nFamPadre
               ,nFamTipo
               ,nFamEstado
               ,nFamEliminado
               ,dFamFecha_Act
               ,Usuario_Id
        FROM `familia` WHERE nFamEliminado = 0 AND  nFamPadre = pFamilia_Id ORDER BY sFamDescripcion ASC ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fam_listar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fam_listar`(IN `pFamilia_Id` INT(11))
BEGIN
   IF pFamilia_Id = 0 THEN
        SELECT Familia_Id
               ,sFamCodigo
               ,sFamDescripcion
               ,nFamPadre
               ,nFamTipo
               ,nFamEstado
               ,nFamEliminado
               ,dFamFecha_Act
               ,Usuario_Id
        FROM `familia` WHERE nFamEliminado = 0 ORDER BY sFamCodigo ASC;
   ELSE
        SELECT Familia_Id
               ,sFamCodigo
               ,sFamDescripcion
               ,nFamPadre
               ,nFamTipo
               ,nFamEstado
               ,nFamEliminado
               ,dFamFecha_Act
               ,Usuario_Id
        FROM `familia` WHERE `Familia_Id` = pFamilia_Id AND nFamEliminado = 0 ORDER BY sFamCodigo ASC;
   END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fam_padre` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fam_padre`(IN `pFamilia_Id` INT, IN `pnFamPadre` INT)
    NO SQL
UPDATE familia SET nFamPadre = pnFamPadre WHERE nFamPadre = pFamilia_Id ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Imp_Movimiento` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `Imp_Movimiento`(IN `pMovimiento_Id` INT(11))
    NO SQL
SELECT M.Movimiento_Id,
        D.Documento_Id,
		CONCAT(D.sDocNombreCorto , M.sMovDocumento) as Documento,	
        dMovFecha as Fecha,
		OD.sODNombre as Nombre,
		OD.sODRucDni as RUCDNI,
		OD.sODDireccion as Dirección,
		M.sMovDocReferencia as Referencia,	
        M.nMovTipopago as TipoPago,
		MD.nMovDetCantidad AS CANTIDAD,
		UM.sUndAlias AS UNIDAD,
		P.sProSrvNombre AS PRODUCTO,
		MD.nMovDetPrecioUnitario AS PRECIOUNITARIO,
		MD.nMovDetImporte AS SUBTOTAL,
		M.nMovImporte as Importe,
		M.nMovIGV as IGV,
		M.nMovTotal as Total,
		-- Mo.sMonImprimir as MonedaLargo,
		Mo.sMonAlias as MonedaAlias,
        U.sUsuNombre as Usuario
		-- @IGV as PorcIGV
		FROM Movimiento M
		INNER JOIN Documento D on (D.Documento_Id = M.Documento_Id)
		INNER JOIn OrigenDestino OD on (OD.OrigenDestino_Id = M.nMovOrigenDestino_Id)
		INNER JOIN MovimientoDetalle MD ON (MD.Movimiento_Id=M.Movimiento_Id)
		INNER JOIN ProductoLote PL ON (PL.ProductoLote_Id=MD.ProductoLote_Id)
		INNER JOIN ProdServ P ON (P.ProdServ_Id=PL.ProdServ_Id)
		INNER JOIN UnidadMedida UM ON (UM.UnidadMedida_Id=MD.nMovDetUnidad_Id)
		INNER JOIN Moneda Mo ON (Mo.Moneda_Id=M.Moneda_Id)
        INNER JOIN Usuario U ON (U.Usuario_Id=M.Usuario_Id)
		WHERE 
		M.Movimiento_Id = pMovimiento_Id ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `movdet_crear` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `movdet_crear`(IN `pMovimiento_Id` INT(11), IN `pTipoMovimiento_Id` INT, IN `pTipoDestino` INT, IN `pProdServ_Id` INT, IN `pAlmacen_Id` INT, IN `PLote` VARCHAR(50), IN `pFechaVencimiento` VARCHAR(50), IN `pCantidad` DOUBLE, IN `pUnidad_Id` INT, IN `pMonedaCompra_Id` INT, IN `pPrecioLista` DOUBLE, IN `pDescuento` DOUBLE, IN `pPrecio` DOUBLE, IN `pSubTotal` DOUBLE, IN `pUsuario_Id` INT, IN `pTipoCambio` DOUBLE, IN `pnMovDetIncluyeIgv` INT)
BEGIN
    
	

	

		
	
	DECLARE pnTMovSentido tinyint;

	DECLARE pProductoLote_Id INT;
	DECLARE pStokReal double;		
	DECLARE pSaldoAnteriorLote double;
	DECLARE pSaldoPosteriorLote double;
	DECLARE pSaldoAnterior double;
	DECLARE pSaldoPosterior double	;
	DECLARE pCODIGO char(20)	;
	Declare pFactor double;
	Declare pCantidadLote double;
	Declare pPrecioLote double;
	Declare pPrecioListaLote double;

	DECLARE pPrecioVenta double;
	DECLARE pPrecioVenta2 double;
	DECLARE pPrecioVenta3 double;
	DECLARE pEmpresa_Id int;

	SET pSaldoAnteriorLote = 0;	
	SET pSaldoAnterior  = 0;

	
	SET pnTMovSentido = (
	SELECT nTMovSentido FROM tipomovimiento	
	WHERE TipoMovimiento_Id=pTipoMovimiento_Id);
	
	
	SET pCODIGO = (				
	Select  sProSrvCodigo from prodserv 
	where ProdServ_Id=pProdServ_Id);
	
	
	IF LEFT(pCODIGO,1)='2' THEN
			SET pCantidadLote  = 0;
			SET pPrecioLote  = 0;
			SET pPrecioListaLote  = 0;
			SET pProductoLote_Id = (
				Select  ProductoLote_Id
			    FROM productolote
				Where ProdServ_Id=pProdServ_Id
				AND Almacen_Id=pAlmacen_Id);
			SET pStokReal = (
				Select  nProLStockReal
				FROM productolote
				Where ProdServ_Id=pProdServ_Id
				AND Almacen_Id=pAlmacen_Id);

	ELSE 
		
		SET pProductoLote_Id = (
		Select ProductoLote_Id			   
		FROM productolote
		Where ProdServ_Id=pProdServ_Id
		AND Almacen_Id=pAlmacen_Id
		AND sProLNombreLote = pLote);
		
		SET pStokReal = (
		Select nProLStockReal
		FROM productolote
		Where ProdServ_Id=pProdServ_Id
		AND Almacen_Id=pAlmacen_Id
		AND sProLNombreLote = pLote);

		
		SET pFactor = (
		Select nUndFac_Cnv
		from unidadmedida
		Where UnidadMedida_Id = pUnidad_Id);
		
		SET pCantidadLote  =  pCantidad * pFactor;
		
		SET pPrecioLote = (pPrecio* pTipoCambio) / pFactor;
		
		SET pPrecioListaLote = (pPrecioLista) / pFactor;
		
			
		
		
		
		
		SET pSaldoAnterior = (
		Select SUM(nProLStockReal)
		from productolote PL Inner join almacen A ON (A.Almacen_Id=PL.Almacen_Id)
		Where ProdServ_Id=pProdServ_Id) ;

		SET pPrecioVenta = (
		Select MAX(nProLPrecioV)
		from productolote PL Inner join almacen A ON (A.Almacen_Id=PL.Almacen_Id)
		Where ProdServ_Id=pProdServ_Id) ;

		SET pPrecioVenta2 = (
		Select MAX(nProLPrecioV2)
		from productolote PL Inner join almacen A ON (A.Almacen_Id=PL.Almacen_Id)
		Where ProdServ_Id=pProdServ_Id) ;

		SET pPrecioVenta3 = (
		Select MAX(nProLPrecioV3)
		from productolote PL Inner join almacen A ON (A.Almacen_Id=PL.Almacen_Id)
		Where ProdServ_Id=pProdServ_Id) ;
		
		SET pSaldoAnterior = IFNULL(pSaldoAnterior,0);
		SET pPrecioVenta = IFNULL(pPrecioVenta,0);
		SET pPrecioVenta2 = IFNULL(pPrecioVenta2,0);
		SET pPrecioVenta3 = IFNULL(pPrecioVenta3,0);
		
	END IF;
	


	
	
	IF ISNULL(pProductoLote_Id) THEN
			
				INSERT INTO productolote
				   (ProdServ_Id
				   ,Almacen_Id
				   ,sProLNombreLote
				   ,nProLStockReal
				   ,dProLFechaVencimiento
				   ,nProLPrecioL
				   ,nProLPrecioC
				   ,nProLPrecioV
				   ,nProLPrecioV2
				   ,nProLPrecioV3
				   ,dProLFecha_Act
				   ,nProLUsuario_Id
				   ,nProLMonedaCompra_Id
				   ,nProLDescuentoCompra)		  			   
				VALUES
				   (pProdServ_Id
				   ,pAlmacen_Id
				   ,pLote
				   ,pCantidadLote
				   ,STR_TO_DATE(pFechaVencimiento,'%d/%m/%Y')	   
				   ,pPrecioListaLote
				   ,pPrecioLote
				   ,pPrecioVenta
				   ,pPrecioVenta2
				   ,pPrecioVenta3
				   ,now()
				   ,pUsuario_Id
				   ,pMonedaCompra_Id
				   ,pDescuento);			   			   	   
				SET pProductoLote_Id = LAST_INSERT_ID();
				SET pSaldoPosterior = pSaldoAnterior + pCantidadLote;
				SET pSaldoPosteriorLote =pCantidadLote;						
				

	ELSEIF pProductoLote_Id>0 THEN

		
		
		IF LEFT(pCODIGO,1)='1' THEN 			
			
			SET pSaldoAnteriorLote = pStokReal; 
			
			SELECT pnTMovSentido;
			
			IF pnTMovSentido = 1 THEN  
				SELECT 1;
					SET pSaldoPosteriorLote =pStokReal + pCantidadLote;
					SET pSaldoPosterior =pSaldoAnterior + pCantidadLote;
					
					UPDATE productolote SET  
						 nProLDescuentoCompra=(((pStokReal * nProLDescuentoCompra) + ( pCantidadLote*pDescuento))/(pStokReal+pCantidadLote)),
						 nProLPrecioL=(((pStokReal * nProLPrecioL) + ( pCantidadLote*pPrecioListaLote))/(pStokReal+pCantidadLote)),
						 nProLPrecioC=(((pStokReal * nProLPrecioC) + ( pCantidadLote*pPrecioLote))/(pStokReal+pCantidadLote)),
						 nProLPrecioV=pPrecioVenta, 
						 nProLPrecioV2=pPrecioVenta2, 
						 nProLPrecioV3=pPrecioVenta3, 
						 nProLStockReal=pStokReal + pCantidadLote , 
						 dProLFecha_Act=curdate(),
						 nProLUsuario_Id=pUsuario_Id									
					WHERE ProductoLote_Id = pProductoLote_Id;
					
					
			
			
			ELSEIF pnTMovSentido = 2 THEN
				SELECT 2;
					SET pSaldoPosteriorLote =pStokReal - pCantidadLote;
					SET pSaldoPosterior =pSaldoAnterior - pCantidadLote;
					UPDATE productolote SET 
						nProLStockReal=pStokReal - pCantidadLote, 
						dProLFecha_Act=curdate(),
						nProLUsuario_Id=pUsuario_Id									
					WHERE ProductoLote_Id = pProductoLote_Id;
					
			END IF;		
		END IF; 
	END IF;  


BEGIN
	SELECT  pSaldoAnteriorLote,pSaldoPosteriorLote,pSaldoAnterior,pSaldoPosterior;
	
	INSERT INTO MovimientoDetalle
			   (
			    Movimiento_Id
			   ,ProductoLote_Id
			   ,nMovDetCantidad
			   ,nMovDetUnidad_Id
			   ,nMovDetPrecioLista
			   ,nMovDetPorcDescuento1
			   ,nMovDetPrecioUnitario
			   ,nMovDetImporte
			   ,nMovDetSaldoAnteriorLote
			   ,nMovDetSaldoPosteriorLote
			   ,nMovDetSaldoAnterior
			   ,nMovDetSaldoPosterior
			   ,nMovDetIncluyeIgv)				   
		 VALUES
			   (pMovimiento_id
			   ,pProductoLote_Id
			   ,pCantidad			   
			   ,pUnidad_Id
			   ,pPrecioLista
			   ,pDescuento
			   ,pPrecio
			   ,pSubTotal
			   ,pSaldoAnteriorLote
			   ,pSaldoPosteriorLote
			   ,pSaldoAnterior
			   ,pSaldoPosterior
			   ,pnMovDetIncluyeIgv);
END;





END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `movdet_listar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `movdet_listar`(IN `pMovimiento_Id` INT(11))
BEGIN
   IF pMovimiento_Id = 0 THEN
        SELECT Movimiento_Id
               ,ProductoLote_Id
               ,nMovDetUnidad_Id
               ,nMovDetCantidad
               ,nMovDetPrecioLista
               ,nMovDetPorcDescuento1
               ,nMovDetPorcDecuento2
               ,nMovDetPrecioUnitario
               ,nMovDetImporte
               ,nMovDetPrecioFlete
               ,nMovDetSaldoAnteriorLote
               ,nMovDetSaldoPosteriorLote
               ,nMovDetSaldoAnterior
               ,nMovDetSaldoPosterior
               ,nMovDetIncluyeIGV
        FROM `movimientodetalle`;
   ELSE
        SELECT Movimiento_Id
               ,ProductoLote_Id
               ,nMovDetUnidad_Id
               ,nMovDetCantidad
               ,nMovDetPrecioLista
               ,nMovDetPorcDescuento1
               ,nMovDetPorcDecuento2
               ,nMovDetPrecioUnitario
               ,nMovDetImporte
               ,nMovDetPrecioFlete
               ,nMovDetSaldoAnteriorLote
               ,nMovDetSaldoPosteriorLote
               ,nMovDetSaldoAnterior
               ,nMovDetSaldoPosterior
               ,nMovDetIncluyeIGV
        FROM `movimientodetalle` WHERE `Movimiento_Id` = pMovimiento_Id;
   END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Mov_ActualizaEstado` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `Mov_ActualizaEstado`(IN `pMovimiento_Id` INT, IN `pEstado_Id` TINYINT, IN `pUsuario_Id` TINYINT)
BEGIN
	UPDATE movimiento
      SET
      nMovEstado= pEstado_Id,
            Usuario_Id= pUsuario_Id
       WHERE Movimiento_Id = pMovimiento_Id;   
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Mov_Anular` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `Mov_Anular`(IN `pMovimiento_Id` INT(11), IN `pUsuario_Id` INT(11))
BEGIN
    
	

	DECLARE DONE INT DEFAULT 0;
	DECLARE pnMovEstado int;

	

	DECLARE pnMovAlmacen_Id INT;
	DECLARE pnMovTipoMovimiento_Id INT;
	DECLARE pnMovTipoOrigenDestino INT;
	DECLARE pnMovOrigenDestino_Id INT;
	DECLARE pdMovFecha DATE;
	DECLARE pnMovTipoDestino TINYINT;
	DECLARE pnMovTipoPago TINYINT;
	DECLARE pnMovTipoDocumento INT;
	DECLARE psMovDocumento NVARCHAR(20);
	DECLARE psMovDocReferencia NVARCHAR(20);
	DECLARE pnMovImporte DOUBLE;
	DECLARE pnMovIGV DOUBLE;
	DECLARE pnMovTotal DOUBLE;
	DECLARE pnMovSaldo DOUBLE;
	DECLARE pnMovTotalCancelado DOUBLE;
	DECLARE pnMovDetracción DOUBLE;
	DECLARE pnMovPercepcion DOUBLE;
	DECLARE pnMovRetencion DOUBLE;
	DECLARE psMovObservación NVARCHAR(255);
	DECLARE pnMovMoneda_Id int;
	DECLARE pnMovTipoCambio DOUBLE;
	
	DECLARE pNEWMOV_ID INT;
	DECLARE pNEWTIPODOCUMENTO INT;
	DECLARE pNEWNRODOCUMENTO NVARCHAR(20);
	DECLARE pnTMovSentido TINYINT;
	
	
	DECLARE	pProdServ_Id int;
	DECLARE pProductoLote_Id int;
	
	
	DECLARE pCantidad int;
	DECLARE pUnidad_Id int;
	DECLARE pPrecioLista double;
	DECLARE pPrecio double;
	
	
	
	

	
	Declare pFactor Double;
	Declare pCantidadLote Double;
	Declare pPrecioLote Double;
	Declare pPrecioListaLote Double;
	
	DECLARE pSaldoAnteriorLote Double;
	DECLARE pSaldoPosteriorLote Double;
	DECLARE pSaldoAnterior Double;
	DECLARE pSaldoPosterior Double;
	DECLARE pStokReal Double;
	DECLARE pDescuento Double;
	
	DECLARE pCODIGO NVARCHAR(50);
	DECLARE pEmpresa_Id int;

	DECLARE MOVCURSOR CURSOR for		
		SELECT
		PL.ProdServ_Id,
		MD.ProductoLote_Id ,
		
		
		MD.nMovDetCantidad,
		MD.nMovDetUnidad_Id,
		MD.nMovDetPrecioLista,
		MD.nMovDetPrecioUnitario,
		MD.nMovDetPorcDescuento1
		FROM MovimientoDetalle MD
		INNER JOIN Movimiento M on (M.Movimiento_Id=MD.Movimiento_Id)
		INNER JOIN ProductoLote PL ON (PL.ProductoLote_id=MD.ProductoLote_id)
		WHERE M.Movimiento_Id =pMovimiento_Id;
	
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

	
	SET pnMovEstado = (
	SELECT nMovEstado FROM Movimiento 
	WHERE Movimiento_Id = pMovimiento_Id);
	
	IF pnMovEstado>0 THEN 
		
		SET pNEWTIPODOCUMENTO=0;  
		SET pSaldoAnteriorLote = 0;
		SET pSaldoPosteriorLote =0;
		SET pSaldoAnterior = 0;
		SET pSaldoPosterior =0;
		SET pDescuento =0;		


		SET pnMovAlmacen_Id = (SELECT M.Almacen_Id FROM Movimiento M WHERE Movimiento_Id = pMovimiento_Id);
		SET pnMovTipoMovimiento_Id = (SELECT M.nMovTipoMovimiento_Id FROM Movimiento M WHERE Movimiento_Id = pMovimiento_Id);
		SET pnMovTipoOrigenDestino= (SELECT M.nMovTipoOrigenDestino FROM Movimiento M WHERE Movimiento_Id = pMovimiento_Id);
		SET pnMovOrigenDestino_Id =(SELECT M.nMovOrigenDestino_Id FROM Movimiento M WHERE Movimiento_Id = pMovimiento_Id);
		SET pdMovFecha =(SELECT M.dMovFecha FROM Movimiento M WHERE Movimiento_Id = pMovimiento_Id);
		SET pnMovTipoDestino = (SELECT M.nMovTipoDestino  FROM Movimiento M WHERE Movimiento_Id = pMovimiento_Id);
		SET pnMovTipoPago = (SELECT M.nMovTipoPago FROM Movimiento M WHERE Movimiento_Id = pMovimiento_Id);
		SET pnMovTipoDocumento = (SELECT M.Documento_Id FROM Movimiento M WHERE Movimiento_Id = pMovimiento_Id);
		SET psMovDocumento = (SELECT M.sMovDocumento FROM Movimiento M WHERE Movimiento_Id = pMovimiento_Id);
		SET psMovDocReferencia = (SELECT M.sMovDocReferencia FROM Movimiento M WHERE Movimiento_Id = pMovimiento_Id);
		SET pnMovImporte = (SELECT M.nMovImporte FROM Movimiento M WHERE Movimiento_Id = pMovimiento_Id);
		SET pnMovIGV = (SELECT M.nMovIGV FROM Movimiento M WHERE Movimiento_Id = pMovimiento_Id);
		SET pnMovTotal = (SELECT M.nMovTotal FROM Movimiento M WHERE Movimiento_Id = pMovimiento_Id);
		SET pnMovSaldo = (SELECT M.nMovSaldo FROM Movimiento M WHERE Movimiento_Id = pMovimiento_Id);
        SET pnMovTotalCancelado = (SELECT M.nMovTotalCancelado FROM Movimiento M WHERE Movimiento_Id = pMovimiento_Id);
		SET pnMovDetracción = (SELECT M.dMovDetraccion FROM Movimiento M WHERE Movimiento_Id = pMovimiento_Id);
		SET pnMovPercepcion = (SELECT M.dMovPercepcion FROM Movimiento M WHERE Movimiento_Id = pMovimiento_Id);
		SET pnMovRetencion = (SELECT M.dMovRetencion FROM Movimiento M WHERE Movimiento_Id = pMovimiento_Id);
		SET psMovObservación = (SELECT M.sMovObservacion FROM Movimiento M WHERE Movimiento_Id = pMovimiento_Id);
		SET pnMovEstado = (SELECT nMovEstado FROM Movimiento M WHERE Movimiento_Id = pMovimiento_Id);
		SET pnMovMoneda_Id = (SELECT Moneda_Id FROM Movimiento M WHERE Movimiento_Id = pMovimiento_Id);
		SET pnMovTipoCambio = (SELECT nMovTipoCambio  FROM Movimiento M WHERE Movimiento_Id = pMovimiento_Id);
		
		SET pNEWNRODOCUMENTO= (Select sDocNombreCorto FROM Documento	WHERE Documento_Id=pnMovTipoDocumento);		
		SET pNEWNRODOCUMENTO= CONCAT(pNEWNRODOCUMENTO,' ',psMovDocumento);
	
		
		INSERT INTO Movimiento
           (Almacen_Id
           ,nMovTipoMovimiento_Id
           ,nMovTipoOrigenDestino
           ,nMovOrigenDestino_Id
           ,dMovFecha
           ,nMovTipoDestino
           ,nMovTipoPago
           ,Documento_Id
           ,sMovDocumento
           ,sMovDocReferencia
           ,nMovImporte
           ,nMovIGV
           ,nMovTotal
           ,nMovSaldo
           ,nMovTotalCancelado
           ,dMovDetraccion
           ,dMovPercepcion
           ,dMovRetencion
           ,sMovObservacion
           ,nMovEstado
           ,dMovFecha_Act
           ,Usuario_Id
           ,Moneda_Id
           ,nMovTipoCambio
		   ,nMovClasificador_Id
		   ,nMovMovimiento_Id
		   ,nMovOrdenCampo_Id)
		VALUES
           (pnMovAlmacen_Id,
           pnMovTipoMovimiento_Id ,
		   pnMovTipoOrigenDestino ,
           pnMovOrigenDestino_Id ,
           NOW() ,
           pnMovTipoDestino ,
           pnMovTipoPago ,
           pNEWTIPODOCUMENTO,
           pNEWNRODOCUMENTO ,
           psMovDocReferencia,
           pnMovImporte ,
           pnMovIGV ,
           pnMovTotal ,
           pnMovSaldo,
           0 ,
           pnMovDetracción ,
           pnMovPercepcion ,
           pnMovRetencion ,
           psMovObservación ,
           1 ,
           NOW(),
           pUsuario_Id,
           pnMovMoneda_Id,
           pnMovTipoCambio,
		   0,
		   0,
		   0);
           
        SET pNEWMOV_ID  = LAST_INSERT_ID();
	
		
		SET pnTMovSentido = (SELECT nTMovSentido
		FROM TipoMovimiento
		WHERE TipoMovimiento_Id=pnMovTipoMovimiento_Id);	
		IF pnTMovSentido = 1 THEN
			SET pnTMovSentido=2;
		ELSE
			SET pnTMovSentido=1;				
		END IF;
	  
			
		OPEN MOVCURSOR;	
		REPEAT
			FETCH MOVCURSOR INTO pProdServ_Id, pProductoLote_Id, pCantidad, pUnidad_Id, pPrecioLista, pPrecio, pDescuento;
			
			IF NOT done THEN

				SET pSaldoAnteriorLote = 0;
				SET pSaldoPosteriorLote =0;
				SET pSaldoAnterior  = 0;
				SET pSaldoPosterior =0;
			
				SET pCODIGO = (Select sProSrvCodigo from ProdServ where ProdServ_Id= pProdServ_Id);
				
				
				IF LEFT(pCODIGO,1)='1' THEN
					
					
					SET pFactor = (Select nUndFac_Cnv from UnidadMedida Where UnidadMedida_Id = pUnidad_Id);
					SET pCantidadLote  =  pCantidad * pFactor;
					SET pPrecioLote = (pPrecio * pnMovTipoCambio) / pFactor;
					SET pPrecioListaLote = (pPrecioLista) / pFactor;
					
					SET pEmpresa_Id =1;				
					SET pSaldoAnterior = (Select SUM(nProLStockReal) from ProductoLote PL Where ProdServ_Id=pProdServ_Id);				
					SET pSaldoAnterior = IFNULL(pSaldoAnterior,0);				
					SET pStokReal = (Select nProLStockReal from ProductoLote Where ProductoLote_Id=pProductoLote_Id);
					SET pDescuento = (Select nProLDescuentoCompra from ProductoLote Where ProductoLote_Id=pProductoLote_Id);
					SET pSaldoAnteriorLote = pStokReal; 
					
					IF pnTMovSentido = 1 THEN 
						SET pSaldoPosteriorLote =pStokReal + pCantidadLote;
						SET pSaldoPosterior =pSaldoAnterior + pCantidadLote;
						
						UPDATE ProductoLote SET  nProLStockReal=pStokReal + pCantidadLote , 
												 dProLFecha_Act=curdate(),
												 nProLUsuario_Id=pUsuario_Id									
						WHERE ProductoLote_Id = pProductoLote_Id;
					END IF;
					
					IF pnTMovSentido = 2 THEN 
						SET pSaldoPosteriorLote =pStokReal - pCantidadLote;
						SET pSaldoPosterior =pSaldoAnterior - pCantidadLote;
						
						
						
						IF pStokReal-pCantidadLote = 0 THEN						
							UPDATE ProductoLote SET 
													nProLStockReal=pStokReal - pCantidadLote , 
													dProLFecha_Act=curdate(),
													nProLUsuario_Id=pUsuario_Id									
							WHERE ProductoLote_Id = pProductoLote_Id;
						ELSE
							UPDATE ProductoLote SET 
								nProLDescuentoCompra=(((pStokReal * nProLDescuentoCompra) - ( pCantidadLote*pDescuento))/(pStokReal+pCantidadLote)),
								nProLPrecioL=(((pStokReal * nProLPrecioL) - ( pCantidadLote*pPrecioListaLote))/(pStokReal+pCantidadLote)),
								nProLPrecioC=(((pStokReal * nProLPrecioC) - ( pCantidadLote*pPrecioLote))/(pStokReal-pCantidadLote)),
								
								nProLStockReal=pStokReal - pCantidadLote , 
								dProLFecha_Act=curdate(),
								nProLUsuario_Id=pUsuario_Id
								WHERE ProductoLote_Id = pProductoLote_Id;
						END IF;
					END IF;
				END IF; 

				INSERT INTO MovimientoDetalle
				(
					Movimiento_Id
				   ,ProductoLote_Id
				   ,nMovDetCantidad
				   ,nMovDetUnidad_Id
				   ,nMovDetPrecioLista
				   ,nMovDetPorcDescuento1
				   ,nMovDetPrecioUnitario
				   ,nMovDetImporte
				   ,nMovDetSaldoAnteriorLote
				   ,nMovDetSaldoPosteriorLote
				   ,nMovDetSaldoAnterior
				   ,nMovDetSaldoPosterior)				   
				 VALUES
				   (pNEWMOV_ID
				   ,pProductoLote_Id
				   ,pCantidad			   
				   ,pUnidad_Id
				   ,0
				   ,0
				   ,0
				   ,0
				   ,pSaldoAnteriorLote
				   ,pSaldoPosteriorLote
				   ,pSaldoAnterior
				   ,pSaldoPosterior);
			
			END IF; 
		UNTIL DONE END REPEAT;    	
		CLOSE MOVCURSOR;
			
		
		UPDATE Movimiento
		SET nMovEstado=0
		
		WHERE Movimiento_Id=pMovimiento_Id;
		
	END IF;
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `mov_crear` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `mov_crear`(IN `pdMovFecha` DATE, IN `pnMovTipoMovimiento_Id` INT(11), IN `pnMovTipopago` TINYINT(4), IN `pAlmacen_Id` INT(11), IN `pnMovTipoOrigenDestino` INT(11), IN `pnMovOrigenDestino_Id` INT(11), IN `pnMovTipodestino` TINYINT(4), IN `pDocumento_Id` INT(11), IN `psMovDocumento` VARCHAR(20), IN `psMovDocReferencia` VARCHAR(20), IN `pMoneda_Id` TINYINT(4), IN `pnMovTipoCambio` DECIMAL(10,2), IN `pnMovImporte` DECIMAL(10,2), IN `pnMovIGV` DECIMAL(10,2), IN `pnMovTotal` DECIMAL(10,2), IN `pnMovSaldo` DECIMAL(10,2), IN `pnMovTotalCancelado` DECIMAL(10,2), IN `pdMovDetraccion` DECIMAL(10,2), IN `pdMovPercepcion` DECIMAL(10,2), IN `pdMovRetencion` DECIMAL(10,2), IN `psMovObservacion` VARCHAR(200), IN `pnMovEstado` TINYINT(4), IN `pnMovEliminado` TINYINT(4), IN `pUsuario_Id` INT(11), OUT `pMovimiento_Id` INT(11))
BEGIN
    INSERT INTO `movimiento`(
                              `dMovFecha`,
                              `nMovTipoMovimiento_Id`,
                              `nMovTipopago`,
                              `Almacen_Id`,
                              `nMovTipoOrigenDestino`,
                              `nMovOrigenDestino_Id`,
                              `nMovTipodestino`,
                              `Documento_Id`,
                              `sMovDocumento`,
                              `sMovDocReferencia`,
                              `Moneda_Id`,
                              `nMovTipoCambio`,
                              `nMovImporte`,
                              `nMovIGV`,
                              `nMovTotal`,
							  `nMovSaldo`,
                              `nMovTotalCancelado`,
                              `dMovDetraccion`,
                              `dMovPercepcion`,
                              `dMovRetencion`,
                              `sMovObservacion`,
                              `nMovEstado`,
                              `nMovEliminado`,
                              `dMovFecha_Act`,
                              `Usuario_Id`,
                              `nMovMovimiento_Id`,
                              `nMovOrdenCampo_Id`,
                              `nMovClasificador_Id`
                              ) 
                      VALUES (
                              pdMovFecha,
                              pnMovTipoMovimiento_Id,
                              pnMovTipopago,
                              pAlmacen_Id,
                              pnMovTipoOrigenDestino,
                              pnMovOrigenDestino_Id,
                              pnMovTipodestino,
                              pDocumento_Id,
                              psMovDocumento,
                              psMovDocReferencia,
                              pMoneda_Id,
                              pnMovTipoCambio,
                              pnMovImporte,
                              pnMovIGV,
                              pnMovTotal,
							  pnMovSaldo,
                              pnMovTotalCancelado,
                              pdMovDetraccion,
                              pdMovPercepcion,
                              pdMovRetencion,
                              psMovObservacion,
                              pnMovEstado,
                              pnMovEliminado,
                              now(),
                              pUsuario_Id,
                              0,
                              0,
                              0
                              );
      SET pMovimiento_Id = LAST_INSERT_ID() ;   		
			Select pMovimiento_Id as Movimiento_Id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `mov_date` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `mov_date`(IN `pInicio` DATE, IN `pFin` DATE)
    NO SQL
BEGIN
SET pFin  = date_add(pFin,INTERVAL 1 Day);
SELECT m.Movimiento_Id
               ,m.dMovFecha
               ,m.nMovTipoMovimiento_Id
               ,t.sTMovNombre
               ,m.nMovTipopago
               ,m.Almacen_Id
               ,m.nMovTipoOrigenDestino
               ,m.nMovOrigenDestino_Id
               ,a.sAlmNombre
               ,m.nMovTipodestino
               ,m.Documento_Id
               ,d.sDocNombre
			   ,d.sDocNombreCorto
               ,m.sMovDocumento
               ,m.sMovDocReferencia
               ,m.Moneda_Id
               ,m.nMovTipoCambio
               ,m.nMovImporte
               ,m.nMovIGV
               ,m.nMovTotal
			   ,m.nMovSaldo	
               ,m.nMovTotalCancelado
               ,m.dMovDetraccion

               ,m.dMovPercepcion
               ,m.dMovRetencion
               ,m.sMovObservacion
               ,m.nMovEstado
               ,m.nMovEliminado
               ,m.dMovFecha_Act
               ,m.Usuario_Id
               ,nMovClasificador_Id
               ,nMovMovimiento_Id
               ,nMovOrdenCampo_Id
               ,CONCAT( d.sDocNombreCorto, ' ', m.sMovDocumento) as Documento

        FROM `movimiento` m 

        INNER JOIN `tipomovimiento` t
        ON t.TipoMovimiento_Id = m.nMovTipoMovimiento_Id 
        INNER JOIN `almacen` a
        ON a.Almacen_Id = m.Almacen_Id
        INNER JOIN `documento` d
        ON d.Documento_Id = m.Documento_Id 
        WHERE m.nMovEliminado = 0
		AND m.dMovFecha>=pInicio
		AND m.dMovFecha<pFin
        ORDER BY m.dMovFecha;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `mov_editar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `mov_editar`(IN `pdMovFecha` DATE, IN `pnMovTipoMovimiento_Id` INT(11), IN `pnMovTipopago` TINYINT(4), IN `pAlmacen_Id` INT(11), IN `pnMovTipoOrigenDestino` INT(11), IN `pnMovOrigenDestino_Id` INT(11), IN `pnMovTipodestino` TINYINT(4), IN `pDocumento_Id` INT(11), IN `psMovDocumento` VARCHAR(20), IN `psMovDocReferencia` VARCHAR(20), IN `pMoneda_Id` INT(11), IN `pnMovTipoCambio` DECIMAL(10,2), IN `pnMovImporte` DECIMAL(10,2), IN `pnMovIGV` DECIMAL(10,2), IN `pnMovTotal` DECIMAL(10,2), IN `pnMovSaldo` DECIMAL(10,2), IN `pnMovTotalCancelado` DECIMAL(10,2), IN `pdMovDetraccion` DECIMAL(10,2), IN `pdMovPercepcion` DECIMAL(10,2), IN `pdMovRetencion` DECIMAL(10,2), IN `psMovObservacion` VARCHAR(200), IN `pnMovEstado` TINYINT(4), IN `pnMovEliminado` TINYINT(4), IN `pUsuario_Id` INT(11), IN `pMovimiento_Id` INT(11))
BEGIN
    UPDATE `movimiento` SET 
                        `dMovFecha` = pdMovFecha,
                        `nMovTipoMovimiento_Id` = pnMovTipoMovimiento_Id,
                        `nMovTipopago` = pnMovTipopago,
                        `Almacen_Id` = pAlmacen_Id,
                        `nMovTipoOrigenDestino` = pnMovTipoOrigenDestino,
                        `nMovOrigenDestino_Id` = pnMovOrigenDestino_Id,
                        `nMovTipodestino` = pnMovTipodestino,
                        `Documento_Id` = pDocumento_Id,
                        `sMovDocumento` = psMovDocumento,
                        `sMovDocReferencia` = psMovDocReferencia,
                        `Moneda_Id` = pMoneda_Id,
                        `nMovTipoCambio` = pnMovTipoCambio,
                        `nMovImporte` = pnMovImporte,
                        `nMovIGV` = pnMovIGV,
                        `nMovTotal` = pnMovTotal,
                        `nMovSaldo` = pnMovSaldo,
                        `nMovTotalCancelado` = pnMovTotalCancelado,
                        `dMovDetraccion` = pdMovDetraccion,
                        `dMovPercepcion` = pdMovPercepcion,
                        `dMovRetencion` = pdMovRetencion,
                        `sMovObservacion` = psMovObservacion,
                        `nMovEstado` = pnMovEstado,
                        `nMovEliminado` = pnMovEliminado,
                        `dMovFecha_Act` = now(),
                        `Usuario_Id` = pUsuario_Id
                  WHERE `Movimiento_Id`=pMovimiento_Id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `mov_listar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `mov_listar`(IN `pMovimiento_Id` INT(11))
BEGIN
   IF pMovimiento_Id = 0 THEN
        SELECT m.Movimiento_Id
               ,m.dMovFecha
               ,m.nMovTipoMovimiento_Id
               ,t.sTMovNombre
               ,m.nMovTipopago
               ,m.Almacen_Id
               ,m.nMovTipoOrigenDestino
               ,m.nMovOrigenDestino_Id
               ,a.sAlmNombre
               ,m.nMovTipodestino
               ,m.Documento_Id
               ,d.sDocNombre
			   ,d.sDocNombreCorto
               ,m.sMovDocumento
               ,m.sMovDocReferencia
               ,m.Moneda_Id
               ,m.nMovTipoCambio
               ,m.nMovImporte
               ,m.nMovIGV
               ,m.nMovTotal
			   ,m.nMovSaldo
               ,m.nMovTotalCancelado
               ,m.dMovDetraccion

               ,m.dMovPercepcion
               ,m.dMovRetencion
               ,m.sMovObservacion
               ,m.nMovEstado
               ,m.nMovEliminado
               ,m.dMovFecha_Act
               ,m.Usuario_Id
               ,nMovClasificador_Id
               ,nMovMovimiento_Id
               ,nMovOrdenCampo_Id
               ,CONCAT( d.sDocNombreCorto, ' ', m.sMovDocumento) as Documento

        FROM `movimiento` m 

        INNER JOIN `tipomovimiento` t
        ON t.TipoMovimiento_Id = m.nMovTipoMovimiento_Id 
        INNER JOIN `almacen` a
        ON a.Almacen_Id = m.Almacen_Id
        INNER JOIN `documento` d
        ON d.Documento_Id = m.Documento_Id 
        WHERE m.nMovEliminado = 0;
   ELSE
        SELECT m.Movimiento_Id
               ,m.dMovFecha
               ,m.nMovTipoMovimiento_Id
               ,t.sTMovNombre
               ,m.nMovTipopago
               ,m.Almacen_Id
               ,m.nMovTipoOrigenDestino
               ,m.nMovOrigenDestino_Id
               ,a.sAlmNombre
               ,m.nMovTipodestino
               ,m.Documento_Id
               ,d.sDocNombre
			   ,d.sDocNombreCorto
               ,m.sMovDocumento
               ,m.sMovDocReferencia
               ,m.Moneda_Id
               ,m.nMovTipoCambio
               ,m.nMovImporte
               ,m.nMovIGV
               ,m.nMovTotal
			   ,m.nMovSaldo
               ,m.nMovTotalCancelado
               ,m.dMovDetraccion

               ,m.dMovPercepcion
               ,m.dMovRetencion
               ,m.sMovObservacion
               ,m.nMovEstado
               ,m.nMovEliminado
               ,m.dMovFecha_Act
               ,m.Usuario_Id
               ,nMovClasificador_Id
               ,nMovMovimiento_Id
               ,nMovOrdenCampo_Id
				,CONCAT( d.sDocNombreCorto, ' ', m.sMovDocumento) as Documento

        FROM `movimiento` m 
        INNER JOIN `tipomovimiento` t
        ON t.TipoMovimiento_Id = m.nMovTipoMovimiento_Id 
        INNER JOIN `almacen` a
        ON a.Almacen_Id = m.Almacen_Id
        INNER JOIN `documento` d
        ON d.Documento_Id = m.Documento_Id
		WHERE m.`Movimiento_Id` = pMovimiento_Id AND m.nMovEliminado = 0;
   END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `OD_Crear` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `OD_Crear`(IN `pnODTipo` INT(11), IN `psODRucDni` VARCHAR(11), IN `psODNombre` VARCHAR(50), IN `psODNombreComercial` VARCHAR(50), IN `psODDireccion` VARCHAR(100), IN `psODTelefono` VARCHAR(50), IN `psODCorreo` VARCHAR(50), IN `psODContacto` VARCHAR(30), IN `pnODDiasCredito` VARCHAR(11), IN `pnODEstado` TINYINT(4), IN `pnODUsuario_Id` INT(11), IN `pnODEliminado` TINYINT(4))
BEGIN
    
	
	
	
			DECLARE	pOrigenDestino_Id INT(11);
    
			INSERT INTO origendestino
			   (nODTipo
			   ,sODRucDni
			   ,sODNombre
			   ,sODNombreComercial
			   ,sODDireccion
			   ,sODTelefono
			   ,sODCorreo
			   ,sODContacto
			   ,nODDiasCredito
               ,nODEstado
			   ,dODFechaActualizacion
			   ,nODUsuario_Id
               ,nODEliminado)
			VALUES
			   (pnODTipo,
			   psODRucDni,
			   psODNombre,
			   psODNombreComercial,
			   psODDireccion ,
			   psODTelefono,
			   psODCorreo,
			   psODContacto,
			   pnODDiasCredito,
               pnODEstado,
			   now(),
			   pnODUsuario_Id, 
               pnODEliminado);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `OD_Editar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `OD_Editar`(IN `pOrigenDestino_Id` INT, IN `pnODTipo` INT(11), IN `psODRucDni` VARCHAR(11), IN `psODNombre` VARCHAR(50), IN `psODNombreComercial` VARCHAR(50), IN `psODDireccion` VARCHAR(100), IN `psODTelefono` VARCHAR(50), IN `psODCorreo` VARCHAR(50), IN `psODContacto` VARCHAR(30), IN `pnODDiasCredito` VARCHAR(11), IN `pnODEstado` TINYINT(4), IN `pnODUsuario_Id` INT, IN `pnODEliminado` TINYINT(4))
BEGIN
		   UPDATE origendestino
		   SET sODRucDni = psODRucDni
			  ,nODTipo = pnODTipo
			  ,sODNombre = psODNombre
			  ,sODNombreComercial = psODNombreComercial
			  ,sODDireccion = psODDireccion
			  ,sODTelefono = psODTelefono
			  ,sODCorreo = psODCorreo
			  ,sODContacto = psODContacto
			  ,nODDiasCredito=pnODDiasCredito
			  ,nODEstado=pnODEstado
			  ,dODFechaActualizacion = now()
			  ,nODUsuario_Id = pnODUsuario_Id
			  ,nODEliminado = pnODEliminado
		   WHERE OrigenDestino_Id=pOrigenDestino_Id;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `OD_eliminar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `OD_eliminar`(IN `pOrigenDestino_Id` INT(11))
BEGIN
        UPDATE `origendestino` SET `nODEliminado` = 1 WHERE `OrigenDestino_Id` = pOrigenDestino_Id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `OD_Listar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `OD_Listar`(IN `pOrigenDestino_Id` INT(11))
BEGIN
   IF pOrigenDestino_Id = 0 THEN
        SELECT OrigenDestino_Id
               ,nODTipo
               ,sODRucDni
               ,sODNombre
               ,sODNombreComercial
               ,sODDireccion
               ,sODTelefono
               ,sODCorreo
               ,sODContacto
               ,nODDiasCredito
               ,nODEstado
               ,dODFechaActualizacion
               ,nODUsuario_Id
               ,nODEliminado
        FROM `origendestino` WHERE nODEliminado = 0
        ORDER BY OrigenDestino_Id ASC;
   ELSE
        SELECT OrigenDestino_Id
               ,nODTipo
               ,sODRucDni
               ,sODNombre
               ,sODNombreComercial
               ,sODDireccion
               ,sODTelefono
               ,sODCorreo
               ,sODContacto
               ,nODDiasCredito
               ,nODEstado
               ,dODFechaActualizacion
               ,nODUsuario_Id
               ,nODEliminado
        FROM `origendestino` WHERE `OrigenDestino_Id` = pOrigenDestino_Id  AND nODEliminado = 0
		ORDER BY OrigenDestino_Id ASC;
   END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ope_crear` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ope_crear`(IN `psOpeDni` VARCHAR(8), IN `psOpeApePat` VARCHAR(45), IN `psOpeApeMat` VARCHAR(45), IN `psOpeNombres` VARCHAR(45), IN `psOpeCargo` VARCHAR(45), IN `psOpeDireccion` VARCHAR(45), IN `psOpeTelefono` VARCHAR(45), IN `psOpeCorreo` VARCHAR(45), IN `pnOpeEstado` TINYINT(4))
BEGIN
	INSERT INTO `operario`(
								sOpeDni,
								sOpeApePat,
								sOpeApeMat,
								sOpeNombres,
								sOpeCargo,
								sOpeDireccion,
								sOpeTelefono,
								sOpeCorreo,
								nOpeEstado
                              ) 
                      VALUES (
                              psOpeDni,
								psOpeApePat,
								psOpeApeMat,
								psOpeNombres,
								psOpeCargo,
								psOpeDireccion,
								psOpeTelefono,
								psOpeCorreo,
								pnOpeEstado
                              );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ope_editar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ope_editar`(IN `psOpeDni` VARCHAR(8), IN `psOpeApePat` VARCHAR(45), IN `psOpeApeMat` VARCHAR(45), IN `psOpeNombres` VARCHAR(45), IN `psOpeCargo` VARCHAR(45), IN `psOpeDireccion` VARCHAR(45), IN `psOpeTelefono` VARCHAR(45), IN `psOpeCorreo` VARCHAR(45), IN `pnOpeEstado` TINYINT(4), IN `pOperario_Id` INT(11))
BEGIN
    UPDATE `operario` SET 
sOpeDni = psOpeDni,
sOpeApePat = psOpeApePat,
sOpeApeMat = psOpeApeMat,
sOpeNombres = psOpeNombres,
sOpeCargo = psOpeCargo,
sOpeDireccion = psOpeDireccion,
sOpeTelefono = psOpeTelefono,
sOpeCorreo = psOpeCorreo,
nOpeEstado = pnOpeEstado
                  WHERE `Operario_Id`= pOperario_Id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ope_eliminar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ope_eliminar`(IN `pOperario_Id` INT(11))
BEGIN
        UPDATE `operario` SET nOpeEliminado = 1 WHERE `Operario_Id` = pOperario_Id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ope_listar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ope_listar`(IN `pOperario_Id` INT(11))
BEGIN

   IF pOperario_Id = 0 THEN
		SELECT Operario_Id,
		   sOpeApePat,
			sOpeDni,
			sOpeApePat,
			sOpeApeMat,
			sOpeNombres,
			sOpeCargo,
			sOpeDireccion,
			sOpeTelefono,
			sOpeCorreo,
			nOpeEstado
			FROM 11protex.operario WHERE nOpeEliminado = 0 ;
    ELSE
					SELECT Operario_Id,
		   sOpeApePat,
			sOpeDni,
			sOpeApePat,
			sOpeApeMat,
			sOpeNombres,
			sOpeCargo,
			sOpeDireccion,
			sOpeTelefono,
			sOpeCorreo,
			nOpeEstado
			FROM 11protex.operario WHERE nOpeEliminado = 0  AND Operario_id = pOperario_Id;

	END IF;
         
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `otrabdet_buscar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `otrabdet_buscar`( IN `otrab_Id` INT(11),  IN `pProdServ_Id` INT(11))
BEGIN
SELECT       OrdenTrabajo_detalle,
               OrdenTrabajo_Id
               ,pd.ProdServ_Id
			   ,pr.sProSrvNombre
               ,pd.UnidadMedida_Id
               ,un.sUndAlias
               ,CantidadAsig
               ,CantidadProg
               ,CantidadRest
        FROM `ordentrabajo_detalle` pd 
        
        left join `prodserv` pr on pr.ProdServ_Id = pd.ProdServ_Id
        left join `unidadmedida` un on un.UnidadMedida_Id = pd.UnidadMedida_Id 
         
        where OrdenTrabajo_Id = otrab_Id
        and pd.ProdServ_Id = pProdServ_Id
        order by OrdenTrabajo_detalle desc limit 0,1;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `otrabdet_crear` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `otrabdet_crear`(IN `ordentrabajo_id` INT(11), IN `ProdServ_Id` INT(11), IN `UnidadMedida_Id` INT(11), IN `CantidadAsig` INT(11), IN `CantidadProg` INT(11), IN `CantidadRest` INT(11))
BEGIN
    INSERT INTO `ordentrabajo_detalle`(
                              `OrdenTrabajo_Id`,
                              `ProdServ_Id`,
                              `UnidadMedida_Id`,
                              `CantidadAsig`,
                              `CantidadProg`,
                              `CantidadRest`

                              ) 
                      VALUES (ordentrabajo_id,
                              ProdServ_Id,
                              UnidadMedida_Id,
                              CantidadAsig,
                              CantidadProg,
                              CantidadRest);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `otrabdet_editar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `otrabdet_editar`(
  IN `otrab_Id` INT(11),
  IN `ProdServ_Id` INT(11),
  IN `UnidadMedida_Id` INT(11),
  IN `CantidadAsig` DECIMAL(10,2),
  IN `CantidadProg` DECIMAL(10,2),
  IN `CantidadRest` DECIMAL(10,2)
  )
BEGIN
    UPDATE `ordentrabajo_detalle` SET 
                   `ProdServ_Id`=ProdServ_Id,
                   `UnidadMedida_Id`=UnidadMedida_Id,
                   `CantidadAsig`=CantidadAsig,
				   `CantidadProg`=CantidadProg, 
				   `CantidadRest`= CantidadRest      
                  WHERE `OrdenTrabajo_Id`=otrab_Id
                  AND `ProdServ_Id`=ProdServ_Id
				  AND `UnidadMedida_Id`=UnidadMedida_Id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `otrabdet_listar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `otrabdet_listar`(IN `pCodigo` INT(11), IN `pTipo` INT(11))
BEGIN
IF pTipo = 0 THEN
        SELECT OrdenTrabajo_detalle,
               OrdenTrabajo_Id
               ,pd.ProdServ_Id
			   ,pr.sProSrvNombre
               ,pd.UnidadMedida_Id
               ,un.sUndAlias
               ,CantidadAsig
               ,CantidadProg
               ,CantidadRest
        FROM `ordentrabajo_detalle` pd 
        
        left join `prodserv` pr on pr.ProdServ_Id = pd.ProdServ_Id
        left join `unidadmedida` un on un.UnidadMedida_Id = pd.UnidadMedida_Id 
        ORDER BY OrdenTrabajo_detalle ;
   else 
	         SELECT OrdenTrabajo_detalle,
               OrdenTrabajo_Id
               ,pd.ProdServ_Id
			   ,pr.sProSrvNombre
               ,pd.UnidadMedida_Id
               ,un.sUndAlias
               ,CantidadAsig
               ,CantidadProg
               ,CantidadRest
        FROM `ordentrabajo_detalle` pd 
        
        left join `prodserv` pr on pr.ProdServ_Id = pd.ProdServ_Id
        left join `unidadmedida` un on un.UnidadMedida_Id = pd.UnidadMedida_Id 
         
        where pd.OrdenTrabajo_Id = pCodigo;
  end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `otrab_buscar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `otrab_buscar`(IN `oNroOrdenTrabajo` VARCHAR(10))
BEGIN
         SELECT ot.OrdenTrabajo_Id
               ,ot.otNroOrdenTrabajo
               
               
        FROM `ordentrabajo` ot
        WHERE otEliminado = 0 
	    AND otEstado = 1 
        AND ot.otNroOrdenTrabajo = oNroOrdenTrabajo
        ORDER BY ot.OrdenTrabajo_Id DESC limit 0,1;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `otrab_crear` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `otrab_crear`(IN `Fecha` DATE,
 IN `NroOrdenCompra` VARCHAR(10), IN `Cliente_Id` INT(11),
 IN `NroOrdenTrabajo` VARCHAR(10), IN `FechaE` DATE, IN `Estado` TINYINT(4),
 IN `Eliminado` TINYINT(4), IN `Usuario_Id` INT(11), OUT `ot_Id` INT(11))
BEGIN
    INSERT INTO `ordentrabajo`(
                              `otFecha`,
                              `otNroOrdenCompra`,
                              `otCliente_Id`,
                              `otNroOrdenTrabajo`,
                              `otFechaE`,
                              `otEstado`,
                              `otEliminado`,
                              `otFecha_Act`,
                              `Usuario_Id`) 
                      VALUES ( Fecha,
                               NroOrdenCompra,
							   Cliente_Id,
							   NroOrdenTrabajo,
							   FechaE,
							   Estado,
							   Eliminado,
							   now(),
							   Usuario_Id);
		
      SET ot_Id = LAST_INSERT_ID() ;
	  Select ot_Id as OrdenTrabajo_Id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `otrab_date` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `otrab_date`(IN `pInicio` DATE, IN `pFin` DATE)
    NO SQL
BEGIN 
SELECT OrdenTrabajo_Id
               ,otFecha
			   ,otNroOrdenCompra
			   ,otCliente_Id
			   
			   ,o.sODNombre	
               
			   ,otNroOrdenTrabajo
               ,otFechaE
               ,otEstado
               ,otEliminado
               ,otFecha_Act
               ,Usuario_Id
               
        FROM `ordentrabajo` ot 
        INNER JOIN `origendestino` o ON ot.otCliente_Id=o.OrigenDestino_Id
        WHERE otEliminado = 0
		AND ot.otFecha>=pInicio
		AND ot.otFecha<=pFin
ORDER BY ot.otFecha DESC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `otrab_editar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `otrab_editar`(

  IN `pOrdenTrabajo_Id` INT(11),
  IN `potFecha` DATE, 
  IN `potNroOrdenCompra` VARCHAR(10),
  IN `potCliente_Id` INT(11),
  IN `potNroOrdenTrabajo` VARCHAR(10),
  IN `potFechaE` DATE, 
  IN `potEstado` TINYINT(4),
  IN `pUsuario_Id` INT(11)
)
BEGIN
    UPDATE `ordentrabajo` SET 
                        `otFecha` = potFecha,
                        `otNroOrdenCompra` = potNroOrdenCompra,
                        `otCliente_Id` = potCliente_Id,
                        `otNroOrdenTrabajo` = potNroOrdenTrabajo,
                        `otFechaE` = potFechaE,
                        `otEstado` = potEstado,
                        `otFecha_Act` = now(),
                        `Usuario_Id` = pUsuario_Id
						
                  WHERE `OrdenTrabajo_Id`= pOrdenTrabajo_Id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `otrab_eliminar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `otrab_eliminar`(IN `pOrdenTrabajo_Id` INT(11))
BEGIN
        UPDATE `ordentrabajo` SET `otEliminado` = 1 WHERE `OrdenTrabajo_Id` = pOrdenTrabajo_Id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `otrab_listar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `otrab_listar`(IN `oOrdenTrabajo_Id` INT(11))
BEGIN
   IF oOrdenTrabajo_Id = 0 THEN
         SELECT OrdenTrabajo_Id
               ,otFecha
			   ,otNroOrdenCompra
			   ,otCliente_Id
			   
			   ,o.sODNombre	
               
			   ,otNroOrdenTrabajo
               ,otFechaE
               ,otEstado
               ,otEliminado
               ,otFecha_Act
               ,Usuario_Id
               
        FROM `ordentrabajo` ot 
        INNER JOIN `origendestino` o ON ot.otCliente_Id=o.OrigenDestino_Id
        WHERE otEliminado = 0 AND otEstado = 1 ORDER BY OrdenTrabajo_Id DESC;
   ELSE
         SELECT OrdenTrabajo_Id
               ,otFecha
			   ,otNroOrdenCompra
			   ,otCliente_Id
			   
			   ,o.sODNombre	
               
			   ,otNroOrdenTrabajo
               ,otFechaE
               ,otEstado
               ,otEliminado
               ,otFecha_Act
               ,Usuario_Id
               
        FROM `ordentrabajo` ot 
        INNER JOIN `origendestino` o ON ot.otCliente_Id=o.OrigenDestino_Id
        WHERE otEliminado = 0 AND `OrdenTrabajo_Id` = oOrdenTrabajo_Id ORDER BY OrdenTrabajo_Id DESC;
   END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Pag_Crear` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `Pag_Crear`(IN `pMovimiento_Id` INT, IN `pdPagFecha` VARCHAR(40), IN `pnPagTipoPago_Id` INT, IN `psPagDocumento` VARCHAR(15), IN `pnPagImporte` DOUBLE, IN `pnPagUsuario_Id` INT)
BEGIN
	DECLARE pPago_Id INT;
	DECLARE pImporteCancelado DOUBLE;
    DECLARE pTotalFactura DOUBLE;

         INSERT INTO pago
           (Movimiento_Id
           ,dPagFecha
           ,nPagTipoPago_Id
           ,sPagDocumento
           ,nPagImporte
           ,nPagLetra_Id
           ,dPagFecha_Act
           ,nPagUsuario_Id)
     VALUES
           (pMovimiento_Id,
           pdPagFecha, 
           pnPagTipoPago_Id,
           psPagDocumento,            
           pnPagImporte, 
           1,
           NOW(), 
           pnPagUsuario_Id);
			SET pPago_Id = LAST_INSERT_ID() ;
			Select pPago_Id as Pago_Id;

    	SET pImporteCancelado=(SELECT nMovTotalCancelado
    FROM movimiento
    WHERE Movimiento_Id=pMovimiento_Id);
    SET pTotalFactura = (SELECT nMovTotal
    FROM movimiento
    WHERE Movimiento_Id=pMovimiento_Id);

    UPDATE movimiento
    SET nMovTotalCancelado = pImporteCancelado + pnPagImporte
    WHERE Movimiento_Id=pMovimiento_Id;

	        IF pTotalFactura <= ( pImporteCancelado + pnPagImporte) then
		BEGIN
			CALL Mov_ActualizaEstado (pMovimiento_Id, 2, pnPagUsuario_Id);
		END;
	 		 													END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Pag_eliminar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `Pag_eliminar`(IN `pPago_Id` INT(11))
BEGIN
        UPDATE `pago` SET nPagEliminado = 1 WHERE `Pago_Id` = pPago_Id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Pag_listar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `Pag_listar`(IN `pEstado` INT(11), IN `pCodigo` INT(11))
BEGIN
   IF pEstado = 0 THEN
        SELECT Pago_Id
               ,Movimiento_Id
               ,dPagFecha
               ,nPagTipoPago_Id
               ,sPagDocumento
               ,nPagImporte
               ,nPagLetra_Id
               ,dPagFecha_Act
               ,nPagUsuario_Id
               ,nPagEliminado
        FROM `pago` WHERE Movimiento_Id = pCodigo AND nPagEliminado=0;
   ELSE
        SELECT Pago_Id
               ,Movimiento_Id
               ,dPagFecha
               ,nPagTipoPago_Id
               ,sPagDocumento
               ,nPagImporte
               ,nPagLetra_Id
               ,dPagFecha_Act
               ,nPagUsuario_Id
               ,nPagEliminado
        FROM `pago` WHERE `Pago_Id` = pPago_Id and nPagEliminado=0;
   END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `param_editar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `param_editar`(IN `psParValor` TEXT, IN `pParametro_Id` VARCHAR(50))
    NO SQL
UPDATE `parametros` SET `sParValor`=psParValor WHERE `Parametro_Id`=pParametro_Id ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `param_listar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `param_listar`(IN `pParametro_Id` INT)
    NO SQL
BEGIN 
   IF pParametro_Id = 0 THEN
       SELECT `Parametro_Id`, `sParDescripcion`, `sParValor` FROM `parametros`;
   ELSE 
       SELECT `Parametro_Id`, `sParDescripcion`, `sParValor` FROM `parametros` WHERE `Parametro_Id` = pParametro_Id;
   END IF; 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `peddet_crear` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `peddet_crear`(IN `pPedido_Id` INT(11), IN `pProdServ_Id` INT(11), IN `pUnidadMedida_Id` INT(11), IN `pnPedDetCantidad` INT(11), IN `pdPedDetPrecioUnitario` DECIMAL(10,2), IN `pdPedDetSubtotal` DECIMAL(10,2))
BEGIN
    INSERT INTO `pedido_detalle`(
                              `Pedido_Id`,
                              `ProdServ_Id`,
                              `UnidadMedida_Id`,
                              `nPedDetCantidad`,
                              `dPedDetPrecioUnitario`,
                              `dPedDetSubtotal`

                              ) 
                      VALUES (
                              pPedido_Id,
                              pProdServ_Id,
                              pUnidadMedida_Id,
                              pnPedDetCantidad,
                              pdPedDetPrecioUnitario,
                              pdPedDetSubtotal
                              );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `peddet_editar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `peddet_editar`(
  IN `pdPedidodetalle_Id` INT(11),
  IN `pdPedido_Id` INT(11),
  IN `pdProdServ_Id` INT(11),
  IN `pdUnidadMedida_Id` INT(11),
  IN `pdnPedDetCantidad` INT(11),
  IN `pddPedDetPrecioUnitario` DECIMAL(10,2), 
  IN `pddPedDetSubTotal` DECIMAL(10,2)
  )
BEGIN
    UPDATE `pedido_detalle` SET 
                   `ProdServ_Id`=pdProdServ_Id,
                   `UnidadMedida_Id`=pdUnidadMedida_Id,
                   `nPedDetCantidad`=pdnPedDetCantidad,
				   `dPedDetPrecioUnitario`=pddPedDetPrecioUnitario, 
				   `dPedDetSubTotal`= pddPedDetSubTotal      
                  WHERE `Pedido_Id`=pdPedido_Id AND `Pedidodetalle_Id`=pdPedidodetalle_Id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `peddet_eliminar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `peddet_eliminar`(IN `pPedido_Id` INT(11))
BEGIN
        DELETE FROM `pedido_detalle` WHERE `Pedido_Id` = pPedido_Id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `peddet_listar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `peddet_listar`(IN `pCodigo` INT(11), IN `pTipo` INT(11))
BEGIN
   IF pTipo = 0 THEN
        SELECT Pedidodetalle_Id
               ,Pedido_Id
               ,pd.ProdServ_Id
			   ,pr.sProSrvNombre
               ,pd.UnidadMedida_Id
               ,un.sUndAlias
               ,nPedDetCantidad
               ,dPedDetPrecioUnitario
               ,dPedDetSubtotal
        FROM `pedido_detalle` pd 
        left join `prodserv` pr on pr.ProdServ_Id = pd.ProdServ_Id
        left join `unidadmedida` un on un.UnidadMedida_Id = pd.UnidadMedida_Id  
		WHERE `Pedido_Id` = pCodigo;
   ELSE
        SELECT Pedidodetalle_Id
               ,Pedido_Id
               ,pd.ProdServ_Id
			   ,pr.sProSrvNombre
               ,pd.UnidadMedida_Id
               ,un.sUndAlias
               ,nPedDetCantidad
               ,dPedDetPrecioUnitario
               ,dPedDetSubtotal
        FROM `pedido_detalle` pd 
        left join `prodserv` pr on pr.ProdServ_Id = pd.ProdServ_Id
        left join `unidadmedida` un on un.UnidadMedida_Id = pd.UnidadMedida_Id 
		WHERE `Pedido_Id` = pCodigo;
   END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ped_cerrar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ped_cerrar`(IN `pPedido_Id` INT(11))
BEGIN
        UPDATE `pedido` SET `nPedEstado`=2 WHERE `Pedido_Id` = pPedido_Id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ped_crear` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ped_crear`(IN `pOrigenDestino_Id` INT(11), IN `pdPedFecha` DATE, IN `pnPedTipopago` INT(11), IN `pDocumento_Id` INT(11), IN `pdPedImporte` DECIMAL(10,2), IN `pdPedIGV` DECIMAL(10,2), IN `pdPedTotal` DECIMAL(10,2), IN `pdPedSaldo` DECIMAL(10,2), IN `psPedObservaciones` VARCHAR(200), IN `pnPedEstado` TINYINT(4), IN `pdPedEliminado` TINYINT(4), IN `pUsuario_Id` INT(11),IN `pdPedImporteAd` DECIMAL(10,2),IN `pnroOrdenTrabajo` VARCHAR(10),IN `pMoneda_Id` INT(10), OUT `pPedido_Id` INT(11))
BEGIN
    INSERT INTO `pedido`(
                              `OrigenDestino_Id`,
                              `dPedFecha`,
                              `nPedTipopago`,
                              `Documento_Id`,
                              `dPedImporte`,
                              `dPedIGV`,
                              `dPedTotal`,
                              `dPedSaldo`,
                              `sPedObservaciones`,
                              `nPedEstado`,
                              `dPedEliminado`,
                              `dPedFecha_Act`,
                              `Usuario_Id`,
                              `dPedImporteAd`,
                              `nroOrdenTrabajo`,
                              `Moneda_Id`
                              ) 
                      VALUES (
                              pOrigenDestino_Id,
                              pdPedFecha,
                              pnPedTipopago,
                              pDocumento_Id,
                              pdPedImporte,
                              pdPedIGV,
                              pdPedTotal,
                              pdPedSaldo,
                              psPedObservaciones,
                              pnPedEstado,
                              pdPedEliminado,
                              now(),
                              pUsuario_Id,
                              pdPedImporteAd,
                              pnroOrdenTrabajo,
                              pMoneda_Id
                              );
		
      SET pPedido_Id = LAST_INSERT_ID() ;
	  Select pPedido_Id as Pedido_Id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ped_date` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ped_date`(IN `pInicio` DATE, IN `pFin` DATE)
    NO SQL
BEGIN
SELECT Pedido_Id
               ,p.OrigenDestino_Id
			   ,o.sODRucDni
			   ,o.sODNombre	
               ,dPedFecha
               ,nPedTipopago
               ,Documento_Id
               ,dPedImporteAd
               ,dPedImporte
               ,dPedIGV
               ,dPedTotal
			   ,dPedSaldo
               ,sPedObservaciones
               ,nPedEstado
               ,dPedEliminado
               ,dPedFecha_Act
               ,Usuario_Id
               ,nroOrdenTrabajo
        FROM `pedido` p INNER JOIN `origendestino` o ON p.OrigenDestino_Id=o.OrigenDestino_Id
        WHERE dPedEliminado = 0 
		AND p.dPedFecha>=pInicio
		AND p.dPedFecha<=pFin
ORDER BY p.dPedFecha DESC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ped_editar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ped_editar`(

  IN `pOrigenDestino_Id` INT(11),
  IN `pdPedFecha` DATE, 
  IN `pnPedTipopago` INT(11),
  IN `pDocumento_Id` INT(11),
  IN `pdPedImporteAd` DECIMAL(10,2),
  IN `pdPedImporte` DECIMAL(10,2), 
  IN `pdPedIGV` DECIMAL(10,2),
  IN `pdPedTotal` DECIMAL(10,2),
  IN `pdPedSaldo` DECIMAL(10,2),
  IN `psPedObservaciones` VARCHAR(200),
  IN `pnPedEstado` TINYINT(4),
  IN `pdPedEliminado` TINYINT(4), 
  IN `pUsuario_Id` INT(11),
  IN `pPedido_Id` INT(11),
  IN `pnroOrdenTrabajo` VARCHAR(10),
  IN `pMoneda_Id`  INT(10)
)
BEGIN
    UPDATE `pedido` SET 
                        `OrigenDestino_Id` = pOrigenDestino_Id,
                        `dPedFecha` = pdPedFecha,
                        `nPedTipopago` = pnPedTipopago,
                        `Documento_Id` = pDocumento_Id,
                        `dPedImporteAd` = pdPedImporteAd,
                        `dPedImporte` = pdPedImporte,
                        `dPedIGV` = pdPedIGV,
                        `dPedTotal` = pdPedTotal,
						`dPedSaldo` = pdPedSaldo,
                        `sPedObservaciones` = psPedObservaciones,
                        `nPedEstado` = pnPedEstado,
                        `dPedEliminado` = pdPedEliminado,
                        `dPedFecha_Act` = now(),
                        `Usuario_Id` = pUsuario_Id,
						`nroOrdenTrabajo` = pnroOrdenTrabajo,
                        `Moneda_Id` = pMoneda_Id

                  WHERE `Pedido_Id`=pPedido_Id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ped_eliminar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ped_eliminar`(IN `pPedido_Id` INT(11))
BEGIN
        UPDATE `pedido` SET `dPedEliminado` = 1 WHERE `Pedido_Id` = pPedido_Id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ped_estado` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ped_estado`(

  IN `pPedido_Id` INT(11)
  ,IN `pnPedEstado` TINYINT(4)
)
BEGIN
    UPDATE `pedido` SET 
                        `nPedEstado` = pnPedEstado
                        
                  WHERE `Pedido_Id`=pPedido_Id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ped_listar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ped_listar`(IN `pPedido_Id` INT(11))
BEGIN
   IF pPedido_Id = 0 THEN
         SELECT Pedido_Id
               ,p.OrigenDestino_Id
			   ,o.sODRucDni
			   ,o.sODNombre	
               ,dPedFecha
               ,nPedTipopago
               ,p.Documento_Id
               ,sDocNombre
               ,dPedImporteAd
               ,dPedImporte
               ,dPedIGV
               ,dPedTotal
			   ,dPedSaldo
               ,sPedObservaciones
               ,nPedEstado
               ,dPedEliminado
               ,dPedFecha_Act
               ,p.Usuario_Id
               ,nroOrdenTrabajo
               ,Moneda_Id
        FROM `pedido` p 
        INNER JOIN `origendestino` o ON p.OrigenDestino_Id=o.OrigenDestino_Id
        INNER JOIN `documento` d ON p.Documento_Id=d.Documento_Id
        WHERE dPedEliminado = 0  ORDER BY Pedido_Id DESC;
   ELSE
        SELECT Pedido_Id
               ,p.OrigenDestino_Id
			   ,o.sODRucDni
			   ,o.sODNombre	
               ,dPedFecha
               ,nPedTipopago
               ,p.Documento_Id
               ,sDocNombre
               ,dPedImporteAd
               ,dPedImporte
               ,dPedIGV
               ,dPedTotal
			   ,dPedSaldo
               ,sPedObservaciones
               ,nPedEstado
               ,dPedEliminado
               ,dPedFecha_Act
               ,p.Usuario_Id
               ,nroOrdenTrabajo
               ,Moneda_Id
        FROM `pedido` p 
        INNER JOIN `origendestino` o ON p.OrigenDestino_Id=o.OrigenDestino_Id
        INNER JOIN `documento` d ON p.Documento_Id=d.Documento_Id
	    WHERE dPedEliminado = 0 AND `Pedido_Id` = pPedido_Id;
   END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ped_listarODT` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ped_listarODT`(IN `pPedido_Id` INT(11))
BEGIN
   IF pPedido_Id = 0 THEN
         SELECT Pedido_Id
               ,p.OrigenDestino_Id
			   ,o.sODRucDni
			   ,o.sODNombre	
               ,dPedFecha
               ,nPedTipopago
               ,p.Documento_Id
               ,sDocNombre
               ,dPedImporteAd
               ,dPedImporte
               ,dPedIGV
               ,dPedTotal
			   ,dPedSaldo
               ,sPedObservaciones
               ,nPedEstado
               ,dPedEliminado
               ,dPedFecha_Act
               ,p.Usuario_Id
               ,nroOrdenTrabajo
               ,Moneda_Id
        FROM `pedido` p 
        INNER JOIN `origendestino` o ON p.OrigenDestino_Id=o.OrigenDestino_Id
        INNER JOIN `documento` d ON p.Documento_Id=d.Documento_Id
        WHERE dPedEliminado = 0 AND nroOrdenTrabajo IS NOT NULL 
        AND  nroOrdenTrabajo NOT IN (
			SELECT `otNroOrdenTrabajo`
            FROM `ordentrabajo_detalle` od
			INNER JOIN  ordentrabajo ot ON  od.`OrdenTrabajo_Id` = ot.`OrdenTrabajo_Id`
			GROUP BY od.OrdenTrabajo_Id 
			having SUM(od.`CantidadRest`) = 0
		)
        ORDER BY Pedido_Id DESC;
   ELSE
        SELECT Pedido_Id
               ,p.OrigenDestino_Id
			   ,o.sODRucDni
			   ,o.sODNombre	
               ,dPedFecha
               ,nPedTipopago
               ,p.Documento_Id
               ,sDocNombre
               ,dPedImporteAd
               ,dPedImporte
               ,dPedIGV
               ,dPedTotal
			   ,dPedSaldo
               ,sPedObservaciones
               ,nPedEstado
               ,dPedEliminado
               ,dPedFecha_Act
               ,p.Usuario_Id
               ,nroOrdenTrabajo
               ,Moneda_Id
        FROM `pedido` p 
        INNER JOIN `origendestino` o ON p.OrigenDestino_Id=o.OrigenDestino_Id
        INNER JOIN `documento` d ON p.Documento_Id=d.Documento_Id
	    WHERE (dPedEliminado = 0 AND `Pedido_Id` = pPedido_Id AND nroOrdenTrabajo IS NOT NULL AND  nroOrdenTrabajo NOT IN (
			SELECT otNroOrdenTrabajo
			FROM `ordentrabajo_detalle` od
			INNER JOIN  ordentrabajo ot ON  od.`OrdenTrabajo_Id` = ot.`OrdenTrabajo_Id`
			GROUP BY od.OrdenTrabajo_Id 
			having SUM(od.`CantidadRest`) > 0
		))
        ORDER BY Pedido_Id DESC;
   END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ped_otrab` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ped_otrab`(IN `pOrdenTrabajo` VARCHAR(10))
BEGIN
          SELECT Pedido_Id
               ,nPedEstado
               ,dPedEliminado
               ,nroOrdenTrabajo
               
        FROM `pedido`
        WHERE dPedEliminado = 0 AND nPedEstado = 1
        AND nroOrdenTrabajo = pOrdenTrabajo
        ORDER BY Pedido_Id DESC
        LIMIT 0,1;
   
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `pre_crear` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pre_crear`(IN `pProdServ_Id` INT(11), IN `pUnidadMedida_Id` INT(11), IN `pMoneda_Id` TINYINT(4),  IN `pPreVenPrecio` DECIMAL(10,2), IN `pnPreVenEliminado` TINYINT(4), IN `pUsuario_Id` INT(11))
BEGIN
    INSERT INTO `precioventa`(
                              `ProdServ_Id`,
                              `UnidadMedida_Id`,
                              `Moneda_Id`,
						      `PreVenPrecio`,
                              `dPreVenFechaAct`,
                              `nPreVenEliminado`,
                              `Usuario_Id`
                              ) 
                      VALUES (
                              pProdServ_Id,
                              pUnidadMedida_Id,
                              pMoneda_Id,
						      pPreVenPrecio,
                              now(),
                              pnPreVenEliminado,
                              pUsuario_Id
                              );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `pre_eliminar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pre_eliminar`(IN `pPrecioVenta_Id` INT(11))
BEGIN
        UPDATE `precioventa` SET nPreVenEliminado = 1 WHERE `PrecioVenta_Id` = pPrecioVenta_Id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `pre_listar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pre_listar`(IN `pCodigo` INT(11), IN `pTipo` TINYINT(4))
BEGIN
   IF pTipo = 0 THEN
        SELECT pre.PrecioVenta_Id
               ,pre.ProdServ_Id
			   ,pro.sProSrvNombre
               ,pre.UnidadMedida_Id
               ,pre.Moneda_Id
               ,uni.sUndDescripcion
               ,pre.PreVenPrecio
               ,pre.dPreVenFechaAct
               ,pre.nPreVenEliminado
               ,pre.Usuario_Id
        FROM `precioventa` pre
		INNER JOIN prodserv pro
		ON pro.ProdServ_Id = pre.ProdServ_Id 
		INNER JOIN unidadmedida uni
		ON uni.UnidadMedida_Id = pre.UnidadMedida_Id
		WHERE pre.nPreVenEliminado = 0 AND pre.ProdServ_Id = pCodigo;
   ELSE
        SELECT pre.PrecioVenta_Id
               ,pre.ProdServ_Id
			   ,pro.sProSrvNombre
               ,pre.UnidadMedida_Id
               ,pre.Moneda_Id
               ,uni.sUndDescripcion
               ,pre.PreVenPrecio
               ,pre.dPreVenFechaAct
               ,pre.nPreVenEliminado
               ,pre.Usuario_Id
        FROM `precioventa` pre
		INNER JOIN prodserv pro
		ON pro.ProdServ_Id = pre.ProdServ_Id 
		INNER JOIN unidadmedida uni
		ON uni.UnidadMedida_Id = pre.UnidadMedida_Id
		WHERE pre.nPreVenEliminado = 0
		AND pre.`PrecioVenta_Id` = pCodigo;
   END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `pre_obtenerprecio` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pre_obtenerprecio`(IN `pProdServ_Id` INT(11), IN `pUnidadMedida_Id` INT(11))
    NO SQL
SELECT	 pre.PrecioVenta_Id
        ,pre.ProdServ_Id
        ,pro.sProSrvNombre
        ,pre.UnidadMedida_Id
	    ,pre.Moneda_Id
        ,uni.sUndDescripcion
        ,pre.PreVenPrecio
        ,pre.dPreVenFechaAct
        ,pre.nPreVenEliminado
        ,pre.Usuario_Id
FROM `precioventa` pre
INNER JOIN prodserv pro
ON pro.ProdServ_Id = pre.ProdServ_Id 
INNER JOIN unidadmedida uni
ON uni.UnidadMedida_Id = pre.UnidadMedida_Id
WHERE pre.nPreVenEliminado = 0
AND pre.`ProdServ_Id` = pProdServ_Id
AND pre.`UnidadMedida_Id` = pUnidadMedida_Id ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `procli_crear` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `procli_crear`(IN `pnProcliTipo` INT(10), IN `pnProcliRUCDNI` CHAR(11), IN `psProcliNombre` VARCHAR(50), IN `psProcliNomComercial` VARCHAR(70), IN `psProcliDireccion` VARCHAR(70), IN `pnProcliTelefono` CHAR(9), IN `psProcliCorreo` VARCHAR(70), IN `psProcliContacto` VARCHAR(70), IN `pnProcliDiasCredito` INT(11), IN `pnProcliEstado` TINYINT(4), IN `pnProcliEliminado` TINYINT(4), IN `pUsuario_Id` INT(11))
BEGIN
    INSERT INTO `provclien`(
                              `nProcliTipo`,
                              `nProcliRUCDNI`,
                              `sProcliNombre`,
                              `sProcliNomComercial`,
                              `sProcliDireccion`,
                              `nProcliTelefono`,
                              `sProcliCorreo`,
                              `sProcliContacto`,
                              `nProcliDiasCredito`,
                              `nProcliEstado`,
                              `nProcliEliminado`,
                              `dProcliFecha_Act`,
                              `Usuario_Id`
                              ) 
                      VALUES (
                              pnProcliTipo,
                              pnProcliRUCDNI,
                              psProcliNombre,
                              psProcliNomComercial,
                              psProcliDireccion,
                              pnProcliTelefono,
                              psProcliCorreo,
                              psProcliContacto,
                              pnProcliDiasCredito,
                              pnProcliEstado,
                              pnProcliEliminado,
                              now(),
                              pUsuario_Id
                              );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `procli_editar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `procli_editar`(IN `pnProcliTipo` INT(11), IN `pnProcliRUCDNI` CHAR(11), IN `psProcliNombre` VARCHAR(50), IN `psProcliNomComercial` VARCHAR(70), IN `psProcliDireccion` VARCHAR(70), IN `pnProcliTelefono` CHAR(9), IN `psProcliCorreo` VARCHAR(70), IN `psProcliContacto` VARCHAR(70), IN `pnProcliDiasCredito` INT(11), IN `pnProcliEstado` TINYINT(4), IN `pnProcliEliminado` TINYINT(4), IN `pUsuario_Id` INT(11), IN `pProvclien_Id` INT(11))
BEGIN
    UPDATE `provclien` SET 
                        `nProcliTipo` = pnProcliTipo,
                        `nProcliRUCDNI` = pnProcliRUCDNI,
                        `sProcliNombre` = psProcliNombre,
                        `sProcliNomComercial` = psProcliNomComercial,
                        `sProcliDireccion` = psProcliDireccion,
                        `nProcliTelefono` = pnProcliTelefono,

                        `sProcliCorreo` = psProcliCorreo,
                        `sProcliContacto` = psProcliContacto,
                        `nProcliDiasCredito` = pnProcliDiasCredito,
                        `nProcliEstado` = pnProcliEstado,
                        `nProcliEliminado` = pnProcliEliminado,
                        `dProcliFecha_Act` = now(),
                        `Usuario_Id` = pUsuario_Id
                  WHERE `Provclien_Id`=pProvclien_Id;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `procli_eliminar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `procli_eliminar`(IN `pProvclien_Id` INT(11))
BEGIN
        UPDATE provclien SET `nProcliEliminado` = 1 WHERE `Provclien_Id` = pProvclien_Id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `procli_listar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `procli_listar`(IN `pProvclien_Id` INT(11))
BEGIN
   IF pProvclien_Id = 0 THEN
        SELECT Provclien_Id
               ,nProcliTipo
               ,nProcliRUCDNI
               ,sProcliNombre
               ,sProcliNomComercial
               ,sProcliDireccion
               ,nProcliTelefono
               ,sProcliCorreo
               ,sProcliContacto
               ,nProcliDiasCredito
               ,nProcliEstado
               ,nProcliEliminado
               ,dProcliFecha_Act
               ,Usuario_Id
        FROM `provclien` where nProcliEliminado = 0;
   ELSE
        SELECT Provclien_Id
               ,nProcliTipo
               ,nProcliRUCDNI
               ,sProcliNombre
               ,sProcliNomComercial
               ,sProcliDireccion
               ,nProcliTelefono
               ,sProcliCorreo
               ,sProcliContacto
               ,nProcliDiasCredito
               ,nProcliEstado
               ,nProcliEliminado
               ,dProcliFecha_Act
               ,Usuario_Id
        FROM `provclien` WHERE `Provclien_Id` = pProvclien_Id AND nProcliEliminado = 0;

   END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `prosrv_BuscarLotes` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `prosrv_BuscarLotes`(IN `pProductoLote_Id` INT)
    NO SQL
SELECT ProductoLote_Id
               ,ProdServ_Id
               ,Almacen_Id
               ,sProLNombreLote
               ,nProLStockReal
               ,dProLFechaVencimiento
               ,nProLPrecioL
               ,nProLPrecioC
               ,nProLTipoPrecio
               ,nProLPorc1
               ,nProLPrecioV
               ,nProLPorc2
               ,nProLPrecioV2
               ,nProLPorc3
               ,nProLPrecioV3
               ,dProLFecha_Act
               ,nProLUsuario_Id
               ,nProLMonedaCompra_Id
               ,nProLDescuentoCompra
        FROM `productolote` WHERE `ProductoLote_Id` = pProductoLote_Id ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `prosrv_fotomodif` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `prosrv_fotomodif`(IN `psProSrvCodigo` VARCHAR(50), IN `pProSrvNomImagen` VARCHAR(255))
BEGIN
  UPDATE `prodserv` SET                    
                `ProSrvNomImagen`=pProSrvNomImagen
      WHERE    `sProSrvCodigo`=psProSrvCodigo;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `prosrv_ListarLotes` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `prosrv_ListarLotes`(IN `pProdServ_Id` INT(11), IN `pConStock` INT(11))
BEGIN
		DECLARE pIGV DOUBLE;
		
		
		SET pIGV =(SELECT pIGV = nConValor
		FROM conceptofijo
		WHERE Concepto_Id=1);

		IF pConStock = 1 THEN
		    (Select 
		    PL.ProductoLote_Id,
		    A.sAlmNombre as Almacén,
		    PL.sProLNombreLote as Lote, 	     
			PL.dProLFechaVencimiento as Vencimiento,
			PL.nProLStockReal as Stock,	   
			U.sUndDescripcion as Unidad,
			PL.nProLTipoPrecio as TipoPrecio,
			CASE P.nProSrvExoneradoIGV

				WHEN 0 THEN	PL.nProLPrecioL * ( 1 + pIGV/100)
				WHEN 1 THEN	PL.nProLPrecioL 
			END as PrecioListaIGV,
			CASE P.nProSrvExoneradoIGV
				WHEN 0 THEN	PL.nProLPrecioC * ( 1 + pIGV/100)
				WHEN 1 THEN	PL.nProLPrecioC 
			END as PrecioCostoIGV,
			PL.nProLPrecioV as Precio1,
			PL.nProLPrecioV2 as Precio2,
			PL.nProLPrecioV3 as Precio3,
			PL.nProLPorc1 as Porc1,
			PL.nProLPorc2 as Porc2,
			PL.nProLPorc3 as Porc3,
			A.Almacen_Id,
			PL.nProLDescuentoCompra as Descuento,
			PL.nProLMonedaCompra_Id,
			PL.nProLPrecioC as PrecioCosto
			
			FROM productolote PL
			INNER JOIN prodserv P ON (P.ProdServ_Id = PL.ProdServ_Id)
			INNER JOIN almacen  A ON (A.Almacen_Id = PL.Almacen_Id)
			LEFT JOIN unidadmedida  U ON (U.UnidadMedida_Id = P.nProSrvUnidad_Id)
						WHERE PL.ProdServ_Id = pProdServ_Id
						AND PL.nProLStockReal > 0)
			order by PL.ProductoLote_Id;
		ELSE
			(SELECT
			PL.ProductoLote_Id,
		    A.sAlmNombre as Almacén,
		    PL.sProLNombreLote as Lote, 	     
			PL.dProLFechaVencimiento as Vencimiento,
			PL.nProLStockReal as Stock,	   
			U.sUndDescripcion as Unidad,
			PL.nProLTipoPrecio as TipoPrecio,
			PL.nProLPrecioL * ( 1 + pIGV/100) as PrecioListaIGV,
			PL.nProLPrecioC * ( 1 + pIGV/100) as PrecioCostoIGV,
			PL.nProLPrecioV as Precio1,
			PL.nProLPrecioV2 as Precio2,
			PL.nProLPrecioV3 as Precio3,
			PL.nProLPorc1 as Porc1,
			PL.nProLPorc2 as Porc2,
			PL.nProLPorc3 as Porc3,
			A.Almacen_Id,
			PL.nProLDescuentoCompra as Descuento,
			PL.nProLMonedaCompra_Id,
			PL.nProLPrecioC as PrecioCosto
			
			FROM productolote PL
			INNER JOIN prodserv P ON (P.ProdServ_Id = PL.ProdServ_Id)
			INNER JOIN almacen  A ON (A.Almacen_Id = PL.Almacen_Id)
			LEFT JOIN unidadmedida  U ON (U.UnidadMedida_Id = P.nProSrvUnidad_Id)
						WHERE PL.ProdServ_Id = pProdServ_Id)
						order by PL.ProductoLote_Id DESC;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `prosrv_movlistar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `prosrv_movlistar`(IN `psProLNombreLote` VARCHAR(15))
BEGIN
        SELECT ProductoLote_Id
               ,ProdServ_Id
               ,Almacen_Id
               ,sProLNombreLote
               ,nProLStockReal
               ,dProLFechaVencimiento
               ,nProLPrecioL
               ,nProLPrecioC
               ,nProLTipoPrecio
               ,nProLPorc1
               ,nProLPrecioV
               ,nProLPorc2
               ,nProLPrecioV2
               ,nProLPorc3
               ,nProLPrecioV3
               ,dProLFecha_Act
               ,nProLUsuario_Id
               ,nProLMonedaCompra_Id
               ,nProLDescuentoCompra
        FROM `productolote` WHERE `sProLNombreLote` = psProLNombreLote;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `prosrv_verificastock` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `prosrv_verificastock`(IN `ProdServ_Id` INT, IN `Cantidad` DOUBLE, IN `Unidad_Id` INT)
    NO SQL
BEGIN

	DECLARE STOCK double;
	DECLARE FACTOR DOUBLE;
    

    SET FACTOR = (select nUndFac_Cnv from UnidadMedida UM
    where  UM.UnidadMedida_Id =Unidad_Id);
    
	SET STOCK = (select SUM(nProLStockReal)  from ProductoLote PL
    where PL.ProdServ_Id=ProdServ_Id);
	
    IF (Cantidad * FACTOR) <= STOCK THEN
		SELECT 1 AS 'RESULT';
    ELSE
		SELECT 0 AS 'RESULT';
    END IF;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `pro_crear` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_crear`(IN `psProSrvCodigo` VARCHAR(50), IN `psProSrvNombre` VARCHAR(50), IN `psProSrvCodigoBarras` VARCHAR(50), IN `pnProSrvFamilia_Id` INT(11), IN `pActivo_Id` INT(11), IN `pnProSrvUnidad_Id` INT(11), IN `pProSrvEspecificacion` text, IN `pnProSrvExoneradoIGV` TINYINT(4), IN `pnProSrvEstado` TINYINT(4), IN `pnProSrvEliminado` INT(11), IN `pUsuario_Id` INT(11), OUT `pProdServ_Id` INT(11))
BEGIN

    INSERT INTO `prodserv`(

                              `sProSrvCodigo`,

                              `sProSrvNombre`,

                              `sProSrvCodigoBarras`,

                              `nProSrvFamilia_Id`,

                              `Activo_Id`,

                              `nProSrvUnidad_Id`,

                              `ProSrvEspecificacion`,

                              `nProSrvExoneradoIGV`,

                              `nProSrvEstado`,

                              `nProSrvEliminado`,

                              `dProSrvFecha_Act`,

                              `Usuario_Id`

                              ) 

                      VALUES (

                              psProSrvCodigo,

                              psProSrvNombre,

                              psProSrvCodigoBarras,

                              pnProSrvFamilia_Id,

                              pActivo_Id,

                              pnProSrvUnidad_Id,

                              pProSrvEspecificacion,     

                              pnProSrvExoneradoIGV,

                              pnProSrvEstado,

                              pnProSrvEliminado,

                              now(),

                              pUsuario_Id
                              );

    SET pProdServ_Id = LAST_INSERT_ID() ;   		

	Select pProdServ_Id as ProdServ_Id;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `pro_editar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_editar`(IN `psProSrvCodigo` VARCHAR(50), IN `psProSrvNombre` VARCHAR(50), IN `psProSrvCodigoBarras` VARCHAR(50), IN `pnProSrvFamilia_Id` INT(11), IN `pActivo_Id` INT(11), IN `pnProSrvUnidad_Id` INT(11), IN `pnProSrvExoneradoIGV` TINYINT(4), IN `pnProSrvEstado` TINYINT(4), IN `pnProSrvEliminado` INT(11), IN `pUsuario_Id` INT(11), IN `pProSrvEspecificacion` text, IN `pProdServ_Id` INT(11))
BEGIN
    UPDATE `prodserv` SET 
                        `sProSrvCodigo` = psProSrvCodigo,
                        `sProSrvNombre` = psProSrvNombre,
                        `sProSrvCodigoBarras` = psProSrvCodigoBarras,
                        `nProSrvFamilia_Id` = pnProSrvFamilia_Id,
                        `Activo_Id` = pActivo_Id,
                        `nProSrvUnidad_Id` = pnProSrvUnidad_Id,
                        `nProSrvExoneradoIGV` = pnProSrvExoneradoIGV,
                        `nProSrvEstado` = pnProSrvEstado,
                        `nProSrvEliminado` = pnProSrvEliminado,
                        `dProSrvFecha_Act` = now(),
                        `Usuario_Id` = pUsuario_Id,
                        `ProSrvEspecificacion` = pProSrvEspecificacion
                  WHERE `ProdServ_Id`=pProdServ_Id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `pro_eliminar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_eliminar`(IN `pProdServ_Id` INT(11))
BEGIN
        UPDATE `prodserv` SET nProSrvEliminado = 1 WHERE `ProdServ_Id` = pProdServ_Id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `pro_filtro` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_filtro`(IN `psProSrvNombre` VARCHAR(50))
    NO SQL
SELECT ps.ProdServ_Id
            ,ps.sProSrvCodigo
            ,ps.sProSrvNombre
            ,ps.sProSrvCodigoBarras
            ,ps.nProSrvFamilia_Id
            ,fm.sFamDescripcion               
            ,ps.Activo_Id
            ,av.sActDescripcion
            ,ps.nProSrvUnidad_Id
            ,um.sUndDescripcion
            ,ps.nProSrvExoneradoIGV
            ,ps.nProSrvEstado
            ,ps.nProSrvEliminado
            ,ps.dProSrvFecha_Act
            ,ps.Usuario_Id
    FROM prodserv ps
    INNER JOIN familia fm
    ON ps.nProSrvFamilia_Id = fm.Familia_Id 
    INNER JOIN activo av 
    ON ps.Activo_Id = av.Activo_Id 
    INNER JOIN unidadmedida um
    ON ps.nProSrvUnidad_Id = um.Unidadmedida_Id 
    WHERE ps.nProSrvEliminado = 0 AND ps.sProSrvNombre LIKE  CONCAT('%', psProSrvNombre, '%' ) 
    ORDER BY ps.sProSrvNombre ASC ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `pro_listar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_listar`(IN `pProdServ_Id` INT(11))
BEGIN
   IF pProdServ_Id = 0 THEN
        SELECT ps.ProdServ_Id
               ,ps.sProSrvCodigo
               ,ps.sProSrvNombre
               ,ps.sProSrvCodigoBarras
               ,ps.nProSrvFamilia_Id
               ,fm.sFamDescripcion               
               ,ps.Activo_Id
               ,av.sActDescripcion
               ,ps.nProSrvUnidad_Id
               ,um.sUndDescripcion

               ,ps.nProSrvExoneradoIGV
               ,ps.nProSrvEstado
               ,ps.nProSrvEliminado

               ,ps.dProSrvFecha_Act
               ,ps.Usuario_Id
        FROM prodserv ps
        INNER JOIN familia fm

        ON ps.nProSrvFamilia_Id = fm.Familia_Id 
        INNER JOIN activo av 
        ON ps.Activo_Id = av.Activo_Id 

        INNER JOIN unidadmedida um
        ON ps.nProSrvUnidad_Id = um.Unidadmedida_Id WHERE ps.nProSrvEliminado = 0 ORDER BY ps.sProSrvNombre ASC ;
   ELSE
        SELECT ps.ProdServ_Id
               ,ps.sProSrvCodigo
               ,ps.sProSrvNombre
               ,ps.sProSrvCodigoBarras
               ,ps.nProSrvFamilia_Id
               ,fm.sFamDescripcion               
               ,ps.Activo_Id
               ,av.sActDescripcion
               ,ps.nProSrvUnidad_Id
               ,um.sUndDescripcion
               ,ps.nProSrvExoneradoIGV
               ,ps.nProSrvEstado
               ,ps.nProSrvEliminado
               ,ps.dProSrvFecha_Act
               ,ps.Usuario_Id
               ,ps.ProSrvEspecificacion
               ,ps.ProSrvNomImagen
        FROM prodserv ps
        INNER JOIN familia fm

        ON ps.nProSrvFamilia_Id = fm.Familia_Id 
        INNER JOIN activo av 
        ON ps.Activo_Id = av.Activo_Id 
        INNER JOIN unidadmedida um
        ON ps.nProSrvUnidad_Id = um.Unidadmedida_Id WHERE ps.ProdServ_Id = pProdServ_Id AND ps.nProSrvEliminado = 0 ORDER BY ps.sProSrvNombre ASC;
   END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `pro_UltimoCodigo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_UltimoCodigo`(IN `pFamilia_id` INT(11))
BEGIN

	DECLARE UltCodigo char(20);
	DECLARE CodFamilia char(20);
    
			
	SET UltCodigo = (select  sProSrvCodigo
	from ProdServ 
	WHERE ProdServ_Id in (select MAX(ProdServ_Id) 
						  from ProdServ 
						  where nProSrvFamilia_Id=pFamilia_id
						  AND sProSrvCodigo <> '')

	);

	SET CodFamilia = CONCAT((SELECT sFamCodigo FROM familia WHERE Familia_Id = pFamilia_id),'-0');


				
	
	SELECT ifnull(UltCodigo, CodFamilia) AS UltCodigo;

		
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `rec_crear` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `rec_crear`(IN `pnRecProdServ_Id` INT(11), IN `pUnidadMedida_Id` INT(11), IN `pnRecCantidad` INT(11), IN `pnRecEliminado` TINYINT(4), IN `pUsuario_Id` INT(11), IN `pProdServ_Id` INT(11))
BEGIN

    INSERT INTO `receta`(

                              `nRecProdServ_Id`,

                              `UnidadMedida_Id`,

                              `nRecCantidad`,

                              `nRecEliminado`,

                              `dRecFecha_Act`,

                              `Usuario_Id`,

                              `ProdServ_Id`

                              ) 

                      VALUES (

                              pnRecProdServ_Id,

                              pUnidadMedida_Id,

                              pnRecCantidad,

                              pnRecEliminado,

                              now(),

                              pUsuario_Id,

                              pProdServ_Id

                              );

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `rec_eliminar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `rec_eliminar`(IN `pReceta_Id` INT(11))
BEGIN

        UPDATE `receta` SET nRecEliminado = 1 WHERE `Receta_Id` = pReceta_Id;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `rec_listar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `rec_listar`(IN `pProdServ_Id` INT(11))
BEGIN

   IF pProdServ_Id = 0 THEN

        SELECT Receta_Id

               ,nRecProdServ_Id

               ,UnidadMedida_Id

               ,nRecCantidad

               ,nRecEliminado

               ,dRecFecha_Act

               ,Usuario_Id

               ,ProdServ_Id

        FROM `receta` WHERE nRecEliminado = 0;

   ELSE

        SELECT Receta_Id

               ,nRecProdServ_Id

               ,UnidadMedida_Id

               ,nRecCantidad

               ,nRecEliminado

               ,dRecFecha_Act

               ,Usuario_Id

               ,ProdServ_Id

        FROM `receta` WHERE `ProdServ_Id` = pProdServ_Id AND nRecEliminado = 0;

   END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `RPT_Alm_StockConsolidado` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `RPT_Alm_StockConsolidado`(IN `pStockCero` INT)
BEGIN
		IF pStockCero=1 THEN

				Select 
				P.sProSrvCodigo  as Codigo ,
				F.sFamDescripcion as Familia ,
				P.sProSrvNombre as ProductoServicio ,
				SUM(PL.nProLStockReal) as Stock,	   
				U.sUndAlias as Unidad,
				ROUND(CASE SUM(PL.nProLStockReal)
					WHEN 0 THEN 0
					ELSE (SUM(PL.nProLPrecioC * PL.nProLStockReal) / SUM(PL.nProLStockReal))
				END,3) as PrecioProm,
				ROUND(CASE SUM(PL.nProLStockReal)
					WHEN 0 THEN 0
					ELSE (SUM(PL.nProLStockReal) *(SUM(PL.nProLPrecioC * PL.nProLStockReal) / SUM(PL.nProLStockReal)) )
				END,3) as ValorizadoProm
				FROM productolote PL
				INNER JOIN prodserv P ON (P.ProdServ_Id  = PL.ProdServ_Id )
				INNER JOIN almacen  A ON (A.Almacen_Id = PL.Almacen_Id)
				INNER join familia F On (F.Familia_Id = P.nProSrvFamilia_Id)
				LEFT JOIN unidadmedida  U ON (U.UnidadMedida_Id = P.nProSrvUnidad_Id )
				GROUP BY
				P.sProSrvNombre,P.sProSrvCodigo, U.sUndAlias			
				ORDER BY P.sProSrvNombre;
		ELSE

				Select 
				P.sProSrvCodigo  as Codigo ,   
				F.sFamDescripcion as Familia ,
				P.sProSrvNombre as ProductoServicio ,
				SUM(PL.nProLStockReal) as Stock,	   
				U.sUndAlias as Unidad,
				ROUND(CASE SUM(PL.nProLStockReal)
					WHEN 0 THEN 0
					ELSE (SUM(PL.nProLPrecioC * PL.nProLStockReal) / SUM(PL.nProLStockReal))
				END,3) as PrecioProm,
				ROUND(CASE SUM(PL.nProLStockReal)
					WHEN 0 THEN 0
					ELSE (SUM(PL.nProLStockReal) *(SUM(PL.nProLPrecioC * PL.nProLStockReal) / SUM(PL.nProLStockReal)) )
				END,3) as ValorizadoProm
				FROM productolote PL
				INNER JOIN prodserv P ON (P.ProdServ_Id  = PL.ProdServ_Id )
				INNER JOIN almacen  A ON (A.Almacen_Id = PL.Almacen_Id)
				INNER join familia F On (F.Familia_Id = P.nProSrvFamilia_Id)
				LEFT JOIN unidadmedida  U ON (U.UnidadMedida_Id = P.nProSrvUnidad_Id )
				WHERE PL.nProLStockReal <>0
				GROUP BY
				P.sProSrvNombre,P.sProSrvCodigo, U.sUndAlias			
				ORDER BY P.sProSrvNombre;
				
			END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `RPT_Kardex` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `RPT_Kardex`(IN `pProdServ_id` INT(11), IN `INICIO` DATE, IN `FIN` DATE)
BEGIN

		SET FIN = DATE_ADD(FIN,INTERVAL 1 DAY);


Select  
		M.dMovFecha_Act as FechaHora,

		TM.sTMovNombre AS TipoMovimiento,
		A.sAlmNombre AS Almacen ,
		CONCAT(D.sDocNombreCorto,'', M.sMovDocumento) as Documento,
		M.dMovFecha as FechaDoc,
		OD.sODNombreComercial as OrigenDestino,	
		CASE M.nMovEstado 
			WHEN 0 THEN 'ANULADA'
			WHEN 1 THEN 'R'
			WHEN 2 THEN 'P'
			ELSE '-' 			
		end as Estado,
		PL.sProLNombreLote as Lote,
		MD.nMovDetCantidad AS Cant,
		U.sUndAlias as Und,
		nMovDetSaldoAnteriorLote as SAntLote,
		MD.nMovDetSaldoPosteriorLote as SPosLote,
		MD.nMovDetSaldoAnterior as SAnterior,
		MD.nMovDetSaldoPosterior as SPosterior,
		MD.nMovDetCantidad * U.nUndFac_Cnv AS Cant_Id
		FROM movimiento M
		INNER JOIN movimientodetalle MD ON (M.Movimiento_Id= MD.Movimiento_Id)
		INNER JOIN tipomovimiento TM ON (M.nMovTipoMovimiento_Id=TM.TipoMovimiento_Id)
		LEFT JOIN origendestino OD ON (OD.OrigenDestino_Id=M.nMovOrigenDestino_Id)
		INNER JOIN productolote PL ON (PL.ProductoLote_Id=MD.ProductoLote_Id)
		INNER JOIN almacen A ON (A.Almacen_Id = PL.Almacen_Id)
		LEFT JOIN documento D ON (D.Documento_Id=M.Documento_Id)
		INNER JOIN unidadmedida U ON (U.UnidadMedida_Id=MD.nMovDetUnidad_Id)
		WHERE PL.ProdServ_Id = pProdServ_id
		AND M.dMovFecha_Act >= INICIO 
		and M.dMovFecha_Act < FIN
		order by M.dMovFecha_Act ASC;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `RPT_VentaDetallada` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `RPT_VentaDetallada`(IN `INICIO` DATE, IN `FIN` DATE)
BEGIN
		DECLARE factorIGV DOUBLE;

		SET factorIGV=1+((SELECT nConValor FROM conceptofijo
		WHERE Concepto_Id=1)/100);
		
		SET FIN = DATE_ADD(FIN,INTERVAL 1 DAY);
				
	Select 	
	M.dMovFecha as Fecha,
	A.sAlmNombre AS Almacén ,
	CONCAT(D.sDocNombreCorto,' ',M.sMovDocumento) as Documento,
	CASE M.nMovTipoPago
		WHEN 1 THEN 'CONTADO'
		WHEN 2 THEN 'CREDITO'
	END as Condición,
	OD.sODNombreComercial as Cliente,
	PRO.sProSrvNombre AS Producto,
	MD.nMovDetCantidad as Cantidad,
	U.sUndAlias AS Unidad,
	PL.nProLPrecioC * U.nUndFac_Cnv as PCostoUpuntoSIGV,
	CASE D.nDocAfectoIGV 
		WHEN 1 THEN MD.nMovDetPrecioUnitario*M.nMovTipoCambio
		ELSE CASE PRO.nProSrvExoneradoIGV
			 WHEN 0 THEN MD.nMovDetPrecioUnitario *M.nMovTipoCambio/factorIGV	
			 WHEN 1 THEN MD.nMovDetPrecioUnitario*M.nMovTipoCambio	
		END
	END  as PVentaUpuntoSIGV,
	Mo.sMonDescripcion as Moneda,
	Mo.Moneda_Id,
	CASE PRO.nProSrvExoneradoIGV
		 WHEN 0 THEN
			CASE D.nDocAfectoIGV 
				WHEN 1 THEN MD.nMovDetImporte*factorIGV
				ELSE MD.nMovDetImporte
			END
		WHEN 1 THEN 
			CASE D.nDocAfectoIGV 
				WHEN 1 THEN MD.nMovDetImporte
				ELSE MD.nMovDetImporte
			END													
	END AS ImporteVenta,
	CASE PRO.nProSrvExoneradoIGV
		 WHEN 0 THEN
			CASE D.nDocAfectoIGV 
				WHEN 1 THEN MD.nMovDetImporte*factorIGV*M.nMovTipoCambio
				ELSE MD.nMovDetImporte*M.nMovTipoCambio
			END
		WHEN 1 THEN 
			CASE D.nDocAfectoIGV 
				WHEN 1 THEN MD.nMovDetImporte*M.nMovTipoCambio
				ELSE MD.nMovDetImporte*M.nMovTipoCambio
			END													
	END AS ImporteVenta,
	CASE D.nDocAfectoIGV 
		WHEN 1 THEN MD.nMovDetCantidad *((MD.nMovDetPrecioUnitario*M.nMovTipoCambio) - (PL.nProLPrecioC * U.nUndFac_Cnv ))
		ELSE CASE PRO.nProSrvExoneradoIGV
			 WHEN 0 THEN MD.nMovDetCantidad *(((MD.nMovDetPrecioUnitario*M.nMovTipoCambio) /factorIGV) - (PL.nProLPrecioC * U.nUndFac_Cnv ) )	
			 WHEN 1 THEN MD.nMovDetCantidad *((MD.nMovDetPrecioUnitario*M.nMovTipoCambio) - (PL.nProLPrecioC * U.nUndFac_Cnv ))	
			 END
	END  as Utilidad,						
	CASE PL.nProLPrecioC  		WHEN 0 THEN 100
		ELSE 

			((CASE D.nDocAfectoIGV 
			WHEN 1 THEN MD.nMovDetPrecioUnitario*M.nMovTipoCambio
			ELSE CASE PRO.nProSrvExoneradoIGV
				 WHEN 0 THEN MD.nMovDetPrecioUnitario*M.nMovTipoCambio /factorIGV	
				 WHEN 1 THEN MD.nMovDetPrecioUnitario*M.nMovTipoCambio	
				 END
			END) / 	(PL.nProLPrecioC * U.nUndFac_Cnv )-1)*100								
			
	END as PorcRentabilidad,
	M.nMovTipoMovimiento_Id as TipoMov
	FROM movimientodetalle MD
	INNER JOIN movimiento M ON (M.Movimiento_Id = MD.Movimiento_Id)
	INNER JOIN origendestino OD ON (OD.OrigenDestino_Id=M.nMovOrigenDestino_Id)
	INNER JOIN documento D ON (M.Documento_Id=D.Documento_Id)
	INNER JOIN productolote PL ON (PL.ProductoLote_Id = MD.ProductoLote_Id)
	INNER JOIN prodserv PRO ON (PRO.ProdServ_Id = PL.ProdServ_Id)
	INNER JOIN almacen A ON (A.Almacen_Id =PL.Almacen_Id)
	INNER JOIN unidadmedida U ON (U.UnidadMedida_Id = MD.nMovDetUnidad_Id)
	INNER JOIN moneda Mo ON (Mo.moneda_Id = M.Moneda_Id)
	WHERE M.dMovFecha >= INICIO
	and M.dMovFecha < FIN
	AND M.nMovEstado<>0
	AND M.Documento_Id not in (0)
	AND M.nMovTipoMovimiento_Id=2
	order by M.dMovFecha_Act ASC;
					
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `tip_listar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `tip_listar`(IN `pTipoMovimiento_Id` INT(11))
BEGIN
   IF pTipoMovimiento_Id = 0 THEN
        SELECT TipoMovimiento_Id
               ,sTMovNombre
               ,nTMovTipoOD
               ,nTMovSentido
               ,nTMovVisible
        FROM `tipomovimiento` WHERE nTMovVisible = 1;
   ELSE
        SELECT TipoMovimiento_Id
               ,sTMovNombre
               ,nTMovTipoOD
               ,nTMovSentido
               ,nTMovVisible
        FROM `tipomovimiento` WHERE `TipoMovimiento_Id` = pTipoMovimiento_Id AND nTMovVisible = 1;
   END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `uni_crear` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `uni_crear`(IN `psUndDescripcion` VARCHAR(60), IN `psUndAlias` VARCHAR(20), IN `pnUndPadre_Id` INT(11), IN `pnUndFac_Cnv` DOUBLE, IN `pnUndEstado` TINYINT(4), IN `pnUndEliminado` INT(11), IN `pUsuario_Id` INT(11))
BEGIN
    INSERT INTO `unidadmedida`(
                              `sUndDescripcion`,
                              `sUndAlias`,
                              `nUndPadre_Id`,
                              `nUndFac_Cnv`,
                              `nUndEstado`,
                              `nUndEliminado`,
                              `dUndFecha_Act`,
                              `Usuario_Id`
                              ) 
                      VALUES (
                              psUndDescripcion,
                              psUndAlias,
                              pnUndPadre_Id,
                              pnUndFac_Cnv,
                              pnUndEstado,
                              pnUndEliminado,
                              now(),
                              pUsuario_Id
                              );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `uni_editar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `uni_editar`(IN `psUndDescripcion` VARCHAR(60), IN `psUndAlias` VARCHAR(20), IN `pnUndPadre_Id` INT(11), IN `pnUndFac_Cnv` DOUBLE, IN `pnUndEstado` TINYINT(4), IN `pnUndEliminado` INT(11), IN `pUsuario_Id` INT(11), IN `pUnidadMedida_Id` INT(11))
BEGIN
    UPDATE `unidadmedida` SET 
                        `sUndDescripcion` = psUndDescripcion,
                        `sUndAlias` = psUndAlias,
                        `nUndPadre_Id` = pnUndPadre_Id,
                        `nUndFac_Cnv` = pnUndFac_Cnv,
                        `nUndEstado` = pnUndEstado,
                        `nUndEliminado` = pnUndEliminado,
                        `dUndFecha_Act` = now(),
                        `Usuario_Id` = pUsuario_Id
                  WHERE `UnidadMedida_Id`=pUnidadMedida_Id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `uni_eliminar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `uni_eliminar`(IN `pUnidadMedida_Id` INT)
    NO SQL
UPDATE unidadmedida SET nUndEliminado = 1 WHERE UnidadMedida_Id = pUnidadMedida_Id ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `uni_hijos` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `uni_hijos`(IN `pUnidadmedida_Id` INT)
    NO SQL
SELECT UnidadMedida_Id
               ,sUndDescripcion
               ,sUndAlias
               ,nUndPadre_Id
               ,nUndFac_Cnv
               ,nUndEstado
               ,nUndEliminado
               ,dUndFecha_Act
               ,Usuario_Id
        FROM `unidadmedida` WHERE nUndEliminado = 0 AND nUndPadre_Id = pUnidadmedida_Id ORDER BY sUndDescripcion ASC ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `uni_listar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `uni_listar`(IN `pUnidadmedida_Id` INT(11))
BEGIN
   IF pUnidadmedida_Id = 0 THEN
        SELECT UnidadMedida_Id
               ,sUndDescripcion
               ,sUndAlias
               ,nUndPadre_Id
               ,nUndFac_Cnv
               ,nUndEstado
               ,nUndEliminado
               ,dUndFecha_Act
               ,Usuario_Id
        FROM `unidadmedida` WHERE nUndEliminado = 0 ORDER BY sUndDescripcion ASC;
   ELSE
        SELECT UnidadMedida_Id
               ,sUndDescripcion
               ,sUndAlias
               ,nUndPadre_Id
               ,nUndFac_Cnv
               ,nUndEstado
               ,nUndEliminado
               ,dUndFecha_Act
               ,Usuario_Id
        FROM `unidadmedida` WHERE `UnidadMedida_Id` = pUnidadMedida_Id AND nUndEliminado = 0 ORDER BY sUndDescripcion ASC;
   END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `uni_padre` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `uni_padre`(IN `pUnidadmedida_Id` INT, IN `pnUndPadre_Id` INT)
    NO SQL
UPDATE unidadmedida SET nUndPadre_Id = pnUndPadre_Id WHERE nUndPadre_Id = pUnidadmedida_Id ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `uni_unipro` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `uni_unipro`(IN `pUnidadMedida_Id` INT(11))
    NO SQL
SELECT UnidadMedida_Id
               ,sUndDescripcion
               ,sUndAlias
               ,nUndPadre_Id
               ,nUndFac_Cnv
               ,nUndEstado
               ,nUndEliminado
               ,dUndFecha_Act
               ,Usuario_Id
        FROM `unidadmedida` WHERE nUndPadre_Id = pUnidadMedida_Id OR UnidadMedida_Id = pUnidadMedida_Id AND nUndEliminado = 0 ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `usu_autentica` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `usu_autentica`(IN `pusuario` VARCHAR(45), IN `pclave` VARCHAR(45))
BEGIN
	select
		Usuario_Id,
		sUsuNombre,
		sUsuLogin,
		sUsuContrasena,
		nUsuTipo,
		nUsuEstado
	from
		usuario
	where
		sUsuLogin=pusuario
		AND nUsuEliminado=0
		and
		sUsuContrasena=pclave;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `usu_crear` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `usu_crear`(IN `psUsuNombre` VARCHAR(45), IN `psUsuLogin` VARCHAR(45), IN `psUsuContrasena` VARCHAR(45), IN `pnUsuTipo` INT(11), IN `pnUsuEstado` TINYINT(4), IN `pnUsuEliminado` TINYINT(4), IN `pnUsuUsuarioOwn_Id` INT(11))
BEGIN
   INSERT INTO `usuario`(
		`sUsuNombre`, 
		`sUsuLogin`, 
		`sUsuContrasena`, 
		`nUsuTipo`, 
		`nUsuEstado`, 
		`nUsuEliminado`, 
		`dUsuFecha_Act`, 
		`nUsuUsuarioOwn_Id`
       ) VALUES (
       psUsuNombre
       ,psUsuLogin
       ,psUsuContrasena
       ,pnUsuTipo
       ,pnUsuEstado
       ,pnUsuEliminado
       , now() 
       ,pnUsuUsuarioOwn_Id
       );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `usu_editar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `usu_editar`(IN `psUsuNombre` VARCHAR(45), IN `psUsuLogin` VARCHAR(45), IN `psUsuContrasena` VARCHAR(45), IN `pnUsuTipo` INT(11), IN `pnUsuEstado` TINYINT(4), IN `pnUsuEliminado` TINYINT(4), IN `pnUsuUsuarioOwn_Id` INT(11), IN `pUsuario_Id` INT(11))
    NO SQL
UPDATE `usuario` SET `sUsuNombre`=psUsuNombre,
					 `sUsuLogin`=psUsuLogin,
					 `sUsuContrasena`=psUsuContrasena,
					 `nUsuTipo`=pnUsuTipo,
					 `nUsuEstado`=pnUsuEstado,
					 `nUsuEliminado`=pnUsuEliminado,
					 `dUsuFecha_Act`=now(),
					 `nUsuUsuarioOwn_Id`=pnUsuUsuarioOwn_Id
			   WHERE `Usuario_Id`=pUsuario_Id ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `usu_eliminar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `usu_eliminar`(IN `pUsuario_Id` INT)
    NO SQL
UPDATE `usuario` SET `nUsuEliminado` = 1 WHERE `Usuario_Id`=pUsuario_Id ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `usu_listar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `usu_listar`(IN `pUsuario_Id` INT)
    NO SQL
BEGIN
	IF pUsuario_Id = 0 THEN
		SELECT `Usuario_Id`, `sUsuNombre`, `sUsuLogin`, `sUsuContrasena`, `nUsuTipo`, `nUsuEstado`, `nUsuEliminado`, `dUsuFecha_Act`, `nUsuUsuarioOwn_Id` FROM usuario WHERE `nUsuEliminado`=0;
	ELSE		
		SELECT `Usuario_Id`, `sUsuNombre`, `sUsuLogin`, `sUsuContrasena`, `nUsuTipo`, `nUsuEstado`, `nUsuEliminado`, `dUsuFecha_Act`, `nUsuUsuarioOwn_Id` FROM usuario WHERE `Usuario_Id` = pUsuario_Id AND `nUsuEliminado`=0;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-02-20  3:54:42
