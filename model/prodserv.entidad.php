<?php

class Prodserv{
    private $ProdServ_Id;
    private $sProSrvCodigo;
    private $sProSrvCodigo1;
    private $sProSrvCodigo2;
    private $sProSrvNombre;
    private $sProSrvCodigoBarras;
    private $nProSrvFamilia_Id;
    private $sFamDescripcion;
    private $Activo_Id;
    private $sActDescripcion;
    private $nProSrvUnidad_Id;
    private $sUndDescripcion;
    private $nProSrvExoneradoIGV;
    private $nProSrvEstado;
    private $nProSrvEliminado;
    private $dProSrvFecha_Act;
    private $Usuario_Id;
    private $ProSrvEspecificacion;
    private $ProSrvImagen;
    private $ProSrvNomImagen;
    
    public function __GET($a){
        return $this->$a;
    }
    public function __SET($a, $b) {
        return $this->$a = $b;
    }
}
