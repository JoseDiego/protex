<?php

class Familia{
    private $Familia_Id;
    private $sFamCodigo;
    private $sFamDescripcion;
    private $nFamPadre;
    private $nFamTipo;
    private $nFamEstado;
    private $nFamEliminado;
    private $dFamFecha_Act;
    private $Usuario_Id;

    public function __GET($a){
        return $this->$a;
    }
    public function __SET($a, $b) {
        return $this->$a = $b;
    }
}