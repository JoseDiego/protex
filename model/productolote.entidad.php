<?php

class Productolote{
    private $ProductoLote_Id;
    private $Almacén;
    private $Lote;
    private $Vencimiento;
    private $Stock;
    private $Unidad;
    private $TipoPrecio;
    private $PrecioListaIGV;
    private $PrecioCostoIGV;
    private $Precio1;
    private $Precio2;
    private $Precio3;
    private $Porc1;
    private $Porc2;
    private $Porc3;
    private $Almacen_Id;
    private $Descuento;
    private $nProLMonedaCompra_Id;
    private $PrecioCosto;
    public function __GET($a){
        return $this->$a;
    }
    public function __SET($a, $b) {
        return $this->$a = $b;
    }
}