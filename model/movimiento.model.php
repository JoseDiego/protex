<?php
class MovimientoModel {
    private $pdo;
    function __CONSTRUCT(){
        try {
            require_once 'model/database.php';
            $this->pdo = DataBase::ObtenerConexion();
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function ListarDate($fechaInicio, $fechaFin){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL mov_date('".date('Y-m-d', strtotime($fechaInicio))."','".date('Y-m-d', strtotime($fechaFin))."')");
            $stm->execute();
            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $mov = new Movimiento();
                $mov->__SET('Movimiento_Id', $r->Movimiento_Id);
                $mov->__SET('dMovFecha', date('d-m-Y', strtotime($r->dMovFecha)));
                $mov->__SET('nMovTipoMovimiento_Id', $r->nMovTipoMovimiento_Id);
                $mov->__SET('sTMovNombre', $r->sTMovNombre);
                $mov->__SET('nMovTipopago', $r->nMovTipopago);
                $mov->__SET('Almacen_Id', $r->Almacen_Id);
                $mov->__SET('nMovTipoOrigenDestino', $r->nMovTipoOrigenDestino);
                $mov->__SET('nMovOrigenDestino_Id', $r->nMovOrigenDestino_Id);
                $mov->__SET('sAlmNombre', $r->sAlmNombre);
                $mov->__SET('nMovTipodestino', $r->nMovTipodestino);
                $mov->__SET('Documento_Id', $r->Documento_Id);
                $mov->__SET('sDocNombre', $r->sDocNombre);
                $mov->__SET('sDocNombreCorto', $r->sDocNombreCorto);
                $mov->__SET('sMovDocumento', $r->sMovDocumento);
                $mov->__SET('sMovDocReferencia', $r->sMovDocReferencia);
                $mov->__SET('Moneda_Id', $r->Moneda_Id);
                $mov->__SET('nMovTipoCambio', $r->nMovTipoCambio);
                $mov->__SET('nMovImporte', $r->nMovImporte);
                $mov->__SET('nMovIGV', $r->nMovIGV);
                $mov->__SET('nMovTotal', $r->nMovTotal);
                $mov->__SET('nMovSaldo', $r->nMovSaldo);
                $mov->__SET('nMovTotalCancelado', $r->nMovTotalCancelado);
                $mov->__SET('dMovDetraccion', $r->dMovDetraccion);
                $mov->__SET('dMovPercepcion', $r->dMovPercepcion);
                $mov->__SET('dMovRetencion', $r->dMovRetencion);
                $mov->__SET('nMovEstado', $r->nMovEstado);
                $mov->__SET('nMovEliminado', $r->nMovEliminado);
                $mov->__SET('dMovFecha_Act', $r->dMovFecha_Act);
                $mov->__SET('Usuario_Id', $r->Usuario_Id);
                $mov->__SET('Documento', $r->Documento);
                $result[] = $mov;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function Listar(){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL mov_listar(0)");
            $stm->execute();
            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $mov = new Movimiento();
                $mov->__SET('Movimiento_Id', $r->Movimiento_Id);
                $mov->__SET('dMovFecha', date('d-m-Y', strtotime($r->dMovFecha)));
                $mov->__SET('nMovTipoMovimiento_Id', $r->nMovTipoMovimiento_Id);
                $mov->__SET('sTMovNombre', $r->sTMovNombre);
                $mov->__SET('nMovTipopago', $r->nMovTipopago);
                $mov->__SET('Almacen_Id', $r->Almacen_Id);
                $mov->__SET('nMovTipoOrigenDestino', $r->nMovTipoOrigenDestino);
                $mov->__SET('nMovOrigenDestino_Id', $r->nMovOrigenDestino_Id);
                $mov->__SET('sAlmNombre', $r->sAlmNombre);
                $mov->__SET('nMovTipodestino', $r->nMovTipodestino);
                $mov->__SET('Documento_Id', $r->Documento_Id);
                $mov->__SET('sDocNombre', $r->sDocNombre);
                $mov->__SET('sDocNombreCorto', $r->sDocNombreCorto);
                $mov->__SET('sMovDocumento', $r->sMovDocumento);
                $mov->__SET('sMovDocReferencia', $r->sMovDocReferencia);
                $mov->__SET('Moneda_Id', $r->Moneda_Id);
                $mov->__SET('nMovTipoCambio', $r->nMovTipoCambio);
                $mov->__SET('nMovImporte', $r->nMovImporte);
                $mov->__SET('nMovIGV', $r->nMovIGV);
                $mov->__SET('nMovTotal', $r->nMovTotal);
                $mov->__SET('nMovSaldo', $r->nMovSaldo);
                $mov->__SET('nMovTotalCancelado', $r->nMovTotalCancelado);
                $mov->__SET('dMovDetraccion', $r->dMovDetraccion);
                $mov->__SET('dMovPercepcion', $r->dMovPercepcion);
                $mov->__SET('dMovRetencion', $r->dMovRetencion);
                $mov->__SET('nMovEstado', $r->nMovEstado);
                $mov->__SET('nMovEliminado', $r->nMovEliminado);
                $mov->__SET('dMovFecha_Act', $r->dMovFecha_Act);
                $mov->__SET('Usuario_Id', $r->Usuario_Id);
                $mov->__SET('Documento', $r->Documento);
                $result[] = $mov;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function Buscar($Movimiento_Id){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL mov_listar($Movimiento_Id)");
            $stm->execute();
            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $mov = new Movimiento();
                $mov->__SET('Movimiento_Id', $r->Movimiento_Id);
                $mov->__SET('dMovFecha', date('d-m-Y', strtotime($r->dMovFecha)));
                $mov->__SET('nMovTipoMovimiento_Id', $r->nMovTipoMovimiento_Id);
                $mov->__SET('sTMovNombre', $r->sTMovNombre);
                $mov->__SET('nMovTipopago', $r->nMovTipopago);
                $mov->__SET('Almacen_Id', $r->Almacen_Id);
                $mov->__SET('nMovTipoOrigenDestino', $r->nMovTipoOrigenDestino);
                $mov->__SET('nMovOrigenDestino_Id', $r->nMovOrigenDestino_Id);
                $mov->__SET('nMovTipodestino', $r->nMovTipodestino);
                $mov->__SET('Documento_Id', $r->Documento_Id);
                $mov->__SET('sMovDocumento', $r->sMovDocumento);
                $mov->__SET('sDocNombreCorto', $r->sDocNombreCorto);
                $mov->__SET('sMovDocReferencia', $r->sMovDocReferencia);
                $mov->__SET('Moneda_Id', $r->Moneda_Id);
                $mov->__SET('nMovTipoCambio', $r->nMovTipoCambio);
                $mov->__SET('nMovImporte', $r->nMovImporte);
                $mov->__SET('nMovIGV', $r->nMovIGV);
                $mov->__SET('nMovTotal', $r->nMovTotal);
                $mov->__SET('nMovSaldo', $r->nMovSaldo);
                $mov->__SET('nMovTotalCancelado', $r->nMovTotalCancelado);
                $mov->__SET('dMovDetraccion', $r->dMovDetraccion);
                $mov->__SET('dMovPercepcion', $r->dMovPercepcion);
                $mov->__SET('dMovRetencion', $r->dMovRetencion);
                $mov->__SET('sMovObservacion', $r->sMovObservacion);
                $mov->__SET('nMovEstado', $r->nMovEstado);
                $mov->__SET('nMovEliminado', $r->nMovEliminado);
                $mov->__SET('dMovFecha_Act', $r->dMovFecha_Act);
                $mov->__SET('Usuario_Id', $r->Usuario_Id);
                $mov->__SET('Documento', $r->Documento);
                $result[] = $mov;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function Guardar(Movimiento $data){
        try {
            $Movimiento_Id = 0;
            $stm = $this->pdo->prepare("CALL mov_crear(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,@Movimiento_Id)");
            $stm->execute(array(
                                 date('Y-m-d', strtotime($data->__get('dMovFecha'))),
                                 $data->__get('nMovTipoMovimiento_Id'),
                                 $data->__get('nMovTipopago'),
                                 $data->__get('Almacen_Id'),
                                 $data->__get('nMovTipoOrigenDestino'),
                                 $data->__get('nMovOrigenDestino_Id'),
                                 $data->__get('nMovTipodestino'),
                                 $data->__get('Documento_Id'),
                                 $data->__get('sMovDocumento'),
                                 $data->__get('sMovDocReferencia'),
                                 $data->__get('Moneda_Id'),
                                 $data->__get('nMovTipoCambio'),
                                 $data->__get('nMovImporte'),
                                 $data->__get('nMovIGV'),
                                 $data->__get('nMovTotal'),
                                 $data->__get('nMovSaldo'),
                                 $data->__get('nMovTotalCancelado'),
                                 $data->__get('dMovDetraccion'),
                                 $data->__get('dMovPercepcion'),
                                 $data->__get('dMovRetencion'),
                                 $data->__get('sMovObservacion'),
                                 $data->__get('nMovEstado'),
                                 $data->__get('nMovEliminado'),
                                 $data->__get('Usuario_Id')
                                 ));
            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $Movimiento_Id = $r->Movimiento_Id;
            }
//            echo "Se guardo correctamente ->".$nMovTipoMovimiento_Id;
        return $Movimiento_Id;
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    public function Editar(Movimiento $data){
        try {
            $stm = $this->pdo->prepare("CALL mov_editar(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            $stm->execute(array(
                                 date('Y-m-d', strtotime($data->__get('dMovFecha'))),
                                 $data->__get('nMovTipoMovimiento_Id'),
                                 $data->__get('nMovTipopago'),
                                 $data->__get('Almacen_Id'),
                                 $data->__get('nMovTipoOrigenDestino'),
                                 $data->__get('nMovOrigenDestino_Id'),
                                 $data->__get('nMovTipodestino'),
                                 $data->__get('Documento_Id'),
                                 $data->__get('sMovDocumento'),
                                 $data->__get('sMovDocReferencia'),
                                 $data->__get('Moneda_Id'),
                                 $data->__get('nMovTipoCambio'),
                                 $data->__get('nMovImporte'),
                                 $data->__get('nMovIGV'),
                                 $data->__get('nMovTotal'),
                                 $data->__get('nMovSaldo'),
                                 $data->__get('nMovTotalCancelado'),
                                 $data->__get('dMovDetraccion'),
                                 $data->__get('dMovPercepcion'),
                                 $data->__get('dMovRetencion'),
                                 $data->__get('sMovObservacion'),
                                 $data->__get('nMovEstado'),
                                 $data->__get('nMovEliminado'),
                                 $data->__get('Usuario_Id'),
                                 $data->__get('Movimiento_Id')
                                 ));
            echo '<div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Exito información editado</strong>, los datos fueron editados correctamente.
                  </div>'; 
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    public function Eliminar(Movimiento $data){
        try {
             $stm = $this->pdo->prepare("CALL Mov_Anular(?,?)");
             $stm->execute(array($data->__get('Movimiento_Id'),$data->__get('Usuario_Id')));
        } catch (Exception $e) {
             echo die($e->getMessage());
        }
   }
}
?>
