<?php 

class ReportesModel {
    private $pdo;
    public function __CONSTRUCT(){
        try {
            require_once 'model/database.php';
            $this->pdo = DataBase::ObtenerConexion();
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function Stock(){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL RPT_Alm_StockConsolidado(0)");
            $stm->execute();

            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $usu = new Usuario();
                $usu->__SET('valor01', $r->Codigo);
                $usu->__SET('valor02', $r->Familia);
                $usu->__SET('valor03', $r->ProductoServicio);
                $usu->__SET('valor04', $r->Stock);
                $usu->__SET('valor05', $r->Unidad);
                $usu->__SET('valor06', $r->PrecioProm);
                $usu->__SET('valor07', $r->ValorizadoProm);
                $result[] = $usu;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function Kardex($ProdServ_Id,$fechainicio,$fechafin){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL RPT_Kardex($ProdServ_Id,'".date('Y-m-d', strtotime($fechainicio))."','".date('Y-m-d', strtotime($fechafin))."')");
            $stm->execute();

            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $usu = new Usuario();
                $usu->__SET('valor01', $r->FechaHora);
                $usu->__SET('valor02', $r->TipoMovimiento);
                $usu->__SET('valor03', $r->Almacen);
                $usu->__SET('valor04', $r->Documento);
                $usu->__SET('valor05', $r->FechaDoc);
                $usu->__SET('valor06', $r->OrigenDestino);
                $usu->__SET('valor07', $r->Estado);
                $usu->__SET('valor08', $r->Lote);
                $usu->__SET('valor09', $r->Cant);
                $usu->__SET('valor10', $r->Und);
                $usu->__SET('valor11', $r->SAntLote);
                $usu->__SET('valor12', $r->SPosLote);
                $usu->__SET('valor13', $r->SAnterior);
                $usu->__SET('valor14', $r->SPosterior);
                $usu->__SET('valor15', $r->Cant_Id);
                $result[] = $usu;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function VentaDetallada($fechainicio,$fechafin){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL RPT_VentaDetallada('".date('Y-m-d', strtotime($fechainicio))."','".date('Y-m-d', strtotime($fechafin))."')");
            $stm->execute();
            
            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $usu = new Usuario();
                $usu->__SET('valor01', $r->Fecha);
                $usu->__SET('valor02', $r->Almacén);
                $usu->__SET('valor03', $r->Documento);
                $usu->__SET('valor04', $r->Condición);
                $usu->__SET('valor05', $r->Cliente);
                $usu->__SET('valor06', $r->Producto);
                $usu->__SET('valor07', $r->Cantidad);
                $usu->__SET('valor08', $r->Unidad);
                $usu->__SET('valor09', $r->PCostoUpuntoSIGV);
                $usu->__SET('valor10', $r->PVentaUpuntoSIGV);
                $usu->__SET('valor11', $r->Moneda);
                $usu->__SET('valor12', $r->Moneda_Id);
                $usu->__SET('valor13', $r->ImporteVenta);
                $usu->__SET('valor14', $r->Utilidad);
                $usu->__SET('valor15', $r->PorcRentabilidad);
                $usu->__SET('valor16', $r->TipoMov);
                $result[] = $usu;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    
}