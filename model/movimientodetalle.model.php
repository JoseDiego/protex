<?php
class MovimientodetalleModel {
    private $pdo;
    function __CONSTRUCT(){
        try {
            require_once 'model/database.php';
            $this->pdo = DataBase::ObtenerConexion();
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function Buscar($Movimiento_Id){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL movdet_listar($Movimiento_Id)");
            $stm->execute();

            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $mov = new Movimientodetalle();
                $mov->__SET('Movimiento_Id', $r->Movimiento_Id);
                $mov->__SET('ProductoLote_Id', $r->ProductoLote_Id);
                $mov->__SET('nMovDetUnidad_Id', $r->nMovDetUnidad_Id);
                $mov->__SET('nMovDetCantidad', $r->nMovDetCantidad);
                $mov->__SET('nMovDetPrecioLista', $r->nMovDetPrecioLista);
                $mov->__SET('nMovDetPorcDescuento1', $r->nMovDetPorcDescuento1);
                $mov->__SET('nMovDetPorcDecuento2', $r->nMovDetPorcDecuento2);
                $mov->__SET('nMovDetPrecioUnitario', $r->nMovDetPrecioUnitario);
                $mov->__SET('nMovDetImporte', $r->nMovDetImporte);
                $mov->__SET('nMovDetPrecioFlete', $r->nMovDetPrecioFlete);
                $mov->__SET('nMovDetSaldoAnteriorLote', $r->nMovDetSaldoAnteriorLote);
                $mov->__SET('nMovDetSaldoPosteriorLote', $r->nMovDetSaldoPosteriorLote);
                $mov->__SET('nMovDetSaldoAnterior', $r->nMovDetSaldoAnterior);
                $mov->__SET('nMovDetSaldoPosterior', $r->nMovDetSaldoPosterior);
                $mov->__SET('nMovDetIncluyeIGV', $r->nMovDetIncluyeIGV);
                $result[] = $mov;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }

    public function Guardar(Movimientodetalle $data){
        try {
            $stm = $this->pdo->prepare("CALL movdet_crear(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            $stm->execute(array(
                                 $data->__get('Movimiento_Id'),
                                 $data->__get('TipoMovimiento_Id'),
                                 $data->__get('TipoDestino'),
                                 $data->__get('ProdServ_Id'),
                                 $data->__get('Almacen_Id'),
                                 $data->__get('Lote'),
                                 date('Y-m-d', strtotime($data->__get('FechaVencimiento'))),
                                 $data->__get('Cantidad'),
                                 $data->__get('Unidad_Id'),
                                 $data->__get('MonedaCompra_Id'),
                                 $data->__get('PrecioLista'),
                                 $data->__get('Descuento'),
                                 $data->__get('Precio'),
                                 $data->__get('SubTotal'),
                                 $data->__get('Usuario_Id'),
                                 $data->__get('TipoCambio'),
                                 $data->__get('nMovDetIncluyeIgv')
                                 ));
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
}
?>
