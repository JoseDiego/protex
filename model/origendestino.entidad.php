<?php
class Origendestino{
    private $OrigenDestino_Id;
    private $nODTipo;
    private $sODRucDni;
    private $sODNombre;
    private $sODNombreComercial;
    private $sODDireccion;
    private $sODTelefono;
    private $sODCorreo;
    private $sODContacto;
    private $nODDiasCredito;
    private $nODEstado;
    private $dODFechaActualizacion;
    private $nODUsuario_Id;
    private $nODEliminado;

    public function __GET($a){
        return $this->$a;
    }

    public function __SET($a, $b){
        return $this->$a = $b;
    }
}
?>
