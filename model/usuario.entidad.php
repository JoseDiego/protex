<?php

class Usuario{
    private $Usuario_Id;
    private $sUsuNombre;
    private $sUsuLogin;
    private $sUsuContrasena;
    private $nUsuTipo;
    private $nUsuEstado;
    private $nUsuEliminado;
    private $dUsuFecha_Act;
    private $nUsuUsuarioOwn_Id;

    public function __GET($a){
        return $this->$a;
    }
    public function __SET($a, $b) {
        return $this->$a = $b;
    }
}
