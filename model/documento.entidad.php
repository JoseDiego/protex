<?php

class Documento{
    private $Documento_Id;
    private $Almacen_Id;
    private $sAlmNombre;
    private $nDocTipoMovimiento_Id;
    private $sTMovNombre;
    private $sDocNombre;
    private $sDocNombreCorto;
    private $nDocNomAutomatico;
    private $sDocSiguiente;
    private $nDocAfectoIGV;
    private $nDocEstado;
    private $nDocEliminado;
    private $dDocFecha_Act;
    private $Usuario_Id;

    public function __GET($a){
        return $this->$a;
    }
    public function __SET($a, $b) {
        return $this->$a = $b;
    }
}