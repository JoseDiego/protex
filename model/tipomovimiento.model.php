<?php

class TipomovimientoModel {
    private $pdo;
    public function __CONSTRUCT(){
        try {
            require_once 'model/database.php';
            $this->pdo = DataBase::ObtenerConexion();
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function Listar(){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL tip_listar(0)");
            $stm->execute();

            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $tip = new Tipomovimiento();
                $tip->__SET('TipoMovimiento_Id', $r->TipoMovimiento_Id);
                $tip->__SET('sTMovNombre', $r->sTMovNombre);
                $tip->__SET('nTMovTipoOD', $r->nTMovTipoOD);
                $tip->__SET('nTMovSentido', $r->nTMovSentido);
                $tip->__SET('nTMovVisible', $r->nTMovVisible);
                $result[] = $tip;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }

    public function Buscar($Tipomovimiento_Id){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL tip_listar($Tipomovimiento_Id)");
            $stm->execute();

            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $tip = new Tipomovimiento();
                $tip->__SET('TipoMovimiento_Id', $r->TipoMovimiento_Id);
                $tip->__SET('sTMovNombre', $r->sTMovNombre);
                $tip->__SET('nTMovTipoOD', $r->nTMovTipoOD);
                $tip->__SET('nTMovSentido', $r->nTMovSentido);
                $tip->__SET('nTMovVisible', $r->nTMovVisible);
                $result[] = $tip;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
}