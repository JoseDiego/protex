<?php
class OrigendestinoModel {
    private $pdo;
    function __CONSTRUCT(){
        try {
            require_once 'model/database.php';
            $this->pdo = DataBase::ObtenerConexion();
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function Listar(){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL OD_Listar(0)");
            $stm->execute();
            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $ori = new Origendestino();
                $ori->__SET('OrigenDestino_Id', $r->OrigenDestino_Id);
                $ori->__SET('nODTipo', $r->nODTipo);
                $ori->__SET('sODRucDni', $r->sODRucDni);
                $ori->__SET('sODNombre', $r->sODNombre);
                $ori->__SET('sODNombreComercial', $r->sODNombreComercial);
                $ori->__SET('sODDireccion', $r->sODDireccion);
                $ori->__SET('sODTelefono', $r->sODTelefono);
                $ori->__SET('sODCorreo', $r->sODCorreo);
                $ori->__SET('sODContacto', $r->sODContacto);
                $ori->__SET('nODDiasCredito', $r->nODDiasCredito);
                $ori->__SET('nODEstado', $r->nODEstado);
                $ori->__SET('dODFechaActualizacion', $r->dODFechaActualizacion);
                $ori->__SET('nODUsuario_Id', $r->nODUsuario_Id);
                $ori->__SET('nODEliminado', $r->nODEliminado);
                $result[] = $ori;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    
        public function ListarProveedor(){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL OD_Listar(0)");
            $stm->execute();
            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $ori = new Origendestino();
                if ($r->nODTipo == 1){
                $ori->__SET('OrigenDestino_Id', $r->OrigenDestino_Id);
                $ori->__SET('nODTipo', $r->nODTipo);
                $ori->__SET('sODRucDni', $r->sODRucDni);
                $ori->__SET('sODNombre', $r->sODNombre);
                $ori->__SET('sODNombreComercial', $r->sODNombreComercial);
                $ori->__SET('sODDireccion', $r->sODDireccion);
                $ori->__SET('sODTelefono', $r->sODTelefono);
                $ori->__SET('sODCorreo', $r->sODCorreo);
                $ori->__SET('sODContacto', $r->sODContacto);
                $ori->__SET('nODDiasCredito', $r->nODDiasCredito);
                $ori->__SET('nODEstado', $r->nODEstado);
                $ori->__SET('dODFechaActualizacion', $r->dODFechaActualizacion);
                $ori->__SET('nODUsuario_Id', $r->nODUsuario_Id);
                $ori->__SET('nODEliminado', $r->nODEliminado);
                $result[] = $ori;
            }
            
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function Buscar($OrigenDestino_Id){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL OD_Listar($OrigenDestino_Id)");
            $stm->execute();
            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $ori = new Origendestino();
                $ori->__SET('OrigenDestino_Id', $r->OrigenDestino_Id);
                $ori->__SET('nODTipo', $r->nODTipo);
                $ori->__SET('sODRucDni', $r->sODRucDni);
                $ori->__SET('sODNombre', $r->sODNombre);
                $ori->__SET('sODNombreComercial', $r->sODNombreComercial);
                $ori->__SET('sODDireccion', $r->sODDireccion);
                $ori->__SET('sODTelefono', $r->sODTelefono);
                $ori->__SET('sODCorreo', $r->sODCorreo);
                $ori->__SET('sODContacto', $r->sODContacto);
                $ori->__SET('nODDiasCredito', $r->nODDiasCredito);
                $ori->__SET('nODEstado', $r->nODEstado);
                $ori->__SET('dODFechaActualizacion', $r->dODFechaActualizacion);
                $ori->__SET('nODUsuario_Id', $r->nODUsuario_Id);
                $ori->__SET('nODEliminado', $r->nODEliminado);
                $result[] = $ori;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function Crear(Origendestino $data){
        try {
            $stm = $this->pdo->prepare("CALL OD_Crear(?,?,?,?,?,?,?,?,?,?,?,?)");
            $stm->execute(array(
                                 $data->__get('nODTipo'),
                                 $data->__get('sODRucDni'),
                                 $data->__get('sODNombre'),
                                 $data->__get('sODNombreComercial'),
                                 $data->__get('sODDireccion'),
                                 $data->__get('sODTelefono'),
                                 $data->__get('sODCorreo'),
                                 $data->__get('sODContacto'),
                                 $data->__get('nODDiasCredito'),
                                 $data->__get('nODEstado'),
                                 $data->__get('nODUsuario_Id'),
                                 $data->__get('nODEliminado')
                                 ));
            echo '<div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Exito información guardado</strong>, los datos fueron registrados correctamente.
                  </div>';
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    public function Editar(Origendestino $data){
        try {
            $stm = $this->pdo->prepare("CALL OD_editar(?,?,?,?,?,?,?,?,?,?,?,?,?)");
            $stm->execute(array(                                 
                                 $data->__get('OrigenDestino_Id'),
                                 $data->__get('nODTipo'),
                                 $data->__get('sODRucDni'),
                                 $data->__get('sODNombre'),
                                 $data->__get('sODNombreComercial'),
                                 $data->__get('sODDireccion'),
                                 $data->__get('sODTelefono'),
                                 $data->__get('sODCorreo'),
                                 $data->__get('sODContacto'),
                                 $data->__get('nODDiasCredito'),
                                 $data->__get('nODEstado'),
                                 $data->__get('nODUsuario_Id'),
                                 $data->__get('nODEliminado')
                                 ));
            echo '<div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Exito información editado</strong>, los datos fueron editados correctamente.
                  </div>'; 
            echo '<script type="text/javascript">window.location="?c=Clienteproveedor&a=Index";</script>';
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    public function Eliminar(Origendestino $data){
        try {
             $stm = $this->pdo->prepare("CALL OD_eliminar(?)");
             $stm->execute(array($data->__get('OrigenDestino_Id')));
        } catch (Exception $e) {
             echo die($e->getMessage());
        }
   }
}
?>
