<?php

class ParametroModel {
    private $pdo;
    public function __CONSTRUCT(){
        try {
            require_once 'model/database.php';
            $this->pdo = DataBase::ObtenerConexion();
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    public function ListarParametros(){
        try {
            $result = array();
            $stm = $this->pdo->prepare('CALL `param_listar`(0);');
            $stm->execute();
            
            foreach ($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $param = new Parametro();
                $param->__SET('Parametro_Id', $r->Parametro_Id);                
                $param->__SET('sParDescripcion', $r->sParDescripcion);
                $param->__SET('sParValor', $r->sParValor);
                $result[] = $param;
            }
            return $result;
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    public function guardarParametros($data){
        try {
            for($i=0;$i<count($data);$i++){
                $sql = "CALL param_editar(?, ?)";

                $this->pdo->prepare($sql)
                     ->execute(array($data[$i]["datos"],
                                     $data[$i]["codigo"]
                                    )); 
            }
            echo '<div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Exito!</strong>, Los Datos fueron Registrados Correctamente.
                  </div>';
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
}
