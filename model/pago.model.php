<?php
class PagoModel {
    private $pdo;
    function __CONSTRUCT(){
        try {
            require_once 'model/database.php';
            $this->pdo = DataBase::ObtenerConexion();
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function Listar($Movimiento_Id){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL Pag_listar(0,$Movimiento_Id)");
            $stm->execute();
            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $pag = new Pago();
                $pag->__SET('Pago_Id', $r->Pago_Id);
                $pag->__SET('Movimiento_Id', $r->Movimiento_Id);
                $pag->__SET('dPagFecha', date('d-m-Y', strtotime($r->dPagFecha)));
                $pag->__SET('nPagTipoPago_Id', $r->nPagTipoPago_Id);
                $pag->__SET('sPagDocumento', $r->sPagDocumento);
                $pag->__SET('nPagImporte', $r->nPagImporte);
                $pag->__SET('nPagLetra_Id', $r->nPagLetra_Id);
                $pag->__SET('dPagFecha_Act', $r->dPagFecha_Act);
                $pag->__SET('nPagUsuario_Id', $r->nPagUsuario_Id);
                $result[] = $pag;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function Buscar($Pago_Id){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL pag_listar($Pago_Id)");
            $stm->execute();
            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $pag = new Pago();
                $pag->__SET('Pago_Id', $r->Pago_Id);
                $pag->__SET('Movimiento_Id', $r->Movimiento_Id);
                $pag->__SET('dPagFecha', date('d-m-Y', strtotime($r->dPagFecha)));
                $pag->__SET('nPagTipoPago_Id', $r->nPagTipoPago_Id);
                $pag->__SET('sPagDocumento', $r->sPagDocumento);
                $pag->__SET('nPagImporte', $r->nPagImporte);
                $pag->__SET('nPagLetra_Id', $r->nPagLetra_Id);
                $pag->__SET('dPagFecha_Act', $r->dPagFecha_Act);
                $pag->__SET('nPagUsuario_Id', $r->nPagUsuario_Id);
                $result[] = $pag;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function Crear(Pago $data){
        try {
            $stm = $this->pdo->prepare("CALL Pag_Crear(?,?,?,?,?,?)");
            $stm->execute(array(
                                 $data->__get('Movimiento_Id'),
                                 date('Y-m-d', strtotime($data->__get('dPagFecha'))),
                                 $data->__get('nPagTipoPago_Id'),
                                 $data->__get('sPagDocumento'),
                                 $data->__get('nPagImporte'),
                                 $data->__get('nPagUsuario_Id')
                                 ));
            echo '<div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Exito información guardado</strong>, los datos fueron registrados correctamente.
                  </div>';
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    public function Editar(Pago $data){
        try {
            $stm = $this->pdo->prepare("CALL pag_editar(?,?,?,?,?,?,?,?)");
            $stm->execute(array(
                                 $data->__get('Movimiento_Id'),
                                 $data->__get('dPagFecha'),
                                 $data->__get('nPagTipoPago_Id'),
                                 $data->__get('sPagDocumento'),
                                 $data->__get('nPagImporte'),
                                 $data->__get('nPagLetra_Id'),
                                 $data->__get('nPagUsuario_Id'),
                                 $data->__get('Pago_Id')
                                 ));
            echo '<div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Exito información editado</strong>, los datos fueron editados correctamente.
                  </div>'; 
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    public function Eliminar(Pago $data){
        try {
             $stm = $this->pdo->prepare("CALL Pag_eliminar(?)");
             $stm->execute(array($data->__get('Pago_Id')));
             echo "Se elimino correctamente";
        } catch (Exception $e) {
             echo die($e->getMessage());
        }
   }
}
?>
