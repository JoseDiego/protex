<?php
class OperarioModel {
  private $pdo;
  function __CONSTRUCT(){
    try {
      require_once 'model/database.php';
      $this->pdo = DataBase::ObtenerConexion();
      $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (Exception $e) {
      echo die($e->getMessage());
    }
  }
  public function Listar(){
    try {
      $result = array();
      $stm = $this->pdo->prepare("CALL ope_listar(0)");
      $stm->execute();
      foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
        $rec = new Operario();
        $rec->__SET('Operario_Id', $r->Operario_Id);
        $rec->__SET('sOpeDni', $r->sOpeDni);
        $rec->__SET('sOpeApePat', $r->sOpeApePat);
        $rec->__SET('sOpeApeMat', $r->sOpeApeMat);
        $rec->__SET('sOpeNombres', $r->sOpeNombres);
        $rec->__SET('sOpeCargo', $r->sOpeCargo);
        $rec->__SET('sOpeDireccion', $r->sOpeDireccion);
        $rec->__SET('sOpeTelefono', $r->sOpeTelefono);
        $rec->__SET('sOpeCorreo', $r->sOpeCorreo);
        $rec->__SET('nOpeEstado', $r->nOpeEstado);
        $result[] = $rec;
      }
      return $result;
    } catch (Exception $e) {
      echo die($e->getMessage());
    }
  }
  public function Buscar($Operario_Id){
      try {
        $result = array();
        $stm = $this->pdo->prepare("CALL ope_listar($Operario_Id)");
        $stm->execute();
        foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
          $rec = new Operario();
          $rec->__SET('Operario_Id', $r->Operario_Id);
          $rec->__SET('sOpeDni', $r->sOpeDni);
          $rec->__SET('sOpeApePat', $r->sOpeApePat);
          $rec->__SET('sOpeApeMat', $r->sOpeApeMat);
          $rec->__SET('sOpeNombres', $r->sOpeNombres);
          $rec->__SET('sOpeCargo', $r->sOpeCargo);
          $rec->__SET('sOpeDireccion', $r->sOpeDireccion);
          $rec->__SET('sOpeTelefono', $r->sOpeTelefono);
          $rec->__SET('sOpeCorreo', $r->sOpeCorreo);
          $rec->__SET('nOpeEstado', $r->nOpeEstado);
          $result[] = $rec;
        }
        return $result;
      } catch (Exception $e) {
        echo die($e->getMessage());
      }
  }

  public function Guardar(Operario $data){
      try {
          $stm = $this->pdo->prepare("CALL ope_crear(?,?,?,?,?,?,?,?,?)");
          $stm->execute(array(
                              $data->__get('sOpeDni'),
                              $data->__get('sOpeApePat'),
                              $data->__get('sOpeApeMat'),
                              $data->__get('sOpeNombres'),
                              $data->__get('sOpeCargo'),
                              $data->__get('sOpeDireccion'),
                              $data->__get('sOpeTelefono'),
                              $data->__get('sOpeCorreo'),
                              $data->__get('nOpeEstado')
                       ));
          echo '<div class="alert alert-success" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <strong>Exito información guardado</strong>, los datos fueron registrados correctamente.
                </div>';
      } catch (Exception $e) {
          echo die($e->getMessage());
      }
  }

  public function Editar(Operario $data){
      try {
          $stm = $this->pdo->prepare("CALL ope_editar(?,?,?,?,?,?,?,?,?,?)");
          $stm->execute(array(
                              $data->__get('sOpeDni'),
                              $data->__get('sOpeApePat'),
                              $data->__get('sOpeApeMat'),
                              $data->__get('sOpeNombres'),
                              $data->__get('sOpeCargo'),
                              $data->__get('sOpeDireccion'),
                              $data->__get('sOpeTelefono'),
                              $data->__get('sOpeCorreo'),
                              $data->__get('nOpeEstado'),
                              $data->__get('Operario_Id')
                       ));
          echo '<div class="alert alert-success" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <strong>Exito información guardado</strong>, los datos fueron registrados correctamente.
                </div>';
      } catch (Exception $e) {
          echo die($e->getMessage());
      }
  }
  public function Eliminar(Operario $data){
      try {
          $stm = $this->pdo->prepare("CALL ope_eliminar(?)");
          $stm->execute(array($data->__get('Operario_Id')));
          echo "Se elimino correctamente";
      } catch (Exception $e) {
          echo die($e->getMessage());
      }
  }
}
?>
