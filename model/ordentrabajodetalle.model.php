<?php

class OrdenTrabajoDetalleModel {
    private $pdo;
    function __CONSTRUCT(){
        try {
            require_once 'model/database.php';
            $this->pdo = DataBase::ObtenerConexion();
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    
    public function Guardar(OrdenTrabajoDetalle $data){
        try {
            $stm = $this->pdo->prepare("CALL otrabdet_crear(?,?,?,?,?,?)");
            $stm->execute(array(
                                 $data->__get('OrdenTrabajo_Id'),
                                 $data->__get('ProdServ_Id'),
                                 $data->__get('UnidadMedida_Id'),
                                 $data->__get('CantidadAsig'),
                                 $data->__get('CantidadProg'),
                                 $data->__get('CantidadRest')
                                 ));
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    
        public function Listar($OrdenTrabajo_Id){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL otrabdet_listar($OrdenTrabajo_Id,1)");
            $stm->execute();
            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $ped = new OrdenTrabajoDetalle();
                $ped->__SET('OrdenTrabajo_detalle', $r->OrdenTrabajo_detalle);
                $ped->__SET('OrdenTrabajo_Id', $r->OrdenTrabajo_Id);
                $ped->__SET('ProdServ_Id', $r->ProdServ_Id);
                $ped->__SET('sProvSrvNombre', $r->sProSrvNombre);
                $ped->__SET('UnidadMedida_Id', $r->UnidadMedida_Id);
                $ped->__SET('sUndAlias', $r->sUndAlias);
                $ped->__SET('CantidadAsig', $r->CantidadAsig);
                $ped->__SET('CantidadProg', $r->CantidadProg);
                $ped->__SET('CantidadRest', $r->CantidadRest);
                $result[] = $ped;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    
    public function Editar(OrdenTrabajoDetalle $data){
        try {
            $stm = $this->pdo->prepare("CALL otrabdet_editar(?,?,?,?,?,?)");
            $stm->execute(array(
                                 $data->__get('OrdenTrabajo_Id'),
                                 $data->__get('ProdServ_Id'),
                                 $data->__get('UnidadMedida_Id'),
                                 $data->__get('CantidadAsig'),
                                 $data->__get('CantidadProg'),
                                 $data->__get('CantidadRest')
                                 
                                 ));
            
            
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    
    public function Buscar($OrdenTrabajo_Id,$ProdServ_Id){
        try {
            
            $result = array();
            $stm = $this->pdo->prepare("CALL otrabdet_buscar($OrdenTrabajo_Id,$ProdServ_Id)");
            $stm->execute();
            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $peddet = new OrdenTrabajoDetalle();
                $peddet->__SET('OrdenTrabajo_detalle', $r->OrdenTrabajo_detalle);
                $peddet->__SET('OrdenTrabajo_Id', $r->OrdenTrabajo_Id);
                $peddet->__SET('ProdServ_Id', $r->ProdServ_Id);
                $peddet->__SET('sProvSrvNombre', $r->sProSrvNombre);
                $peddet->__SET('UnidadMedida_Id', $r->UnidadMedida_Id);
                $peddet->__SET('sUndAlias', $r->sUndAlias);
                $peddet->__SET('CantidadAsig', $r->CantidadAsig);
                $peddet->__SET('CantidadProg', $r->CantidadProg);
                $peddet->__SET('CantidadRest', $r->CantidadRest);
                
            }
            return $peddet;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }

}

