<?php

class FamiliaModel {
    private $pdo;
    public function __CONSTRUCT(){
        try {
            require_once 'model/database.php';
            $this->pdo = DataBase::ObtenerConexion();
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function Hijos($idPadre){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL fam_hijos($idPadre)");
            $stm->execute();

            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $fam = new Familia();
                $fam->__SET('Familia_Id', $r->Familia_Id);
                $fam->__SET('sFamCodigo', $r->sFamCodigo);
                $fam->__SET('sFamDescripcion', $r->sFamDescripcion);
                $fam->__SET('nFamPadre', $r->nFamPadre);
                $fam->__SET('nFamTipo', $r->nFamTipo);
                $fam->__SET('nFamEstado', $r->nFamEstado);
                $fam->__SET('nFamEliminado', $r->nFamEliminado);
                $fam->__SET('dFamFecha_Act', $r->dFamFecha_Act);
                $fam->__SET('Usuario_Id', $r->Usuario_Id);
                $result[] = $fam;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    
    public function Padre(Familia $data){
        try {
            $stm = $this->pdo->prepare("CALL fam_padre(?,?)");
            $stm->execute(array($data->__get('Familia_Id'),
                                $data->__get('nFamPadre')));
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }  
    public function Listar(){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL fam_listar(0)");
            $stm->execute();

            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $fam = new Familia();
                $fam->__SET('Familia_Id', $r->Familia_Id);
                $fam->__SET('sFamCodigo', $r->sFamCodigo);
                $fam->__SET('sFamDescripcion', $r->sFamDescripcion);
                $fam->__SET('nFamPadre', $r->nFamPadre);
                $fam->__SET('nFamTipo', $r->nFamTipo);
                $fam->__SET('nFamEstado', $r->nFamEstado);
                $fam->__SET('nFamEliminado', $r->nFamEliminado);
                $fam->__SET('dFamFecha_Act', $r->dFamFecha_Act);
                $fam->__SET('Usuario_Id', $r->Usuario_Id);
                $result[] = $fam;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }    
    
    public function Buscar($Familia_Id){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL fam_listar($Familia_Id)");
            $stm->execute();

            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $fam = new Familia();
                $fam->__SET('Familia_Id', $r->Familia_Id);
                $fam->__SET('sFamCodigo', $r->sFamCodigo);
                $fam->__SET('sFamDescripcion', $r->sFamDescripcion);
                $fam->__SET('nFamPadre', $r->nFamPadre);
                $fam->__SET('nFamTipo', $r->nFamTipo);
                $fam->__SET('nFamEstado', $r->nFamEstado);
                $fam->__SET('nFamEliminado', $r->nFamEliminado);
                $fam->__SET('dFamFecha_Act', $r->dFamFecha_Act);
                $fam->__SET('Usuario_Id', $r->Usuario_Id);
                $result[] = $fam;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }

    public function Guardar(Familia $data){
        try {
            $stm = $this->pdo->prepare("CALL fam_crear(?,?,?,?,?,?,?)");
            $stm->execute(array(
                                $data->__get('sFamCodigo'),
                                $data->__get('sFamDescripcion'),
                                $data->__get('nFamPadre'),
                                $data->__get('nFamTipo'),
                                $data->__get('nFamEstado'),
                                $data->__get('nFamEliminado'),
                                $data->__get('Usuario_Id')
                         ));
            echo '<div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Exito información guardado</strong>, los datos fueron registrados correctamente.
                  </div>';
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }

    public function Editar(Familia $data){
        try {
            $stm = $this->pdo->prepare("CALL fam_editar(?,?,?,?,?,?,?,?)");
            $stm->execute(array(
                                 $data->__get('sFamCodigo'),
                                 $data->__get('sFamDescripcion'),
                                 $data->__get('nFamPadre'),
                                 $data->__get('nFamTipo'),
                                 $data->__get('nFamEstado'),
                                 $data->__get('nFamEliminado'),
                                 $data->__get('Usuario_Id'),
                                 $data->__get('Familia_Id')
                                ));
            echo '<div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Exito información editado</strong>, los datos fueron editados correctamente.
                  </div>';  
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }

    public function Eliminar(Familia $data){
        try {
            $stm = $this->pdo->prepare("CALL fam_eliminar(?)");
            $stm->execute(array($data->__get('Familia_Id')));
            echo "Se elimino correctamente";
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
}
