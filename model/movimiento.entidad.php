<?php
class Movimiento{
    private $Movimiento_Id;
    private $dMovFecha;
    private $nMovTipoMovimiento_Id;
    private $sTMovNombre;
    private $nMovTipopago;
    private $Almacen_Id;
    private $nMovTipoOrigenDestino;
    private $nMovOrigenDestino_Id;
    private $sAlmNombre;
    private $nMovTipodestino;
    private $Documento_Id;
    private $sDocNombre;
    private $sMovDocumento;
    private $sMovDocReferencia;
    private $Moneda_Id;
    private $nMovTipoCambio;
    private $nMovImporte;
    private $nMovIGV;
    private $nMovTotal;
    private $nMovTotalCancelado;
    private $dMovDetraccion;
    private $dMovPercepcion;
    private $dMovRetencion;
    private $sMovObservación;
    private $nMovEstado;
    private $nMovEliminado;
    private $dMovFecha_Act;
    private $Usuario_Id;

    private $Documento;
    public function __GET($a){
        return $this->$a;
    }

    public function __SET($a, $b){
        return $this->$a = $b;
    }
}
?>
