<?php 

class UsuarioModel {
    private $pdo;
    public function __CONSTRUCT(){
        try {
            require_once 'model/database.php';
            $this->pdo = DataBase::ObtenerConexion();
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function Listar(){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL usu_listar(0)");
            $stm->execute();

            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $usu = new Usuario();
                $usu->__SET('Usuario_Id', $r->Usuario_Id);
                $usu->__SET('sUsuNombre', $r->sUsuNombre);
                $usu->__SET('sUsuLogin', $r->sUsuLogin);
                $usu->__SET('sUsuContrasena', $r->sUsuContrasena);
                $usu->__SET('nUsuTipo', $r->nUsuTipo);
                $usu->__SET('nUsuEstado', $r->nUsuEstado);
                $usu->__SET('nUsuEliminado', $r->nUsuEliminado);
                $usu->__SET('dUsuFecha_Act', $r->dUsuFecha_Act);
                $usu->__SET('nUsuUsuarioOwn_Id', $r->nUsuUsuarioOwn_Id);
                $result[] = $usu;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }

    public function Buscar($Usuario_Id){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL usu_listar($Usuario_Id)");
            $stm->execute();

            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $usu = new Usuario();
                $usu->__SET('Usuario_Id', $r->Usuario_Id);
                $usu->__SET('sUsuNombre', $r->sUsuNombre);
                $usu->__SET('sUsuLogin', $r->sUsuLogin);
                $usu->__SET('sUsuContrasena', $r->sUsuContrasena);
                $usu->__SET('nUsuTipo', $r->nUsuTipo);
                $usu->__SET('nUsuEstado', $r->nUsuEstado);
                $usu->__SET('nUsuEliminado', $r->nUsuEliminado);
                $usu->__SET('dUsuFecha_Act', $r->dUsuFecha_Act);
                $usu->__SET('nUsuUsuarioOwn_Id', $r->nUsuUsuarioOwn_Id);
                $result[] = $usu;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function autenticar($Usuario,$Clave){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL usu_autentica('$Usuario','$Clave')");
            $stm->execute();

            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $usu = new Usuario();
                $usu->__SET('Usuario_Id', $r->Usuario_Id);
                $usu->__SET('sUsuNombre', $r->sUsuNombre);
                $usu->__SET('sUsuLogin', $r->sUsuLogin);
                $usu->__SET('sUsuContrasena', $r->sUsuContrasena);
                $usu->__SET('nUsuTipo', $r->nUsuTipo);
                $usu->__SET('nUsuEstado', $r->nUsuEstado);
                $result[] = $usu;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }

    public function Guardar(Usuario $data){
        try {
            $stm = $this->pdo->prepare("CALL usu_crear(?,?,?,?,?,?,?)");
            $stm->execute(array(
                                $data->__get('sUsuNombre'),
                                $data->__get('sUsuLogin'),
                                $data->__get('sUsuContrasena'),
                                $data->__get('nUsuTipo'),
                                $data->__get('nUsuEstado'),
                                $data->__get('nUsuEliminado'),
                                $data->__get('nUsuUsuarioOwn_Id')
                         ));
            echo '<div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Exito información guardado</strong>, los datos fueron registrados correctamente.
                  </div>';
        } catch (Exception $e) {
            echo '<div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Error al guardar información!</strong>,'.$e->getMessage().'.
                  </div>';
        }
    }

    public function Editar(Usuario $data){
        try {
            $stm = $this->pdo->prepare("CALL usu_editar(?,?,?,?,?,?,?,?)");
            $stm->execute(array(
                                 $data->__get('sUsuNombre'),
                                 $data->__get('sUsuLogin'),
                                 $data->__get('sUsuContrasena'),
                                 $data->__get('nUsuTipo'),
                                 $data->__get('nUsuEstado'),
                                 $data->__get('nUsuEliminado'),
                                 $data->__get('nUsuUsuarioOwn_Id'),
                                 $data->__get('Usuario_Id')
                                ));
            echo '<div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Exito información editado</strong>, los datos fueron editados correctamente.
                  </div>';            
            echo '<script type="text/javascript">window.location="?c=Usuario&a=Index";</script>';
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }

    public function Eliminar(Usuario $data){
        try {
            $stm = $this->pdo->prepare("CALL usu_eliminar(?)");
            $stm->execute(array($data->__get('Usuario_Id')));
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
}