<?php
class PedidoModel {
    private $pdo;
    function __CONSTRUCT(){
        try {
            require_once 'model/database.php';
            $this->pdo = DataBase::ObtenerConexion();
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function Listar(){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL ped_listar(0)");
            $stm->execute();
            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $ped = new Pedido();
                $ped->__SET('Pedido_Id', $r->Pedido_Id);
                $ped->__SET('OrigenDestino_Id', $r->OrigenDestino_Id);
                $ped->__SET('dPedFecha', $r->dPedFecha);
                $ped->__SET('nPedTipopago', $r->nPedTipopago);
                $ped->__SET('Documento_Id', $r->Documento_Id);
//                $ped->__SET('sPedDocumento', $r->sPedDocumento);
                $ped->__SET('dPedImporte', $r->dPedImporte);
                $ped->__SET('dPedIGV', $r->dPedIGV);
                $ped->__SET('dPedTotal', $r->dPedTotal);
                $ped->__SET('sPedObservaciones', $r->sPedObservaciones);
                $ped->__SET('nPedEstado', $r->nPedEstado);
                $ped->__SET('dPedEliminado', $r->dPedEliminado);
                $ped->__SET('dPedFecha_Act', $r->dPedFecha_Act);
                $ped->__SET('Usuario_Id', $r->Usuario_Id);
                $ped->__SET('nroOrdenTrabajo', $r->nroOrdenTrabajo);
                $result[] = $ped;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function Buscar($Pedido_Id){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL ped_listar($Pedido_Id)");
            $stm->execute();
            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $ped = new Pedido();
                $ped->__SET('Pedido_Id', $r->Pedido_Id);
                $ped->__SET('OrigenDestino_Id', $r->OrigenDestino_Id);
                $ped->__SET('sODRucDni', $r->sODRucDni);
                $ped->__SET('sODNombre', $r->sODNombre);
                $ped->__SET('dPedFecha', $r->dPedFecha);
                $ped->__SET('nPedTipopago', $r->nPedTipopago);
                $ped->__SET('Documento_Id', $r->Documento_Id);
//                $ped->__SET('sPedDocumento', $r->sPedDocumento);
                $ped->__SET('dPedImporte', $r->dPedImporte);
                $ped->__SET('dPedIGV', $r->dPedIGV);
                $ped->__SET('dPedTotal', $r->dPedTotal);
                $ped->__SET('dPedSaldo', $r-> dPedSaldo);
                $ped->__SET('sPedObservaciones', $r->sPedObservaciones);
                $ped->__SET('nPedEstado', $r->nPedEstado);
                $ped->__SET('dPedEliminado', $r->dPedEliminado);
                $ped->__SET('dPedFecha_Act', $r->dPedFecha_Act);
                $ped->__SET('Usuario_Id', $r->Usuario_Id);
                $result[] = $ped;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function Crear(Pedido $data){
        try {
            $Pedido_Id = 0;
            $stm = $this->pdo->prepare("CALL ped_crear(?,?,?,?,?,?,?,?,?,?,?,@Pedido_Id)");
            $stm->execute(array(
                                 $data->__get('OrigenDestino_Id'),
                                 date('Y-m-d', strtotime($data->__get('dPedFecha'))),
                                 $data->__get('nPedTipopago'),
                                 $data->__get('Documento_Id'),
//                                 $data->__get('sPedDocumento'),
                                 $data->__get('dPedImporte'),
                                 $data->__get('dPedIGV'),
                                 $data->__get('dPedTotal'),
                                 $data->__get('sPedObservaciones'),
                                 $data->__get('nPedEstado'),
                                 $data->__get('dPedEliminado'),
                                 $data->__get('Usuario_Id')
                                 ));            
            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $Pedido_Id = $r->Pedido_Id;
            }
            return $Pedido_Id;
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    public function Editar(Pedido $data){
        try {
            $stm = $this->pdo->prepare("CALL ped_editar(?,?,?,?,?,?,?,?,?,?,?,?)");
            $stm->execute(array(
                                 $data->__get('OrigenDestino_Id'),
                                 date('Y-m-d', strtotime($data->__get('dPedFecha'))),
                                 $data->__get('nPedTipopago'),
                                 $data->__get('Documento_Id'),
//                                 $data->__get('sPedDocumento'),
                                 $data->__get('dPedImporte'),
                                 $data->__get('dPedIGV'),
                                 $data->__get('dPedTotal'),
                                 $data->__get('sPedObservaciones'),
                                 $data->__get('nPedEstado'),
                                 $data->__get('dPedEliminado'),
                                 $data->__get('Usuario_Id'),
                                 $data->__get('Pedido_Id')
                                 ));
            
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    public function Cerrar(Pedido $data){
        try {
             $stm = $this->pdo->prepare("CALL ped_cerrar(?)");
             $stm->execute(array($data->__get('Pedido_Id')));
        } catch (Exception $e) {
             echo die($e->getMessage());
        }
   }
    public function Eliminar(Pedido $data){
        try {
             $stm = $this->pdo->prepare("CALL ped_eliminar(?)");
             $stm->execute(array($data->__get('Pedido_Id')));
        } catch (Exception $e) {
             echo die($e->getMessage());
        }
   }
}
?>
