<?php
class Pedido{
    private $Pedido_Id;
    private $OrigenDestino_Id;
    private $sODRucDni;
    private $sODNombre;
    private $dPedFecha;
    private $nPedTipopago;
    private $Moneda_Id;
    private $Documento_Id;
    private $dPedImporteAd;
    private $dPedImporte;
    private $dPedIGV;
    private $dPedTotal;
    private $dPedSaldo;
    private $sPedObservaciones;
    private $nPedEstado;
    private $dPedEliminado;
    private $dPedFecha_Act;
    private $Usuario_Id;
    private $nroOrdenTrabajo;
    
    
    public function __GET($a){
        return $this->$a;
    }

    public function __SET($a, $b){
        return $this->$a = $b;
    }
}
?>
