<?php

class ProductoloteModel {
    private $pdo;
    public function __CONSTRUCT(){
        try {
            require_once 'model/database.php';
            $this->pdo = DataBase::ObtenerConexion();
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function Listar($ProdServ_Id){
        try {
            $result = array();
            $stm = $this->pdo->prepare('CALL prosrv_ListarLotes('.$ProdServ_Id.',1);');
            $stm->execute();
            
            foreach ($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $param = new Productolote();
                $param->__SET('ProductoLote_Id', $r->ProductoLote_Id);                
                $param->__SET('Almacén', $r->Almacén);
                $param->__SET('Lote', $r->Lote);
                $param->__SET('Vencimiento', $r->Vencimiento);
                $param->__SET('Stock', $r->Stock);
                $param->__SET('Unidad', $r->Unidad);
                $param->__SET('TipoPrecio', $r->TipoPrecio);
                $param->__SET('PrecioListaIGV', $r->PrecioListaIGV);
                $param->__SET('PrecioCostoIGV', $r->PrecioCostoIGV);
                $param->__SET('Precio1', $r->Precio1);
                $param->__SET('Precio2', $r->Precio2);
                $param->__SET('Precio3', $r->Precio3);
                $param->__SET('Porc1', $r->Porc1);
                $param->__SET('Porc2', $r->Porc2);
                $param->__SET('Porc3', $r->Porc3);
                $param->__SET('Almacen_Id', $r->Almacen_Id);
                $param->__SET('Descuento', $r->Descuento);
                $param->__SET('nProLMonedaCompra_Id', $r->nProLMonedaCompra_Id);
                $param->__SET('PrecioCosto', $r->PrecioCosto);
                $result[] = $param;
            }
            return $result;
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    public function Buscar($ProductoLote_Id){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL prosrv_BuscarLotes($ProductoLote_Id)");
            $stm->execute();

            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $pro = new Productolote();
                $pro->__SET('ProductoLote_Id', $r->ProductoLote_Id);
                $pro->__SET('ProdServ_Id', $r->ProdServ_Id);
                $pro->__SET('Almacen_Id', $r->Almacen_Id);
                $pro->__SET('sProLNombreLote', $r->sProLNombreLote);
                $pro->__SET('nProLStockReal', $r->nProLStockReal);
                $pro->__SET('dProLFechaVencimiento', $r->dProLFechaVencimiento);
                $pro->__SET('nProLPrecioL', $r->nProLPrecioL);
                $pro->__SET('nProLPrecioC', $r->nProLPrecioC);
                $pro->__SET('nProLTipoPrecio', $r->nProLTipoPrecio);
                $pro->__SET('nProLPorc1', $r->nProLPorc1);
                $pro->__SET('nProLPrecioV', $r->nProLPrecioV);
                $pro->__SET('nProLPorc2', $r->nProLPorc2);
                $pro->__SET('nProLPrecioV2', $r->nProLPrecioV2);
                $pro->__SET('nProLPorc3', $r->nProLPorc3);
                $pro->__SET('nProLPrecioV3', $r->nProLPrecioV3);
                $pro->__SET('dProLFecha_Act', $r->dProLFecha_Act);
                $pro->__SET('nProLUsuario_Id', $r->nProLUsuario_Id);
                $pro->__SET('nProLMonedaCompra_Id', $r->nProLMonedaCompra_Id);
                $pro->__SET('nProLDescuentoCompra', $r->nProLDescuentoCompra);
                $result[] = $pro;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }    
    public function Lotes($sProLNombreLote){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL prosrv_movlistar('$sProLNombreLote')");
            $stm->execute();

            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $pro = new Productolote();
                $pro->__SET('ProductoLote_Id', $r->ProductoLote_Id);
                $pro->__SET('ProdServ_Id', $r->ProdServ_Id);
                $pro->__SET('Almacen_Id', $r->Almacen_Id);
                $pro->__SET('sProLNombreLote', $r->sProLNombreLote);
                $pro->__SET('nProLStockReal', $r->nProLStockReal);
                $pro->__SET('dProLFechaVencimiento', $r->dProLFechaVencimiento);
                $pro->__SET('nProLPrecioL', $r->nProLPrecioL);
                $pro->__SET('nProLPrecioC', $r->nProLPrecioC);
                $pro->__SET('nProLTipoPrecio', $r->nProLTipoPrecio);
                $pro->__SET('nProLPorc1', $r->nProLPorc1);
                $pro->__SET('nProLPrecioV', $r->nProLPrecioV);
                $pro->__SET('nProLPorc2', $r->nProLPorc2);
                $pro->__SET('nProLPrecioV2', $r->nProLPrecioV2);
                $pro->__SET('nProLPorc3', $r->nProLPorc3);
                $pro->__SET('nProLPrecioV3', $r->nProLPrecioV3);
                $pro->__SET('dProLFecha_Act', $r->dProLFecha_Act);
                $pro->__SET('nProLUsuario_Id', $r->nProLUsuario_Id);
                $pro->__SET('nProLMonedaCompra_Id', $r->nProLMonedaCompra_Id);
                $pro->__SET('nProLDescuentoCompra', $r->nProLDescuentoCompra);
                $result[] = $pro;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
}