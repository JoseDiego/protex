<?php
class RecetaModel {
    private $pdo;
    function __CONSTRUCT(){
        try {
            require_once 'model/database.php';
            $this->pdo = DataBase::ObtenerConexion();
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function Listar(){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL rec_listar(0)");
            $stm->execute();
            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $rec = new Receta();
                $rec->__SET('Receta_Id', $r->Receta_Id);
                $rec->__SET('nRecProdServ_Id', $r->nRecProdServ_Id);
                $rec->__SET('UnidadMedida_Id', $r->UnidadMedida_Id);
                $rec->__SET('nRecCantidad', $r->nRecCantidad);
                $rec->__SET('nRecEliminado', $r->nRecEliminado);
                $rec->__SET('dRecFecha_Act', $r->dRecFecha_Act);
                $rec->__SET('Usuario_Id', $r->Usuario_Id);
                $rec->__SET('ProdServ_Id', $r->ProdServ_Id);
                $result[] = $rec;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function Buscar($ProdServ_Id){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL rec_listar($ProdServ_Id)");
            $stm->execute();
            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $result[] = array("Receta_Id"       => $r->Receta_Id,
                                  "ProdServ_Id"     => $r->nRecProdServ_Id,
                                  "UnidadMedida_Id" => $r->UnidadMedida_Id,
                                  "nRecCantidad"    => $r->nRecCantidad);;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function Crear(Receta $data){
        try {
            $stm = $this->pdo->prepare("CALL rec_crear(?,?,?,?,?,?)");
            $stm->execute(array(
                                 $data->__get('nRecProdServ_Id'),
                                 $data->__get('UnidadMedida_Id'),
                                 $data->__get('nRecCantidad'),
                                 $data->__get('nRecEliminado'),
                                 $data->__get('Usuario_Id'),
                                 $data->__get('ProdServ_Id')
                                 ));
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    public function Editar(Receta $data){
        try {
            $stm = $this->pdo->prepare("CALL rec_editar(?,?,?,?,?,?,?)");
            $stm->execute(array(
                                 $data->__get('nRecProdServ_Id'),
                                 $data->__get('UnidadMedida_Id'),
                                 $data->__get('nRecCantidad'),
                                 $data->__get('nRecEliminado'),
                                 $data->__get('dRecFecha_Act'),
                                 $data->__get('Usuario_Id'),
                                 $data->__get('ProdServ_Id'),
                                 $data->__get('Receta_Id')
                                 ));
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    public function Eliminar(Receta $data){
        try {
             $stm = $this->pdo->prepare("CALL rec_eliminar(?)");
             $stm->execute(array($data->__get('Receta_Id')));
        } catch (Exception $e) {
             echo die($e->getMessage());
        }
   }
}
?>
