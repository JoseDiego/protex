<?php
class Pedido_detalleModel {
    private $pdo;
    function __CONSTRUCT(){
        try {
            require_once 'model/database.php';
            $this->pdo = DataBase::ObtenerConexion();
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function Listar($Pedido_Id){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL peddet_listar($Pedido_Id,1)");
            $stm->execute();
            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $ped = new Pedido_detalle();
                $ped->__SET('Pedidodetalle_Id', $r->Pedidodetalle_Id);
                $ped->__SET('Pedido_Id', $r->Pedido_Id);
                $ped->__SET('ProdServ_Id', $r->ProdServ_Id);
                $ped->__SET('UnidadMedida_Id', $r->UnidadMedida_Id);
                $ped->__SET('nPedDetCantidad', $r->nPedDetCantidad);
                $ped->__SET('dPedDetPrecioUnitario', $r->dPedDetPrecioUnitario);
                $ped->__SET('dPedDetSubtotal', $r->dPedDetSubtotal);
                $result[] = $ped;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function Buscar($Pedidodetalle_Id){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL peddet_listar($Pedidodetalle_Id,0)");
            $stm->execute();
            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $ped = new Pedido_detalle();
                $ped->__SET('Pedidodetalle_Id', $r->Pedidodetalle_Id);
                $ped->__SET('Pedido_Id', $r->Pedido_Id);
                $ped->__SET('ProdServ_Id', $r->ProdServ_Id);
                $ped->__SET('UnidadMedida_Id', $r->UnidadMedida_Id);
                $ped->__SET('nPedDetCantidad', $r->nPedDetCantidad);
                $ped->__SET('dPedDetPrecioUnitario', $r->dPedDetPrecioUnitario);
                $ped->__SET('dPedDetSubtotal', $r->dPedDetSubtotal);
                $result[] = $ped;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function Crear(Pedido_detalle $data){
        try {
            $stm = $this->pdo->prepare("CALL peddet_crear(?,?,?,?,?,?)");
            $stm->execute(array(
                                 $data->__get('Pedido_Id'),
                                 $data->__get('ProdServ_Id'),
                                 $data->__get('UnidadMedida_Id'),
                                 $data->__get('nPedDetCantidad'),
                                 $data->__get('dPedDetPrecioUnitario'),
                                 $data->__get('dPedDetSubtotal')
                                 ));
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    public function Editar(Pedido_detalle $data){
        try {
            $stm = $this->pdo->prepare("CALL peddet_editar(?,?,?,?,?,?)");
            $stm->execute(array(
                                 $data->__get('Pedido_Id'),
                                 $data->__get('ProdServ_Id'),
                                 $data->__get('UnidadMedida_Id'),
                                 $data->__get('nPedDetCantidad'),
                                 $data->__get('dPedDetPrecioUnitario'),
                                 $data->__get('dPedDetSubtotal'),
                                 $data->__get('Pedidodetalle_Id')
                                 ));
            echo "Se modifico correctamente";
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    public function Eliminar($Pedido_Id){
        try {
             $stm = $this->pdo->prepare("CALL peddet_eliminar($Pedido_Id)");
             $stm->execute();
//             echo "Se elimino correctamente";
        } catch (Exception $e) {
             echo die($e->getMessage());
        }
   }
}
?>
