<?php

class DocumentoModel {
    private $pdo;
    public function __CONSTRUCT(){
        try {
            require_once 'model/database.php';
            $this->pdo = DataBase::ObtenerConexion();
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function Listar(){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL doc_listar(0)");
            $stm->execute();

            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $doc = new Documento();
                $doc->__SET('Documento_Id', $r->Documento_Id);
                $doc->__SET('Almacen_Id', $r->Almacen_Id);
                $doc->__SET('sAlmNombre', $r->sAlmNombre);
                $doc->__SET('nDocTipoMovimiento_Id', $r->nDocTipoMovimiento_Id);
                $doc->__SET('sTMovNombre', $r->sTMovNombre);
                $doc->__SET('sDocNombre', $r->sDocNombre);
                $doc->__SET('sDocNombreCorto', $r->sDocNombreCorto);
                $doc->__SET('nDocNomAutomatico', $r->nDocNomAutomatico);
                $doc->__SET('sDocSiguiente', $r->sDocSiguiente);
                $doc->__SET('nDocAfectoIGV', $r->nDocAfectoIGV);
                $doc->__SET('nDocEstado', $r->nDocEstado);
                $doc->__SET('nDocEliminado', $r->nDocEliminado);
                $doc->__SET('dDocFecha_Act', $r->dDocFecha_Act);
                $doc->__SET('Usuario_Id', $r->Usuario_Id);
                $result[] = $doc;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }

    public function Buscar($Documento_Id){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL doc_listar($Documento_Id)");
            $stm->execute();

            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $doc = new Documento();
                $doc->__SET('Documento_Id', $r->Documento_Id);
                $doc->__SET('Almacen_Id', $r->Almacen_Id);
                $doc->__SET('sAlmNombre', $r->sAlmNombre);
                $doc->__SET('nDocTipoMovimiento_Id', $r->nDocTipoMovimiento_Id);
                $doc->__SET('sTMovNombre', $r->sTMovNombre);
                $doc->__SET('sDocNombre', $r->sDocNombre);
                $doc->__SET('sDocNombreCorto', $r->sDocNombreCorto);
                $doc->__SET('nDocNomAutomatico', $r->nDocNomAutomatico);
                $doc->__SET('sDocSiguiente', $r->sDocSiguiente);
                $doc->__SET('nDocAfectoIGV', $r->nDocAfectoIGV);
                $doc->__SET('nDocEstado', $r->nDocEstado);
                $doc->__SET('nDocEliminado', $r->nDocEliminado);
                $doc->__SET('dDocFecha_Act', $r->dDocFecha_Act);
                $doc->__SET('Usuario_Id', $r->Usuario_Id);
                $result[] = $doc;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }

    public function Guardar(Documento $data){
        try {
            $stm = $this->pdo->prepare("CALL doc_crear(?,?,?,?,?,?,?,?,?,?)");
            $stm->execute(array(
                                $data->__get('Almacen_Id'),
                                $data->__get('nDocTipoMovimiento_Id'),
                                $data->__get('sDocNombre'),
                                $data->__get('sDocNombreCorto'),
                                $data->__get('nDocNomAutomatico'),
                                $data->__get('sDocSiguiente'),
                                $data->__get('nDocAfectoIGV'),
                                $data->__get('nDocEstado'),
                                $data->__get('nDocEliminado'),
                                $data->__get('Usuario_Id')
                         ));
            echo '<div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Exito información guardado</strong>, los datos fueron registrados correctamente.
                  </div>';
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }

    public function Editar(Documento $data){
        try {
            $stm = $this->pdo->prepare("CALL doc_editar(?,?,?,?,?,?,?,?,?,?,?)");
            $stm->execute(array(
                                 $data->__get('Almacen_Id'),
                                 $data->__get('nDocTipoMovimiento_Id'),
                                 $data->__get('sDocNombre'),
                                 $data->__get('sDocNombreCorto'),
                                 $data->__get('nDocNomAutomatico'),
                                 $data->__get('sDocSiguiente'),
                                 $data->__get('nDocAfectoIGV'),
                                 $data->__get('nDocEstado'),
                                 $data->__get('nDocEliminado'),
                                 $data->__get('Usuario_Id'),
                                 $data->__get('Documento_Id')
                                ));
            echo '<div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Exito información editado</strong>, los datos fueron editados correctamente.
                  </div>';  
            echo '<script type="text/javascript">window.location="?c=Tipodocumento&a=Index";</script>'; 
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }

    public function Eliminar(Documento $data){
        try {
            $stm = $this->pdo->prepare("CALL doc_eliminar(?)");
            $stm->execute(array($data->__get('Documento_Id')));
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function Incrementa($Documento_Id){
        try {
            $stm = $this->pdo->prepare("CALL Doc_Incrementa($Documento_Id)");
            $stm->execute();
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
}