<?php

class ActivoModel {
    private $pdo;
    public function __CONSTRUCT(){
        try {
            require_once 'model/database.php';
            $this->pdo = DataBase::ObtenerConexion();
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function Listar(){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL act_listar(0)");
            $stm->execute();

            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $act = new Activo();
                $act->__SET('Activo_Id', $r->Activo_Id);
                $act->__SET('sActDescripcion', $r->sActDescripcion);
                $act->__SET('nActEstado', $r->nActEstado);
                $act->__SET('nActEliminado', $r->nActEliminado);
                $act->__SET('dActFecha_Act', $r->dActFecha_Act);
                $act->__SET('Usuario_Id', $r->Usuario_Id);
                $result[] = $act;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }

    public function Buscar($Activo_Id){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL act_listar($Activo_Id)");
            $stm->execute();

            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $act = new Activo();
                $act->__SET('Activo_Id', $r->Activo_Id);
                $act->__SET('sActDescripcion', $r->sActDescripcion);
                $act->__SET('nActEstado', $r->nActEstado);
                $act->__SET('nActEliminado', $r->nActEliminado);
                $act->__SET('dActFecha_Act', $r->dActFecha_Act);
                $act->__SET('Usuario_Id', $r->Usuario_Id);
                $result[] = $act;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }

    public function Guardar(Activo $data){
        try {
            $stm = $this->pdo->prepare("CALL act_crear(?,?,?,?,?)");
            $stm->execute(array(
                                $data->__get('sActDescripcion'),
                                $data->__get('nActEstado'),
                                $data->__get('nActEliminado'),
                                $data->__get('dActFecha_Act'),
                                $data->__get('Usuario_Id')
                         ));
            echo '<div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Exito información guardado</strong>, los datos fueron registrados correctamente.
                  </div>';
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }

    public function Editar(Activo $data){
        try {
            $stm = $this->pdo->prepare("CALL act_editar(?,?,?,?,?,?)");
            $stm->execute(array(
                                 $data->__get('sActDescripcion'),
                                 $data->__get('nActEstado'),
                                 $data->__get('nActEliminado'),
                                 $data->__get('dActFecha_Act'),
                                 $data->__get('Usuario_Id'),
                                 $data->__get('Activo_Id')
                                ));
            echo '<div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Exito información editado</strong>, los datos fueron editados correctamente.
                  </div>';  
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }

    public function Eliminar(Activo $data){
        try {
            $stm = $this->pdo->prepare("CALL act_eliminar(?)");
            $stm->execute(array($data->__get('Activo_Id')));
            echo "Se elimino correctamente";
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
}
