<?php

class ProdservModel {
    private $pdo;
    public function __CONSTRUCT(){
        try {
            require_once 'model/database.php';
            $this->pdo = DataBase::ObtenerConexion();
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function UltimoCodigo($Familia_Id){
        try {
            $stm = $this->pdo->prepare("CALL pro_UltimoCodigo($Familia_Id)");
            $stm->execute();
            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                return $r->UltCodigo;
            }
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function Filtrar($txtNameProduct){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL pro_filtro('$txtNameProduct')");
            $stm->execute();

            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $pro = new Prodserv();
                $pro->__SET('ProdServ_Id', $r->ProdServ_Id);
                $pro->__SET('sProSrvCodigo', $r->sProSrvCodigo);
                $pro->__SET('sProSrvNombre', $r->sProSrvNombre);
                $pro->__SET('sProSrvCodigoBarras', $r->sProSrvCodigoBarras);
                $pro->__SET('nProSrvFamilia_Id', $r->nProSrvFamilia_Id);
                $pro->__SET('sFamDescripcion', $r->sFamDescripcion);
                $pro->__SET('Activo_Id', $r->Activo_Id);
                $pro->__SET('sActDescripcion', $r->sActDescripcion);
                $pro->__SET('nProSrvUnidad_Id', $r->nProSrvUnidad_Id);
                $pro->__SET('sUndDescripcion', $r->sUndDescripcion);
                $pro->__SET('nProSrvExoneradoIGV', $r->nProSrvExoneradoIGV);
                $pro->__SET('nProSrvEstado', $r->nProSrvEstado);
                $pro->__SET('nProSrvEliminado', $r->nProSrvEliminado);
                $pro->__SET('dProSrvFecha_Act', $r->dProSrvFecha_Act);
                $pro->__SET('Usuario_Id', $r->Usuario_Id);
                $result[] = $pro;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function Listar(){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL pro_listar(0)");
            $stm->execute();

            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $pro = new Prodserv();
                $pro->__SET('ProdServ_Id', $r->ProdServ_Id);
                $pro->__SET('sProSrvCodigo', $r->sProSrvCodigo);
                $pro->__SET('sProSrvNombre', $r->sProSrvNombre);
                $pro->__SET('sProSrvCodigoBarras', $r->sProSrvCodigoBarras);
                $pro->__SET('nProSrvFamilia_Id', $r->nProSrvFamilia_Id);
                $pro->__SET('sFamDescripcion', $r->sFamDescripcion);
                $pro->__SET('Activo_Id', $r->Activo_Id);
                $pro->__SET('sActDescripcion', $r->sActDescripcion);
                $pro->__SET('nProSrvUnidad_Id', $r->nProSrvUnidad_Id);
                $pro->__SET('sUndDescripcion', $r->sUndDescripcion);
                $pro->__SET('nProSrvExoneradoIGV', $r->nProSrvExoneradoIGV);
                $pro->__SET('nProSrvEstado', $r->nProSrvEstado);
                $pro->__SET('nProSrvEliminado', $r->nProSrvEliminado);
                $pro->__SET('dProSrvFecha_Act', $r->dProSrvFecha_Act);
                $pro->__SET('Usuario_Id', $r->Usuario_Id);
                $result[] = $pro;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }

    public function Buscar($ProdServ_Id){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL pro_listar($ProdServ_Id)");
            $stm->execute();

            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $pro = new Prodserv();
                $pro->__SET('ProdServ_Id', $r->ProdServ_Id);
                $pro->__SET('sProSrvCodigo', $r->sProSrvCodigo);
                $pro->__SET('sProSrvNombre', $r->sProSrvNombre);
                $pro->__SET('sProSrvCodigoBarras', $r->sProSrvCodigoBarras);
                $pro->__SET('nProSrvFamilia_Id', $r->nProSrvFamilia_Id);
                $pro->__SET('sFamDescripcion', $r->sFamDescripcion);
                $pro->__SET('Activo_Id', $r->Activo_Id);
                $pro->__SET('sActDescripcion', $r->sActDescripcion);
                $pro->__SET('nProSrvUnidad_Id', $r->nProSrvUnidad_Id);
                $pro->__SET('sUndDescripcion', $r->sUndDescripcion);
                $pro->__SET('nProSrvExoneradoIGV', $r->nProSrvExoneradoIGV);
                $pro->__SET('nProSrvEstado', $r->nProSrvEstado);
                $pro->__SET('nProSrvEliminado', $r->nProSrvEliminado);
                $pro->__SET('dProSrvFecha_Act', $r->dProSrvFecha_Act);
                $pro->__SET('Usuario_Id', $r->Usuario_Id);
    //            $pro->__SET('ProSrvEspecificacion', $r->ProSrvEspecificacion);
    //            $pro->__SET('ProSrvNomImagen', $r->ProSrvNomImagen);
                $result[] = $pro;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }

    public function Guardar(Prodserv $data){
        try {
//            echo "<pre>";
//            print_r($data);
//            echo "</pre>";
            $stm = $this->pdo->prepare("CALL pro_crear(?,?,?,?,?,?,?,?,?,?,?,@ProdServ_Id)");
            $stm->execute(array(
                                $data->__get('sProSrvCodigo'),
                                $data->__get('sProSrvNombre'),
                                $data->__get('sProSrvCodigoBarras'),
                                $data->__get('nProSrvFamilia_Id'),
                                $data->__get('Activo_Id'),
                                $data->__get('nProSrvUnidad_Id'),
                                $data->__get('ProSrvEspecificacion'),
                                $data->__get('nProSrvExoneradoIGV'),
                                $data->__get('nProSrvEstado'),
                                $data->__get('nProSrvEliminado'),
                                $data->__get('Usuario_Id'),
                         ));
            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                return $r->ProdServ_Id;
            }
//            echo '<div class="alert alert-success" role="alert">
//                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
//                    <strong>Exito información guardado</strong>, los datos fueron registrados correctamente.
//                  </div>';
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }

    public function ActualizarFotoProdserv($codProducto,$foto){
        try{
            $stm = $this->pdo->prepare("CALL prosrv_fotomodif(?,?)");
            $stm->execute(array($codProducto,$foto));

        }catch(Exception $e){
            echo die($e->getMessage());
        }
    }

    public function Editar(Prodserv $data){
        try {
            $stm = $this->pdo->prepare("CALL pro_editar(?,?,?,?,?,?,?,?,?,?,?,?)");
            $stm->execute(array(
                                 $data->__get('sProSrvCodigo'),
                                 $data->__get('sProSrvNombre'),
                                 $data->__get('sProSrvCodigoBarras'),
                                 $data->__get('nProSrvFamilia_Id'),
                                 $data->__get('Activo_Id'),
                                 $data->__get('nProSrvUnidad_Id'),
                                 $data->__get('nProSrvExoneradoIGV'),
                                 $data->__get('nProSrvEstado'),
                                 $data->__get('nProSrvEliminado'),
                                 $data->__get('Usuario_Id'),
                                 $data->__get('ProSrvEspecificacion'),
                                 $data->__get('ProdServ_Id')
                                )); 
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }

    public function Eliminar(Prodserv $data){
        try {
            $stm = $this->pdo->prepare("CALL pro_eliminar(?)");
            $stm->execute(array($data->__get('ProdServ_Id')));
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function VerificarStock($ProdServ_Id, $Cantidad, $Unidad_Id){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL prosrv_verificastock('$ProdServ_Id','$Cantidad','$Unidad_Id')");
            $stm->execute();

            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                
                $result = $r->RESULT;
                
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }

}