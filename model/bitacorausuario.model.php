<?php

class BitacorausuarioModel {
    private $pdo;
    public function __CONSTRUCT(){
        try {
            require_once 'model/database.php';
            $this->pdo = DataBase::ObtenerConexion();
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    public function ListarBitacorausuario($idPasajero){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL rep_auditoria('$idPasajero');");
            $stm->execute();

            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $bi = new Bitacorausuario();
                $bi->__SET('dBiusuFechaHora', $r->FechaHora); 
                $bi->__SET('nBiusuDescripcion', $r->Evento); 
                $result[] = $bi;
            }
            return $result;
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    public function guardarBitacorausuario(Bitacorausuario $data){
        try {
            $sql = "CALL biusu_crear(?, ?, ?)";

            $this->pdo->prepare($sql)
                 ->execute(array($data->__GET("nBiusuTipoModulo"),
                                 $data->__GET("nBiusuUsuario_Id"),
                                 $data->__GET("nBiusuDescripcion")
                                )); 
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
}
