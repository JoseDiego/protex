<?php
class PrecioventaModel {
    private $pdo;
    function __CONSTRUCT(){
        try {
            require_once 'model/database.php';
            $this->pdo = DataBase::ObtenerConexion();
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function Precio($ProdServ_Id,$UnidadMedida_Id){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL pre_obtenerprecio($ProdServ_Id,$UnidadMedida_Id)");
            $stm->execute();
            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $pre = new Precioventa();
                $pre->__SET('PrecioVenta_Id', $r->PrecioVenta_Id);
                $pre->__SET('ProdServ_Id', $r->ProdServ_Id);
                $pre->__SET('sProSrvNombre', $r->sProSrvNombre);
                $pre->__SET('UnidadMedida_Id', $r->UnidadMedida_Id);
                $pre->__SET('Moneda_Id', $r->Moneda_Id);
                $pre->__SET('sUndDescripcion', $r->sUndDescripcion);
                $pre->__SET('PreVenPrecio', $r->PreVenPrecio);
                $pre->__SET('dPreVenFechaAct', $r->dPreVenFechaAct);
                $pre->__SET('nPreVenEliminado', $r->nPreVenEliminado);
                $pre->__SET('Usuario_Id', $r->Usuario_Id);
                $result[] = $pre;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function Listar($codigo){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL pre_listar($codigo,0)");
            $stm->execute();
            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $pre = new Precioventa();
                $pre->__SET('PrecioVenta_Id', $r->PrecioVenta_Id);
                $pre->__SET('ProdServ_Id', $r->ProdServ_Id);
                $pre->__SET('sProSrvNombre', $r->sProSrvNombre);
                $pre->__SET('UnidadMedida_Id', $r->UnidadMedida_Id);
                $pre->__SET('Moneda_Id', $r->Moneda_Id);
                $pre->__SET('sUndDescripcion', $r->sUndDescripcion);
                $pre->__SET('PreVenPrecio', $r->PreVenPrecio);
                $pre->__SET('dPreVenFechaAct', $r->dPreVenFechaAct);
                $pre->__SET('nPreVenEliminado', $r->nPreVenEliminado);
                $pre->__SET('Usuario_Id', $r->Usuario_Id);
                $result[] = $pre;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function Buscar($codigo){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL pre_listar($codigo,1)");
            $stm->execute();
            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $pre = new Precioventa();
                $pre->__SET('PrecioVenta_Id', $r->PrecioVenta_Id);
                $pre->__SET('ProdServ_Id', $r->ProdServ_Id);
                $pre->__SET('sProSrvNombre', $r->sProSrvNombre);
                $pre->__SET('UnidadMedida_Id', $r->UnidadMedida_Id);
                $pre->__SET('Moneda_Id', $r->Moneda_Id);
                $pre->__SET('sUndDescripcion', $r->sUndDescripcion);
                $pre->__SET('PreVenPrecio', $r->PreVenPrecio);
                $pre->__SET('dPreVenFechaAct', $r->dPreVenFechaAct);
                $pre->__SET('nPreVenEliminado', $r->nPreVenEliminado);
                $pre->__SET('Usuario_Id', $r->Usuario_Id);
                $result[] = $pre;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function Crear(Precioventa $data){
        try {
            $stm = $this->pdo->prepare("CALL pre_crear(?,?,?,?,?,?)");
            $stm->execute(array(
                                 $data->__get('ProdServ_Id'),
                                 $data->__get('UnidadMedida_Id'),
                                 $data->__get('Moneda_Id'),
                                 $data->__get('PreVenPrecio'),
                                 $data->__get('nPreVenEliminado'),
                                 $data->__get('Usuario_Id')
                                 ));
            echo '<div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Exito información guardado</strong>, los datos fueron registrados correctamente.
                  </div>';
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    public function Editar(Precioventa $data){
        try {
            $stm = $this->pdo->prepare("CALL pre_editar(?,?,?,?,?,?,?)");
            $stm->execute(array(
                                 $data->__get('ProdServ_Id'),
                                 $data->__get('UnidadMedida_Id'),
                                 $data->__get('Moneda_Id'),
                                 $data->__get('PreVenPrecio'),
                                 $data->__get('dPreVenFechaAct'),
                                 $data->__get('nPreVenEliminado'),
                                 $data->__get('Usuario_Id'),
                                 $data->__get('PrecioVenta_Id')
                                 ));
            echo '<div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Exito información editado</strong>, los datos fueron editados correctamente.
                  </div>'; 
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    public function Eliminar(Precioventa $data){
        try {
             $stm = $this->pdo->prepare("CALL pre_eliminar(?)");
             $stm->execute(array($data->__get('PrecioVenta_Id')));
             echo '<div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Exito información Eliminada</strong>, los datos fueron eliminados correctamente.
                  </div>';
        } catch (Exception $e) {
             echo die($e->getMessage());
        }
   }
}
?>
