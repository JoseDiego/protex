<?php
class AlmacenModel {
    private $pdo;
    public function __CONSTRUCT(){
        try {
            require_once 'model/database.php';
            $this->pdo = DataBase::ObtenerConexion();
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function Listar(){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL alm_listar(0)");
            $stm->execute();

            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $alm = new Almacen();
                $alm->__SET('Almacen_Id', $r->Almacen_Id);
                $alm->__SET('sAlmNombre', $r->sAlmNombre);
                $alm->__SET('sAlmUbicacion', $r->sAlmUbicacion);
                $alm->__SET('nAlmEstado', $r->nAlmEstado);
                $alm->__SET('nAlmEliminado', $r->nAlmEliminado);
                $alm->__SET('dAlmFecha_Act', $r->dAlmFecha_Act);
                $alm->__SET('Usuario_Id', $r->Usuario_Id);
                $result[] = $alm;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }

    public function Buscar($Almacen_Id){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL alm_listar($Almacen_Id)");
            $stm->execute();

            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $alm = new Almacen();
                $alm->__SET('Almacen_Id', $r->Almacen_Id);
                $alm->__SET('sAlmNombre', $r->sAlmNombre);
                $alm->__SET('sAlmUbicacion', $r->sAlmUbicacion);
                $alm->__SET('nAlmEstado', $r->nAlmEstado);
                $alm->__SET('nAlmEliminado', $r->nAlmEliminado);
                $alm->__SET('dAlmFecha_Act', $r->dAlmFecha_Act);
                $alm->__SET('Usuario_Id', $r->Usuario_Id);
                $result[] = $alm;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }

    public function Guardar(Almacen $data){
        try {
            $stm = $this->pdo->prepare("CALL alm_crear(?,?,?,?,?)");
            $stm->execute(array(
                                $data->__get('sAlmNombre'),
                                $data->__get('sAlmUbicacion'),
                                $data->__get('nAlmEstado'),
                                $data->__get('nAlmEliminado'),
                                $data->__get('Usuario_Id')
                         ));
            echo '<div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Exito información guardado</strong>, los datos fueron registrados correctamente.
                  </div>';
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }

    public function Editar(Almacen $data){
        try {
            $stm = $this->pdo->prepare("CALL alm_editar(?,?,?,?,?,?)");
            $stm->execute(array(
                                 $data->__get('sAlmNombre'),
                                 $data->__get('sAlmUbicacion'),
                                 $data->__get('nAlmEstado'),
                                 $data->__get('nAlmEliminado'),
                                 $data->__get('Usuario_Id'),
                                 $data->__get('Almacen_Id')
                                ));
            echo '<div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Exito información editado</strong>, los datos fueron editados correctamente.
                  </div>';
            echo '<script type="text/javascript">window.location="?c=Almacen&a=Index";</script>';
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }

    public function Eliminar(Almacen $data){
        try {
            $stm = $this->pdo->prepare("CALL alm_eliminar(?)");
            $stm->execute(array($data->__get('Almacen_Id')));
            echo "Se elimino correctamente";
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
}
