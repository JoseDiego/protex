<?php
class Ordencompra{
    private $Movimiento_Id;
    private $OrigenDestino_Id;
    private $dPedFecha;
    private $nPedTipopago;
    private $Documento_Id;
    private $sDocNombre;
    private $nroOrdenTrabajo;
    private $Moneda_Id;
    
    private $dPedImporte;
    private $dPedIGV;
    private $dPedTotal;
    private $dPedSaldo;
    private $sPedObservaciones;
    private $nPedEstado;
    private $dPedEliminado;
    private $dPedFecha_Act;
    private $Usuario_Id;
    private $Pedido_Id;
    private $dPedImporteAd;
    
    
    public function __GET($a){
        return $this->$a;
    }

    public function __SET($a, $b){
        return $this->$a = $b;
    }
}
?>