<?php
class Movimientodetalle{
    private $Movimiento_Id;
    private $TipoMovimiento_Id;
    private $TipoDestino;
    private $ProdServ_Id;
    private $Almacen_Id;
    private $Lote;
    private $FechaVencimiento;
    private $Cantidad;
    private $Unidad_Id;	
    private $MonedaCompra_Id;
    private $PrecioLista;
    private $Descuento;
    private $Precio;
    private $SubTotal;
    private $Usuario_Id;
    private $TipoCambio;
    private $nMovDetIncluyeIgv; 	

    public function __GET($a){
        return $this->$a;
    }

    public function __SET($a, $b){
        return $this->$a = $b;
    }
}
?>
