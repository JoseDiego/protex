<?php

class UnidadmedidaModel {
    private $pdo;
    public function __CONSTRUCT(){
        try {
            require_once 'model/database.php';
            $this->pdo = DataBase::ObtenerConexion();
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function ListarUP($UnidadMedida_Id){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL uni_unipro($UnidadMedida_Id)");
            $stm->execute();

            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $uni = new Unidadmedida();
                $uni->__SET('UnidadMedida_Id', $r->UnidadMedida_Id);
                $uni->__SET('sUndDescripcion', $r->sUndDescripcion);
                $uni->__SET('sUndAlias', $r->sUndAlias);
                $uni->__SET('nUndPadre_Id', $r->nUndPadre_Id);
                $uni->__SET('nUndFac_Cnv', $r->nUndFac_Cnv);
                $uni->__SET('nUndEstado', $r->nUndEstado);
                $uni->__SET('nUndEliminado', $r->nUndEliminado);
                $uni->__SET('dUndFecha_Act', $r->dUndFecha_Act);
                $uni->__SET('Usuario_Id', $r->Usuario_Id);
                $result[] = $uni;
            }
            return $result;
        } catch (Exception $e) {
            echo 'funcion hijos'.die($e->getMessage());
        }
    }
    public function Hijos($idPadre){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL uni_hijos($idPadre)");
            $stm->execute();

            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $uni = new Unidadmedida();
                $uni->__SET('UnidadMedida_Id', $r->UnidadMedida_Id);
                $uni->__SET('sUndDescripcion', $r->sUndDescripcion);
                $uni->__SET('sUndAlias', $r->sUndAlias);
                $uni->__SET('nUndPadre_Id', $r->nUndPadre_Id);
                $uni->__SET('nUndFac_Cnv', $r->nUndFac_Cnv);
                $uni->__SET('nUndEstado', $r->nUndEstado);
                $uni->__SET('nUndEliminado', $r->nUndEliminado);
                $uni->__SET('dUndFecha_Act', $r->dUndFecha_Act);
                $uni->__SET('Usuario_Id', $r->Usuario_Id);
                $result[] = $uni;
            }
            return $result;
        } catch (Exception $e) {
            echo 'funcion hijos'.die($e->getMessage());
        }
    }
    public function Padre(Unidadmedida $data){
        try {
            $stm = $this->pdo->prepare("CALL uni_padre(?,?)");
            $stm->execute(array($data->__get('UnidadMedida_Id'),
                                $data->__get('nUndPadre_Id')));
        } catch (Exception $e) {
            echo 'funcion padre'.die($e->getMessage());
        }
    }
    public function Listar(){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL uni_listar(0)");
            $stm->execute();

            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $uni = new Unidadmedida();
                $uni->__SET('UnidadMedida_Id', $r->UnidadMedida_Id);
                $uni->__SET('sUndDescripcion', $r->sUndDescripcion);
                $uni->__SET('sUndAlias', $r->sUndAlias);
                $uni->__SET('nUndPadre_Id', $r->nUndPadre_Id);
                $uni->__SET('nUndFac_Cnv', $r->nUndFac_Cnv);
                $uni->__SET('nUndEstado', $r->nUndEstado);
                $uni->__SET('nUndEliminado', $r->nUndEliminado);
                $uni->__SET('dUndFecha_Act', $r->dUndFecha_Act);
                $uni->__SET('Usuario_Id', $r->Usuario_Id);
                $result[] = $uni;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }

    public function Buscar($UnidadMedida_Id){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL uni_listar($UnidadMedida_Id)");
            $stm->execute();

            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $uni = new Unidadmedida();
                $uni->__SET('UnidadMedida_Id', $r->UnidadMedida_Id);
                $uni->__SET('sUndDescripcion', $r->sUndDescripcion);
                $uni->__SET('sUndAlias', $r->sUndAlias);
                $uni->__SET('nUndPadre_Id', $r->nUndPadre_Id);
                $uni->__SET('nUndFac_Cnv', $r->nUndFac_Cnv);
                $uni->__SET('nUndEstado', $r->nUndEstado);
                $uni->__SET('nUndEliminado', $r->nUndEliminado);
                $uni->__SET('dUndFecha_Act', $r->dUndFecha_Act);
                $uni->__SET('Usuario_Id', $r->Usuario_Id);
                $result[] = $uni;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }

    public function Guardar(Unidadmedida $data){
        try {
            $stm = $this->pdo->prepare("CALL uni_crear(?,?,?,?,?,?,?)");
            $stm->execute(array(
                                $data->__get('sUndDescripcion'),
                                $data->__get('sUndAlias'),
                                $data->__get('nUndPadre_Id'),
                                $data->__get('nUndFac_Cnv'),
                                $data->__get('nUndEstado'),
                                $data->__get('nUndEliminado'),
                                $data->__get('Usuario_Id')
                         ));
            echo '<div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Exito información guardado</strong>, los datos fueron registrados correctamente.
                  </div>';
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }

    public function Editar(Unidadmedida $data){
        try {
            $stm = $this->pdo->prepare("CALL uni_editar(?,?,?,?,?,?,?,?)");
            $stm->execute(array(
                                 $data->__get('sUndDescripcion'),
                                 $data->__get('sUndAlias'),
                                 $data->__get('nUndPadre_Id'),
                                 $data->__get('nUndFac_Cnv'),
                                 $data->__get('nUndEstado'),
                                 $data->__get('nUndEliminado'),
                                 $data->__get('Usuario_Id'),
                                 $data->__get('UnidadMedida_Id')
                                ));
            echo '<div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Exito información editado</strong>, los datos fueron editados correctamente.
                  </div>'; 
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }

    public function Eliminar(Unidadmedida $data){
        try {
            $stm = $this->pdo->prepare("CALL uni_eliminar(?)");
            $stm->execute(array($data->__get('UnidadMedida_Id')));
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
}