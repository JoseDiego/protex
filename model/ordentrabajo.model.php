<?php

class OrdenTrabajoModel {
    private $pdo;
    function __CONSTRUCT(){
        try {
            require_once 'model/database.php';
            $this->pdo = DataBase::ObtenerConexion();
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    
        public function Listar(){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL otrab_listar(0)");
            $stm->execute();
            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $ped = new OrdenTrabajo();
                $ped->__SET('OrdenTrabajo_Id', $r->OrdenTrabajo_Id);
                $ped->__SET('otFecha', $r->otFecha);
                $ped->__SET('otNroOrdenCompra', $r->otNroOrdenCompra);
                $ped->__SET('otCliente_Id', $r->otCliente_Id);
                $ped->__SET('sODNombre', $r->sODNombre);
                $ped->__SET('otNroOrdenTrabajo', $r->otNroOrdenTrabajo);
                $ped->__SET('otFechaE', $r->otFechaE);
//                $ped->__SET('sPedDocumento', $r->sPedDocumento);
                $ped->__SET('otEstado', $r->otEstado);
                $ped->__SET('otEliminado', $r->otEliminado);
                $ped->__SET('otFecha_Act', $r->otFecha_Act);
                $ped->__SET('Usuario_Id', $r->Usuario_Id);
                $result[] = $ped;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    
    public function Buscar($OrdenTrabajo_Id){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL otrab_listar(".$OrdenTrabajo_Id.")");
            $stm->execute();
            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $ped = new OrdenTrabajo();
                $ped->__SET('OrdenTrabajo_Id', $r->OrdenTrabajo_Id);
                $ped->__SET('otFecha', $r->otFecha);
                $ped->__SET('otNroOrdenCompra', $r->otNroOrdenCompra);
                $ped->__SET('otCliente_Id', $r->otCliente_Id);
                $ped->__SET('sODNombre', $r->sODNombre);
                $ped->__SET('otNroOrdenTrabajo', $r->otNroOrdenTrabajo);
                $ped->__SET('otFechaE', $r->otFechaE);
//                $ped->__SET('sPedDocumento', $r->sPedDocumento);
                $ped->__SET('otEstado', $r->otEstado);
                $ped->__SET('otEliminado', $r->otEliminado);
                $ped->__SET('otFecha_Act', $r->otFecha_Act);
                $ped->__SET('Usuario_Id', $r->Usuario_Id);
                $result[] = $ped;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    
    public function ListarFechas($fInicio,$fFin){
        try {
            $result = array();
            $stm = $this->pdo->prepare("CALL otrab_date('".date('Y-m-d', strtotime($fInicio))."','".date('Y-m-d', strtotime($fFin))."')");
            $stm->execute();
            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $ped = new OrdenTrabajo();
                $ped->__SET('OrdenTrabajo_Id', $r->OrdenTrabajo_Id);
                $ped->__SET('otFecha', $r->otFecha);
                $ped->__SET('otNroOrdenCompra', $r->otNroOrdenCompra);
                $ped->__SET('otCliente_Id', $r->otCliente_Id);
                $ped->__SET('sODNombre', $r->sODNombre);
                $ped->__SET('otNroOrdenTrabajo', $r->otNroOrdenTrabajo);
                $ped->__SET('otFechaE', $r->otFechaE);
//                $ped->__SET('sPedDocumento', $r->sPedDocumento);
                $ped->__SET('otEstado', $r->otEstado);
                $ped->__SET('otEliminado', $r->otEliminado);
                $ped->__SET('otFecha_Act', $r->otFecha_Act);
                $ped->__SET('Usuario_Id', $r->Usuario_Id);
                $result[] = $ped;
            }
            return $result;
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
    public function Guardar(OrdenTrabajo $data){
        try {
            $OrdenTrabajo_Id = 0;
            $stm = $this->pdo->prepare("CALL otrab_crear(?,?,?,?,?,?,?,?,@OrdenTrabajo_Id)");
            $stm->execute(array(
                                 date('Y-m-d', strtotime($data->__get('otFecha'))),
                                 $data->__get('otNroOrdenCompra'),
                                 $data->__get('otCliente_Id'),
                                 $data->__get('otNroOrdenTrabajo'),
                                 date('Y-m-d', strtotime($data->__get('otFechaE'))),
                                 $data->__get('otEstado'),
                                 $data->__get('otEliminado'),
                                 $data->__get('Usuario_Id')
                                 ));            
            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $OrdenTrabajo_Id = $r->OrdenTrabajo_Id;
            }
            return $OrdenTrabajo_Id;
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    
    public function Eliminar(OrdenTrabajo $data){
        try {
             $stm = $this->pdo->prepare("CALL otrab_eliminar(?)");
             $stm->execute(array($data->__get('OrdenTrabajo_Id')));
        } catch (Exception $e) {
             echo die($e->getMessage());
        }
   }
   
    public function Editar(OrdenTrabajo $data){
        try {
            $stm = $this->pdo->prepare("CALL otrab_editar(?,?,?,?,?,?,?,?)");
            $stm->execute(array(
                                 $data->__get('OrdenTrabajo_Id'),
                                 date('Y-m-d', strtotime($data->__get('otFecha'))),
                                 $data->__get('otNroOrdenCompra'),
                                 $data->__get('otCliente_Id'),
                                 $data->__get('otNroOrdenTrabajo'),
                                 date('Y-m-d', strtotime($data->__get('otFechaE'))),
                                 $data->__get('otEstado'),
                                 $data->__get('Usuario_Id')
                                 ));
            
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    
    public function BuscarO($nroOrdenT){
        try {
            $ped = new OrdenTrabajo();
            $ped->__SET('OrdenTrabajo_Id', 0);
            $ped->__SET('otNroOrdenTrabajo', 0);
            
            $result = array();
            $stm = $this->pdo->prepare("CALL otrab_buscar($nroOrdenT)");
            $stm->execute();
            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r){
                $ped = new OrdenTrabajo();
                $ped->__SET('OrdenTrabajo_Id', $r->OrdenTrabajo_Id);
               
                $ped->__SET('otNroOrdenTrabajo', $r->otNroOrdenTrabajo);
                
            }
            return $ped;
            
        } catch (Exception $e) {
            echo die($e->getMessage());
        }
    }
}
