<?php

class Unidadmedida{
    private $UnidadMedida_Id;
    private $sUndDescripcion;
    private $sUndAlias;
    private $nUndPadre_Id;
    private $nUndFac_Cnv;
    private $nUndEstado;
    private $nUndEliminado;
    private $dUndFecha_Act;
    private $Usuario_Id;

    public function __GET($a){
        return $this->$a;
    }
    public function __SET($a, $b) {
        return $this->$a = $b;
    }
}