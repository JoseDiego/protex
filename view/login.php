<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>LOGIN | Protex</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- Bootstrap 3.3.4 -->
        <link href="view/librerias/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- Font Awesome Icons -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="view/librerias/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
        <!-- iCheck -->
        <link href="view/librerias/plugins/iCheck/square/blue.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
            .login-page, .register-page{
                background-image: url('view/librerias/dist/img/body_background login.jpg');
                background-attachment: fixed;
                background-repeat: no-repeat;
                background-size: 100% 100%;
            }
            body {
                overflow: hidden;
            }            
            /* preloader */
            #preloader {
                position: fixed;
                top:0; left:0;
                right:0; bottom:0;
                background: #000;
                opacity: 0.7;
                z-index: 100000;
            }
            #loader {
                width: 100px;
                height: 100px;
                position: absolute;
                left:50%; top:50%;
                background: url(view/librerias/dist/img/loader.gif) no-repeat center 0;
                margin:-50px 0 0 -50px;
            }
        </style>
    </head>
    <body class="login-page">
        <div id="preloader">
            <div id="loader">&nbsp;</div>
        </div>
        <div class="login-box" style="margin: 10% auto;">
            <div class="login-box-body">
<!--                <center>                    
                    <img src="view/librerias/dist/img/logo_alinen_012013.jpg" width="150" class="img-responsive text-center">
                </center>
                -->
                <center>
                    <h2 style="text-transform: uppercase; color: #0063dc; font-weight: bold;">
                        PROTEX
                    </h2>
                </center>
                <p class="login-box-msg">Ingrese sus datos para iniciar sesión</p>
                <form action="javascript:ingresar();" method="post">
                    <div class="form-group has-feedback" id="resultado"></div>
                    <div class="form-group has-feedback">
                        <input name="usuario" id="usuario" type="text" class="form-control" placeholder="Usuario" data-toggle="tooltip" data-placement="top" title="Ingresar Nombre de Usuario" value=""/>
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input name="password" id="password" type="password" class="form-control" placeholder="Contraseña" data-toggle="tooltip" data-placement="top" title="Ingresar Contraseña" value=""/>
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback" style="margin-bottom: 0px;">
                        <div class="row">                        
                            <div class="col-md-6 form-group">
                                <input type="text" class="form-control input-sm" name="codigocaptcha" id="codigocaptcha" placeholder="Codigo Captcha"  data-toggle="tooltip" data-placement="top" title="Ingresar Codigo Captcha"/>
                            </div>
                            <div class="col-md-6 form-group">
                                <img src="view/librerias/catpcha/captcha.php" class="img-thumbnail" id="captcha" style="border-radius: 0px;" data-toggle="tooltip" data-placement="top" title="Codigo Captcha Generado"/>
                                <a onclick="document.getElementById('captcha').src = 'view/librerias/catpcha/captcha.php?'+ Math.random(); return false" name="actualizar" id="actualizar"  data-toggle="tooltip" data-placement="top" title="Actualizar codigo Captcha." style="cursor: pointer;"><span class="glyphicon glyphicon-refresh"></span></a>
                            </div>
                        </div>                    
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-primary btn-block btn-flat" style="width: 100%;">INGRESAR AL SISTEMA</button>
                        </div><!-- /.col -->
                    </div>
                </form>
            </div><!-- /.login-box-body -->
        </div><!-- /.login-box -->

        <!-- jQuery 2.1.4 -->
        <script src="view/librerias/plugins/jQuery/jQuery-2.1.4.min.js"></script>
        <!-- Bootstrap 3.3.2 JS -->
        <script src="view/librerias/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="view/librerias/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
        <script>
            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                });
            });
            $(window).load(function() {
                $('#preloader').fadeIn('slow');
                $('#preloader').fadeOut('slow');
                $('body').css({'overflow':'visible'});
            });
            $(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
            function validar(){           
                var usuario = document.getElementById("usuario").value;
                var password = document.getElementById("password").value;
                var codigocaptcha = document.getElementById("codigocaptcha").value;
                if(usuario=="" || password=="" || codigocaptcha==""){
                    return false;
                }else{
                    return true;
                }

            }
            function ingresar(){           
                $('#preloader').fadeIn('slow');
                if(validar()==true){
                    var usuario = document.getElementById("usuario").value;
                    var password = document.getElementById("password").value;
                    var codigocaptcha = document.getElementById("codigocaptcha").value;
                    $.post('?c=Principal&a=verificarUsuario',{
                        usuario: usuario,
                        clave: password,
                        codigocaptcha: codigocaptcha
                    }, function(data){          
                        $("#resultado").html(data);  
                    });
                }else{
                    $("#resultado").html('<div class="alert alert-danger alert-dismissible text-center" role="alert">'+
                                            '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
                                            '<strong>Error!</strong>, Alguno de los campos están vacios.'+
                                          '</div>');
                    $('#preloader').fadeOut('slow');
                    $('body').css({'overflow':'visible'});
                }
            }
        </script>
    </body>
</html>