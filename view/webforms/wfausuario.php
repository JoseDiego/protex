
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Usuarios
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="wfamain.php"><i class="fa fa-dashboard"></i> Inicio</a></li>
                <li><a href="#">Usuarios</a></li>
            </ol>
        </section>        
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Relación completa de usuarios</h3>

                            <div class="row">
                                <!--                        <div class="col-sm-6">
                                                            <div class="row">
                                                                <div class="col-xs-5">
                                                                    <label>Desde</label>
                                                                    <input type="date" class="form-control date" id="fechaInicio" name="fechaInicio" value="" placeholder="Fecha Inicio">
                                                                </div>
                                                                <div class="col-xs-5">
                                                                    <label>Hasta</label>
                                                                    <input type="date" class="form-control date" id="fechaFin" name="fechaFin" value="" placeholder="Fecha Fin">
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <label>&nbsp;</label><br>
                                                                    <a class="btn btn-primary" onclick=""><span class="glyphicon glyphicon-search"></span></a>
                                                                </div>
                                                            </div>
                                                        </div>-->
                                <div class="col-md-6 text-left" style="margin-top: 15px;">
                                    <a class="btn btn-primary btn-flat" onclick="FrmGuardarUsuario();">Nuevo Usuario</a>
                                </div>
                            </div>

                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Login</th>
                                        <th>Tipo</th>
                                        <th width="12%">Estado</th>
                                        <th width="15%">Operaciones</th>
                                    </tr>
                                </thead>                  	
                                <tbody>  
                                    <?php foreach ($usuarios as $r): ?>                    
                                        <tr> 
                                            <td><?php echo $r->__GET('sUsuNombre'); ?></td>
                                            <td><?php echo $r->__GET('sUsuLogin'); ?></td>
                                            <td>
                                                <?php
                                                if ($r->__GET('nUsuTipo') == 1) {
                                                    echo "ADMINISTRADOR";
                                                } elseif ($r->__GET('nUsuTipo') == 2) {
                                                    echo "ASISTENTE";
                                                } elseif ($r->__GET('nUsuTipo') == 3) {
                                                    echo "REPORTES";
                                                } else {
                                                    echo "SUPERVISOR";
                                                }
                                                ?>                            
                                            </td>
                                            <td>
                                                <?php
                                                if ($r->__GET('nUsuEstado') != 0) {
                                                    ?>
                                                    <span class="label label-success">Habilitado</span>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <span class="label label-danger">Inhabilitado</span>
                                                    <?php
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <a class="btn btn-info btn-xs" onclick="mostrarUsuario('<?php echo $r->__GET('Usuario_Id'); ?>');" title="Ver detalles" data-toggle="modal" data-target="#modal-ver"><i class="fa fa-eye"></i></a>
                                                <a class="btn btn-primary btn-xs" title="Editar" onclick="FrmEditarUsuario('<?php echo $r->__GET('Usuario_Id'); ?>');"><i class="fa fa-pencil"></i></a>
                                                <button type="button" class="btn btn-danger btn-xs" title="Eliminar" data-toggle="modal" data-target="#modal-eliminar" data-codigo="<?php echo $r->__GET('Usuario_Id'); ?>"><i class="fa fa-trash-o"></i></button>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Login</th>
                                        <th>Tipo</th>
                                        <th>Estado</th>
                                        <th>Operaciones</th>
                                    </tr>
                                </tfoot>
                            </table>

                            <!-- Modal modeal-ver -->
                            <div class="modal fade" id="modal-ver" tabindex="-1" role="dialog" aria-labelledby="modal-ver-label" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form action="?c=Usuario&a=reporteUsuarioDatos" method="POST">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                                <h4 class="modal-title" id="modal-ver-label">Registro de Usuario</h4>
                                            </div>
                                            <div class="modal-body">
                                                <table class="table table-striped" id="ver_datos">

                                                </table>
                                            </div>
                                            <div class="modal-footer">
                                                <!--<button type="submit" class="btn btn-danger" formtarget="_blak"><i class="fa fa-print"></i>&nbsp;Imprimir</button>-->
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- /Modal modal-ver -->

                            <!-- Modal modal-eliminar -->
                            <div class="modal fade" id="modal-eliminar" tabindex="-1" role="dialog" aria-labelledby="modal-eliminar-label" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form action="?c=Usuario&a=EliminarUsuario" method="POST">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                                <h4 class="modal-title" id="modal-eliminar-label">Eliminar</h4>
                                            </div>
                                            <div class="modal-body">
                                                <input type="hidden" name="idUsuario" id="idUsuario" value="">
                                                ¿Estás seguro que desea eliminar?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary">Si, eliminar</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">No, cancelar</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- /Modal modal-eliminar -->

                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </section><!-- /.content -->
        <script type="text/javascript" src="view/librerias/dist/js/funcionesUsuario.js"></script>
        <script type="text/javascript">
            $(function() {
                jQuery.fn.dataTableExt.oSort['fecha-asc'] = function(a, b) {
                    var ukDatea = a.split('-');
                    var ukDateb = b.split('-');

                    var x = (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
                    var y = (ukDateb[2] + ukDateb[1] + ukDateb[0]) * 1;

                    return ((x < y) ? -1 : ((x > y) ? 1 : 0));
                };

                jQuery.fn.dataTableExt.oSort['fecha-desc'] = function(a, b) {
                    var ukDatea = a.split('-');
                    var ukDateb = b.split('-');

                    var x = (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
                    var y = (ukDateb[2] + ukDateb[1] + ukDateb[0]) * 1;

                    return ((x < y) ? 1 : ((x > y) ? -1 : 0));
                };
                $("#example1").dataTable({
                    "scrollX": true,
                    "bSort": false,
                    "language": {
                        "search": "Buscar",
                        "lengthMenu": "Visualizar _MENU_ registro por página",
                        "zeroRecords": "No hay información para mostrar",
                        "info": "Pagina _PAGE_ de _PAGES_ de _MAX_ ",
                        "infoEmpty": "Pagina _PAGE_ - _PAGES_ de _MAX_ registros",
                        "infoFiltered": "(filtered from _MAX_ total records)",
                        "paginate": {
                            "previous": "Anterior",
                            "next": "Siguiente"
                        }
                    }
                });
            });
            $('#modal-eliminar').on('show.bs.modal', function(event) {
                var button = $(event.relatedTarget); // Button that triggered the modal
                var recipient = button.data('codigo'); // Extract info from data-* attributes

                // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this);
                modal.find('.modal-body input').val(recipient);
            });
        </script>
