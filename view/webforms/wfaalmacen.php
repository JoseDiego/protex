<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Almacen
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Almacen</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12" id="resultadoPasajero">

        </div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h4>Relacion completa de almacen</h4>                    
                </div>
                <div class="box-body">
                    <div class="form-group">                        
                        <button class="btn btn-primary btn-flat" data-toggle="modal" data-target="#Agregar">Nuevo Almacén</button>
                    </div>
                    <table id="example1" class="table table-bordered table-striped text-center">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Ubicacion</th>
                                <th>Estado</th>
                                <th>Operaciones</th>
                            </tr>
                        </thead>                  	
                        <tbody>  
                            <?php 
                            foreach ($almacen as $r){
                            ?>
                            <tr>
                                <td><?php echo $r->__GET('sAlmNombre'); ?></td>
                                <td><?php echo $r->__GET('sAlmUbicacion'); ?></td>
                                <td><?php echo $r->__GET('nAlmEstado') == 1 ? '<span class="label label-success">Habilitado</span>':'<span class="label label-danger">Deshabilitado</span>'; ?></td>
                                <td>
                                    <a href="#" class="btn btn-primary btn-flat btn-xs" data-toggle="modal" data-target="#Mostrar" onclick="MostrarAlmacen('<?php echo $r->__GET('Almacen_Id'); ?>');"><i class="glyphicon glyphicon-eye-open"></i></a>
                                    <a href="#" class="btn btn-success btn-flat btn-xs" onclick="FrmEditarAlmacen('<?php echo $r->__GET('Almacen_Id'); ?>');"><i class="fa fa-pencil"></i></a>
                                    <a href="#" class="btn btn-danger btn-flat btn-xs" data-toggle="modal" data-target="#Eliminar" data-codigo="<?php echo $r->__GET('Almacen_Id'); ?>"><i class="fa fa-trash-o"></i></a>
                                </td>
                            </tr>
                            <?php 
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Nombre</th>
                                <th>Ubicacion</th>
                                <th>Estado</th>
                                <th>Operaciones</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="Mostrar">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="?c=Almacen&a=EliminarAlmacen" method="POST">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Mostrar Almacen</h4>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered" id="ver_datos">
                        <tbody>
                            <tr>
                                <td>Cargando datos...</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Cerrar</button>
                    <!--<button type="submit" class="btn btn-danger btn-flat">Si, Eliminar</button>-->
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="Eliminar">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="?c=Almacen&a=EliminarAlmacen" method="POST">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Eliminar</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="idAlmacen" name="idAlmacen">
                    <p>¿Estás seguro que desea eliminar?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">No, Cancelar</button>
                    <button type="submit" class="btn btn-danger btn-flat">Si, Eliminar</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- Modal modal-eliminar -->
<div class="modal fade" id="Agregar" tabindex="-1" role="dialog" aria-labelledby="modal-eliminar-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="javascript:GuardarAlmacen();" method="POST">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                    <h4 class="modal-title" id="modal-eliminar-label">Agregar Almacen</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">                            
                            <div id="resultado"></div>
                        </div>
                        <div class="col-md-12 form-group">
                            <input type="hidden" name="Almacen_Id" id="Almacen_Id">
                            <label>Nombre</label>
                            <input type="text" class="form-control" name="sAlmNombre" id="sAlmNombre" placeholder="Nombre" required="required">
                        </div>
                        <div class="col-md-12 form-group">
                            <label>Ubicacion</label>
                            <input type="text" class="form-control" name="sAlmUbicacion" id="sAlmUbicacion" placeholder="Ubicación" required="required">
                        </div>
                        <div class="col-md-12 form-group">
                            <label>Estado</label>
                            <select class="form-control" name="nAlmEstado" id="nAlmEstado" required="required">
                                <option value="">Seleccionar estado</option>
                                <option value="1">Habilitado</option>
                                <option value="0">Deshabilitado</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-flat">Si, Agregar</button>
                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">No, cancelar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /Modal modal-eliminar -->
<script type="text/javascript" src="view/librerias/dist/js/funcionesAlmacen.js"></script>
<script type="text/javascript">
    $(function() {
        jQuery.fn.dataTableExt.oSort['fecha-asc'] = function(a, b) {
            var ukDatea = a.split('-');
            var ukDateb = b.split('-');

            var x = (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
            var y = (ukDateb[2] + ukDateb[1] + ukDateb[0]) * 1;

            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        };

        jQuery.fn.dataTableExt.oSort['fecha-desc'] = function(a, b) {
            var ukDatea = a.split('-');
            var ukDateb = b.split('-');

            var x = (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
            var y = (ukDateb[2] + ukDateb[1] + ukDateb[0]) * 1;

            return ((x < y) ? 1 : ((x > y) ? -1 : 0));
        };
        $("#example1").dataTable({
            "scrollX": true,
            "bSort": false,
            "language": {
                "search": "Buscar",
                "lengthMenu": "Visualizar _MENU_ registro por página",
                "zeroRecords": "No hay información para mostrar",
                "info": "Pagina _PAGE_ de _PAGES_ de _MAX_ ",
                "infoEmpty": "Pagina _PAGE_ - _PAGES_ de _MAX_ registros",
                "infoFiltered": "(filtered from _MAX_ total records)",
                "paginate": {
                    "previous": "Anterior",
                    "next": "Siguiente"
                }
            }
        });
    });
    $('#Eliminar').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var recipient = button.data('codigo'); // Extract info from data-* attributes

        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this);
        modal.find('.modal-body input').val(recipient);
    });
</script>

