
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Pagos
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="wfamain.php"><i class="fa fa-dashboard"></i> Inicio</a></li>
                <li><a href="#">Pagos</a></li>
            </ol>
        </section>        
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Relación completa de Pagos</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <a class="btn btn-danger btn-flat" href="?c=Creditos&a=Index">REGRESAR</a>
                                </div>
                                <div class="col-md-12">                                    
                                    <table id="example1" class="table table-bordered table-striped text-center">
                                        <thead>
                                            <tr>
                                                <th>Fecha</th>
                                                <th>Tipo Pago</th>
                                                <th>Documento</th>
                                                <th>Importe</th>
                                                <th>Operaciones</th>
                                            </tr>
                                        </thead>                  	
                                        <tbody>  
                                            <?php 
                                            foreach ($pagos as $r){
                                            ?>
                                            <tr>
                                                <td><?php echo $r->__GET('dPagFecha'); ?></td>
                                                <td><?php echo $this->TipoPago($r->__GET('nPagTipoPago_Id')); ?></td>
                                                <td><?php echo $r->__GET('sPagDocumento'); ?></td>
                                                <td><?php echo $r->__GET('nPagImporte'); ?></td>
                                                <td>
                                                    <a href="#" class="btn btn-danger btn-flat btn-xs" data-toggle="modal" data-target="#modal-eliminar" data-codigo="<?php echo $r->__GET('Pago_Id'); ?>"><i class="fa fa-trash-o"></i></a>
                                                </td>
                                            </tr>
                                            <?php 
                                            }
                                            ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Fecha</th>
                                                <th>Tipo Pago</th>
                                                <th>Documento</th>
                                                <th>Importe</th>
                                                <th>Operaciones</th>
                                            </tr>
                                        </tfoot>
                                    </table>                                    
                                </div>
                                
                            </div>

                            <!-- Modal modeal-ver -->
                            <div class="modal fade" id="modal-ver" tabindex="-1" role="dialog" aria-labelledby="modal-ver-label" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form action="?c=Usuario&a=reporteUsuarioDatos" method="POST">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                                <h4 class="modal-title" id="modal-ver-label">Registro de Usuario</h4>
                                            </div>
                                            <div class="modal-body">
                                                <table class="table table-striped" id="ver_datos">

                                                </table>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-danger" formtarget="_blak"><i class="fa fa-print"></i>&nbsp;Imprimir</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- /Modal modal-ver -->

                            <!-- Modal modal-eliminar -->
                            <div class="modal fade" id="modal-eliminar" tabindex="-1" role="dialog" aria-labelledby="modal-eliminar-label" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form action="?c=Pago&a=EliminarPago" method="POST">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                                <h4 class="modal-title" id="modal-eliminar-label">Eliminar</h4>
                                            </div>
                                            <div class="modal-body">
                                                <input type="hidden" name="IdPago" id="IdPago" value="">
                                                ¿Estás seguro que desea eliminar?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">No, cancelar</button>
                                                <button type="submit" class="btn btn-danger btn-flat">Si, eliminar</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- /Modal modal-eliminar -->

                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </section><!-- /.content -->
        <script type="text/javascript" src="view/librerias/dist/js/funcionesMovimiento.js"></script>
        <script type="text/javascript">
            $(function() {
                jQuery.fn.dataTableExt.oSort['fecha-asc'] = function(a, b) {
                    var ukDatea = a.split('-');
                    var ukDateb = b.split('-');

                    var x = (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
                    var y = (ukDateb[2] + ukDateb[1] + ukDateb[0]) * 1;

                    return ((x < y) ? -1 : ((x > y) ? 1 : 0));
                };

                jQuery.fn.dataTableExt.oSort['fecha-desc'] = function(a, b) {
                    var ukDatea = a.split('-');
                    var ukDateb = b.split('-');

                    var x = (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
                    var y = (ukDateb[2] + ukDateb[1] + ukDateb[0]) * 1;

                    return ((x < y) ? 1 : ((x > y) ? -1 : 0));
                };
                $("#example1").dataTable({
                    "scrollX": true,
                    "bSort": false,
                    "language": {
                        "search": "Buscar",
                        "lengthMenu": "Visualizar _MENU_ registro por página",
                        "zeroRecords": "No hay información para mostrar",
                        "info": "Pagina _PAGE_ de _PAGES_ de _MAX_ ",
                        "infoEmpty": "Pagina _PAGE_ - _PAGES_ de _MAX_ registros",
                        "infoFiltered": "(filtered from _MAX_ total records)",
                        "paginate": {
                            "previous": "Anterior",
                            "next": "Siguiente"
                        }
                    }
                });
            });
            $('#modal-eliminar').on('show.bs.modal', function(event) {
                var button = $(event.relatedTarget); // Button that triggered the modal
                var recipient = button.data('codigo'); // Extract info from data-* attributes

                // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this);
                modal.find('.modal-body input').val(recipient);
            });
        </script>
