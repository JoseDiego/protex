<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Clasificación
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Clasificación</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12" id="resultadoPasajero">

        </div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h4>Relacion completa de clasificación</h4>                    
                </div>
                <div class="box-body">
                    <div class="form-group">                        
                        <button class="btn btn-primary btn-flat" data-toggle="modal" data-target="#modal-agregar">Nueva clasificación</button>
                    </div>
                    <table id="example1" class="table table-bordered table-striped text-center">
                        <thead>
                            <tr>
                                <th>Descripcion</th>
                                <th>Estado</th>
                                <th>Operaciones</th>
                            </tr>
                        </thead>                  	
                        <tbody>  

                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Descripcion</th>
                                <th>Estado</th>
                                <th>Operaciones</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Modal modal-eliminar -->
<div class="modal fade" id="modal-agregar" tabindex="-1" role="dialog" aria-labelledby="modal-eliminar-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="POST">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                    <h4 class="modal-title" id="modal-eliminar-label">Agregar Clasificación</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label>Descripcion de Clasificación</label>
                            <textarea class="form-control" name="" id="" placeholder="Descripción"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-flat">Si, Agregar</button>
                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">No, cancelar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /Modal modal-eliminar -->
<script type="text/javascript" src="view/librerias/dist/js/funcionesUsuario.js"></script>
<script type="text/javascript">
    $(function() {
        jQuery.fn.dataTableExt.oSort['fecha-asc'] = function(a, b) {
            var ukDatea = a.split('-');
            var ukDateb = b.split('-');

            var x = (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
            var y = (ukDateb[2] + ukDateb[1] + ukDateb[0]) * 1;

            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        };

        jQuery.fn.dataTableExt.oSort['fecha-desc'] = function(a, b) {
            var ukDatea = a.split('-');
            var ukDateb = b.split('-');

            var x = (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
            var y = (ukDateb[2] + ukDateb[1] + ukDateb[0]) * 1;

            return ((x < y) ? 1 : ((x > y) ? -1 : 0));
        };
        $("#example1").dataTable({
            "scrollX": true,
            "bSort": false,
            "language": {
                "search": "Buscar",
                "lengthMenu": "Visualizar _MENU_ registro por página",
                "zeroRecords": "No hay información para mostrar",
                "info": "Pagina _PAGE_ de _PAGES_ de _MAX_ ",
                "infoEmpty": "Pagina _PAGE_ - _PAGES_ de _MAX_ registros",
                "infoFiltered": "(filtered from _MAX_ total records)",
                "paginate": {
                    "previous": "Anterior",
                    "next": "Siguiente"
                }
            }   
        });
    });
    $('#modal-eliminar').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var recipient = button.data('codigo'); // Extract info from data-* attributes

        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this);
        modal.find('.modal-body input').val(recipient);
    });
</script>

