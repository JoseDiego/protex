<style type="text/css">
    .modal-open, .modal-open .navbar-fixed-top, .modal-open .navbar-fixed-bottom {
        padding-right: 0px !important;
    }
    .scroll-box{
        margin-top: 40px;
        height: 100%;       
        border: 1px solid #ddd;     
        overflow-y: scroll;     
    }   
    .scroll-content{
        height: auto;
    }
    .modal-body {
        max-height: calc(100vh - 210px);
        overflow-y: auto;
    }
</style>
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="view/librerias/plugins/iCheck/all.css">
<!-- Content Header (Page header) -->
<!--        <section class="content-header">
    <h1>
        Ventas
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="wfamain.php"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#">Ventas</a></li>
    </ol>
</section>        -->
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body no-padding">
                    <div class="row">
                        <div class="col-md-3" id='lista_pedidos'>
                            <!-- small box -->
                            <div class="small-box bg-primary no-padding no-margin">
                                <div class="inner">
                                    <h3>
                                        <?php 
                                        $o=0;
                                        foreach ($pedidos as $cont){
                                            $o++;
                                        }
                                        echo $o;
                                        ?>
                                        
                                    </h3>
                                    <p>PEDIDOS</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-shopping-cart"></i>
                                </div>
                                <?php 
                                $i=0;
                                foreach ($pedidos as $r){
                                ?>                               
                                <button type="button" class="close btn_eliminar_pedido" aria-label="Close" data-pedidocodigo='<?php echo $r->__GET('Pedido_Id'); ?>' data-toggle="modal" data-target="#eliminar_pedido"><span aria-hidden="true">&times;</span></button>                
                                <a href="#" class="small-box-footer text-bold" style="border-bottom: 1px solid #0770BB; font-size: 25px; padding-top: 13px; padding-bottom: 12px;" onclick="EditarPedido('<?php echo $r->__GET('Pedido_Id'); ?>');">                                    
                                    <i class="fa fa-shopping-cart"></i>&nbsp;
                                    <p class="vnombre_cliente"><?php echo $this->NombreClienteProveedor($r->__GET('OrigenDestino_Id')); /*substr($this->NombreClienteProveedor($r->__GET('OrigenDestino_Id')), 0, 8);*/ ?></p>
                                </a>
                                <?php                                 
                                    $i++;
                                }
                                $total = 10 - $i;
                                for($j=1;$j<=$total;$j++){
                                    if($j==1){
                                ?>
                                <a href="#" class="text-bold btn btn-success" onclick="NuevoPedido();" style="width: 100%; padding-top: 20px; padding-bottom: 19px;">
                                    <i class="fa fa-plus"></i>
                                </a>
                                <?php 
                                    }else{
                                ?>                                
                                <a class="small-box-footer table-bordered text-bold" style="background: #F9F9F9; color: #999999">
                                    ?
                                </a>
                                <?php
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <form action="javascript:GuardarPedido();" method="POST" id="FrmPedido">
                                <div class="col-md-12 form-group">
                                    <h3>Venta</h3>
                                </div>
                                <div class="col-md-12">                                    
                                    <div id="resultado"></div>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6 form-group">    
                                            <input type="hidden" id="Pedido_Id" name="Pedido_Id" value="0">
                                            <label>CLIENTE</label>
                                            <select class="form-control input-lg" name="OrigenDestino_Id" id="OrigenDestino_Id" disabled>
                                                <option value="">Seleccionar cliente</option>
                                                <?php
                                                foreach ($this->origendestino_model->Listar() as $o):
                                                    if ($o->__GET('nODEstado') != 0 && $o->__GET('nODTipo') == 2) {
                                                        ?>
                                                        <option value="<?php echo $o->__GET('OrigenDestino_Id'); ?>"><?php echo $o->__GET('sODNombre'); ?></option>
                                                        <?php
                                                    }
                                                endforeach;
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>FECHA</label>
                                            <input type="text" class="form-control  input-lg datetimepicker" name="dPedFecha" id="dPedFecha" placeholder="DD-MM-YYYY" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6 form-group">
                                            <label>TIPO DE PAGO</label>
                                            <select class="form-control input-lg" name="nPedTipopago" id="nPedTipopago" disabled>
                                                <option value="1">Contado</option>
                                                <option value="2">Credito</option>                                        
                                            </select>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>TIPO DE DOCUMENTO</label>
                                            <select class="form-control input-lg" name="Documento_Id" id="Documento_Id" disabled onchange="CalcularImplIGVTot();">

                                            </select>
                                            <!--onchange="NumeroDocumento();"--> 
                                            <input type="hidden" name="sMovDocumento" id="sMovDocumento" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 form-group">
                                    <table class="table table-bordered table-striped text-center tablesaw" data-tablesaw-mode="swipe">
                                        <thead>
                                            <tr>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="1">PRODUCTO</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2">CANTIDAD</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">UNIDAD</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">PRECIO</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="5">SUB TOTAL</th>
                                                <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">OPERACIONES</th>
                                            </tr>                                        
                                            <tr>
                                                <td>-</td>
                                                <td>-</td>
                                                <td>-</td>
                                                <td>-</td>
                                                <td>-</td>
                                                <td>
                                                    <button type="button" class="btn btn-danger btn-sm btn-agregarproserv" data-toggle="modal" data-target="#familias" disabled><i class="fa fa-cart-plus"></i></button>
                                                </td>
                                            </tr>
                                        </thead>
                                        <tbody id="productos_servicios">
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-4 form-group">
                                    <label>Importe</label>
                                    <input type="text" class="form-control input-lg" name="dPedImporte" id="dPedImporte" placeholder="0.00" value="" disabled>
                                </div>
                                <div class="col-md-4 form-group">
                                    <label>IGV</label>
                                    <input type="text" class="form-control input-lg" name="dPedIGV" id="dPedIGV" placeholder="0.00" value="" disabled>
                                </div>
                                <div class="col-md-4 form-group">
                                    <label>TOTAL</label>
                                    <input type="text" class="form-control input-lg" name="dPedTotal" id="dPedTotal" placeholder="0.00" value="" disabled>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Observaciónes</label>
                                        <textarea class="form-control input-lg" name="sPedObservaciones" id="sPedObservaciones" placeholder="Observaciones" rows="3" disabled></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6" style="margin-bottom: 15px;">
                                    <button type="button" class="btn btn-danger btn-flat btn-lg text-bold btn-cancelar" id="boton_cancelar" style="width: 100%;" onclick="CancelarPedido();" disabled><i class="fa fa-ban"></i>&nbsp;&nbsp;CANCELAR</button>
                                    <button type="button" class="btn btn-danger btn-flat btn-lg text-bold btn-cancelar" id="boton_cerrar" style="width: 100%; display: none;"  data-toggle="modal" data-target="#cerrar_pedido">&nbsp;&nbsp;CERRAR PEDIDO</button>
                                </div>
                                <div class="col-md-6" style="margin-bottom: 15px;">
                                    <button type="submit" class="btn btn-primary btn-flat btn-lg text-bold btn-guardar" id='GuardarInformacionPedido' style="width: 100%;" disabled><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;GUARDAR INFORMACION</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- /.box -->

        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<div class="modal fade" id="familias" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Familia de productos</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 form-group">
                        <input type="text" class="form-control input-lg" name="txtNameProduct" id="txtNameProduct" placeholder="Nombre de producto" onkeyup="BusquedaProducto();">
                    </div>                    
                </div>
                <div class="row" id="productos_filtro">
                    <?php
                    foreach ($familia as $f) {
                        ?>
                        <div class="col-md-4 margin-bottom">
                            <button class="btn <?php echo $f->__GET('nFamEstado') == 0 ? 'btn-danger disabled' : 'btn-warning'; ?> btn-flat btn-producto" style="width: 100%;" onclick="FiltrarProductos('<?php echo $f->__GET('Familia_Id'); ?>');" data-dismiss="modal" data-toggle="modal" data-target="#productos">
                                <i class="fa fa-info-circle"></i><br>
                                <p style="font-size: 14px;"><?php echo $f->__GET('sFamDescripcion'); ?></p>
                            </button>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<div class="modal fade" id="eliminar_pedido" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="?c=Pedido&a=EliminarPedido" method="POST">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Eliminar</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="IdPedido" id="IdPedido"> 
                   <h4>¿Esta seguro que desea eliminar el pedido?</h4>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-6">                                
                            <button type="button" class="btn btn-default btn-flat btn-lg" style="width: 100%;" data-dismiss="modal">No, Cancelar</button>
                        </div>
                        <div class="col-md-6">                                
                            <button type="submit" class="btn btn-danger btn-flat btn-lg" style="width: 100%;">Si, Eliminar</button>
                        </div>
                    </div>                       
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<div class="modal fade" id="cerrar_pedido" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="javascript:CerrarPedido();" method="POST">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Cerrar Pedido</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="Id_Pedido" id="Id_Pedido"> 
                   <h4>¿Esta seguro que desea cerrar el pedido?</h4>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-6">                                
                            <button type="button" class="btn btn-default btn-flat btn-lg" style="width: 100%;" data-dismiss="modal">No, Cancelar</button>
                        </div>
                        <div class="col-md-6">                                
                            <button type="submit" class="btn btn-danger btn-flat btn-lg" style="width: 100%;">Si, Cerrar</button>
                        </div>
                    </div>                       
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<div class="modal fade" id="productos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Lista de productos</h4>
            </div>
            <div class="modal-body" id="resultado_productos">

            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-12">                                
                        <button type="button" class="btn btn-default btn-flat btn-lg" style="width: 100%;" data-dismiss="modal" data-toggle="modal" data-target="#familias">REGRESAR</button>
                    </div>
                </div>                              
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<div class="modal fade" id="productos_detalle" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <form action="javascript:AgregarProdVent();" method="POST" id='FrmDetalleProducto'>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Detalle de producto</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12" id="mensaje_stock"></div>
                        <div class="col-md-12 form-group">
                            <input type="hidden" name="ProdServ_Id" id="ProdServ_Id">
                            <label>CANTIDAD</label>
                            <input class="form-control input-lg"
                                   id="nPedDetCantidad"
                                   name="nPedDetCantidad"
                                   type="text"
                                   data-bts-min="1"
                                   data-bts-max="100"
                                   data-bts-init-val="1"
                                   data-bts-step="1"
                                   data-bts-decimal="1"
                                   data-bts-step-interval="100"
                                   data-bts-button-down-class="btn btn-default btn-lg"
                                   data-bts-button-up-class="btn btn-default btn-lg"
                                   value="1"
                                   />
                        </div>
                        <div class="col-md-12 form-group">
                            <label>UNIDAD</label>
                            <select class="form-control input-lg" id="Unidadmedida_Id" name="Unidadmedida_Id" onchange="PrecioProducto();">
                            </select>
                        </div>
                        <div class="col-md-12 form-group">
                            <label>PRECIO</label>
                            <input type="text" class="form-control input-lg" name="dPedDetPrecioUnitario" id="dPedDetPrecioUnitario" placeholder="0.00">
                        </div>
<!--                        <div class="col-md-12 text-center">
                            <input type="checkbox" name="nIncluyeVentIGV" id="nIncluyeVentIGV" checked>&nbsp;<b>Incluido IGV</b>
                        </div>-->
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-12">                                
                            <button type="submit" class="btn btn-danger btn-flat btn-lg" style="width: 100%;" id="Seleccionar_Producto">SELECCIONAR</button>
                        </div>
                        <div class="col-md-12">                                
                            <button type="button" class="btn btn-default btn-flat btn-lg" data-dismiss="modal" data-toggle="modal" data-target="#productos" style="width: 100%;">REGRESAR</button>
                        </div>
                    </div>                        
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!--<script type="text/javascript" src="view/librerias/dist/js/funcionesVentas.js"></script>-->
<!-- iCheck 1.0.1 -->
<script src="view/librerias/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
<script src="view/librerias/dist/js/funcionesGenerales.js"></script>
<script src="view/librerias/dist/js/funcionesMovimiento.js"></script>
<script src="view/librerias/dist/js/funcionesVentas.js"></script>
<script type="text/javascript" >
    /*NOMBRE DE VENTANAS MODAL 
    - familias
    - productos
    - productos_detalle*/
    $("input[name='nPedDetCantidad']").TouchSpin({});
    
    $(document).ready(function () {
        Tipomovimiento_venta();        
    });
    $('.datetimepicker').datetimepicker({
        viewMode: 'days',
        format: 'DD-MM-YYYY'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
        checkboxClass: 'icheckbox_minimal-red',
        radioClass: 'iradio_minimal-red'
    });    
    $('#eliminar_pedido').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var recipient = button.data('pedidocodigo'); // Extract info from data-* attributes

        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this);
        modal.find('.modal-body input').val(recipient);
    });
    $('#cerrar_pedido').on('show.bs.modal', function(event) {
        var recipient = document.getElementById('Pedido_Id').value; // Extract info from data-* attributes

        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this);
        modal.find('.modal-body input').val(recipient);
    });
</script>