<style type="text/css">
    img { border: none; }
    input, select, textarea, th, td { font-size: 1em; }

    /* CSS Tree menu styles */
    ol.tree
    {
        padding: 0 0 0 30px;
        width: 100%;
    }
    li 
    { 
        position: relative; 
        margin-left: -15px;
        list-style: none;
    }
    li.file
    {
        margin-left: -1px !important;
    }
    li.file a
    {
        background: url(view/librerias/dist/img/document.png) 0 0 no-repeat;
        color: #fff;
        padding-left: 21px;
        text-decoration: none;
        display: block;
    }
    li.file a[href *= '.pdf']	{ background: url(view/librerias/dist/img/document.png) 0 0 no-repeat; }
    li.file a[href *= '.html']	{ background: url(view/librerias/dist/img/document.png) 0 0 no-repeat; }
    li.file a[href $= '.css']	{ background: url(view/librerias/dist/img/document.png) 0 0 no-repeat; }
    li.file a[href $= '.js']    { background: url(view/librerias/dist/img/document.png) 0 0 no-repeat; }
    li input
    {
        position: absolute;
        left: 0;
        margin-left: 0;
        opacity: 0;
        z-index: 2;
        cursor: pointer;
        height: 1em;
        width: 1em;
        top: 0;
    }
    li input + ol
    {
        background: url(view/librerias/dist/img/toggle-small-expand.png) 40px 3px no-repeat;
        margin: -1.5em 0 0 -44px; /* 15px */
        height: 1.5em;
    }
    li input + ol > li { display: none; margin-left: -14px !important; padding-left: 1px; }
    li label
    {
        background: url(view/librerias/dist/img/folder-horizontal.png) 15px 1px no-repeat;
        cursor: pointer;
        display: block;
        padding-left: 37px;
    }
    li label{
        margin-bottom: 0px;
    }

    li input:checked + ol
    {
        background: url(view/librerias/dist/img/toggle-small.png) 40px -1px no-repeat;
        margin: -1.25em 0 0 -44px; /* 20px */
        padding: 1.563em 0 0 80px;
        height: auto;
    }
    li input:checked + ol > li { display: block; margin: 0 0 0.125em;  /* 2px */}
    li input:checked + ol > li:last-child { margin: 0 0 0.063em; /* 1px */ }
    li label a{
        text-decoration: none;
        color: #3f3f3f;
    }
    li label a:hover{
        color: #5e5d5d;
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Familias de productos
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Registrar Familias de productos</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12" id="resultadoPasajero">

        </div>
        <!-- left column -->        
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header"><h4>Familias</h4></div>
                <div class="box-body" id="arbol_contenido">
                    <?php 
                    $ex = array();
                    foreach ($familia as $arb){
                        $ex[] = array('id' => $arb->__GET('Familia_Id'),
                                      'codigo' => $arb->__GET('sFamCodigo'),
                                      'nombre' => $arb->__GET('sFamDescripcion'),
                                      'parentId' => $arb->__GET('nFamPadre'));
                    }
                    ?>
                    <ol class="tree">
                        <?php
                        $arbol_lista = "";
                        for($j=0;$j<count($ex);$j++){
                            if($ex[$j]['parentId'] == 0){
                                $arbol_lista .= '<li>
                                                    <label>'.$ex[$j]['codigo'].'- '.$ex[$j]['nombre'].'</label>
                                                    <input type="checkbox" checked id="folder1" />'
                                                    .Arbol($ex,$ex[$j]['id']).'
                                                </li>';
                            }  
                        }
                        echo $arbol_lista;
                        ?>
                    </ol>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-primary">
                <form action="" method="POST">
                    <div class="box-header"><h4>Informacion basica</h4></div>
                    <div class="box-body">
                            <div id="resultado"></div>
                            <div class="row" id="Frm_Familia">
                                <?php 
                                require_once 'view/webforms/wfafamiliasa.php';
                                ?>
                            </div>  
                    </div>
                    <div class="box-footer">
                        <div class="col-md-6">               
                        <button class="btn btn-primary btn-flat btn-block" onclick="javascript: form.action='javascript:GuardarFamilia();'">Guardar informacion</button>
                        </div>
                        <div class="col-md-6">
                        <button class="btn btn-danger btn-flat btn-block"  onclick="javascript: form.action='javascript:LimpiarFrm();';">Cancelar</button>
                        </div>
                    </div> 
                </form>
                    <div class="box-footer">
                        <div class="col-md-12"></br>
                        <p><button class="btn btn-warning btn-flat btn-block"  style="display: none;" id="Eliminar" data-toggle="modal" data-target="#modal-eliminar-familia">Eliminar</button></p>
                       </div>
                    </div>
                </br>
                </br>
                </br>
                </br>
            </div>
        </div>

        <div class="modal fade" id="modal-eliminar-familia" tabindex="-1" role="dialog" aria-labelledby="modal-eliminar-label" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                            <h4 class="modal-title" id="modal-eliminar-label">Eliminar</h4>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" name="IdPrecioVenta" id="IdPrecioVenta" value="">
                            ¿Estás seguro que desea eliminar?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Cerrarr</button>
                            <button type="button" class="btn btn-danger btn-flat" onclick="EliminarFamilia();" id="btnEliminar">Si, eliminar</button>
                        </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript" src="view/librerias/dist/js/funcionesFamilia.js"></script>

