            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Tipo de documento
                    <small></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
                    <li class="active">Registrar Tipo de documento</li>
                </ol>
            </section>
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-md-12" id="resultadoPasajero">
                        
                    </div>
                    <!-- left column -->
                    <div class="col-md-6">
                        <div class="box box-primary">
                            <div class="box-header">
                                <h3 class="box-title">Datos básicos</h3>
                            </div><!-- /.box-header -->
                            <!-- form start -->
                            <form role="form" action="javascript:GuardarDocumento();" method="POST" id="FrmUsuario">
                                <div class="box-body">                                    
                                    <div class="row">
<!--                                        <div class="col-md-12 form-group">
                                             <label>Empresa</label>
                                            <input type="text" class="form-control" name="" id="" placeholder="Empresa">                                                
                                        </div>-->
                                        <div class="col-md-12 form-group">
                                            <div id="resultado"></div>
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <input type="hidden" name="Documento_Id" id="Documento_Id" value="<?php echo isset($r) ? $r->__GET('Documento_Id') : '0'; ?>" required="required">
                                            <label>Tipo de Movimiento</label>
                                            <select class="form-control" name="Tipomovimiento_Id" id="Tipomovimiento_Id">
                                                <option>Seleccione movimiento</option>
                                                <?php 
                                                foreach ($tipomovimiento as $mov){ 
                                                ?>
                                                <option value="<?php echo $mov->__GET('TipoMovimiento_Id'); ?>" <?php echo isset($r) ? $r->__GET('nDocTipoMovimiento_Id') == $mov->__GET('TipoMovimiento_Id') ? 'selected':'' : ''; ?>><?php echo $mov->__GET('sTMovNombre'); ?></option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <label>Almacen</label>
                                            <select class="form-control" name="Almacen_Id" id="Almacen_Id">
                                                <option>Seleccione almacen</option>
                                                <?php 
                                                foreach ($almacen as $alm){ 
                                                ?>
                                                <option value="<?php echo $alm->__GET('Almacen_Id'); ?>" <?php echo isset($r) ? $r->__GET('Almacen_Id') == $alm->__GET('Almacen_Id') ? 'selected':'' : ''; ?>><?php echo $alm->__GET('sAlmNombre'); ?></option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <label>Nombre</label>
                                            <input type="text" class="form-control" name="sDocNombre" id="sDocNombre" placeholder="Nombre" value="<?php echo isset($r) ? $r->__GET('sDocNombre'):''; ?>" required="required">
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <label>Nombre corto</label>
                                            <input type="text" class="form-control" name="sDocNombreCorto" id="sDocNombreCorto" placeholder="Nombre corto" value="<?php echo isset($r) ? $r->__GET('sDocNombreCorto'):''; ?>" required="required">
                                        </div>     
                                        <div class="col-md-6 form-group">
                                            <label>Automatico</label>
                                            <select class="form-control" name="nDocNomAutomatico" id="nDocNomAutomatico">
                                                <option>Seleccione</option>
                                                <option value="1" <?php echo isset($r) ? $r->__GET('nDocNomAutomatico') == 1 ? 'selected': '' : ''; ?>>Si</option>
                                                <option value="0" <?php echo isset($r) ? $r->__GET('nDocNomAutomatico') == 0 ? 'selected': '' : ''; ?>>No</option>
                                            </select>
                                        </div>   
                                        <div class="col-md-6 form-group">
                                            <label>Número siguiente</label>
                                            <input type="text" class="form-control" name="sDocSiguiente" id="sDocSiguiente" placeholder="Numero siguiente" value="<?php echo isset($r) ? $r->__GET('sDocSiguiente'):''; ?>" required="required">
                                        </div>                              
                                        <div class="col-md-12 form-group">
                                            <label>Estado</label>
                                            <select class="form-control" name="nDocEstado" id="nDocEstado">
                                                <option <?php if(isset($r)){ echo $r->__GET('nDocEstado') == 1 ? 'selected' : ''; } ?> value="1">Habilitado</option>
                                                <option <?php if(isset($r)){ echo $r->__GET('nDocEstado') == 0 ? 'selected' : ''; } ?> value="0">Deshabilitado</option>
                                            </select>
                                        </div>
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <label>Afecto a IGV &nbsp;&nbsp;<input type="checkbox" name="nDocAfectoIGV" id="nDocAfectoIGV" <?php echo isset($r) ? $r->__GET('nDocAfectoIGV') == 1 ? 'checked':'' :''; ?>></label>&nbsp;&nbsp;
                                        <button type="submit" class="btn btn-primary btn-flat" data-toggle="tooltip" title="Guardar nuevo viajero">
                                        Guardar informacion
                                        </button>
                                        <a href="?c=Tipodocumento&a=Index" class="btn btn-danger">Cancelar</a>
                                    </div>               
                                    </div>
                            </form>
                        </div><!-- /.box -->
                    </div><!--/.col (left) -->                    
                </div>   <!-- /.row -->
            </section><!-- /.content -->
            <script type="text/javascript" src="view/librerias/dist/js/funcionesTipodocumento.js"></script>
            <script type="text/javascript" src="view/librerias/dist/js/funcionesGenerales.js"></script>