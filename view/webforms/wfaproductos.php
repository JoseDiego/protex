
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Productos
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="wfamain.php"><i class="fa fa-dashboard"></i> Inicio</a></li>
                <li><a href="#">Productos</a></li>
            </ol>
        </section>        
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Relación completa de Productos</h3>

                            <div class="row">
                                <div class="col-md-6 text-left" style="margin-top: 15px;">
                                    <a class="btn btn-primary btn-flat" onclick="FrmGuardarProductos();">Nuevo Producto</a>
                                </div>
                            </div>

                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped text-center">
                                <thead>
                                    <tr>
                                        <th>Codigo</th>
                                        <th>Familia</th>
                                        <th>Nombre producto</th>
                                        <th>Unidad medida</th>
                                        <th>Estado</th>
                                        <th>Operaciones</th>
                                    </tr>
                                </thead>                  	
                                <tbody>  
                                    <?php foreach ($productos as $r): ?>                    
                                        <tr> 
                                            <td><?php echo $r->__GET('sProSrvCodigo'); ?></td>
                                            <td><?php echo $r->__GET('sFamDescripcion'); ?></td>
                                            <td><?php echo $r->__GET('sProSrvNombre'); ?></td>
                                            <td><?php echo $r->__GET('sUndDescripcion'); ?></td>
                                            <td><?php echo $r->__GET('nProSrvEstado') != 0 ? '<span class="label label-success">Habilitado</span>':'<span class="label label-danger">Inhabilitado</span>';?></td>
                                            <td>
                                                <a class="btn btn-warning btn-xs btn-flat" data-prodser="<?php echo $r->__GET('ProdServ_Id'); ?>" data-toggle="modal" data-target="#modal-precio"><i class="fa fa-money"></i></a>
                                                <a class="btn btn-primary btn-xs btn-flat" onclick="mostrarProducto('<?php echo $r->__GET('ProdServ_Id'); ?>');" data-toggle="modal" data-target="#modal-ver"><i class="fa fa-eye"></i></a>
                                                <a class="btn btn-success btn-xs btn-flat" title="Editar" onclick="FrmEditarProductos('<?php echo $r->__GET('ProdServ_Id'); ?>');"><i class="fa fa-pencil"></i></a>
                                                <button type="button" class="btn btn-danger btn-xs btn-flat" title="Eliminar" data-toggle="modal" data-target="#modal-eliminar" data-codigo="<?php echo $r->__GET('ProdServ_Id'); ?>"><i class="fa fa-trash-o"></i></button>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Codigo</th>
                                        <th>Familia</th>
                                        <th>Nombre producto</th>
                                        <th>Unidad medida</th>
                                        <th>Estado</th>
                                        <th>Operaciones</th>
                                    </tr>
                                </tfoot>
                            </table>

                            <!-- Modal modeal-ver -->
                            <div class="modal fade" id="modal-ver" tabindex="-1" role="dialog" aria-labelledby="modal-ver-label" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form action="?c=Productos&a=PDFProductos" method="POST">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                                <h4 class="modal-title" id="modal-ver-label">Mostrar Producto</h4>
                                            </div>
                                            <div class="modal-body">
                                                <table class="table table-striped" id="ver_datos">

                                                </table>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-danger" formtarget="_blak"><i class="fa fa-print"></i>&nbsp;Imprimir</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- /Modal modal-ver -->
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </section><!-- /.content -->
        <!-- Modal modal-eliminar -->
        <div class="modal fade" id="modal-eliminar" tabindex="-1" role="dialog" aria-labelledby="modal-eliminar-label" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="?c=Productos&a=EliminarProductos" method="POST">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                            <h4 class="modal-title" id="modal-eliminar-label">Eliminar</h4>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" name="idProdServ" id="idProdServ" value="">
                            ¿Estás seguro que desea eliminar?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">No, cancelar</button>
                            <button type="submit" class="btn btn-danger btn-flat">Si, eliminar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /Modal modal-eliminar -->
        <!-- Modal modal-eliminar -->
        <div class="modal fade" id="modal-precio" tabindex="-1" role="dialog" aria-labelledby="modal-eliminar-label" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                        <h4 class="modal-title" id="modal-eliminar-label">Precio</h4>
                    </div>
                    <div class="modal-body">        
                        <div id="resultado_precio"></div>
                        <form action="javascript:GuardarPrecioProducto();" method="POST">
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <input type="hidden" name="PrecioVenta_Id" id="PrecioVenta_Id" value="0">
                                    <input type="hidden" name="ProdServ_Id" id="ProdServ_Id">
                                    <label>Unidad de medida</label>
                                    <select class="form-control" name="Unidadmedida_Id" id="Unidadmedida_Id"  required="required">
                                        <option value="">Seleccione unidad de medida</option>
                                    </select>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>Precio</label>
                                    <input type="number" step="any" class="form-control" name="PreVenPrecio" id="PreVenPrecio" placeholder="0.00"  required="required">
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>Tipo Moneda</label>
                                    <select class="form-control" name="Moneda_Id" id="Moneda_Id"  required="required">
                                        <option value="">Seleccione tipo de moneda</option>
                                        <option value="1">Soles (S/)</option>
                                        <option value="2">Dolares ($)</option>
                                    </select>
                                </div>
                                <div class="col-md-6 form-group" style="height: 60px;">                                    
                                </div>           
                                <div class="col-md-6 form-group">                                    
                                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal" style="width: 100%;">No, cancelar</button>
                                </div>
                                <div class="col-md-6 form-group">      
                                    <button type="submit" class="btn btn-danger btn-flat" style="width: 100%;">Si, Guardar</button>
                                </div>
                            </div>
                        </form>
                        <div class="row">                                
                            <div class="col-md-12 form-group">
                                <table class="table table-bordered table-striped text-center" id="example2">
                                    <thead>
                                        <tr>
                                            <th>Unidad de medida</th>
                                            <th>Precio</th>
                                            <th>Moneda</th>
                                            <th>Eliminar</th>
                                        </tr>
                                    </thead>
                                    <tbody id="productos_precios">
                                        <tr>
                                            <td colspan="3">No se encontro ninguna informacion</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Modal modal-eliminar -->
        <div class="modal fade" id="modal-eliminar-precioventa" tabindex="0" role="dialog" aria-labelledby="modal-eliminar-label" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="javascript:EliminarPrecio();" method="POST">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                            <h4 class="modal-title" id="modal-eliminar-label">Eliminar</h4>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" name="IdPrecioVenta" id="IdPrecioVenta" value="">
                            ¿Estás seguro que desea eliminar?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">No, cancelar</button>
                            <button type="submit" class="btn btn-danger btn-flat">Si, eliminar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /Modal modal-eliminar -->
        <script type="text/javascript" src="view/librerias/dist/js/funcionesMovimiento.js"></script>
        <script type="text/javascript" src="view/librerias/dist/js/funcionesProductos.js"></script>
        <script type="text/javascript">
            function Limpiar(){
                document.getElementById('PrecioVenta_Id').value="0";
                $('#Unidadmedida_Id').prop('selectedIndex',0);
                $('#Moneda_Id').prop('selectedIndex',0);
                $('#Operacion_Id').prop('selectedIndex',0);
                document.getElementById('PreVenPrecio').value="";
            }
            function GuardarPrecioProducto(){
                var PrecioVenta_Id = document.getElementById('PrecioVenta_Id').value;
                var ProdServ_Id = document.getElementById('ProdServ_Id').value;
                var Unidadmedida_Id = document.getElementById('Unidadmedida_Id').value;
                var Moneda_Id = document.getElementById('Moneda_Id').value;
               var PreVenPrecio = document.getElementById('PreVenPrecio').value;
                $.post('?c=precioventa&a=GuardarPrecioventa',{
                    PrecioVenta_Id: PrecioVenta_Id,
                    ProdServ_Id: ProdServ_Id,
                    Unidadmedida_Id: Unidadmedida_Id,
                    Moneda_Id : Moneda_Id,
                    PreVenPrecio: PreVenPrecio
                },function(data){
                    $("#resultado_precio").html(data);
                    ListarPrecios(ProdServ_Id);
                    Limpiar();
                });
            }
            $(function() {
                $("#example1").dataTable({
                    "aLengthMenu": [
                        [10, 15, 25, 50, 100, -1],
                        [10, 15, 25, 50, 100, "All"]
                    ],
                    "iDisplayLength": 10,
                    "scrollX": true,
                    "bSort": false,
                    "language": {
                        "search": "Buscar",
                        "lengthMenu": "Visualizar _MENU_ registro por página",
                        "zeroRecords": "No hay información para mostrar",
                        "info": "Pagina _PAGE_ de _PAGES_ de _MAX_ ",
                        "infoEmpty": "Pagina _PAGE_ - _PAGES_ de _MAX_ registros",
                        "infoFiltered": "(filtered from _MAX_ total records)",
                        "paginate": {
                            "previous": "Anterior",
                            "next": "Siguiente"
                        }
                    },
                    "rting": []
                });
            });
            $('#modal-eliminar').on('show.bs.modal', function(event) {
                var button = $(event.relatedTarget);
                var recipient = button.data('codigo');
                var modal = $(this);
                modal.find('.modal-body input').val(recipient);
            });
            $('#modal-precio').on('show.bs.modal', function(event) {
                var button = $(event.relatedTarget);
                var recipient = button.data('prodser');                
                var modal = $(this);
                modal.find('#ProdServ_Id').val(recipient);
                ProductosUNILista();
                ListarPrecios(recipient);
            });
            $('#modal-eliminar-precioventa').on('show.bs.modal', function(event) {
                var button = $(event.relatedTarget);
                var recipient = button.data('codigopv');                
                var modal = $(this);
                modal.find('#IdPrecioVenta').val(recipient);
            });
        </script>
