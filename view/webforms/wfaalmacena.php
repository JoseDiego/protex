<div class="box-body">
                    <table id="example1" class="table table-bordered table-striped text-center">
                                         	
                        <tbody>  
                            <?php 
                            foreach ($almacen as $r){
                            ?>
                            <tr>
                                <td><?php echo $r->__GET('sAlmNombre'); ?></td>
                                <td><?php echo $r->__GET('sAlmUbicacion'); ?></td>
                                <td><?php echo $r->__GET('nAlmEstado') == 1 ? '<span class="label label-success">Habilitado</span>':'<span class="label label-danger">Deshabilitado</span>'; ?></td>
                                <td>
                                    <a href="#" class="btn btn-primary btn-flat btn-xs" data-toggle="modal" data-target="#Mostrar" onclick="MostrarAlmacen('<?php echo $r->__GET('Almacen_Id'); ?>');"><i class="glyphicon glyphicon-eye-open"></i></a>
                                    <a href="#" class="btn btn-success btn-flat btn-xs" onclick="FrmEditarAlmacen('<?php echo $r->__GET('Almacen_Id'); ?>');"><i class="fa fa-pencil"></i></a>
                                    <a href="#" class="btn btn-danger btn-flat btn-xs" data-toggle="modal" data-target="#Eliminar" data-codigo="<?php echo $r->__GET('Almacen_Id'); ?>"><i class="fa fa-trash-o"></i></a>
                                </td>
                            </tr>
                            <?php 
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>