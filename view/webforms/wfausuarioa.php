            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Usuario
                    <small></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
                    <li class="active">Registrar Usuario</li>
                </ol>
            </section>
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-md-12" id="resultadoPasajero">
                        
                    </div>
                    <!-- left column -->
                    <div class="col-md-6">
                        <div class="box box-primary">
                            <div class="box-header">
                                <h3 class="box-title">Datos básicos</h3>
                            </div><!-- /.box-header -->
                            <!-- form start -->
                            <form role="form" action="javascript:guardarUsuario();" method="POST" id="FrmUsuario">
                                <div class="box-body">
                                    <div id="resultado"></div>
                                    <!-- Date range -->
                                    <div class="form-group">
                                        <input type="hidden" name="idRegistro" id="idRegistro" value="<?php if(isset($r)){ echo $r->__GET('Usuario_Id'); }else{ echo '0'; };?>">
                                        <label>Nombre</label>
                                        <input type="text" name="nombre" id="nombre" class="form-control" onkeypress="return ValidaSoloLetras(event);" maxlength="45" placeholder="Nombre" value="<?php if(isset($r)){ echo $r->__GET('sUsuNombre'); } ?>" required="required"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Login</label>
                                        <input type="text" name="login" id="login" class="form-control" maxlength="45" placeholder="Login" value="<?php if(isset($r)){ echo $r->__GET('sUsuLogin'); } ?>" required="required"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Contraseña</label>
                                        <input type="password" name="contraseñaOne" id="contraseñaOne" class="form-control" maxlength="45" placeholder="************" value="" required="required"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Vuelve a escribir la contraseña</label>
                                        <input type="password" name="contraseñaTwo" id="contraseñaTwo" class="form-control" maxlength="45" placeholder="************" value="" required="required"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Tipo Usuario</label>
                                        <select class="form-control" name="tipo" id="tipo" required="required">
                                            <option value="">Seleccionar Tipo</option>
                                            <option <?php if(isset($r)){ echo $r->__GET('nUsuTipo') == 1 ? 'selected' : ''; } ?> value="1">ADMINISTRADOR</option>
                                            <option <?php if(isset($r)){ echo $r->__GET('nUsuTipo') == 2 ? 'selected' : ''; } ?> value="2">ASISTENTE</option>
                                            <option <?php if(isset($r)){ echo $r->__GET('nUsuTipo') == 3 ? 'selected' : ''; } ?> value="3">REPORTES</option>
                                            <option <?php if(isset($r)){ echo $r->__GET('nUsuTipo') == 4 ? 'selected' : ''; } ?> value="4">SUPERVIRSOR</option>
                                        </select>
                                    </div>                                    
                                    <div class="form-group">
                                        <label>Estado</label>
                                        <select class="form-control" name="estado" id="estado">
                                            <option <?php if(isset($r)){ echo $r->__GET('nUsuEstado') == 1 ? 'selected' : ''; } ?> value="1">Habilitado</option>
                                            <option <?php if(isset($r)){ echo $r->__GET('nUsuEstado') == 0 ? 'selected' : ''; } ?> value="0">Deshabilitado</option>
                                        </select>
                                    </div>
                                </div><!-- /.box-body -->

                                <div class="box-footer">
                                    <button type="submit" class="btn btn-primary" data-toggle="tooltip" title="Guardar nuevo viajero">
                                    Guardar
                                    </button>
                                </div>               
                                <!-- Modal modeal-1 -->
                                <!-- /Modal modal-1 -->
                            </form>
                        </div><!-- /.box -->
                    </div><!--/.col (left) -->                    
                </div>   <!-- /.row -->
            </section><!-- /.content -->
            <script type="text/javascript" src="view/librerias/dist/js/funcionesUsuario.js"></script>
            <script type="text/javascript" src="view/librerias/dist/js/funcionesGenerales.js"></script>