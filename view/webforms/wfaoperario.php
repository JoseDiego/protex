
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Operario
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="wfamain.php"><i class="fa fa-dashboard"></i> Inicio</a></li>
                <li><a href="#">Operario</a></li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Relación completa de Operarios</h3>

                            <div class="row">
                                <div class="col-md-6 text-left" style="margin-top: 15px;">
                                    <a class="btn btn-primary btn-flat" onclick="FrmGuardarOperario();">Nuevo Operario</a>
                                </div>
                            </div>

                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped text-center">
                                <thead>
                                    <tr>
                                        <th>DNI</th>
                                        <th>Apellidos</th>
                                        <th>Nombres</th>
                                        <th>Cargo</th>
                                        <th>Telefono</th>
                                        <th>Estado</th>
                                        <th>Operaciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($operario as $ope){
                                    ?>
                                    <tr>
                                        <td><?php echo $ope->__GET('sOpeDni') ?></td>
                                        <td><?php echo $ope->__GET('sOpeApePat') . ' ' . $ope->__GET('sOpeApeMat'); ?></td>
                                        <td><?php echo $ope->__GET('sOpeNombres'); ?></td>
                                        <td><?php echo $ope->__GET('sOpeCargo'); ?></td>
                                        <td><?php echo $ope->__GET('sOpeTelefono'); ?></td>
                                        <td><?php echo $ope->__GET('nOpeEstado') == 1 ? '<span class="label label-success">Habilitado</span>':'<span class="label label-danger">Deshabilitado</span>'; ?></td>
                                        <td>
                                            <a  class="btn btn-primary btn-flat btn-xs" data-toggle="modal" data-target="#modal-ver" onclick="mostrarOperario('<?php echo $ope->__GET('Operario_Id'); ?>');"><i class="glyphicon glyphicon-eye-open"></i></a>
                                            <a href="#" class="btn btn-success btn-flat btn-xs" onclick="FrmEditarOperario('<?php echo $ope->__GET('Operario_Id'); ?>');"><i class="fa fa-pencil"></i></a>
                                            <a href="#" class="btn btn-danger btn-flat btn-xs" data-toggle="modal" data-target="#modal-eliminar" data-codigo="<?php echo $ope->__GET('Operario_Id'); ?>"><i class="fa fa-trash-o"></i></a>
                                        </td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>DNI</th>
                                        <th>Apellidos</th>
                                        <th>Nombres</th>
                                        <th>Cargo</th>
                                        <th>Telefono</th>
                                        <th>Estado</th>
                                        <th>Operaciones</th>
                                    </tr>
                                </tfoot>
                            </table>

                            <!-- Modal modeal-ver -->
                            <div class="modal fade" id="modal-ver" tabindex="-1" role="dialog" aria-labelledby="modal-ver-label" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form action="?c=Operario&a=PDFCliente" method="POST">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                                <h4 class="modal-title" id="modal-ver-label">Ver Operario</h4>
                                            </div>
                                            <div class="modal-body">
                                                <table class="table table-striped" id="ver_datos">

                                                </table>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-danger" formtarget="_blak"><i class="fa fa-print"></i>&nbsp;Imprimir</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- /Modal modal-ver -->

                            <!-- Modal modal-eliminar -->
                            <div class="modal fade" id="modal-eliminar" tabindex="-1" role="dialog" aria-labelledby="modal-eliminar-label" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form action="?c=Operario&a=EliminarOperario" method="POST">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                                <h4 class="modal-title" id="modal-eliminar-label">Eliminar</h4>
                                            </div>
                                            <div class="modal-body">
                                                <input type="hidden" name="Operario_Id" id="Operario_Id" value="">
                                                ¿Estás seguro que desea eliminar?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">No, cancelar</button>
                                                <button type="submit" class="btn btn-danger btn-flat">Si, eliminar</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- /Modal modal-eliminar -->

                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </section><!-- /.content -->
        <script type="text/javascript" src="view/librerias/dist/js/funcionesOperario.js"></script>
        <script type="text/javascript">
            $(function() {
                jQuery.fn.dataTableExt.oSort['fecha-asc'] = function(a, b) {
                    var ukDatea = a.split('-');
                    var ukDateb = b.split('-');

                    var x = (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
                    var y = (ukDateb[2] + ukDateb[1] + ukDateb[0]) * 1;

                    return ((x < y) ? -1 : ((x > y) ? 1 : 0));
                };

                jQuery.fn.dataTableExt.oSort['fecha-desc'] = function(a, b) {
                    var ukDatea = a.split('-');
                    var ukDateb = b.split('-');

                    var x = (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
                    var y = (ukDateb[2] + ukDateb[1] + ukDateb[0]) * 1;

                    return ((x < y) ? 1 : ((x > y) ? -1 : 0));
                };
                $("#example1").dataTable({
                    "scrollX": true,
                    "bSort": false,
                    "language": {
                        "search": "Buscar",
                        "lengthMenu": "Visualizar _MENU_ registro por página",
                        "zeroRecords": "No hay información para mostrar",
                        "info": "Pagina _PAGE_ de _PAGES_ de _MAX_ ",
                        "infoEmpty": "Pagina _PAGE_ - _PAGES_ de _MAX_ registros",
                        "infoFiltered": "(filtered from _MAX_ total records)",
                        "paginate": {
                            "previous": "Anterior",
                            "next": "Siguiente"
                        }
                    }
                });
            });
            $('#modal-eliminar').on('show.bs.modal', function(event) {
                var button = $(event.relatedTarget); // Button that triggered the modal
                var recipient = button.data('codigo'); // Extract info from data-* attributes

                // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this);
                modal.find('.modal-body input').val(recipient);
            });
        </script>
