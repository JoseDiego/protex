<div class="col-md-6 form-group">
    <input type="hidden" id="Familia_Id" name="Familia_Id" value="<?php echo isset($r) ? $r->__GET('Familia_Id'):'0'; ?>"> 
    <label>Codigo</label>
    <input type="text" class="form-control" name="sFamCodigo" id="sFamCodigo" placeholder="codigo" value="<?php echo isset($r) ? $r->__GET('sFamCodigo'):''; ?>"  required="required">
</div>
<div class="col-md-6 form-group">
    <label>Padre</label>
    <select class="form-control" name="nFamPadre" id="nFamPadre"  required="required">
        <option value="">Seleccionar padre</option>
        <?php
        foreach ($familia as $f) {
            ?>
            <option value="<?php echo $f->__GET('Familia_Id'); ?>" <?php echo isset($r) ? ($r->__GET('nFamPadre') == $f->__GET('Familia_Id') ? 'selected':'') :''; ?>><?php echo $f->__GET('sFamDescripcion'); ?></option>
            <?php
        }
        ?>
    </select>
</div>
<div class="col-md-6 form-group">
    <label>Tipo</label>
    <select class="form-control" name="nFamTipo" id="nFamTipo"  required="required">
        <option value="">Seleccionar tipo</option>
        <option value="1" <?php echo isset($r) ? ($r->__GET('nFamTipo') == 1 ? 'selected':'') :''; ?>>Producto</option>
        <option value="2" <?php echo isset($r) ? ($r->__GET('nFamTipo') == 2 ? 'selected':'') :''; ?>>Servicio</option>
    </select>
</div>
<div class="col-md-6 form-group">
    <label>Estado</label>
    <select class="form-control" name="nFamEstado" id="nFamEstado"  required="required">
        <option value="1" <?php echo isset($r) ? ($r->__GET('nFamEstado') == 1 ? 'selected':'') :''; ?>>Habilitado</option>
        <option value="0" <?php echo isset($r) ? ($r->__GET('nFamEstado') == 0 ? 'selected':'') :''; ?>>Deshabilitado</option>
    </select>
</div>
<div class="col-md-12 form-group">
    <label>Descripción</label>
    <textarea class="form-control" name="sFamDescripcion" id="sFamDescripcion" placeholder="Descripción"><?php echo isset($r) ? $r->__GET('sFamDescripcion'):''; ?></textarea>
</div>