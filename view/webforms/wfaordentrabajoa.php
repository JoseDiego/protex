<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Nueva Orden de Trabajo
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="wfamain.php"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li><a href="#">Orden Trabajo</a></li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-primary">
        <form action="javascript:GuardarOrdenTrabajo();" method="POST" id="FrmMovimiento">
          <br/>
          <div class="row">
            <div class="col-md-2"></div>

            <div class="col-md-8" style="border-width:2px; border-style:solid; border-color: #3C8DBC; margin: auto;">
              <div class="box-header">
                <h3 class="box-title">Registrar Orden de Trabajo</h3>
              </div>

              <div class="box-body">
                <div id="resultado"></div>

                <div class="row">
                  <div class="col-md-1"></div>

                  <div class="col-md-4 form-group">
                    <input type="hidden" id="OrdenTrabajo_Id" name="OrdenTrabajo_Id" value="<?php echo isset($r) ? $r->__GET('OrdenTrabajo_Id'):''; ?>">

                    <label>Fecha</label>
                    <br/>
                    <?php  if(isset($r)){$date = date_create($r->__GET('otFecha')); $fecha=date_format($date, 'd-m-Y');}else{} ?>
                    <input type="text" class="form-control" name=otFecha id=otFecha placeholder="Fecha" value="<?php echo isset($date) ? $fecha:''; ?>" required>
                  </div>

                  <div class="col-md-2 form-group">
                  </div>

                  <div class="col-md-4 form-group">
                    <label>Nro. Orden de Trabajo</label>
                    <br/>
                    <input type="text"  name=ordCompra id=ordCompra class="form-control" required onkeypress="return justNumbers(event);" placeholder="Nro" value="<?php echo isset($r) ? $r->__GET('otNroOrdenCompra'):''; ?>">
                  </div>

                  <div class="col-md-1"></div>
                </div>

                <div class="row">
                  <div class="col-md-1"></div>

                  <div class="col-md-10 form-group">
                    <input type="hidden" id="OrdenTrabajo_Id" name="OrdenTrabajo_Id" value="<?php echo isset($r) ? $r->__GET('OrdenTrabajo_Id'):'0'; ?>">
                    <label>Cliente</label> <br/>

                    <select class="form-control"  name="Cliente_Id" id="Cliente_Id" onchange="OrdenesTrabajo();" required>
                      <option value="">Seleccione Cliente</option>

                    </select>
                  </div>

                  <div class="col-md-1"></div>
                </div>

                <div class="row">
                  <div class="col-md-1"></div>

                  <div class="col-md-4 form-group">
                    <label>Orden de Compra del cliente</label>
                    <br/>
                    <select class="form-control" required name="ordentrabajo" id="ordentrabajo" disabled="true" onchange="CargarDetalles();">
                      <option value="">Seleccione Orden</option>
                      <!-- -->
                    </select>
                  </div>

                  <div class="col-md-2 form-group">
                  </div>

                  <div class="col-md-4 form-group">
                    <label>Fecha de entrega</label>
                    <br/>
                    <?php  if(isset($r)){$dateE = date_create($r->__GET('otFechaE')); $fechaE=date_format($dateE, 'd-m-Y');}else{} ?>
                    <input type="text" required class="form-control" name="otFechaE" id="otFechaE" placeholder="Fecha" value="<?php echo isset($dateE) ? $fechaE:''; ?>" disabled="<?php echo isset($dateE) ? 'false':'true'; ?>">
                  </div>
                  <div class="col-md-1"></div>
                </div>
              </div>
            </div>
            <div class="col-md-2"></div>
          </div>
          <br/>
          <div class="row"></div>
          <br/>

          <div class="row">
            <div class="col-md-2"></div>

            <div class="col-md-8">
              <table  id="datos" class="table table-bordered table-striped text-center">
                <thead>
                  <tr>
                    <th>Producto</th>
                    <th>Unidad</th>
                    <th>Cantidad Asig.</th>
                    <th>Cantidad Prog.</th>
                    <th>Cantidad Rest.</th>
                  </tr>
                </thead>
                <tbody id="productos_servicios">
                  <tr>
                    <td colspan="5">No hay datos</td>
                  </tr>
                </tbody>
              </table>
            </div>

            <div class="col-md-2"></div>
          </div>
          <div class="col-md-10">
            <div class="row">
              <div class="col-md-8" style="left:220px;">
                <a  class="btn btn-primary btn-flat " data-toggle="modal" data-target="#modal-ver" onclick="reqMateriales()">Generar Req. Materiales</a>
              </div>
              <div class="col-md-4">
                <table>
                  <tr>
                    <td align="right">
                      <a href="?c=OrdenTrabajo&a=Index" class="btn btn-danger btn-flat">Cancelar</a>
                      <button type="submit" class="btn btn-primary btn-flat">Guardar información</button>
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
          <!-- /.box-header -->

          <!-- /.box-body -->
        </form>
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->
<!-- /Modal modal-eliminar -->
<div class="modal fade" id="agregar_producto" tabindex="-1" role="dialog" aria-labelledby="modal-eliminar-label" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content" id="FrmTipomovimiento">

    </div>
  </div>
</div>
<div class="modal fade" id="modal-ver" tabindex="-1" role="dialog" aria-labelledby="modal-ver-label" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form action="?c=OrdenTrabajo&a=PDFReqMateriales" method="POST">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
          <h4 class="modal-title" id="modal-ver-label">Ver Requerimiento de Materiales</h4>
        </div>
        <div class="modal-body">
          <table class="table table-striped" >
            <thead>
              <tr>
                <th>Producto</th>
                <th>Unidad</th>
                <th>Cantidad a Utilizar</th>
                <th>Cantidad Existente</th>
              </tr>
            </thead>
            <tbody id="ver_datos">

            </tbody>
          </table>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-danger" formtarget="_blak"><i class="fa fa-print"></i>&nbsp;Imprimir</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </form>
    </div>
  </div>
</div>
</div>

<!-- /Modal modal-eliminar -->
<script src="view/librerias/dist/js/funcionesOrdenTrabajo.js"></script>
<script type="text/javascript">
$('#otFecha').datetimepicker({
  viewMode: 'days',
  format: 'DD-MM-YYYY'
});
$('#otFechaE').datetimepicker({
  viewMode: 'days',
  format: 'DD-MM-YYYY'
});
$(document).ready(function(){
  //TipoPrvClien();
  listarCliente();
  <?php
  if(isset($r)){
    echo 'OrdenesTrabajo();
    ';
  }
  ?>
});


function xd() {
  var table = document.getElementById( "datos" );
  var tableArr = [];
  for ( var i = 1; i < table.rows.length; i++ ) {
    tableArr.push({
      ProdServ_Id: table.rows[i].cells[0].innerHTML,
      ProdservdNombre: table.rows[i].cells[1].innerHTML,
      UnidadNombre: table.rows[i].cells[2].innerHTML,
      UnidadMedida_Id: table.rows[i].cells[3].innerHTML,
      nPedDetCantidad: table.rows[i].cells[4].innerHTML,
      nOTCantidadProg: table.rows[i].cells[5].innerHTML,
      nOTCantidadRest: table.rows[i].cells[6].innerHTML,
      nt: table.rows[i].cells[0].innerHTML

    });

  }
  //alert(JSON.stringify(tableArr));
  var jsonArray = JSON.stringify(tableArr);

  //     $.post('?c=ordencompra&a=ListarDetallesOC', {
  //                     Pedido_Id : Pedido_Id
  //				    }, function(data) {
  //				        $("#productos_servicios").html(data);
  //				    });


}

function calculo(prog,cant,rest){
  var idPost=parseFloat(document.getElementById(prog).innerHTML);
  var cnt=parseFloat(document.getElementById(cant).innerHTML);

  //var cantProg = document.getElementById(prog).innerHTML;
  //var cant = document.getElementById(cant).innerHTML;


  if (idPost <= cnt) {
    $.post('?c=ordentrabajo&a=CalculoRest', {
      cant : cnt,
      cantProg : idPost
    }, function(data) {

      $("#"+rest).html(data.trim());
    });
  } else { alert('Cantidad Prog. debe ser MENOR a la Cantidad Asig.');
  document.getElementById(prog).innerHTML = '';
  document.getElementById(rest).innerHTML = cnt;
  calculo();
}

}
function CargarDetalles(){
  var Pedido_Id = document.getElementById('ordentrabajo').value;
  var combo = document.getElementById("ordentrabajo");
  var selected = combo.options[combo.selectedIndex].text;
  if(selected !== 'Seleccione Orden'){
    $.post('?c=ordencompra&a=ListarDetallesOC', {
      Pedido_Id : Pedido_Id,
      OrdenTrabajo: selected
    }, function(data) {
      $("#productos_servicios").html(data);
    });
  }
}

function OrdenesTrabajo(){
  var Cliente_Id = <?php echo isset($r) ? $r->__GET('otCliente_Id'): "document.getElementById('Cliente_Id').value"; ?>;
  var valor = <?php echo isset($r) ? $r->__GET('otNroOrdenTrabajo'): "0"; ?>;
  $.post('?c=OrdenTrabajo&a=ListarOrdenC', {
    Cliente_Id: Cliente_Id
  }, function(data){
    $("#ordentrabajo").html(data);
    document.getElementById('ordentrabajo').disabled = false;
    document.getElementById('otFechaE').disabled = false;
    <?php echo isset($r) ?
    "document.getElementById('ordentrabajo').value = $('#ordentrabajo option:contains('+valor+')').val();
    CargarDetalles();":"" ?>
  });
}

function listarCliente(){
  var OrigenDestino_Id=0;
  $.post('?c=OrdenCompra&a=ListarComboBoxClientes',{
    OrigenDestino_Id: OrigenDestino_Id
  },function(data){
    $("#Cliente_Id").html(data);
    document.getElementById('Cliente_Id').value = <?php echo isset($r) ? $r->__GET('otCliente_Id'):'0'; ?>;
  });
}

$('#eliminar_productos').on('show.bs.modal', function(event) {
  var button = $(event.relatedTarget); // Button that triggered the modal
  var recipient = button.data('productoid'); // Extract info from data-* attributes

  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this);
  modal.find('.modal-body input').val(recipient);
});

function justNumbers(e)
{
  var keynum = window.event ? window.event.keyCode : e.which;
  if ((keynum == 8) || (keynum == 46))
  return true;
  return /\d/.test(String.fromCharCode(keynum));
}
</script>
