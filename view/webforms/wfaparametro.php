<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Parámetros
                    <small></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
                    <li class="active">Registrar Parámetros</li>
                </ol>
            </section>
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-6">
                        <div class="box box-primary">
                            <div class="box-header">
                                <h3 class="box-title">Información básica</h3>
                            </div><!-- /.box-header -->
                            <!-- form start -->
                            <form role="form">
                                <div class="box-body">
                                    <div class="row">                                        
                                        <div class="col-md-12" id="resultadoPasajero">

                                        </div>
                                    </div>
                                    <!-- Date range -->
                                    <?php 
                                    foreach ($parametro as $row){
                                        $descripcion = explode("|", $row->__GET('sParDescripcion'));
                                    ?>
                                    <div class="form-group">
                                        <label><?php echo $descripcion[0]; ?></label><small><?php echo $descripcion[1]; ?></small>
                                        <input type="hidden" name="codigo[]" id="codigo" value="<?php echo $row->__GET('Parametro_Id'); ?>">
                                        <input type="text" class="form-control" name="dato[]" id="dato" placeholder="<?php echo $descripcion[0]; ?>" onkeypress="<?php if($row->__GET('Parametro_Id')==1 || $row->__GET('Parametro_Id')==2 || $row->__GET('Parametro_Id')==3){ echo 'ValidaSoloNumeros();'; } ?>" value="<?php echo $row->__GET('sParValor'); ?>"/>                                        
                                    </div> 
                                    <?php 
                                    }
                                    ?>
                                </div><!-- /.box-body -->

                                <div class="box-footer">
                                    <a onclick="GuardarParametros();" class="btn btn-primary" data-toggle="tooltip" title="Guardar parámetros">
                                    Guardar Parámetros
                                    </a>
                                </div>               
                                <!-- Modal modeal-1 -->
                                <!-- /Modal modal-1 -->
                            </form>
                        </div><!-- /.box -->
                    </div><!--/.col (left) -->                    
                </div>   <!-- /.row -->
            </section><!-- /.content -->
            <script type="text/javascript" src="view/librerias/dist/js/funcionesParametros.js"></script>