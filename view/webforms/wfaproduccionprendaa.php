
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Nuevo Plan de Producción
    <small></small>
  </h1>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-primary">
        <form action="javascript:GuardarMovimiento();" method="POST" id="FrmMovimiento">
          <br/>
          <div class="row">
            <div class="col-md-2"></div>

            <div class="col-md-8" style="border-width:2px; border-style:solid; border-color: #3C8DBC; margin: auto;">
              <div class="box-header">
                <h3 class="box-title">Registrar Plan de Producción</h3>
              </div>

              <div class="box-body">
                <div id="resultado"></div>

                <div class="row">
                  <div class="col-md-1"></div>

                  <div class="col-md-4 form-group">
                    <label>Fecha</label>
                    <br/>
                    <input type="text" class="form-control" name="dMovFecha" id="dMovFecha" placeholder="Fecha">
                  </div>

                  <div class="col-md-2 form-group">
                  </div>

                  <div class="col-md-4 form-group">
                    <label>Operario</label>
                    <br/>
                    <select class="form-control" name="ListaOperario" id="ListaOperario" required="">
                    </select>
                  </div>

                  <div class="col-md-1"></div>
                </div>

                <div class="row">
                  <div class="col-md-1"></div>

                  <div class="col-md-10 form-group">
                    <div class="row">
                      <div class="col-md-10">
                        <table class="table table-bordered table-striped text-center">
                          <thead>
                            <tr>
                              <th>Cant. Program.</th>
                              <th>Cant. x Asig.</th>
                              <th>Unidad</th>
                              <th>Producto</th>
                            </tr>
                          </thead>
                          <tbody id="productos_servicios">
                            <tr>
                              <td colspan="4">No hay datos</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>

                      <div class="col-md-2">
                        <a class="btn btn-success" onclick="AgregarDetalle()"><span class="glyphicon glyphiconglyphicon glyphicon-plus"></span></a>
                        <a class="btn btn-danger" onclick="EliminarDetalle()"><span class="glyphicon glyphiconglyphicon glyphicon-minus"></span></a>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-1"></div>
                </div>
              </div>
            </div>

            <div class="col-md-2"></div>
          </div>

          <br/>
          <div class="row"></div>
          <br/>
          <div class="row">
            <div class="col-md-7"></div>

            <div class="col-md-3">
              <table>
                <tr>
                  <td align="right">
                    <button type="submit" class="btn btn-primary btn-flat">Generar Req. Materiales</button>
                  </td>
                </tr>
                <tr>
                  <td align="right">
                    <br/>
                  </td>
                </tr>
                <tr>
                  <td align="right">
                    <a href="?c=ProduccionPrenda&a=Index" class="btn btn-danger btn-flat">Cancelar</a>
                    <button type="submit" class="btn btn-primary btn-flat">Guardar información</button>
                  </td>
                </tr>
              </table>
            </div>

            <div class="col-md-2"></div>
          </div>
          <!-- /.box-header -->

          <!-- /.box-body -->

        </form>
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->
<!-- /Modal modal-eliminar -->
<div class="modal fade" id="agregar_producto" tabindex="-1" role="dialog" aria-labelledby="modal-eliminar-label" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content" id="FrmTipomovimiento">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
        <h4 class="modal-title">Agregar</h4>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="eliminar_productos" tabindex="-1" role="dialog" aria-labelledby="modal-eliminar-label" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content" id="FrmTipomovimiento">
      <form action="javascript:EliminarProductoLista();" method="POST" id="FrmIngresoCompra">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
          <h4 class="modal-title" id="modal-eliminar-label">ELIMINAR PRODUCTOS</h4>
        </div>
        <div class="modal-body">
          <input type="hidden" name="Codigo_Producto" id="Codigo_Producto">
          ¿Desea eliminar al producto de la lista?
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">No, cancelar</button>
          <button type="submit" class="btn btn-danger btn-flat">Si, Eliminar</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- /Modal modal-eliminar -->
<script src="view/librerias/dist/js/funcionesProduccionPrenda.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    listarOperario();
  })

  $('#dMovFecha').datetimepicker({
    viewMode: 'days',
    format: 'DD-MM-YYYY'
  });

  $('#fechaEntrega').datetimepicker({
    viewMode: 'days',
    format: 'DD-MM-YYYY'
  });
  function listarOperario(){
    var Operario_Id = 0;
    $.post('?c=produccionprenda&a=ListarComboBoxOperarios',{
      Operario_Id: Operario_Id
    },function(data){
      $("#ListaOperario").html(data);
      document.getElementById('ListaOperario').value = <?php echo isset($r) ? $r->__GET('Operario_Id'):'0'; ?>
    });
  }
  function TipoMoneda(){
    var moneda = $("#TipoMoneda");
    var Moneda_Id = document.getElementById('Moneda_Id').value;
    if(Moneda_Id == 1){
      moneda.removeClass('col-md-3');
      moneda.addClass('col-md-6');
      $("#TipoCambio").hide();
      $("#nMovTipoCambio").val("1");
    }else{
      moneda.removeClass('col-md-6');
      moneda.addClass('col-md-3');
      $("#TipoCambio").show();
      $("#nMovTipoCambio").val("");
    }
  }
  $('#eliminar_productos').on('show.bs.modal', function(event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var recipient = button.data('productoid'); // Extract info from data-* attributes

    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this);
    modal.find('.modal-body input').val(recipient);
  });
</script>
