<div class="col-md-6 form-group">
    <input type="hidden" name="Unidadmedida_Id" id="Unidadmedida_Id" value="<?php echo isset($r) ? $r->__GET('UnidadMedida_Id'):''; ?>">
    <label>Descripcion</label>
    <input type="text" class="form-control" name="sUndDescripcion" id="sUndDescripcion" placeholder="Descripcion" value="<?php echo isset($r) ? $r->__GET('sUndDescripcion'):''; ?>" required="required">
</div>
<div class="col-md-6 form-group">
    <label>Alias</label>
    <input type="text" class="form-control" name="sUndAlias" id="sUndAlias" placeholder="Alias" value="<?php echo isset($r) ? $r->__GET('sUndAlias'):''; ?>" required="required">
</div>
<div class="col-md-12 form-group">
    <label>Padre</label>
    <select class="form-control" name="nUndPadre_Id" id="nUndPadre_Id" required="required">
        <option value="">Seleccionar padre</option>
        <?php
        foreach ($unidadmedida as $u) {
            ?>
            <option value="<?php echo $u->__GET('UnidadMedida_Id'); ?>" <?php echo isset($r) ? ($r->__GET('nUndPadre_Id') == $u->__GET('UnidadMedida_Id') ? 'selected':'') :''; ?>><?php echo $u->__GET('sUndDescripcion') ?></option>
            <?php
        }
        ?>
    </select>
</div>
<div class="col-md-12 form-group">
    <label>Equivalencia <small style="font-weight: normal;">(Una(a) ? equivale a: )</small></label>
    <input type="text" class="form-control" name="nUndFac_Cnv" id="nUndFac_Cnv" placeholder="Unidade(es)" value="<?php echo isset($r) ? $r->__GET('nUndFac_Cnv'):''; ?>" required="required">
</div>
<div class="col-md-12 form-group">
    <label>Estado</label>
    <select class="form-control" name="nUndEstado" id="nUndEstado" required="required">clubpacasmayo
        <option value="1" <?php echo isset($r) ? ($r->__GET('nUndEstado') == 1 ? 'selected':'') :''; ?>>Habilitado</option>
        <option value="0" <?php echo isset($r) ? ($r->__GET('nUndEstado') == 0 ? 'selected':'') :''; ?>>Deshabilitado</option>
    </select>
</div>