
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Clientes/Proveedores
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="wfamain.php"><i class="fa fa-dashboard"></i> Inicio</a></li>
                <li><a href="#">Clientes/Proveedores</a></li>
            </ol>
        </section>        
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-6">
                    <div class="box">
                        <form action="javascript:GuardarProvclien();" method="POST" id="FrmMovimiento">
                            <div class="box-header">
                                <h3 class="box-title">Datos basicos</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div id="resultado"></div>
                                <div class="row">                                    
                                    <div class="col-md-12 form-group">
                                        <input type="hidden" id="Provclien_Id" name="Provclien_Id" value="<?php echo isset($r) ? $r->__GET('OrigenDestino_Id'):'0'; ?>">
                                        <label>Tipo</label>
                                        <select class="form-control" name="nProcliTipo" id="nProcliTipo">
                                            <option value="">Seleccione tipo</option>
                                            <option value="1" <?php echo isset($r) ? $r->__GET('nODTipo') == 1 ? 'selected':'' :''; ?>>Proveedor</option>
                                            <option value="2" <?php echo isset($r) ? $r->__GET('nODTipo') == 2 ? 'selected':'' :''; ?>>Cliente</option>
                                        </select>
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label>RUC/DNI</label>
                                        <input type="text" class="form-control" name="nProcliRUCDNI" id="nProcliRUCDNI" placeholder="RUC/DNI" value="<?php echo isset($r) ? $r->__GET('sODRucDni'):''; ?>" required="required">
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label>Nombre</label>
                                        <input type="text" class="form-control" name="sProcliNombre" id="sProcliNombre" placeholder="Nombre" value="<?php echo isset($r) ? $r->__GET('sODNombre'):''; ?>" required="required">
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label>Nombre Comercial</label>
                                        <input type="text" class="form-control" name="sProcliNomComercial" id="sProcliNomComercial" placeholder="Nom. Comercial" value="<?php echo isset($r) ? $r->__GET('sODNombreComercial'):''; ?>" required="required">
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label>Dirección</label>
                                        <input type="text" class="form-control" name="sProcliDireccion" id="sProcliDireccion" placeholder="Dirección" value="<?php echo isset($r) ? $r->__GET('sODDireccion'):''; ?>" required="required">
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label>Teléfono</label>
                                        <input type="text" class="form-control" name="nProcliTelefono" id="nProcliTelefono" placeholder="Teléfono" value="<?php echo isset($r) ? $r->__GET('sODTelefono'):''; ?>" required="required">
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label>Correo</label>
                                        <input type="text" class="form-control" name="sProcliCorreo" id="sProcliCorreo" placeholder="Correo" value="<?php echo isset($r) ? $r->__GET('sODCorreo'):''; ?>" required="required">
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label>Contacto</label>
                                        <input type="text" class="form-control" name="sProcliContacto" id="sProcliContacto" placeholder="Contacto" value="<?php echo isset($r) ? $r->__GET('sODContacto'):''; ?>" required="required">
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label>Dias de credito</label>
                                        <input type="text" class="form-control" name="nProcliDiasCredito" id="nProcliDiasCredito" placeholder="Dias de credito" value="<?php echo isset($r) ? $r->__GET('nODDiasCredito'):'0'; ?>" required="required">
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label>Tipo</label>
                                        <select class="form-control" name="nProcliEstado" id="nProcliEstado">
                                            <option value="1" <?php echo isset($r) ? $r->__GET('nODEstado') == 1 ? 'selected':'' :''; ?>>Habilitado</option>
                                            <option value="0" <?php echo isset($r) ? $r->__GET('nODEstado') == 0 ? 'selected':'' :''; ?>>Deshabilitado</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary btn-flat">Guardar información</button>
                                <a href="?c=Clienteproveedor&a=Index" class="btn btn-danger btn-flat">Cancelar</a>
                            </div>
                        </form>
                    </div><!-- /.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </section><!-- /.content -->
        <script type="text/javascript">
            $(document).ready(function(){
                $("#sProcliNombre").blur(function(){
                    var sProcliNomComercial = document.getElementById('sProcliNomComercial').value;
                    if(sProcliNomComercial == ""){
                        document.getElementById('sProcliNomComercial').value = document.getElementById('sProcliNombre').value;
                    }
                });
            });
        </script>
