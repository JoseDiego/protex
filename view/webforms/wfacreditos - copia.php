
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Movimiento
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="wfamain.php"><i class="fa fa-dashboard"></i> Inicio</a></li>
                <li><a href="#">Movimiento</a></li>
            </ol>
        </section>        
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Relación completa de Movimiento</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped text-center">
                                <thead>
                                    <tr>
                                        <th>Tipo movimiento</th>
                                        <th>Fecha</th>
                                        <th>Documento</th>
                                        <th>Cliente/Proveedor</th>
                                        <th>Moneda</th>
                                        <th>Total</th>
                                        <th>Por Pagar</th>
                                        <th>Estado</th>
                                        <th>Operaciones</th>
                                    </tr>
                                </thead>                  	
                                <tbody>  
                                    <?php 
                                    foreach ($movimientos as $r){
                                        if($r->__GET('nMovEstado') == 1){
                                    ?>
                                    <tr>
                                        <td><?php echo $r->__GET('sTMovNombre'); ?></td>
                                        <td><?php echo $r->__GET('dMovFecha'); ?></td>
                                        <td><?php echo $r->__GET('sDocNombreCorto').'-'.$r->__GET('sMovDocumento'); ?></td>
                                        <td><?php echo $this->NombreClienteProveedor($r->__GET('nMovOrigenDestino_Id')); ?></td>
                                        <td><?php echo $r->__GET('Moneda_Id') == 1 ? 'Soles':'Dolares'; ?></td>
                                        <td><?php echo $r->__GET('nMovTotal'); ?></td>
                                        <td><?php echo $r->__GET('nMovTotal') - $r->__GET('nMovTotalCancelado'); ?></td>
                                        <td>
                                            <?php 
                                            if($r->__GET('nMovEstado') == '0'){
                                                echo '<span class="label label-danger">Anulado</span>';
                                            }else if($r->__GET('nMovEstado') == '1'){
                                                echo '<span class="label label-warning">Registrado</span>';
                                            }else if($r->__GET('nMovEstado') == '2'){
                                                echo '<span class="label label-success">Pagado</span>'; 
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <form action="?c=Pago&a=Index" method="POST" style="display: inline-block;">
                                                <input type="hidden" id="Movimiento_Id" name="Movimiento_Id" value="<?php echo $r->__GET('Movimiento_Id'); ?>">
                                                <button type="submit" class="btn btn-danger btn-xs btn-flat"><i class="fa fa-bars"></i></button>
                                            </form>
                                            <a href="#" class="btn btn-warning btn-flat btn-xs" data-toggle="modal" data-target="#creditos" data-codigo="<?php echo $r->__GET('Movimiento_Id'); ?>" data-credito="<?php echo $r->__GET('nMovTotal') - $r->__GET('nMovTotalCancelado'); ?>"><i class="fa fa-credit-card"></i></a>
                                        </td>
                                    </tr>
                                    <?php 
                                        }
                                    }
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Tipo movimiento</th>
                                        <th>Fecha</th>
                                        <th>Documento</th>
                                        <th>Moneda</th>
                                        <th>Total</th>
                                        <th>Estado</th>
                                        <th>Operaciones</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </section><!-- /.content -->
        

        <!-- Modal modal-eliminar -->
        <div class="modal fade" id="creditos" tabindex="-1" role="dialog" aria-labelledby="modal-eliminar-label" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <form action="javascript:GuardarPago();" method="POST">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                            <h4 class="modal-title" id="modal-eliminar-label">Credito</h4>
                        </div>
                        <div class="modal-body">
                            <div id="resultado"></div>
                            <input type="hidden" name="Pago_Id" id="Pago_Id" value="0">
                            <input type="hidden" name="Movimiento_Id" id="Movimiento_Id" value="">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Fecha</label>
                                    <input type="text" class="form-control" name="dPagFecha" id="dPagFecha" placeholder="dd-mm-yyyy" value="<?php echo date('d-m-Y');?>">
                                </div>
                                <div class="col-md-12">
                                    <label>Tipo de pago</label>
                                    <select class="form-control" id="nPagTipoPago_Id" name="nPagTipoPago_Id">
                                        <!--<option value="">Seleccione tipo de pago</option>-->
                                        <option value="1" selected>Efectivo</option>
                                        <option value="2">Banco-Deposito</option>
                                        <option value="3">Banco-Transferencia</option>
                                        <option value="4">Cheque</option>
                                        <option value="5">Letra</option>
                                        <option value="6">Nota de credito</option>
                                    </select>
                                </div>
                                <div class="col-md-12">
                                    <label>Tipo de documento</label>
                                    <textarea class="form-control" name="sPagDocumento" id="sPagDocumento" placeholder="Documento"></textarea>
                                </div>
                                <div class="col-md-12">
                                    <label>Importe</label>
                                    <input type="text" class="form-control" name="nPagImporte" id="nPagImporte" placeholder="0.00">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">No, cancelar</button>
                            <button type="submit" class="btn btn-danger btn-flat">Si, Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /Modal modal-eliminar -->
        <script type="text/javascript" src="view/librerias/dist/js/funcionesMovimiento.js"></script>
        <script type="text/javascript">
            
            $('#dPagFecha').datetimepicker({
               viewMode: 'days',
               format: 'DD-MM-YYYY'
            });   
            $(function() {
                jQuery.fn.dataTableExt.oSort['fecha-asc'] = function(a, b) {
                    var ukDatea = a.split('-');
                    var ukDateb = b.split('-');

                    var x = (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
                    var y = (ukDateb[2] + ukDateb[1] + ukDateb[0]) * 1;

                    return ((x < y) ? -1 : ((x > y) ? 1 : 0));
                };

                jQuery.fn.dataTableExt.oSort['fecha-desc'] = function(a, b) {
                    var ukDatea = a.split('-');
                    var ukDateb = b.split('-');

                    var x = (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
                    var y = (ukDateb[2] + ukDateb[1] + ukDateb[0]) * 1;

                    return ((x < y) ? 1 : ((x > y) ? -1 : 0));
                };
                $("#example1").dataTable({
                    "scrollX": true,
                    "bSort": false,
                    "language": {
                        "search": "Buscar",
                        "lengthMenu": "Visualizar _MENU_ registro por página",
                        "zeroRecords": "No hay información para mostrar",
                        "info": "Pagina _PAGE_ de _PAGES_ de _MAX_ ",
                        "infoEmpty": "Pagina _PAGE_ - _PAGES_ de _MAX_ registros",
                        "infoFiltered": "(filtered from _MAX_ total records)",
                        "paginate": {
                            "previous": "Anterior",
                            "next": "Siguiente"
                        }
                    }
                });
            });
            $('#creditos').on('show.bs.modal', function(event) {
                var button = $(event.relatedTarget); // Button that triggered the modal
                var recipient = button.data('codigo'); // Extract info from data-* attributes
                var por_pagar = button.data('credito');
                // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this);
                modal.find('#Movimiento_Id').val(recipient);
                modal.find('#nPagImporte').val(por_pagar);
            });
            function LimpiarFrmPago(){
                document.getElementById('Pago_Id').value = '0';
                document.getElementById('dPagFecha').value='';
                $('#nPagTipoPago_Id').prop('selectedIndex',0);
                $('#sPagDocumento').val('');
                document.getElementById('nPagImporte').value='';                
            }
            function GuardarPago(){
                var Pago_Id = document.getElementById('Pago_Id').value;
                var Movimiento_Id = document.getElementById('Movimiento_Id').value;
                var dPagFecha = document.getElementById('dPagFecha').value;
                var nPagTipoPago_Id = document.getElementById('nPagTipoPago_Id').value;
                var sPagDocumento = document.getElementById('sPagDocumento').value;
                var nPagImporte = document.getElementById('nPagImporte').value;
                $.post('?c=Creditos&a=GuardarPago',{
                    Pago_Id: Pago_Id,
                    Movimiento_Id: Movimiento_Id,
                    dPagFecha: dPagFecha,
                    nPagTipoPago_Id: nPagTipoPago_Id,
                    sPagDocumento: sPagDocumento,
                    nPagImporte: nPagImporte
                },function(data){
                    $('#resultado').html(data);
                    LimpiarFrmPago();
                });
            }
        </script>
