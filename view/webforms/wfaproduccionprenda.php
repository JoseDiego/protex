
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Plan de Producción
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="wfamain.php"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#">Produccion de Prenda</a></li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div class="row">
						<div class="col-md-4" style="margin-top: 15px;">
                            <a class="btn btn-primary btn-flat" onclick="NuevoPlanProduccion();">Nuevo Plan de Producción</a>
                        </div>
						<br/>
					</div>

					<div class="row">
						<div class="col-md-8" style="margin-top: 15px;">
                            <form action="?c=ProduccionPrenda&a=Index" method="POST">
                                <div class="row">
                                    <!--<div class="col-md-4"><input type="text" class="form-control col-md-4" name="fechaInicio" id="fechaInicio" value="<?php echo isset($_POST['fechaInicio']) ? $_POST['fechaInicio'] : date('d-m-Y', strtotime('-3 month', strtotime(date('d-m-Y')))) ?>"></div>
                                    <div class="col-md-4"><input type="text" class="form-control col-md-4" name="fechaFin" id="fechaFin" value="<?php echo isset($_POST['fechaFin']) ? $_POST['fechaFin'] : date('d-m-Y', strtotime('+3 month', strtotime(date('d-m-Y')))) ?>"></div>
                                    -->
									<div class="col-md-4">
										Fecha Inicio <br/>
										<input type="text" class="form-control col-md-4" name="fechaInicio" id="fechaInicio" value="<?php echo isset($_POST['fechaInicio']) ? $_POST['fechaInicio'] : date('d-m-Y', strtotime(date('d-m-Y'))) ?>">
									</div>

									<div class="col-md-4">
										Fecha Fin <br/>
										<input type="text" class="form-control col-md-4" name="fechaFin" id="fechaFin" value="<?php echo isset($_POST['fechaFin']) ? $_POST['fechaFin'] : date('d-m-Y', strtotime(date('d-m-Y'))) ?>">
									</div>

                                    <div class="col-md-4">
										<br/><button type="submit" class="btn btn-danger btn-flat">Filtrar</button>
									</div>
                                </div>
                            </form>
                        </div>
					</div>

                </div><!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped text-center">
                        <thead>
                            <tr>
                                <th>Nro</th>
                                <th>Fecha</th>
                                <th>Cliente</th>
                                <th>Orden de Trabajo</th>
                                <th>Producto</th>
								                <th>Cant. Progreso</th>
                                <th>Cant. x Hacer Total</th>
                                <th>Nro. Pedido</th>
                                <th>Estado</th>
                                <th>Operaciones</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Nro</th>
                                <th>Fecha</th>
                                <th>Cliente</th>
                                <th>Orden de Trabajo</th>
                                <th>Producto</th>
								                <th>Cant. Progreso</th>
                                <th>Cant. x Hacer Total</th>
                                <th>Nro. Pedido</th>
                                <th>Estado</th>
                                <th>Operaciones</th>
                            </tr>
                        </tfoot>
                    </table>

                    <!-- Modal modeal-ver -->
                    <div class="modal fade" id="modal-ver" tabindex="-1" role="dialog" aria-labelledby="modal-ver-label" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <form action="?c=Usuario&a=reporteUsuarioDatos" method="POST">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                        <h4 class="modal-title" id="modal-ver-label">Registro de Usuario</h4>
                                    </div>
                                    <div class="modal-body">
                                        <table class="table table-striped" id="ver_datos">

                                        </table>
                                    </div>
                                    <div class="modal-footer">
                                        <!--<button type="submit" class="btn btn-danger" formtarget="_blak"><i class="fa fa-print"></i>&nbsp;Imprimir</button>-->
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /Modal modal-ver -->

                    <!-- Modal modal-eliminar -->
                    <div class="modal fade" id="modal-eliminar" tabindex="-1" role="dialog" aria-labelledby="modal-eliminar-label" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <form action="?c=Movimiento&a=AnularMovimiento" method="POST">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                        <h4 class="modal-title" id="modal-eliminar-label">Anular movimiento</h4>
                                    </div>
                                    <div class="modal-body">
                                        <input type="hidden" name="IdMovimiento" id="IdMovimiento" value="">
                                        ¿Estás seguro que desea anular el movimiento?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">No, cancelar</button>
                                        <button type="submit" class="btn btn-danger btn-flat">Si, eliminar</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /Modal modal-eliminar -->

                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<script type="text/javascript" src="view/librerias/dist/js/funcionesProduccionPrenda.js"></script>
<script type="text/javascript">
$('#fechaInicio').datetimepicker({
    viewMode: 'days',
    format: 'DD-MM-YYYY'
});

$('#fechaFin').datetimepicker({
    viewMode: 'days',
    format: 'DD-MM-YYYY'
});
$(function () {
    jQuery.fn.dataTableExt.oSort['fecha-asc'] = function (a, b) {
        var ukDatea = a.split('-');
        var ukDateb = b.split('-');

        var x = (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
        var y = (ukDateb[2] + ukDateb[1] + ukDateb[0]) * 1;

        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    };

    jQuery.fn.dataTableExt.oSort['fecha-desc'] = function (a, b) {
        var ukDatea = a.split('-');
        var ukDateb = b.split('-');

        var x = (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
        var y = (ukDateb[2] + ukDateb[1] + ukDateb[0]) * 1;

        return ((x < y) ? 1 : ((x > y) ? -1 : 0));
    };
    $("#example1").dataTable({
        "columnDefs": [
          { "visible": false, "targets": 0 }
        ],
        "scrollX": true,
        "bSort": false,
        "language": {
            "search": "Buscar",
            "lengthMenu": "Visualizar _MENU_ registro por página",
            "zeroRecords": "No hay información para mostrar",
            "info": "Pagina _PAGE_ de _PAGES_ de _MAX_ ",
            "infoEmpty": "Pagina _PAGE_ - _PAGES_ de _MAX_ registros",
            "infoFiltered": "(filtered from _MAX_ total records)",
            "paginate": {
                "previous": "Anterior",
                "next": "Siguiente"
            }
        }
    });
});

$('#modal-eliminar').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var recipient = button.data('codigo'); // Extract info from data-* attributes

    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this);
    modal.find('.modal-body input').val(recipient);
});
</script>
