<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Nueva Orden Compra Cliente
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="wfamain.php"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li><a href="#">Orden Compra</a></li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-primary">
        <form action="javascript:GuardarOrdencompra();" method="POST" id="FrmMovimiento">
          <br/>
          <div class="col-md-10" style="border-width:2px; border-style:solid; border-color: #3C8DBC; margin: auto;">
            <div class="box-header">
              <h3 class="box-title">Registrar Orden de Compra	</h3>
            </div>

            <div class="box-body">
              <div id="resultado"></div>

              <div class="row">
                <div class="col-md-8 form-group">
                  <input type="hidden" id="OrdenCompra_Id" name="OrdenCompra_Id" value="<?php echo isset($r) ? $r->__GET('Pedido_Id'):''; ?>">

                  <label>Cliente</label> <br/>

                  <select class="form-control" name="ListarCliente" id="ListarCliente"  data-live-search="true"  data-size="5" required>
                    <option value="">Cargando...</option>
                  </select>
                </div>

              </div>

              <div class="row">
                <div class="col-md-5 form-group">
                  <input type="hidden" id="Movimiento_Id" name="Movimiento_Id" value="">
                  <label>Fecha</label>
                  <br/>
                  <?php  if(isset($r)){$date = date_create($r->__GET('dPedFecha')); $fecha=date_format($date, 'd-m-Y');}else{} ?>

                  <input type="text" class="form-control" name="dMovFecha" id="dMovFecha" placeholder="Fecha" value="<?php echo isset($date) ? $fecha : ''; ?>" required>
                </div>

                <div class="col-md-2 form-group">
                </div>

                <div class="col-md-5 form-group">
                  <label>Nro. Orden de Compra</label>
                  <br/>
                  <input type="text" class="form-control" name="sMovDocumento" id="sMovDocumento" placeholder="Nro" value="<?php echo isset($r) ? $r->__GET('nroOrdenTrabajo'):'';?>" required>
                </div>
              </div>

              <div class="row">
                <div class="col-md-5 form-group">
                  <label>Tipo Pago</label>
                  <br/>
                  <select class="form-control" id="selectTipoPago" onchange="credito()" required>
                    <option value="">Seleccione Tipo Pago</option>
                    <option value="1" <?php echo isset($r) ? $r->__GET('nPedTipopago') == 1 ? 'selected':'' :''; ?>>Contado</option>
                    <option value="2" <?php echo isset($r) ? $r->__GET('nPedTipopago') == 2 ? 'selected':'' :''; ?>>Credito</option>
                    <!-- -->
                  </select>
                </div>

                <div class="col-md-2 form-group">
                </div>

                <div class="col-md-5 form-group">
                  <label>Importe Adelantado</label>
                  <input type="number"  class="form-control" id="importeAd" onkeyup="CalcularSaldo()" value="<?php echo isset($r) ? $r->__GET('dPedImporteAd'):'0.00';?>" placeholder="0.00" disabled="true">
                </div>
              </div>

              <div class="row">
                <div class="col-md-5 form-group">
                  <label>Moneda</label>
                  <br/>
                  <select class="form-control" name="Moneda_Id" id="Moneda_Id" onchange="TipoMoneda()" required>
                    <option value="">Seleccione tipo moneda</option>
                    <option value="1" <?php echo isset($r) ? $r->__GET('Moneda_Id') == 1 ? 'selected':'' :''; ?>>Soles (S/)</option>
                    <option value="2" <?php echo isset($r) ? $r->__GET('Moneda_Id') == 2 ? 'selected':'' :''; ?>>Dolares ($)</option>
                  </select>
                </div>
              </div>

            </div>
          </div>

          <br/>
          <div class="row">
          </div>
          <br/>

          <div class="col-md-10">
            <div class="row">
              <div class="col-md-10">
                <table class="table table-bordered table-striped text-center">
                  <thead>
                    <tr>
                      <th>Cantidad</th>
                      <th>Producto</th>
                      <th>Unidad</th>
                      <th>Prec. Unitario</th>
                      <th>Sub Total</th>
                    </tr>
                  </thead>
                  <tbody id="productos_servicios">

                    <tr>
                      <td colspan="5">No hay datos</td>
                    </tr>
                  </tbody>
                </table>
              </div>

              <div class="col-md-2">
                <a class="btn btn-success"  onclick="ValidarModalProducto()"><span class="glyphicon glyphiconglyphicon glyphicon-plus"></span>&nbsp;&nbsp;Agregar</a>
              </div>
            </div>

            <div class="row">
              <div id="resultado_productos"></div>
              <div class="col-md-8">
                <div class="form-group">
                  <label>Observación</label>
                  <textarea class="form-control" rows="3" id="sMovObservacion">
                    <?php echo isset($r) ? $r->__GET('sPedObservaciones'):''; ?>
                  </textarea>
                </div>
              </div>

              <div class="col-md-4">
                <div class="row">
                  <div class="col-md-6" style="text-align:right;">
                    <label>IMPORTE</label>
                  </div>

                  <div class="col-md-6">
                    <input type="text" class="form-control" name="dMovImporte" id="dMovImporte" placeholder="0.00"  value="<?php echo isset($r) ? $r->__GET('dPedImporte'):'0.00'; ?>" disabled="true">
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-6" style="text-align:right;">
                    <label>IGV</label>
                  </div>

                  <div class="col-md-6">
                    <input type="text" class="form-control" name="dMovImporte" id="dMovIGV" placeholder="0.00" value="<?php echo isset($r) ? $r->__GET('dPedIGV'):'0.00'; ?>" disabled="true">
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-6" style="text-align:right;">
                    <label>TOTAL</label>
                  </div>
                  <div class="col-md-6" style="text-align:right;">
                    <input type="text" class="form-control" name="dMovImporte" id="dMovTotal" placeholder="0.00" value="<?php echo isset($r) ? $r->__GET('dPedTotal'):'0.00'; ?>" disabled="true">
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6" style="text-align:right;">
                    <label>SALDO</label>
                  </div>
                  <div class="col-md-6" style="text-align:right;">
                    <input type="text" class="form-control" name="dMovImporte" id="dMovSaldo" placeholder="0.00" value="<?php echo isset($r) ? $r->__GET('dPedSaldo'):'0.00'; ?>" disabled="true">
                  </div>
                </div>
              </div>
            </div>

          </div>

          <div class="col-md-10">
            <div class="row">
              <div class="col-md-8"></div>

              <div class="col-md-4" style="top: 10px;">
                <table>
                  <tr>
                    <td align="right">
                      <a href="?c=Ordencompra&a=Index" class="btn btn-danger btn-flat">Cancelar</a>
                      <button type="submit" class="btn btn-primary btn-flat">Guardar información</button>
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
          <!-- /.box-header -->

          <!-- /.box-body -->

        </form>
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->
<!-- /Modal modal-eliminar -->
<div class="modal fade" id="agregar_productos" tabindex="-1" role="dialog" aria-labelledby="modal-eliminar-label" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content" id="FrmModalProductos"><!-- formulario de productos -->
      <form action="javascript:AgregarProductoC();" method="POST" id="FrmModalProductos">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" onclick="document.getElementById('mensajeModal').style.display = 'none'; LimpiarFrmProductos(); "><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
          <h4 class="modal-title" id="modal-eliminar-label">Cargar Productos</h4>
        </div>
        <div class="modal-body">
          <div id="resultado_ingresocompra"></div>
          <div class="row">
            <div class="col-md-12 form-group">
              <label>Productos</label>
              <select class="form-control" id="ProdServ_Id" name="ProdServ_Id" onclick="ProductosUNILista();limpiar()" required>
                <option value="">Seleccionar producto</option>
                <?php
                $productos = $this->prodserv_model->Listar();
                foreach ($productos as $p){
                  if($p->__GET('nProSrvFamilia_Id') == 14){
                    ?>
                    <option value="<?php echo $p->__GET('ProdServ_Id'); ?>"><?php echo $p->__GET('sProSrvNombre'); ?></option>
                    <?php
                  }}
                  ?>
                </select>
              </div>
              <div class="col-md-6">
                <div class="col-md-12 form-group" style="padding-left: 0px; padding-right: 0px;">
                  <label>Cantidad</label>
                  <input type="number" class="form-control" min="0" onchange="CalcularSubtotal()" onkeyup="CalcularSubtotal()" name="sMovDetConCantidad" id="sMovDetConCantidad" placeholder="Cantidad" value="0" required>
                </div>

              </div>
              <div class="col-md-6">
                <div class="col-md-12 form-group" style="padding-left: 0px; padding-right: 0px;">
                  <label>Unidad</label>
                  <select class="form-control" name="Unidadmedida_Id" id="Unidadmedida_Id" onclick="limpiar()" onchange="PrecioUnitario();CalcularSubtotal()" required="">
                    <option value="">Seleccione unidad.</option>
                  </select>
                </div>
                <div class="col-md-12 form-group" style="padding-left: 0px; padding-right: 0px;">
                  <label>P. Unitario</label>
                  <input type="text" class="form-control" onkeyup="CalcularSubtotal()" name="nMovDetConPrecioUnitario" id="nMovDetConPrecioUnitario" placeholder="P. Unitario" value="0.00" disabled="true" required>
                </div>
                <div class="col-md-12 form-group" style="padding-left: 0px; padding-right: 0px;">
                  <label>Sub total</label>
                  <input type="text" class="form-control" name="nMovDetConSubtotal" id="nMovDetConSubtotal" placeholder="Sub total" disabled="true" required>
                </div>
              </div>

            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="document.getElementById('mensajeModal').style.display = 'none'; LimpiarFrmProductos(); ">No, cancelar</button>
            <button type="submit" class="btn btn-danger btn-flat">Si, Agregar</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="modal fade" id="eliminar_productos" tabindex="-1" role="dialog" aria-labelledby="modal-eliminar-label" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content" id="FrmModalProductos">
        <form action="javascript:EliminarProductoList();" method="POST" id="FrmModalProductos">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
            <h4 class="modal-title" id="modal-eliminar-label">ELIMINAR PRODUCTO</h4>
          </div>
          <div class="modal-body">
            <input type="hidden" name="Codigo_Producto" id="Codigo_Producto">
            ¿Desea eliminar al producto de la lista?
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-flat" data-dismiss="modal" onclick="document.getElementById('mensajeModal').style.display = 'none'; LimpiarFrmProductos();">No, cancelar</button>
            <button type="submit" class="btn btn-danger btn-flat">Si, Eliminar</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <!-- /Modal modal-eliminar -->

  <script src="view/librerias/dist/js/funcionesOrdenCompra.js"></script>


  <script type="text/javascript">
  $(document).ready(function(){
    //TipoPrvClien();
    listarCliente();
    <?php if(isset($r)){?>
      CargarDetalles();
      <?php } else{}?>
      $(document).on('change', '#Unidadmedida_Id', function () {
        if ($(this).val() !== '') {
          $('#nMovDetConPrecioUnitario').prop("disabled", false)
        }
      })
    });

    function credito(){
      var pago = document.getElementById('selectTipoPago').value;
      if(pago == '2'){
        document.getElementById('importeAd').disabled=false;
      }
      else{document.getElementById('importeAd').disabled=true;}
    }
    function listarCliente(){
      var OrigenDestino_Id=0;
      $.post('?c=ordencompra&a=ListarComboBoxClientes',{
        OrigenDestino_Id: OrigenDestino_Id
      },function(data){
        $("#ListarCliente").html(data);
        document.getElementById('ListarCliente').value = <?php echo isset($r) ? $r->__GET('OrigenDestino_Id'):'0'; ?>



      });
    }
    $('#dMovFecha').datetimepicker({
      viewMode: 'days',
      format: 'DD-MM-YYYY'
    });
    function CargarDetalles(){
      var Pedido_Id = document.getElementById('OrdenCompra_Id').value;
      $.post('?c=ordencompra&a=ListarDetalles', {
        Pedido_Id : Pedido_Id
      }, function(data) {
        $("#productos_servicios").html(data);
      });
    }
    function TipoMoneda(){
      var moneda = $("#TipoMoneda");
      var Moneda_Id = document.getElementById('Moneda_Id').value;
      if(Moneda_Id == 1){
        moneda.removeClass('col-md-3');
        moneda.addClass('col-md-6');
        $("#TipoCambio").hide();
        $("#nMovTipoCambio").val("1");
      }else{
        moneda.removeClass('col-md-6');
        moneda.addClass('col-md-3');
        $("#TipoCambio").show();
        $("#nMovTipoCambio").val("");
      }
    }
    $('#eliminar_productos').on('show.bs.modal', function(event) {
      var button = $(event.relatedTarget); // Button that triggered the modal
      var recipient = button.data('productoid'); // Extract info from data-* attributes

      // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
      // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
      var modal = $(this);
      modal.find('.modal-body input').val(recipient);
    });
    function ValidarModalProducto(){
      $("#agregar_productos").modal('show');
    }
    function ProductosUNILista(){
      var ProdServ_Id = document.getElementById('ProdServ_Id').value;
      $.post('?c=Movimiento&a=ListarUnidades', {
        ProdServ_Id: ProdServ_Id
      }, function(data){
        $("#Unidadmedida_Id").html(data);
        document.getElementById('sMovDetConCantidad').value = 1;
      });
    }



    function AgregarProductoC(){
      var ProdServ_Id = document.getElementById('ProdServ_Id').value;
      var Unidadmedida_Id = document.getElementById('Unidadmedida_Id').value;
      var sMovDetConCantidad = document.getElementById('sMovDetConCantidad').value;
      var nMovDetConSubtotal = document.getElementById('nMovDetConSubtotal').value;
      var nMovDetConPrecioUnitario = document.getElementById('nMovDetConPrecioUnitario').value;
      var nIncluyeIGV = 0;
      if($("#nIncluyeIGV").prop("checked") == true){
        nIncluyeIGV = 1;
      }
      $.post('?c=Ordencompra&a=AgregarProductoC', {
        ProdServ_Id: ProdServ_Id,
        Unidadmedida_Id: Unidadmedida_Id,
        sMovDetConCantidad: sMovDetConCantidad,
        nMovDetConSubtotal: nMovDetConSubtotal,
        nMovDetConPrecioUnitario: nMovDetConPrecioUnitario,
        nIncluyeIGV: nIncluyeIGV
      }, function (data) {
        $("#resultado_ingresocompra").html(data);
        LimpiarFrmProductos();
        ListarProductos();
        CalcularImpTotIGV();
        document.getElementById('Moneda_Id').disabled=true;
      });
    }
    function ListarProductos() {
      $.post('?c=Ordencompra&a=ListarProductos', {
      }, function(data) {
        $("#productos_servicios").html(data);
      });
    }
    function EliminarProductoList(){
      var Codigo_Producto = document.getElementById('Codigo_Producto').value;
      $.post('?c=Ordencompra&a=EliminarProductoLista',{
        Codigo_Producto: Codigo_Producto
      }, function(data){
        $("#resultado").html(data);
        ListarProductos();
        CalcularImpTotIGV();
        $("#eliminar_productos").modal("hide");
      });
    }

    </script>
