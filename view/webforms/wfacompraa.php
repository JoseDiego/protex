
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Nueva Orden Compra Protex
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="wfamain.php"><i class="fa fa-dashboard"></i> Inicio</a></li>
                <li><a href="#">Compras</a></li>
            </ol>
        </section>        
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-primary">
                        <form action="javascript:GuardarMovimiento();" method="POST" id="FrmMovimiento">
							<br/>
							<div class="col-md-10" style="border-width:2px; border-style:solid; border-color: #3C8DBC; margin: auto;">
								<div class="box-header">
									<h3 class="box-title">Registrar Compra Protex</h3>
								</div>
								
								<div class="box-body">
									<div id="resultado"></div>
									
									<div class="row">
										<div class="col-md-8 form-group">
											<input type="hidden" id="OrdenCompra_Id" name="OrdenCompra_Id">
										</div>
										
										<div class="col-md-4 form-group">
											<label>Nro de Compra</label>
                                                                                        <input type="text" name=nroCompra id=nroCompra class="form-control" placeholder="Nro." required>
										</div>
									</div>
									
									<div class="row">
										<div class="col-md-12 form-group">
											
											<label>Proveedor</label> <br/>
											
                                                                                        <select class="form-control" name="Proveedor_Id" id="Proveedor_Id"  onchange="javascript:cargarProductos();" data-live-search="true"  data-size="5">
												<option value="">Seleccione Proveedor</option>
											</select>
										</div>
									</div>
									
									<div class="row">
										<div class="col-md-5 form-group">
											<label>Tipo de Pago</label>
											<br/>
                                                                                        <select class="form-control" id="tipopago" name="tipopago" onchange="credito();">
												<option value="">Seleccione Pago</option>
												<option value="1">Contado</option>
												<option value="2">Credito</option>
											</select>
										</div>
										
										<div class="col-md-2 form-group">										
										</div>
																			
										<div class="col-md-5 form-group">
											<label>Importe Adelantado</label>
											<br/>
                                                                                        <input type="number" id="importeAd" name="importeAd"  onkeyup="CalcularSaldo()" class="form-control" placeholder="S/ 0.00" disabled="true">
										</div>
									</div>
									
									<div class="row">
										<div class="col-md-5 form-group">											
											<label>Moneda</label>
											<br/>
											<select class="form-control" name="Moneda_Id" id="Moneda_Id" onchange="TipoMoneda()">                                        
												<!--<option value="">Seleccione tipo moneda</option>-->
												<option value="1">Soles (S/)</option>
												<option value="2">Dolares ($)</option>
											</select>										
										</div>
										
										<div class="col-md-2 form-group">
										</div>
										
										<div class="col-md-5 form-group">
											<label>Estado</label>
											<br/>
											<select id=estado name="estado" class="form-control" disabled>
												<!--<option value="">Seleccione tipo moneda</option>-->
												<option value="0">Estado</option>
                                                                                                <option value="1">Pagado</option>
                                                                                                <option value="2">Deuda</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							
							<br/>
							<div class="row">
							</div>
							<br/>
							
                            <div class="col-md-10">
								<div class="row">
									<div class="col-md-10">
										<table class="table table-bordered table-striped text-center">
                                            <thead>
                                                <tr>
                                                    <th>Cantidad</th>
                                                    <th>Unidad</th>
						    <th>Producto</th>													
                                                    <th>Prec. Unitario</th>
                                                    <th>Sub Total</th>                                                    
                                                </tr>
                                            </thead>
                                            <tbody id="productos_servicios">
                                                <tr>
                                                    <td colspan="5">No hay datos</td>
                                                </tr>
                                            </tbody>
                                        </table>
									</div>
									
									<div class="col-md-2">
										<a class="btn btn-success" onclick="ValidarModalProducto()"><span class="glyphicon glyphiconglyphicon glyphicon-plus"></span>&nbsp;&nbsp;Agregar</a>
								        </div>
								</div>
								
								<div class="row">
                                                                    <div id="resultado_productos"></div>
									<div class="col-md-8">
                                        <div class="form-group">
                                            <label>Observación</label>
                                            <textarea class="form-control" rows="3"></textarea>
                                        </div>
                                    </div>
									
									<div class="col-md-4">
										<div class="row">
											<div class="col-md-6" style="text-align:right;">
												<label>IMPORTE</label>
											</div>
											
											<div class="col-md-6">
                                                                                            <input type="text" name="dMovImporte" id="dMovImporte" class="form-control" placeholder="0.00" disabled="true">
											</div>
										</div>
										
										<div class="row">
											<div class="col-md-6" style="text-align:right;">
												<label>IGV</label>
											</div>
											
											<div class="col-md-6">
                                                                                            <input type="text" name="dMovImporte" id="dMovIGV" class="form-control" placeholder="0.00" disabled="true">
											</div>
										</div>
										
										<div class="row">
											<div class="col-md-6" style="text-align:right;">
												<label>TOTAL</label>
											</div>
											<div class="col-md-6" style="text-align:right;">
                                                                                            <input type="text" name="dMovImporte" id="dMovTotal" class="form-control" placeholder="0.00" disabled="true">
											</div>
										</div>
                                                                                <div class="row">
											<div class="col-md-6" style="text-align:right;">
												<label>SALDO</label>
											</div>
											<div class="col-md-6" style="text-align:right;">
                                                                                            <input type="text" class="form-control" name="dMovImporte" id="dMovSaldo" placeholder="0.00" value="<?php echo isset($r) ? $r->__GET('dPedSaldo'):'0.00'; ?>" disabled="true">
											</div>
										</div>
									</div>
								</div>
								
							</div>
							
							<div class="col-md-10">								
								<div class="row">
									<div class="col-md-8">
										<button type="submit" class="btn btn-primary btn-flat">Registro de Facturas</button>
									</div>
									
									<div class="col-md-4">
										<table>
											<tr>
												<td align="right">
													<a href="?c=Movimiento&a=Index" class="btn btn-danger btn-flat">Cancelar</a>
													<button type="submit" class="btn btn-primary btn-flat">Guardar información</button>
												</td>
											</tr>
										</table>										
									</div>													
								</div>
							</div>
                            <!-- /.box-header -->
                            
                            <!-- /.box-body -->
                            
                        </form>
                    </div><!-- /.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </section><!-- /.content -->
        
        
        <!-- /Modal modal-eliminar -->
        <div class="modal fade" id="agregar_productos" tabindex="-1" role="dialog" aria-labelledby="modal-eliminar-label" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content" id="FrmModalProductos"><!-- formulario de productos -->
					<form action="javascript:AgregarProductoC();" method="POST" id="FrmModalProductos">
					    <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" onclick="LimpiarFrmProductos() "><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
					        <h4 class="modal-title" id="modal-eliminar-label">Cargar Productos</h4>
					    </div>
					    <div class="modal-body">   
					        <div id="resultado_ingresocompra"></div>
					        <div class="row">
					            <div class="col-md-12 form-group">
					                <label>Productos</label>
                                                        <select class="form-control" id="ProdServ_Id" name="ProdServ_Id" onclick="ProductosUNILista()" required>
					                    
					                    
					                </select>
					            </div>
					            <div class="col-md-6">
					                <div class="col-md-12 form-group" style="padding-left: 0px; padding-right: 0px;">
					                    <label>Cantidad</label>
					                    <input type="number" class="form-control" onkeyup="CalcularSubtotal()" name="sMovDetConCantidad" id="sMovDetConCantidad" placeholder="Cantidad" value="0" required>
					                </div>

					            </div>
					            <div class="col-md-6">
					                <div class="col-md-12 form-group" style="padding-left: 0px; padding-right: 0px;">
					                    <label>Unidad</label>
                                                            <select class="form-control" name="Unidadmedida_Id" id="Unidadmedida_Id" onchange="PrecioUnitario()" required="">
					                        <option value="">Seleccione unidad.</option>
					                    </select>
					                </div>
					                <div class="col-md-12 form-group" style="padding-left: 0px; padding-right: 0px;">
					                    <label>P. Unitario</label>
                                                            <input type="text" class="form-control" onkeyup="CalcularSubtotal()" name="nMovDetConPrecioUnitario" id="nMovDetConPrecioUnitario" placeholder="P. Unitario" value="0.00" disabled="true" required>
					                </div>
					                <div class="col-md-12 form-group" style="padding-left: 0px; padding-right: 0px;">
					                    <label>Sub total</label>
                                                            <input type="text" class="form-control" name="nMovDetConSubtotal" id="nMovDetConSubtotal" placeholder="Sub total" disabled="true" required>
					                </div>
					            </div>
					            
					        </div>
					    </div>
					    <div class="modal-footer">
					        <button type="button" class="btn btn-default" data-dismiss="modal" onclick=" LimpiarFrmProductos() ">No, cancelar</button>
					        <button type="submit" class="btn btn-danger btn-flat">Si, Agregar</button>
					    </div>
					</form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="eliminar_productos" tabindex="-1" role="dialog" aria-labelledby="modal-eliminar-label" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content" id="FrmTipomovimiento">
                    <form action="javascript:EliminarProductoLista();" method="POST" id="FrmIngresoCompra">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                            <h4 class="modal-title" id="modal-eliminar-label">ELIMINAR PRODUCTOS</h4>
                        </div>
                        <div class="modal-body"> 
                            <input type="hidden" name="Codigo_Producto" id="Codigo_Producto">
                            ¿Desea eliminar al producto de la lista?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">No, cancelar</button>
                            <button type="submit" class="btn btn-danger btn-flat">Si, Eliminar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
       
        <!-- /Modal modal-eliminar -->
        <script type="text/javascript" src="view/librerias/dist/js/funcionesCompra.js" ></script>
        
        <script type="text/javascript" src="view/librerias/dist/js/funcionesCompra.js"></script>
        
        <script type="text/javascript">
 $(document).ready(function(){
                
                listarProveedor();
                
            });
            
            


            $('#dMovFecha').datetimepicker({
               viewMode: 'days',
               format: 'DD-MM-YYYY'
            });     
            function TipoMoneda(){
                var moneda = $("#TipoMoneda");
                var Moneda_Id = document.getElementById('Moneda_Id').value;
                if(Moneda_Id == 1){
                    moneda.removeClass('col-md-3');
                    moneda.addClass('col-md-6');
                    $("#TipoCambio").hide();
                    $("#nMovTipoCambio").val("1");
                }else{
                    moneda.removeClass('col-md-6');
                    moneda.addClass('col-md-3');
                    $("#TipoCambio").show();
                    $("#nMovTipoCambio").val("");
                }
            }
            $('#eliminar_productos').on('show.bs.modal', function(event) {
                var button = $(event.relatedTarget); // Button that triggered the modal
                var recipient = button.data('productoid'); // Extract info from data-* attributes

                // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this);
                modal.find('.modal-body input').val(recipient);
            });
            
</script>