<form action="javascript:AgregarProductoLote();" method="POST" id="FrmSalidaVenta">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
        <h4 class="modal-title" id="modal-eliminar-label">SALIDA / VENTA</h4>
    </div>
    <div class="modal-body">  
        <div id="resultado_saluda_venta"></div>
        <div class="row">
            <div class="col-md-12 form-group">
                <label>Productos</label>
                <select class="form-control" id="ProdServ_Id" name="ProdServ_Id" onchange="ListarProductosLote();">
                    <option value="">Seleccionar producto</option>
                    <?php 
                    foreach ($productos as $p){
                    ?>
                    <option value="<?php echo $p->__GET('ProdServ_Id'); ?>"><?php echo $p->__GET('sProSrvNombre'); ?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
            <div class="col-md-12 form-group">
                <table class="table table-bordered text-center">
                    <thead>
                        <tr>
                            <th>Check</th>
                            <th>Lote</th>
                            <th>Cantidad</th>
                        </tr>
                    </thead>
                    <tbody id="lista_lotes">
                        <tr>
                            <td colspan="3">No hay datos</td> 
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-6">
                <div class="col-md-12 form-group" style="padding-left: 0px; padding-right: 0px;">
                    <label>UNIDAD</label>
                    <select class="form-control" name="Unidadmedida_Id" id="Unidadmedida_Id">
                        <option value="">Seleccione unidad.</option>
                    </select>
                </div>
                <div class="col-md-12 form-group" style="padding-left: 0px; padding-right: 0px;">
                    <label>CAMTIDAD</label>
                    <input type="text" class="form-control" onkeyup="CalcularSubtotal()" name="sMovDetConCantidad" id="sMovDetConCantidad" placeholder="Cantidad">
                </div>
            </div>
            <div class="col-md-6">
                <div class="col-md-12 form-group" style="padding-left: 0px;padding-right: 0px">
                    <label>PRECIO DE VENTA</label>
                    <input type="text" class="form-control" onkeyup="CalcularSubtotal()" name="nMovDetConPrecioUnitario" id="nMovDetConPrecioUnitario" placeholder="Precio de venta">
                </div>
                <div class="col-md-12 form-group" style="padding-left: 0px;padding-right: 0px">
                    <label>SUB TOTAL</label>
                    <input type="text" class="form-control" name="nMovDetConSubtotal" id="nMovDetConSubtotal" placeholder="Sub total">
                </div>
            </div>
            <div class="col-md-12 text-center">
                <input type="checkbox" name="nIncluyeLoteIGV" id="nIncluyeLoteIGV">&nbsp;<b>Incluido IGV</b>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">No, cancelar</button>
        <button type="submit" class="btn btn-danger btn-flat">Si, Agregar</button>
    </div>
</form>
<script type="text/javascript">      
    jQuery.validator.addMethod("decimales", function (value, element) {
        return this.optional(element) || /^([0-9]+\.+[0-9]|[0-9])+$/.test(value);
    });
    $("#FrmSalidaVenta").validate({
        errorClass: 'error',
        rules: {       
            Unidadmedida_Id: {
                required: true
            },
            sMovDetConCantidad: {
               required: true,
               number: true
            },
            nMovDetConPrecioUnitario: {
                required: true,
                decimales: true
            },
            nMovDetConSubtotal: {
                required: true,
                decimales: true
            }
        },
        messages: {     
            Unidadmedida_Id: {
                required: "Seleccione unidad."  
            },
            sMovDetConCantidad: {
                required: "Ingrese cantidad por favor.",
                number: "Permitodo solo numeros."
            },     
            nMovDetConPrecioUnitario: {
                required: "Ingrese precio unitario por favor.",
                decimales: "Solo esta permitido numeros enteros y decimales por favor."
            },
            nMovDetConSubtotal: {
                required: "Ingrese subtotal por favor.",            
                decimales: "Permitido numero decimales"
            }
        }
    });
</script>