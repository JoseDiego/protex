            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Productos
                    <small></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
                    <li class="active">Registrar Productos</li>
                </ol>
            </section>
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-5">
                        <div class="box box-primary">
                            <div class="box-header">
                                <h3 class="box-title">Datos básicos</h3>
                            </div><!-- /.box-header -->
                            <!-- form start -->
                            <form role="form" action="?c=Productos&a=GuardarProdserv" method="POST" id="formuploadajax" enctype="multipart/form-data">
                                <div class="box-body">
                                    <div id="resultado"></div>
                                    <div class="row">
                                        <div class="col-md-12 form-group">
                                            <input type="hidden" name="ProdServ_Id" id="ProdServ_Id" value="<?php echo isset($r) ? $r->__GET('ProdServ_Id'):'0';?>">
                                            <label>Codigo</label>
                                            <div class="row">   
                                                <?php
                                                isset($r) ? $codigo = explode('-', $r->__GET('sProSrvCodigo')):"";
                                                ?>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control" name="sProSrvCodigo1" id="sProSrvCodigo1" placeholder="0" value="<?php echo isset($codigo) ? $codigo[0] :'';?>" readonly="readonly">
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control" name="sProSrvCodigo2" id="sProSrvCodigo2" placeholder="0" value="<?php echo isset($codigo) ? $codigo[1]:'';?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <label>Familia</label>
                                            <select class="form-control" name="Familia_Id" id="Familia_Id"  onchange="UltimoCodigo();" required="required">
                                                <option value="">Seleccione familia</option>
                                                <?php 
                                                foreach ($familia as $fa){
                                                    if($fa->__GET('nFamPadre') != 0 && $fa->__GET('Familia_Id')!=2 && $fa->__GET('Familia_Id')!=3){
                                                ?>
                                                <option value="<?php echo $fa->__GET('Familia_Id'); ?>" <?php echo isset($r) ? $r->__GET('nProSrvFamilia_Id') == $fa->__GET('Familia_Id') ? 'selected':'' :''; ?>><?php echo $fa->__GET('sFamCodigo').'- '.$fa->__GET('sFamDescripcion'); ?></option>
                                                    <?php 
                                                   
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        
                                        <div class="col-md-12 form-group">
                                            <label>Nombre de producto</label>
                                            <input type="text" class="form-control" name="sProSrvNombre" id="sProSrvNombre" placeholder="Nombre de producto" value="<?php echo isset($r) ? $r->__GET('sProSrvNombre'):'';?>" required="required">
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <label>Unidad</label>
                                            <select class="form-control" name="Unidadmedida_Id" id="Unidadmedida_Id" required="required">
                                                <option value="">Seleccione unidad</option>
                                                <?php 
                                                $unipre = "";
                                                foreach ($unidadmedida as $uni){
                                                    if($uni->__GET('UnidadMedida_Id')!=1){
                                                        if($this->VerificarPadre($uni->__GET('UnidadMedida_Id')) == true || $uni->__GET('nUndPadre_Id') == 1){
                                                ?>
                                                <option value="<?php echo $uni->__GET('UnidadMedida_Id'); ?>" 
                                                    <?php 
                                                    if(isset($r)){
                                                        if($r->__GET('nProSrvUnidad_Id') == $uni->__GET('UnidadMedida_Id')){
                                                            echo 'selected';
                                                            $unipre = $uni->__GET('sUndAlias');
                                                        }
                                                    }
                                                    ?>><?php echo $uni->__GET('sUndDescripcion'); ?></option>
                                                        
                                                <?php 
                                                        }
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <label>Especificación</label>
                                            <textarea style="overflow:auto;resize:none" type="textarea" class="form-control" name="ProSrvEspecificacion" id="ProSrvEspecificacion" placeholder="Escriba las especificaciones del producto"><?php if(isset($r)){echo $r->__GET('ProSrvEspecificacion');}?></textarea>
                                        </div>
<!--                                        <div class="col-md-6 form-group">
                                            <label>Unidad present</label>
                                            <input type="text" class="form-control" name="" id="" disabled placeholder="Unidad presente" value="<?php echo $unipre; ?>">
                                        </div>    -->
                                        <!--<div class="col-md-6 form-group">
                                            <label>Activo</label>
                                            <select class="form-control" name="Activo_Id" id="Activo_Id">                                                
                                                <option value="">Seleccione activo</option>-->
                                                <?php 
                                                //foreach ($activos as $act){
                                                ?>
                                                <input type="hidden" name="Activo_Id" id="Activo_Id" value="1">
                                                <!--
                                                <option value="<?php //echo $act->__GET('Activo_Id'); ?>" <?php //echo isset($r) ? $r->__GET('Activo_Id') == $act->__GET('Activo_Id') ? 'selected':'' :''; ?>><?php //echo $act->__GET('sActDescripcion'); ?></option>
                                                -->
                                                <?php 
                                                //}
                                                ?>
                                            <!--</select>
                                        </div>-->
                                        <!--<div class="col-md-6 form-group">
                                            <label>Cód Barras</label>-->
                                            <input type="hidden" class="form-control" name="sProSrvCodigoBarras" id="sProSrvCodigoBarras" placeholder="Cod. Barras" value="<?php echo isset($r) ? $r->__GET('sProSrvCodigoBarras'):''; ?>">
                                        <!--</div>-->                              
                                        <div class="col-md-12 form-group">
                                            <label>Estado</label>
                                            <select class="form-control" name="nProSrvEstado" id="nProSrvEstado">
                                                <option <?php if(isset($r)){ echo $r->__GET('nProSrvEstado') == 1 ? 'selected' : ''; } ?> value="1">Habilitado</option>
                                                <option <?php if(isset($r)){ echo $r->__GET('nProSrvEstado') == 0 ? 'selected' : ''; } ?> value="0">Deshabilitado</option>
                                            </select>
                                        </div> 
                                        <!--<label>Exonerado de IGV &nbsp;&nbsp;-->
                                        <input type="hidden" name="nProSrvExoneradoIGV" id="nProSrvExoneradoIGV" value="0">
                                        <!--</label>-->
                                        <!-- CARGAR IMAGEN-->
                                        <div class="col-md-12">
                                                <label>Subir Imagen</label>
                                                <input id="files" name="files" type="file" value="<?php echo isset($r) ? $r->__GET('ProSrvImagen'):'';?>" /><br>
                                        </div>
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary btn-flat" data-toggle="tooltip" title="Guardar nuevo viajero">
                                        Guardar informacion
                                        </button>
                                        <a href="?c=Productos&a=Index" class="btn btn-danger btn-flat">Cancelar</a>
                                    </div>               
                                </div>
                            </form>
                        </div><!-- /.box -->
                    </div><!--/.col (left) --> 

                    <div class="col-md-7">
                        <div class="box box-primary">
                            <div class="box-header">
                                <h4>Patron</h4>
                            </div>
                            <div class="box-body">
                                <div id="resultado_recete"></div>
<!--                                <button class="btn btn-danger btn-flat text-bold">AGREGAR RECETA&nbsp;<i class="fa fa-coffee"></i></button>
                                <hr style="margin-top: 5px; margin-bottom: 5px;">-->
                                <table class="table table-bordered table-striped text-center">
                                    <thead>
                                        <tr>
                                            <th style="width: 30%;">Producto</th>
                                            <th style="width: 30%;">Unidad de medida</th>
                                            <th style="width: 20%;">Cantidad</th>
                                            <th style="width: 20%;">Operaciones</th>
                                        </tr>
                                        <tr>
                                            <td style="width: 30%;">
                                                <select class="form-control" name="Receta_ProdServ_Id" id="Receta_ProdServ_Id" onchange="ProductosUnidades();">
                                                    <option value="">Seleccione producto</option>
                                                    <?php 
                                                    foreach ($productos as $pro){
                                                    ?>
                                                    <option value="<?php echo $pro->__GET('ProdServ_Id'); ?>">
                                                        <?php echo $pro->__GET('sProSrvNombre'); ?>
                                                    </option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </td>
                                            <td style="width: 30%;">
                                                <select class="form-control" name="Receta_UnidadMedida_Id" id="Receta_UnidadMedida_Id" disabled>
                                                    <option value="">Seleccione unidad</option>
                                                    <?php 
                                                    foreach ($unidadmedida as $uni){
                                                        if($uni->__GET('UnidadMedida_Id')!=1){
                                                    ?>
                                                    <option value="<?php echo $uni->__GET('UnidadMedida_Id'); ?>">
                                                        <?php echo $uni->__GET('sUndDescripcion'); ?>
                                                    </option>
                                                    <?php 
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </td>
                                            <td style="width: 20%;">
                                                <input class="form-control"
                                                        id="nRecCantidad"
                                                        name="nRecCantidad"
                                                        type="text"
                                                        data-bts-min="1"
                                                        data-bts-max="100"
                                                        data-bts-init-val="1"
                                                        data-bts-step="1"
                                                        data-bts-decimal="1"
                                                        data-bts-step-interval="100"
                                                        data-bts-button-down-class="btn btn-default"
                                                        data-bts-button-up-class="btn btn-default"
                                                        value="1"
                                                        />
                                            </td>
                                            <td style="width: 20%;">
                                                <button type="button" class="btn btn-success" onclick="AgregarReceta();"><i class="fa fa-plus-circle"></i></button>
                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody id="lista_recetas">
                                        <tr>
                                            <td colspan="4">No existe niguna receta registrada</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="box box-primary">
                            <div class="box-header">
                                <h4>Visualizar Imagen</h4>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <output id="list"></output><br>
                                        <?php if (isset($r)) { ?>
                                            <img src="view/librerias/dist/productimg/<?php echo isset($r) ? $r->__GET('ProSrvNomImagen'):'';?>"/>
                                        <?php } ?>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>   
                </div>   <!-- /.row -->
            </section><!-- /.content -->
            <!-- Modal modal-eliminar -->
            <div class="modal fade" id="modal-eliminar" tabindex="-1" role="dialog" aria-labelledby="modal-eliminar-label" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="javascript:EliminarReceta();" method="POST">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                <h4 class="modal-title" id="modal-eliminar-label">Eliminar receta</h4>
                            </div>
                            <div class="modal-body">
                                <input type="hidden" name="post" id="post">
                                ¿Esta seguro que desea eliminar la receta de este producto?
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-danger btn-flat">Si, Agregar</button>
                                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">No, cancelar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /Modal modal-eliminar -->

            <script type="text/javascript" src="view/librerias/dist/js/funcionesProductos.js"></script>
            <script type="text/javascript" src="view/librerias/dist/js/funcionesGenerales.js"></script>
            <script type="text/javascript">
                $(document).ready(function(){
                      ListarReceta();
                      
                });
                
                $("input[name='nRecCantidad']").TouchSpin({});
                $('#modal-eliminar').on('show.bs.modal', function(event) {
                    var button = $(event.relatedTarget); // Button that triggered the modal
                    var recipient = button.data('codigo'); // Extract info from data-* attributes

                    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                    var modal = $(this);
                    modal.find('.modal-body input').val(recipient);
                });
            </script>
            <script>
                $(document).ready(function(){
                    $("#frm-foto").submit(function(){
                        return $(this).validate();
                    });
                })
            </script>
            <script>
 
    } 
            
             function archivo(evt) {
                      var files = evt.target.files; // FileList object
                 
                      // Obtenemos la imagen del campo "file".
                      for (var i = 0, f; f = files[i]; i++) {
                        //Solo admitimos imágenes.
                        if (!f.type.match('image.*')) {
                            continue;
                        }
                 
                        var reader = new FileReader();
                 
                        reader.onload = (function(theFile) {
                            return function(e) {
                              // Insertamos la imagen
                             document.getElementById("list").innerHTML = ['<img class="thumb" src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');
                            };
                        })(f);
                 
                        reader.readAsDataURL(f);
                      }
                  }
                 
                  document.getElementById('files').addEventListener('change', archivo, false);
            </script>