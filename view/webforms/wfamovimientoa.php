
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Movimiento
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="wfamain.php"><i class="fa fa-dashboard"></i> Inicio</a></li>
                <li><a href="#">Movimiento</a></li>
            </ol>
        </section>        
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-primary">
                        <form action="javascript:GuardarMovimiento();" method="POST" id="FrmMovimiento">
                            <div class="box-header">
                                <h3 class="box-title">Datos basicos</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div id="resultado"></div>
                                <div class="row">                                    
                                    <div class="col-md-4 form-group">
                                        <input type="hidden" id="Movimiento_Id" name="Movimiento_Id" value="<?php echo isset($r) ? $r->__GET('Movimiento_Id'):'0'; ?>"> 
                                        <label>Fecha</label>
                                        <input type="text" class="form-control" name="dMovFecha" id="dMovFecha" placeholder="Fecha" value="<?php echo isset($r) ? $r->__GET('dMovFecha'):''; ?>">
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <label>Tipo movimiento</label>
                                        <select class="form-control" name="Tipomovimiento_Id" id="Tipomovimiento_Id" onchange="Tipomovimiento()">
                                            <option value="">Seleccione tipo movimiento</option>
                                            <?php
                                            foreach ($tipomovimiento as $timo) {
                                            ?>
                                            <option value="<?php echo $timo->__GET('TipoMovimiento_Id'); ?>" <?php echo isset($r) ? $r->__GET('nMovTipoMovimiento_Id') == $timo->__GET('TipoMovimiento_Id') ? 'selected':'' : ''?>><?php echo $timo->__GET('sTMovNombre'); ?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <label>Tipo pago</label>
                                        <select class="form-control" name="nMovTipopago" id="nMovTipopago">
                                            <option value="">Seleccione tipo pago</option>
                                            <option value="1" <?php echo isset($r) ? $r->__GET('nMovTipopago') == 1 ? 'selected':'' : ''; ?>>Contado</option>
                                            <option value="2" <?php echo isset($r) ? $r->__GET('nMovTipopago') == 2 ? 'selected':'' : ''; ?>>Credito</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <label>Tipo</label>
                                        <select class="form-control" name="nMovTipoOrigenDestino" id="nMovTipoOrigenDestino" onchange="TipoPrvClien()">
                                            <option value="1" <?php echo isset($r) ? $r->__GET('nMovTipoOrigenDestino') == 1 ? 'selected':'' : ''; ?>>Proveedor</option>
                                            <option value="2" <?php echo isset($r) ? $r->__GET('nMovTipoOrigenDestino') == 2 ? 'selected':'' : ''; ?>>Cliente</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label>Cliente/Proveedor</label>
                                        <select class="form-control" name="nMovOrigenDestino_Id" id="nMovOrigenDestino_Id">
                                            <option value="">Seleccione cliente/proveedor</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 form-group proveedor">
                                        <label>Destino</label>
                                        <select class="form-control" name="nMovTipodestino" id="nMovTipodestino">
                                            <option value="1" <?php echo isset($r) ? $r->__GET('nMovTipodestino') == 1 ? 'selected':'' : ''; ?>>Almacen</option>
                                            <option value="0" <?php echo isset($r) ? $r->__GET('nMovTipodestino') == 0 ? 'selected':'' : ''; ?>>Consumo</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 form-group proveedor">
                                        <label>Almacen</label>
                                        <select class="form-control" name="Almacen_Id" id="Almacen_Id">
                                            <!--<option value="">Seleccione almacen</option>-->
                                            <?php
                                            foreach ($almacen as $alm) {
                                            ?>
                                                <option value="<?php echo $alm->__GET('Almacen_Id'); ?>" <?php echo isset($r) ? $alm->__GET('Almacen_Id') == $r->__GET('Almacen_Id') ? '':'' :'';?>><?php echo $alm->__GET('sAlmNombre'); ?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-8 form-group">
                                        <label>Documento</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <select class="form-control" name="Documento_Id" id="Documento_Id" onchange="NumeroDocumento();" disabled>
                                                    <option value="">Seleccionar documento</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" name="sMovDocumento" id="sMovDocumento" placeholder="Nro" value="<?php echo isset($r) ? $r->__GET('sMovDocumento') : ''; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label>Documento referencia</label>
                                        <input type="text" class="form-control" name="sMovDocReferencia" id="sMovDocReferencia" placeholder="Documento referencia" value="<?php echo isset($r) ? $r->__GET('dMovFecha'):''; ?>">
                                    </div>
                                    <div class="col-md-6 form-group" id="TipoMoneda">
                                        <label>Tipo moneda</label>
                                        <!--El tipo de cambio solo sera para el dolar caso contrario 1-->
                                        <select class="form-control" name="Moneda_Id" id="Moneda_Id" onchange="TipoMoneda()">                                        
                                            <!--<option value="">Seleccione tipo moneda</option>-->
                                            <option value="1" <?php echo isset($r) ? $r->__GET('Moneda_Id') == 1 ? 'selected':'' : ''; ?>>Soles</option>
                                            <option value="2" <?php echo isset($r) ? $r->__GET('Moneda_Id') == 2 ? 'selected':'' : ''; ?>>Dolares</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group" id="TipoCambio" style="display: none;">
                                        <label>Tipo de cambio</label>
                                        <input type="text" class="form-control" name="nMovTipoCambio" id="nMovTipoCambio" value="<?php echo isset($r) ? $r->__GET('nMovTipoCambio') : '1'; ?>">
                                    </div>
<!--                                    <div class="col-md-6 form-group">
                                        <label>Estado</label>
                                        El tipo de cambio solo sera para el dolar caso contrario 1
                                        <select class="form-control" name="nMovEstado" id="nMovEstado">                                        
                                            <option value="">Seleccione estado</option>
                                            <option value="1">Pagado</option>
                                            <option value="2">Por pagar</option>
                                        </select>
                                    </div>-->
                                    
                                    <div class="col-md-12 form-group">
                                        <div class="row" style="margin-top: 20px;">   
                                            <div class="col-md-6">
                                                <h4>&nbsp;&nbsp;Lista de productos</h4>
                                            </div>
                                            <div class="col-md-6 text-right">
                                                <a class="btn btn-success" onclick="ValidarTipomovimiento()"><span class="glyphicon glyphiconglyphicon glyphicon-plus"></span>&nbsp;Agregar producto</a>
                                            </div>
                                        </div>
                                        <hr style="margin-top: 0px;">
                                        <table class="table table-bordered table-striped text-center">
                                            <thead>
                                                <tr>
                                                    <th>Producto</th>
                                                    <th>Cantidad</th>
                                                    <th>Unidad</th>
                                                    <th>Precio</th>
                                                    <th>Sub Total</th>
                                                    <th>Operaciones</th>
                                                </tr>
                                            </thead>
                                            <tbody id="productos_servicios">
                                                <tr>
                                                    <td colspan="7">No hay datos</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-3">
                                        <div id="resultado_productos"></div>
                                        <div class="row">
                                            <div class="col-md-6 form-group text-center">
                                                <input type="checkbox" name="dMovDetraccion_check" id="dMovDetraccion_check">&nbsp;Detracción
                                            </div>
                                            <div class="col-md-6 form-group">
                                                <input type="text" class="form-control" name="dMovDetraccion" id="dMovDetraccion" placeholder="0.00" value="<?php echo isset($r) ? $r->__GET('dMovDetraccion'):'0'; ?>">
                                            </div>
                                            <div class="col-md-6 form-group text-center">
                                                <input type="checkbox" name="dMovPercepcion_check" id=""dMovPercepcion_check>&nbsp;Percepción
                                            </div>
                                            <div class="col-md-6 form-group">
                                                <input type="text" class="form-control" name="dMovPercepcion" id="dMovPercepcion" placeholder="0.00" value="<?php echo isset($r) ? $r->__GET('dMovPercepcion'):'0'; ?>">
                                            </div>
                                            <div class="col-md-6 form-group text-center">
                                                <input type="checkbox" name="dMovRetencion_check" id="dMovRetencion_check">&nbsp;Retención
                                            </div>
                                            <div class="col-md-6 form-group">
                                                <input type="text" class="form-control" name="dMovRetencion" id="dMovRetencion" placeholder="0.00" value="<?php echo isset($r) ? $r->__GET('dMovRetencion'):'0'; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-4 form-group">
                                                <label>Importe</label>
                                                <input type="text" class="form-control" name="dMovImporte" id="dMovImporte" placeholder="0.00" value="<?php echo isset($r) ? $r->__GET('nMovImporte'):''; ?>">
                                            </div>
                                            <div class="col-md-4 form-group">
                                                <label>IGV</label>
                                                <input type="text" class="form-control" name="dMovIGV" id="dMovIGV" placeholder="0.00" value="<?php echo isset($r) ? $r->__GET('nMovIGV'):''; ?>">
                                            </div>
                                            <div class="col-md-4 form-group">
                                                <label>TOTAL</label>
                                                <input type="text" class="form-control" name="dMovTotal" id="dMovTotal" placeholder="0.00" value="<?php echo isset($r) ? $r->__GET('nMovTotal'):''; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Observaciónes</label>
                                            <textarea class="form-control" name="sMovObservacion" id="sMovObservacion" placeholder="Observaciones" rows="3"><?php echo isset($r) ? $r->__GET('sMovObservacion'):''; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary btn-flat">Guardar información</button>
                                <a href="?c=Movimiento&a=Index" class="btn btn-danger btn-flat">Cancelar</a>
                            </div>
                        </form>
                    </div><!-- /.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </section><!-- /.content --> 
        <!-- /Modal modal-eliminar -->
        <div class="modal fade" id="agregar_producto" tabindex="-1" role="dialog" aria-labelledby="modal-eliminar-label" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content" id="FrmTipomovimiento">
                    
                </div>
            </div>
        </div>
        <div class="modal fade" id="eliminar_productos" tabindex="-1" role="dialog" aria-labelledby="modal-eliminar-label" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content" id="FrmTipomovimiento">
                    <form action="javascript:EliminarProductoLista();" method="POST" id="FrmIngresoCompra">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                            <h4 class="modal-title" id="modal-eliminar-label">ELIMINAR PRODUCTOS</h4>
                        </div>
                        <div class="modal-body"> 
                            <input type="hidden" name="Codigo_Producto" id="Codigo_Producto">
                            ¿Desea eliminar al producto de la lista?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">No, cancelar</button>
                            <button type="submit" class="btn btn-danger btn-flat">Si, Eliminar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
       
        <!-- /Modal modal-eliminar -->
        <script src="view/librerias/dist/js/funcionesMovimiento.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                TipoPrvClien();
            });
            $('#dMovFecha').datetimepicker({
               viewMode: 'days',
               format: 'DD-MM-YYYY'
            });     
            function TipoMoneda(){
                var moneda = $("#TipoMoneda");
                var Moneda_Id = document.getElementById('Moneda_Id').value;
                if(Moneda_Id == 1){
                    moneda.removeClass('col-md-3');
                    moneda.addClass('col-md-6');
                    $("#TipoCambio").hide();
                    $("#nMovTipoCambio").val("1");
                }else{
                    moneda.removeClass('col-md-6');
                    moneda.addClass('col-md-3');
                    $("#TipoCambio").show();
                    $("#nMovTipoCambio").val("");
                }
            }
            $('#eliminar_productos').on('show.bs.modal', function(event) {
                var button = $(event.relatedTarget); // Button that triggered the modal
                var recipient = button.data('productoid'); // Extract info from data-* attributes

                // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this);
                modal.find('.modal-body input').val(recipient);
            });
        </script>