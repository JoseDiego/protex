<form action="javascript:AgregarProductoC();" method="POST" id="FrmModalProductos">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
        <h4 class="modal-title" id="modal-eliminar-label">Cargar Productos</h4>
    </div>
    <div class="modal-body">   
        <div id="resultado_ingresocompra"></div>
        <div class="row">
            <div class="col-md-12 form-group">
                <label>Productos</label>
                <select class="form-control" id="ProdServ_Id" name="ProdServ_Id" onchange="ProductosUNILista()">
                    <option value="">Seleccionar producto</option>
                    <?php 
                    foreach ($productos as $p){
                    ?>
                    <option value="<?php echo $p->__GET('ProdServ_Id'); ?>"><?php echo $p->__GET('sProSrvNombre'); ?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
            <div class="col-md-6">
                <div class="col-md-12 form-group" style="padding-left: 0px; padding-right: 0px;">
                    <label>Cantidad</label>
                    <input type="text" class="form-control" onkeyup="CalcularSubtotal()" name="sMovDetConCantidad" id="sMovDetConCantidad" placeholder="Cantidad" value="0">
                </div>

            </div>
            <div class="col-md-6">
                <div class="col-md-12 form-group" style="padding-left: 0px; padding-right: 0px;">
                    <label>Unidad</label>
                    <select class="form-control" name="Unidadmedida_Id" id="Unidadmedida_Id">
                        <option value="">Seleccione unidad.</option>
                    </select>
                </div>
                <div class="col-md-12 form-group" style="padding-left: 0px; padding-right: 0px;">
                    <label>P. Unitario</label>
                    <input type="text" class="form-control" onkeyup="CalcularSubtotal()" name="nMovDetConPrecioUnitario" id="nMovDetConPrecioUnitario" placeholder="P. Unitario" value="0">
                </div>
                <div class="col-md-12 form-group" style="padding-left: 0px; padding-right: 0px;">
                    <label>Sub total</label>
                    <input type="text" class="form-control" name="nMovDetConSubtotal" id="nMovDetConSubtotal" placeholder="Sub total">
                </div>
            </div>
            <div class="col-md-12 text-center">
                <input type="checkbox" name="nIncluyeIGV" id="nIncluyeIGV">&nbsp;<b>Incluido IGV</b>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">No, cancelar</button>
        <button type="submit" class="btn btn-danger btn-flat">Si, Agregar</button>
    </div>
</form>
<script type="text/javascript"> 
    jQuery.validator.addMethod("decimales", function (value, element) {
        return this.optional(element) || /^([0-9]+\.+[0-9]|[0-9])+$/.test(value);
    });
    $("#FrmIngresoCompra").validate({
        errorClass: 'error',
        rules: {        
            ProdServ_Id: {            
               required: true
            },
            sMovDetConCantidad: {
               required: true,
               number: true
            },
            Unidadmedida_Id: {
                required: true
            },
            nMovDetConPrecioUnitario: {
                required: true,
                decimales: true
            },
            nMovDetConSubtotal: {
                required: true,
                decimales: true
            }
        },
        messages: {   
            ProdServ_Id: "Seleccione producto por favor.",       
            sMovDetConCantidad: {
                required: "Ingrese cantidad por favor.",
                number: "Permitodo solo numeros."
            },     
            nComPrecioUnitario: {
                required: "Ingrese precio unitario por favor.",
                decimales: "Solo esta permitido numeros enteros y decimales por favor."
            },
            Unidadmedida_Id: "Seleccione unidad por favor.",
            nMovDetConPrecioUnitario: {
                required: "Ingrese precio unitario por favor.",
                decimales: "Permitido numero decimales"
            },
            nMovDetConSubtotal: {
                required: "Ingrese subtotal por favor.",            
                decimales: "Permitido numero decimales"
            }
        }
    });
</script>