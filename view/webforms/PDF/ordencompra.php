<?php
ob_start();
?>
<style type="text/css">
<!--
    table.page_header {width: 100%; border: none; border-bottom: solid 0.1mm #E6892F; padding: 3mm 4mm }
    table.page_footer {width: 100%; border: none; border-top: solid 0.1mm #E6892F; padding: 3mm 4mm}
    h1 {color: #000033}
    h2 {color: #000055}
    h3 {color: #000077}
    h4 {
        margin: 0px;
    }

    div.niveau
    {
        padding-left: 5mm;
    }
    #example1{
        width: 100%;
    }
    #example1 tbody tr td,
    #example1 tbody tr th{
        font-size: 11px;
        text-align: center;
        border-bottom: 0.1mm solid #ccc;
        padding: 5px 0px;
    }
    hr{
        border: 0.2mm solid #A7A7A7;
        margin-bottom: 10px;
        margin-top: 5px;
    }
    h5{
        margin: 0px;
        text-align: left;
    }
-->
</style>
<page backtop="30mm" backbottom="20mm" backleft="20mm" backright="20mm" style="font-size: 12pt">
    <page_header>
        <table class="page_header">
            <tr>
                <td style="width: 50%; text-align: left">
                    <img src="view/librerias/dist/img/logo_protex.png" width="150">
                </td>
                <td style="width: 50%; text-align: right; color: #7b7b7b;">
                   ORDEN DE COMPRA
                </td>
            </tr>
        </table>
    </page_header>
    <page_footer>
        <table class="page_footer">
            <tr>
                <td style="width: 50%; text-align: left; font-weight: normal; font-size: 11px;">
                    <?PHP echo date('d-m-Y H:i A');?>
                </td>
                <td style="width: 50%; text-align: right">
                    [[page_cu]]/[[page_nb]]
                </td>
            </tr>
        </table>
    </page_footer>
    <bookmark title="Sommaire" level="0">

        <h4 style="font-weight: normal; text-align: center; width: 100%;">REGISTRO DE ORDEN DE COMPRA</h4>
        <hr>                  	
        <?php             
        foreach ($datos as $r){
        ?>
        <table id="example1" cellpadding="0" cellspacing="0">     
           <tbody>
                <tr>
                   <th style="width: 30%; text-align: right;">Código de la Orden de Compra :</th>
                   <td style="width: 70%;"><?php echo $r->__GET('Pedido_Id'); ?></td>
                </tr> 
                <tr>
                   <th style="width: 30%; text-align: right;">RUC/DNI:</th>
                   <td style="width: 70%;"><?php echo $r->__GET('sODRucDni'); ?></td>
                </tr>
                <tr>
                   <th style="width: 30%; text-align: right;">Nombre Cliente:</th>
                   <td style="width: 70%;"><?php echo $r->__GET('sODNombre'); ?></td>
                </tr>   
                <tr>
                   <th style="width: 30%; text-align: right;">Fecha de Orden de Compra:</th>
                   <td style="width: 70%;"><?php echo date('d/m/Y', strtotime($r->__GET('dPedFecha'))); ?></td>
                </tr>    
                <tr>
                   <th style="width: 30%; text-align: right;">Tipo de Pago:</th>
                   <td style="width: 70%;"><?php if($r->__GET('nPedTipopago')==1){ echo 'Efectivo';}else{ echo 'Credito';} ?></td>
                </tr>  
                <tr>
                   <th style="width: 30%; text-align: right;">Moneda:</th>
                   <td style="width: 70%;"><?php if($r->__GET('Moneda_Id')==1){ echo 'Soles (S/.)';}else{ echo 'Dolares ($)';} ?></td>
                </tr>
                <tr>
                   <th style="width: 30%; text-align: right;">Documento:</th>
                   <td style="width: 70%;"><?php echo $r->__GET('sDocNombre'); ?></td>
                </tr>
                <tr>
                   <th style="width: 30%; text-align: right;">Nro Orden Trabajo:</th>
                   <td style="width: 70%;"><?php echo $r->__GET('nroOrdenTrabajo'); ?></td>
                </tr>
                <tr>
                   <th style="width: 30%; text-align: right;">Observaciones:</th>
                   <td style="width: 70%;"><?php echo $r->__GET('sPedObservaciones'); ?></td>
                </tr>
                <tr>
                   <th style="width: 30%; text-align: right;">Estado:</th>
                   <td style="width: 70%;"><?php
                       $estado = $r->__GET('nPedEstado');
                        if($estado == 1){echo 'En Espera';}
                        else{
                              if($estado == 2){echo 'En Proceso';}
                              else{echo 'Terminado';} 
                            }
                   ?></td>
                </tr>
                <tr>
                    <th style="width: 30%;"></th>
                    <td  style="text-align: center;">
                          <table class="table table-bordered table-striped text-center" >
                                            <thead>
                                                <tr>
                                                    <th style="width: 60px;"> Cantidad  </th>
                                                    <th style="width: 60px;"> Producto </th>
						    <th style="width: 60px;"> Unidad </th>
                                                    <th style="width: 60px;">Precio Unitario</th>
                                                    <th style="width: 60px;">Sub Total</th>                                                    
                                                </tr>
                                            </thead>
                                            <tbody id="productos_servicios">
                                                 
                                <?php echo $detalles;?>                
                                
             </tbody>
                          </table></td></tr>
              <tr>
                   <th style="width: 30%; text-align: right;">Importe de Adelanto:</th>
                   <td style="width: 70%;"><?php echo $r->__GET('dPedImporteAd'); ?></td>
                </tr>               
                <tr>
                   <th style="width: 30%; text-align: right;">Importe:</th>
                   <td style="width: 70%;"><?php echo $r->__GET('dPedImporte'); ?></td>
                </tr>
                <tr>
                   <th style="width: 30%; text-align: right;">IGV:</th>
                   <td style="width: 70%;"><?php echo $r->__GET('dPedIGV'); ?></td>
                </tr>
                <tr>
                   <th style="width: 30%; text-align: right;">Total:</th>
                   <td style="width: 70%;"><?php echo $r->__GET('dPedTotal'); ?></td>
                </tr>
                                <tr>
                   <th style="width: 30%; text-align: right;">Saldo:</th>
                   <td style="width: 70%;"><?php echo $r->__GET('dPedSaldo'); ?></td>
                </tr>
           </tbody></table>
        
        <?php
        }
        ?>
        
    </bookmark>
</page>

<?php
    
    $content = ob_get_clean();

    require_once('view/librerias/html2pdf/html2pdf.class.php');
    try
    {
        $html2pdf = new HTML2PDF('P', 'A4', 'fr', true, 'UTF-8', 0);
        $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
//        $html2pdf->createIndex('Sommaire', 25, 12, false, true, 1);
        $content = ob_get_clean();
        $html2pdf->Output('bookmark.pdf');
    }
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }
