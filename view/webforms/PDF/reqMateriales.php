<?php
ob_start();
?>
<style type="text/css">
<!--
    table.page_header {width: 100%; border: none; border-bottom: solid 0.1mm #E6892F; padding: 3mm 4mm }
    table.page_footer {width: 100%; border: none; border-top: solid 0.1mm #E6892F; padding: 3mm 4mm}
    h1 {color: #000033}
    h2 {color: #000055}
    h3 {color: #000077}
    h4 {
        margin: 0px;
    }

    div.niveau
    {
        padding-left: 5mm;
    }
    #example1{
        width: 100%;
    }
    #example1 tbody tr td,
    #example1 tbody tr th{
        font-size: 9px;
        text-align: center;
        border-bottom: 0.1mm solid #ccc;
        padding: 5px 0px;
    }
    hr{
        border: 0.2mm solid #A7A7A7;
        margin-bottom: 10px;
        margin-top: 5px;
    }
    h5{
        margin: 0px;
        text-align: left;
    }
-->
</style>
<page backtop="30mm" backbottom="20mm" backleft="20mm" backright="20mm" style="font-size: 12pt">
    <page_header>
        <table class="page_header">
            <tr>
                <td style="width: 50%; text-align: left">
                    <img src="view/librerias/dist/img/logo_protex.png" width="150">
                </td>
                <td style="width: 50%; text-align: right; color: #7b7b7b;">
                   REQUERIMIENTO DE MATERIALES
                </td>
            </tr>
        </table>
    </page_header>
    <page_footer>
        <table class="page_footer">
            <tr>
                <td style="width: 50%; text-align: left; font-weight: normal; font-size: 11px;">
                    <?PHP echo date('d-m-Y H:i A');?>
                </td>
                <td style="width: 50%; text-align: right">
                    [[page_cu]]/[[page_nb]]
                </td>
            </tr>
        </table>
    </page_footer>
    <bookmark title="Sommaire" level="0">

        <h4 style="font-weight: normal; text-align: center; width: 100%;">REQUERIMIENTO DE MATERIALES</h4>
        <hr> 
        <br>
        <table class="table table-bordered table-striped text-center" align='center' style="font-size: 12px;">
                                            <thead>
                                                <tr style='text-align: center;'>
                                                  <th style="width: 120px;">Producto</th>
                                                  <th style="width: 100px;">Unidad</th>
                                                  <th style="width: 120px;">Cantidad a Utilizar</th>
                                                  <th style="width: 120px;">Cantidad Existente</th>
                                                  
                                                </tr>
                                            </thead>
                                            <tbody id="productos_servicios">
                                                 
                                                <?php echo $datos;?>                
                                
                                           </tbody>
         </table>
    </bookmark>
</page>

<?php
    
    $content = ob_get_clean();

    require_once('view/librerias/html2pdf/html2pdf.class.php');
    try
    {
        $html2pdf = new HTML2PDF('P', 'A4', 'fr', true, 'UTF-8', 0);
        $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
//        $html2pdf->createIndex('Sommaire', 25, 12, false, true, 1);
        ob_get_clean();
        $html2pdf->Output('bookmark.pdf');
    }
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }
