<?php
/**
 * HTML2PDF Librairy - example
 *
 * HTML => PDF convertor
 * distributed under the LGPL License
 *
 * @author      Laurent MINGUET <webmaster@html2pdf.fr>
 *
 * isset($_GET['vuehtml']) is not mandatory
 * it allow to display the result in the HTML format
 */

ob_start();
?>
<style type="text/css">
<!--
    table.page_header {width: 100%; border: none; border-bottom: solid 0.1mm #E6892F; padding: 3mm 4mm }
    table.page_footer {width: 100%; border: none; border-top: solid 0.1mm #E6892F; padding: 3mm 4mm}
    h1 {color: #000033}
    h2 {color: #000055}
    h3 {color: #000077}
    h4 {
        margin: 0px;
    }

    div.niveau
    {
        padding-left: 5mm;
    }
    #example1{
        width: 100%;
    }
    #example1 tbody tr td,
    #example1 tbody tr th{
        font-size: 11px;
        text-align: center;
        border-bottom: 0.1mm solid #ccc;
        padding: 5px 0px;
    }
    hr{
        border: 0.2mm solid #A7A7A7;
        margin-bottom: 10px;
        margin-top: 5px;
    }
    h5{
        margin: 0px;
        text-align: left;
    }
-->
</style>
<page backtop="30mm" backbottom="20mm" backleft="20mm" backright="20mm" style="font-size: 12pt">
    <page_header>
        <table class="page_header">
            <tr>
                <td style="width: 50%; text-align: left">
                    <img src="view/librerias/dist/img/logo_protex.png" width="150">
                </td>
                <td style="width: 50%; text-align: right; color: #7b7b7b;">
                   PRODUCTOS
                </td>
            </tr>
        </table>
    </page_header>
    <page_footer>
        <table class="page_footer">
            <tr>
                <td style="width: 50%; text-align: left; font-weight: normal; font-size: 11px;">
                    <?PHP echo date('d-m-Y H:i A');?>
                </td>
                <td style="width: 50%; text-align: right">
                    [[page_cu]]/[[page_nb]]
                </td>
            </tr>
        </table>
    </page_footer>
    <bookmark title="Sommaire" level="0">
        <?php             
        foreach ($datos as $r){
        ?>
        <table id="example1" cellpadding="0" cellspacing="0">
            <tbody>
                <tr>
                    <th style="width: 60%; text-align: right;"></th>
                   <td style="width: 40%;">
                        <?php $foto=$r->__GET("ProSrvNomImagen"); ?>
                        <?php 
                        if (isset($foto)) { ?>
                            <br><img style="width:40%;" src="view/librerias/dist/productimg/<?php echo $r->__GET("ProSrvNomImagen"); ?>"/>
                        <?php } ?>
                   </td>
                </tr>
            </tbody>
        </table>
        <?php
        }
        ?>
        <h4 style="font-weight: normal; text-align: center; width: 100%;">REGISTRO DE PRODUCTO</h4>
        <hr>                  	
        <?php             
        foreach ($datos as $r){
        ?>
        <table id="example1" cellpadding="0" cellspacing="0">     
           <tbody>
                <tr>
                   <th style="width: 30%; text-align: right;">Código del producto:</th>
                   <td style="width: 70%;"><?php echo $r->__GET('sProSrvCodigo'); ?></td>
                </tr> 
                <tr>
                   <th style="width: 30%; text-align: right;">Nombre:</th>
                   <td style="width: 70%;"><?php echo $r->__GET('sProSrvNombre'); ?></td>
                </tr>
                <tr>
                   <th style="width: 30%; text-align: right;">Activo:</th>
                   <td style="width: 70%;"><?php echo ($r->__GET('sActDescripcion') == '1' ? '<span class="label bg-green">Habilitado</span>' : '<span class="label bg-danger">Habilitado</span>'); ?></td>
                </tr>   
                <tr>
                   <th style="width: 30%; text-align: right;">Unidad de medida:</th>
                   <td style="width: 70%;"><?php echo $r->__GET('sUndDescripcion'); ?></td>
                </tr>    
                <tr>
                   <th style="width: 30%; text-align: right;">Descripción:</th>
                   <td style="width: 70%;"><?php echo $r->__GET('ProSrvEspecificacion'); ?></td>
                </tr>    
                <tr>
                   <th style="width: 30%; text-align: right;">Estado:</th>
                   <td style="width: 70%;"><?php echo ($r->__GET('nProSrvEstado') == '1' ? '<span class="label bg-green">Habilitado</span>' : '<span class="label bg-danger">Inhabilitado</span>'); ?></td>
                </tr>
             </tbody>
        </table>
        
        <?php
        }
        ?>
    </bookmark>
</page>

<?php
    $content = ob_get_clean();

    require_once('view/librerias/html2pdf/html2pdf.class.php');
    try
    {
        $html2pdf = new HTML2PDF('P', 'A4', 'fr', true, 'UTF-8', 0);
        $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
//        $html2pdf->createIndex('Sommaire', 25, 12, false, true, 1);
        $html2pdf->Output('bookmark.pdf');
    }
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }
