
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Operario
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="wfamain.php"><i class="fa fa-dashboard"></i> Inicio</a></li>
                <li><a href="#">Operario</a></li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-6">
                    <div class="box">
                        <form action="javascript:GuardarOperario();" method="POST" id="FrmOperario">
                            <div class="box-header">
                                <h3 class="box-title">Datos basicos</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div id="resultado"></div>
                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <input type="hidden" id="Operario_Id" name="Operario_Id" value="<?php echo isset($r) ? $r->__GET('Operario_Id'):'0'; ?>">
                                        <label>DNI</label>
                                        <input type="text" class="form-control" name="sOpeDni" id="sOpeDni" placeholder="DNI" value="<?php echo isset($r) ? $r->__GET('sOpeDni'):''; ?>" required="required">
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label>Apellido Paterno</label>
                                        <input type="text" class="form-control" name="sOpeApePat" id="sOpeApePat" placeholder="Apellido Paterno" value="<?php echo isset($r) ? $r->__GET('sOpeApePat'):''; ?>" required="required">
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label>Apellido Materno</label>
                                        <input type="text" class="form-control" name="sOpeApeMat" id="sOpeApeMat" placeholder="Apellido Materno" value="<?php echo isset($r) ? $r->__GET('sOpeApeMat'):''; ?>" required="required">
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label>Nombres</label>
                                        <input type="text" class="form-control" name="sOpeNombres" id="sOpeNombres" placeholder="Nombres" value="<?php echo isset($r) ? $r->__GET('sOpeNombres'):''; ?>" required="required">
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label>Cargo</label>
                                        <input type="text" class="form-control" name="sOpeCargo" id="sOpeCargo" placeholder="Cargo" value="<?php echo isset($r) ? $r->__GET('sOpeCargo'):''; ?>" required="required">
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label>Direccion</label>
                                        <input type="text" class="form-control" name="sOpeDireccion" id="sOpeDireccion" placeholder="Direccion" value="<?php echo isset($r) ? $r->__GET('sOpeDireccion'):''; ?>" required="required">
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label>Telefono</label>
                                        <input type="text" class="form-control" name="sOpeTelefono" id="sOpeTelefono" placeholder="Telefono" value="<?php echo isset($r) ? $r->__GET('sOpeTelefono'):''; ?>" required="required">
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label>Correo</label>
                                        <input type="text" class="form-control" name="sOpeCorreo" id="sOpeCorreo" placeholder="Correo" value="<?php echo isset($r) ? $r->__GET('sOpeCorreo'):''; ?>" required="required">
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label>Estado</label>
                                        <select class="form-control" name="nOpeEstado" id="nOpeEstado">
                                            <option value="1" <?php echo isset($r) ? $r->__GET('nOpeEstado') == 1 ? 'selected':'' :''; ?>>Habilitado</option>
                                            <option value="0" <?php echo isset($r) ? $r->__GET('nOpeEstado') == 0 ? 'selected':'' :''; ?>>Deshabilitado</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary btn-flat">Guardar información</button>
                                <a href="?c=Operario&a=Index" class="btn btn-danger btn-flat">Cancelar</a>
                            </div>
                        </form>
                    </div><!-- /.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </section><!-- /.content -->
