var reportes_stock = (function (reportes_stock, undefined) {
    var URLactual = window.location.host;
    _ObtenerFiltroDatos = function () {
        var table = reportes_stock.TableData('example1');
        var oVentaDetallada = [];        
        var filteredrows = table._('tr', {"filter": "applied"});
        for (var i = 0; i < filteredrows.length; i++) {
            var data = {};
            data.Codigo = filteredrows[i][0];
            data.Familia = filteredrows[i][1];
            data.ProductoServicio = filteredrows[i][2];
            data.Stock = filteredrows[i][3];
            data.Unidad = filteredrows[i][4];
            data.PrecioProm = filteredrows[i][5];
            data.ValorizadoProm = filteredrows[i][6]; 
            oVentaDetallada.push(data);             
        };
       return JSON.parse(JSON.stringify(oVentaDetallada));
    };
    _POST = function (URL, data) {      
        $.ajax({
            data: data,
            url: URL,
            type: 'post',
            async: false,
            success: function (response) {
                json = response;
                window.open("http://"+URLactual+"/clubpacasmayo/" + json, "_blank");                
            }
        });
    },
    
    reportes_stock.Excel = function () {
        $("#Exportar_Excel").click(function(){           
            $('#preloader').fadeIn('slow');
            var data = _ObtenerFiltroDatos();  
            _POST('?c=Reportes&a=Stock_Excel', {
                data: data
            });            
            $('#preloader').fadeOut('slow');
            $('body').css({'overflow':'visible'});
        });
    },
    reportes_stock.TableData = function (DivId) {
        var table;
        if ($.fn.dataTable.isDataTable('#' + DivId)) {
            table = $("#" + DivId).dataTable();
        } else {
            table = $("#" + DivId).dataTable({
                "scrollX": true,
                "bSort": false,
                "language": {
                    "search": "Buscar",
                    "lengthMenu": "Visualizar _MENU_ registro por página",
                    "zeroRecords": "No hay información para mostrar",
                    "info": "Pagina _PAGE_ de _PAGES_ de _MAX_ ",
                    "infoEmpty": "Pagina _PAGE_ - _PAGES_ de _MAX_ registros",
                    "infoFiltered": "(filtered from _MAX_ total records)",
                    "paginate": {
                        "previous": "Anterior",
                        "next": "Siguiente"
                    }
                }
            });
        }
        return table;
    };
    return reportes_stock;
})(reportes_stock || {});

$(document).ready(function () {
    reportes_stock.TableData("example1");
    reportes_stock.Excel();
});