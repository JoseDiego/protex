function FrmGuardarUsuario(){
    $.post('?c=Usuario&a=FrmGuardarUsuario',{        
    },function(data){
        $(".content-wrapper").html(data);
    });
}
function FrmEditarUsuario(Usuario_Id){
    $.post('?c=Usuario&a=FrmEditarUsuario',{   
        Usuario_Id: Usuario_Id
    },function(data){
        $(".content-wrapper").html(data);
    });
}
function mostrarUsuario(Usuario_Id){
    $.post('?c=Usuario&a=MostrarUsuario',{   
        Usuario_Id: Usuario_Id
    },function(data){
        $("#ver_datos").html(data);
    });
}
function limpiarFrmUsuario(){
    document.getElementById('idRegistro').value = '';
    document.getElementById('nombre').value = '';
    document.getElementById('login').value = '';
    document.getElementById('contraseñaOne').value = '';
    document.getElementById('contraseñaTwo').value = '';
    $('#tipo').prop('selectedIndex',0);
    $('#estado').prop('selectedIndex',0);
}
function guardarUsuario(){
    var idRegistro = document.getElementById('idRegistro').value;
    var nombre = document.getElementById('nombre').value;
    var login = document.getElementById('login').value;
    var contraseñaOne = document.getElementById('contraseñaOne').value;
    var contraseñaTwo = document.getElementById('contraseñaTwo').value;
    var tipo = document.getElementById('tipo').value;
    var estado = document.getElementById('estado').value;
    if(contraseñaOne == contraseñaTwo){
        $.post('?c=Usuario&a=GuardarUsuario',{
            idRegistro: idRegistro,
            nombre: nombre,
            login: login,
            contraseñaTwo: contraseñaTwo,
            tipo: tipo,
            estado: estado
        },function(data){
            $("#resultado").html(data);
            limpiarFrmUsuario();
            $('html, body').animate({ scrollTop: '0px'}, 1000);
        });
    }else{
        $("#resultado").html('<div class="alert alert-warning" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Advertencia!</strong>, las contraseñas no coinciden.</div>');
    }
}