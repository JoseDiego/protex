function NuevaCompra(){
    $.post('?c=Compra&a=NuevaCompra',{        
    },function(data){
        $(".content-wrapper").html(data);
    });
}
    
function cargarProductos(){
    var Proveedor_Id = document.getElementById('Proveedor_Id').value;
    $.post('?c=Compra&a=ListarPro', {
        Proveedor_Id:Proveedor_Id
    }, function(data) {
        $("#ProdServ_Id").html(data);
    });
}

        function PrecioUnitario(){
            var ProdServ_Id = document.getElementById('ProdServ_Id').value;
            var Unidadmedida_Id = document.getElementById('Unidadmedida_Id').value;
            var Moneda_Id = document.getElementById('Moneda_Id').value;
            var Operacion_Id = 2;            
	    $.post('?c=Precioventa&a=PrecioProductoCompra', {
	        ProdServ_Id: ProdServ_Id,
                Unidadmedida_Id: Unidadmedida_Id,
	        Moneda_Id: Moneda_Id,
                Operacion_Id: Operacion_Id
	    }, function(data){
	        $("#nMovDetConPrecioUnitario").html(data);
                 CalcularSubtotal();
	    });
}

function credito(){
                var pago = document.getElementById('tipopago').value;
                if(pago == 2){
                document.getElementById('importeAd').disabled=false;
                document.getElementById('estado').value = 2;
                document.getElementById('estado').disabled=true;
                }
                else{document.getElementById('importeAd').disabled=true;
                     document.getElementById('estado').value = 1;
                     document.getElementById('estado').disabled=true;
                }
}            
            
            
function ValidarModalProducto(){
	       $("#agregar_productos").modal('show');
}
            

            
function ProductosUNILista(){
	    var ProdServ_Id = document.getElementById('ProdServ_Id').value;
	    $.post('?c=Movimiento&a=ListarUnidades', {
	        ProdServ_Id: ProdServ_Id
	    }, function(data){
	       $("#Unidadmedida_Id").html(data);
	       });
}
        
function CalcularSaldo(){
    var dPedImporteAd = document.getElementById('importeAd').value;
    var dMovTotal = document.getElementById('dMovTotal').value;
    var saldo = dMovTotal - dPedImporteAd;
    var nsaldo = saldo.toFixed(2);
    document.getElementById('dMovSaldo').value = nsaldo;
}



function listarProveedor(){
            	var OrigenDestino_Id=0;
            	$.post('?c=compra&a=ListarComboBoxProveedor',{
                         OrigenDestino_Id: OrigenDestino_Id
                    },function(data){
                        $("#Proveedor_Id").html(data);
                        
                      });
}

function LimpiarFrmProductos(){
    $('#ProdServ_Id').prop('selectedIndex',0);
    document.getElementById('sMovDetConCantidad').value = "0";
//    document.getElementById('dMovDetConFechaVencimiento').value = "";
    $('#Unidadmedida_Id').prop('selectedIndex',0);
    document.getElementById('nMovDetConPrecioUnitario').value = "0.00";
    document.getElementById('nMovDetConSubtotal').value = "0.00";
    document.getElementById('mensajeModal').style.display = 'none';
    document.getElementById('Moneda_Id').disabled = true;
    document.getElementById('Proveedor_Id').disabled = true;    
    
}
function CalcularSubtotal(){
    var sMovDetConCantidad = document.getElementById('sMovDetConCantidad').value;
    var nMovDetConPrecioUnitario = document.getElementById('nMovDetConPrecioUnitario').value;
    document.getElementById('nMovDetConSubtotal').value = sMovDetConCantidad * nMovDetConPrecioUnitario;
}

function CalcularImpTotIGV(){
    var Documento_Id = 9;
    $.post('?c=Compra&a=CalcularImpTotIGV',{     
        Documento_Id: Documento_Id
    }, function(data){
        $("#resultado_productos").html(data);
    });
}

function EliminarProductoLista(){
				    var Codigo_Producto = document.getElementById('Codigo_Producto').value;
				    $.post('?c=Compra&a=EliminarProductoLista',{
				        Codigo_Producto: Codigo_Producto
				    }, function(data){
				        $("#resultado").html(data);
				        ListarProductos();
				        CalcularImpTotIGV();
				        $("#eliminar_productos").modal("hide");
				    });
}

function AgregarProductoC(){
    var ProdServ_Id = document.getElementById('ProdServ_Id').value;
    var Unidadmedida_Id = document.getElementById('Unidadmedida_Id').value;
    var sMovDetConCantidad = document.getElementById('sMovDetConCantidad').value;
    var nMovDetConSubtotal = document.getElementById('nMovDetConSubtotal').value;
    var nMovDetConPrecioUnitario = document.getElementById('nMovDetConPrecioUnitario').value;

    $.post('?c=Compra&a=AgregarProductoC', {
        ProdServ_Id: ProdServ_Id,
        Unidadmedida_Id: Unidadmedida_Id,
        sMovDetConCantidad: sMovDetConCantidad,
        nMovDetConSubtotal: nMovDetConSubtotal,
        nMovDetConPrecioUnitario: nMovDetConPrecioUnitario,
		        
    }, function (data) {
        $("#resultado_ingresocompra").html(data);
        LimpiarFrmProductos();
        ListarProductos();
        CalcularImpTotIGV();
    });
}

function ListarProductos() {
    $.post('?c=Compra&a=ListarProductos', {
        }, function(data) {
	        $("#productos_servicios").html(data);
        });

}


