var reportes_ventadetallada = (function (reportes_ventadetallada, undefined) {
    var URLactual = window.location.host;
    _ObtenerFiltroDatos = function () {
        var table = reportes_ventadetallada.TableData('example1');
        var oVentaDetallada = [];        
        var filteredrows = table._('tr', {"filter": "applied"});
        for (var i = 0; i < filteredrows.length; i++) {
            var data = {};
            data.Fecha = filteredrows[i][0];
            data.Almacén = filteredrows[i][1];
            data.Documento = filteredrows[i][2];
            data.Condición = filteredrows[i][3];
            data.Cliente = filteredrows[i][4];
            data.Producto = filteredrows[i][5];
            data.Cantidad = filteredrows[i][6];
            data.Unidad = filteredrows[i][7];
            data.PCostoUpuntoSIGV = filteredrows[i][8];
            data.PVentaUpuntoSIGV = filteredrows[i][9];
            data.Moneda = filteredrows[i][10];
            data.ImporteVenta = filteredrows[i][11];
            data.Utilidad = filteredrows[i][12];
            data.PorcRentabilidad = filteredrows[i][13];
            oVentaDetallada.push(data);           
        };
       return JSON.parse(JSON.stringify(oVentaDetallada));
    };
    _POST = function (URL, data) {      
        $.ajax({
            data: data,
            url: URL,
            type: 'post',
            async: false,
            success: function (response) {
                json = response;
                window.open("http://"+URLactual+"/clubpacasmayo/" + json, "_blank");                
            }
        });
    },
    
    reportes_ventadetallada.Excel = function () {
        $("#Exportar_Excel").click(function(){             
            $('#preloader').fadeIn('slow');
            var data = _ObtenerFiltroDatos();  
            var fechaInicio = $("#fechainicio").val();
            var fechafin = $("#fechafin").val();
            _POST('?c=Reportes&a=VentaDetallada_Excel', {
                data: data,
                fechaInicio: fechaInicio,
                fechaFin: fechafin
            });            
            $('#preloader').fadeOut('slow');
            $('body').css({'overflow':'visible'});
        });
    },
    reportes_ventadetallada.datepicker = function (InputId) {
        $('#' + InputId).datetimepicker({
            viewMode: 'days',
            format: 'DD-MM-YYYY'
        });
    },
    reportes_ventadetallada.TableData = function (DivId) {
        var table;
        if ($.fn.dataTable.isDataTable('#' + DivId)) {
            table = $("#" + DivId).dataTable();
        } else {
            table = $("#" + DivId).dataTable({
                "scrollX": true,
                "bSort": false,
                "language": {
                    "search": "Buscar",
                    "lengthMenu": "Visualizar _MENU_ registro por página",
                    "zeroRecords": "No hay información para mostrar",
                    "info": "Pagina _PAGE_ de _PAGES_ de _MAX_ ",
                    "infoEmpty": "Pagina _PAGE_ - _PAGES_ de _MAX_ registros",
                    "infoFiltered": "(filtered from _MAX_ total records)",
                    "paginate": {
                        "previous": "Anterior",
                        "next": "Siguiente"
                    }
                }
            });
        }
        return table;
    };
    return reportes_ventadetallada;
})(reportes_ventadetallada || {});

$(document).ready(function () {
    reportes_ventadetallada.datepicker('fechainicio');
    reportes_ventadetallada.datepicker('fechafin');
    reportes_ventadetallada.TableData("example1");
    reportes_ventadetallada.Excel();
});