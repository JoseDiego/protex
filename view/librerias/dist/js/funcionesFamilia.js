function LimpiarFrm(){
    document.getElementById('Familia_Id').value="0";
    document.getElementById('sFamCodigo').value="";
    $('#sFamDescripcion').val("");
    $('#nFamPadre').prop('selectedIndex',0);
    $('#nFamTipo').prop('selectedIndex',0);
    $('#nFamEstado').prop('selectedIndex',0);
    $("#Eliminar").hide();
}
function GuardarFamilia(){
    var Familia_Id = document.getElementById('Familia_Id').value;
    var sFamCodigo = document.getElementById('sFamCodigo').value;
    var sFamDescripcion = document.getElementById('sFamDescripcion').value;
    var nFamPadre = document.getElementById('nFamPadre').value;
    var nFamTipo = document.getElementById('nFamTipo').value;
    var nFamEstado = document.getElementById('nFamEstado').value;
    $.post('?c=Familia&a=GuardarFamilia',{
        Familia_Id: Familia_Id,
        sFamCodigo: sFamCodigo,
        sFamDescripcion: sFamDescripcion,
        nFamPadre: nFamPadre,
        nFamTipo: nFamTipo,
        nFamEstado: nFamEstado
    },function(data){
        $('#resultado').html(data);
         LimpiarFrm();
         listarArbolContenido();
         limpiarAlert();
         FrmFamilia();
    });
}
function FrmFamilia(){
    $.post('?c=Familia&a=FrmFamilia', {        
    }, function(data){
        $("#Frm_Familia").html(data);
    });
}
function FrmEditarFamilia(Familia_Id){
    $.post('?c=Familia&a=FrmEditarFamilia', {     
        Familia_Id: Familia_Id
    }, function(data){
        $("#Frm_Familia").html(data);
        $("#Eliminar").show();
    });
}
function EliminarFamilia(){    
    var Familia_Id = document.getElementById('Familia_Id').value;
    var nFamPadre = document.getElementById('nFamPadre').value;
    $.post('?c=Familia&a=EliminarFamilia', {
        Familia_Id: Familia_Id,        
        nFamPadre: nFamPadre
    }, function(data){
        $("#Frm_Familia").html(data);
        $("#Eliminar").hide();
        $('#btnEliminar').hide();
        listarArbolContenido();
        limpiarAlert();
        FrmFamilia();
    });
}
function listarArbolContenido(){
    $.post('?c=Familia&a=ListarArbolContenido',{        
    }, function(data){
        $("#arbol_contenido").html(data);
    });
}

