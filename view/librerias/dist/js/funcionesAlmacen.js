function LimpiarFrmPaciente(){
    document.getElementById('Almacen_Id').value="0";
    document.getElementById('sAlmNombre').value="";
    document.getElementById('sAlmUbicacion').value="";
    $('#nAlmEstado').prop('selectedIndex',0);
}
function GuardarAlmacen(){
    var Almacen_Id = document.getElementById('Almacen_Id').value;
    var sAlmNombre = document.getElementById('sAlmNombre').value;
    var sAlmUbicacion = document.getElementById('sAlmUbicacion').value;
    var nAlmEstado = document.getElementById('nAlmEstado').value;
    $.post('?c=Almacen&a=GuardarAlmacen',{
         Almacen_Id: Almacen_Id,
         sAlmNombre: sAlmNombre,
         sAlmUbicacion: sAlmUbicacion,
         nAlmEstado: nAlmEstado
    },function(data){
         $('#resultado').html(data);
         LimpiarFrmPaciente();
         FrmAlmacen();
    });
}
function FrmEditarAlmacen(Almacen_Id){
    $('#Agregar').modal("show");
    $.post('?c=Almacen&a=FrmEditarAlmacen',{
        Almacen_Id: Almacen_Id
    }, function(data){
        $("#resultado").html(data);
    });
}
function MostrarAlmacen(Almacen_Id){
    $.post('?c=Almacen&a=MostrarAlmacen',{
        Almacen_Id: Almacen_Id
    }, function(data){
        $("#ver_datos").html(data);
    });
}
function FrmAlmacen(){
    $.post('?c=Almacen&a=FrmAlmacen', {        
    }, function(data){
        $("#example1").html(data);
    });
}