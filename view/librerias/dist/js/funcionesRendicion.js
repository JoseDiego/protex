function FrmGuardarRendicion(){
    $.post('?c=Rendicion&a=FrmGuardarRendicion',{        
    },function(data){
        $(".content-wrapper").html(data);
    });
}
function FrmEditarRendicion(Rendicion_Id){
    $.post('?c=Rendicion&a=FrmEditarRendicion',{   
        Rendicion_Id: Rendicion_Id
    },function(data){
        $(".content-wrapper").html(data);
    });
}
function mostrarRendicion(Rendicion_Id){
    $.post('?c=Rendicion&a=MostrarRendicion',{   
        Rendicion_Id: Rendicion_Id
    },function(data){
        $("#ver_datos").html(data);
    });
}
function limpiarCampos(){
    document.getElementById('Rendicion_Id').value = '0';
    document.getElementById('sRenNombre').value = '';
    document.getElementById('dRenMontoRecibido').value = '';
    document.getElementById('dRenFecha').value = '';
    document.getElementById('dRenFechaRendicion').value = '';
    document.getElementById('sRenRecepcionadoPor').value = '';
    document.getElementById('dRenTotal').value = '';
    document.getElementById('dRenSaldo').value = '';
    $('#Paciente_Id').prop('selectedIndex',0);
    $('#nRenEstado').prop('selectedIndex',0);
} 
function GuardarRendicion(){
    var Rendicion_Id = document.getElementById('Rendicion_Id').value;
    var sRenNombre = document.getElementById('sRenNombre').value;
    var dRenMontoRecibido = document.getElementById('dRenMontoRecibido').value;
    var dRenFecha = document.getElementById('dRenFecha').value;
    var dRenFechaRendicion = document.getElementById('dRenFechaRendicion').value;
    var sRenRecepcionadoPor = document.getElementById('sRenRecepcionadoPor').value;
    var dRenTotal = document.getElementById('dRenTotal').value;
    var dRenSaldo = document.getElementById('dRenSaldo').value;
    var Paciente_Id = document.getElementById('Paciente_Id').value;
    var nRenEstado = document.getElementById('nRenEstado').value;
    $.post('?c=Rendicion&a=GuardarRendicion',{
        Rendicion_Id: Rendicion_Id,
        sRenNombre: sRenNombre,
        dRenMontoRecibido: dRenMontoRecibido,
        dRenFecha: dRenFecha,
        dRenFechaRendicion: dRenFechaRendicion,
        sRenRecepcionadoPor: sRenRecepcionadoPor,
        dRenTotal: dRenTotal,
        dRenSaldo: dRenSaldo,
        Paciente_Id: Paciente_Id,
        nRenEstado: nRenEstado
    },function(data){
        $('#resultado').html(data);
        limpiarCampos();
        $('html, body').animate({ scrollTop: '0px'}, 1000);
    });
}
function LimpiarComprobante(){
    document.getElementById('post_edit').value="-1";
    document.getElementById('Comprobante_Id').value="0";
    document.getElementById('dComFecha').value="";
    document.getElementById('nComNumero').value="";
    document.getElementById('sComDescripcion').value="";
    document.getElementById('nComCantidad').value="";
    document.getElementById('nComPrecioUnitario').value="";
    document.getElementById('dComTotal').value="";
}
function GuardarComprobante(){
    var post = document.getElementById('post_edit').value;
    var Comprobante_Id = document.getElementById('Comprobante_Id').value;
    var dComFecha = document.getElementById('dComFecha').value;
    var nComNumero = document.getElementById('nComNumero').value;
    var sComDescripcion = document.getElementById('sComDescripcion').value;
    var nComCantidad = document.getElementById('nComCantidad').value;
    var nComPrecioUnitario = document.getElementById('nComPrecioUnitario').value;
    $.post('?c=Rendicion&a=AgregarComprobante',{
        post: post,
        Comprobante_Id: Comprobante_Id,
        dComFecha: dComFecha,
        nComNumero: nComNumero,
        sComDescripcion: sComDescripcion,
        nComCantidad: nComCantidad,
        nComPrecioUnitario: nComPrecioUnitario
    },function(data){
        $('#comprobantes').html(data);
        LimpiarComprobante();
        $("#agregar").modal('hide');
    });
}
function EditarComprobante(post){
    $.post('?c=Rendicion&a=MostrarComprobante', {
        post: post
    }, function(data){
        $("#resultado").html(data);
    });
}
function EliminarComprobante(){
    var post = document.getElementById('post').value;
    $.post('?c=Rendicion&a=EliminarComprobante',{  
        post: post
    }, function(data){
        $('#comprobantes').html(data);
        $("#modal-eliminar").modal('hide');
    });
}
function calcularTotal(){
    var cantidad = document.getElementById('nComCantidad').value;
    var precio = document.getElementById('nComPrecioUnitario').value;
    var total = cantidad*precio;
    document.getElementById('dComTotal').value = total;
}
function calcularSaldo(){
    var monto = parseFloat(document.getElementById('dRenMontoRecibido').value);
    var total = parseFloat(document.getElementById('dRenTotal').value);
    $.post('?c=Rendicion&a=CalcularSaldo',{
        monto:monto,
        total:total
    }, function(data){
        $("#resultado_comprobante").html(data);
    });
}