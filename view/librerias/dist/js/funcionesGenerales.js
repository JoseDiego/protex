jQuery.validator.addMethod("letras", function (value, element) {
    return this.optional(element) || /^[a-záéóóúàèìòùäëïöüñ\s]+$/i.test(value);
});
jQuery.validator.addMethod("decimales", function (value, element) {
    return this.optional(element) || /^([0-9]+\.+[0-9]|[0-9])+$/.test(value);
});
jQuery.validator.addMethod("fecha", function (value, element) {
    return this.optional(element) || /^([0-9]{2}\/[0-9]{2}\/[0-9]{4})$/.test(value);
});
$("#FrmIngresoCompra").validate({
    errorClass: 'error',
    rules: {        
        nombre: {            
           required: true,
           letras: true
        },
        login: {
           required: true
        },
        contraseñaOne: {
           required: true
        },
        contraseñaTwo: {
           required: true,
           equalTo: "#contraseñaOne"
        },
        tipo: {
           required: true
        },
        estado: {
           required: true
        }
    },
    messages: {        
        nombre: "Ingrese nombre valido solo letras.",
        login: "Ingrese login porfavor.",
        contraseñaOne: "Ingrese contraseña que utilizara.",
        contraseñaTwo: "Por favor ingrese nuevamente la contraseña.",
        tipo: "seleccione el tipo de usuario.",
        estado: "seleccione estado"
    }
});

$("#FrmPedido").validate({
    errorClass: 'error',
    rules: {        
        OrigenDestino_Id: {            
           required: true
        },
        dPedFecha: {
            required: true
        },
        nPedTipopago: {
            required: true
        },
        Documento_Id: {
            required: true
        }
    },
    messages: {        
        OrigenDestino_Id: "Seleccione cliente por favor.",
        dPedFecha: "Ingrese fecha por favor.",
        nPedTipopago: "Seleccione el tipo de pago por favor.",
        Documento_Id: "Seleccione el tipo de documento por favor"
    }
});

//jQuery.validator.setDefaults({
//  debug: true,
//  success: "valid"
//});
var form = $( "#FrmDetalleProducto" );
form.validate({
    errorClass: 'error',
    rules: {        
        nPedDetCantidad: {            
           required: true
        },
        Unidadmedida_Id: {
            required: true
        },
        dPedDetPrecioUnitario: {
            required: true
        }
    },
    messages: {        
        nPedDetCantidad: "Ingrese cantidad por favor.",
        Unidadmedida_Id: "Seleccione Unidad de Medida por favor.",
        dPedDetPrecioUnitario: "Ingrese Precio Unitario."
    }
});
$( "#FrmDetalleProducto" ).submit(function() {
    if(form.valid() == true){
        return true;
    }else{
        return false;
    }
});



