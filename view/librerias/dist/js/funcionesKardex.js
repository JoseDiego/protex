var reportes_Kardex = (function (reportes_Kardex, undefined) {
    var URLactual = window.location.host;
    _ObtenerFiltroDatos = function () {
        var table = reportes_Kardex.TableData('example1');
        var oVentaDetallada = [];        
        var filteredrows = table._('tr', {"filter": "applied"});
        for (var i = 0; i < filteredrows.length; i++) {
            var data = {};
            data.FechaHora = filteredrows[i][0];
            data.TipoMovimiento = filteredrows[i][1];
            data.Almacen = filteredrows[i][2];
            data.Documento = filteredrows[i][3];
            data.FechaDocumento = filteredrows[i][4];
            data.OrigenDestino = filteredrows[i][5];
            data.Estado = filteredrows[i][6];
            data.Lote = filteredrows[i][7];
            data.Cantidad = filteredrows[i][8];
            data.Unidad = filteredrows[i][9];
            data.SaldoAnteriorLote = filteredrows[i][10];
            data.SaldoPosteriorLote = filteredrows[i][11];
            data.SaldoAnterior = filteredrows[i][12];
            data.SaldoPosterior = filteredrows[i][13];
            oVentaDetallada.push(data);           
        };
       return JSON.parse(JSON.stringify(oVentaDetallada));
    };
    _POST = function (URL, data) {      
        $.ajax({
            data: data,
            url: URL,
            type: 'post',
            async: false,
            success: function (response) {
                json = response;
                window.open("http://"+URLactual+"/clubpacasmayo/" + json, "_blank");                
            }
        });
    },
    
    reportes_Kardex.Excel = function () {
        $("#Exportar_Excel").click(function(){           
            $('#preloader').fadeIn('slow');
            var data = _ObtenerFiltroDatos();  
            var fechaInicio = $("#fechainicio").val();
            var fechafin = $("#fechafin").val();
            _POST('?c=Reportes&a=Kardex_Excel', {
                data: data,
                fechaInicio: fechaInicio,
                fechaFin: fechafin
            });
            $('#preloader').fadeOut('slow');
            $('body').css({'overflow':'visible'});
        });
    },
    reportes_Kardex.datepicker = function (InputId) {
        $('#' + InputId).datetimepicker({
            viewMode: 'days',
            format: 'DD-MM-YYYY'
        });
    },
    reportes_Kardex.TableData = function (DivId) {
        var table;
        if ($.fn.dataTable.isDataTable('#' + DivId)) {
            table = $("#" + DivId).dataTable();
        } else {
            table = $("#" + DivId).dataTable({
                "scrollX": true,
                "bSort": false,
                "language": {
                    "search": "Buscar",
                    "lengthMenu": "Visualizar _MENU_ registro por página",
                    "zeroRecords": "No hay información para mostrar",
                    "info": "Pagina _PAGE_ de _PAGES_ de _MAX_ ",
                    "infoEmpty": "Pagina _PAGE_ - _PAGES_ de _MAX_ registros",
                    "infoFiltered": "(filtered from _MAX_ total records)",
                    "paginate": {
                        "previous": "Anterior",
                        "next": "Siguiente"
                    }
                }
            });
        }
        return table;
    };
    return reportes_Kardex;
})(reportes_Kardex || {});

$(document).ready(function () {
    reportes_Kardex.datepicker('fechainicio');
    reportes_Kardex.datepicker('fechafin');
    reportes_Kardex.TableData("example1");
    reportes_Kardex.Excel();
});