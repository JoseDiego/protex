
function limpiarFrm(){
    document.getElementById('ProdServ_Id').value = '0';
    document.getElementById('sProSrvCodigo1').value = '';
    document.getElementById('sProSrvCodigo2').value = '';
    document.getElementById('sProSrvNombre').value = '';
    document.getElementById('sProSrvCodigoBarras').value = '';
    $('#Familia_Id').prop('selectedIndex',0);
    $('#Activo_Id').prop('selectedIndex',0);
    $('#Unidadmedida_Id').prop('selectedIndex',0);
//    $("#nProSrvExoneradoIGV").prop("checked", false);
    $('#nProSrvEstado').prop('selectedIndex',0);
}

function GuardarProdserv() {
    var ProdServ_Id = document.getElementById('ProdServ_Id').value;
    var sProSrvCodigo1 = document.getElementById('sProSrvCodigo1').value;
    var sProSrvCodigo2 = document.getElementById('sProSrvCodigo2').value;
    var sProSrvCodigo = sProSrvCodigo1 + '-' + sProSrvCodigo2;
    var sProSrvNombre = document.getElementById('sProSrvNombre').value;
    var sProSrvCodigoBarras = document.getElementById('sProSrvCodigoBarras').value;
    var Familia_Id = document.getElementById('Familia_Id').value;
    var Activo_Id = document.getElementById('Activo_Id').value;
    var Unidadmedida_Id = document.getElementById('Unidadmedida_Id').value;
    var ProSrvEspecificacion = document.getElementById('ProSrvEspecificacion').value;
    var nProSrvExoneradoIGV;
    var nProSrvExoneradoIGV = '0';
    if($('#nProSrvExoneradoIGV').is(':checked')){
        nProSrvExoneradoIGV = '1';
    }
    var nProSrvEstado = document.getElementById('nProSrvEstado').value;
    $.post('?c=Productos&a=GuardarProdserv', {
        ProdServ_Id: ProdServ_Id,
        sProSrvCodigo: sProSrvCodigo,
        sProSrvNombre: sProSrvNombre,
        sProSrvCodigoBarras: sProSrvCodigoBarras,
        Familia_Id: Familia_Id,
        Activo_Id: Activo_Id,
        Unidadmedida_Id: Unidadmedida_Id,
        nProSrvExoneradoIGV: nProSrvExoneradoIGV,
        nProSrvEstado: nProSrvEstado,
        ProSrvEspecificacion: ProSrvEspecificacion
    }, function (data) {
        $('#resultado').html(data);
        $('#btnCargarImagen').prop("disabled", false);
        limpiarFrm();
        ListarReceta();
        $('html, body').animate({scrollTop: '0px'}, 1000);
    });
}

function GuardarProdservicio(){
    var ProdServ_Id = document.getElementById('ProdServ_Id').value;
    var sProSrvCodigo1 = document.getElementById('sProSrvCodigo1').value;
    var sProSrvCodigo2 = document.getElementById('sProSrvCodigo2').value;
    var sProSrvCodigo = sProSrvCodigo1 + '-' + sProSrvCodigo2;
    var sProSrvNombre = document.getElementById('sProSrvNombre').value;
    var sProSrvCodigoBarras = document.getElementById('sProSrvCodigoBarras').value;
    var Familia_Id = document.getElementById('Familia_Id').value;
    var Activo_Id = document.getElementById('Activo_Id').value;
    var Unidadmedida_Id = document.getElementById('Unidadmedida_Id').value;
    var ProSrvEspecificacion = document.getElementById('ProSrvEspecificacion').value;
    var nProSrvExoneradoIGV;
    var nProSrvExoneradoIGV = '0';
    if($('#nProSrvExoneradoIGV').is(':checked')){
        nProSrvExoneradoIGV = '1';
    }
    var nProSrvEstado = document.getElementById('nProSrvEstado').value;
    $.post('?c=Productos&a=GuardarProdserv', {
        ProdServ_Id: ProdServ_Id,
        sProSrvCodigo: sProSrvCodigo,
        sProSrvNombre: sProSrvNombre,
        sProSrvCodigoBarras: sProSrvCodigoBarras,
        Familia_Id: Familia_Id,
        Activo_Id: Activo_Id,
        Unidadmedida_Id: Unidadmedida_Id,
        nProSrvExoneradoIGV: nProSrvExoneradoIGV,
        nProSrvEstado: nProSrvEstado,
        ProSrvEspecificacion: ProSrvEspecificacion
    }, function (data) {
        $("input[name='file']").on("change", function(){
            //e.preventDefault();
            var formData = new FormData($("#formuploadajax")[0]);
            $.ajax({
                    url: 'view/subirImgPro.php',
                    type: "post",
                    data: formData,
                    cache: false,
                    contentType: false,
             processData: false
                });
        });
        $('#resultado').html(data);
        $('#btnCargarImagen').prop("disabled", false);
        limpiarFrm();
        ListarReceta();
        $('html, body').animate({scrollTop: '0px'}, 1000);
    });
    //$(function(){
        
    //});
}

function FrmGuardarProductos(){
    $.post('?c=Productos&a=FrmGuardarProductos',{        
    },function(data){
        $(".content-wrapper").html(data);
    });
}
function FrmEditarProductos(ProdServ_Id){
    $.post('?c=Productos&a=FrmEditarProductos',{   
        ProdServ_Id: ProdServ_Id
    },function(data){
        $(".content-wrapper").html(data);        
    });
}
function mostrarProducto(ProdServ_Id){
    $.post('?c=Productos&a=MostrarProducto',{   
        ProdServ_Id: ProdServ_Id
    },function(data){
        $("#ver_datos").html(data);
    });
}
function ListarPrecios(codigo){
    $.post('?c=Precioventa&a=ListarPrecios',{   
        codigo: codigo
    },function(data){
        $("#productos_precios").html(data);
    });
}
function EliminarPrecio(){
    var IdPrecioVenta = document.getElementById('IdPrecioVenta').value;
    
    $.post('?c=Precioventa&a=EliminarPrecioventa',{
        IdPrecioVenta: IdPrecioVenta
    },function(data){
        
       $("#resultado_precio").html(data); 
    });
    
}
function LimpiarFrmReceta(){
    $('#Receta_ProdServ_Id').prop('selectedIndex',0);
    $('#Receta_UnidadMedida_Id').prop('selectedIndex',0);
    document.getElementById('nRecCantidad').value = '1';
}
function AgregarReceta(){
    if(ValidarFrmReceta()==true){
        var ProdServ_Id = document.getElementById('Receta_ProdServ_Id').value;
        var UnidadMedida_Id = document.getElementById('Receta_UnidadMedida_Id').value;
        var nRecCantidad = document.getElementById('nRecCantidad').value;
        $.post('?c=Receta&a=AgregarReceta',{
            ProdServ_Id: ProdServ_Id,
            UnidadMedida_Id: UnidadMedida_Id,
            nRecCantidad: nRecCantidad
        },function(data){
            $("#resultado_recete").html(data);
            ListarReceta();
            LimpiarFrmReceta();
        });
    }else{
        if(document.getElementById('Receta_ProdServ_Id').value == ""){
            document.getElementById('Receta_ProdServ_Id').focus();
        }else if(document.getElementById('Receta_UnidadMedida_Id').value == ""){
            document.getElementById('Receta_UnidadMedida_Id').focus();
        }
        $("#resultado_recete").html('<div class="alert alert-warning alert-dismissible" role="alert">'+
                                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
                                        '<strong>Advertencia!</strong> No ah seleccionado el producto o la unidad de medida.'+
                                     '</div>');
    }
}
function ValidarFrmReceta(){
    if(document.getElementById('Receta_ProdServ_Id').value == "" || document.getElementById('Receta_UnidadMedida_Id').value == ""){
        return false;
    }else{
        return true;
    }
}
function ListarReceta(){
    $.post('?c=Receta&a=ListarReceta',{        
    },function(data){
        $("#lista_recetas").html(data);
    });
}
function EliminarReceta(){   
    var post = document.getElementById('post').value;
    $.post('?c=Receta&a=EliminarReceta',{  
        post: post
    },function(data){
//        $("#lista_recetas").html(data);
        ListarReceta();
        $("#modal-eliminar").modal("hide");
    });
}

function UltimoCodigo(){
    var Familia_Id = document.getElementById('Familia_Id').value;
    $.post('?c=Productos&a=UltimoCodigo',{
        Familia_Id: Familia_Id
    }, function(data){
        var codigo = data.split('-');
        console.log(codigo);
        document.getElementById('sProSrvCodigo1').value = codigo[0];
        document.getElementById('sProSrvCodigo2').value = parseInt(codigo[1])+1;

    });
}

function listarProveedor(){
            	var OrigenDestino_Id=0;
            	$.post('?c=ordencompra&a=ListarComboBoxProveedor',{
                         OrigenDestino_Id: OrigenDestino_Id
                    },function(data){
                        $("#Proveedor_Id").html(data);
               });
            }
            
function ProductosUnidades(){
    var ProdServ_Id = document.getElementById('Receta_ProdServ_Id').value;
    $.post('?c=Movimiento&a=ListarUnidades', {
        ProdServ_Id: ProdServ_Id
    }, function(data){
        $("#Receta_UnidadMedida_Id").html(data);
        $("#Receta_UnidadMedida_Id").prop('disabled', false);
    });
}