function BusquedaProducto(){
    var txtNameProduct = document.getElementById('txtNameProduct').value;
    $.post('?c=Productos&a=FiltroProducto',{
        txtNameProduct: txtNameProduct
    },function(data){
        $("#productos_filtro").html(data);
    });
}
function CerrarPedido(){
    var Pedido_Id = document.getElementById('Id_Pedido').value;
    $.post('?c=Pedido&a=CerrarPedido', {
        Pedido_Id: Pedido_Id
    }, function (data) {        
        $("#resultado").html(data);
    });
}
function EditarPedido(Pedido_Id) {
    $.post('?c=Pedido&a=FrmPedidoEditar', {
        Pedido_Id: Pedido_Id
    }, function (data) {
        $("#resultado").html(data);
    });
}
function LimpiarFrmPedido() {   
    EliminarSessionDetalle();
    document.getElementById('Pedido_Id').value = "0";
    $('#OrigenDestino_Id').prop('selectedIndex', 0);
    FechaActual();
    $('#nPedTipopago').prop('selectedIndex', 0);
//    $('#Documento_Id').prop('selectedIndex', 0);
    document.getElementById('dPedImporte').value = "";
    document.getElementById('dPedIGV').value = "";
    document.getElementById('dPedTotal').value = "";
    $('#sPedObservaciones').val("");
    $('#nPedEstado').prop('selectedIndex', 0);
}
function GuardarPedido() {
    var Pedido_Id = document.getElementById('Pedido_Id').value;
    var OrigenDestino_Id = document.getElementById('OrigenDestino_Id').value;
    var dPedFecha = document.getElementById('dPedFecha').value;
    var nPedTipopago = document.getElementById('nPedTipopago').value;
    var Documento_Id = document.getElementById('Documento_Id').value;
//    var sPedDocumento = document.getElementById('sMovDocumento').value;
    var dPedImporte = document.getElementById('dPedImporte').value;
    var dPedIGV = document.getElementById('dPedIGV').value;
    var dPedTotal = document.getElementById('dPedTotal').value;
    var sPedObservaciones = document.getElementById('sPedObservaciones').value;
    var nPedEstado = 1;
    $.post('?c=Pedido&a=GuardarPedido', {
        Pedido_Id: Pedido_Id,
        OrigenDestino_Id: OrigenDestino_Id,
        dPedFecha: dPedFecha,
        nPedTipopago: nPedTipopago,
        Documento_Id: Documento_Id,
//        sPedDocumento: sPedDocumento,
        dPedImporte: dPedImporte,
        dPedIGV: dPedIGV,
        dPedTotal: dPedTotal,
        sPedObservaciones: sPedObservaciones,
        nPedEstado: nPedEstado
    }, function (data) {
        $('#resultado').html(data);
//        LimpiarFrmPedido();
//        ListarProVent();
        ListarPedidos();
        limpiarAlert();
    });
}
function PrecioProducto() {
    var ProdServ_Id = document.getElementById('ProdServ_Id').value;
    var Unidadmedida_Id = document.getElementById('Unidadmedida_Id').value;
    $.post('?c=Productos&a=PrecioProducto', {
        ProdServ_Id: ProdServ_Id,
        Unidadmedida_Id: Unidadmedida_Id
    }, function (data) {
        document.getElementById('dPedDetPrecioUnitario').value = data;
    });
}
function NuevoPedido(){
    Habilitar();
    LimpiarFrmPedido();
    
    LimpiarFrmPedido();
    ListarProVent();
}
//Inhabilitar
function Habilitar() {
    $("#OrigenDestino_Id").prop("disabled", false);
    $("#dPedFecha").prop("disabled", false);
    $("#nPedTipopago").prop("disabled", false);
    $("#Documento_Id").prop("disabled", false);
    $(".btn-agregarproserv").prop("disabled", false);
    $("#dPedImporte").prop("disabled", false);
    $("#dPedIGV").prop("disabled", false);
    $("#dPedTotal").prop("disabled", false);
    $("#sPedObservaciones").prop("disabled", false);
    $(".btn-guardar").prop("disabled", false);
    $(".btn-cancelar").prop("disabled", false);
}
function deshabilitarPedido() {
    $("#OrigenDestino_Id").prop("disabled", true);
    $("#dPedFecha").prop("disabled", true);
    $("#nPedTipopago").prop("disabled", true);
    $("#Documento_Id").prop("disabled", true);
    $(".btn-agregarproserv").prop("disabled", true);
    $("#dPedImporte").prop("disabled", true);
    $("#dPedIGV").prop("disabled", true);
    $("#dPedTotal").prop("disabled", true);
    $("#sPedObservaciones").prop("disabled", true);
    $(".btn-guardar").prop("disabled", true);
    $(".btn-cancelar").prop("disabled", true);
}
function CerrarVentanas() {
    $("#productos_detalle").modal("hide");
}
function producto_detalle(ProdServ_Id) {
    document.getElementById('ProdServ_Id').value = ProdServ_Id;
    document.getElementById('nPedDetCantidad').value = "1";
    ProductosUNILista2();
}

function ProductosUNILista2(){
    var ProdServ_Id = document.getElementById('ProdServ_Id').value;
    $.post('?c=Productos&a=ListarUnidades', {
        ProdServ_Id: ProdServ_Id
    }, function(data){
        $("#Unidadmedida_Id").html(data);
    });
}
function FiltrarProductos(Familia_Id) {
//        $("#familias").modal("hide");
    $.post('?c=Ventas&a=ProdServListVenta', {
        Familia_Id: Familia_Id
    }, function (data) {
        $("#resultado_productos").html(data);
        $("#productos").modal("show");
    });
}
function LimpiarProdVent(){
    document.getElementById('nPedDetCantidad').value = '1';
    $('#Unidadmedida_Id option[value=""]').attr('selected','selected');
    document.getElementById('dPedDetPrecioUnitario').value = "";
}
function AgregarProdVent() {
    /*
     ProdServ_Id
     sMovDetConCantidad
     Unidadmedida_Id
     nMovDetConPrecioUnitario
     */
    var ProdServ_Id = document.getElementById('ProdServ_Id').value;
    var nPedDetCantidad = document.getElementById('nPedDetCantidad').value;
    var Unidadmedida_Id = document.getElementById('Unidadmedida_Id').value;
    var dPedDetPrecioUnitario = document.getElementById('dPedDetPrecioUnitario').value;
    var nIncluyeLoteIGV = 0;
    if($("#nIncluyeVentIGV").prop("checked") == true){
        nIncluyeLoteIGV = 1;
    }
    
    $.post('?c=Productos&a=VerificarStock', {
        ProdServ_Id: ProdServ_Id,
        nPedDetCantidad: nPedDetCantidad,
        Unidadmedida_Id: Unidadmedida_Id
    }, function (data) {
        if(parseInt(JSON.parse(data)) == 1){
            $.post('?c=Ventas&a=AgregarProducto', {
                ProdServ_Id: ProdServ_Id,
                nPedDetCantidad: nPedDetCantidad,
                Unidadmedida_Id: Unidadmedida_Id,
                dPedDetPrecioUnitario: parseFloat(dPedDetPrecioUnitario),
                nIncluyeLoteIGV: nIncluyeLoteIGV
            }, function (data) {
                $("#resultado").html(data);
                ListarProVent();
                LimpiarProdVent();
                $("#productos_detalle").modal("hide");
                CalcularImplIGVTot();
            });
        }else{            
            $("#mensaje_stock").html('<div class="alert alert-warning alert-dismissible" role="alert">'+
                                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
                                        '<strong>Advertencia!</strong> No cuenta con stock suficiente.'+
                                     '</div>');
            setTimeout(function(){
                $("#mensaje_stock .alert").hide();
            }, 3000);
        }
    });
}

function CancelarPedido() {
    deshabilitarPedido();
    LimpiarFrmPedido();
    $.post('?c=Pedido&a=EliminarSessiones', {
    }, function (data) {
        $("#resultado").html(data);
        ListarProVent();
    });
}
function ListarProVent() {
    $.post('?c=Ventas&a=ListarProducto', {
    }, function (data) {
        $("#productos_servicios").html(data);
    });
}
function EliminarProducto(index) {
    $.post('?c=Ventas&a=EliminarProducto', {
        index: index
    }, function (data) {
        $("#resultado").html(data);
        ListarProVent();
        CalcularImplIGVTot();
    });
}
function CalcularImplIGVTot() {
    var Documento_Id = document.getElementById('Documento_Id').value;
    $.post('?c=Ventas&a=CalcularImplIGVTot', {
        Documento_Id: Documento_Id
    }, function (data) {
        $("#resultado").html(data);
    });
}
function FechaActual(){
    $.post('?c=Metodos&a=FechaActual',{        
    },function(data){        
        document.getElementById('dPedFecha').value = data;
    });
}
function EliminarSessionDetalle(){
    $.post('?c=Pedido&a=EliminarSessiones',{        
    },function(data){   
    });
}
function ListarPedidos(){
    $.post('?c=Ventas&a=ListaPedidos',{
        
    },function(data){
        $("#lista_pedidos").html(data);
    });
}