function LimpiarFrm(){
    document.getElementById('Unidadmedida_Id').value="0";
    $('#sUndDescripcion').val("");
    document.getElementById('sUndAlias').value="";
    $('#sUndAlias').prop('selectedIndex',0);
    $('#nUndPadre_Id').prop('selectedIndex',0);
    document.getElementById('nUndFac_Cnv').value="";
    $('#nUndEstado').prop('selectedIndex',0);
    $("#Eliminar").hide();
}
function GuardarUnidadmedida() {
    var Unidadmedida_Id = document.getElementById('Unidadmedida_Id').value;
    var sUndDescripcion = document.getElementById('sUndDescripcion').value;
    var sUndAlias = document.getElementById('sUndAlias').value;
    var nUndPadre_Id = document.getElementById('nUndPadre_Id').value;
    var nUndFac_Cnv = document.getElementById('nUndFac_Cnv').value;
    var nUndEstado = document.getElementById('nUndEstado').value;
    $.post('?c=Unidadesmedida&a=GuardarUnidadmedida', {
        Unidadmedida_Id: Unidadmedida_Id,
        sUndDescripcion: sUndDescripcion,
        sUndAlias: sUndAlias,
        nUndPadre_Id: nUndPadre_Id,
        nUndFac_Cnv: nUndFac_Cnv,
        nUndEstado: nUndEstado
    }, function (data) {
        $('#resultado').html(data);
         LimpiarFrm();
         listarArbolContenido();
         limpiarAlert();
         FrmUnidadmedida();
    });
}
function FrmUnidadmedida(){
    $.post('?c=Unidadesmedida&a=FrmUnidadmedida', {        
    }, function(data){
        $("#Frm_Unidadmedida").html(data);
    });
}
function FrmEditarUnidadmedida(Unidadmedida_Id){
    $.post('?c=Unidadesmedida&a=FrmEditarUnidadmedida', {     
        Unidadmedida_Id: Unidadmedida_Id
    }, function(data){
        $("#Frm_Unidadmedida").html(data);
        $("#Eliminar").show();
    });
}
function EliminarUnidadmedida(){ 
    var Unidadmedida_Id = document.getElementById('Unidadmedida_Id').value;
    var nUndPadre_Id = document.getElementById('nUndPadre_Id').value;
    $.post('?c=Unidadesmedida&a=EliminarUnidadmedida', {
        Unidadmedida_Id: Unidadmedida_Id,        
        nUndPadre_Id: nUndPadre_Id
    }, function(data){
        $("#Frm_Unidadmedida").html(data);
         listarArbolContenido();
         limpiarAlert();
         FrmUnidadmedida();
        $("#Eliminar").hide();
        $('#btnEliminar').hide();
        
    });
}
function listarArbolContenido(){
    $.post('?c=Unidadesmedida&a=ListarArbolContenido',{        
    }, function(data){
        $("#arbol_contenido").html(data);
    });
}