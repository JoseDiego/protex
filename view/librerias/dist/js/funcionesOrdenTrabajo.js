function FrmEditarOrdenTrabajo(OrdenTrabajo_Id){
    $.post('?c=Ordentrabajo&a=FrmEditarOrdenTrabajo',{   
        OrdenTrabajo_Id: OrdenTrabajo_Id
    },function(data){
        $(".content-wrapper").html(data);
    });
}
function NuevaOrdenTrabajo(){
    $.post('?c=OrdenTrabajo&a=NuevaOrdenTrabajo',{        
    },function(data){
        $(".content-wrapper").html(data);
    });
}



function LimpiarDatos(){
    document.getElementById('otFecha').placeholder = "Fecha";//FECHA
    document.getElementById('ordCompra').value = " ";
    document.getElementById('Cliente_Id').selectedIndex= "0";//tipo de pago
    document.getElementById('ordentrabajo').selectedIndex = "0";
    document.getElementById('otFechaE').placeholder = "Fecha";
    
    var table = document.getElementById( "datos" );
    for ( var i = 1; i < table.rows.length; i++ ) {
        table.rows[i].deleteRow(i);
    }
    
}

function ListarFechas(){
    var fInicio = document.getElementById('fechaInicio').value;
    var fFin = document.getElementById('fechaFin').value;
    $.post('?c=OrdenTrabajo&a=ListarFechasO', {
        fInicio: fInicio,
        fFin: fFin
        }, function (data) {
        $("#bodyt").html(data);
        $('html, body').animate({scrollTop: '0px'}, 1000);
    });
}

function AgregarDetalle() {
    $('html, body').animate({ scrollTop: '0px'}, 1000);
}

function reqMateriales(){
    var table = document.getElementById( "datos" );
    var tableArr = [];
        for ( var i = 1; i < table.rows.length; i++ ) {
          tableArr.push({
               ProdServ_Id: table.rows[i].cells[0].innerHTML,
               ProdservdNombre: table.rows[i].cells[1].innerHTML,
               UnidadNombre: table.rows[i].cells[2].innerHTML,
               UnidadMedida_Id: table.rows[i].cells[3].innerHTML,
               nPedDetCantidad: table.rows[i].cells[4].innerHTML,
               nOTCantidadProg: table.rows[i].cells[5].innerHTML,
               nOTCantidadRest: table.rows[i].cells[6].innerHTML
          });
          
         }
         //alert(JSON.stringify(tableArr));
         var jsonArray = JSON.stringify(tableArr);
         $.post('?c=OrdenTrabajo&a=validarStock', {
            jsonArray:jsonArray
//        nMovEstado: nMovEstado
    }, function (data) {
        $("#ver_datos").html(data);
       
    });
}
function GuardarOrdenTrabajo(){
    
    var OrdenTrabajo_Id = document.getElementById('OrdenTrabajo_Id').value;
    var otFecha = document.getElementById('otFecha').value;
    var ordCompra = document.getElementById('ordCompra').value;
    var Cliente_Id = document.getElementById('Cliente_Id').value;
    var ordentrabajo = document.getElementById('ordentrabajo');
    var nroOrdenTrabajo = ordentrabajo.options[ordentrabajo.selectedIndex].text; 
    var otFechaE = document.getElementById('otFechaE').value;
    var table = document.getElementById( "datos" );
    var tableArr = [];
        for ( var i = 1; i < table.rows.length; i++ ) {
          tableArr.push({
               ProdServ_Id: table.rows[i].cells[0].innerHTML,
               ProdservdNombre: table.rows[i].cells[1].innerHTML,
               UnidadNombre: table.rows[i].cells[2].innerHTML,
               UnidadMedida_Id: table.rows[i].cells[3].innerHTML,
               nPedDetCantidad: table.rows[i].cells[4].innerHTML,
               nOTCantidadProg: table.rows[i].cells[5].innerHTML,
               nOTCantidadRest: table.rows[i].cells[6].innerHTML
          });
          
         }
         //alert(JSON.stringify(tableArr));
         var jsonArray = JSON.stringify(tableArr);
//    var nMovEstado = document.getElementById('nMovEstado').value;
    $.post('?c=OrdenTrabajo&a=GuardarOrdenTrabajo', {
            otFecha:otFecha,
            ordCompra:ordCompra,
            Cliente_Id:Cliente_Id,
            nroOrdenTrabajo:nroOrdenTrabajo,
            otFechaE:otFechaE,
            OrdenTrabajo_Id: OrdenTrabajo_Id, 
            jsonArray:jsonArray
//        nMovEstado: nMovEstado
    }, function (data) {
        $("#resultado").html(data);
        LimpiarDatos();
        $('html, body').animate({scrollTop: '0px'}, 1000);
    });
}

function mostrarOrdenTrabajo(OrdenTrabajo_Id){
    $.post('?c=OrdenTrabajo&a=MostrarOrdenTrabajo',{   
        OrdenTrabajo_Id: OrdenTrabajo_Id
    },function(data){
        //alert(data);
        $("#ver_datos").html(data);
    });
}

