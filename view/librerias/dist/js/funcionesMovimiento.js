function MostrarMovimiento (Movimiento_Id){
    $.post('?c=Movimiento&a=MostrarMovimiento',{  
        Movimiento_Id: Movimiento_Id
    },function(data){
        $("#ver_datos").html(data);
    });
}
function FrmGuardarMovimiento(){
    $.post('?c=Movimiento&a=FrmGuardarMovimiento',{        
    },function(data){
        $(".content-wrapper").html(data);
    });
}
function FrmEditarMovimiento(Movimiento_Id){
    $.post('?c=Movimiento&a=FrmEditarMovimiento',{       
        Movimiento_Id: Movimiento_Id
    },function(data){
        $(".content-wrapper").html(data);
    });
}
function FrmEditarUsuario(Usuario_Id){
    $.post('?c=Usuario&a=FrmEditarUsuario',{   
        Usuario_Id: Usuario_Id
    },function(data){
        $(".content-wrapper").html(data);
    });
}
function mostrarUsuario(Usuario_Id){
    $.post('?c=Usuario&a=MostrarUsuario',{   
        Usuario_Id: Usuario_Id
    },function(data){
        $("#ver_datos").html(data);
    });
}
function Limpiar(){
    document.getElementById('Movimiento_Id').value = '0';
    document.getElementById('dMovFecha').value = '';
    $('#Tipomovimiento_Id').prop('selectedIndex',0);
    $('#nMovTipopago').prop('selectedIndex',0);
//    $('#nMovTipoOrigenDestino').prop('selectedIndex',0);
    $('#nMovOrigenDestino_Id').prop('selectedIndex',0);
    $('#nMovTipodestino').prop('selectedIndex',0);
    $('#Almacen_Id').prop('selectedIndex',0);
    $('#Documento_Id').prop('selectedIndex',0);
    document.getElementById('sMovDocumento').value = '';
    document.getElementById('sMovDocReferencia').value = '';
    $('#Moneda_Id').prop('selectedIndex',0);
    if(document.getElementById('Moneda_Id').value == '2'){
        document.getElementById('nMovTipoCambio').value = '1';
    }
    $('dMovDetraccion_check').prop("checked", true);
    document.getElementById('dMovDetraccion').value = '0';
    $('dMovPercepcion_check').prop("checked", true);
    document.getElementById('dMovPercepcion').value = '0';
    $('dMovRetencion_check').prop("checked", true);
    document.getElementById('dMovRetencion').value = '0';
    document.getElementById('dMovImporte').value = '0';
    document.getElementById('dMovIGV').value = '0';
    document.getElementById('dMovTotal').value = '0';
    ocument.getElementById('dMovSaldo').value = '0';
    $("#sMovObservacion").val("");
    $('#tipo').prop('selectedIndex',0);
}
function LimpiarFrmProductos(){
    $('#ProdServ_Id').prop('selectedIndex',0);
    document.getElementById('sMovDetConCantidad').value = "0";
//    document.getElementById('dMovDetConFechaVencimiento').value = "";
    $('#Unidadmedida_Id').prop('selectedIndex',0);
    document.getElementById('nMovDetConPrecioUnitario').value = "0";
    document.getElementById('nMovDetConSubtotal').value = "0";
}
function LimpiarFrmProductosLotes(){
    $("#lotes").attr('checked', false);
    $('#ProdServ_Id').prop('selectedIndex',0);
    $('#Unidadmedida_Id').prop('selectedIndex',0);
    document.getElementById('sMovDetConCantidad').value = "0";
    document.getElementById('nMovDetConPrecioUnitario').value = "0";
    document.getElementById('nMovDetConSubtotal').value = "0";
}
function ValidarTipomovimiento() {
    var Tipomovimiento_Id = document.getElementById('Tipomovimiento_Id').value;
    if (Tipomovimiento_Id == "") {
        $("#Tipomovimiento_Id").addClass('error');
        $("#Tipomovimiento_Id").parent().children().first().addClass("error");
    } else {
        $("#Tipomovimiento_Id").removeClass('error');
        $("#Tipomovimiento_Id").parent().children().first().removeClass("error");
        $("#agregar_producto").modal('show');
    }
    $('html, body').animate({ scrollTop: '0px'}, 1000);
}
function GuardarMovimiento() {
    var Movimiento_Id = document.getElementById('Movimiento_Id').value;
    var dMovFecha = document.getElementById('dMovFecha').value;
    var Tipomovimiento_Id = document.getElementById('Tipomovimiento_Id').value;
    var nMovTipopago = document.getElementById('nMovTipopago').value;
    var Almacen_Id = document.getElementById('Almacen_Id').value;
    var nMovTipoOrigenDestino = document.getElementById('nMovTipoOrigenDestino').value;
    var nMovOrigenDestino_Id = document.getElementById('nMovOrigenDestino_Id').value;
    var nMovTipodestino = document.getElementById('nMovTipodestino').value;
    var Documento_Id = document.getElementById('Documento_Id').value;
    var sMovDocumento = document.getElementById('sMovDocumento').value;
    var sMovDocReferencia = document.getElementById('sMovDocReferencia').value;
    var Moneda_Id = document.getElementById('Moneda_Id').value;
    var nMovTipoCambio = document.getElementById('nMovTipoCambio').value;
    var dMovImporte = document.getElementById('dMovImporte').value;
    var dMovIGV = document.getElementById('dMovIGV').value;
    var dMovTotal = document.getElementById('dMovTotal').value;
    var dMovSaldo = document.getElementById('dMovSaldo').value;
    var dMovDetraccion = document.getElementById('dMovDetraccion').value;
    var dMovPercepcion = document.getElementById('dMovPercepcion').value;
    var dMovRetencion = document.getElementById('dMovRetencion').value;
    var sMovObservacion = document.getElementById('sMovObservacion').value;
//    var nMovEstado = document.getElementById('nMovEstado').value;
    $.post('?c=Movimiento&a=GuardarMovimiento', {
        Movimiento_Id: Movimiento_Id,
        dMovFecha: dMovFecha,
        Tipomovimiento_Id: Tipomovimiento_Id,
        nMovTipopago: nMovTipopago,
        Almacen_Id: Almacen_Id,
        nMovTipoOrigenDestino: nMovTipoOrigenDestino,
        nMovOrigenDestino_Id: nMovOrigenDestino_Id,
        nMovTipodestino: nMovTipodestino,
        Documento_Id: Documento_Id,
        sMovDocumento: sMovDocumento,
        sMovDocReferencia: sMovDocReferencia,
        Moneda_Id: Moneda_Id,
        nMovTipoCambio: nMovTipoCambio,
        dMovImporte: dMovImporte,
        dMovIGV: dMovIGV,
        dMovTotal: dMovTotal,
        dMovSaldo: dMovSaldo,
        dMovDetraccion: dMovDetraccion,
        dMovPercepcion: dMovPercepcion,
        dMovRetencion: dMovRetencion,
        sMovObservacion: sMovObservacion
//        nMovEstado: nMovEstado
    }, function (data) {
        $("#resultado").html(data);
        Limpiar();
        ListarProductos();
        CalcularImpTotIGV();
        $('html, body').animate({scrollTop: '0px'}, 1000);
    });
}
function AgregarProductoLote(){
    var ProdServ_Id = document.getElementById('ProdServ_Id').value;
    var lotes = $('input:radio[name=lotes]:checked').val();
    var Unidadmedida_Id = document.getElementById('Unidadmedida_Id').value;
    var sMovDetConCantidad = document.getElementById('sMovDetConCantidad').value;
    var nMovDetConPrecioUnitario = document.getElementById('nMovDetConPrecioUnitario').value;
    var nMovDetConSubtotal = document.getElementById('nMovDetConSubtotal').value;
    var nIncluyeLoteIGV = 0;
    if($("#nIncluyeLoteIGV").prop("checked") == true){
        nIncluyeLoteIGV = 1;
    }
     
    $.post('?c=Movimiento&a=AgregarProductoC', {
        ProdServ_Id: ProdServ_Id,
        lotes: lotes,
        Unidadmedida_Id: Unidadmedida_Id,
        sMovDetConCantidad: sMovDetConCantidad,
        nMovDetConPrecioUnitario: nMovDetConPrecioUnitario,
        nMovDetConSubtotal: nMovDetConSubtotal,
        nIncluyeIGV: nIncluyeLoteIGV
    }, function (data) {
        $("#resultado_saluda_venta").html(data);
        LimpiarFrmProductosLotes();
        ListarProductos();
        CalcularImpTotIGV();
    });
}
function AgregarProductoC(){
    var ProdServ_Id = document.getElementById('ProdServ_Id').value;
    var Unidadmedida_Id = document.getElementById('Unidadmedida_Id').value;
    var sMovDetConCantidad = document.getElementById('sMovDetConCantidad').value;
    var dMovDetConFechaVencimiento = document.getElementById('dMovDetConFechaVencimiento').value;
    var nMovDetConSubtotal = document.getElementById('nMovDetConSubtotal').value;
    var nMovDetConPrecioUnitario = document.getElementById('nMovDetConPrecioUnitario').value;
    var nIncluyeIGV = 0; 
    if($("#nIncluyeIGV").prop("checked") == true){
        nIncluyeIGV = 1;
    }
    $.post('?c=Movimiento&a=AgregarProductoC', {
        ProdServ_Id: ProdServ_Id,
        Unidadmedida_Id: Unidadmedida_Id,
        sMovDetConCantidad: sMovDetConCantidad,
        dMovDetConFechaVencimiento: dMovDetConFechaVencimiento,
        nMovDetConSubtotal: nMovDetConSubtotal,
        nMovDetConPrecioUnitario: nMovDetConPrecioUnitario,
        nIncluyeIGV: nIncluyeIGV
    }, function (data) {
        $("#resultado_ingresocompra").html(data); // Mostrar la respuestas del script PHP.
            LimpiarFrmProductos();
            ListarProductos();
            CalcularImpTotIGV();
    });
//    $.ajax({
//        type: "POST",
//        url: '?c=Movimiento&a=AgregarProductoC',
//        data: $("#FrmIngresoCompra").serialize(), // Adjuntar los campos del formulario enviado.
//        success: function (data)
//        {
//            $("#resultado_ingresocompra").html(data); // Mostrar la respuestas del script PHP.
//            LimpiarFrmProductos();
//            ListarProductos();
//            CalcularImpTotIGV();
//        }
//    });
}
function ProductosUNILista(){
    var ProdServ_Id = document.getElementById('ProdServ_Id').value;
    $.post('?c=Movimiento&a=ListarUnidades', {
        ProdServ_Id: ProdServ_Id
    }, function(data){
        $("#Unidadmedida_Id").html(data);
    });
}
function ListarProductos() {
    $.post('?c=Movimiento&a=ListarProductos', {
    }, function(data) {
        $("#productos_servicios").html(data);
    });
}
function ListarProductosLote(){
    var ProdServ_Id = document.getElementById('ProdServ_Id').value;
    $.post('?c=Movimiento&a=ListarLoteProducto',{
        ProdServ_Id: ProdServ_Id
    }, function(data){
       $("#lista_lotes").html(data); 
       ProductosUNILista();
    });
}
function TotalGravamen() {
    $.post('?c=Garantias&a=TotalGarantias', {
    }, function (data) {
        document.getElementById('').value = data;
    });
}
function CalcularImpTotIGV(){
    var Documento_Id = document.getElementById('Documento_Id').value;
    $.post('?c=Movimiento&a=CalcularImpTotIGV',{     
        Documento_Id: Documento_Id
    }, function(data){
        $("#resultado_productos").html(data);
    });
}
function CalcularImpTotIGV_venta(){
    var Documento_Id = document.getElementById('Documento_Id').value;
    $.post('?c=Movimiento&a=CalcularImpTotIGV_venta',{     
        Documento_Id: Documento_Id
    }, function(data){
        $("#resultado_productos").html(data);
    });
}
function CalcularSubtotal(){
    var sMovDetConCantidad = document.getElementById('sMovDetConCantidad').value;
    var nMovDetConPrecioUnitario = document.getElementById('nMovDetConPrecioUnitario').value;
    document.getElementById('nMovDetConSubtotal').value = sMovDetConCantidad * nMovDetConPrecioUnitario;
}
function Tipomovimiento() {
    var Tipomovimiento_Id = document.getElementById('Tipomovimiento_Id').value;
    $.post('?c=Movimiento&a=TipoMovimiento', {
        Tipomovimiento_Id: Tipomovimiento_Id
    }, function (data) {
        $("#FrmTipomovimiento").html(data);
        TipoDocumento(Tipomovimiento_Id);
        $("#Documento_Id").attr('disabled', false);
        CalcularImpTotIGV();
    });
}
function Tipomovimiento_venta() {
    var Tipomovimiento_Id = 2;
    $.post('?c=Movimiento&a=TipoMovimiento', {
        Tipomovimiento_Id: Tipomovimiento_Id
    }, function (data) {
        $("#FrmTipomovimiento").html(data);
        TipoDocumento_vetna(Tipomovimiento_Id);
        CalcularImpTotIGV_venta();
    });
}
function TipoDocumento(Tipomovimiento_Id) {
    $.post('?c=Tipodocumento&a=ListarConbobox', {
        Tipomovimiento_Id: Tipomovimiento_Id
    }, function (data) {
        $("#Documento_Id").html(data);
        NumeroDocumento();
    });
}
function TipoDocumento_vetna(Tipomovimiento_Id) {
    $.post('?c=Tipodocumento&a=ListarConbobox', {
        Tipomovimiento_Id: Tipomovimiento_Id
    }, function (data) {
        $("#Documento_Id").html(data);
        NumeroDocumento_Venta();
    });
}
function TipoDocumentoEditar(nMovOrigenDestino_Id) {
    var Tipomovimiento_Id = document.getElementById('Tipomovimiento_Id').value;
    $.post('?c=Tipodocumento&a=ListarConbobox', {
        Tipomovimiento_Id: Tipomovimiento_Id
    }, function (data) {
        $("#Documento_Id").html(data);
        $("#Documento_Id").attr('disabled', false);
        $("#nMovOrigenDestino_Id").val(nMovOrigenDestino_Id);
    });
}
function NumeroDocumento(){
    var Documento_Id = document.getElementById('Documento_Id').value;
    $.post('?c=Tipodocumento&a=NumeroDocumento', {
        Documento_Id: Documento_Id
    }, function (data) {
        document.getElementById("sMovDocumento").value = parseInt(data);
        AutomaticoSINO(Documento_Id);
        CalcularImpTotIGV();
    });
}
function NumeroDocumento_Venta(){
    var Documento_Id = document.getElementById('Documento_Id').value;
    $.post('?c=Tipodocumento&a=NumeroDocumento', {
        Documento_Id: Documento_Id
    }, function (data) {
        document.getElementById("sMovDocumento").value = parseInt(data);
        AutomaticoSINO(Documento_Id);
        CalcularImpTotIGV_venta();
    });
}
function AutomaticoSINO(Documento_Id){
    $.post('?c=Tipodocumento&a=AutomaticoSINO',{
        Documento_Id: Documento_Id
    },function(data){
//        console.log(data);
        $("#resultado_productos").html(data);
    });
}
function TipoPrvClien() {
    var tipo = document.getElementById('nMovTipoOrigenDestino').value;
    $.post('?c=Clienteproveedor&a=TipoPrvClien', {
        tipo: tipo
    }, function (data) {
        $("#nMovOrigenDestino_Id").html(data);
    });
}
function EliminarProductoLista(){
    var Codigo_Producto = document.getElementById('Codigo_Producto').value;
    $.post('?c=Movimiento&a=EliminarProductoLista',{
        Codigo_Producto: Codigo_Producto
    }, function(data){
        $("#resultado").html(data);
        ListarProductos();
        CalcularImpTotIGV();
        $("#eliminar_productos").modal("hide");
    });
}
jQuery.validator.addMethod("decimales", function (value, element) {
    return this.optional(element) || /^([0-9]+\.+[0-9]|[0-9])+$/.test(value);
});

$("#FrmMovimiento").validate({
    errorClass: 'error',
    rules: {        
        dMovFecha: {            
           required: true
        },
        Tipomovimiento_Id: {
           required: true
        },
        nMovTipopago: {
           required: true
        },
        nMovTipoOrigenDestino: {
           required: true
        },
        nMovOrigenDestino_Id: {
           required: true
        },
        nMovTipodestino: {
           required: true
        },
        Almacen_Id: {
           required: true
        },
        Documento_Id: {
           required: true
        },
        sMovDocumento: {
           required: true
        },     
        Moneda_Id: {
           required: true
        },        
        nMovTipoCambio: {
           decimales: true
        },        
        dMovDetraccion: {
           decimales: true
        },        
        dMovPercepcion: {
           decimales: true
        },        
        dMovRetencion: {
           decimales: true
        },          
        dMovImporte: {
           decimales: true
        },        
        dMovIGV: {
           decimales: true
        },        
        dMovTotal: {
           decimales: true
        }
    },
    messages: {        
        dMovFecha: "Por favor ingrese fecha.",
        Tipomovimiento_Id: "Seleccione un tipo de movimiento.",
        nMovTipopago: "Seleccione tipo de pago",
        nMovTipoOrigenDestino: "Seleccione tipo de destino.",
        nMovOrigenDestino_Id: "Seleccionar origen destino..",
        nMovTipodestino: "Seleccione tipo de destino.",
        Almacen_Id: "Seleccione almacen por favor.",
        Documento_Id: "Seleccione documento por favor.",
        sMovDocumento: "Seleccione provincia que nacio.",
        Moneda_Id: "Seleccione moneda por favor.",
        nMovTipoCambio: "El tipo de cambio solo es numero o decimal.",
        dMovDetraccion: "La detraccion solo es numero o decimal.",
        dMovPercepcion: "La detraccion solo es numero o decimal.",
        dMovRetencion: "La detraccion solo es numero o decimal.",
        dMovImporte: "La detraccion solo es numero o decimal.",
        dMovIGV: "La detraccion solo es numero o decimal.",
        dMovTotal: "La detraccion solo es numero o decimal."
    }
});

//FORMULARIO VENTAS
