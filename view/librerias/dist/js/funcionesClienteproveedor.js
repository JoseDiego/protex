
function FrmGuardarClienteproveedor(){
    $.post('?c=Clienteproveedor&a=FrmGuardarClienteproveedor',{        
    },function(data){
        $(".content-wrapper").html(data);
    });
}
function FrmEditarClienteproveedor(Provclien_Id){
    $.post('?c=Clienteproveedor&a=FrmEditarClienteproveedor',{   
        Provclien_Id: Provclien_Id
    },function(data){
        $(".content-wrapper").html(data);
    });
}
function mostrarClienteproveedor(Provclien_Id){
    $.post('?c=Clienteproveedor&a=MostrarClienteproveedor',{   
        Provclien_Id: Provclien_Id
    },function(data){
        //alert(data);
        $("#ver_datos").html(data);
    });
}
function limpiarFrm(){
    document.getElementById('Provclien_Id').value = '0';
    $('#nProcliTipo').prop('selectedIndex',0);
    document.getElementById('nProcliRUCDNI').value = '';
    document.getElementById('sProcliNombre').value = '';
    document.getElementById('sProcliNomComercial').value = '';
    document.getElementById('sProcliDireccion').value = '';
    document.getElementById('nProcliTelefono').value = '';
    document.getElementById('sProcliCorreo').value = '';
    document.getElementById('sProcliContacto').value = '';
    document.getElementById('nProcliDiasCredito').value = '0';
    $('#nProcliEstado').prop('selectedIndex',0);
}
function GuardarProvclien() {
    var Provclien_Id = document.getElementById('Provclien_Id').value;
    var nProcliTipo = document.getElementById('nProcliTipo').value;
    var nProcliRUCDNI = document.getElementById('nProcliRUCDNI').value;
    var sProcliNombre = document.getElementById('sProcliNombre').value;
    var sProcliNomComercial = document.getElementById('sProcliNomComercial').value;
    var sProcliDireccion = document.getElementById('sProcliDireccion').value;
    var nProcliTelefono = document.getElementById('nProcliTelefono').value;
    var sProcliCorreo = document.getElementById('sProcliCorreo').value;
    var sProcliContacto = document.getElementById('sProcliContacto').value;
    var nProcliDiasCredito = document.getElementById('nProcliDiasCredito').value;
    var nProcliEstado = document.getElementById('nProcliEstado').value;
    $.post('?c=Clienteproveedor&a=GuardarClienteproveedor', {
        Provclien_Id: Provclien_Id,
        nProcliTipo: nProcliTipo,
        nProcliRUCDNI: nProcliRUCDNI,
        sProcliNombre: sProcliNombre,
        sProcliNomComercial: sProcliNomComercial,
        sProcliDireccion: sProcliDireccion,
        nProcliTelefono: nProcliTelefono,
        sProcliCorreo: sProcliCorreo,
        sProcliContacto: sProcliContacto,
        nProcliDiasCredito: nProcliDiasCredito,
        nProcliEstado: nProcliEstado
    }, function (data) {        
        $("#resultado").html(data);
        limpiarFrm();
        $('html, body').animate({ scrollTop: '0px'}, 1000);
    });
}
$("#FrmMovimiento").validate({
    errorClass: 'error',
    rules: {        
        dMovFecha: {            
           required: true
        },
        Tipomovimiento_Id: {
           required: true
        },
        nMovTipopago: {
           required: true
        },
        nMovTipoOrigenDestino: {
           required: true
        },
        nMovOrigenDestino_Id: {
           required: true
        },
        nMovTipodestino: {
           required: true
        },
        Almacen_Id: {
           required: true
        },
        Documento_Id: {
           required: true
        },
        sMovDocumento: {
           required: true
        },     
        Moneda_Id: {
           required: true
        },        
        nMovTipoCambio: {
           decimales: true
        },        
        dMovDetraccion: {
           decimales: true
        },        
        dMovPercepcion: {
           decimales: true
        },        
        dMovRetencion: {
           decimales: true
        },          
        dMovImporte: {
           decimales: true
        },        
        dMovIGV: {
           decimales: true
        },        
        dMovTotal: {
           decimales: true
        },
        nProcliTipo: {
            required: true
        }
    },
    messages: {        
        dMovFecha: "Por favor ingrese fecha.",
        Tipomovimiento_Id: "Seleccione un tipo de movimiento.",
        nMovTipopago: "Seleccione tipo de pago",
        nMovTipoOrigenDestino: "Seleccione tipo de destino.",
        nMovOrigenDestino_Id: "Seleccionar origen destino..",
        nMovTipodestino: "Seleccione tipo de destino.",
        Almacen_Id: "Seleccione almacen por favor.",
        Documento_Id: "Seleccione documento por favor.",
        sMovDocumento: "Seleccione provincia que nacio.",
        Moneda_Id: "Seleccione moneda por favor.",
        nMovTipoCambio: "El tipo de cambio solo es numero o decimal.",
        dMovDetraccion: "La detraccion solo es numero o decimal.",
        dMovPercepcion: "La detraccion solo es numero o decimal.",
        dMovRetencion: "La detraccion solo es numero o decimal.",
        dMovImporte: "La detraccion solo es numero o decimal.",
        dMovIGV: "La detraccion solo es numero o decimal.",
        dMovTotal: "La detraccion solo es numero o decimal.",
        nProcliTipo: "El tipo de cliente es requerido"
    }
});