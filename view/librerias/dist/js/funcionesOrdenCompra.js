function LimpiarFrmProductos(){
    $('#ProdServ_Id').prop('selectedIndex',0);
    document.getElementById('sMovDetConCantidad').value = "0";
//    document.getElementById('dMovDetConFechaVencimiento').value = "";
    $('#Unidadmedida_Id').prop('selectedIndex',0);
    document.getElementById('nMovDetConPrecioUnitario').value = "0.00";
    document.getElementById('nMovDetConSubtotal').value = "0.00";
   
    
}

function FrmNuevaOrdenCompra(){
    $.post('?c=OrdenCompra&a=NuevaOrdenCompra',{        
    },function(data){
        $(".content-wrapper").html(data);
    });
}

function AgregarDetalle() {
    $('html, body').animate({ scrollTop: '0px'}, 1000);
}

function PrecioUnitario(){
    var ProdServ_Id = document.getElementById('ProdServ_Id').value;
    var Unidadmedida_Id = document.getElementById('Unidadmedida_Id').value;
    var Moneda_Id = document.getElementById('Moneda_Id').value;
 
        $.post('?c=Precioventa&a=PrecioProductoVenta', {
            ProdServ_Id: ProdServ_Id,
            Unidadmedida_Id: Unidadmedida_Id,
            Moneda_Id: Moneda_Id
	    }, function(data){
               $("#nMovDetConPrecioUnitario").html(data);
               CalcularSubtotal();
	    });
}

function CalcularSubtotal(){
    var sMovDetConCantidad = document.getElementById('sMovDetConCantidad').value;
    var nMovDetConPrecioUnitario = document.getElementById('nMovDetConPrecioUnitario').value;
    document.getElementById('nMovDetConSubtotal').value = sMovDetConCantidad * nMovDetConPrecioUnitario;
}

function CalcularSaldo(){
    var dPedImporteAd = document.getElementById('importeAd').value;
    var dMovTotal = document.getElementById('dMovTotal').value;
    var saldo = dMovTotal - dPedImporteAd;
    var nsaldo = saldo.toFixed(2);
    document.getElementById('dMovSaldo').value = nsaldo;
}
function EliminarProductoList(){
    var Codigo_Producto = document.getElementById('Codigo_Producto').value;
    $.post('?c=Ordencompra&a=EliminarProductoLista',{
        Codigo_Producto: Codigo_Producto
    }, function(data){
        $("#resultado").html(data);
        ListarProductos();
        CalcularImpTotIGV();
        $("#eliminar_productos").modal("hide");
    });
}

function mostrarOrdenCompra(Pedido_Id){
    $.post('?c=Ordencompra&a=MostrarOrdenCompra',{   
        Pedido_Id: Pedido_Id
    },function(data){
        //alert(data);
        $("#ver_datos").html(data);
    });
}

function ListarProductos() {
    $.post('?c=Ordencompra&a=ListarProductos', {
    }, function(data) {
        $("#productos_servicios").html(data);
    });
}

function FrmEditarOrdenCompra(Pedido_Id){
    $.post('?c=Ordencompra&a=FrmEditarOrdenCompra',{   
        Pedido_Id: Pedido_Id
    },function(data){
        $(".content-wrapper").html(data);
    });
}

function CalcularImpTotIGV(){
    var Documento_Id = 1;
    $.post('?c=Ordencompra&a=CalcularImpTotIGV',{     
        Documento_Id: Documento_Id
    }, function(data){
        $("#resultado_productos").html(data);
    });
}
function limpiar(){
    document.getElementById('nMovDetConPrecioUnitario').value = 0;
    document.getElementById('nMovDetConSubtotal').value = 0;
}
function LimpiarDatos(){
    document.getElementById('dMovFecha').placeholder = "Fecha";//FECHA
    document.getElementById('selectTipoPago').selectedIndex= "0";//tipo de pago
    document.getElementById('ListarCliente').selectedIndex = "0";
    document.getElementById('sMovDocumento').value = "0";//Nro ORDEN TRABAJO
    document.getElementById('Moneda_Id').selectedIndex = "0";//TIPO DE MONEDA
    document.getElementById('importeAd').value = "0.00";//IMPORTE ADELANTADO
}

function ListarFechas(){
    var fInicio = document.getElementById('fechaInicio').value;
    var fFin = document.getElementById('fechaFin').value;
    $.post('?c=OrdenCompra&a=ListarFechasO', {
        fInicio: fInicio,
        fFin: fFin
        }, function (data) {
        $("#bodyt").html(data);
        $('html, body').animate({scrollTop: '0px'}, 1000);
    });
}
function GuardarOrdencompra(){
    var Movimiento_Id = document.getElementById('Movimiento_Id').value;
    var OrdenCompra_Id = document.getElementById('OrdenCompra_Id').value;
    var dMovFecha = document.getElementById('dMovFecha').value;//FECHA
    var nMovTipopago = document.getElementById('selectTipoPago').value;//tipo de pago
    var nMovOrigenDestino_Id = document.getElementById('ListarCliente').value;//CLIENTE
    var sMovDocumento = document.getElementById('sMovDocumento').value;//Nro ORDEN TRABAJO
    var Moneda_Id = document.getElementById('Moneda_Id').value;//TIPO DE MONEDA
    var dPedImporteAd = document.getElementById('importeAd').value;//IMPORTE ADELANTADO
    var dMovImporte = document.getElementById('dMovImporte').value;
    var dMovIGV = document.getElementById('dMovIGV').value;
    var dMovTotal = document.getElementById('dMovTotal').value;
    var dMovSaldo = document.getElementById('dMovSaldo').value;
    var sMovObservacion = document.getElementById('sMovObservacion').value;
    //var UnidadMedida_Id = document.getElementById('Unidadmedida_Id').value;
//    var nMovEstado = document.getElementById('nMovEstado').value;
    $.post('?c=OrdenCompra&a=GuardarMovimiento', {
        Movimiento_Id: Movimiento_Id,
        OrdenCompra_Id:OrdenCompra_Id,
        dMovFecha: dMovFecha,
        nMovTipopago: nMovTipopago,
        nMovOrigenDestino_Id: nMovOrigenDestino_Id,
        sMovDocumento: sMovDocumento,
        Moneda_Id: Moneda_Id,
        dPedImporteAd: dPedImporteAd,
        dMovImporte: dMovImporte,
        dMovIGV: dMovIGV,
        dMovTotal: dMovTotal,
        dMovSaldo: dMovSaldo,
        sMovObservacion: sMovObservacion,
        //UnidadMedida_Id: UnidadMedida_Id
//        nMovEstado: nMovEstado
    }, function (data) {
        $("#resultado").html(data);
        ListarProductos();
        CalcularImpTotIGV();
        LimpiarDatos();
        $('html, body').animate({scrollTop: '0px'}, 1000);
    });
}