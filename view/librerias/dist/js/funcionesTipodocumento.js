function limpiarFrm(){
    document.getElementById('Documento_Id').value = '0';
    $('#Almacen_Id').prop('selectedIndex',0);
    $('#Tipomovimiento_Id').prop('selectedIndex',0);
    document.getElementById('sDocNombre').value = '';
    document.getElementById('sDocNombreCorto').value = '';
    $('#nDocNomAutomatico').prop('selectedIndex',0);
    document.getElementById('sDocSiguiente').value = '';
    $("#nDocAfectoIGV").prop("checked", false);
    $('#nDocEstado').prop('selectedIndex',0);
}
function GuardarDocumento(){
    var Documento_Id = document.getElementById('Documento_Id').value;
    var Almacen_Id = document.getElementById('Almacen_Id').value;
    var Tipomovimiento_Id = document.getElementById('Tipomovimiento_Id').value;
    var sDocNombre = document.getElementById('sDocNombre').value;
    var sDocNombreCorto = document.getElementById('sDocNombreCorto').value;
    var nDocNomAutomatico = document.getElementById('nDocNomAutomatico').value;
    var sDocSiguiente = document.getElementById('sDocSiguiente').value;
    var nDocAfectoIGV = '0';
    if($('#nDocAfectoIGV').is(':checked')){
        nDocAfectoIGV = '1';
    }
    var nDocEstado = document.getElementById('nDocEstado').value;
    $.post('?c=Tipodocumento&a=GuardarDocumento', {
        Documento_Id: Documento_Id,
        Almacen_Id: Almacen_Id,
        Tipomovimiento_Id: Tipomovimiento_Id,
        sDocNombre: sDocNombre,
        sDocNombreCorto: sDocNombreCorto,
        nDocNomAutomatico: nDocNomAutomatico,
        sDocSiguiente: sDocSiguiente,
        nDocAfectoIGV: nDocAfectoIGV,
        nDocEstado: nDocEstado
    }, function (data) {
        $('#resultado').html(data);
        limpiarFrm();
        $('html, body').animate({scrollTop: '0px'}, 1000);
    });
}
function FrmGuardarTipodocumento(){
    $.post('?c=Tipodocumento&a=FrmGuardarTipodocumento',{        
    },function(data){
        $(".content-wrapper").html(data);
    });
}
function FrmEditarTipodocumento(Documento_Id){
    $.post('?c=Tipodocumento&a=FrmEditarTipodocumento',{   
        Documento_Id: Documento_Id
    },function(data){
        $(".content-wrapper").html(data);
    });
}
function mostrarTipodocumento(Documento_Id){
    $.post('?c=Tipodocumento&a=MostrarTipodocumento',{   
        Documento_Id: Documento_Id
    },function(data){
        $("#ver_datos").html(data);
    });
}