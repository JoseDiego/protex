
function FrmGuardarOperario(){
    $.post('?c=Operario&a=FrmGuardarOperario',{
    },function(data){
        $(".content-wrapper").html(data);
    });
}
function FrmEditarOperario(Operario_Id){
    $.post('?c=Operario&a=FrmEditarOperario',{
        Operario_Id: Operario_Id
    },function(data){
        $(".content-wrapper").html(data);
    });
}
function mostrarOperario(Operario_Id){
    $.post('?c=Operario&a=MostrarOperario',{
        Operario_Id: Operario_Id
    },function(data){
        $("#ver_datos").html(data);
    });
}
function limpiarFrm(){
    document.getElementById('Operario_Id').value = '';
    document.getElementById('sOpeDni').value = '';
    document.getElementById('sOpeApeMat').value = '';
    document.getElementById('sOpeApePat').value = '';
    document.getElementById('sOpeNombres').value = '';
    document.getElementById('sOpeCargo').value = '';
    document.getElementById('sOpeDireccion').value = '';
    document.getElementById('sOpeTelefono').value = '';
    document.getElementById('sOpeCorreo').value = '';
    $('#nOpeEstado').prop('selectedIndex',0);
}
function GuardarOperario() {
    var Operario_Id = document.getElementById('Operario_Id').value;
    var sOpeDni = document.getElementById('sOpeDni').value;
    var sOpeApeMat = document.getElementById('sOpeApeMat').value;
    var sOpeApePat = document.getElementById('sOpeApePat').value;
    var sOpeNombres = document.getElementById('sOpeNombres').value;
    var sOpeCargo = document.getElementById('sOpeCargo').value;
    var sOpeDireccion = document.getElementById('sOpeDireccion').value;
    var sOpeTelefono = document.getElementById('sOpeTelefono').value;
    var sOpeCorreo = document.getElementById('sOpeCorreo').value;
    var nOpeEstado = document.getElementById('nOpeEstado').value;

    $.post('?c=Operario&a=GuardarOperario', {
        Operario_Id: Operario_Id,
        sOpeDni: sOpeDni,
        sOpeApePat: sOpeApePat,
        sOpeApeMat: sOpeApeMat,
        sOpeNombres: sOpeNombres,
        sOpeCargo: sOpeCargo,
        sOpeDireccion: sOpeDireccion,
        sOpeTelefono: sOpeTelefono,
        sOpeCorreo: sOpeCorreo,
        nOpeEstado: nOpeEstado
    }, function (data) {
        $("#resultado").html(data);
        limpiarFrm();
        $('html, body').animate({ scrollTop: '0px'}, 1000);
    });
}
//
// $("#FrmOperario").validate({
//     errorClass: 'error',
//     rules: {
//         dMovFecha: {
//            required: true
//         },
//         Tipomovimiento_Id: {
//            required: true
//         },
//         nMovTipopago: {
//            required: true
//         },
//         nMovTipoOrigenDestino: {
//            required: true
//         },
//         nMovOrigenDestino_Id: {
//            required: true
//         },
//         nMovTipodestino: {
//            required: true
//         },
//         Almacen_Id: {
//            required: true
//         },
//         Documento_Id: {
//            required: true
//         },
//         sMovDocumento: {
//            required: true
//         },
//         Moneda_Id: {
//            required: true
//         },
//         nMovTipoCambio: {
//            decimales: true
//         },
//         dMovDetraccion: {
//            decimales: true
//         },
//         dMovPercepcion: {
//            decimales: true
//         },
//         dMovRetencion: {
//            decimales: true
//         },
//         dMovImporte: {
//            decimales: true
//         },
//         dMovIGV: {
//            decimales: true
//         },
//         dMovTotal: {
//            decimales: true
//         },
//         nProcliTipo: {
//             required: true
//         }
//     },
//     messages: {
//         dMovFecha: "Por favor ingrese fecha.",
//         Tipomovimiento_Id: "Seleccione un tipo de movimiento.",
//         nMovTipopago: "Seleccione tipo de pago",
//         nMovTipoOrigenDestino: "Seleccione tipo de destino.",
//         nMovOrigenDestino_Id: "Seleccionar origen destino..",
//         nMovTipodestino: "Seleccione tipo de destino.",
//         Almacen_Id: "Seleccione almacen por favor.",
//         Documento_Id: "Seleccione documento por favor.",
//         sMovDocumento: "Seleccione provincia que nacio.",
//         Moneda_Id: "Seleccione moneda por favor.",
//         nMovTipoCambio: "El tipo de cambio solo es numero o decimal.",
//         dMovDetraccion: "La detraccion solo es numero o decimal.",
//         dMovPercepcion: "La detraccion solo es numero o decimal.",
//         dMovRetencion: "La detraccion solo es numero o decimal.",
//         dMovImporte: "La detraccion solo es numero o decimal.",
//         dMovIGV: "La detraccion solo es numero o decimal.",
//         dMovTotal: "La detraccion solo es numero o decimal.",
//         nProcliTipo: "El tipo de cliente es requerido"
//     }
// });
