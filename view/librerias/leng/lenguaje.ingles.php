<?php
$lenguaje = array(
    'Cerrar sesion'=>'Close session',
    'en linea'=>'Online',
    'MENUS' => 'MENUS',
    'Inicio' => 'Home',   
    'Registrar Viaje' => 'Register Travel',
    'Confirmar Viaje' => 'Travel Check',
    'Descripcion'=>'description',
    'Fecha Viaje'=>'Outward journey date',
    'Fecha Viaje de Retorno'=>'Date return journey',
    '¿Desea confirmar este viaje?'=>'Do you want to confirm this trip ?',
    'Imprimir Constancia de Viaje'=>'Print Constancia Travel',
    'Cerrar'=>'Close',
    'Si, Imprimir'=>'Yes, print',
    'No, Cancelar'=>'No, cancel',
    'Eliminar'=>'Delete',
    '¿Esta seguro que desea eliminar el viaje?'=>'Are you sure you want to delete the trip?',
    'Se sugiere eliminar tambien el viaje asociado (Ida-retorno). Si tuviese algun inconveniente comunicarse con Programación de Convoy.'=>'It also suggests eliminating & eacute; n the associated trip ( Ida- return). If I had alg STILL drawback contact Convoy programming.',
    'Imprimir'=>'Print',    
    'Constancia de Viaje'=>'Travel constancia.',
    'Si, eliminar'=>'Yes, delete',
    'Si, confirmar'=>'If confirmed',
    'No, retroceder'=>'No, Go Back',    
    'Validacion'=>'Validation',
    'La fecha de viaje no puede ser igual que la fecha de retorno porfavor verificar fechas'=>'The date of travel can not be the same as the return date please verify dates',
    'La fecha de viaje es mayor a la fecha de retorno porfavor verificar fechas'=>'The date of travel is greater than the return date please verify dates',
//    'Registrar Convoy' => 'Register Convoy',
//    'Finalizar Viaje' => 'Finish Travel',
//    'Pasajero' => 'Passenger',
//    'Conductor' => 'Drive',
//    'Vehículo' => 'Vehicle',
//    'Usuarios' => 'Users',
//    'CATALOGOS' => 'CATALOGUE',
//    'Reporte' => 'Report',
//    'CONFIGURACION' => 'CONFIGURATION',
//    'Parametros' => 'Parameter',
//    'Cerrar Sesion' => 'Logout',
//    'Todos los derechos reservados' => 'All rights reserved',
//    'Ingresa tus datos para entrar al sistema' => 'Enter your information to login',
//    'Usuario' => 'User',
//    'Contrasena' => 'Password',
//    'Olvidé mi contrasena' => 'I forgot my password',
//    'Ingresar' => 'Sign in',
    
    
    
    'días'=>'days',
    'Usted no puede estar mas de'=>'You may not be over',
    'El tiempo de registro para la fecha actual termino.'=>'The recording time for the current term date.',
    'El origen no puede ser el mismo que su destino porfavor elija un destino correcto.'=>'The source may not be the same as your destination please choose a proper destination.',
    'Elegir ruta y fechas de viajes'=>'Choose route and travel dates',
    'Ruta'=>'Route',    
    'Registrar nuevo viaje'=>'Register new journey',
    'Confirmacion'=>'Confirmation',
    'Viajes Registrados'=>'Registered travel',
    'Convoy Nombres' => 'Name Convoy',
    'Origen'=>'Source',
    'Fecha de Sierra'=>'Outward journey date',    
    'Fecha' => 'Date',
    'viaje de Ida'=>'Round trip',
    'Fecha de Bravo'=>'Return journey date',
    'viaje de Retorno'=>'return trip',
    'cantidad de dias'=>'Number of Days',
    'Destino'=>'Destination',
    'Estado de Convoy'=>'State Convoy',
    'Operaciones'=>'operations'
    
);
/*MENUS=MENU
Inicio=Home
Registrar Viaje=Register Travel
Registrar Convoy=Register Convoy
 *
Finalizar Viaje=Finish Travel
Pasajero=Passenger
Conductor=Drive
Vehículo=Vehicle
Usuarios=Users
CATÁLOGOS=CATALOGUE
Reporte=Report
CONFIGURACIÓN=CONFIGURATION
Parámetros=Parameter
Cerrar Sesión=Logout
Todos los derechos reservados=All rights reserved
Ingresa tus datos para entrar al sistema=Enter your information to login
Usuario=User
Contraseña=Password
Olvidé mi contraseña=I forgot my password
Ingresar=sign in
*/
?>
