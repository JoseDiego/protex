        </div>
        <footer class="main-footer">
            <div class="pull-right hidden-xs">
              <b>Version</b> 1.161104
            </div>
            <strong>Copyright &copy; 2016 <a href="http://www.felipemarroquin.com" target="_blank">Felipe Marroquin S.A.C.</a>.</strong> Todos los derechos reservados.
        </footer>
        <!-- Bootstrap 3.3.2 JS -->
        <script src="view/librerias/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>    
        <!-- Morris.js charts -->
        <script src="view/librerias/dist/js/raphael-min.js"></script>
        <script src="view/librerias/plugins/morris/morris.min.js" type="text/javascript"></script>
        <!-- Sparkline -->
        <script src="view/librerias/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
        <!-- jvectormap -->
        <script src="view/librerias/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
        <script src="view/librerias/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
        <!-- jQuery Knob Chart -->
        <script src="view/librerias/plugins/knob/jquery.knob.js" type="text/javascript"></script>
        
        <!-- Bootstrap WYSIHTML5 -->
        <script src="view/librerias/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
        
        <script src="view/librerias/plugins/input-mask/jquery.inputmask.js" type="text/javascript"></script>
        <script src="view/librerias/plugins/input-mask/jquery.inputmask.date.extensions.js" type="text/javascript"></script>
        <script src="view/librerias/plugins/input-mask/jquery.inputmask.extensions.js" type="text/javascript"></script>
        
        <!-- DATA TABES SCRIPT -->
        <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" language="javascript" src="//cdn.datatables.net/responsive/1.0.2/js/dataTables.responsive.js"></script>
        <script type="text/javascript" language="javascript" src="//cdn.datatables.net/plug-ins/a5734b29083/integration/bootstrap/3/dataTables.bootstrap.js"></script>
        
        
        <!-- Slimscroll -->
        <script src="view/librerias/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <!-- FastClick -->
        <script src='view/librerias/plugins/fastclick/fastclick.min.js'></script>
        <!-- AdminLTE App -->
        <script src="view/librerias/dist/js/app.min.js" type="text/javascript"></script>    

        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="view/librerias/dist/js/pages/dashboard.js" type="text/javascript"></script>    

        <!-- AdminLTE for demo purposes -->
        <script src="view/librerias/dist/js/demo.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(window).load(function() {
                $('#preloader').fadeIn('slow');
                $('#preloader').fadeOut('slow');
                $('body').css({'overflow':'visible'});
            });
        </script>
        <script type="text/javascript" src="view/librerias/dist/js/jquery.timeTo.js"></script>
        <?php 
        foreach ($parametro as $r){ 
            if($r->__GET('Parametro_Id')==1){
                $tiempo = $r->__GET('sParValor')*60;
            }
        }
        ?>    
        <script type="text/javascript">

            function killerSession(){
                window.location="?c=Principal&a=CerrarSession";;
            }
            $('body').click(function(){
                $('#countdown').timeTo({
                    displayHours: false,
                    seconds: <?php echo $tiempo ?>, 
                    callback: function(){ killerSession(); },
                    fontSize: 10
                });
            });
            $('#countdown').timeTo({
                displayHours: false,
                seconds: <?php echo $tiempo ?>,
                callback: function(){ killerSession(); },
                fontSize: 10
            });
        </script>
    </body>
</html>