<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Error 404</title>
        <link type="text/css" rel="stylesheet" href="view/librerias/bootstrap/css/bootstrap.min.css">
        <style type="text/css">
            body{
                color: #e7e7e7;
            }
            #titulo404{
                font-size: 10em;
                color: #616161;
            }
            #contenido h4{
                color: #616161;
            }
            #contenido p{
                color: #8B8B8B;
            }
        </style>
    </head>
    <body>
        <div class="container-fluid">
            <div class="container text-center">
                <span id="titulo404">
                    404
                </span>
                <div class="row">
                    <div class="col-md-offset-2 col-md-8 col-md-offset-2" id="contenido">
                        <h4>Archivo no encontrado</h4>
                        <p>
                            
                            El sitio configurado en esta dirección no contiene el archivo solicitado.
                            Si este es su sitio, asegúrese de que el caso de nombre de archivo coincide con la URL. 
                        </p>
                        <a href="?c=Principal&a=wfalogin" style="text-decoration: none;">Regresar a Portal web</a>
                    </div>
                </div>
            </div>
        </div>
        <script src="view/librerias/plugins/jQuery/jquery-1.11.0.min.js"></script>
        <script src="view/librerias/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>
