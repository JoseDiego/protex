
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Venta Detallada
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="wfamain.php"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#">Venta Detallada</a></li>
    </ol>
</section>        
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <div class="row">
                        <div class="col-md-6 text-left">
                            <h3 class="box-title">Relación completa de Venta Detallada</h3>
                        </div>
                        <div class="col-md-6 text-right">
                            <!--<button class="btn btn-danger text-bold">PDF&nbsp;&nbsp;<i class="fa fa-file-pdf-o"></i></button>-->
                            <button class="btn btn-success text-bold" id="Exportar_Excel">EXCEL&nbsp;&nbsp;<i class="fa fa-file-excel-o"></i></button>
                        </div>                        
                        <div class="col-md-12">
                            <form action="?c=Reportes&a=VentaDetallada" method="POST">
                                <div class="row">
                                    <div class="col-md-2 form-group">
                                        <label>Fecha de inicio</label>
                                        <input type="text" class="form-control" name="fechainicio" id="fechainicio" placeholder="dd-mm-yyyy" value="<?php echo isset($_POST['fechainicio']) ? $_POST['fechainicio'] : date('d-m-Y', strtotime('-30 day' ,strtotime(date('d-m-Y')))); ?>">
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <label>Fecha de inicio</label>
                                        <input type="text" class="form-control" name="fechafin" id="fechafin" placeholder="dd-mm-yyyy" value="<?php echo isset($_POST['fechafin']) ? $_POST['fechafin'] : date('d-m-Y', strtotime('+30 day' ,strtotime(date('d-m-Y')))); ?>">
                                    </div>
                                    <div class="col-md-1 form-group">
                                        <label style="width: 100%;">&nbsp;</label>
                                        <button type="submit" class="btn btn-danger" style="width: 100%;">Buscar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="table">
                        <table id="example1" class="table table-bordered table-striped text-center">
                            <thead>
                                <tr>
                                    <th>FECHA</th>
                                    <th>ALMACEN</th>
                                    <th>DOCUMENTO</th>
                                    <th>CONDICIÓN</th>
                                    <th>CLIENTE</th>
                                    <th>PRODUCTO</th>
                                    <th>CANTIDAD</th>
                                    <th>UNIDAD</th>
                                    <th>PRECIO COSTO UNITARIO SIN IGV</th>
                                    <th>PRECIO VENTA UNITARIO SIN IGV</th>
                                    <th>MONEDA</th>
                                    <th>IMPORTE VENTA</th>
                                    <th>UTILIDAD</th>
                                    <th>PORCENTAJE RENTABILIDAD</th>
                                </tr>
                            </thead>                  	
                            <tbody>  
                                <?php foreach ($reportes as $r): ?>                    
                                    <tr> 
                                        <td><?php echo date('d-m-Y', strtotime($r->__GET('valor01'))); ?></td>
                                        <td><?php echo $r->__GET('valor02'); ?></td>
                                        <td><?php echo $r->__GET('valor03'); ?></td>
                                        <td><?php echo $r->__GET('valor04'); ?></td>
                                        <td><?php echo $r->__GET('valor05'); ?></td>
                                        <td><?php echo $r->__GET('valor06'); ?></td>
                                        <td><?php echo $r->__GET('valor07'); ?></td>
                                        <td><?php echo $r->__GET('valor08'); ?></td>
                                        <td><?php echo $r->__GET('valor09'); ?></td>
                                        <td><?php echo number_format($r->__GET('valor10'), 2, ".", "."); ?></td>
                                        <td><?php echo $r->__GET('valor11'); ?></td>
                                        <td><?php echo $r->__GET('valor13'); ?></td>
                                        <td><?php echo number_format($r->__GET('valor14'), 2, ".", "."); ?></td>
                                        <td><?php echo $r->__GET('valor15'); ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>FECHA</th>
                                    <th>ALMACEN</th>
                                    <th>DOCUMENTO</th>
                                    <th>CONDICIÓN</th>
                                    <th>CLIENTE</th>
                                    <th>PRODUCTO</th>
                                    <th>CANTIDAD</th>
                                    <th>UNIDAD</th>
                                    <th>PRECIO COSTO UNITARIO SIN IGV</th>
                                    <th>PRECIO VENTA UNITARIO SIN IGV</th>
                                    <th>MONEDA</th>
                                    <th>IMPORTE VENTA</th>
                                    <th>UTILIDAD</th>
                                    <th>PORCENTAJE RENTABILIDAD</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                    <!-- Modal modeal-ver -->
                    <div class="modal fade" id="modal-ver" tabindex="-1" role="dialog" aria-labelledby="modal-ver-label" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <form action="?c=Usuario&a=reporteUsuarioDatos" method="POST">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                        <h4 class="modal-title" id="modal-ver-label">Registro de Usuario</h4>
                                    </div>
                                    <div class="modal-body">
                                        <table class="table table-striped" id="ver_datos">

                                        </table>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-danger" formtarget="_blak"><i class="fa fa-print"></i>&nbsp;Imprimir</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /Modal modal-ver -->

                    <!-- Modal modal-eliminar -->
                    <div class="modal fade" id="modal-eliminar" tabindex="-1" role="dialog" aria-labelledby="modal-eliminar-label" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <form action="?c=Productos&a=EliminarProductos" method="POST">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                        <h4 class="modal-title" id="modal-eliminar-label">Eliminar</h4>
                                    </div>
                                    <div class="modal-body">
                                        <input type="hidden" name="idProdServ" id="idProdServ" value="">
                                        ¿Estás seguro que desea eliminar?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">No, cancelar</button>
                                        <button type="submit" class="btn btn-danger btn-flat">Si, eliminar</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /Modal modal-eliminar -->
                    
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<script type="text/javascript" src="view/librerias/dist/js/funcionesVentaDetallada.js"></script>
