
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Stock
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="wfamain.php"><i class="fa fa-dashboard"></i> Inicio</a></li>
                <li><a href="#">Stock</a></li>
            </ol>
        </section>        
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            <div class="row">
                                <div class="col-md-6 text-left">
                                    <h3 class="box-title">Relación completa de Stock</h3>
                                </div>
                                <div class="col-md-6 text-right">
                                    <button class="btn btn-success text-bold" id="Exportar_Excel">EXCEL&nbsp;&nbsp;<i class="fa fa-file-excel-o"></i></button>
                                </div>
                            </div>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped text-center">
                                <thead>
                                    <tr>
                                        <th>Codigo</th>
                                        <th>Familia</th>
                                        <th>Producto/Servicio</th>
                                        <th>Stock</th>
                                        <th>Unidad</th>
                                        <th>Precio</th>
                                        <th>Valorizado</th>
                                    </tr>
                                </thead>                  	
                                <tbody>  
                                    <?php foreach ($reportes as $r): ?>                    
                                        <tr> 
                                            <td><?php echo $r->__GET('valor01'); ?></td>
                                            <td><?php echo $r->__GET('valor02'); ?></td>
                                            <td><?php echo $r->__GET('valor03'); ?></td>
                                            <td><?php echo $r->__GET('valor04'); ?></td>
                                            <td><?php echo $r->__GET('valor05');?></td>
                                            <td><?php echo $r->__GET('valor06');?></td>
                                            <td><?php echo $r->__GET('valor07');?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Codigo</th>
                                        <th>Familia</th>
                                        <th>Producto/Servicio</th>
                                        <th>Stock</th>
                                        <th>Unidad</th>
                                        <th>Precio</th>
                                        <th>Valorizado</th>
                                    </tr>
                                </tfoot>
                            </table>

                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </section><!-- /.content -->
        <script type="text/javascript" src="view/librerias/dist/js/funcionesStock.js"></script>

