<link rel="stylesheet" href="view/librerias/dist/css/print.css">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="view/librerias/plugins/iCheck/all.css">
<!-- Content Header (Page header) -->
<!--        <section class="content-header">
    <h1>
        Ventas
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="wfamain.php"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#">Ventas</a></li>
    </ol>
</section>        -->
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body padding" style="padding-top: 3.5%; padding-bottom: 3.5%;">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h3>COMPROBANTE DE VENTA</h3>
                            <br>
                            <div id="baucher-content">
                                <?php 
                                foreach ($pedido as $p){
                                ?>
                                <div class="baucher text-center" id="boucher-print" style="font-size: 13px;">                       
                                    CLUB PACASMAYO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo date('d/m/Y H:i A'); ?><br>
                                    <?php echo $this->ObtenerNombreDocumento($p->__GET('Documento_Id')).': '.$this->ObtenerNumeroDocumento($_GET['Movimiento']); ?><br>
                                    <?php echo $this->NombreClienteProveedor($p->__GET('OrigenDestino_Id')); ?><br>
                                    --------------------------------------<br>
                                    <?php 
                                    $total = 0;
                                    foreach ($this->pedido_detalle_model->Buscar($p->__GET('Pedido_Id')) as $ped_det):
                                        $total = $total + ($ped_det->__GET('nPedDetCantidad')*$ped_det->__GET('dPedDetPrecioUnitario'));
                                    ?>
                                    <div class="producto">
                                        <p class="baucher-producto">
                                            <?php echo str_pad($ped_det->__GET('nPedDetCantidad'), 2, '0', STR_PAD_LEFT).' '.$this->NombreProdServ($ped_det->__GET('ProdServ_Id')); ?>
                                        </p>
                                        <p class="baucher-precio">
                                            S/. <?php echo $ped_det->__GET('dPedDetPrecioUnitario'); ?>
                                        </p>
                                    </div>
                                    <?php 
                                    endforeach;
                                    ?>
    <!--                                1 SINCOL GOTAS /15 ML&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;14.00<br>
                                    FCO/15 ML ROEMMERS LABORATORIOS<br>
                                    1 GRAVAMIN JBE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;24.00<br>
                                    FCO/60 ML medifarma<br>-->
                                    --------------------------------------<br>
                                    <div class="total">
                                        <p class="total-etiqueta">TOTAL:</p>
                                        <p class="total-precio">S/. <?php echo number_format($total,2,",","."); ?></p>
                                    </div>                                    
                                    <div class="pie-pagina">
                                        Vendedor: <?php echo $_SESSION['usu_nombre']; ?><br>
                                        * CANJEAR POR BOLETA O FACTURA *<br>
                                        No se Aceptan Cambios o Devoluciones<br>
                                    </div>
                                </div>
                                <?php 
                                }
                                ?>
                            </div>
                            <div class="bottones">
                                <div class="row">
                                    <div class="col-md-6">
                                        <!-- onclick="Print()" -->
                                        <a href="?c=Principal&a=ImprimirTicket&Movimiento=<?php echo $_GET['Movimiento']; ?>" target="_blank" class="btn btn-danger btn-flat" style="width: 100%;" type="button"><i class="fa fa-print"></i>&nbsp;Imprimir</a>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <a href="?c=Ventas&a=Index" class="btn btn-default btn-flat" style="width: 100%;"><i class="fa fa-long-arrow-left"></i>&nbsp;Regresar</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.box -->

        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<!-- iCheck 1.0.1 -->
<script src="view/librerias/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
<script src="view/librerias/dist/js/funcionesGenerales.js"></script>
<script src="view/librerias/dist/js/funcionesMovimiento.js"></script>
<script src="view/librerias/dist/js/funcionesVentas.js"></script>
<script type="text/javascript">
    function Print() {
        var contents = document.getElementById("boucher-print").innerHTML;
        var frame1 = document.createElement('iframe');
        frame1.name = "frame1";
        frame1.style.position = "absolute";
        frame1.style.top = "-1000000px";
        document.body.appendChild(frame1);
        var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
        frameDoc.document.open();
        frameDoc.document.write('<html>\n\
                                    <head>\n\
                                        <title></title>\n\
                                        <link rel="stylesheet" href="view/librerias/dist/css/print.css">\n\
                                        <style>\n\
                                            body{font-family: "Courier New";font-size: 12px;}\n\
                                            .producto{width: 265px;}.pie-pagina{width: 265px;}\n\
                                            .baucher-producto{width: 190px;}.baucher-precio{width: 75px;}\n\
                                            .total-etiqueta{width: 190px;}.total-precio{width: 75px;}\n\
                                            @page{\n\
                                                margin: 0;\n\
                                            }\n\
                                        </style>\n\
                                ');
        frameDoc.document.write('</head>\n\
                                 <body>\n\
                                    <br><br>\n\
                                ');
        frameDoc.document.write(contents);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            document.body.removeChild(frame1);
        }, 500);
        return false;
    }
</script>