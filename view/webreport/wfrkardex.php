
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Kardex
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="wfamain.php"><i class="fa fa-dashboard"></i> Inicio</a></li>
                <li><a href="#">Kardex</a></li>
            </ol>
        </section>        
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            <div class="row">
                                <div class="col-md-6 text-left">
                                    <h3 class="box-title">Relación completa de Kardex</h3>
                                </div>
                                <div class="col-md-6 text-right">
                                    <button class="btn btn-success text-bold" id="Exportar_Excel">EXCEL&nbsp;&nbsp;<i class="fa fa-file-excel-o"></i></button>
                                </div>                                
                                <div class="col-md-12">
                                    <form action="?c=Reportes&a=Kardex" method="POST">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label>Producto</label>
                                                <select class="form-control" id="ProdServ_Id" name="ProdServ_Id">
                                                    <option value="">Seleccionar Producto</option>
                                                    <?php
                                                    foreach ($productos as $pro):
                                                        ?>
                                                    <option value="<?php echo $pro->__GET('ProdServ_Id'); ?>" <?php echo isset($_POST['ProdServ_Id']) ? $_POST['ProdServ_Id'] == $pro->__GET('ProdServ_Id') ? 'selected':'' : ''?>><?php echo $pro->__GET('sProSrvNombre'); ?></option>
                                                        <?php
                                                    endforeach;
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-2 form-group">
                                                <label>Fecha de inicio</label>
                                                <input type="text" class="form-control" name="fechainicio" id="fechainicio" placeholder="dd-mm-yyyy" value="<?php echo isset($_POST['fechainicio']) ? $_POST['fechainicio'] : date('d-m-Y', strtotime('-30 day' ,strtotime(date('d-m-Y')))); ?>">
                                            </div>
                                            <div class="col-md-2 form-group">
                                                <label>Fecha de inicio</label>
                                                <input type="text" class="form-control" name="fechafin" id="fechafin" placeholder="dd-mm-yyyy" value="<?php echo isset($_POST['fechafin']) ? $_POST['fechafin'] : date('d-m-Y', strtotime('+30 day' ,strtotime(date('d-m-Y')))); ?>">
                                            </div>
                                            <div class="col-md-1 form-group">
                                                <label style="width: 100%;">&nbsp;</label>
                                                <button class="btn btn-danger" style="width: 100%;">Buscar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped text-center">
                                <thead>
                                    <tr>
                                        <th>FECHA HORA</th>
                                        <th>TIPO MOVIMIENTO</th>
                                        <th>ALMACEN</th>
                                        <th>DOCUMENTO</th>
                                        <th>FECHA DOCUMENTO</th>
                                        <th>ORIGEN/DESTINO</th>
                                        <th>ESTADO</th>
                                        <th>LOTE</th>
                                        <th>CANTIDAD</th>
                                        <th>UNIDAD</th>
                                        <th>SALDO ANTERIOR LOTE</th>
                                        <th>SALDO POSTERIOR LOTE</th>
                                        <th>SALDO ANTERIOR</th>
                                        <th>SALDO POSTERIOR</th>
                                    </tr>
                                </thead>                  	
                                <tbody>  
                                    <?php foreach ($reportes as $r): ?>                    
                                        <tr> 
                                            <td><?php echo $r->__GET('valor01'); ?></td>
                                            <td><?php echo $r->__GET('valor02'); ?></td>
                                            <td><?php echo $r->__GET('valor03'); ?></td>
                                            <td><?php echo $r->__GET('valor04'); ?></td>
                                            <td><?php echo $r->__GET('valor05');?></td>
                                            <td><?php echo $r->__GET('valor06');?></td>
                                            <td><?php echo $r->__GET('valor07');?></td>
                                            <td><?php echo $r->__GET('valor08');?></td>
                                            <td><?php echo $r->__GET('valor09');?></td>
                                            <td><?php echo $r->__GET('valor10');?></td>
                                            <td><?php echo $r->__GET('valor11');?></td>
                                            <td><?php echo $r->__GET('valor12');?></td>
                                            <td><?php echo $r->__GET('valor13');?></td>
                                            <td><?php echo $r->__GET('valor14');?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>FECHA HORA</th>
                                        <th>TIPO MOVIMIENTO</th>
                                        <th>ALMACEN</th>
                                        <th>DOCUMENTO</th>
                                        <th>FECHA DOCUMENTO</th>
                                        <th>ORIGEN/DESTINO</th>
                                        <th>ESTADO</th>
                                        <th>LOTE</th>
                                        <th>CANTIDAD</th>
                                        <th>UNIDAD</th>
                                        <th>SALDO ANTERIOR LOTE</th>
                                        <th>SALDO POSTERIOR LOTE</th>
                                        <th>SALDO ANTERIOR</th>
                                        <th>SALDO POSTERIOR</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </section><!-- /.content -->
        <script type="text/javascript" src="view/librerias/dist/js/funcionesKardex.js"></script>
