<?php
if (!isset($_SESSION['usu_autenticado'])) {
    header("location: ?c=Principal&a=Index");
}

function nombremes($mes) {
    setlocale(LC_TIME, 'spanish');
    $nombre = strftime("%B", mktime(0, 0, 0, $mes, 1, 2000));
    return $nombre;
}

$year = date('Y');
$mont = nombremes(date('m'));
$day = date('d');
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Protex</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

        <link href="view/librerias/dist/css/bootstrap-datetimepicker.css" rel="stylesheet">
        <!-- Bootstrap 3.3.4 -->
        <link href="view/librerias/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- FontAwesome 4.3.0 -->
        <link href="view/librerias/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons 2.0.0 -->
        <!--<link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />-->
        <!-- DATA TABLES -->
        <link rel="stylesheet" href="http://cdn.datatables.net/plug-ins/a5734b29083/integration/bootstrap/3/dataTables.bootstrap.css"/>
        <link rel="stylesheet" href="http://cdn.datatables.net/responsive/1.0.2/css/dataTables.responsive.css"/>
        <!-- Theme style -->
        <link href="view/librerias/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link href="view/librerias/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
        <!-- iCheck -->
        <link href="view/librerias/plugins/iCheck/flat/blue.css" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="view/librerias/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
        <!-- jvectormap -->
        <link href="view/librerias/plugins/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        <!-- Date Picker -->
        <link href="view/librerias/plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
        <!-- Daterange picker -->
        <link href="view/librerias/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        <!-- bootstrap wysihtml5 - text editor -->
        <link href="view/librerias/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="view/librerias/plugins/iCheck/all.css" rel="stylesheet" type="text/css" />
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!--libreria css de bootstrap select-->
        <!--<link rel="stylesheet" href="view/librerias/dist/css/bootstrap-select.css">-->
        <!--<link rel="stylesheet" href="view/librerias/dist/css/normalize.css">-->
        <!--<link rel="stylesheet" href="view/librerias/dist/css/stylesheet.css">-->

        <link href="view/librerias/dist/css/sweet-alert.css" rel="stylesheet">
        <link href="view/librerias/dist/css/timeTo.css" rel="stylesheet">
        <link href="view/librerias/plugins/touchspin/jquery.bootstrap-touchspin.css" rel="stylesheet">
        <link rel="stylesheet" href="view/librerias/plugins/tablesaw/tablesaw.css">
        <!--[if lt IE 9]><script src="dependencies/respond.js"></script><!--<![endif]-->
        <style type="text/css">
            body {
                overflow: hidden;
            }
            /* preloader */
            #preloader {
                position: fixed;
                top:0; left:0;
                right:0; bottom:0;
                background: #000;
                opacity: 0.7;
                z-index: 100000;
            }
            #loader {
                width: 100px;
                height: 100px;
                position: absolute;
                left:50%; top:50%;
                background: url(view/librerias/dist/img/loader.gif) no-repeat center 0;
                margin:-50px 0 0 -50px;
            }
            .error{
                border: 1px solid #FF7E7E;
            }
            label.error{
                color: #FF7E7E;
                border: none;
            }
            .main-footer{
                padding: 5px 15px;
                font-size: 9px;
            }.small-box>.small-box-footer{
                padding-top: 20px;
                padding-bottom: 19px;
            }
            /*
            .table thead tr th,
            .table tbody tr td{
                padding: 15px;
                font-size: 16px;
            }*/
            .btn-producto{
                font-size: 30px;
            }
            .btn-producto p{
                white-space: normal;
            }
            #productos_servicios tr td{
                font-size: 14px;
            }
            .vnombre_cliente{
                padding: 0px;
                margin: 0px;
                display: inline-block;
                font-size: 9px;
            }
            button.btn_eliminar_pedido{
                float: left;
                margin: 0px;
                height: 60px;
                width: 40px;
                position: relative;
                color: #fff;
                z-index: 11;
                background: #dd4b39;
                opacity: 1;
            }
            .thumb{
                height: 300px;
                border: 1px solid #000;
                margin: 10px 5px 0 0;
            }

        </style>
        <!-- jQuery UI 1.11.2 -->
        <script src="view/librerias/plugins/jQuery/jquery-1.11.0.min.js" type="text/javascript"></script>
<!--        <script src="view/librerias/plugins/jQuery/jQuery-2.1.4.min.js"></script>-->
        <script type="text/javascript" src="view/librerias/dist/js/jquery.validate.js" type="text/javascript"></script>
        <script src="view/librerias/dist/js/standalone/selectize.js"></script>
        <script src="view/librerias/dist/js/index.js"></script>
        <script type="text/javascript">
            function limpiarAlert(){
                setTimeout(function() {
                    $(".alert").fadeOut(1500);
                },2000);
            }
        </script>
        <script type="text/javascript" src="view/librerias/dist/js/funcionesGenerales.js"></script>
        <!-- daterangepicker -->
        <script src="view/librerias/dist/js/moment.min.js" type="text/javascript"></script>
        <script src="view/librerias/dist/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
        <script src="view/librerias/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
        <!-- datepicker -->
        <script src="view/librerias/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
        <script src="view/librerias/plugins/touchspin/jquery.bootstrap-touchspin.js" type="text/javascript"></script>
        <script src="view/librerias/plugins/tablesaw/tablesaw.js"></script>
	<script src="view/librerias/plugins/tablesaw/tablesaw-init.js"></script>
    </head>
    <body class="skin-blue sidebar-mini" style="padding-right: 0px;">
        <div id="preloader">
            <div id="loader">&nbsp;</div>
        </div>
        <div class="wrapper">
            <header class="main-header">
                <!-- Logo -->
                <a href="?c=Principal&a=Index" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <!--<span class="logo-mini"><b>ALINEN</b></span>-->
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b style="text-transform: uppercase;">PROTEX</b></span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top" role="navigation">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <span class="glyphicon glyphicon-user img-circle"></span>
                                    <span class="hidden-xs"><?php echo $_SESSION['usu_nombre']; ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <p>
                                            <?php echo $_SESSION['usu_nombre']; ?>
                                            <small><?php echo $day . ' de ' . $mont . ' del ' . $year; ?>.</small>
                                        </p>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-right">
                                            <a href="?c=Principal&a=CerrarSession" class="btn btn-default btn-flat">Cerrar sesión</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <!-- Control Sidebar Toggle Button -->
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
<!--                        <div class="pull-left image text-center">
                            <span class="glyphicon glyphicon-user img-circle" style="font-size: 20px; color: #fff; margin: 10px;"></span>
                            <img src="view/librerias/dist/img/user.png" class="img-circle" alt="User Image" style="background: #fff;"/>
                        </div>-->
                        <div class="pull-left info">
                            <p><?php echo $_SESSION['usu_nombre']; ?></p>
                            <a href="#"><i class="fa fa-circle text-success"></i> En linea</a>
                        </div>
                    </div>
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <!--<li class="header">MENU DE NAVEGACION</li>-->
                        <li class="treeview inicio">
                            <a href="?c=Principal&a=Index">
                                <i class="fa fa-home"></i> <span>INICIO</span>
                            </a>
                        </li>
                        <li class="treeview operaciones">
                            <a href="#">
                                <i class="fa fa-server"></i><span>OPERACIONES</span> <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="?c=OrdenCompra&a=Index"><i class="fa fa-archive"></i>Orden de Compra</a></li>
                                <li><a href="?c=OrdenTrabajo&a=Index"><i class="fa fa-sitemap"></i>Orden de Trabajo</a></li>
                                <!-- <li><a href="?c=Compra&a=Index"><i class="fa fa-bitbucket"></i>Compras</a></li> -->
                                <li><a href="?c=ProduccionPrenda&a=Index"><i class="fa fa-file-archive-o"></i>Producción de Prenda</a></li>
								<li><a href="?c=Movimiento&a=Index"><i class="fa fa-sitemap"></i>Movimientos de Almacen</a></li>
                                <li><a href="?c=Creditos&a=Index"><i class="fa fa-file-archive-o"></i>Créditos</a></li>
                            </ul>
                        </li>
                        <li class="treeview catalogos">
                            <a href="#">
                                <i class="fa fa-server"></i><span>CATALOGOS</span> <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="?c=Clienteproveedor&a=Index"><i class="fa fa-archive"></i>Cliente/Proveedor</a></li>
                                <li><a href="?c=Operario&a=Index"><i class="fa fa-cog"></i>Operario</a></li>
                                <li><a href="?c=Almacen&a=Index"><i class="fa fa-archive"></i>Almacen</a></li>
                                <li><a href="?c=Familia&a=Index"><i class="fa fa-sitemap"></i>Familia de producto</a></li>
                                <li><a href="?c=Productos&a=Index"><i class="fa fa-bitbucket"></i>Productos</a></li>
                                <li><a href="?c=Tipodocumento&a=Index"><i class="fa fa-file-archive-o"></i>Tipo Documento</a></li>
                                <li><a href="?c=Usuario&a=Index"><i class="fa fa-user"></i>Usuario</a></li>
                                <li><a href="?c=Unidadesmedida&a=Index"><i class="glyphicon glyphicon-indent-right"></i>Unidad de medida</a></li>
                            </ul>
                        </li>
                        <li class="treeview reportes">
                            <a href="#">
                              <i class="fa fa-clipboard"></i><span>REPORTES</span> <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="?c=Parametro&a=Index"><i class="fa fa-file-pdf-o"></i> Movimiento</a></li>
                                <li><a href="?c=Reportes&a=Stock"><i class="fa fa-file-pdf-o"></i> Stock</a></li>
                                <li><a href="?c=Reportes&a=Kardex"><i class="fa fa-file-pdf-o"></i> Kardex</a></li>
                                <li><a href="?c=Reportes&a=VentaDetallada"><i class="fa fa-file-pdf-o"></i> Venta Detallada</a></li>
                                <li><a href="?c=Parametro&a=Index"><i class="fa fa-file-pdf-o"></i> Solicitud</a></li>
                            </ul>
                        </li>
                        <li class="treeview config">
                            <a href="#">
                              <i class="fa fa-cogs"></i><span>CONFIGURACIÓN</span> <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li>
                                    <a href="?c=Parametro&a=Index">
                                      <i class="fa fa-user"></i> Parámetros
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!--<li><a href="documentation/index.html"><i class="fa fa-book"></i> <span>Documentation</span></a></li>-->
<!--                        <li class="header">LABELS</li>
                        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
                        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
                        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>-->
                    </ul>
                    <style type="text/css">
                        #tiempo{
                            position: fixed;
                            z-index: 100;
                            bottom: 0;
                            left: 25px;
                            text-align: center;
                            font-size: 10px;
                            color: #fff;
                        }
                        #tiempo, #countdown{
                            display: inline-block;
                            font-size: 11px;
                        }
                    </style>
                    <div id="tiempo">
                        <i class="glyphicon glyphicon-time"></i>
                        &nbsp;La sesión finalizara en: <div id="countdown"></div>
                    </div>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
