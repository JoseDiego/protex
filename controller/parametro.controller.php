<?php
require_once 'model/parametro.model.php';
require_once 'model/parametro.entidad.php';
class ParametroController {
    private $parametro_model;
    public function __CONSTRUCT() {
        $this->parametro_model = new ParametroModel();
    }
    public function Index(){      
        $parametro = $this->parametro_model->ListarParametros();
        require_once 'view/header.php';    
        require 'view/webforms/wfaparametro.php';
        echo "<script type='text/javascript'>
                $(document).ready(function (){
                    $('.config').addClass('active');
                });
          </script>";
        require_once 'view/footer.php';
    }
    public function GuardarParametro(){
        $datos = array();
        
        $arrayCodigo = $_POST['arraycodigo'];
        $arrayDato = $_POST['arraydato'];
        
        $data = array();
        for($i=0;$i<count($arrayCodigo);$i++){
            $data = array("codigo" => $arrayCodigo[$i],
                          "datos" => $arrayDato[$i]);
            array_push($datos, $data);  
        }
        $this->parametro_model->guardarParametros($datos);        
    }
}
