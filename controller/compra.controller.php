<?php    
    require_once 'model/parametro.model.php';
    require_once 'model/parametro.entidad.php';
    require_once 'model/documento.model.php';
    require_once 'model/documento.entidad.php';
    require_once 'model/unidadmedida.model.php';
    require_once 'model/unidadmedida.entidad.php';
    require_once 'model/familia.model.php';
    require_once 'model/familia.entidad.php';
    require_once 'model/prodserv.model.php';
    require_once 'model/prodserv.entidad.php';
    require_once 'model/activo.model.php';
    require_once 'model/activo.entidad.php';
    require_once 'model/precioventa.model.php';
    require_once 'model/precioventa.entidad.php';
    require_once 'controller/receta.controller.php';
    require_once 'controller/metodos.controller.php';
    require_once 'model/origendestino.entidad.php';
    require_once 'model/origendestino.model.php';
    require_once 'controller/clienteproveedor.controller.php';
    require_once 'model/ordencompra.model.php';
    require_once 'model/ordencompra.entidad.php';
    require_once 'model/ordencompradetalle.model.php';
    require_once 'model/ordencompradetalle.entidad.php';
    require_once 'model/pedido.entidad.php';
    require_once 'model/pedido.model.php';
    
	class CompraController extends MetodosController{
        private $parametro_model;
        private $documento_model;
        public $unidadmedida_model;
        private $familia_model;
        private $prodserv_model;
        private $activo_model;
        public $precioventa_model;
        private $receta_controller;
        private $clienteproveedor_controller;
        private $origendestino_model;
        private $ordencompra_model;
        private $ordencompradetalle_model;
        private $pedido_model;
        public function __CONSTRUCT(){
            $this->parametro_model = new ParametroModel();
            $this->documento_model = new DocumentoModel();
            $this->unidadmedida_model = new UnidadmedidaModel();
            $this->familia_model = new FamiliaModel();
            $this->prodserv_model = new ProdservModel();
            $this->activo_model = new ActivoModel();
            $this->precioventa_model = new PrecioventaModel();
            $this->receta_controller = new RecetaController();
            $this->origendestino_model = new OrigendestinoModel();
            $this->ordencompra_model = new OrdencompraModel();
            $this->ordencompradetalle_model = new OrdencompradetalleModel();
            $this->pedido_model = new PedidoModel();
        }
        
        public function Index(){
            $parametro = $this->parametro_model->ListarParametros();
            $productos = $this->prodserv_model->Listar();
            
            require_once 'view/header.php';
            require_once 'view/webforms/wfacompra.php';
            echo "<script type='text/javascript'>
                        $(document).ready(function (){
                            $('.operaciones').addClass('active');
                        });
                  </script>";
            require_once 'view/footer.php';
        }
        
		public function NuevaCompra() {			

			require_once 'view/webforms/wfacompraa.php';
		}
                
        public function ListarComboBoxProveedor(){
            $clientes= $this->origendestino_model->Listar();
            echo '<option value="0">Seleccionar Proveedor</option>';
            //if ($_POST['OrigenDestino_Id']==0) {
                foreach ($clientes as $c){
                    if($c->__GET('nODTipo')==1){
                        echo '<option value="'.$c->__GET('OrigenDestino_Id').'" '.($_POST['OrigenDestino_Id'] != 0 ? $c->__GET('OrigenDestino_Id') == $_POST['OrigenDestino_Id'] ? 'selected':'':'').'>'.$c->__GET('sODNombre').'</option>';
                    }
                }
            //}
        }
        
        public function ListarPro(){
            $prod = $this->prodserv_model->Listar();
            echo '<option value="0">Seleccionar Producto</option>';
            
                foreach ($prod as $p){
                    
                    if($p->__GET('OrigenDestino_Id')==$_POST['Proveedor_Id'] && $p->__GET('nProSrvFamilia_Id')==15){
                        echo '<option value="'.$p->__GET('ProdServ_Id').'">'.$p->__GET('sProSrvNombre').  '</option>';
                    }
                }
            
        }
        
        public function AgregarProductoC() {
            $data = array();
            $array = array( "ProdServ_Id" => isset($_POST['ProdServ_Id']) ? $_POST['ProdServ_Id'] : '0',
                            "lotes" => isset($_POST['lotes']) ? $_POST['lotes'] : '0',
                            "sMovDetConCantidad" => $_POST['sMovDetConCantidad'],
                            "Unidadmedida_Id" => (int)$_POST['Unidadmedida_Id'],
                            "nMovDetConPrecioUnitario" => $_POST['nMovDetConPrecioUnitario'],
                            "nMovDetConSubtotal" =>  $_POST['nMovDetConPrecioUnitario'] * $_POST['sMovDetConCantidad']);
            if (isset($_SESSION['producto_c'])) {
                $data = $_SESSION['producto_c'];
                array_push($data, $array);
            } else {
                $data[] = $array;
            }
            $_SESSION['producto_c'] = $data;
            echo '<div class="alert alert-success" role="alert" name="mensajeModal" id="mensajeModal">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Exito!</strong>, Los Datos fueron agregados correctamente.
                      </div>';
        }

        public function ListarProductos() {
            if (isset($_SESSION['producto_c'])) {
                $data = $_SESSION['producto_c'];
                for ($i = 0; $i < count($data); $i++) {
                    echo "<tr>
                                <td>" . $data[$i]['sMovDetConCantidad'] . "</td>
                                <td>" . ($data[$i]['lotes'] != 0 ? $data[$i]['lotes'] : $this->ProdservdNombre($data[$i]['ProdServ_Id'])) . "</td>
                                <td>" . $this->UnidadNombre($data[$i]['Unidadmedida_Id']) . "</td>
                                <td>" . number_format($data[$i]['nMovDetConPrecioUnitario'], 2, ".", ".") . "</td>
                                <td>" . number_format($data[$i]['nMovDetConSubtotal'], 2, ".", ".") . "</td>
                                <td>
                                    <a href=\"#\" class=\"btn btn-danger btn-flat btn-xs\" data-toggle=\"modal\" data-target=\"#eliminar_productos\" data-productoid='" . $i . "'><i class=\"fa fa-trash-o\"></i></a>                                            
                                </td>
                            </tr>";
                }
            } else {
                echo "<tr>
                            <td colspan='6'>No hay datos</td>
                          </tr>";
            }
        }

        public function ProdservdNombre($ProdServ_Id) {
            $data = $this->prodserv_model->Buscar($ProdServ_Id);
            foreach ($data as $pro) {
                return $pro->__GET('sProSrvNombre');
            }
        }
        public function UnidadNombre($Unidadmedida_Id) {
            $dato = $this->unidadmedida_model->Buscar($Unidadmedida_Id);
            foreach ($dato as $d) {
                return $d->__GET('sUndAlias');
          }
        
        }
            
        public function EliminarProductoLista() {
            $array = array();
            $data = $_SESSION['producto_c'];
            for ($i = 0; $i < count($data); $i++) {
                if ($i != $_POST['Codigo_Producto']) {
                    $array[] = array("ProdServ_Id" => isset($_POST['ProdServ_Id']) ? $_POST['ProdServ_Id'] : $data[$i]['ProdServ_Id'],
                        "lotes" => isset($_POST['lotes']) ? $_POST['lotes'] : '0',
                        "sMovDetConCantidad" => $data[$i]['sMovDetConCantidad'],
                        "Unidadmedida_Id" => $data[$i]['Unidadmedida_Id'],
                        "nMovDetConPrecioUnitario" => $data[$i]['nMovDetConPrecioUnitario'],
                        "nMovDetConSubtotal" => $data[$i]['nMovDetConSubtotal']);
                }
            }
            $_SESSION['producto_c'] = $array;
            echo "Datos eliminados";
        }
        
    public function CalcularImpTotIGV() {
            $importe = 0;
            $total = 0;
            $IGV = 0.18;

            if (isset($_SESSION['producto_c'])) {
                $data = $_SESSION['producto_c'];
                for ($i = 0; $i < count($data); $i++) {
                    $importe = $importe + $data[$i]['nMovDetConSubtotal'];
                }
            }
            echo "<script type='text/javascript'>
                        $('#dMovImporte').val('" . number_format($importe, 2, ".", "") . "');
                        $('#dMovIGV').val('" . number_format(($importe *  $IGV ), 2, ".", "") . "');
                        $('#dMovTotal').val('" . number_format(($importe * $IGV + $importe), 2, ".", "") . "');
                        $('#dMovSaldo').val('" . number_format(($importe * $IGV + $importe), 2, ".", "") . "');
                    </script>";
        }

    }
?>