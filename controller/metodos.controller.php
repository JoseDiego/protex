<?php

class MetodosController
{
   
    public function NombreCortoDocumento($Documento_Id)
    {
        foreach ($this->documento_model->Buscar($Documento_Id) as $d)
        {
            return $d->__GET('sDocNombreCorto');
        }
    }
    public function NombreUnidadMedida($Documento_Id)
    {
        foreach ($this->documento_model->Buscar($Documento_Id) as $d)
        {
            return $d->__GET('sDocNombreCorto');
        }
    }
    public function NombreUnidadMedidaProducto($UnidadMedida_Id)
    {
        foreach ($this->unidadmedida_model->Buscar($UnidadMedida_Id) as $uni)
        {
            return $uni->__GET('sUndDescripcion');
        }
    }
    public function NombreProdServ($ProdServ_Id)
    {
        foreach ($this->prodserv_model->Buscar($ProdServ_Id) as $pro)
        {
            return $pro->__GET('sProSrvNombre');
        }
    }
    public function VerificarPadre($UnidadMedida_Id)
    {        
        $estado = false;
        foreach ($this->unidadmedida_model->Listar() as $uni)
        {
            if($uni->__GET('nUndPadre_Id') == $UnidadMedida_Id)
            {
                return true;
            }
        }
        if($estado == false)
        {
            return false;     
        }               
    }
    public function NombreClienteProveedor($OrigenDestino_Id)
    {
        foreach ($this->origendestino_model->Buscar($OrigenDestino_Id) as $ori)
        {
            return $ori->__GET('sODNombreComercial');
        }
    }
    public function FechaActual()
    {
        echo date('d-m-Y');
    }
    public function PrecioProducto()
    {
        $precio = 0;
        $producto = $this->precioventa_model->Precio($_POST['ProdServ_Id'], $_POST['Unidadmedida_Id']);
        foreach ($producto as $r){
            $precio = $r->__GET('PreVenPrecio');
        }
        echo $precio;
    }
    public function PrecioProductoVenta($ProdServ_Id, $Unidadmedida_Id)
    {
        $precio = 0;
        $producto = $this->precioventa_model->Precio($ProdServ_Id, $Unidadmedida_Id);
        foreach ($producto as $r){
            return $precio = $r->__GET('PreVenPrecio');
        }
    }
    public function AutomaticoSINO()
    {
        $documento = $this->documento_model->Buscar($_POST['Documento_Id'] != "" ? $_POST['Documento_Id'] : "-1");
        if(count($documento) > 0)
        {
            foreach ($documento as $d)
            {
                if($d->__GET('nDocNomAutomatico') == 1)
                {
                    echo '<script type="text/javascript">$("#sMovDocumento").prop("disabled",true);</script>';
                }
                else
                {
                    echo '<script type="text/javascript">$("#sMovDocumento").prop("disabled",false);</script>';
                }
            }
        }
    }
    public function ObtenerNumeroDocumento($Documento_Id)
    {
        $documento = $this->documento_model->Buscar($Documento_Id);
        foreach ($documento as $d)
        {
            return $d->__GET('sDocSiguiente');       
        }
    }
    public function IdProductoLote($ProductoLote_Id)
    {
        foreach ($this->productolote_model->Buscar($ProductoLote_Id) as $prolot)
        {
            return $prolot->__GET('ProdServ_Id');
        }
    }
    public function NombreProductoLote($ProductoLote_Id)
    {
        foreach ($this->productolote_model->Buscar($ProductoLote_Id) as $prolot)
        {
            return $prolot->__GET('sProLNombreLote');
        }
    }
    public function FechaVencimiento($ProductoLote_Id)
    {
        $producto_lote = $this->productolote_model->Buscar($ProductoLote_Id);
        foreach ($producto_lote as $prolot):
            return $prolot->__GET('dProLFechaVencimiento');
        endforeach;
    }
}

