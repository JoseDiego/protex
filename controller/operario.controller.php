<?php
    require_once 'model/parametro.model.php';
    require_once 'model/parametro.entidad.php';
    require_once 'model/operario.model.php';
    require_once 'model/operario.entidad.php';
    class OperarioController {
        private $parametro_model;
        private $operario_model;
        public function __CONSTRUCT(){
            $this->parametro_model = new ParametroModel();
            $this->operario_model = new OperarioModel();
        }

        public function Index(){
            $parametro = $this->parametro_model->ListarParametros();
            $operario = $this->operario_model->Listar();
            require_once 'view/header.php';
            require_once 'view/webforms/wfaoperario.php';
            echo "<script type='text/javascript'>
                        $(document).ready(function (){
                            $('.catalogos').addClass('active');
                        });
                  </script>";
            require_once 'view/footer.php';
        }
        public function FrmGuardarOperario(){
            require_once 'view/webforms/wfaoperarioa.php';
        }
        public function GuardarOperario(){
            $ope = new Operario();
            if($_POST['Operario_Id'] != 0 ? $ope->__SET('Operario_Id', $_POST['Operario_Id']) : '');
            $ope->__SET('sOpeDni', $_POST['sOpeDni']);
            $ope->__SET('sOpeApePat', $_POST['sOpeApePat']);
            $ope->__SET('sOpeApeMat', $_POST['sOpeApeMat']);
            $ope->__SET('sOpeNombres', $_POST['sOpeNombres']);
            $ope->__SET('sOpeCargo', $_POST['sOpeCargo']);
            $ope->__SET('sOpeDireccion', $_POST['sOpeDireccion']);
            $ope->__SET('sOpeTelefono', $_POST['sOpeTelefono']);
            $ope->__SET('sOpeCorreo', $_POST['sOpeCorreo']);
            $ope->__SET('nOpeEstado', $_POST['nOpeEstado']);
            if($_POST['Operario_Id'] != 0 ? $this->operario_model->Editar($ope) : $this->operario_model->Guardar($ope));
        }
        public function FrmEditarOperario(){
            $datos = $this->operario_model->Buscar($_POST['Operario_Id']);
            foreach ($datos as $r):
                require_once 'view/webforms/wfaoperarioa.php';
            endforeach;
        }
        public function MostrarOperario(){
            $operarios = $this->operario_model->Buscar($_POST['Operario_Id']);
            foreach ($operarios as $r):
                echo " <tbody>
                          <tr>
                             <td>
                                <input type='hidden' name='Operario_Id' id='Operario_Id' value='".$r->__GET('Operario_Id')."'>
                                Dni
                             </td>
                             <td>".$r->__GET('sOpeDni')."</td>
                          </tr>
                          <tr>
                             <td>Apellidos</td>
                             <td>".$r->__GET('sOpeApePat'). " ".$r->__GET('sOpeApeMat')."</td>
                          </tr>
                          <tr>
                             <td>Nombres</td>
                             <td>".$r->__GET('sOpeNombres')."</td>
                          </tr>
                          <tr>
                             <td>Cargo</td>
                             <td>".$r->__GET('sOpeCargo')."</td>
                          </tr>
                          <tr>
                             <td>Dirección</td>
                             <td>".$r->__GET('sOpeDireccion')."</td>
                          </tr>
                          <tr>
                             <td>Teléfono</td>
                             <td>".$r->__GET('sOpeTelefono')."</td>
                          </tr>
                          <tr>
                             <td>Correo</td>
                             <td>".$r->__GET('sOpeCorreo')."</td>
                          </tr>
                          <tr>
                             <td>Estado</td>
                             <td>".($r->__GET('nOpeEstado') == '1' ? '<span class="label bg-green">Habilitado</span>' : '<span class="label bg-danger">Inhabilitado</span>')."</td>
                          </tr>
                       </tbody>
                     ";
            endforeach;
        }
        public function EliminarOperario(){
            $proope = new Operario();
            $proope->__SET('Operario_Id', $_POST['Operario_Id']);
            $this->operario_model->Eliminar($proope);
            echo '<script type="text/javascript">window.location="?c=Operario&a=Index";</script>';
        }
        
        public function PDFCliente(){
          $datos= $this->origendestino_model->Buscar($_POST['OrigenDestino_Id']);
          require_once 'view/webforms/PDF/cliente.php';
        }
    }
?>
