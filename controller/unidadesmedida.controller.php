<?php    
    require_once 'model/parametro.model.php';
    require_once 'model/parametro.entidad.php';
    require_once 'model/unidadmedida.model.php';
    require_once 'model/unidadmedida.entidad.php';
    class UnidadesmedidaController {
        private $parametro_model;
        private $unidadmedida_model;
        public function __CONSTRUCT(){
            $this->parametro_model = new ParametroModel();
            $this->unidadmedida_model = new UnidadmedidaModel();
        }
        
        public function Index(){
            $unidadmedida = $this->unidadmedida_model->Listar();
            $parametro = $this->parametro_model->ListarParametros();
            require_once 'view/header.php';
            require_once 'view/webforms/wfaunidadesmedida.php';
            echo "<script type='text/javascript'>
                        $(document).ready(function (){
                            $('.catalogos').addClass('active');
                        });
                  </script>";
            require_once 'view/footer.php';
        }
        public function FrmUnidadmedida(){
            $unidadmedida = $this->unidadmedida_model->Listar();
            require 'view/webforms/wfaunidadesmedidaa.php';
        }
        public function FrmEditarUnidadmedida(){
            $unidadmedida = $this->unidadmedida_model->Listar();
            $datos = $this->unidadmedida_model->Buscar($_POST['Unidadmedida_Id']);
            foreach ($datos as $r){                
                require 'view/webforms/wfaunidadesmedidaa.php';
            }
        }
        public function GuardarUnidadmedida(){
            $uni = new Unidadmedida();
            if($_POST['Unidadmedida_Id'] != 0 ? $uni->__SET('UnidadMedida_Id', $_POST['Unidadmedida_Id']) : '');
            $uni->__SET('sUndDescripcion', $_POST['sUndDescripcion']);
            $uni->__SET('sUndAlias', $_POST['sUndAlias']);
            $uni->__SET('nUndPadre_Id', $_POST['nUndPadre_Id']);
            $uni->__SET('nUndFac_Cnv', $_POST['nUndFac_Cnv']);
            $uni->__SET('nUndEstado', $_POST['nUndEstado']);
            $uni->__SET('nUndEliminado', 0);
            $uni->__SET('Usuario_Id', $_SESSION['usu_codigo']);
            if($_POST['Unidadmedida_Id'] != 0 ? $this->unidadmedida_model->Editar($uni) : $this->unidadmedida_model->guardar($uni));
        }
        public function ListarArbolContenido(){
            $unidadmedida = $this->unidadmedida_model->Listar();
            $ex = array();
            foreach ($unidadmedida as $arb){
                $ex[] = array('id' => $arb->__GET('UnidadMedida_Id'),
                              'nombre' => $arb->__GET('sUndDescripcion'),
                              'alias' => $arb->__GET('sUndAlias'),
                              'parentId' => $arb->__GET('nUndPadre_Id'));
            }
            echo '<ol class="tree">';
            $arbol_lista = "";
            for($j=0;$j<count($ex);$j++){
                if($ex[$j]['parentId'] == 0){
                    $arbol_lista .= '<li>
                                        <label>'.$ex[$j]['nombre'].' ('.$ex[$j]['alias'].')</label>
                                        <input type="checkbox" checked id="folder1" />'
                                        .Arbol($ex,$ex[$j]['id']).'
                                    </li>';
                }  
            }
            echo $arbol_lista;
            echo '</ol>';
        }
        public function EliminarUnidadmedida(){
            $uni = new Unidadmedida();
            $uni->__SET('UnidadMedida_Id', $_POST['Unidadmedida_Id']); 
            $dato = $this->unidadmedida_model->Hijos($_POST['Unidadmedida_Id']);
            if(count($dato)>0){  
                $un = new Unidadmedida();
                $un->__SET('UnidadMedida_Id', $_POST['Unidadmedida_Id']); 
                $un->__SET('nUndPadre_Id', $_POST['nUndPadre_Id']);
                $this->unidadmedida_model->Padre($un);
                $this->unidadmedida_model->Eliminar($uni);  
            }else{ 
                $this->unidadmedida_model->Eliminar($uni);                
            }
            echo '<div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Exito guardado</strong>, los datos fueron eliminados correctamente.
                  </div>';        
        }
    }
    function Arbol($tree, $padre){
        $cont = 0;
        $list = "<ol>";
        for($i=0;$i<count($tree);$i++){
            if($tree[$i]['parentId'] == $padre){
                $list .= '<li>
                            <label><a onclick="FrmEditarUnidadmedida(\''.$tree[$i]['id'] .'\');">'. $tree[$i]['nombre'] .' ('.$tree[$i]['alias'].')</a></label>
                            <input type="checkbox" checked id="'.$tree[$i]['nombre'].'" />'
                            .Arbol($tree, $tree[$i]['id']).' 
                          </li>';
                $cont++;
            }
        }    
        $list .= "</ol>";
        if($cont == 0){
            $list = "";
        }
        return $list;
    }
?>