<?php
    require_once 'model/ordentrabajo.model.php';
    require_once 'model/ordentrabajo.entidad.php';
    require_once 'model/ordentrabajodetalle.model.php';
    require_once 'model/ordentrabajodetalle.entidad.php';
    require_once 'model/parametro.model.php';
    require_once 'model/parametro.entidad.php';
    require_once 'model/documento.model.php';
    require_once 'model/documento.entidad.php';
    require_once 'model/unidadmedida.model.php';
    require_once 'model/unidadmedida.entidad.php';
    require_once 'model/familia.model.php';
    require_once 'model/familia.entidad.php';
    require_once 'model/prodserv.model.php';
    require_once 'model/prodserv.entidad.php';
    require_once 'model/activo.model.php';
    require_once 'model/activo.entidad.php';
    require_once 'model/precioventa.model.php';
    require_once 'model/precioventa.entidad.php';
    require_once 'controller/receta.controller.php';
    require_once 'controller/metodos.controller.php';
    require_once 'model/origendestino.entidad.php';
    require_once 'model/origendestino.model.php';
    require_once 'controller/clienteproveedor.controller.php';
    require_once 'model/ordencompra.model.php';
    require_once 'model/ordencompra.entidad.php';
    require_once 'model/ordencompradetalle.model.php';
    require_once 'model/ordencompradetalle.entidad.php';
    require_once 'model/pedido.entidad.php';
    require_once 'model/pedido.model.php';


	class OrdenCompraController extends MetodosController{
        private $parametro_model;
        private $documento_model;
        public $unidadmedida_model;
        private $familia_model;
        private $prodserv_model;
        private $activo_model;
        public $precioventa_model;
        private $receta_controller;
        private $clienteproveedor_controller;
        private $origendestino_model;
        private $ordencompra_model;
        private $ordencompradetalle_model;
        private $pedido_model;
        private $ordentrabajo_model;
        private $ordentrabajodetalle_model;

        public function __CONSTRUCT(){
            $this->parametro_model = new ParametroModel();
            $this->documento_model = new DocumentoModel();
            $this->unidadmedida_model = new UnidadmedidaModel();
            $this->familia_model = new FamiliaModel();
            $this->prodserv_model = new ProdservModel();
            $this->activo_model = new ActivoModel();
            $this->precioventa_model = new PrecioventaModel();
            $this->receta_controller = new RecetaController();
            $this->origendestino_model = new OrigendestinoModel();
            $this->ordencompra_model = new OrdencompraModel();
            $this->ordencompradetalle_model = new OrdencompradetalleModel();
            $this->pedido_model = new PedidoModel();
            $this->ordentrabajo_model = new OrdenTrabajoModel();
            $this->ordentrabajodetalle_model = new OrdenTrabajoDetalleModel();

        }

        public function Index(){
            $parametro = $this->parametro_model->ListarParametros();
            $productos = $this->prodserv_model->Listar();
            $clientes = $this->origendestino_model->Listar();
            $ordencompra= $this->ordencompra_model->Listar();
            $orden = new OrdenCompraController();
            $this->DestruirSessiones();
            //$ordenes = $this->ordencompra_model->ListarFechas($_POST['fInicio'],$_POST['fFin']);
            require_once 'view/header.php';
            require_once 'view/webforms/wfaordencompra.php';
            echo "<script type='text/javascript'>
                        $(document).ready(function (){
                            $('.operaciones').addClass('active');
                        });
                  </script>";
            require_once 'view/footer.php';
        }

		public function NuevaOrdenCompra() {

			require_once 'view/webforms/wfaordencompraa.php';
		}

        public function ListarComboBoxClientes(){
            $clientes= $this->origendestino_model->Listar();
            echo '<option value="0">Seleccionar clientes</option>';
            //if ($_POST['OrigenDestino_Id']==0) {
                foreach ($clientes as $c){
                    if($c->__GET('nODTipo')==2){
                        echo '<option value="'.$c->__GET('OrigenDestino_Id').'" '.($_POST['OrigenDestino_Id'] != 0 ? $c->__GET('OrigenDestino_Id') == $_POST['OrigenDestino_Id'] ? 'selected':'':'').'>'.$c->__GET('sODNombre').'</option>';
                    }
                }
            //}
        }

        public function ListarComboBoxProveedor(){
            $clientes= $this->origendestino_model->Listar();
            echo '<option value="0">Seleccionar Proveedor</option>';
            //if ($_POST['OrigenDestino_Id']==0) {
                foreach ($clientes as $c){
                    if($c->__GET('nODTipo')==1){
                        echo '<option value="'.$c->__GET('OrigenDestino_Id').'" '.($_POST['OrigenDestino_Id'] != 0 ? $c->__GET('OrigenDestino_Id') == $_POST['OrigenDestino_Id'] ? 'selected':'':'').'>'.$c->__GET('sODNombre').'</option>';
                    }
                }
            //}
        }

        public function FrmEditarOrdenCompra(){
            $datos = $this->ordencompra_model->Buscar($_POST['Pedido_Id']);
          //  $detalle = $this->ordencompradetalle_model->Buscar($_POST['Pedido_Id']);
          //  $_SESSION['detalle'] = $detalle;

            foreach ($datos as $r):
                require_once 'view/webforms/wfaordencompraa.php';
            endforeach;
           // foreach ($detalle as $det):
           //     require_once 'view/webforms/wfaordencompraa.php';
           // endforeach;
        }

        public function ListarDetalles() {
            $detalle = $this->ordencompradetalle_model->Buscar($_POST['Pedido_Id']);
            $data = array();
            //$_SESSION['producto_c']=$detalle;

            if (isset($detalle)) {
                $i=0;
                foreach ($detalle as $det){

                    if(isset($det)){
                    $array = array ( "Pedidodetalle_Id"=>$det->__GET('Pedidodetalle_Id'),
                            "ProdServ_Id" => empty($det->__GET('ProdServ_Id')) ? $det->__GET('ProdServ_Id') : '0',
                            "lotes" => '0',
                            "sMovDetConCantidad" => $det->__GET('nPedDetCantidad'),
                            "Unidadmedida_Id" => (int)$det->__GET('UnidadMedida_Id'),
                            "nMovDetConPrecioUnitario" => $det->__GET('dPedDetPrecioUnitario'),
                            "nMovDetConSubtotal" =>  $det->__GET('dPedDetPrecioUnitario') * $det->__GET('nPedDetCantidad'));
                array_push($data, $array);
                    } else{}

                   echo "<tr>
                                <td>" . $det->__GET('nPedDetCantidad') . "</td>
                                <td>" . $this->ProdservdNombre($det->__GET('ProdServ_Id')) . "</td>
                                <td>" . $this->UnidadNombre($det->__GET('UnidadMedida_Id')) . "</td>
                                <td>" . number_format($det->__GET('dPedDetPrecioUnitario'), 2, ".", ".") . "</td>
                                <td>" . number_format($det->__GET('dPedDetSubtotal'), 2, ".", ".") . "</td>
                                <td>
                                    <a href=\"#\" class=\"btn btn-danger btn-flat btn-xs\" data-toggle=\"modal\" data-target=\"#eliminar_productos\" data-productoid='" . $i . "'><i class=\"fa fa-trash-o\"></i></a>
                                </td>
                            </tr>";
                   $i++;
                }
                $_SESSION['producto_c'] = $data;
            } else {

                echo "<tr>
                            <td colspan='6'>No hay datos</td>
                          </tr>";
            }

        }


        public function CantidadTotal($unidad,$cant){
            $cantidad = $cant;
            if($unidad  == 29){$cantidad = $cant*12;} else{}
        if($unidad ==  30){$cantidad = $cant*100;} else{}

        if($unidad == 31){$cantidad = $cant*6;}
             else{
                    if($unidad  == 32){
                    $cantidad = $cant*3;}
                    else{}
             }

             return $cantidad;
        }
        public function ListarDetallesOC() {
            $detalle = $this->ordencompradetalle_model->Buscar($_POST['Pedido_Id']);
            if(isset($_POST['OrdenTrabajo'])){
                 $ot = new OrdenTrabajo();
                 $ot = $this->ordentrabajo_model->BuscarO($_POST['OrdenTrabajo']);
            }

            $data = array();
            //$_SESSION['producto_c']=$detalle;
            $result='';
            if (isset($detalle)) {
                $i=0;
                foreach ($detalle as $det){
                    $unidad = $det->__GET('UnidadMedida_Id');
                    $cantidad = $det->__GET('nPedDetCantidad');

                    $cantidad = $this->CantidadTotal($unidad, $cantidad);
                    if($unidad >= 29 && $unidad <= 32){$unidad = 2;}


                                $result .= "<tr>   <td style='display:none;'>" . $det->__GET('ProdServ_Id') . "</td>
                                <td>" . $this->ProdservdNombre($det->__GET('ProdServ_Id')) . "</td>
                                <td>" . $this->UnidadNombre($unidad) . "</td>
                                <td style='display:none;'>" . $unidad . "</td>";
                    if($ot->__GET('OrdenTrabajo_Id') != 0 ){
                                $otdetalle = new OrdenTrabajoDetalle();
                                $otdetalle = $this->ordentrabajodetalle_model->Buscar($ot->__GET('OrdenTrabajo_Id'), $det->__GET('ProdServ_Id'));
                                if(isset($otdetalle)){
                       $result .= "<td name='cant".$i."' id='cant".$i."' >".$otdetalle->__GET('CantidadRest')."</td>
                                   <td name='cantProg".$i."' id='cantProg".$i."' required onkeypress='justNumbers(event);' onkeyup='calculo(this.id,cant".$i.".id,cantRest".$i.".id)' contentEditable='true'></td>
                                   <td name='cantRest".$i."' id='cantRest".$i."' >". $otdetalle->__GET('CantidadRest')."</td>
                                  </tr>";
                    }}
                    else{
                    $result .="<td name='cant".$i."' id='cant".$i."' >".$cantidad."</td>
                               <td name='cantProg".$i."' id='cantProg".$i."' required onkeypress='justNumbers(event);' onkeyup='calculo(this.id,cant".$i.".id,cantRest".$i.".id)' contentEditable='true'></td>
                               <td name='cantRest".$i."' id='cantRest".$i."' >". $cantidad."</td>
                    </tr>";}
                   $i++;


                }
              } else {

                echo "<tr>
                            <td colspan='5'>No hay datos</td>
                          </tr>";
            }
            if ($result == '') {
              $result = '<tr><td colspan="5">No hay datos</td></tr>';
            }
            echo $result;
        }

        public function CalculoRest() {
            $cant = (int)$_POST['cant'];
            $cantProg = (int)$_POST['cantProg'];
            $result = $cant - $cantProg;
            echo $result;

        }

        public function ModalOrdenCompra() {
            $productos = $this->prodserv_model->Listar();
            require_once 'view/webforms/ordencompra/orden_compra.php';
        }

        public function AgregarProductoC() {
            $data = array();
            if($_POST['nIncluyeIGV'] == 1){
                $nMovDetConPrecioUnitario = $_POST['nMovDetConPrecioUnitario'] / $this->IGV;
            }else{
                $nMovDetConPrecioUnitario = $_POST['nMovDetConPrecioUnitario'];
            }
            $array = array( "ProdServ_Id" => isset($_POST['ProdServ_Id']) ? $_POST['ProdServ_Id'] : '0',
                            "lotes" => isset($_POST['lotes']) ? $_POST['lotes'] : '0',
                            "sMovDetConCantidad" => $_POST['sMovDetConCantidad'],
                            "Unidadmedida_Id" => (int)$_POST['Unidadmedida_Id'],
                            "nMovDetConPrecioUnitario" => $nMovDetConPrecioUnitario,
                            "nMovDetConSubtotal" =>  $nMovDetConPrecioUnitario * $_POST['sMovDetConCantidad']);
            if (isset($_SESSION['producto_c'])) {
                $data = $_SESSION['producto_c'];
                array_push($data, $array);
            } else {
                $data[] = $array;
            }
            $_SESSION['producto_c'] = $data;
            echo '<div class="alert alert-success" role="alert" name="mensajeModal" id="mensajeModal">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Exito!</strong>, Los Datos fueron agregados correctamente.
                      </div>';
        }

        public function ListarProductos() {
            if (isset($_SESSION['producto_c'])) {
                $data = $_SESSION['producto_c'];
                for ($i = 0; $i < count($data); $i++) {
                    echo "<tr>
                                <td>" . $data[$i]['sMovDetConCantidad'] . "</td>
                                <td>" . ($data[$i]['lotes'] != 0 ? $data[$i]['lotes'] : $this->ProdservdNombre($data[$i]['ProdServ_Id'])) . "</td>
                                <td>" . $this->UnidadNombre($data[$i]['Unidadmedida_Id']) . "</td>
                                <td>" . number_format($data[$i]['nMovDetConPrecioUnitario'], 2, ".", ".") . "</td>
                                <td>" . number_format($data[$i]['nMovDetConSubtotal'], 2, ".", ".") . "</td>
                                <td>
                                    <a href=\"#\" class=\"btn btn-danger btn-flat btn-xs\" data-toggle=\"modal\" data-target=\"#eliminar_productos\" data-productoid='" . $i . "'><i class=\"fa fa-trash-o\"></i></a>
                                </td>
                            </tr>";
                }
            } else {
                echo "<tr>
                            <td colspan='6'>No hay datos</td>
                          </tr>";
            }
        }
        public function ProdservdNombre($ProdServ_Id) {
            $data = $this->prodserv_model->Buscar($ProdServ_Id);
            foreach ($data as $pro) {
                return $pro->__GET('sProSrvNombre');
            }
        }
        public function UnidadNombre($Unidadmedida_Id) {
            $dato = $this->unidadmedida_model->Buscar($Unidadmedida_Id);
            foreach ($dato as $d) {
                return $d->__GET('sUndAlias');
            }
        }

        public function ListarUnidades() {
            $unidad = $this->unidadmedida_model->ListarUP($this->ObtenerID($_POST['ProdServ_Id']));
            $_SESSION['$unidad'] = $unidad;
            echo "<option value=''>Seleccione unidad</option>";
            foreach ($unidad as $uni) {
                echo "<option value='" . $uni->__GET('UnidadMedida_Id') . "'>" . $uni->__GET('sUndDescripcion') . "</option>";
            }
        }

        public function EliminarProductoLista() {
            $array = array();
            $data = $_SESSION['producto_c'];

            for ($i = 0; $i < count($data); $i++) {
                if ($i != $_POST['Codigo_Producto']) {
                    $array[] = array("ProdServ_Id" => isset($_POST['ProdServ_Id']) ? $_POST['ProdServ_Id'] : $data[$i]['ProdServ_Id'],
                        "lotes" => isset($_POST['lotes']) ? $_POST['lotes'] : '0',
                        "sMovDetConCantidad" => $data[$i]['sMovDetConCantidad'],
                        "Unidadmedida_Id" => $data[$i]['Unidadmedida_Id'],
                        "nMovDetConPrecioUnitario" => $data[$i]['nMovDetConPrecioUnitario'],
                        "nMovDetConSubtotal" => $data[$i]['nMovDetConSubtotal']);
                }
            }
            $_SESSION['producto_c'] = $array;
            echo "Datos eliminados";


        }

        public function CalcularImpTotIGV() {
            $importe = 0;
            $total = 0;
            $IGV = 0.18;

            if (isset($_SESSION['producto_c'])) {
                $data = $_SESSION['producto_c'];
                for ($i = 0; $i < count($data); $i++) {
                    $importe = $importe + $data[$i]['nMovDetConSubtotal'];
                }
            }
            echo "<script type='text/javascript'>
                        $('#dMovImporte').val('" . number_format($importe, 2, ".", "") . "');
                        $('#dMovIGV').val('" . number_format(($importe *  $IGV ), 2, ".", "") . "');
                        $('#dMovTotal').val('" . number_format(($importe * $IGV + $importe), 2, ".", "") . "');
                        $('#dMovSaldo').val('" . number_format(($importe * $IGV + $importe), 2, ".", "") . "');
                    </script>";
        }

        public function ListarFechasO(){
            $orden = $this->ordencompra_model->ListarFechas($_POST['fInicio'],$_POST['fFin']);
        $_SESSION['$orden'] = $orden;
           if (empty($orden)) {
                echo '<tr><td colspan="8"><h4>No existen Ordenes de Compra del  '.$_POST['fInicio']. ' al ' .$_POST['fFin'].'</h4></td></tr>';
            }
            else{
            foreach ($orden as $oc) {

                $estado = $oc->__GET('nPedEstado');
                $valor = $this->Estado($estado);
                echo '<tr>
                          <td>'.$oc->__GET('Pedido_Id').'</td>
                          <td>'.$oc->__GET('dPedFecha').'</td>
                          <td>'.$oc->__GET('sODRucDni').'</td>
                          <td>'.$oc->__GET('sODNombre').'</td>
                          <td>'.$oc->__GET('nroOrdenTrabajo').'</td>
                          <td>'.$oc->__GET('dPedTotal').'</td>
                          <td>'.$valor.'</td>
                           <td>
                                            <a  class="btn btn-primary btn-flat btn-xs" data-toggle="modal" data-target="#modal-ver" onclick="mostrarOrdenCompra('. $oc->__GET('Pedido_Id').');"><i class="glyphicon glyphicon-eye-open"></i></a>
                                            <a href="#" class="btn btn-success btn-flat btn-xs" onclick="FrmEditarOrdenCompra('.$oc->__GET('Pedido_Id').');"><i class="fa fa-pencil"></i></a>
                                            <a href="#" class="btn btn-danger btn-flat btn-xs" data-toggle="modal" data-target="#modal-eliminar" data-codigo="'.$oc->__GET('Pedido_Id').'"><i class="fa fa-trash-o"></i></a>
                                        </td>
                     </tr>';
            }

           }
        }

        public function Estado($estado){
            if($estado == 1){
                        $valor = '<span class="label label-warning">En Espera</span>';
                    }
                    else{
                      if($estado == 2){
                        $valor = '<span class="label label-success">En Proceso</span>';
                       }
                      else{
                          $valor = '<span class="label label-danger">Terminado</span>';
                      }
                    }
           return $valor;
        }
        public function MostrarOrdenCompra(){
            $ordencompra= $this->ordencompra_model->Buscar($_POST['Pedido_Id']);
            $Pedido_Id = $_POST['Pedido_Id'];
            foreach ($ordencompra as $r):
                if($r->__GET('Moneda_Id')==1){ $moneda='Soles (S/.)';}else{$moneda='Dolares ($)';}
                if($r->__GET('nPedTipopago')==1){ $tipo='Efectivo';}else{$tipo='Credito';}
                if($r->__GET('dPedEliminado')==1){ $del='Si';}else{$del='No';}
                 $estado = $r->__GET('nPedEstado');
                 $valor = $this->Estado($estado);
                echo " <tbody>
                          <tr>
                             <td>
                                <input type='hidden' name='Pedido_Id' id='Pedido_Id' value='".$r->__GET('Pedido_Id')."'>
                                RUC/DNI
                             </td>
                             <td>".$r->__GET('sODRucDni')."</td>
                          </tr>
                          <tr>
                             <td>Nombre Cliente</td>
                             <td>".$r->__GET('sODNombre')."</td>
                          </tr>
                          <tr>
                             <td>Fecha</td>
                             <td>".date('d/m/Y', strtotime($r->__GET('dPedFecha')))."</td>
                          </tr>
                          <tr>
                             <td>Tipo de Pago</td>
                             <td>".$tipo."</td>
                          </tr>
                          <tr>
                             <td>Moneda</td>
                             <td>".$moneda."</td>
                          </tr>
                          <tr>
                             <td>Documento</td>
                             <td>".$r->__GET('sDocNombre')."</td>
                          </tr>
                          <tr>
                             <td>Nro Orden de Trabajo</td>
                             <td>".$r->__GET('nroOrdenTrabajo')."</td>
                          </tr>
                          <tr>
                             <td>Importe de Adelanto</td>
                             <td>".$r->__GET('dPedImporteAd')."</td>
                          </tr>
                          <tr>
                             <td>Importe</td>
                             <td>".$r->__GET('dPedImporte')."</td>
                          </tr>
                          <tr>
                             <td>IGV</td>
                             <td>".$r->__GET('dPedIGV')."</td>
                          </tr>
                          <tr>
                             <td>Total</td>
                             <td>".$r->__GET('dPedTotal')."</td>
                          </tr>
                          <tr>
                             <td>Saldo</td>
                             <td>".$r->__GET('dPedSaldo')."</td>
                          </tr>
                          <tr>
                             <td>Observaciones</td>
                             <td>".$r->__GET('sPedObservaciones')."</td>
                          </tr>
                          <tr>
                             <td>Eliminado</td>
                             <td>".$del."</td>
                          </tr>
                          <tr>
                             <td>Estado</td>
                             <td>".$valor."</td>
                          </tr>
                          <tr><td colspan='2'>
                          <table class='table table-bordered table-striped text-center'>
                                            <thead>
                                                <tr>
                                                    <th>Cantidad</th>
                                                    <th>Producto</th>
						    <th>Unidad</th>
                                                    <th>Prec. Unitario</th>
                                                    <th>Sub Total</th>
                                                </tr>
                                            </thead>
                                            <tbody id='productos_servicios'>


                                                    ". $this->Detalles($Pedido_Id)."

                                            </tbody>
                                        </table>
                          </td></tr>
                       </tbody>
                     ";
            endforeach;
        }

        public function Detalles($Pedido_Id){
            $ordencompradetalle = $this->ordencompradetalle_model->Listar($Pedido_Id);
            $result = '';
             foreach($ordencompradetalle as $ocd) {
                    $result .= "<tr>
                                <td>" . $ocd->__GET('nPedDetCantidad') . "</td>
                                <td>" . $this->ProdservdNombre($ocd->__GET('ProdServ_Id')) . "</td>
                                <td>" . $this->UnidadNombre($ocd->__GET('UnidadMedida_Id')) . "</td>
                                <td>" . number_format($ocd->__GET('dPedDetPrecioUnitario'), 2, ".", ".") . "</td>
                                <td>" . number_format($ocd->__GET('dPedDetSubtotal'), 2, ".", ".") . "</td>
                                </tr>";

             }
             return $result;
        }


        public function GuardarMovimiento() {
            $mov = new Ordencompra();
            if ($_POST['Movimiento_Id'] != 0 ? $mov->__SET('Movimiento_Id', $_POST['Movimiento_Id']) : '')
                ;
            $mov->__SET('OrigenDestino_Id', $_POST['nMovOrigenDestino_Id']);
            $mov->__SET('dPedFecha', $_POST['dMovFecha']);
            $mov->__SET('nPedTipopago', $_POST['nMovTipopago']);
            $mov->__SET('Documento_Id', 7);
            $mov->__SET('nroOrdenTrabajo', $_POST['sMovDocumento']);
            $mov->__SET('Moneda_Id', $_POST['Moneda_Id']);
            $mov->__SET('dPedImporteAd', $_POST['dPedImporteAd']);
            $mov->__SET('dPedImporte', $_POST['dMovImporte']);
            $mov->__SET('dPedIGV', $_POST['dMovIGV']);
            $mov->__SET('dPedTotal', $_POST['dMovTotal']);
            $mov->__SET('dPedSaldo', $_POST['dMovSaldo']);
            $mov->__SET('sPedObservaciones', $_POST['sMovObservacion']);
            $mov->__SET('nPedEstado', 1);
            $mov->__SET('dPedEliminado', 0);
            $mov->__SET('dPedFecha_Act', date('Ymdhis'));
            $mov->__SET('Usuario_Id', $_SESSION['usu_codigo']);
            $mov->__SET('Pedido_Id', $_POST['OrdenCompra_Id']);

            if ($_POST['Movimiento_Id'] != 0) {
                $this->movimiento_model->Editar($mov);//PARA ACTUALIZAR

            } else {
                if($_POST['OrdenCompra_Id'] != 0){
                    $this->ordencompra_model->Editar($mov);
                    $Pedido_Id = $_POST['OrdenCompra_Id'];
                }
                else{
                $Pedido_Id = $this->ordencompra_model->Guardar($mov);
                }
                if (isset($_SESSION['producto_c'])) {
                    $data = $_SESSION['producto_c'];
                    for ($i = 0; $i < count($data); $i++) {
    //                        echo "<h1>$Movimiento_Id</h1><br>";
                        $movdet = new Ordencompradetalle();
                        $movdet->__SET('Movimiento_Id', $Pedido_Id);
                        $movdet->__SET('ProdServ_Id', $data[$i]['ProdServ_Id']);
                        //$movdet->__SET('Pedidodetalle_Id', $data[$i]['Pedidodetalle_Id']);
                        $movdet->__SET('UnidadMedida_Id', (int)$data[$i]['Unidadmedida_Id']);
                        $movdet->__SET('nPedDetCantidad', $data[$i]['sMovDetConCantidad']);
                        $movdet->__SET('dPedDetPrecioUnitario', $data[$i]['nMovDetConPrecioUnitario']);
                        $movdet->__SET('dPedDetSubtotal', $data[$i]['nMovDetConSubtotal']);;
                        if($_POST['OrdenCompra_Id'] != 0){
                                  $this->ordencompradetalle_model->Editar($movdet);
                        }
                        else{
                              $this->ordencompradetalle_model->Guardar($movdet);
                        }
                    }
                }
            }

            //if($_POST['OrdenCompra_Id'] != 0 ? $this->ordencompra_model->Editar($mov) : $this->ordencompra_model->Crear($mov));
            //if($_POST['OrdenCompra_Id'] != 0 ? $this->ordencompradetalle_model->Editar($movdet) : $this->ordencompradetalle_model->Crear($movdet));
            $this->DestruirSessiones();
            echo '<div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Exito información guardado</strong>, los datos fueron registrados correctamente.
                      </div>';
        }

        function DestruirSessiones() {
            unset($_SESSION['producto_c']);
        }

        public function EliminarOrdenCompra(){
            $ordenC = new Ordencompra();
            $ordenC->__SET('Pedido_Id', $_POST['IdMovimiento']);
            $this->ordencompra_model->Eliminar($ordenC);
            echo '<script type="text/javascript">window.location="?c=Ordencompra&a=Index";</script>';
        }

        public function PDFOrdenCompra(){
            $datos= $this->ordencompra_model->Buscar($_POST['Pedido_Id']);
            $detalles = $this->Detalles($_POST['Pedido_Id']);
            require_once 'view/webforms/PDF/ordencompra.php';
        }
    }
?>
