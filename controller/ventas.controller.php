<?php

require_once 'model/usuario.entidad.php';
require_once 'model/usuario.model.php';
require_once 'model/parametro.model.php';
require_once 'model/parametro.entidad.php';
require_once 'model/origendestino.model.php';
require_once 'model/origendestino.entidad.php';
require_once 'model/familia.model.php';
require_once 'model/familia.entidad.php';
require_once 'model/prodserv.model.php';
require_once 'model/prodserv.entidad.php';
require_once 'model/unidadmedida.model.php';
require_once 'model/unidadmedida.entidad.php';
require_once 'model/pedido.entidad.php';
require_once 'model/pedido.model.php';
require_once 'model/pedido_detalle.entidad.php';
require_once 'model/pedido_detalle.model.php';
require_once 'model/documento.entidad.php';
require_once 'model/documento.model.php';
require_once 'model/movimiento.entidad.php';
require_once 'model/movimiento.model.php';
require_once 'model/documento.entidad.php';
require_once 'model/documento.model.php';
require_once 'controller/metodos.controller.php';
class VentasController extends MetodosController{

    private $familia_model;
    private $usuario_model;
    private $parametro_model;
    public $origendestino_model;
    public $prodserv_model;
    private $unidadmedida_model;
    private $pedido_detalle_model;
    private $pedido_model;
    private $documento_model;
    private $movimiento_model;
    private $IGV = 0;
    
    public function __CONSTRUCT() {
        $this->familia_model = new FamiliaModel();
        $this->usuario_model = new UsuarioModel();
        $this->parametro_model = new ParametroModel();
        $this->origendestino_model = new OrigendestinoModel();
        $this->prodserv_model = new ProdservModel();
        $this->unidadmedida_model = new unidadmedidaModel();
        $this->pedido_detalle_model = new Pedido_detalleModel();
        $this->pedido_model = new PedidoModel();
        $this->documento_model = new DocumentoModel();
        $this->movimiento_model = new MovimientoModel();
        foreach ($this->parametro_model->ListarParametros() as $pa) {
            if ($pa->__GET('Parametro_Id') == 2) {
                $this->IGV = ($pa->__GET('sParValor') + 100) / 100;
            }
        }
    }

    public function Index() {
        $familia = $this->familia_model->Hijos(2);
        $parametro = $this->parametro_model->ListarParametros();
        $usuarios = $this->usuario_model->Listar();
        $pedidos = $this->pedido_model->Listar();
        require_once 'view/header.php';
        require_once 'view/webforms/wfaventas.php';
        echo "<script type='text/javascript'>
                        $(document).ready(function (){
                            $('.operaciones').addClass('active');
                        });
                  </script>";
        require_once 'view/footer.php';
    }
    public function ProdServListVenta(){
        $prodserv = $this->prodserv_model->Listar();        
        require_once 'view/webforms/prodserv/prodserv.php';
    }
    public function AgregarProducto(){
        $dPedDetPrecioUnitario = 0;
        echo $_POST['nIncluyeLoteIGV'];
        if($_POST['nIncluyeLoteIGV'] == 1){
            $dPedDetPrecioUnitario = $_POST['dPedDetPrecioUnitario'] / $this->IGV ;
        }else{
            $dPedDetPrecioUnitario = $_POST['dPedDetPrecioUnitario'];
        }
        $data = isset($_SESSION['productos_v']) ? $_SESSION['productos_v']:array();
        $array = array( "ProdServ_Id"             =>  $_POST['ProdServ_Id'],
                        "UnidadMedida_Id"         => $_POST['Unidadmedida_Id'],
                        "nPedDetCantidad"         => $_POST['nPedDetCantidad'],
                        "dPedDetPrecioUnitario"   => $dPedDetPrecioUnitario,
                        "dPedDetSubtotal"         => $_POST['nPedDetCantidad']*$dPedDetPrecioUnitario);
        if(isset($_SESSION['productos_v'])){
            array_push($data, $array);
        }else{
            $data[] = $array;
        }
        $_SESSION['productos_v'] = $data;
    }
    public function ListarProducto() {
        $data = isset($_SESSION['productos_v']) ? $_SESSION['productos_v']:array();
        if (count($data)>0) {
            $data = $_SESSION['productos_v'];
            for ($i = 0; $i < count($data); $i++) {
                echo "<tr>
                            <td>" . ($this->ProdservdNombre($data[$i]['ProdServ_Id'])) . "</td>
                            <td>" . $this->UnidadNombre($data[$i]['UnidadMedida_Id']) . "</td>
                            <td>" . $data[$i]['nPedDetCantidad'] . "</td>
                            <td>" . number_format($data[$i]['dPedDetPrecioUnitario'], 2, ".", ".") . "</td>
                            <td>" . number_format($data[$i]['dPedDetSubtotal'], 2, ".", ".") . "</td>
                            <td>
                                <a href=\"#\" class=\"btn btn-danger btn-flat btn-sm\" onclick=\"EliminarProducto('" . $i . "')\"><i class=\"fa fa-trash-o\"></i></a>                                            
                            </td>
                        </tr>";
            }
        } 
    }
    public function EliminarProducto(){
        $array = array();
        $data = $_SESSION['productos_v'];
        for($i=0;$i<count($data);$i++){
            if($i!=$_POST['index']){
                $array[] = array("ProdServ_Id"                  => $data[$i]['ProdServ_Id'],
                                "nPedDetCantidad"               => $data[$i]['nPedDetCantidad'],
                                "UnidadMedida_Id"               => $data[$i]['UnidadMedida_Id'],
                                "dPedDetPrecioUnitario"         => $data[$i]['dPedDetPrecioUnitario'],
                                "dPedDetSubtotal"               => ($data[$i]['nPedDetCantidad']*$data[$i]['dPedDetPrecioUnitario']));
            }
        }
        $_SESSION['productos_v'] = $array;
    }   
    public function CalcularImplIGVTot(){
        $afecto_IGV = $this->documento_model->Buscar($_POST['Documento_Id']);
        $afecto_IGV = $afecto_IGV[0]->nDocAfectoIGV;
//        $valor_IGV = $this->parametro_model->ListarParametros();
//        $IGV = $afecto_IGV == 1 ? 1+($valor_IGV[1]->sParValor/100) : 0;
        $total = 0;
        $importes = 0;
        $subtotal = 0;
        if(isset($_SESSION['productos_v'])){
            $data = isset($_SESSION['productos_v']) ? $_SESSION['productos_v']:array();
            for($i=0;$i<count($data);$i++){
                $subtotal = $subtotal + $data[$i]['dPedDetSubtotal'];
            }
        }      
        
        if($afecto_IGV == 1){
            $importes = $subtotal;
            $IGV = ($subtotal*$this->IGV) - $importes;
            $total = $importes*$this->IGV;
        }else{
            $importes = $subtotal;
            $IGV = 0;
            $total = $subtotal;
        }
        echo "<script>
                document.getElementById('dPedImporte').value = ".number_format($importes, 2, ".", ".").";
                document.getElementById('dPedIGV').value = ".number_format($IGV, 2, ".", ".").";
                document.getElementById('dPedTotal').value = ".number_format($total, 2, ".", ".").";
            </script>";
    }
    public function UnidadNombre($Unidadmedida_Id){
        $dato = $this->unidadmedida_model->Buscar($Unidadmedida_Id);
        foreach ($dato as $d){
            return $d->__GET('sUndAlias');
        }
    }  
    public function ProdservdNombre($ProdServ_Id){
        $data = $this->prodserv_model->Buscar($ProdServ_Id);
        foreach ($data as $pro){
            return $pro->__GET('sProSrvNombre');
        }
    }  
    
    public function ListaPedidos(){ 
        echo '
            <div class="small-box bg-primary no-padding no-margin">
                <div class="inner">
                    <h3>';
                $o=0;
                foreach ($this->pedido_model->Listar() as $cont){
                    $o++;
                }
                echo $o;
                echo '</h3>
                    <p>PEDIDOS</p>
                </div>
                <div class="icon">
                    <i class="fa fa-shopping-cart"></i>
                </div>';  
        $i=0;
        foreach ($this->pedido_model->Listar() as $r){
            echo '<button type="button" class="close btn_eliminar_pedido" aria-label="Close" data-pedidocodigo="'.$r->__GET('Pedido_Id').'"  data-toggle="modal" data-target="#eliminar_pedido"><span aria-hidden="true">&times;</span></button>                
                  <a href="#" class="small-box-footer text-bold" style="border-bottom: 1px solid #0770BB; font-size: 25px; padding-top: 13px; padding-bottom: 12px;" onclick="EditarPedido(\''.$r->__GET('Pedido_Id').'\');">                                    
                    <i class="fa fa-shopping-cart"></i>&nbsp;
                    <p class="vnombre_cliente">'.$this->NombreClienteProveedor($r->__GET('OrigenDestino_Id')).'</p>
                  </a>';                           
            $i++;
        }
        $total = 10 - $i;
        for($j=1;$j<=$total;$j++){
            if($j==1){
                echo '<a href="#" class="text-bold btn btn-success" onclick="NuevoPedido();" style="width: 100%; padding-top: 20px; padding-bottom: 19px;">
                        <i class="fa fa-plus"></i>
                    </a>';
            }else{                       
                echo '<a class="small-box-footer table-bordered text-bold" style="background: #F9F9F9; color: #999999">
                        ?
                    </a>';
            }
        }
        echo '</div>';
    }
    public function PrintVentas(){        
        $parametro = $this->parametro_model->ListarParametros();
        $pedido = $this->pedido_model->Buscar($_GET['Pedido']);        
        require_once 'view/header.php';
        require_once 'view/webreport/wfpventas.php';
        require_once 'view/footer.php';
    }
    public function ObtenerNumeroDocumento($Movimiento_Id){
        $documento_nr = $this->movimiento_model->Buscar($Movimiento_Id);
        $nro_doc = '';
        foreach ($documento_nr as $r){
            $nro_doc = $r->__GET('sMovDocumento');
        }
        return $nro_doc;
    }
    public function ObtenerNombreDocumento($Documento_Id){
        $documentos = $this->documento_model->Buscar($Documento_Id);
        $doc_name = '';
        foreach ($documentos as $d){
            $doc_name = $d->__GET('sDocNombre');
        }
        return $doc_name;
    }
}

?>