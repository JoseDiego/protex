<?php
    require_once 'model/reportes.entidad.php';
    require_once 'model/reportes.model.php';
    require_once 'model/receta.entidad.php';
    require_once 'model/receta.model.php';

    require_once 'model/parametro.model.php';
    require_once 'model/parametro.entidad.php';
    require_once 'model/documento.model.php';
    require_once 'model/documento.entidad.php';
    require_once 'model/unidadmedida.model.php';
    require_once 'model/unidadmedida.entidad.php';
    require_once 'model/familia.model.php';
    require_once 'model/familia.entidad.php';
    require_once 'model/prodserv.model.php';
    require_once 'model/prodserv.entidad.php';
    require_once 'model/activo.model.php';
    require_once 'model/activo.entidad.php';
    require_once 'model/precioventa.model.php';
    require_once 'model/precioventa.entidad.php';
    require_once 'controller/receta.controller.php';
    require_once 'controller/metodos.controller.php';
    require_once 'model/origendestino.entidad.php';
    require_once 'model/origendestino.model.php';
    require_once 'controller/clienteproveedor.controller.php';
    require_once 'model/ordencompra.model.php';
    require_once 'model/ordencompra.entidad.php';
    require_once 'model/ordencompradetalle.model.php';
    require_once 'model/ordencompradetalle.entidad.php';
    require_once 'model/ordentrabajo.model.php';
    require_once 'model/ordentrabajo.entidad.php';
    require_once 'model/ordentrabajodetalle.model.php';
    require_once 'model/ordentrabajodetalle.entidad.php';
    require_once 'model/pedido.entidad.php';
    require_once 'model/pedido.model.php';


	class OrdenTrabajoController extends MetodosController{
        private $parametro_model;
        private $documento_model;
        public $unidadmedida_model;
        private $familia_model;
        private $prodserv_model;
        private $activo_model;
        public $precioventa_model;
        private $receta_controller;
        private $clienteproveedor_controller;
        private $origendestino_model;
        private $ordencompra_model;
        private $ordencompradetalle_model;
        private $ordentrabajo_model;
        private $ordentrabajodetalle_model;
        private $pedido_model;

        private $reportes_model;
        private $receta_model;

        public function __CONSTRUCT(){
            $this->reportes_model = new ReportesModel();
            $this->receta_model = new RecetaModel();

            $this->parametro_model = new ParametroModel();
            $this->documento_model = new DocumentoModel();
            $this->unidadmedida_model = new UnidadmedidaModel();
            $this->familia_model = new FamiliaModel();
            $this->prodserv_model = new ProdservModel();
            $this->activo_model = new ActivoModel();
            $this->precioventa_model = new PrecioventaModel();
            $this->receta_controller = new RecetaController();
            $this->origendestino_model = new OrigendestinoModel();
            $this->ordencompra_model = new OrdencompraModel();
            $this->ordencompradetalle_model = new OrdencompradetalleModel();
            $this->ordentrabajo_model = new OrdenTrabajoModel();
            $this->ordentrabajodetalle_model = new OrdenTrabajoDetalleModel();
            $this->pedido_model = new PedidoModel();
        }

        public function Index(){
            $parametro = $this->parametro_model->ListarParametros();
            $productos = $this->prodserv_model->Listar();
            $ordentrabajo= $this->ordentrabajo_model->Listar();
            require_once 'view/header.php';
            require_once 'view/webforms/wfaordentrabajo.php';
            echo "<script type='text/javascript'>
                        $(document).ready(function (){
                            $('.operaciones').addClass('active');
                        });
                  </script>";
            require_once 'view/footer.php';
        }


        public function NuevaOrdenTrabajo() {

			require_once 'view/webforms/wfaordentrabajoa.php';
		}

        public function ListarFechasO(){
            $orden = $this->ordentrabajo_model->ListarFechas($_POST['fInicio'],$_POST['fFin']);
        $_SESSION['$orden'] = $orden;
           if (empty($orden)) {
                echo '<tr><td colspan="8"><h4>No existen Ordenes de Trabajo del  '.$_POST['fInicio']. ' al ' .$_POST['fFin'].'</h4></td></tr>';
            }
            else{
            foreach ($orden as $oc) {

                $estado = $oc->__GET('otEstado');
                    if($estado == 1){
                        $valor = '<span class="label label-warning">En Espera</span>';
                    }
                    else{
                      if($estado == 2){
                        $valor = '<span class="label label-success">En Proceso</span>';
                       }
                      else{
                          $valor = '<span class="label label-danger">Terminado</span>';
                      }
                    }
                echo '<tr>
                          <td>'.$oc->__GET('OrdenTrabajo_Id').'</td>
                          <td>'.$oc->__GET('otFecha').'</td>
                          <td>'.$oc->__GET('otNroOrdenTrabajo').'</td>
                          <td>'.$oc->__GET('sODNombre').'</td>
                          <td>'.$oc->__GET('otFechaE').'</td>
                          <td>'.$oc->__GET('otNroOrdenCompra').'</td>
                          <td>'.$valor.'</td>
                           <td>
                                            <a  class="btn btn-primary btn-flat btn-xs" data-toggle="modal" data-target="#modal-ver" onclick="mostrarOrdenCompra('. $oc->__GET('OrdenTrabajo_Id').');"><i class="glyphicon glyphicon-eye-open"></i></a>
                                            <a href="#" class="btn btn-success btn-flat btn-xs" onclick="FrmEditarOrdenTrabajo('.$oc->__GET('OrdenTrabajo_Id').');"><i class="fa fa-pencil"></i></a>
                                            <a href="#" class="btn btn-danger btn-flat btn-xs" data-toggle="modal" data-target="#modal-eliminar" data-codigo="'.$oc->__GET('OrdenTrabajo_Id').'"><i class="fa fa-trash-o"></i></a>
                                        </td>
                     </tr>';
            }

           }
        }

        public function FrmEditarOrdenTrabajo(){
            $datos = $this->ordentrabajo_model->Buscar($_POST['OrdenTrabajo_Id']);
            //$detalle = $this->ordentrabajodetalle_model->Listar($_POST['OrdenTrabajo_Id']);
            //$_SESSION['detalle'] = $detalle;

            foreach ($datos as $r):
                require_once 'view/webforms/wfaordentrabajoa.php';
            endforeach;
           // foreach ($detalle as $det):
           //     require_once 'view/webforms/wfaordencompraa.php';
           // endforeach;
        }
        public function ListarDetallesOC() {
            $detalle = $this->ordencompradetalle_model->Buscar($_POST['Pedido_Id']);
            //$data = array();
            //$_SESSION['producto_c']=$detalle;

            if (isset($detalle)) {
                $i=0;
                foreach ($detalle as $det){

                   echo "<tr>
                                <td>".$det->__GET('OrdenTrabajo_detalle')."</td>
                                <td>" . $this->ProdservdNombre($det->__GET('ProdServ_Id')) . "</td>
                                <td>" . $this->UnidadNombre($det->__GET('UnidadMedida_Id')) . "</td>
                                <td><center><input type='text' class='form-control' name='cant' id='cant' placeholder='0' disabled='true' style='width:100px;height:25px;' value='".$det->__GET('nPedDetCantidad')."'></center></td>
                                <td><center><input type='number' class='form-control' onkeyup='calculo()' name='cantProg' id='cantProg' placeholder='0' style='width:100px;height:25px;'></center></td>
                                <td><center><input type='text' class='form-control' name='cantRest' id='cantRest' placeholder='0' disabled='true' style='width:100px;height:25px;'></center></td>

                            </tr>";
                   $i++;
                }

            } else {

                echo "<tr>
                            <td colspan='6'>No hay datos</td>
                          </tr>";
            }

        }

        public function CalculoRest() {
            $cant = (float)$_POST['cant'];
            $cantProg = (float)$_POST['cantProg'];
            $result = $cant - $cantProg;
            echo $result;

        }

        public function ListarOrdenC() {
            $ordenes = $this->ordencompra_model->ListarConOrdenTrabajo();
            // $ordenes = $this->ordencompra_model->Listar();
            $_SESSION['$ordenes'] = $ordenes;
            echo "<option value='0'>Seleccione Orden</option>";

            foreach ($ordenes as $or) {
                if($or->__GET('OrigenDestino_Id')== $_POST['Cliente_Id']){
                    echo "<option value='" . $or->__GET('Pedido_Id') . "'>" . $or->__GET('nroOrdenTrabajo') . "</option>";
                }
            }

            }

        public function ConfigurarPrecio(){
            $parametro = $this->parametro_model->ListarParametros();
            $productos = $this->prodserv_model->Listar();
            require_once 'view/header.php';
            require_once 'view/webforms/wfaconfigurarprecio.php';
            echo "<script type='text/javascript'>
                        $(document).ready(function (){
                            $('.operaciones').addClass('active');
                        });
                  </script>";
            require_once 'view/footer.php';
        }
        public function GuardarProdserv(){
            $pro = new Prodserv();
            if($_POST['ProdServ_Id'] != 0 ? $pro->__SET('ProdServ_Id', $_POST['ProdServ_Id']) : '');
            $pro->__SET('sProSrvCodigo', $_POST['sProSrvCodigo']);
            $pro->__SET('sProSrvNombre', $_POST['sProSrvNombre']);
            $pro->__SET('sProSrvCodigoBarras', $_POST['sProSrvCodigoBarras']);
            $pro->__SET('nProSrvFamilia_Id', $_POST['Familia_Id']);
            $pro->__SET('Activo_Id', $_POST['Activo_Id']);
            $pro->__SET('nProSrvUnidad_Id', $_POST['Unidadmedida_Id']);
            $pro->__SET('nProSrvExoneradoIGV', $_POST['nProSrvExoneradoIGV']);
            $pro->__SET('nProSrvEstado', $_POST['nProSrvEstado']);
            $pro->__SET('nProSrvEliminado', 0);
            $pro->__SET('Usuario_Id', $_SESSION['usu_codigo']);
            if($_POST['ProdServ_Id'] != 0){
                $this->prodserv_model->Editar($pro);
            }else{
                $ProdServ_Id = $this->prodserv_model->guardar($pro);
                echo '<div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Exito información guardado</strong>, los datos fueron registrados correctamente.
                      </div>';
            }
            $this->receta_controller->EliminarReccetasSeleccionadas();
            $this->receta_controller->GuardarReceta($_POST['ProdServ_Id'] == 0 ? $ProdServ_Id:$_POST['ProdServ_Id']);
            $this->EliminarSession();
        }
        public function FrmEditarProductos(){
            $this->EliminarSession();
            $productos = $this->prodserv_model->Listar();
            $familia = $this->familia_model->Listar();
            $unidadmedida = $this->unidadmedida_model->Listar();
            $activos = $this->activo_model->Listar();
            $datos= $this->prodserv_model->Buscar($_POST['ProdServ_Id']);
            $this->receta_controller->ListaEditar($_POST['ProdServ_Id']);
            foreach ($datos as $r):
                require_once 'view/webforms/wfaproductosa.php';
            endforeach;
        }
        public function FrmGuardarProductos(){
            $this->EliminarSession();
            $productos = $this->prodserv_model->Listar();
            $familia = $this->familia_model->Listar();
            $unidadmedida = $this->unidadmedida_model->Listar();
            $activos = $this->activo_model->Listar();
            require_once 'view/webforms/wfaproductosa.php';
        }
        public function MostrarProducto(){
            $producto = $this->prodserv_model->Buscar($_POST['ProdServ_Id']);
            foreach ($producto as $r):
                echo " <tbody>
                          <tr>
                             <td>
                                <input type='hidden' name='ProdServ_IdPri' id='ProdServ_IdPri' value='".$r->__GET('ProdServ_Id')."'>
                                Codigo
                             </td>
                             <td>".$r->__GET('sProSrvCodigo')."</td>
                          </tr>
                          <tr>
                             <td>Nombre</td>
                             <td>".$r->__GET('sProSrvNombre')."</td>
                          </tr>
                          <tr>
                             <td>Codigo barra</td>
                             <td>".$r->__GET('sProSrvCodigoBarras')."</td>
                          </tr>
                          <tr>
                             <td>Activo</td>
                             <td>".$r->__GET('sActDescripcion')."</td>
                          </tr>
                          <tr>
                             <td>Unidad de medida</td>
                             <td>".$r->__GET('sUndDescripcion')."</td>
                          </tr>
                          <tr>
                             <td>Exonerado de IGV</td>
                             <td>".($r->__GET('nProSrvExoneradoIGV') == 1 ? 'Si' :'No')."</td>
                          </tr>
                          <tr>
                             <td>Estado</td>
                             <td>".($r->__GET('nProSrvEstado') == '1' ? '<span class="label bg-green">Habilitado</span>' : '<span class="label bg-danger">Inhabilitado</span>')."</td>
                          </tr>
                       </tbody>
                     ";
            endforeach;
        }
        public function EstadoPrecioUnidad($ProdServ_Id, $Unidadmedida_Id){
            $estado = false;
            $precio = $this->precioventa_model->Precio($ProdServ_Id, $Unidadmedida_Id);
            if(count($precio)>0){
                $estado = true;
            }
            return $estado;
        }
        public function ListarUnidades(){
            $unidad = $this->unidadmedida_model->ListarUP($this->ObtenerID($_POST['ProdServ_Id']));
            echo "<option value=''>Seleccione unidad</option>";
            foreach ($unidad as $uni){
                if($this->EstadoPrecioUnidad($_POST['ProdServ_Id'], $uni->__GET('UnidadMedida_Id')) == true){
                    echo "<option value='".$uni->__GET('UnidadMedida_Id')."'>".$uni->__GET('sUndDescripcion')."</option>";
                }
            }
        }
        public function FiltroProducto(){
            if(!empty($_POST['txtNameProduct'])){
                $productos = $this->prodserv_model->Filtrar($_POST['txtNameProduct']);
                foreach ($productos as $p){
                    echo '<div class="col-md-4 margin-bottom">
                            <button class="btn '.($p->__GET('nProSrvEstado')==0 ? 'btn-danger disabled':'btn-success').' btn-flat btn-producto" style="width: 100%; height: 120px;" onclick="producto_detalle(\''.$p->__GET('ProdServ_Id').'\')" data-dismiss="modal" data-toggle="modal" data-target="#productos_detalle">
                                <i class="fa fa-shopping-cart"></i><br>
                                <p style="font-size: 14px; width: 100%; display: table;">'.$p->__GET('sProSrvNombre').'</p>
                            </button>
                        </div>';
                }
            }else{
                foreach ($this->familia_model->Hijos(2) as $f) {
                    echo '<div class="col-md-4 margin-bottom">
                            <button class="btn '.($f->__GET('nFamEstado') == 0 ? 'btn-danger disabled' : 'btn-warning').' btn-flat btn-producto" style="width: 100%;" onclick="FiltrarProductos(\''.$f->__GET('Familia_Id').'\');" data-dismiss="modal" data-toggle="modal" data-target="#productos">
                                <i class="fa fa-info-circle"></i><br>
                                <p style="font-size: 14px;">'.$f->__GET('sFamDescripcion').'</p>
                            </button>
                        </div>';
                }
            }
        }
        public function ObtenerID($ProdServ_Id){
            foreach ($this->prodserv_model->Buscar($ProdServ_Id) as $p){
                return $p->__GET('nProSrvUnidad_Id');
            }
        }

        public function EliminarSession(){
            unset($_SESSION['recetas']);
            unset($_SESSION['recetas_delete']);
        }
        public function UltimoCodigo(){
            echo !empty($_POST['Familia_Id']) ? $this->prodserv_model->UltimoCodigo($_POST['Familia_Id']) : '0-0';
        }

        public function VerificarStock(){
            print_r(json_encode($this->prodserv_model->VerificarStock($_POST['ProdServ_Id'],$_POST['nPedDetCantidad'],$_POST['Unidadmedida_Id'])));
        }


        public function GuardarOrdenTrabajo() {
            $mov = new OrdenTrabajo();

            $mov->__SET('otFecha', $_POST['otFecha']);
            $mov->__SET('otNroOrdenCompra', $_POST['ordCompra']);
            $mov->__SET('otCliente_Id', $_POST['Cliente_Id']);
            $mov->__SET('otNroOrdenTrabajo', $_POST['nroOrdenTrabajo']);
            $mov->__SET('otFechaE', $_POST['otFechaE']);
            $mov->__SET('otEstado', 1);
            $mov->__SET('otEliminado', 0);
            $mov->__SET('Usuario_Id', $_SESSION['usu_codigo']);
            $mov->__SET('OrdenTrabajo_Id', $_POST['OrdenTrabajo_Id']);
            $nroOrdenT= $mov->__GET('otNroOrdenTrabajo');
             if($_POST['OrdenTrabajo_Id'] != 0){
                    $this->ordentrabajo_model->Editar($mov);
                  $OrdenTrabajo_Id = $_POST['OrdenTrabajo_Id'];
                }
                else{
                     $OrdenTrabajo_Id = $this->ordentrabajo_model->Guardar($mov);
                }

            if (isset($_POST['jsonArray'])) {
                    $data = json_decode($_POST['jsonArray']);


                    for ($i = 0; $i < count($data); $i++) {
    //                        echo "<h1>$Movimiento_Id</h1><br>";
                        $movdet = new OrdenTrabajoDetalle();
                        $movdet->__SET('OrdenTrabajo_Id', $OrdenTrabajo_Id);
                        $movdet->__SET('ProdServ_Id', $data[$i]->ProdServ_Id);
                        $movdet->__SET('UnidadMedida_Id', $data[$i]->UnidadMedida_Id);
                        $movdet->__SET('CantidadAsig', $data[$i]->nPedDetCantidad);
                        $movdet->__SET('CantidadProg', $data[$i]->nOTCantidadProg);
                        $movdet->__SET('CantidadRest', $data[$i]->nOTCantidadRest);
                        if($_POST['OrdenTrabajo_Id'] != 0){
                            $this->ordentrabajodetalle_model->Editar($movdet);
                        }
                        else{
                            $this->ordentrabajodetalle_model->Guardar($movdet);

                            $cantidadAsig = $movdet->__GET('CantidadAsig');
                            $cantidadRest = $movdet->__GET('CantidadRest');
                            $this->Estado($nroOrdenT, $cantidadAsig, $cantidadRest);

                        }
                    }
                }

                unset($data);
            echo '<div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Exito información guardado</strong>, los datos fueron registrados correctamente.
                      </div>';
        }

    public function Estado($nroOrdenT,$cantidadAsig,$cantidadRest){
        if($cantidadRest < $cantidadAsig){
                                $ordenC = $this->ordencompra_model->BuscarO($nroOrdenT);
                                $Estado = 2;
                                $Orden = $ordenC->__GET('Pedido_Id');
                                $this->ordencompra_model->OrdenEstado($Orden, $Estado);

                            }
                            else{}
    }

    public function EliminarOrdenTrabajo(){
            $ordenC = new OrdenTrabajo();
            $ordenC->__SET('OrdenTrabajo_Id', $_POST['IdMovimiento']);
            $this->ordentrabajo_model->Eliminar($ordenC);
            echo '<script type="text/javascript">window.location="?c=OrdenTrabajo&a=Index";</script>';
        }

    public function MostrarOrdenTrabajo(){
            $ordentrabajo= $this->ordentrabajo_model->Buscar($_POST['OrdenTrabajo_Id']);
            $OrdenTrabajo_Id = $_POST['OrdenTrabajo_Id'];
            foreach ($ordentrabajo as $r):
                if($r->__GET('otEliminado')==1){ $del='Si';}else{$del='No';}
                 $estado = $r->__GET('otEstado');
                    if($estado == 1){$valor = '<span class="label label-warning">En Espera</span>';}
                    else{
                      if($estado == 2){$valor = '<span class="label label-success">En Proceso</span>';}
                      else{$valor = '<span class="label label-danger">Terminado</span>';}
                    }
                echo " <tbody>
                          <tr>
                             <td>
                                <input type='hidden' name='OrdenTrabajo_Id' id='OrdenTrabajo_Id' value='".$r->__GET('OrdenTrabajo_Id')."'>
                                Nro Orden Trabajo
                             </td>
                             <td>".$r->__GET('otNroOrdenTrabajo')."</td>
                          </tr>
                          <tr>
                             <td>Fecha</td>
                             <td>".date('d/m/Y', strtotime($r->__GET('otFecha')))."</td>
                          </tr>
                          <tr>
                             <td>Nro Orden Compra</td>
                             <td>".$r->__GET('otNroOrdenCompra')."</td>
                          </tr>
                          <tr>
                             <td>Cliente</td>
                             <td>".$r->__GET('sODNombre')."</td>
                          </tr>
                          <tr>
                             <td>Fecha de Entrega</td>
                             <td>".date('d/m/Y', strtotime($r->__GET('otFechaE')))."</td>
                          </tr>
                          <tr>
                             <td>Estado</td>
                             <td>".$valor."</td>
                          </tr>
                          <tr>
                             <td>Eliminado</td>
                             <td>".$del."</td>
                          </tr>
                          <tr>
                             <td>Fecha de Actualizacion</td>
                             <td>".date('d/m/Y h:m:s', strtotime($r->__GET('otFecha_Act')))."</td>
                          </tr>

                          <tr><td colspan='2'>
                          <table class='table table-bordered table-striped text-center'>
                                            <thead>
                                                <tr>
                                                  <th>Producto</th>
                                                  <th>Unidad</th>
                                                  <th>Cantidad Asig.</th>
                                                  <th>Cantidad Prog.</th>
                                                  <th>Cantidad Rest.</th>
                                                </tr>
                                            </thead>
                                            <tbody id='productos_servicios'>


                                                    ". $this->Detalles($OrdenTrabajo_Id)."

                                            </tbody>
                                        </table>
                          </td></tr>
                       </tbody>
                     ";
            endforeach;
        }

    public function Detalles($OrdenTrabajo_Id){
            $ordentrabajodetalle = $this->ordentrabajodetalle_model->Listar($OrdenTrabajo_Id);
            $result = '';
             foreach($ordentrabajodetalle as $otd) {
                    $result .= "<tr>
                                <td>" . $otd->__GET('sProvSrvNombre') . "</td>
                                <td>" . $otd->__GET('sUndAlias') . "</td>
                                <td>" . number_format($otd->__GET('CantidadAsig'), 2, ".", ".") . "</td>
                                <td>" . number_format($otd->__GET('CantidadProg'), 2, ".", ".") . "</td>
                                <td>" . number_format($otd->__GET('CantidadRest'), 2, ".", ".") . "</td>
                                </tr>";

             }

             if ($result == '') {
               $result = '<tr><td colspan="5">No hay datos</td></tr>';
             }
             return $result;
        }

        public function ProdservdNombre($ProdServ_Id) {
            $data = $this->prodserv_model->Buscar($ProdServ_Id);
            foreach ($data as $pro) {
                return $pro->__GET('sProSrvNombre');
            }
        }
        public function validarStock(){
            if (isset($_POST['jsonArray'])) {
                    $data = json_decode($_POST['jsonArray']);
                    $reportes = $this->reportes_model->Stock();
                     $result = '';
                    $prod = array();
                    for ($i = 0; $i < count($data); $i++) {
                        $movdet = new OrdenTrabajoDetalle();
                        $movdet->__SET('ProdServ_Id', $data[$i]->ProdServ_Id);
                        $movdet->__SET('sProSrvNombre', $data[$i]->ProdservdNombre);
                        $movdet->__SET('sUndAlias', $data[$i]->UnidadNombre);
                        $movdet->__SET('CantidadAsig', $data[$i]->nPedDetCantidad);
                        $movdet->__SET('CantidadProg', $data[$i]->nOTCantidadProg);
                        $movdet->__SET('CantidadRest', $data[$i]->nOTCantidadRest);
                        $receta = $this->receta_model->Buscar($data[$i]->ProdServ_Id);
                        if(count($receta)>0){

                       foreach ($reportes as $r){

                                     $sProd = $r->__GET('valor03');
                                    $sUnd = $r->__GET('valor05');
                                    $sStock = $r->__GET('valor04');
                        for($a=0;$a<count($receta);$a++){
                            $rProd = $this->ProdservdNombre($receta[$a]['ProdServ_Id']);
                            $rUnd = $this->NombreUnidadMedidaProducto($receta[$a]['UnidadMedida_Id']);
                            $rCant = $receta[$a]['nRecCantidad'];
                            $cant = $movdet->__GET('CantidadProg');
                            $cantT = $cant * $rCant ;


                            if ($rProd == $sProd){

                                     $prod[]=array('Producto' => $sProd , 'Unidad' => $sUnd , 'Cantidad' => $cantT , 'Stock' => $sStock);
                                    }

                            $receta2 = $this->receta_model->Buscar($receta[$a]['ProdServ_Id']);
                            if(count($receta2) != 0){
                                 for($b=0;$b<count($receta2);$b++){
                                     $r2Prod = $this->ProdservdNombre($receta2[$b]['ProdServ_Id']);
                                     $r2Und = $this->NombreUnidadMedidaProducto($receta2[$b]['UnidadMedida_Id']);
                                     $r2Cant = $receta2[$b]['nRecCantidad'];
                                     $cant2T = $r2Cant * $cantT ;
                                     $receta3 = $this->receta_model->Buscar($receta2[$b]['ProdServ_Id']);
                                     if($cantT <= $sStock){$cant2T=0;}else{$cntT= abs($cantT-$sStock);$cant2T=$cntT*$r2Cant;}
                                     if ($r2Prod == $sProd){
                                      $prod[]=array('Producto' => $sProd , 'Unidad' => $sUnd , 'Cantidad' => $cant2T , 'Stock' => $sStock);

                                     }
                                     if(count($receta3) != 0){
                                      for($c=0;$c<count($receta3);$c++){
                                         $r3Prod = $this->ProdservdNombre($receta3[$c]['ProdServ_Id']);
                                         $r3Und = $this->NombreUnidadMedidaProducto($receta3[$c]['UnidadMedida_Id']);
                                         $r3Cant = $receta3[$c]['nRecCantidad'];
                                         $cant3T = $r3Cant * $cant2T;
                                         if($cant2T <= $sStock){$cant3T=0;}else{$cant3T= abs($cant2T-$sStock);}
                                         if ($r3Prod == $sProd){
                                               $prod[]=array('Producto' => $sProd , 'Unidad' => $sUnd , 'Cantidad' => $cant3T , 'Stock' => $sStock);
                                         }
                                     }}


                                 }


                            }



                        }
                    }
                    }
                 }
                 $union = $this->qd_sd($prod, 'Producto', 'Cantidad');
                 for($b=0;$b<count($union);$b++){
                     $result .= "<tr style='text-align: center;'>
                                                   <td>".$union[$b]['Producto']."</td>
                                                   <td>".$union[$b]['Unidad']."</td>";
                      if($union[$b]['Total'] > $union[$b]['Stock']){
                                 $result.= "<td bgcolor='#FF8585'>".$union[$b]['Total']."</td>
                                            <td>".$union[$b]['Stock']."</td>
                                            </tr>";
                        }
                      else{
                            $result.= "<td>".$union[$b]['Total']."</td>
                            <td>".$union[$b]['Stock']."</td>
                            </tr>";
                          }
                  }
               }
            $_SESSION['Req'] = $result;
            echo $result;
           }




        public function PDFOrdenTrabajo(){
            $datos= $this->ordentrabajo_model->Buscar($_POST['OrdenTrabajo_Id']);
            $detalles = $this->Detalles($_POST['OrdenTrabajo_Id']);
            require_once 'view/webforms/PDF/ordentrabajo.php';
        }

        public function PDFReqMateriales(){
            $datos= $_SESSION['Req'];
            require_once 'view/webforms/PDF/reqMateriales.php';
        }


        function qd_sd($array, $campo, $campo2) {
           $nuevo = array();
           $suma = 0;
             foreach ($array as $parte) {$clave[] = $parte[$campo];}
             $unico = array_unique($clave);
             foreach ($unico as $un) {
             foreach ($array as $original) {
                 $und = $original['Unidad'];
                 $stock = $original['Stock'];
               if ($un == $original[$campo]) {
                 $suma = $suma + $original[$campo2];
               }
            }
          $ele['Producto'] = $un;
          $ele['Unidad'] = $und;
          $ele['Total'] = $suma;
          $ele['Stock'] = $stock;
          array_push($nuevo, $ele);
          $suma = 0;
         }
          return $nuevo;
        }

}
?>
