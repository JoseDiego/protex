<?php    
    require_once 'model/parametro.model.php';
    require_once 'model/parametro.entidad.php';
    require_once 'model/almacen.model.php';
    require_once 'model/almacen.entidad.php';
    class AlmacenController {
        private $parametro_model;
        private $almacen_model;
        public function __CONSTRUCT(){
            $this->parametro_model = new ParametroModel();
            $this->almacen_model = new AlmacenModel();
        }
        
        public function Index(){
            $parametro = $this->parametro_model->ListarParametros();
            $almacen = $this->almacen_model->Listar();
            require_once 'view/header.php';
            require_once 'view/webforms/wfaalmacen.php';
            echo "<script type='text/javascript'>
                        $(document).ready(function (){
                            $('.catalogos').addClass('active');
                        });
                  </script>";
            require_once 'view/footer.php';
        }

        public function FrmAlmacen(){
          $almacen = $this->almacen_model->Listar();
            require_once 'view/webforms/wfaalmacena.php';
        }

        public function GuardarAlmacen(){
            $alm = new Almacen();
            if($_POST['Almacen_Id'] != 0 ? $alm->__SET('Almacen_Id', $_POST['Almacen_Id']) : '');
            $alm->__SET('sAlmNombre', $_POST['sAlmNombre']);
            $alm->__SET('sAlmUbicacion', $_POST['sAlmUbicacion']);
            $alm->__SET('nAlmEstado', $_POST['nAlmEstado']);
            $alm->__SET('nAlmEliminado', 0);
            $alm->__SET('Usuario_Id', $_SESSION['usu_codigo']);
            if($_POST['Almacen_Id'] != 0 ? $this->almacen_model->Editar($alm) : $this->almacen_model->guardar($alm));
        }
        public function MostrarAlmacen(){
            $datos = $this->almacen_model->Buscar($_POST['Almacen_Id']);
            foreach ($datos as $r){
                echo " <tbody>
                              <tr>
                                 <th>
                                    <input type='hidden' name='Almacen_IdPrin' id='Almacen_IdPrin' value='".$r->__GET('Almacen_Id')."'> 
                                    Nombre
                                 </th>
                                 <td>".$r->__GET('sAlmNombre')."</td>
                              </tr>
                              <tr>
                                 <th>Ubicacion</th>
                                 <td>".$r->__GET('sAlmUbicacion')."</td>
                              </tr>  
                              <tr>
                                 <th>Estado</th>
                                 <td>".($r->__GET('nAlmEstado') == '1' ? '<span class="label bg-green">Habilitado</span>' : '<span class="label bg-danger">Inhabilitado</span>')."</td>
                              </tr>
                           </tbody>
                         ";
            }
        }
        public function EliminarAlmacen(){
            $alm = new Almacen();
            $alm->__SET('Almacen_Id', $_POST['idAlmacen']);
            $this->almacen_model->Eliminar($alm);
            echo '<script type="text/javascript">window.location="?c=Almacen&a=Index";</script>';
        }
//        
        public function FrmEditarAlmacen(){
            $obtener = $this->almacen_model->Buscar($_POST['Almacen_Id']);
            foreach ($obtener as $r):
                echo "<script type='text/javascript'>
                        document.getElementById('Almacen_Id').value = '".$r->__GET('Almacen_Id')."';
                        document.getElementById('sAlmNombre').value = '".$r->__GET('sAlmNombre')."';
                        document.getElementById('sAlmUbicacion').value = '".$r->__GET('sAlmUbicacion')."';
                        $('#nAlmEstado option[value=".$r->__GET('nAlmEstado')."]').attr('selected','selected');
                      </script>";          
            endforeach;       
        }
//        public function MostrarUsuario(){
//            $usuarios= $this->usuario_model->Buscar($_POST['Usuario_Id']);
//            foreach ($usuarios as $r):
//                if($r->__GET('nUsuTipo')==1){
//                    $tipo="Administrador";
//                }elseif ($r->__GET('nUsuTipo')==2) {
//                    $tipo="Asistente";
//                }else{
//                    $tipo="Reportes";
//                }
//                echo " <tbody>
//                          <tr>
//                             <td>
//                                <input type='hidden' name='idUsuario' id='idUsuario' value='".$r->__GET('Usuario_Id')."'> 
//                                Nombre de Usuario
//                             </td>
//                             <td>".$r->__GET('sUsuNombre')."</td>
//                          </tr>
//                          <tr>
//                             <td>Login</td>
//                             <td>".$r->__GET('sUsuLogin')."</td>
//                          </tr>  
//                          <tr>
//                             <td>Tipo de Usuario</td>
//                             <td>".$tipo."</td>
//                          </tr>  
//                          <tr>
//                             <td>Estado</td>
//                             <td>".($r->__GET('nUsuEstado') == '1' ? '<span class="label bg-green">Habilitado</span>' : '<span class="label bg-danger">Inhabilitado</span>')."</td>
//                          </tr>
//                       </tbody>
//                     ";
//            endforeach;
//        }
//        public function GuardarUsuario(){            
//            $usu = new Usuario();
//            if($_POST['idRegistro'] != 0 ? $usu->__SET('Usuario_Id',   $_POST['idRegistro']) : '');
//            $usu->__SET('sUsuNombre', $_POST['nombre']);                
//            $usu->__SET('sUsuLogin', $_POST['login']);
//            $usu->__SET('sUsuContrasena', $_POST['contraseñaTwo']);
//            $usu->__SET('nUsuTipo', $_POST['tipo']);
//            $usu->__SET('nUsuEstado', $_POST['estado']);
//            $usu->__SET('nUsuEliminado', 0);
//            $usu->__SET('nUsuUsuarioOwn_Id', $_SESSION['usu_codigo']);
//            if($_POST['idRegistro'] == 0 ? $this->usuario_model->Guardar($usu) : $this->usuario_model->Editar($usu));
//        }
//        public function EliminarUsuario(){
//            $usu = new Usuario();
//            $usu->__SET('Usuario_Id', $_POST['idUsuario']);
//            $this->usuario_model->Eliminar($usu);
//            echo '<script type="text/javascript">window.location="?c=Usuario&a=Index";</script>';
//            
//        }
    }
?>