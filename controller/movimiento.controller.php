<?php

require_once 'model/parametro.model.php';
require_once 'model/parametro.entidad.php';
require_once 'model/documento.model.php';
require_once 'model/documento.entidad.php';
require_once 'model/almacen.model.php';
require_once 'model/almacen.entidad.php';
require_once 'model/tipomovimiento.model.php';
require_once 'model/tipomovimiento.entidad.php';
require_once 'model/movimiento.model.php';
require_once 'model/movimiento.entidad.php';
require_once 'model/prodserv.model.php';
require_once 'model/prodserv.entidad.php';
require_once 'model/familia.model.php';
require_once 'model/familia.entidad.php';
require_once 'model/unidadmedida.model.php';
require_once 'model/unidadmedida.entidad.php';
require_once 'model/productolote.model.php';
require_once 'model/productolote.entidad.php';
require_once 'model/movimientodetalle.model.php';
require_once 'model/movimientodetalle.entidad.php';
require_once 'model/precioventa.model.php';
require_once 'model/precioventa.entidad.php';
require_once 'model/origendestino.entidad.php';
require_once 'model/origendestino.model.php';
require_once 'model/almacen.entidad.php';
require_once 'model/almacen.model.php';
require_once 'controller/metodos.controller.php';

class MovimientoController extends MetodosController {

    private $parametro_model;
    private $documento_model;
    private $almacen_model;
    private $tipomovimiento_model;
    private $movimiento_model;
    private $prodserv_model;
    private $familia_model;
    private $unidadmedida_model;
    public $productolote_model;
    private $movimientodetalle_model;
    public $precioventa_model;
    public $origendestino_model;
    private $IGV = 0;

    public function __CONSTRUCT() {
        $this->parametro_model = new ParametroModel();
        $this->documento_model = new DocumentoModel();
        $this->almacen_model = new AlmacenModel();
        $this->tipomovimiento_model = new TipomovimientoModel();
        $this->movimiento_model = new MovimientoModel();
        $this->prodserv_model = new ProdservModel();
        $this->familia_model = new FamiliaModel();
        $this->unidadmedida_model = new unidadmedidaModel();
        $this->movimientodetalle_model = new MovimientodetalleModel();
        $this->productolote_model = new ProductoloteModel();
        $this->precioventa_model = new PrecioventaModel();
        $this->origendestino_model = new OrigendestinoModel();
        foreach ($this->parametro_model->ListarParametros() as $pa) {
            if ($pa->__GET('Parametro_Id') == 2) {
                $this->IGV = ($pa->__GET('sParValor') + 100) / 100;
            }
        }
    }

    public function Index() {
        $parametro = $this->parametro_model->ListarParametros();
        $movimientos = $this->movimiento_model->ListarDate(
/*            isset($_POST['fechaInicio']) ? $_POST['fechaInicio'] : date('d-m-Y', strtotime('-3 month', strtotime(date('d-m-Y')))), 
            isset($_POST['fechaFin']) ? $_POST['fechaFin'] : date('d-m-Y', strtotime('+3 month', strtotime(date('d-m-Y'))))
*/          
  			isset($_POST['fechaInicio']) ? $_POST['fechaInicio'] : date('d-m-Y', strtotime(date('d-m-Y'))), 
            isset($_POST['fechaFin']) ? $_POST['fechaFin'] : date('d-m-Y', strtotime(date('d-m-Y')))
        );
        
        require_once 'view/header.php';
        require_once 'view/webforms/wfamovimiento.php';
        echo "<script type='text/javascript'>
                        $(document).ready(function (){
                            $('.operaciones').addClass('active');
                        });
                  </script>";
        require_once 'view/footer.php';
    }

    public function FrmGuardarMovimiento() {
        $this->DetruirSessiones();
        $almacen = $this->almacen_model->Listar();
        $documentos = $this->documento_model->Listar();
        $tipomovimiento = $this->tipomovimiento_model->Listar();
        require_once 'view/webforms/wfamovimientoa.php';
    }

    public function FrmEditarMovimiento() {
        $this->DetruirSessiones();
        $almacen = $this->almacen_model->Listar();
        $documentos = $this->documento_model->Listar();
        $tipomovimiento = $this->tipomovimiento_model->Listar();
        $movimiento = $this->movimiento_model->Buscar($_POST['Movimiento_Id']);
        $data = array();
        foreach ($movimiento as $r):

            $detalle_movimiento = $this->movimientodetalle_model->Buscar($r->__GET('Movimiento_Id'));
            foreach ($detalle_movimiento as $detmov):
                
                    $array = array( "ProdServ_Id" => $this->IdProductoLote($detmov->__GET('ProductoLote_Id')),
                                    "lotes" => 0,
                                    "sMovDetConCantidad" => $detmov->__GET('nMovDetCantidad'),
                                    "Unidadmedida_Id" => $detmov->__GET('nMovDetUnidad_Id'),
                                    "dMovDetConFechaVencimiento" => $this->FechaVencimiento($detmov->__GET('ProductoLote_Id')),
                                    "nMovDetConPrecioUnitario" => $detmov->__GET('nMovDetPrecioUnitario'),
                                    "nMovDetConSubtotal" => $detmov->__GET('nMovDetImporte')
                                );
                    if (isset($_SESSION['producto_c'])) {
                        $data = $_SESSION['producto_c'];
                        array_push($data, $array);
                    } else {
                        $data[] = $array;
                    }
                    $_SESSION['producto_c'] = $data;
            endforeach;
            require_once 'view/webforms/wfamovimientoa.php';
            echo '<script type="text/javascript">
                            $(document).ready(function(){
                                console.log(' . $r->__GET('nMovOrigenDestino_Id') . ');
                                TipoPrvClien();
                                TipoDocumentoEditar(' . $r->__GET('nMovOrigenDestino_Id') . ');    
                                ListarProductos();
                            });
                      </script>';
        endforeach;
    }

    public function FrmEditarClienteproveedor() {
//            $obtenerUsuario= $this->usuario_model->Buscar($_POST['Usuario_Id']);
//            foreach ($obtenerUsuario as $r):
//                require_once 'view/webforms/wfausuarioa.php';            
//            endforeach;       
    }

    public function MostrarMovimiento() {
        $total = 0;
        $almacen = $this->almacen_model->Listar();
        $documentos = $this->documento_model->Listar();
        $tipomovimiento = $this->tipomovimiento_model->Listar();
        $movimiento = $this->movimiento_model->Buscar($_POST['Movimiento_Id']);
        $data = array();
        foreach ($movimiento as $r):
            echo "<tr>";
            echo "<td>Fecha: </td>";
            echo "<td>".$r->__GET('dMovFecha')."</td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td>Tipo Movimiento: </td>";
            echo "<td>".$r->__GET('sTMovNombre')."</td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td>Tipo Pago: </td>";
            echo "<td>".($r->__GET('nMovTipopago') == 1 ? 'Contado':'Credito')."</td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td>Tipo: </td>";
            echo "<td>".($r->__GET('nMovTipoOrigenDestino') == 1 ? 'Proveedor':'Cliente')."</td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td>Cliente/Proveedor:</td>";
            echo "<td>".$this->NombreClienteProveedor($r->__GET('nMovOrigenDestino_Id'))."</td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td>Destino:</td>";
            echo "<td>".($r->__GET('nMovTipodestino') == 1 ? 'Almacen' : 'Consumo')."</td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td>Almacen:</td>";
            echo "<td>".$this->AlmNombre($r->__GET('Almacen_Id'))."</td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td>Documento:</td>";
            echo "<td>".$this->DocNombre($r->__GET('Documento_Id'))."</td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td>Nro. Documento:</td>";
            echo "<td>".$r->__GET('sDocNombreCorto')." - ".$r->__GET('sMovDocumento')."</td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td>Documento referencia:</td>";
            echo "<td>".$r->__GET('sMovDocReferencia')."</td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td>Tipo de moneda:</td>";
            echo "<td>".($r->__GET('Moneda_Id')== 1 ? 'Soles':'Dolares')."</td>";
            echo "</tr>";        
            echo "<tr><td colspan='2'><table class='table table-hover table-bordered'>";
            echo "<thead>";
            echo "<tr>";
            echo "<th>Producto</th>";
            echo "<th>Cantidad</th>";
            echo "<th>Unidad</th>";
            echo "<th>Precio</th>";
            echo "<th>Sub Total</th>";
            echo "<tr>";
            echo "</thead><tbody>";
            $detalle_movimiento = $this->movimientodetalle_model->Buscar($r->__GET('Movimiento_Id'));    
            foreach ($detalle_movimiento as $detmov):
                echo '<tr>';
                echo '<td>'.$this->ProdservdNombre($this->IdProductoLote($detmov->__GET('ProductoLote_Id'))).'</td>';
                echo '<td>'.$detmov->__GET('nMovDetCantidad').'</td>';
                echo '<td>'.$this->UnidadNombre($detmov->__GET('nMovDetUnidad_Id')).'</td>';
                echo '<td>'.$detmov->__GET('nMovDetPrecioUnitario').'</td>';
                echo '<td>'.$detmov->__GET('nMovDetImporte').'</td>';
                echo '</tr>';    
            endforeach;
            echo '<tr><td style="text-align: right;" colspan="4">IMPORTE</td><td>'.$r->__GET('nMovImporte').'</td></tr>';
            echo '<tr><td style="text-align: right;" colspan="4">IGV</td><td>'.$r->__GET('nMovIGV').'</td></tr>';
            echo '<tr><td style="text-align: right;" colspan="4">TOTAL</td><td>'.$r->__GET('nMovTotal').'</td></tr>';
            echo '</tbody></table></td></tr>';
        endforeach;
    }

    public function GuardarMovimiento() {
        $mov = new Movimiento();
        if ($_POST['Movimiento_Id'] != 0 ? $mov->__SET('Movimiento_Id', $_POST['Movimiento_Id']) : '')
            ;
        $mov->__SET('dMovFecha', $_POST['dMovFecha']);
        $mov->__SET('nMovTipoMovimiento_Id', $_POST['Tipomovimiento_Id']);
        $mov->__SET('nMovTipopago', $_POST['nMovTipopago']);
        $mov->__SET('Almacen_Id', $_POST['Almacen_Id']);
        $mov->__SET('nMovTipoOrigenDestino', $_POST['nMovTipoOrigenDestino']);
        $mov->__SET('nMovOrigenDestino_Id', $_POST['nMovOrigenDestino_Id']);
        $mov->__SET('nMovTipodestino', $_POST['nMovTipodestino']);
        $mov->__SET('Documento_Id', $_POST['Documento_Id']);
        $mov->__SET('sMovDocumento', $_POST['sMovDocumento']);
        $mov->__SET('sMovDocReferencia', $_POST['sMovDocReferencia']);
        $mov->__SET('Moneda_Id', $_POST['Moneda_Id']);
        $mov->__SET('nMovTipoCambio', $_POST['nMovTipoCambio']);
        $mov->__SET('nMovImporte', $_POST['dMovImporte']);
        $mov->__SET('nMovIGV', $_POST['dMovIGV']);
        $mov->__SET('nMovTotal', $_POST['dMovTotal']);
        $mov->__SET('nMovTotalCancelado', $_POST['nMovTipopago'] == 1 ? $_POST['dMovTotal'] : '0');
        $mov->__SET('dMovDetraccion', $_POST['dMovDetraccion']);
        $mov->__SET('dMovPercepcion', $_POST['dMovPercepcion']);
        $mov->__SET('dMovRetencion', $_POST['dMovRetencion']);
        $mov->__SET('sMovObservacion', $_POST['sMovObservacion']);
        $mov->__SET('nMovEstado', $_POST['nMovTipopago'] == 1 ? '2' : '1');
        $mov->__SET('nMovEliminado', 0);
        $mov->__SET('Usuario_Id', $_SESSION['usu_codigo']);
        if ($_POST['Movimiento_Id'] != 0) {
            $this->movimiento_model->Editar($mov);
        } else {
            $Movimiento_Id = $this->movimiento_model->Guardar($mov);
            if (isset($_SESSION['producto_c'])) {
                $data = $_SESSION['producto_c'];
                for ($i = 0; $i < count($data); $i++) {
//                        echo "<h1>$Movimiento_Id</h1><br>";
                    $movdet = new Movimientodetalle();
                    $movdet->__SET('Movimiento_Id', $Movimiento_Id);
                    $movdet->__SET('TipoMovimiento_Id', $_POST['Tipomovimiento_Id']);
                    $movdet->__SET('TipoDestino', $_POST['nMovTipodestino']);
                    $movdet->__SET('ProdServ_Id', $data[$i]['ProdServ_Id']);
                    $movdet->__SET('Almacen_Id', $_POST['Almacen_Id']);
                    $movdet->__SET('Lote', $data[$i]['lotes'] != 0 ? $data[$i]['lotes'] : date('Ymdhis'));
                    $movdet->__SET('FechaVencimiento', $data[$i]['dMovDetConFechaVencimiento']);
                    $movdet->__SET('Cantidad', $data[$i]['sMovDetConCantidad']);
                    $movdet->__SET('Unidad_Id', $data[$i]['Unidadmedida_Id']);
                    $movdet->__SET('MonedaCompra_Id', $_POST['Moneda_Id']);
                    $movdet->__SET('PrecioLista', $data[$i]['nMovDetConPrecioUnitario']);
                    $movdet->__SET('Descuento', 0);
                    $movdet->__SET('Precio', $data[$i]['nMovDetConPrecioUnitario']);
                    $movdet->__SET('SubTotal', $data[$i]['nMovDetConSubtotal']);
                    $movdet->__SET('Usuario_Id', $_SESSION['usu_codigo']);
                    $movdet->__SET('TipoCambio', $_POST['nMovTipoCambio']);
                    $movdet->__SET('nMovDetIncluyeIgv', 0);
                    $this->movimientodetalle_model->Guardar($movdet);
                }
            }
            $this->documento_model->Incrementa($_POST['Documento_Id']);
        }
        $this->DetruirSessiones();
        echo '<div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Exito información guardado</strong>, los datos fueron registrados correctamente.
                  </div>';
    }

    public function TipoMovimiento() {
        $productos = $this->prodserv_model->Listar();
        $tipomovimiento = $this->tipomovimiento_model->Buscar($_POST['Tipomovimiento_Id']);
        foreach ($tipomovimiento as $tv) {
            echo '<script type="text/javascript">
                        $(\'#nMovTipoOrigenDestino option[value="' . $tv->__GET('nTMovTipoOD') . '"]\').attr(\'selected\',\'selected\');
                        TipoPrvClien();
                    </script>';
        }
        if ($_POST['Tipomovimiento_Id'] == 1 || $_POST['Tipomovimiento_Id'] == 3) {
            require_once 'view/webforms/tipomovimiento/ingreso_compra.php';
        } elseif ($_POST['Tipomovimiento_Id'] == 2 || $_POST['Tipomovimiento_Id'] == 4) {
            require_once 'view/webforms/tipomovimiento/salida_venta.php';
        }
    }

    public function ListarUnidades() {
        $unidad = $this->unidadmedida_model->ListarUP($this->ObtenerID($_POST['ProdServ_Id']));
        $_SESSION['$unidad'] = $unidad;
        echo "<option value=''>Seleccione unidad</option>";
        foreach ($unidad as $uni) {
            echo "<option value='" . $uni->__GET('UnidadMedida_Id') . "'>" . $uni->__GET('sUndDescripcion') . "</option>";
        }
    }

    public function ListarProductos() {
        if (isset($_SESSION['producto_c'])) {
            $data = $_SESSION['producto_c'];
            for ($i = 0; $i < count($data); $i++) {
                echo "<tr>
                            <td>" . ($data[$i]['lotes'] != 0 ? $data[$i]['lotes'] : $this->ProdservdNombre($data[$i]['ProdServ_Id'])) . "</td>
                            <td>" . $data[$i]['sMovDetConCantidad'] . "</td>
                            <td>" . $this->UnidadNombre($data[$i]['Unidadmedida_Id']) . "</td>
                            <td>" . number_format($data[$i]['nMovDetConPrecioUnitario'], 2, ".", ".") . "</td>
                            <td>" . number_format($data[$i]['nMovDetConSubtotal'], 2, ".", ".") . "</td>
                            <td>
                                <a href=\"#\" class=\"btn btn-danger btn-flat btn-xs\" data-toggle=\"modal\" data-target=\"#eliminar_productos\" data-productoid='" . $i . "'><i class=\"fa fa-trash-o\"></i></a>                                            
                            </td>
                        </tr>";
            }
        } else {
            echo "<tr>
                        <td colspan='6'>No hay datos</td>
                      </tr>";
        }
    }

    public function ListarLoteProducto() {
        $data = $this->productolote_model->Listar(!empty($_POST['ProdServ_Id']) ? $_POST['ProdServ_Id'] : 0);
        if (count($data) > 0) {
            foreach ($data as $l) {
                echo "<tr>
                            <th>
                                <input type='radio' name='lotes' id='lotes' value='" . $l->__GET("Lote") . "'>
                            </th>
                            <td>" . $l->__GET("Lote") . "</td>
                            <td>" . $l->__GET("Stock") . "</td>
                          </tr>";
            }
        } else {
            echo "<tr>
                        <td colspan='3'>
                            No hay datos
                        </td>
                      </tr>";
        }
    }

    public function AgregarProductoC() {
        $data = array();
        if($_POST['nIncluyeIGV'] == 1){
            $nMovDetConPrecioUnitario = $_POST['nMovDetConPrecioUnitario'] / $this->IGV;
        }else{
            $nMovDetConPrecioUnitario = $_POST['nMovDetConPrecioUnitario'];
        }
        $array = array( "ProdServ_Id" => isset($_POST['ProdServ_Id']) ? $_POST['ProdServ_Id'] : '0',
                        "lotes" => isset($_POST['lotes']) ? $_POST['lotes'] : '0',
                        "sMovDetConCantidad" => $_POST['sMovDetConCantidad'],
                        "Unidadmedida_Id" => $_POST['Unidadmedida_Id'],
                        "dMovDetConFechaVencimiento" => isset($_POST['dMovDetConFechaVencimiento']) ? $_POST['dMovDetConFechaVencimiento'] : date('d-m-Y'),
                        "nMovDetConPrecioUnitario" => $nMovDetConPrecioUnitario,
                        "nMovDetConSubtotal" =>  $nMovDetConPrecioUnitario * $_POST['sMovDetConCantidad']);
        if (isset($_SESSION['producto_c'])) {
            $data = $_SESSION['producto_c'];
            array_push($data, $array);
        } else {
            $data[] = $array;
        }
        $_SESSION['producto_c'] = $data;
        echo '<div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Exito!</strong>, Los Datos fueron agregados correctamente.
                  </div>';
    }

    public function EliminarProductoLista() {
        $array = array();
        $data = $_SESSION['producto_c'];
        for ($i = 0; $i < count($data); $i++) {
            if ($i != $_POST['Codigo_Producto']) {
                $array[] = array("ProdServ_Id" => isset($_POST['ProdServ_Id']) ? $_POST['ProdServ_Id'] : $data[$i]['ProdServ_Id'],
                    "lotes" => isset($_POST['lotes']) ? $_POST['lotes'] : '0',
                    "sMovDetConCantidad" => $data[$i]['sMovDetConCantidad'],
                    "Unidadmedida_Id" => $data[$i]['Unidadmedida_Id'],
                    "dMovDetConFechaVencimiento" => $data[$i]['dMovDetConFechaVencimiento'],
                    "nMovDetConPrecioUnitario" => $data[$i]['nMovDetConPrecioUnitario'],
                    "nMovDetConSubtotal" => $data[$i]['nMovDetConSubtotal']);
            }
        }
        $_SESSION['producto_c'] = $array;
        echo "Datos eliminados";
    }

    public function EliminarProductos() {
        $pro = new Prodserv();
        $pro->__SET('ProdServ_Id', $_POST['idProdServ']);
        $this->prodserv_model->Eliminar($pro);
        echo '<script type="text/javascript">window.location="?c=Productos&a=Index";</script>';
    }

    public function AnularMovimiento() {
        $mov = new Movimiento();
        $mov->__SET('Movimiento_Id', $_POST['IdMovimiento']);
        $mov->__SET('Usuario_Id', $_SESSION['usu_codigo']);
        $this->movimiento_model->Eliminar($mov);
        echo '<script type="text/javascript">window.location="?c=Movimiento&a=Index";</script>';
    }

    public function ObtenerID($ProdServ_Id) {
        foreach ($this->prodserv_model->Buscar($ProdServ_Id) as $p) {
            return $p->__GET('nProSrvUnidad_Id');
        }
    }

    public function CalcularImpTotIGV() {
        $importe = 0;
        $total = 0;
        $IGV = 1;
        
        $documento = $this->documento_model->Buscar($_POST['Documento_Id'] != "" ? $_POST['Documento_Id'] : '0');
        if(count($documento)>0){
            foreach ($documento as $doc){
                if($doc->__GET('nDocAfectoIGV') == 1){
                    $IGV = $this->IGV;
                }
            }
        }
        if (isset($_SESSION['producto_c'])) {
            $data = $_SESSION['producto_c'];
            for ($i = 0; $i < count($data); $i++) {
                $importe = $importe + $data[$i]['nMovDetConSubtotal'];
            }
        }
        echo "<script type='text/javascript'>
                    document.getElementById('dMovImporte').value = '" . number_format($importe, 2, ".", ",") . "';
                    document.getElementById('dMovIGV').value = '" . number_format(($importe *  $IGV - $importe), 2, ".", ",") . "';
                    document.getElementById('dMovTotal').value = '" . number_format(($importe * $IGV), 2, ".", ",") . "';
                </script>";
    }

        public function CalcularImpTotIGV_venta() {
        $importe = 0;
        $total = 0;
        $IGV = 1;
        
        $documento = $this->documento_model->Buscar($_POST['Documento_Id'] != "" ? $_POST['Documento_Id'] : '0');
        if(count($documento)>0){
            foreach ($documento as $doc){
                if($doc->__GET('nDocAfectoIGV') == 1){
                    $IGV = $this->IGV;
                }
            }
        }
        if (isset($_SESSION['producto_c'])) {
            $data = $_SESSION['producto_c'];
            for ($i = 0; $i < count($data); $i++) {
                $importe = $importe + $data[$i]['nMovDetConSubtotal'];
            }
        }
        echo "<script type='text/javascript'>
                    document.getElementById('dPedImporte').value = '" . number_format($importe, 2, ".", ",") . "';
                    document.getElementById('dPedIGV').value = '" . number_format(($importe *  $IGV - $importe), 2, ".", ",") . "';
                    document.getElementById('dPedTotal').value = '" . number_format(($importe * $IGV), 2, ".", ",") . "';
                </script>";
    }
    
    public function UnidadNombre($Unidadmedida_Id) {
        $dato = $this->unidadmedida_model->Buscar($Unidadmedida_Id);
        foreach ($dato as $d) {
            return $d->__GET('sUndAlias');
        }
    }

    public function ProdservdNombre($ProdServ_Id) {
        $data = $this->prodserv_model->Buscar($ProdServ_Id);
        foreach ($data as $pro) {
            return $pro->__GET('sProSrvNombre');
        }
    }
    
    public function DocNombre($Documento_Id) {
        $data = $this->documento_model->Buscar($Documento_Id);
        foreach ($data as $doc) {
            return $doc->__GET('sDocNombre');
        }
    }
    public function AlmNombre($Almacen_Id){
        
        $data = $this->almacen_model->Buscar($Almacen_Id);
        foreach ($data as $alm) {
            return $alm->__GET('sAlmNombre');
        }
    }
    function DetruirSessiones() {
        unset($_SESSION['producto_c']);
    }

}

?>