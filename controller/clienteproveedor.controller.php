<?php    
    require_once 'model/parametro.model.php';
    require_once 'model/parametro.entidad.php';
    require_once 'model/origendestino.model.php';
    require_once 'model/origendestino.entidad.php';
    class ClienteproveedorController {
        private $parametro_model;
        private $origendestino_model;
        public function __CONSTRUCT(){
            $this->parametro_model = new ParametroModel();
            $this->origendestino_model = new OrigendestinoModel();
        }
        
        public function Index(){
            $parametro = $this->parametro_model->ListarParametros();
            $clienteproveedor = $this->origendestino_model->Listar();
            require_once 'view/header.php';  
            require_once 'view/webforms/wfaclienteproveedor.php';
            echo "<script type='text/javascript'>
                        $(document).ready(function (){
                            $('.catalogos').addClass('active');
                        });
                  </script>";
            require_once 'view/footer.php'; 
        }
        public function FrmGuardarClienteproveedor(){
            require_once 'view/webforms/wfaclienteproveedora.php'; 
        }
        public function GuardarClienteproveedor(){
            $pro = new Origendestino();
            if($_POST['Provclien_Id'] != 0 ? $pro->__SET('OrigenDestino_Id', $_POST['Provclien_Id']) : '');
            $pro->__SET('nODTipo', $_POST['nProcliTipo']);
            $pro->__SET('sODRucDni', $_POST['nProcliRUCDNI']);
            $pro->__SET('sODNombre', $_POST['sProcliNombre']);
            $pro->__SET('sODNombreComercial', $_POST['sProcliNomComercial']);
            $pro->__SET('sODDireccion', $_POST['sProcliDireccion']);
            $pro->__SET('sODTelefono', $_POST['nProcliTelefono']);
            $pro->__SET('sODCorreo', $_POST['sProcliCorreo']);
            $pro->__SET('sODContacto', $_POST['sProcliContacto']);
            $pro->__SET('nODDiasCredito', $_POST['nProcliDiasCredito']);
            $pro->__SET('nODEstado', $_POST['nProcliEstado']);
            $pro->__SET('nODEliminado', 0);
            $pro->__SET('nODUsuario_Id', $_SESSION['usu_codigo']);
            if($_POST['Provclien_Id'] != 0 ? $this->origendestino_model->Editar($pro) : $this->origendestino_model->Crear($pro));
        }
        public function FrmEditarClienteproveedor(){
            $datos = $this->origendestino_model->Buscar($_POST['Provclien_Id']);
            foreach ($datos as $r):
                require_once 'view/webforms/wfaclienteproveedora.php';            
            endforeach;       
        }
        public function MostrarClienteproveedor(){
            $clientes= $this->origendestino_model->Buscar($_POST['Provclien_Id']);
            foreach ($clientes as $r):
                if($r->__GET('nODTipo')==1){
                    $tipo="Proveedor";
                }elseif ($r->__GET('nODTipo')==2) {
                    $tipo="Cliente";
                }else{
                    $tipo="Reportes";
                }
                echo " <tbody>
                          <tr>
                             <td>
                                <input type='hidden' name='OrigenDestino_Id' id='OrigenDestino_Id' value='".$r->__GET('OrigenDestino_Id')."'> 
                                Nombre
                             </td>
                             <td>".$r->__GET('sODNombre')."</td>
                          </tr>
                          <tr>
                             <td>Nombre Comercial</td>
                             <td>".$r->__GET('sODNombreComercial')."</td>
                          </tr>
                          <tr>
                             <td>RUC/DNI</td>
                             <td>".$r->__GET('sODRucDni')."</td>
                          </tr>
                          <tr>
                             <td>Dirección</td>
                             <td>".$r->__GET('sODDireccion')."</td>
                          </tr>
                          <tr>
                             <td>Teléfono</td>
                             <td>".$r->__GET('sODTelefono')."</td>
                          </tr>
                          <tr>
                             <td>Correo</td>
                             <td>".$r->__GET('sODCorreo')."</td>
                          </tr>
                          <tr>
                             <td>Contacto</td>
                             <td>".$r->__GET('sODContacto')."</td>
                          </tr>
                          <tr>
                             <td>Días de Credito</td>
                             <td>".$r->__GET('nODDiasCredito')."</td>
                          </tr>   
                          <tr>
                             <td>Estado</td>
                             <td>".($r->__GET('nODEstado') == '1' ? '<span class="label bg-green">Habilitado</span>' : '<span class="label bg-danger">Inhabilitado</span>')."</td>
                          </tr>
                       </tbody>
                     ";
            endforeach;
        }
        public function EliminarClienteproveedor(){
            $procli = new Origendestino();
            $procli->__SET('OrigenDestino_Id', $_POST['IdProvclien']);
            $this->origendestino_model->Eliminar($procli);
            echo '<script type="text/javascript">window.location="?c=Clienteproveedor&a=Index";</script>';
        }
        
        public function TipoPrvClien(){
            $provclien = $this->origendestino_model->Listar();
            echo '<option value="">Seleccione cliente/proveedor</option>';
            foreach ($provclien as $p) {
                if($p->__GET('nODTipo') == $_POST['tipo']){
                    echo '<option value="'.$p->__GET('OrigenDestino_Id').'">'.$p->__GET('sODNombreComercial').'</option>';
                }                
            }
        }
        public function PDFCliente(){            
          $datos= $this->origendestino_model->Buscar($_POST['OrigenDestino_Id']);
          require_once 'view/webforms/PDF/cliente.php';
        }
    }
?>