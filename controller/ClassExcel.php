<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ClassExcel
 *
 * @author ANTHONY
 */

class ClassExcel {
        
    public function SetPropiedades($set_propiedades = ["setCreator","setLastModifiedBy","setTitle","setSubject","setDescription","seTitleActive"]){
        $this->objPHPExcel->getProperties()->setCreator($set_propiedades[0]);
        $this->objPHPExcel->getProperties()->setLastModifiedBy($set_propiedades[1]);
        $this->objPHPExcel->getProperties()->setTitle($set_propiedades[2]);
        $this->objPHPExcel->getProperties()->setSubject($set_propiedades[3]);
        $this->objPHPExcel->getProperties()->setDescription($set_propiedades[4]);        
        $this->objPHPExcel->getActiveSheet()->setTitle($set_propiedades[5]);        
    }
    public function SetTitulo($Cells = "A1:C1", $Posicion = "A1", $Title){
        $this->objPHPExcel->setActiveSheetIndex(0)
             ->mergeCells($Cells)
             ->setCellValue($Posicion, $Title);        
    }
    public function HeaderCell($posicion = 3, $titulosColumnas = ['EJEMPLO01', 'EJEMPLO02', 'EJEMPLO03']){        
        $letra = range('A', 'Z');
        for($i=0;$i<count($titulosColumnas);$i++){
            $this->objPHPExcel->setActiveSheetIndex(0)
                 ->setCellValue($letra[$i]."".$posicion,  $titulosColumnas[$i]);
        }
    }
    
    public function Style_Titulo($Cantidad, $letra){
        $let_ini = $letra[0];
        $let_fin = $letra[count($letra)-1];
        $header = array(            
            'font' => array(
                'bold' => true,
            )
        );
        $this->objPHPExcel->getActiveSheet()->getStyle($let_ini."1:".$let_fin."2")->applyFromArray($header);    
        
        $styleArray = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                ),
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startcolor' => array(
                    'argb' => 'FFFFFFFF',
                ),
                'endcolor' => array(
                    'argb' => 'FFFFFFFF',
                ),
            ),
        );
        $this->objPHPExcel->getActiveSheet()->getStyle($let_ini."1:".$let_fin."".($Cantidad+2))->applyFromArray($styleArray);        
    }
    public function Add_Cell_rptVentaDetallado($Posicion = 3, $data, $letra){
        $j = $Posicion;
        for ($i = 0; $i < count($data); $i++) {
            $sub_array = array_values($data[$i]);
            for($k = 0; $k < count($sub_array); $k++) {
                $this->objPHPExcel->setActiveSheetIndex(0)
                     ->setCellValue($letra[$k] . $j, $sub_array[$k]);
            }
            $j++;
        }
    }
    public function AjustarTextoCell($Posicion = 'A4:N4'){        
        $this->objPHPExcel->getActiveSheet()->getStyle($Posicion.$this->objPHPExcel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true); 
        $this->objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
    }
    public function AjustarColumnCell($letras){
        for($i = 0 ; $i < count($letras) ; $i++){
            $this->objPHPExcel->getActiveSheet()->getColumnDimension($letras[$i])->setAutoSize(true);
        }
    }
    public function WriteFormat($name_Excel = 'view/librerias/dist/excel/detalle_ventas.xlsx'){
        $objWriter = new PHPExcel_Writer_Excel2007($this->objPHPExcel);
        $objWriter->save($name_Excel);
        echo $name_Excel;
    }
}
