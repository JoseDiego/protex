<?php
require_once 'model/parametro.model.php';
require_once 'model/parametro.entidad.php';
require_once 'model/unidadmedida.model.php';
require_once 'model/unidadmedida.entidad.php';
require_once 'model/familia.model.php';
require_once 'model/familia.entidad.php';
require_once 'model/prodserv.model.php';
require_once 'model/prodserv.entidad.php';
require_once 'model/activo.model.php';
require_once 'model/activo.entidad.php';
require_once 'model/precioventa.model.php';
require_once 'model/precioventa.entidad.php';
require_once 'model/operario.model.php';
require_once 'model/operario.entidad.php';
require_once 'controller/receta.controller.php';
require_once 'controller/metodos.controller.php';

class ProduccionPrendaController extends MetodosController{
  private $parametro_model;
  public $unidadmedida_model;
  private $familia_model;
  private $prodserv_model;
  private $operario_model;
  private $activo_model;
  public $precioventa_model;
  private $receta_controller;
  public function __CONSTRUCT(){
    $this->parametro_model = new ParametroModel();
    $this->unidadmedida_model = new UnidadmedidaModel();
    $this->familia_model = new FamiliaModel();
    $this->prodserv_model = new ProdservModel();
    $this->activo_model = new ActivoModel();
    $this->precioventa_model = new PrecioventaModel();
    $this->operario_model = new OperarioModel();
    $this->receta_controller = new RecetaController();
  }

  public function Index(){
    $parametro = $this->parametro_model->ListarParametros();
    $productos = $this->prodserv_model->Listar();
    require_once 'view/header.php';
    require_once 'view/webforms/wfaproduccionprenda.php';
    echo "<script type='text/javascript'>
    $(document).ready(function (){
      $('.operaciones').addClass('active');
    });
    </script>";
    require_once 'view/footer.php';
  }

  public function NuevaProduccionPrenda() {

    require_once 'view/webforms/wfaproduccionprendaa.php';
  }

  public function ListarComboBoxOperarios(){
      $operario = $this->operario_model->Listar();
      echo '<option value="0">Seleccionar operario</option>';
      //if ($_POST['OrigenDestino_Id']==0) {
          foreach ($operario as $o){
            echo '<option value="'.$o->__GET('Operario_Id').'" '.($_POST['Operario_Id'] != 0 ? $o->__GET('Operario_Id') == $_POST['Operario_Id'] ? 'selected':'':'').'>'.$o->__GET('sOpeNombres').' '.$o->__GET('sOpeApePat').' '.$o->__GET('sOpeApeMat').'</option>';
          }
      //}
  }
}
?>
