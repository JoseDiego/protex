<?php    
    require_once 'model/movimiento.entidad.php';
    require_once 'model/movimiento.model.php';
    require_once 'model/parametro.model.php';
    require_once 'model/parametro.entidad.php';
    require_once 'model/pago.model.php';
    require_once 'model/pago.entidad.php';
    require_once 'model/documento.model.php';
    require_once 'model/documento.entidad.php';
    require_once 'model/origendestino.model.php';
    require_once 'model/origendestino.entidad.php';
    require_once 'controller/metodos.controller.php';
    class CreditosController extends MetodosController {
        private $movimiento_model;
        private $parametro_model;
        private $pago_model;
        private $documento_model;
        public $origendestino_model;
        public function __CONSTRUCT(){
            $this->movimiento_model = new MovimientoModel();
            $this->parametro_model = new ParametroModel();
            $this->pago_model = new PagoModel();
            $this->documento_model = new DocumentoModel();
            $this->origendestino_model = new OrigendestinoModel();
        }        
        public function Index(){
            $parametro = $this->parametro_model->ListarParametros();
            /*$movimientos= $this->movimiento_model->Listar();*/
            $movimientos = $this->movimiento_model->ListarDate(
         
  			isset($_POST['fechaInicio']) ? $_POST['fechaInicio'] : date('d-m-Y', strtotime(date('d-m-Y'))), 
            isset($_POST['fechaFin']) ? $_POST['fechaFin'] : date('d-m-Y', strtotime(date('d-m-Y')))
        );
	            
            require_once 'view/header.php';  
            require_once 'view/webforms/wfacreditos.php';
            echo "<script type='text/javascript'>
                        $(document).ready(function (){
                            $('.operaciones').addClass('active');
                        });
                  </script>";
            require_once 'view/footer.php'; 
        }
        public function FrmGuardarUsuario(){
//            require_once 'view/webforms/wfausuarioa.php'; 
        }
        
        public function FrmEditarUsuario(){
//            $obtenerUsuario= $this->usuario_model->Buscar($_POST['Usuario_Id']);
//            foreach ($obtenerUsuario as $r):
//                require_once 'view/webforms/wfausuarioa.php';            
//            endforeach;       
        }
        public function MostrarUsuario(){
//            $usuarios= $this->usuario_model->Buscar($_POST['Usuario_Id']);
//            foreach ($usuarios as $r):
//                if($r->__GET('nUsuTipo')==1){
//                    $tipo="Administrador";
//                }elseif ($r->__GET('nUsuTipo')==2) {
//                    $tipo="Asistente";
//                }else{
//                    $tipo="Reportes";
//                }
//                echo " <tbody>
//                          <tr>
//                             <td>
//                                <input type='hidden' name='idUsuario' id='idUsuario' value='".$r->__GET('Usuario_Id')."'> 
//                                Nombre de Usuario
//                             </td>
//                             <td>".$r->__GET('sUsuNombre')."</td>
//                          </tr>
//                          <tr>
//                             <td>Login</td>
//                             <td>".$r->__GET('sUsuLogin')."</td>
//                          </tr>  
//                          <tr>
//                             <td>Tipo de Usuario</td>
//                             <td>".$tipo."</td>
//                          </tr>  
//                          <tr>
//                             <td>Estado</td>
//                             <td>".($r->__GET('nUsuEstado') == '1' ? '<span class="label bg-green">Habilitado</span>' : '<span class="label bg-danger">Inhabilitado</span>')."</td>
//                          </tr>
//                       </tbody>
//                     ";
//            endforeach;
        }
        public function GuardarPago(){
            $pag = new Pago();
            if($_POST['Pago_Id'] != 0 ? $pag->__SET('Pago_Id', $_POST['Pago_Id']) : '');
            $pag->__SET('Movimiento_Id', $_POST['Movimiento_Id']);
            $pag->__SET('dPagFecha', $_POST['dPagFecha']);
            $pag->__SET('nPagTipoPago_Id', $_POST['nPagTipoPago_Id']);
            $pag->__SET('sPagDocumento', $_POST['sPagDocumento']);
            $pag->__SET('nPagImporte', $_POST['nPagImporte']);
            $pag->__SET('nPagLetra_Id', 0);
            $pag->__SET('nPagUsuario_Id', $_SESSION['usu_codigo']);
            if($_POST['Pago_Id'] != 0 ? $this->pago_model->Editar($pag) : $this->pago_model->Crear($pag));
        }
        public function EliminarUsuario(){
//            $usu = new Usuario();
//            $usu->__SET('Usuario_Id', $_POST['idUsuario']);
//            $this->usuario_model->Eliminar($usu);
//            echo '<script type="text/javascript">window.location="?c=Usuario&a=Index";</script>';
//            
        }
    }
?>