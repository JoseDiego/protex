<?php
require_once 'model/pedido.entidad.php';
require_once 'model/pedido.model.php';
require_once 'model/pedido_detalle.entidad.php';
require_once 'model/pedido_detalle.model.php';
require_once 'model/movimiento.entidad.php';
require_once 'model/movimiento.model.php';
require_once 'model/receta.entidad.php';
require_once 'model/receta.model.php';
require_once 'model/productolote.entidad.php';
require_once 'model/productolote.model.php';
require_once 'model/movimientodetalle.entidad.php';
require_once 'model/movimientodetalle.model.php';
require_once 'model/precioventa.entidad.php';
require_once 'model/precioventa.model.php';
require_once 'model/documento.entidad.php';
require_once 'model/documento.model.php';
require_once 'controller/metodos.controller.php';
class PedidoController extends MetodosController{
    private $pedido_model;
    private $pedido_detalle_model;
    private $movimiento_model;
    private $receta_model;
    private $movimientodetalle_model;
    public $documento_model;
    public $precioventa_model ;
    public function __CONSTRUCT() {
        $this->pedido_model = new pedidoModel();
        $this->pedido_detalle_model = new Pedido_detalleModel();
        $this->movimiento_model = new MovimientoModel();
        $this->receta_model = new RecetaModel();
        $this->productolote_model = new ProductoloteModel();
        $this->movimientodetalle_model = new MovimientodetalleModel();
        $this->documento_model = new DocumentoModel();
        $this->precioventa_model = new PrecioventaModel();
    }
    public function Index() {
        //INGRESAR Y LLAMAR A LAS VISTAR PARA EL INDEX
    }
    public function GuardarPedido() {        
        $pedidos = isset($_SESSION['productos_v']) ? $_SESSION['productos_v']:array();
        $ped = new Pedido();
        if($_POST['Pedido_Id'] != 0 ? $ped->__SET('Pedido_Id', $_POST['Pedido_Id']) : '');
        $ped->__SET('OrigenDestino_Id', $_POST['OrigenDestino_Id']);
        $ped->__SET('dPedFecha', $_POST['dPedFecha']);
        $ped->__SET('nPedTipopago', $_POST['nPedTipopago']);
        $ped->__SET('Documento_Id', $_POST['Documento_Id']);
//        $ped->__SET('sPedDocumento', $_POST['sPedDocumento']);
        $ped->__SET('dPedImporte', $_POST['dPedImporte']);
        $ped->__SET('dPedIGV', $_POST['dPedIGV']);
        $ped->__SET('dPedTotal', $_POST['dPedTotal']);
        $ped->__SET('sPedObservaciones', $_POST['sPedObservaciones']);
        $ped->__SET('nPedEstado', $_POST['nPedEstado']);
        $ped->__SET('dPedEliminado', 0);
        $ped->__SET('Usuario_Id', $_SESSION['usu_codigo']);
        if($_POST['Pedido_Id'] != 0){
            $this->pedido_model->Editar($ped);
            $this->pedido_detalle_model->Eliminar($_POST['Pedido_Id']);            
            echo '<div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Exito información editada</strong>, los datos fueron editados correctamente.
                  </div>';
        }else{
            $Pedido_Id = $this->pedido_model->Crear($ped);
            echo '<div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Exito información guardado</strong>, los datos fueron registrados correctamente.
                  </div>';  
        }        
        if(count($pedidos)!=0){
            for($i=0;$i<count($pedidos);$i++){
                $peddet = new Pedido_detalle();
                $peddet->__SET('Pedido_Id', $_POST['Pedido_Id'] != 0 ? $_POST['Pedido_Id']:$Pedido_Id);
                $peddet->__SET('ProdServ_Id', $pedidos[$i]['ProdServ_Id']);
                $peddet->__SET('UnidadMedida_Id', $pedidos[$i]['UnidadMedida_Id']);
                $peddet->__SET('nPedDetCantidad', $pedidos[$i]['nPedDetCantidad']);
                $peddet->__SET('dPedDetPrecioUnitario', $pedidos[$i]['dPedDetPrecioUnitario']);
                $peddet->__SET('dPedDetSubtotal', $pedidos[$i]['dPedDetSubtotal']);
                $this->pedido_detalle_model->Crear($peddet);                
            }
        }
        
        if($_POST['Pedido_Id'] == 0){                      
            echo '<script type="text/javascript">
                    $(document).ready(function(){
                        EditarPedido(\''.$Pedido_Id.'\');
                    });
                    </script>';
        }
//        echo '<script type="text/javascript">window.location="?c=Ventas&a=Index";</script>';
        $this->EliminarSessiones();
    }
    public function FrmPedido() {
        //FORMULARIO
    }
    public function FrmPedidoEditar() {
        $Pedido_Id = $_POST['Pedido_Id'];
        $this->EliminarSessiones();
        $data = array();
        $pedido = $this->pedido_model->Buscar($Pedido_Id);                
        $pedido_detalle = $this->pedido_detalle_model->Buscar($Pedido_Id);
        foreach ($pedido_detalle as $pd){
            $data[] = array("ProdServ_Id"             => $pd->__GET('ProdServ_Id'),
                            "UnidadMedida_Id"         => $pd->__GET('UnidadMedida_Id'),
                            "nPedDetCantidad"         => $pd->__GET('nPedDetCantidad'),
                            "dPedDetPrecioUnitario"   => $pd->__GET('dPedDetPrecioUnitario'),
                            "dPedDetSubtotal"         => $pd->__GET('dPedDetSubtotal'));
        }
        $_SESSION['productos_v'] = $data;
        foreach ($pedido as $p){
            echo '<script type="text/javascript">
                    document.getElementById(\'Pedido_Id\').value = "'.$p->__GET('Pedido_Id').'";
                    $(\'#OrigenDestino_Id option[value="'.$p->__GET('OrigenDestino_Id').'"]\').attr(\'selected\',\'selected\');
                    document.getElementById(\'dPedFecha\').value = "'.$p->__GET('dPedFecha').'";
                    $(\'#nPedTipopago option[value="'.$p->__GET('nPedTipopago').'"]\').attr(\'selected\',\'selected\');
                    $(\'#Documento_Id option[value="'.$p->__GET('Documento_Id').'"]\').attr(\'selected\',\'selected\');
                    document.getElementById(\'dPedImporte\').value = "'.$p->__GET('dPedImporte').'";
                    document.getElementById(\'dPedIGV\').value = "'.$p->__GET('dPedIGV').'";
                    document.getElementById(\'dPedTotal\').value = "'.$p->__GET('dPedTotal').'";
                    document.getElementById(\'sPedObservaciones\').value = "'.$p->__GET('sPedObservaciones').'";
                    $(\'#nPedEstado option[value="'.$p->__GET('nPedEstado').'"]\').attr(\'selected\',\'selected\');
                    Habilitar();
                    ListarProVent();
                    $("#boton_cancelar").hide();
                    $("#boton_cerrar").show();
                 </script>';
        }
    }    
    
    public function CerrarPedido(){     
        $movimiento_data = array();
        $Pedido_Id = $_POST['Pedido_Id'];
        $pedidos = $this->pedido_model->Buscar($Pedido_Id);
        foreach ($pedidos as $p){
            $mov = new Movimiento();
            $mov->__SET('dMovFecha', $p->__GET('dPedFecha'));
            $mov->__SET('nMovTipoMovimiento_Id', 2);
            $mov->__SET('nMovTipopago', $p->__GET('nPedTipopago'));
            $mov->__SET('Almacen_Id', 1);
            $mov->__SET('nMovTipoOrigenDestino', 2);
            $mov->__SET('nMovOrigenDestino_Id', $p->__GET('OrigenDestino_Id'));
            $mov->__SET('nMovTipodestino', 1);
            $mov->__SET('Documento_Id', $p->__GET('Documento_Id'));
            $mov->__SET('sMovDocumento', $this->ObtenerNumeroDocumento($p->__GET('Documento_Id')));
            $mov->__SET('sMovDocReferencia', $Pedido_Id);
            $mov->__SET('Moneda_Id', 1);
            $mov->__SET('nMovTipoCambio', 1);
            $mov->__SET('nMovImporte', $p->__GET('dPedImporte'));
            $mov->__SET('nMovIGV', $p->__GET('dPedIGV'));
            $mov->__SET('nMovTotal', $p->__GET('dPedTotal'));
            $mov->__SET('nMovTotalCancelado', $p->__GET('nPedTipopago') == 1 ? $p->__GET('dPedTotal'):'0');
            $mov->__SET('dMovDetraccion', 0);
            $mov->__SET('dMovPercepcion', 0);
            $mov->__SET('dMovRetencion', 0);
            $mov->__SET('sMovObservacion', $p->__GET('sPedObservaciones'));
            $mov->__SET('nMovEstado', $p->__GET('nPedTipopago') == 1 ? '2':'1');
            $mov->__SET('nMovEliminado', 0);
            $mov->__SET('Usuario_Id', $_SESSION['usu_codigo']);
            $Movimiento_Id = $this->movimiento_model->Guardar($mov);
            /*DETALLE DE PEDIDO*/
            $detalle_pedido = $this->pedido_detalle_model->Buscar($p->__GET('Pedido_Id'));
            foreach ($detalle_pedido as $dp){
                $CantidadDescontar = 0;
                $CantidadRestante = 0;
                $Salir = false;
                if(count($this->receta_model->Buscar($dp->__GET('ProdServ_Id'))) > 0){
                    //echo "Con recetas".'<br>';
                    /*CON RECETAS*/
                    $recetas = $this->receta_model->Buscar($dp->__GET('ProdServ_Id'));
                    for($j=0;$j<count($recetas);$j++):
                        //echo '<br>precio ->'.$this->PrecioProductoVenta($recetas[$j]['ProdServ_Id'], $recetas[$j]['UnidadMedida_Id']).'<br>';
                        //echo $recetas[$j]['ProdServ_Id'].'<br>';
                        $productoslotes = $this->productolote_model->Listar($recetas[$j]['ProdServ_Id']); 
                        $CantidadRestante = $dp->__GET('nPedDetCantidad') * $recetas[$j]['nRecCantidad'];
                        //echo $recetas[$j]['nRecCantidad'].'<br>';
                        foreach($productoslotes as $lotes):
                            if($CantidadRestante <= $lotes->__GET('Stock')){
                                $CantidadDescontar = $CantidadRestante;
                                $Salir = true;
                            }else{
                                $CantidadDescontar = $lotes->__GET('Stock');
                                $CantidadRestante = $CantidadRestante - $lotes->__GET('Stock');
                            }                                                        
                            //echo $lotes->__GET('Lote').'<br>';
                            $NombreLote = $lotes->__GET('Lote');
                            $FechaVecimientoLote = $lotes->__GET('Vencimiento');
                            
                            $movdet = new Movimientodetalle();
                            $movdet->__SET('Movimiento_Id', $Movimiento_Id);
                            $movdet->__SET('TipoMovimiento_Id', 2);
                            $movdet->__SET('TipoDestino', 1);
                            $movdet->__SET('ProdServ_Id', $recetas[$j]['ProdServ_Id']);
                            $movdet->__SET('Almacen_Id', 1);
                            $movdet->__SET('Lote', $NombreLote);
                            $movdet->__SET('FechaVencimiento', $FechaVecimientoLote);
                            $movdet->__SET('Cantidad', $CantidadDescontar);
                            $movdet->__SET('Unidad_Id', $recetas[$j]['UnidadMedida_Id']);
                            $movdet->__SET('MonedaCompra_Id', 1);
                            $movdet->__SET('PrecioLista',  0);
                            $movdet->__SET('Descuento', 0);
                            $movdet->__SET('Precio', $this->PrecioProductoVenta($recetas[$j]['ProdServ_Id'], $recetas[$j]['UnidadMedida_Id']));
                            $movdet->__SET('SubTotal', ($dp->__GET('nPedDetCantidad') * $dp->__GET('dPedDetPrecioUnitario')));
                            $movdet->__SET('Usuario_Id', $_SESSION['usu_codigo']);
                            $movdet->__SET('TipoCambio', 1);
                            $movdet->__SET('nMovDetIncluyeIgv', 1);
                            $this->movimientodetalle_model->Guardar($movdet);
                            
                            if($Salir == true){
                                break;
                            }
                        endforeach;
                    endfor;
                }else{
                    /*SIN RECETAS*/
                    //echo "sin recetas";
                    $productoslotes = $this->productolote_model->Listar($dp->__GET('ProdServ_Id'));
                    $CantidadRestante = $dp->__GET('nPedDetCantidad');
                    foreach($productoslotes as $lotes):
                        if($CantidadRestante <= $lotes->__GET('Stock')){
                            $CantidadDescontar = $CantidadRestante;
                            $Salir = true;
                        }else{
                            $CantidadDescontar = $lotes->__GET('Stock');
                            $CantidadRestante = $CantidadRestante - $lotes->__GET('Stock');
                        }
                        
                        $NombreLote = $lotes->__GET('Lote');
                        $FechaVecimientoLote = $lotes->__GET('Vencimiento');
                        
                        $movdet = new Movimientodetalle();
                        $movdet->__SET('Movimiento_Id', $Movimiento_Id);
                        $movdet->__SET('TipoMovimiento_Id', 2);
                        $movdet->__SET('TipoDestino', 1);
                        $movdet->__SET('ProdServ_Id', $dp->__GET('ProdServ_Id'));
                        $movdet->__SET('Almacen_Id', 1);
                        $movdet->__SET('Lote', $NombreLote);
                        $movdet->__SET('FechaVencimiento', $FechaVecimientoLote);
                        $movdet->__SET('Cantidad', $CantidadDescontar);
                        $movdet->__SET('Unidad_Id', $dp->__GET('UnidadMedida_Id'));
                        $movdet->__SET('MonedaCompra_Id', 1);
                        $movdet->__SET('PrecioLista',  0);
                        $movdet->__SET('Descuento', 0);
                        $movdet->__SET('Precio', $dp->__GET('dPedDetPrecioUnitario'));
                        $movdet->__SET('SubTotal', ($dp->__GET('nPedDetCantidad') * $dp->__GET('dPedDetPrecioUnitario')));
                        $movdet->__SET('Usuario_Id', $_SESSION['usu_codigo']);
                        $movdet->__SET('TipoCambio', 1);
                        $movdet->__SET('nMovDetIncluyeIgv', 1);
                        $this->movimientodetalle_model->Guardar($movdet);
                        
                        if($Salir == true){
                            break;
                        }
                    endforeach;
                }
            }
            //echo $p->__GET('Documento_Id');
            $this->documento_model->Incrementa($p->__GET('Documento_Id'));
        }        
        $ped = new Pedido();
        $ped->__SET('Pedido_Id', $Pedido_Id);
        $this->pedido_model->Cerrar($ped);
//        echo ;
        echo '<script type="text/javascript">window.location="?c=Ventas&a=PrintVentas&Pedido='.$_POST['Pedido_Id'].'&Movimiento='.$Movimiento_Id.'";</script>';
    }
    public function EliminarPedido() {
         $ped = new Pedido();
         $ped->__SET('Pedido_Id', $_POST['IdPedido']);
         $this->pedido_model->Eliminar($ped);
         echo '<script type="text/javascript">window.location="?c=Ventas&a=Index";</script>';
    }
    public function EliminarSessiones(){
        unset($_SESSION['productos_v']);
    }
}
?>

