<?php
date_default_timezone_set('America/Lima');
require_once 'model/parametro.model.php';
require_once 'model/parametro.entidad.php';
require_once 'model/reportes.entidad.php';
require_once 'model/reportes.model.php';
require_once 'model/prodserv.entidad.php';
require_once 'model/prodserv.model.php';
//CLASE CON FUNCIONES PARA REPORTE EN EXCEL
require_once 'ClassExcel.php';
//LIBRERIA DE EXCEL
require_once 'view/librerias/PHPExcel/PHPExcel.php';

class ReportesController extends ClassExcel{

    private $reportes_model;
    private $prodserv_model;
    private $parametro_model;
    public $objPHPExcel;

    public function __CONSTRUCT() {
        $this->reportes_model = new ReportesModel();
        $this->prodserv_model = new ProdservModel();
        $this->parametro_model = new ParametroModel();
        $this->objPHPExcel = new PHPExcel();
    }

    public function Stock() {
        $parametro = $this->parametro_model->ListarParametros();
        $reportes = $this->reportes_model->Stock();
        require_once 'view/header.php';
        require_once 'view/webreport/wfrstock.php';
        echo "<script type='text/javascript'>
                        $(document).ready(function (){
                            $('.reportes').addClass('active');
                        });
                  </script>";
        require_once 'view/footer.php';
    }

    public function Kardex() {
        $parametro = $this->parametro_model->ListarParametros();
        $productos = $this->prodserv_model->Listar();
        $reportes = $this->reportes_model->Kardex(
                isset($_POST['ProdServ_Id']) ? $_POST['ProdServ_Id'] : 0, isset($_POST['fechainicio']) ? $_POST['fechainicio'] : date('d-m-Y', strtotime('-30 day', strtotime(date('d-m-Y')))), isset($_POST['fechafin']) ? $_POST['fechafin'] : date('d-m-Y', strtotime('+30 day', strtotime(date('d-m-Y'))))
        );
        require_once 'view/header.php';
        require_once 'view/webreport/wfrkardex.php';
        echo "<script type='text/javascript'>
                        $(document).ready(function (){
                            $('.reportes').addClass('active');
                        });
                  </script>";
        require_once 'view/footer.php';
    }

    public function VentaDetallada() {
        $parametro = $this->parametro_model->ListarParametros();
        $reportes = $this->reportes_model->VentaDetallada(
                isset($_POST['fechainicio']) ? $_POST['fechainicio'] : date('d-m-Y', strtotime('-30 day', strtotime(date('d-m-Y')))), isset($_POST['fechafin']) ? $_POST['fechafin'] : date('d-m-Y', strtotime('+30 day', strtotime(date('d-m-Y'))))
        );
        require_once 'view/header.php';
        require_once 'view/webreport/wfrventadetallada.php';
        echo "<script type='text/javascript'>
                        $(document).ready(function (){
                            $('.reportes').addClass('active');
                        });
                  </script>";
        require_once 'view/footer.php';
    }

    public function Stock_Excel() {
        $data = $_POST['data'];
        $this->SetPropiedades(["CLUB PACASMAYO","CLUB PACASMAYO","CLUB PACASMAYO","CLUB PACASMAYO","REPORTE DE STOCK","REPORTE DE STOCK"]);
        $this->SetTitulo("A1:G1", "A1", "REPORTE DE STOCK DEL ".date('d/m/Y H:m'));
        $this->HeaderCell(2, ["CODIGO","FAMILIA","PRODUCTO/SERVICIO","STOCK","UNIDAD","PRECIO","VALORIZADO"]);
        $this->Style_Titulo(count($data),range('A', 'G'));
//        $this->AjustarTextoCell('A2:G2');
        $this->AjustarColumnCell(range('A', 'G'));
        
        $this->Add_Cell_rptVentaDetallado(3, $data, range('A', 'G'));
        
        $this->WriteFormat('view/librerias/dist/excel/stock.xlsx');
    }
    public function Kardex_Excel(){
        $data = $_POST['data'];
        $this->SetPropiedades(["CLUB PACASMAYO","CLUB PACASMAYO","CLUB PACASMAYO","CLUB PACASMAYO","REPORTE DE KARDEX","REPORTE DE KARDEX"]);
        $this->SetTitulo("A1:N1", "A1", "REPORTE DE KARDEX DEL ".date('d/m/Y', strtotime($_POST['fechaInicio']))." AL ".date('d/m/Y', strtotime($_POST['fechaFin'])));
        $this->HeaderCell(2, ["FECHA HORA","TIPO MOVIMIENTO","ALMACEN","DOCUMENTO","FECHA DOCUMENTO","ORIGEN/DESTINO","ESTADO","LOTE","CANTIDAD","UNIDAD","SALDO ANTERIOR LOTE","SALDO POSTERIOR LOTE","SALDO ANTERIOR","SALDO POSTERIOR"]);
        $this->Style_Titulo(count($data),range('A', 'N'));
//        $this->AjustarTextoCell('A2:N2');
        $this->AjustarColumnCell(range('A', 'N'));
        $this->Add_Cell_rptVentaDetallado(3, $data, range('A', 'N'));
        
        $this->WriteFormat('view/librerias/dist/excel/kardex.xlsx');
    }
    public function VentaDetallada_Excel() {
        $data = $_POST['data'];
        $this->SetPropiedades(["CLUB PACASMAYO","CLUB PACASMAYO","CLUB PACASMAYO","CLUB PACASMAYO","REPORTE DE VENTAS DETALLADAS","VENTAS DETALLADAS"]);
        $this->SetTitulo("A1:N1", "A1", "VENTAS DETALLADAS DEL ".date('d/m/Y', strtotime($_POST['fechaInicio']))." AL ".date('d/m/Y', strtotime($_POST['fechaFin'])));
        $this->HeaderCell(2, ["FECHA","ALMACEN","DOCUMENTO","CONDICIÓN","CLIENTE","PRODUCTO","CANTIDAD","UNIDAD","PRECIO COSTO UNITARIO SIN IGV","PRECIO VENTA UNITARIO SIN IGV","MONEDA","IMPORTE VENTA","UTILIDAD","PORCENTAJE RENTABILIDAD"]);
        $this->Style_Titulo(count($data),range('A', 'N'));
//        $this->AjustarTextoCell('A2:N2');
        $this->AjustarColumnCell(range('A', 'N'));
        
        $this->Add_Cell_rptVentaDetallado(3, $data, range('A', 'N'));
        
        $this->WriteFormat('view/librerias/dist/excel/detalle_ventas.xlsx');
    }
    public function Creditos_Excel(){
        
        $data = json_decode($_POST['data'], TRUE);
        
        $this->SetPropiedades(["CLUB PACASMAYO","CLUB PACASMAYO","CLUB PACASMAYO","CLUB PACASMAYO","REPORTE DE CREDITOS","CREDITOS"]);
        $this->SetTitulo("A1:H1", "A1", "CREDITOS DEL ".date('d/m/Y', strtotime($_POST['fechaInicio']))." AL ".date('d/m/Y', strtotime($_POST['fechafin'])));
        $this->HeaderCell(2, ["TIPO MOVIMIENTO","FECHA","DOCUMENTO","CLIENTE/PROVEEDOR","MONEDA","TOTAL","POR PAGAR", "ESTADO"]);
        $this->Style_Titulo(count($data),range('A', 'H'));
        
        $this->AjustarColumnCell(range('A', 'H'));
        
        $this->Add_Cell_rptVentaDetallado(3, $data, range('A', 'H'));
        
        $this->WriteFormat('view/librerias/dist/excel/creditos.xlsx');
        
    }
}

?>