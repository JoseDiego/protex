<?php    
    require_once 'model/parametro.model.php';
    require_once 'model/parametro.entidad.php';
    require_once 'model/unidadmedida.model.php';
    require_once 'model/unidadmedida.entidad.php';
    require_once 'model/familia.model.php';
    require_once 'model/familia.entidad.php';
    require_once 'model/prodserv.model.php';
    require_once 'model/prodserv.entidad.php';
    require_once 'model/origendestino.model.php';
    require_once 'model/origendestino.entidad.php';
    require_once 'model/activo.model.php';
    require_once 'model/activo.entidad.php';
    require_once 'model/precioventa.model.php';
    require_once 'model/precioventa.entidad.php';
    require_once 'controller/receta.controller.php';
    require_once 'controller/metodos.controller.php';
    class ProductosController extends MetodosController{
        private $parametro_model;
        public $unidadmedida_model;
        private $familia_model;
        private $prodserv_model;
        private $activo_model;
        public $precioventa_model;
        public $origendestino_model;
        private $receta_controller;
        public function __CONSTRUCT(){
            $this->parametro_model = new ParametroModel();
            $this->unidadmedida_model = new UnidadmedidaModel();
            $this->familia_model = new FamiliaModel();
            $this->prodserv_model = new ProdservModel();
            $this->activo_model = new ActivoModel();
            $this->origendestino_model = new OrigendestinoModel();
            $this->precioventa_model = new PrecioventaModel();
            $this->receta_controller = new RecetaController();
        }
        
        public function Index(){
            $parametro = $this->parametro_model->ListarParametros();
            $productos = $this->prodserv_model->Listar();
            require_once 'view/header.php';
            require_once 'view/webforms/wfaproductos.php';
            echo "<script type='text/javascript'>
                        $(document).ready(function (){
                            $('.catalogos').addClass('active');
                        });
                  </script>";
            require_once 'view/footer.php';
        }
        public function ConfigurarPrecio(){            
            $parametro = $this->parametro_model->ListarParametros();
            $productos = $this->prodserv_model->Listar();
            require_once 'view/header.php';
            require_once 'view/webforms/wfaconfigurarprecio.php';
            echo "<script type='text/javascript'>
                        $(document).ready(function (){
                            $('.operaciones').addClass('active');
                        });
                  </script>";
            require_once 'view/footer.php';
        }
        public function GuardarProdserv(){
            $pro = new Prodserv();
            if($_POST['ProdServ_Id'] != 0 ? $pro->__SET('ProdServ_Id', $_POST['ProdServ_Id']) : ''){}else{}
            
            $pro->__SET('sProSrvCodigo1', $_POST['sProSrvCodigo1']);
            $pro->__SET('sProSrvCodigo2', $_POST['sProSrvCodigo2']);
            $ProSrvCodigo=$pro->sProSrvCodigo1.'-'.$pro->sProSrvCodigo2;
            $pro->__SET('sProSrvCodigo', $ProSrvCodigo);
            $pro->__SET('sProSrvNombre', $_POST['sProSrvNombre']);
            $pro->__SET('sProSrvCodigoBarras', $_POST['sProSrvCodigoBarras']);
            $pro->__SET('nProSrvFamilia_Id', $_POST['Familia_Id']);
            $pro->__SET('Activo_Id', $_POST['Activo_Id']);
            $pro->__SET('nProSrvUnidad_Id', $_POST['Unidadmedida_Id']);
            $pro->__SET('ProSrvEspecificacion', $_POST['ProSrvEspecificacion']);
            $pro->__SET('nProSrvExoneradoIGV', $_POST['nProSrvExoneradoIGV']);
            $pro->__SET('nProSrvEstado', $_POST['nProSrvEstado']);
            $pro->__SET('nProSrvEliminado', 0);
            $pro->__SET('Usuario_Id', $_SESSION['usu_codigo']);
            if($_POST['ProdServ_Id'] != 0){//ACTUALIZAR
                $this->prodserv_model->Editar($pro);//USAMOS MISMO CODIGO PARA MODIFICAR foto
                if( !empty( $_FILES['files']['name'] ) ){
                    $_SESSION['sProSrvCodigo']=$pro->sProSrvCodigo;

                    $codProducto= $_SESSION['sProSrvCodigo'];
                    $foto = $codProducto.'.jpg';
                    if (file_exists('view/librerias/dist/productimg/' . $foto)) {
                        unlink('view/librerias/dist/productimg/' . $foto);
                    }
                    $fotoant = $codProducto.'-.jpg';
                    $foto2 = $codProducto.'.jpg';
                    move_uploaded_file ($_FILES['files']['tmp_name'], 'view/librerias/dist/productimg/' . $fotoant);
                    $size=getimagesize('view/librerias/dist/productimg/' . $fotoant);
                    $reescalando=imagecreatefromjpeg('view/librerias/dist/productimg/' . $fotoant);
                    $new_imagen = imagecreatetruecolor(300, 300);
                    $imagenes = imagecopyresized($new_imagen,$reescalando, 0, 0, 0, 0, 300 , 300,
                    $size[0],$size[1]);
                    imagecopyresampled($new_imagen, $reescalando, 0, 0, 300, 300, 0, 0, 0, 0);
                    imagejpeg($new_imagen,'view/librerias/dist/productimg/'.$foto2);
                    unlink('view/librerias/dist/productimg/' . $fotoant);
                    $this->prodserv_model->ActualizarFotoProdserv($codProducto,$foto2);
                }
                $parametro = $this->parametro_model->ListarParametros();
                $productos = $this->prodserv_model->Listar();
                require_once 'view/header.php';
                require_once 'view/webforms/wfaproductos.php';
                echo "<script type='text/javascript'>
                            $(document).ready(function (){
                                $('.catalogos').addClass('active');
                            });
                            alert('Su producto se registro correctamente');
                      </script>";
                require_once 'view/footer.php';
            }else{//GUARDAR
                $_SESSION['sProSrvCodigo']=$pro->sProSrvCodigo;
                $ProdServ_Id = $this->prodserv_model->guardar($pro);
                if( !empty( $_FILES['files']['name'] ) ){
                    $codProducto= $_SESSION['sProSrvCodigo'];
                    $foto = $codProducto.'-.jpg';
                    $foto2 = $codProducto.'.jpg';
                    move_uploaded_file ($_FILES['files']['tmp_name'], 'view/librerias/dist/productimg/' . $foto);
                    $size=getimagesize('view/librerias/dist/productimg/' . $foto);
                    $reescalando=imagecreatefromjpeg('view/librerias/dist/productimg/' . $foto);
                    $new_imagen = imagecreatetruecolor(300, 300);
                    $imagenes = imagecopyresized($new_imagen,$reescalando, 0, 0, 0, 0, 300 , 300,
                    $size[0],$size[1]);
                    imagecopyresampled($new_imagen, $reescalando, 0, 0, 300, 300, 0, 0, 0, 0);
                    imagejpeg($new_imagen,'view/librerias/dist/productimg/'.$foto2);
                    unlink('view/librerias/dist/productimg/' . $foto);
                    $convBytes=file_get_contents('view/librerias/dist/productimg/' . $foto2);
                    $this->prodserv_model->ActualizarFotoProdserv($codProducto,$foto2);
                }
                $parametro = $this->parametro_model->ListarParametros();
                $productos = $this->prodserv_model->Listar();
                require_once 'view/header.php';
                require_once 'view/webforms/wfaproductos.php';
                echo "<script type='text/javascript'>
                            $(document).ready(function (){
                                $('.catalogos').addClass('active');
                            });
                            alert('Su producto se registro correctamente');
                      </script>";
                require_once 'view/footer.php';
            }            
            $this->receta_controller->EliminarReccetasSeleccionadas();
            $this->receta_controller->GuardarReceta($_POST['ProdServ_Id'] == 0 ? $ProdServ_Id:$_POST['ProdServ_Id']);
            $this->EliminarSession();
        }

        public function EliminarProductos(){
            $pro = new Prodserv();
            $pro->__SET('ProdServ_Id', $_POST['idProdServ']);
            $this->prodserv_model->Eliminar($pro);
            echo '<script type="text/javascript">window.location="?c=Productos&a=Index";</script>';
        }

        function ActualizarFotoProdserv(){
            $pro = new Prodserv();
            if( !empty( $_FILES['ProSrvImagen']['tmp_name'] ) ){
                $codProducto= $_SESSION['sProSrvCodigo'];
                $foto = date('ymdhis') . '-' . strtolower($_FILES['ProSrvImagen']['name']);
                move_uploaded_file ($_FILES['ProSrvImagen']['tmp_name'], 'view/librerias/dist/productimg/' . $foto);
                $convBytes=file_get_contents('view/librerias/dist/productimg/' . $foto);
                $this->prodserv_model->ActualizarFotoProdserv($codProducto,$convBytes,$foto);
                $parametro = $this->parametro_model->ListarParametros();
                $productos = $this->prodserv_model->Listar();
                require_once 'view/header.php';
                require_once 'view/webforms/wfaproductos.php';
                echo "<script type='text/javascript'>
                            $(document).ready(function (){
                                $('.catalogos').addClass('active');
                                alert('La imagen de su producto se registro correctamente');
                            });
                      </script>";
                require_once 'view/footer.php';
            }else{
                $parametro = $this->parametro_model->ListarParametros();
                $productos = $this->prodserv_model->Listar();
                require_once 'view/header.php';
                require_once 'view/webforms/wfaproductos.php';
                echo "<script type='text/javascript'>
                            $(document).ready(function (){
                                $('.catalogos').addClass('active');
                            });
                      </script>";
                require_once 'view/footer.php';
            }
        }
        public function ListarComboBoxProveedor(){
            $clientes= $this->origendestino_model->Listar();
            echo '<option value="0">Seleccionar Proveedor</option>';
            //if ($_POST['OrigenDestino_Id']==0) {
                foreach ($clientes as $c){
                    if($c->__GET('nODTipo')==1){
                        echo '<option value="'.$c->__GET('OrigenDestino_Id').'" '.($_POST['OrigenDestino_Id'] != 0 ? $c->__GET('OrigenDestino_Id') == $_POST['OrigenDestino_Id'] ? 'selected':'':'').'>'.$c->__GET('sODNombre').'</option>';
                    }
                }
            //}
        }
        public function FrmEditarProductos(){
            $this->EliminarSession();
            $productos = $this->prodserv_model->Listar();
            $familia = $this->familia_model->Listar();
            $unidadmedida = $this->unidadmedida_model->Listar();
            $activos = $this->activo_model->Listar();
            $datos= $this->prodserv_model->Buscar($_POST['ProdServ_Id']);
            $this->receta_controller->ListaEditar($_POST['ProdServ_Id']);
            foreach ($datos as $r):
                require_once 'view/webforms/wfaproductosa.php';            
            endforeach;       
        }
        public function FrmGuardarProductos(){
            $this->EliminarSession();
            $productos = $this->prodserv_model->Listar();
            $familia = $this->familia_model->Listar();
            $unidadmedida = $this->unidadmedida_model->Listar();
            $proveedor = $this->origendestino_model->ListarProveedor();
            $activos = $this->activo_model->Listar();
            require_once 'view/webforms/wfaproductosa.php'; 
        }
        public function MostrarProducto(){
            $producto = $this->prodserv_model->Buscar($_POST['ProdServ_Id']);
            foreach ($producto as $r):
                echo " <tbody>
                          <tr>
                             <td>
                                <input type='hidden' name='ProdServ_IdPri' id='ProdServ_IdPri' value='".$r->__GET('ProdServ_Id')."'> 
                                Codigo
                             </td>
                             <td>".$r->__GET('sProSrvCodigo')."</td>
                          </tr>

                          <tr>
                             <td>Nombre</td>
                             <td>".$r->__GET('sProSrvNombre')."</td>
                          </tr>  
                          <tr>
                             <td>Codigo barra</td>
                             <td>".$r->__GET('sProSrvCodigoBarras')."</td>
                          </tr>  
                          <tr>
                             <td>Activo</td>
                             <td>".$r->__GET('sActDescripcion')."</td>
                          </tr>  
                          <tr>
                             <td>Unidad de medida</td>
                             <td>".$r->__GET('sUndDescripcion')."</td>
                          </tr>  
                          <tr>
                             <td>Exonerado de IGV</td>
                             <td>".($r->__GET('nProSrvExoneradoIGV') == 1 ? 'Si' :'No')."</td>
                          </tr>  
                          <tr>
                             <td>Estado</td>
                             <td>".($r->__GET('nProSrvEstado') == '1' ? '<span class="label bg-green">Habilitado</span>' : '<span class="label bg-danger">Inhabilitado</span>')."</td>
                          </tr>
                          <tr>
                             <td>Descripción</td>
                             <td>".$r->__GET('ProSrvEspecificacion')."</td>
                          </tr> 
                          <tr>
                             <td>Imagen</td>";
                    echo '
                             <td><img width="30%" src="view/librerias/dist/productimg/'.$r->__GET("ProSrvNomImagen").'"/></td>
                          </tr> 
                       </tbody>
                     ';
            endforeach;
        }        
        public function EstadoPrecioUnidad($ProdServ_Id, $Unidadmedida_Id){
            $estado = false;
            $precio = $this->precioventa_model->Precio($ProdServ_Id, $Unidadmedida_Id);
            if(count($precio)>0){
                $estado = true;
            }
            return $estado;
        }

        public function ListarUnidades(){    
            $unidad = $this->unidadmedida_model->ListarUP($this->ObtenerID($_POST['ProdServ_Id']));
            echo "<option value=''>Seleccione unidad</option>";
            foreach ($unidad as $uni){
                if($this->EstadoPrecioUnidad($_POST['ProdServ_Id'], $uni->__GET('UnidadMedida_Id')) == true){
                    echo "<option value='".$uni->__GET('UnidadMedida_Id')."'>".$uni->__GET('sUndDescripcion')."</option>";
                }
            }
        } 
        public function FiltroProducto(){
            if(!empty($_POST['txtNameProduct'])){
                $productos = $this->prodserv_model->Filtrar($_POST['txtNameProduct']);
                foreach ($productos as $p){
                    echo '<div class="col-md-4 margin-bottom">
                            <button class="btn '.($p->__GET('nProSrvEstado')==0 ? 'btn-danger disabled':'btn-success').' btn-flat btn-producto" style="width: 100%; height: 120px;" onclick="producto_detalle(\''.$p->__GET('ProdServ_Id').'\')" data-dismiss="modal" data-toggle="modal" data-target="#productos_detalle">
                                <i class="fa fa-shopping-cart"></i><br>
                                <p style="font-size: 14px; width: 100%; display: table;">'.$p->__GET('sProSrvNombre').'</p>
                            </button>
                        </div>';
                }
            }else{
                foreach ($this->familia_model->Hijos(2) as $f) {
                    echo '<div class="col-md-4 margin-bottom">
                            <button class="btn '.($f->__GET('nFamEstado') == 0 ? 'btn-danger disabled' : 'btn-warning').' btn-flat btn-producto" style="width: 100%;" onclick="FiltrarProductos(\''.$f->__GET('Familia_Id').'\');" data-dismiss="modal" data-toggle="modal" data-target="#productos">
                                <i class="fa fa-info-circle"></i><br>
                                <p style="font-size: 14px;">'.$f->__GET('sFamDescripcion').'</p>
                            </button>
                        </div>';
                }
            }
        }
        public function ObtenerID($ProdServ_Id){
            foreach ($this->prodserv_model->Buscar($ProdServ_Id) as $p){
                return $p->__GET('nProSrvUnidad_Id');
            }
        }  
        public function EliminarSession(){
            unset($_SESSION['recetas']);
            unset($_SESSION['recetas_delete']);
        }
        public function UltimoCodigo(){
            echo !empty($_POST['Familia_Id']) ? $this->prodserv_model->UltimoCodigo($_POST['Familia_Id']) : '0-0';
        }
        
        public function VerificarStock(){
            print_r(json_encode($this->prodserv_model->VerificarStock($_POST['ProdServ_Id'],$_POST['nPedDetCantidad'],$_POST['Unidadmedida_Id'])));
        }

        public function PDFProductos(){            
            $datos= $this->prodserv_model->Buscar($_POST['ProdServ_IdPri']);
            require_once 'view/webforms/PDF/productos.php';
        }

    }
?>