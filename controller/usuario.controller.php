<?php    
    require_once 'model/usuario.entidad.php';
    require_once 'model/usuario.model.php';
    require_once 'model/parametro.model.php';
    require_once 'model/parametro.entidad.php';
    class UsuarioController {
        private $usuario_model;
        private $parametro_model;
        public function __CONSTRUCT(){
            $this->usuario_model = new UsuarioModel();
        $this->parametro_model = new ParametroModel();
        }
        
        public function Index(){
            $parametro = $this->parametro_model->ListarParametros();
            $usuarios= $this->usuario_model->Listar();
            require_once 'view/header.php';  
            require_once 'view/webforms/wfausuario.php';
            echo "<script type='text/javascript'>
                        $(document).ready(function (){
                            $('.catalogos').addClass('active');
                        });
                  </script>";
            require_once 'view/footer.php'; 
        }
        public function FrmGuardarUsuario(){
            require_once 'view/webforms/wfausuarioa.php'; 
        }
        
        public function FrmEditarUsuario(){
            $obtenerUsuario= $this->usuario_model->Buscar($_POST['Usuario_Id']);
            foreach ($obtenerUsuario as $r):
                require_once 'view/webforms/wfausuarioa.php';            
            endforeach;       
        }
        public function MostrarUsuario(){
            $usuarios= $this->usuario_model->Buscar($_POST['Usuario_Id']);
            foreach ($usuarios as $r):
                if($r->__GET('nUsuTipo')==1){
                    $tipo="Administrador";
                }elseif ($r->__GET('nUsuTipo')==2) {
                    $tipo="Asistente";
                }else{
                    $tipo="Reportes";
                }
                echo " <tbody>
                          <tr>
                             <td>
                                <input type='hidden' name='idUsuario' id='idUsuario' value='".$r->__GET('Usuario_Id')."'> 
                                Nombre de Usuario
                             </td>
                             <td>".$r->__GET('sUsuNombre')."</td>
                          </tr>
                          <tr>
                             <td>Login</td>
                             <td>".$r->__GET('sUsuLogin')."</td>
                          </tr>  
                          <tr>
                             <td>Tipo de Usuario</td>
                             <td>".$tipo."</td>
                          </tr>  
                          <tr>
                             <td>Estado</td>
                             <td>".($r->__GET('nUsuEstado') == '1' ? '<span class="label bg-green">Habilitado</span>' : '<span class="label bg-danger">Inhabilitado</span>')."</td>
                          </tr>
                       </tbody>
                     ";
            endforeach;
        }
        public function GuardarUsuario(){            
            $usu = new Usuario();
            if($_POST['idRegistro'] != 0 ? $usu->__SET('Usuario_Id',   $_POST['idRegistro']) : '');
            $usu->__SET('sUsuNombre', $_POST['nombre']);                
            $usu->__SET('sUsuLogin', $_POST['login']);
            $usu->__SET('sUsuContrasena', $_POST['contraseñaTwo']);
            $usu->__SET('nUsuTipo', $_POST['tipo']);
            $usu->__SET('nUsuEstado', $_POST['estado']);
            $usu->__SET('nUsuEliminado', 0);
            $usu->__SET('nUsuUsuarioOwn_Id', $_SESSION['usu_codigo']);
            if($_POST['idRegistro'] == 0 ? $this->usuario_model->Guardar($usu) : $this->usuario_model->Editar($usu));
        }
        public function EliminarUsuario(){
            $usu = new Usuario();
            $usu->__SET('Usuario_Id', $_POST['idUsuario']);
            $this->usuario_model->Eliminar($usu);
            echo '<script type="text/javascript">window.location="?c=Usuario&a=Index";</script>';
            
        }
    }
?>