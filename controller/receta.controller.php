<?php
require_once 'model/receta.entidad.php';
require_once 'model/receta.model.php';
require_once 'model/unidadmedida.entidad.php';
require_once 'model/unidadmedida.model.php';
require_once 'model/prodserv.entidad.php';
require_once 'model/prodserv.model.php';
require_once 'controller/metodos.controller.php';
class RecetaController extends MetodosController{
    private $receta_model;
    public $unidadmedida_model;
    public $prodserv_model;
    public function __CONSTRUCT() {
        $this-> receta_model = new recetaModel();
        $this-> unidadmedida_model = new UnidadmedidaModel();
        $this-> prodserv_model = new ProdservModel();
    }
    public function Index() {
        //INGRESAR Y LLAMAR A LAS VISTAR PARA EL INDEX
    }
    public function GuardarReceta($ProdServ_Id) {        
        $array = isset($_SESSION['recetas']) ? $_SESSION['recetas'] : array();
        for($i=0;$i<count($array);$i++){
            $rec = new Receta();
            //$rec->__SET('Receta_Id', $array[$i]['Receta_Id']);
            $rec->__SET('nRecProdServ_Id', $array[$i]['ProdServ_Id']);
            $rec->__SET('UnidadMedida_Id', $array[$i]['UnidadMedida_Id']);
            $rec->__SET('nRecCantidad', $array[$i]['nRecCantidad']);
            $rec->__SET('nRecEliminado', 0);
            $rec->__SET('Usuario_Id', $_SESSION['usu_codigo']);
            $rec->__SET('ProdServ_Id', $ProdServ_Id);
            $array[$i]['Receta_Id'] != 0 ? '' : $this->receta_model->Crear($rec);
        }
    }
    public function FrmReceta() {
        //FORMULARIO
    }
    public function FrmRecetaEditar() {
        //FORMULARIO EDITAR
    }
    public function EliminarReceta() {
        $array = isset($_SESSION['recetas']) ? $_SESSION['recetas'] : array();
        $nueva_array = array();
        for($i=0;$i<count($array);$i++){      
            if($i!=$_POST['post']){
                $nueva_array[] = array("Receta_Id"       => $array[$i]['Receta_Id'],
                                        "ProdServ_Id"     => $array[$i]['ProdServ_Id'],
                                        "UnidadMedida_Id" => $array[$i]['UnidadMedida_Id'],
                                        "nRecCantidad"    => $array[$i]['nRecCantidad']);
            }elseif($array[$i]['Receta_Id'] != 0){
                $array_delete[] = array("Receta_Id"       => $array[$i]['Receta_Id'],
                                        "ProdServ_Id"     => $array[$i]['ProdServ_Id'],
                                        "UnidadMedida_Id" => $array[$i]['UnidadMedida_Id'],
                                        "nRecCantidad"    => $array[$i]['nRecCantidad']);
            }
        }
        $_SESSION['recetas'] = $nueva_array;
        $_SESSION['recetas_delete'] = $array_delete;
    }
    public function ListaEditar($ProdServ_Id){
        $_SESSION['recetas'] = $this->receta_model->Buscar($ProdServ_Id);
    }
    public function AgregarReceta(){
        $data = array();
        $array = array("Receta_Id"       => isset($_POST['Receta_Id']) ? $_POST['Receta_Id']:'0',
                       "ProdServ_Id"     => $_POST['ProdServ_Id'],
                       "UnidadMedida_Id" => $_POST['UnidadMedida_Id'],
                       "nRecCantidad"    => $_POST['nRecCantidad']);
        if(isset($_SESSION['recetas'])){
            $data = $_SESSION['recetas'];
            array_push($data, $array);
        }else{
            $data[] = $array;
        }
        $_SESSION['recetas'] = $data;
    }
    public function ListarReceta(){
        $array = isset($_SESSION['recetas']) ? $_SESSION['recetas'] : array();
        if(count($array)>0){
            for($i=0;$i<count($array);$i++){
                echo '<tr>
                        <td style="width: 30%;">'.$this->NombreProdServ($array[$i]['ProdServ_Id']).'</td>
                        <td style="width: 30%;">'.$this->NombreUnidadMedidaProducto($array[$i]['UnidadMedida_Id']).'</td>
                        <td style="width: 20%;">'.$array[$i]['nRecCantidad'].'</td>
                        <td style="width: 20%;">
                            <a href="#" class="btn btn-danger btn-flat btn-xs" data-toggle="modal" data-target="#modal-eliminar" data-codigo="'.$i.'"><i class="fa fa-trash-o"></i></a>
                        </td>
                     </tr>';
            }
        }else{
            echo '<tr>
                    <td colspan="4">No existe niguna receta registrada</td>
                  </tr>';
        }
    }
    public function EliminarReccetasSeleccionadas(){
        $array = isset($_SESSION['recetas_delete']) ? $_SESSION['recetas_delete'] : array();
        for($i=0;$i<count($array);$i++){
            $rec = new Receta();
            $rec->__SET('Receta_Id', $array[$i]['Receta_Id']);
            $this->receta_model->Eliminar($rec);
        }
    }
}
?>
