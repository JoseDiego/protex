<?php
require_once 'model/precioventa.entidad.php';
require_once 'model/precioventa.model.php';
class PrecioventaController {
    private $precioventa_model;
    public function __CONSTRUCT() {
        $this-> precioventa_model = new precioventaModel();
    }
    public function Index() {
        //INGRESAR Y LLAMAR A LAS VISTAR PARA EL INDEX
    }
    public function GuardarPrecioventa() {        
        $pre = new Precioventa();
        if($_POST['PrecioVenta_Id'] != 0 ? $pre->__SET('PrecioVenta_Id', $_POST['PrecioVenta_Id']) : '');
        $pre->__SET('ProdServ_Id', $_POST['ProdServ_Id']);
        $pre->__SET('UnidadMedida_Id', $_POST['Unidadmedida_Id']);
        $pre->__SET('Moneda_Id', $_POST['Moneda_Id']);
        $pre->__SET('PreVenPrecio', $_POST['PreVenPrecio']);
        $pre->__SET('nPreVenEliminado', 0);
        $pre->__SET('Usuario_Id', $_SESSION['usu_codigo']);            
        if($_POST['PrecioVenta_Id'] != 0){
            $this->precioventa_model->Editar($pre);
        }else{
            if($this->ConsultarUnidadMedida($_POST['ProdServ_Id'],$_POST['Moneda_Id'],$_POST['Unidadmedida_Id']) == false){
                $this->precioventa_model->Crear($pre);
            }else{
                echo '<div class="alert alert-warning" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Advertencia</strong>, La unidad, moneda o operación seleccionada ya existe.
                  </div>'; 
            }            
        }
    }
    
    public function PrecioProducto(){
        $lista = $this->precioventa_model->Precio($_POST['ProdServ_Id'],$_POST['Unidadmedida_Id']);
        if(count($lista)>0){
            foreach ($lista as $r){
                echo "<script type='text/javascript'>
                        $('#nMovDetConPrecioUnitario').val('" .$r->__GET('PreVenPrecio'). "');
                        
                    </script>";
                
            }
        }else{
            echo "<re>
                    <td colspan='3'>No existe ningun dato de este producto</td>
                </tr>";
        }
    }
    public function ListarPrecios() {
        $lista = $this->precioventa_model->Listar($_POST['codigo'],0);
        if(count($lista)>0){
            foreach ($lista as $r){
                if($r->__GET('Moneda_Id')== 1){$moneda='Soles (S/)';}else{$moneda='Dolares ($)';}
                echo "<tr>
                        <td>".$r->__GET('sUndDescripcion')."</td>
                        <td>".$r->__GET('PreVenPrecio')."</td>
                        <td>".$moneda."</td>
                        <td>
                            <button type='button' class='btn btn-danger btn-xs btn-flat' title='Eliminar' data-toggle='modal' data-target='#modal-eliminar-precioventa' data-codigopv='".$r->__GET('PrecioVenta_Id')."'><i class='fa fa-trash-o'></i></button>
                        </td>
                    </tr>";
            }
        }else{
            echo "<re>
                    <td colspan='3'>No existe ningun dato de este producto</td>
                </tr>";
        }
    }
    public function FrmPrecioventaEditar() {
        //FORMULARIO EDITAR
    }
    public function EliminarPrecioventa() {
         $pre = new Precioventa();
         $pre->__SET('PrecioVenta_Id', $_POST['IdPrecioVenta']);
         $this->precioventa_model->Eliminar($pre);
         echo '<script type="text/javascript">window.location="?c=Productos&a=Index";</script>';
    }
    public function ConsultarUnidadMedida($codigo,$moneda,$unidad){
        $estado = false;
        foreach ($this->precioventa_model->Listar($codigo,0) as $r){
            if($unidad == $r->__GET('UnidadMedida_Id') && $moneda == $r->__GET('Moneda_Id')){
                $estado = true;
            }
        }
        return $estado;
    }
    
    
    public function PrecioProductoVenta(){
        $lista = $this->precioventa_model->Listar($_POST['ProdServ_Id']);
        if(count($lista)>0){
            foreach ($lista as $r){
                if($_POST['Unidadmedida_Id']==$r->__GET('UnidadMedida_Id') && $_POST['Moneda_Id']==$r->__GET('Moneda_Id')){
                echo "<script type='text/javascript'>
                        $('#nMovDetConPrecioUnitario').val('" .$r->__GET('PreVenPrecio'). "');
                        
                    </script>";
                
            }}
        }else{
            echo "<re>
                    <td colspan='3'>No existe ningun dato de este producto</td>
                </tr>";
        }
    }    
}
?>
