<?php
require_once 'model/pago.entidad.php';
require_once 'model/pago.model.php';
require_once 'model/parametro.entidad.php';
require_once 'model/parametro.model.php';
class PagoController {
    private $pago_model;
    private $parametro_model;
    public function __CONSTRUCT() {
        $this-> pago_model = new pagoModel();
        $this-> parametro_model = new ParametroModel();
    }
    public function Index() {
        $parametro = $this->parametro_model->ListarParametros();
        $pagos= $this->pago_model->Listar($_GET['Movimiento_Id']);
        require_once 'view/header.php';  
        require_once 'view/webforms/wfapagos.php';
        echo "<script type='text/javascript'>
                    $(document).ready(function (){
                        $('.operaciones').addClass('active');
                    });
              </script>";
        require_once 'view/footer.php'; 
    }
    public function GuardarPago() {
        $pag = new Pago();
        if($_POST['Pago_Id'] != 0 ? $pag->__SET('Pago_Id', $_POST['Pago_Id']) : '');
        $pag->__SET('Movimiento_Id', $_POST['Movimiento_Id']);
        $pag->__SET('dPagFecha', $_POST['dPagFecha']);
        $pag->__SET('nPagTipoPago_Id', $_POST['nPagTipoPago_Id']);
        $pag->__SET('sPagDocumento', $_POST['sPagDocumento']);
        $pag->__SET('nPagImporte', $_POST['nPagImporte']);
        $pag->__SET('nPagLetra_Id', $_POST['nPagLetra_Id']);
        $pag->__SET('dPagFecha_Act', $_POST['dPagFecha_Act']);
        $pag->__SET('nPagUsuario_Id', $_POST['nPagUsuario_Id']);
        $pag->__SET('nPagEliminado', $_POST['nPagEliminado']);
        if($_POST['Pago_Id'] != 0 ? $this->pago_model->Editar($pag) : $this->pago_model->Crear($pag));
    }
    public function FrmPago() {
        //FORMULARIO
    }
    public function FrmPagoEditar() {
        //FORMULARIO EDITAR
    }
    public function EliminarPago() {
         $pag = new Pago();
         $pag->__SET('Pago_Id', $_POST['IdPago']);
         $this->pago_model->Eliminar($pag);
         echo '<script type="text/javascript">window.location="?c=Pago&a=Index";</script>';
    }
    public function TipoPago($nPagTipoPago_Id){
        if ($nPagTipoPago_Id == 1) {
            return 'Efectivo';
        }elseif ($nPagTipoPago_Id == 2) {
            return 'Bacno-Deposito';
        }elseif ($nPagTipoPago_Id == 3) {
            return 'Bacno-Transferencia';
        }elseif ($nPagTipoPago_Id == 4) {
            return 'Cheque';
        }elseif ($nPagTipoPago_Id == 5) {
            return 'Letra';
        }elseif ($nPagTipoPago_Id == 6) {
            return 'Nota de credito';
        }
    }
}
?>
