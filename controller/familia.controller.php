<?php    
    require_once 'model/parametro.model.php';
    require_once 'model/parametro.entidad.php';
    require_once 'model/familia.model.php';
    require_once 'model/familia.entidad.php';
    class FamiliaController {
        private $parametro_model;
        private $familia_model;
        public function __CONSTRUCT(){
            $this->parametro_model = new ParametroModel();
            $this->familia_model = new FamiliaModel();
        }
        
        public function Index(){
            $familia = $this->familia_model->Listar();
            $parametro = $this->parametro_model->ListarParametros();
            require_once 'view/header.php';
            require_once 'view/webforms/wfafamilias.php';
            echo "<script type='text/javascript'>
                        $(document).ready(function (){
                            $('.catalogos').addClass('active');
                        });
                  </script>";
            require_once 'view/footer.php';
        }
        public function FrmFamilia(){
            $familia = $this->familia_model->Listar();
            require 'view/webforms/wfafamiliasa.php';
        }
        public function FrmEditarFamilia(){
            $familia = $this->familia_model->Listar();
            $datos = $this->familia_model->Buscar($_POST['Familia_Id']);
            foreach ($datos as $r){                
                require 'view/webforms/wfafamiliasa.php';
            }
        }
        public function GuardarFamilia(){
            $fam = new Familia();
            if($_POST['Familia_Id'] != 0 ? $fam->__SET('Familia_Id', $_POST['Familia_Id']) : '');
            $fam->__SET('sFamCodigo', $_POST['sFamCodigo']);
            $fam->__SET('sFamDescripcion', $_POST['sFamDescripcion']);
            $fam->__SET('nFamPadre', $_POST['nFamPadre']);
            $fam->__SET('nFamTipo', $_POST['nFamTipo']);
            $fam->__SET('nFamEstado', $_POST['nFamEstado']);
            $fam->__SET('nFamEliminado', 0);
            $fam->__SET('Usuario_Id', $_SESSION['usu_codigo']);
            if($_POST['Familia_Id'] != 0 ? $this->familia_model->Editar($fam) : $this->familia_model->guardar($fam));
        }
        public function ListarArbolContenido(){
            $familia = $this->familia_model->Listar();
            $ex = array();
            foreach ($familia as $arb){
                $ex[] = array('id' => $arb->__GET('Familia_Id'),
                              'codigo' => $arb->__GET('sFamCodigo'),
                              'nombre' => $arb->__GET('sFamDescripcion'),
                              'parentId' => $arb->__GET('nFamPadre'));
            }
            echo '<ol class="tree">';
            $arbol_lista = "";
            for($j=0;$j<count($ex);$j++){
                if($ex[$j]['parentId'] == 0){
                    $arbol_lista .= '<li>
                                        <label>'.$ex[$j]['codigo'].'- '.$ex[$j]['nombre'].'</label>
                                        <input type="checkbox" checked id="folder1" />'
                                        .Arbol($ex,$ex[$j]['id']).'
                                    </li>';
                }  
            }
            echo $arbol_lista;
            echo '</ol>';
        }
        public function EliminarFamilia(){
            $fam = new Familia();
            $fam->__SET('Familia_Id', $_POST['Familia_Id']); 
            $dato = $this->familia_model->Hijos($_POST['Familia_Id']);
            if(count($dato)>0){  
                $fa = new Familia();
                $fa->__SET('Familia_Id', $_POST['Familia_Id']); 
                $fa->__SET('nFamPadre', $_POST['nFamPadre']);
                $this->familia_model->Padre($fa);
                $this->familia_model->Eliminar($fam);  
            }else{ 
                $this->familia_model->Eliminar($fam);                
            }
            echo '<div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Exito información modificado</strong>, los datos fueron eliminados correctamente.
                  </div>';        
        }
    }
    function Arbol($tree, $padre){
        $cont = 0;
        $list = "<ol>";
        for($i=0;$i<count($tree);$i++){
            if($tree[$i]['parentId'] == $padre){
                $list .= '<li>
                            <label><a onclick="FrmEditarFamilia(\''.$tree[$i]['id'].'\');">'.$tree[$i]['codigo'].'- '. $tree[$i]['nombre'] .'</a></label>
                            <input type="checkbox" checked id="'.$tree[$i]['nombre'].'" />'
                            .Arbol($tree, $tree[$i]['id']).' 
                          </li>';
                $cont++;
            }
        }    
        $list .= "</ol>";
        if($cont == 0){
            $list = "";
        }
        return $list;
    }
?>