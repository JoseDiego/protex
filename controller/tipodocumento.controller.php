<?php    
    require_once 'model/parametro.model.php';
    require_once 'model/parametro.entidad.php';
    require_once 'model/almacen.model.php';
    require_once 'model/almacen.entidad.php';
    require_once 'model/tipomovimiento.model.php';
    require_once 'model/tipomovimiento.entidad.php';
    require_once 'model/documento.model.php';
    require_once 'model/documento.entidad.php';
    require_once 'controller/metodos.controller.php';
    class TipodocumentoController extends MetodosController{
        private $parametro_model;
        private $almacen_model;
        private $tipomovimiento_model;
        public $documento_model;
        public function __CONSTRUCT(){
            $this->parametro_model = new ParametroModel();
            $this->almacen_model = new AlmacenModel();
            $this->tipomovimiento_model = new TipomovimientoModel();
            $this->documento_model = new DocumentoModel();
        }        
        public function Index(){
            $parametro = $this->parametro_model->ListarParametros();
            $tipodocumento = $this->documento_model->Listar();
            require_once 'view/header.php';
            require_once 'view/webforms/wfatipodocumento.php';
            echo "<script type='text/javascript'>
                        $(document).ready(function (){
                            $('.catalogos').addClass('active');
                        });
                  </script>";
            require_once 'view/footer.php';
        }    
        
        public function GuardarDocumento(){
            $doc = new Documento();
            if ($_POST['Documento_Id'] != 0 ? $doc->__SET('Documento_Id', $_POST['Documento_Id']) : '');
            $doc->__SET('Almacen_Id', $_POST['Almacen_Id']);
            $doc->__SET('nDocTipoMovimiento_Id', $_POST['Tipomovimiento_Id']);
            $doc->__SET('sDocNombre', $_POST['sDocNombre']);
            $doc->__SET('sDocNombreCorto', $_POST['sDocNombreCorto']);
            $doc->__SET('nDocNomAutomatico', $_POST['nDocNomAutomatico']);
            $doc->__SET('sDocSiguiente', $_POST['sDocSiguiente']);
            $doc->__SET('nDocAfectoIGV', $_POST['nDocAfectoIGV']);
            $doc->__SET('nDocEstado', $_POST['nDocEstado']);
            $doc->__SET('nDocEliminado', 0);
            $doc->__SET('Usuario_Id', $_SESSION['usu_codigo']);
            if ($_POST['Documento_Id'] != 0 ? $this->documento_model->Editar($doc) : $this->documento_model->guardar($doc));
        }

        public function FrmGuardarTipodocumento(){
            $almacen = $this->almacen_model->Listar();
            $tipomovimiento = $this->tipomovimiento_model->Listar();
            require_once 'view/webforms/wfatipodocumentoa.php';
        }    
        public function FrmEditarTipodocumento(){
            $almacen = $this->almacen_model->Listar();
            $tipomovimiento = $this->tipomovimiento_model->Listar();
            $datos= $this->documento_model->Buscar($_POST['Documento_Id']);
            foreach ($datos as $r):
                require_once 'view/webforms/wfatipodocumentoa.php';            
            endforeach;       
        }
        public function MostrarTipodocumento(){
            $producto = $this->documento_model->Buscar($_POST['Documento_Id']);
            foreach ($producto as $r):
                echo " <tbody>
                          <tr>
                             <th style='width: 40%;'>
                                <input type='hidden' name='Documento_IdPri' id='Documento_IdPri' value='".$r->__GET('Documento_Id')."'> 
                                Almacen
                             </th>
                             <td>".$r->__GET('sAlmNombre')."</td>
                          </tr>
                          <tr>
                             <th>Movimiento</th>
                             <td>".$r->__GET('sTMovNombre')."</td>
                          </tr>  
                          <tr>
                             <th>Nombre</th>
                             <td>".$r->__GET('sDocNombre')."</td>
                          </tr>  
                          <tr>
                             <th>Nombre corto</th>
                             <td>".$r->__GET('sDocNombreCorto')."</td>
                          </tr>                           
                          <tr>
                             <th>Automatico</th>
                             <td>".($r->__GET('nDocNomAutomatico') == 1 ? 'Si' :'No')."</td>
                          </tr>  
                          <tr>
                             <th>Numero siguiente</th>
                             <td>".$r->__GET('sDocSiguiente')."</td>
                          </tr> 
                          <tr>
                             <th>Afecto IGV</th>
                             <td>".($r->__GET('nDocAfectoIGV') == 1 ? 'Si' :'No')."</td>
                          </tr>  
                          <tr>
                             <th>Estado</th>
                             <td>".($r->__GET('nDocEstado') == 1 ? '<span class="label bg-green">Habilitado</span>' : '<span class="label bg-danger">Inhabilitado</span>')."</td>
                          </tr>
                       </tbody>
                     ";
            endforeach;
        }
        public function EliminarTipodocumento(){
            $doc = new Documento();
            $doc->__SET('Documento_Id', $_POST['idDocumento']);
            $this->documento_model->Eliminar($doc);
            echo '<script type="text/javascript">window.location="?c=Tipodocumento&a=Index";</script>';            
        }

        public function ListarConbobox() {
            $estado = false;
            $var = 0;
            $tipodocumento = $this->documento_model->Listar();
//            echo '<option value="">Seleccione tipo de documento</option>';
            foreach ($tipodocumento as $t) {
                if ($t->__GET('nDocTipoMovimiento_Id') == $_POST['Tipomovimiento_Id']) {
                    $estado = true;
                    echo '<option value="' . $t->__GET('Documento_Id') . '" '.($t->__GET('Documento_Id') == '7' ? 'selected':'').' '.($var==0 ? 'selected':'').'>' . $t->__GET('sDocNombre') . '</option>';
                    $var++;
                }
            }
            if($estado == false)
                echo '<option value="">DOCUMENTO NO DISPONIBLE</option>';
        }
        public function NumeroDocumento(){
            $tipodocumento = $this->documento_model->Buscar(!empty($_POST['Documento_Id']) ? $_POST['Documento_Id']:-1);
            if(count($tipodocumento) > 0){
                foreach ($tipodocumento as $tp){
                    echo $tp->__GET('sDocSiguiente');
                }
            }else{
                echo 0;
            }
        }

    }

?>