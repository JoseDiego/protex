<?php
require_once 'model/usuario.entidad.php';
require_once 'model/usuario.model.php';
require_once 'model/parametro.model.php';
require_once 'model/parametro.entidad.php';
require_once 'model/bitacorausuario.model.php';
require_once 'model/bitacorausuario.entidad.php';
class PrincipalController {
    private $usuario_model;
    private $parametro_model;
    private $bitacorausuario_model;
    public function __construct() {
        $this->usuario_model = new UsuarioModel();
        $this->parametro_model = new ParametroModel();
        $this->bitacorausuario_model = new BitacorausuarioModel();
    }
    public function Index(){
        if(!isset($_SESSION['usu_autenticado'])){
            require_once 'view/login.php';
        }else{
            $parametro = $this->parametro_model->ListarParametros();
            require_once 'view/header.php';
            require_once 'view/webforms/wfaprincipal.php';
            echo "<script type='text/javascript'>
                        $(document).ready(function (){
                            $('.inicio').addClass('active');
                        });
                  </script>";
            require_once 'view/footer.php';
        }
        
    }
    
    public function verificarUsuario(){
        $verificar = $this->usuario_model->autenticar($_POST['usuario'], $_POST['clave']);
        if(!empty($_POST['usuario']) && !empty($_POST['clave']) && !empty($_POST['codigocaptcha'])){
            if($_SESSION['captcha']==$_POST['codigocaptcha']){
                if(count($verificar)){
                    foreach ($verificar as $row){
                        $_SESSION['usu_autenticado'] = 'Si';
                        $_SESSION['usu_codigo'] = $row->__GET('Usuario_Id');
                        $_SESSION['usu_nombre'] = $row->__GET('sUsuNombre');
                        $_SESSION['usu_login'] = $row->__GET('sUsuLogin');
                        $_SESSION['usu_tipo'] = $row->__GET('nUsuTipo');
                    }
                    $biusu = new Bitacorausuario();
                    $biusu->__SET('nBiusuTipoModulo', 1);
                    $biusu->__SET('nBiusuUsuario_Id', $_SESSION['usu_codigo']);
                    $biusu->__SET('nBiusuDescripcion', 'Inició Sessión');
                    $this->bitacorausuario_model->guardarBitacorausuario($biusu);
                    echo '<script type="text/javascript">window.location="?c=Principal&a=Index";</script>';
                }else{
                    echo '<div class="alert alert-warning" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong>Advertencia</strong>, El usuario o contraseña no son correctos.
                              </div>';
                }            
            }else{
                echo '<div class="alert alert-danger" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong>Advertencia</strong>, El codigo captcha no es correcto.
                              </div>';
            }            
        }else{
            echo '<div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Error</strong>, los campos no son correctos.
                  </div>';
        }        
        echo "<script>
                $('#preloader').fadeOut('slow');
                $('body').css({'overflow':'visible'});
              </script>";
    }
    
    public function CerrarSession(){
        $biusu = new Bitacorausuario();
        $biusu->__SET('nBiusuTipoModulo', 1);
        $biusu->__SET('nBiusuUsuario_Id', $_SESSION['usu_codigo']);
        $biusu->__SET('nBiusuDescripcion', 'Cerro Sessión');
        $this->bitacorausuario_model->guardarBitacorausuario($biusu);
        session_destroy();        
        echo '<script type="text/javascript">window.location="?c=Principal&a=Index";</script>';
    }
    
    public function ImprimirTicket() {
        
        $parametro = $_GET['Movimiento'];
        
        //$comando = 'start /d "C:\STC R2\" start "" "C:\STC R2\STCCFD.exe" ' . $parametro . ' /SEPARATE /WAIT /I && exit';
        $comando = 'start /d "C:\xampp\htdocs\clubpacasmayo\view\librerias\dist\print\" start "" "C:\xampp\htdocs\clubpacasmayo\view\librerias\dist\print\PrintVale.exe" ' . $parametro . ' /SEPARATE /WAIT /I && exit';

        $comando_escapado = escapeshellcmd($comando);

        exec($comando_escapado, $resultadoSalida, $ejecucion);

        if ($ejecucion > 0) {
            
            echo 'Ocurrio un problema en la impresion.';
            
        } else {
            
            echo 'La impresion se realizo correctamente gracias.';
                        
        }
        
        echo '<script type="text/javascript">setTimeout(function(){window.close();}, 3000)</script>';
    }
}
